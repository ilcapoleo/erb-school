
<!-- END WRAPPER -->
<!-- Javascript -->
<style>
    .error{
        border: 1px solid red  !important;
        border-left: 1px double red !important;
        border-top: 2px double red !important;
    }


    .red-border{
        border: 1px solid red  !important;
    }
</style>




<script src="{{asset('assets/vendor/jquery/jquery.min.js')}}"></script>

<script src="{{asset('assets/vendor/bootstrap/js/bootstrap.min.js')}}"></script>

<script src="{{asset('assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>

<script src="{{asset('assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js')}}"></script>

<script src="{{asset('assets/vendor/chartist/js/chartist.min.js')}}"></script>

<script src="{{asset('assets/scripts/klorofil-common.js')}}"></script>

<script src="{{asset('assets/scripts/bootstrap-datepicker.min.js')}}"></script>

<script src="{{asset('assets/scripts/jquery.blockUI.js')}}"></script>

<script src="{{asset('assets/scripts/bootstrap-notify.js')}}"></script>





<!--End intialize datepicker -->


<!--Edit and Save -->


<!--tabs with content -->

<script>
    $(document).ready(function(){
        $(".nav-tabs a").click(function(){
            $(this).tab('show');
        });
        setTimeout($.unblockUI);

    });

</script>



<!--date inputs -->
<script>
    $(document).ready(function(){

        var date_input=$('input[name="date"]'); //our date input has the name "date"
        var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";

        var options={
            dateFormat: 'dd/mm/yy',
            container: container,
            orientation:"bottom right"
        };
        $("#date1").datepicker({format: 'yyyy-mm-dd',autoclose: true, minView: 2,todayBtn: true ,todayHighlight:true});
        $("#date2").datepicker({format: 'yyyy-mm-dd',autoclose: true, minView: 2,todayBtn: true ,todayHighlight:true});
        $("#date4").datepicker({format: 'yyyy-mm-dd',autoclose: true, minView: 2,todayBtn: true ,todayHighlight:true});
        $("#date5").datepicker({format: 'yyyy-mm-dd',autoclose: true, minView: 2,todayBtn: true ,todayHighlight:true});
        $(".alldate").datepicker({format: 'yyyy-mm-dd',autoclose: true, minView: 2,todayBtn: true ,todayHighlight:true});
    })


</script>

<!-- show and hide date of leavving -->


<script>


    $(function () {
        $("input[name='leave']").click(function () {
            if ($("#yes").is(":checked")) {
                $(".leaving").show();
            } else {
                $(".leaving").hide();
            }
        });
    });





</script>
<!-- creat -->

<script>
    $('#creat').click(function(){

        if ($(this).hasClass('save1')) {
            $(this).text("Creat").removeClass('save1');
            $('.btn-danger').show();

            $("input[type=text]").attr('disabled', true).css({
                'border': 'none',
                'outline': 'none',
                'box-shadow' : 'none'
            });
            $("select").prop("disabled", true);
            $("input[type=radio]").attr('disabled', true);
            $("input[name=date]").attr('disabled', true);
            $("input[type=number]").attr('disabled', true);

            $( "#date1" ).datepicker( "option", "disabled", true );
            $( "#date2" ).datepicker( "option", "disabled", true );

            $( "#date3" ).datepicker( "option", "disabled", true );

            $( "#date4" ).datepicker( "option", "disabled", true );



        } else {
            $(this).text("Save").addClass('save1');
            $('.btn-danger').hide();
            $("input[type=text]").attr('disabled', false).css({
                'border': '#eaeaea solid 1px',

                'border-radius':'2px',
                'background-color':'#fff',
                'height': '32px'
            }).focus().val('');


            $("input[type=number]").attr('disabled',false).css({
                'border': '#eaeaea solid 1px',

                'border-radius':'2px',
                'background-color':'#fff',
                'height': '32px'
            }).focus().val('');
            $("select").removeAttr('disabled').prop('selectedIndex', 0);

            $("input[type=radio]").attr('disabled', false);
            $( "#date1" ).datepicker( "option", "disabled", false );
            $( "#date2" ).datepicker( "option", "disabled", false );

            $( "#date3" ).datepicker( "option", "disabled", false );
            $( "#date4" ).datepicker( "option", "disabled", false );

        }

    });
</script>

{{--show users --}}

<script>

    $(document).ready(function() {
        $("#menu li a").on('click', function(e) {
            $("#menu li").removeClass('active');
            $(this).parents('li').addClass('active');
            e.preventDefault()
            var page = $(this).data('page');
            $("#pages .page:not('.hide')").stop().fadeOut('fast', function() {
                $(this).addClass('hide');
                $('#pages .page[data-page="'+page+'"]').fadeIn('slow').removeClass('hide');
            });
        });
    });


</script>


<!-- required inputs -->

<script>
    $( ".required" ).css( "background-color", "lavender" );
    $( ".required" ).closest().css( "background", "yellow" );
    // $( ".required" ).attr('oninvalid',"this.setCustomValidity(notification('glyphicon glyphicon-warning-sign','Warning','nova','danger'))");
</script>
<script>
    const validationErrorClass = 'validation-error';
    const parentErrorClass = 'has-validation-error';
    const inputs = document.querySelectorAll('input, select, textarea');
    inputs.forEach(function (input) {
        function checkValidity (options) {
            const insertError = options.insertError;
            const parent = input.parentNode;
            const error = parent.querySelector(`.${validationErrorClass}`)
                || document.createElement('div');

            if (!input.validity.valid && input.validationMessage) {
                error.className = validationErrorClass;
                // error.textContent = input.validationMessage;
                notification('glyphicon glyphicon-warning-sign','Warning','Please This Field Is Required','danger')


                if (insertError) {
                    parent.insertBefore(error, input);
                    parent.classList.add(parentErrorClass)
                }
            } else {
                parent.classList.remove(parentErrorClass);
                error.remove()
            }
        }
        input.addEventListener('input', function () {
            // We can only update the error or hide it on input.
            // Otherwise it will show when typing.

            checkValidity({insertError: false})
        })
        input.addEventListener('invalid', function (e) {
            // prevent showing the default display

            e.preventDefault();

            // We can also create the error in invalid.
            checkValidity({insertError: true})
        });
    })

</script>

<script>

   $(document).on('click','#save_work',function () {
       var inputs_valid = $('.required');
       $(this).css({
           "border": "",
       });
       inputs_valid.each(function(){
           if( $(this).val() == '' || !$(this).val() ) {

               $(this).css({
                   "border": "1px solid red",
               });
               $(this).siblings('.select2').css({
                   "border": "1px solid red",
               });

           }
           else{
               $(this).css({'border' : ""});
               $(this).siblings('.select2').css({'border' : ""});
           }
       });
   });


</script>





<!-- on icon of calendarr -->

<script>
    $('.input-group').find('.fa-calendar').on('click', function(){
        $(this).parent().siblings('#datetime1').trigger('focus');
    });

    $('.input-group').find('.fa-calendar').on('click', function(){
        $(this).parent().siblings('#datetime2').trigger('focus');
    });

    $('.input-group').find('.fa-calendar').on('click', function(){
        $(this).parent().siblings('#date1').trigger('focus');
    });

    $('.input-group').find('.fa-calendar').on('click', function(){
        $(this).parent().siblings('#date2').trigger('focus');
    });

    $('.input-group').find('.fa-calendar').on('click', function(){
        $(this).parent().siblings('#date4').trigger('focus');
    });

    $('.input-group').find('.fa-calendar').on('click', function(){
        $(this).parent().siblings('#requested_date').trigger('focus');
    });

    $('.input-group').find('.fa-calendar').on('click', function(){
        $(this).parent().siblings('#startDate1').trigger('focus');
    });
    $('.input-group').find('.fa-calendar').on('click', function(){
        $(this).parent().siblings('#approved_date').trigger('focus');
    });
    $('.input-group').find('.fa-calendar').on('click', function(){
        $(this).parent().siblings('#borrow_date').trigger('focus');
    });

</script>




<script>

    $("#home_link").on('click', function() {

        window.location.href = "{{URL::to('/')}}"


    });





    $("#hr_link").on('click', function() {

   var url2=$("#HR a:eq(0)").attr("href");

        window.location = url2;

    });

    $("#Purchase_link").on('click', function() {

        var url3=$("#Purshase a:eq(0)").attr("href");

        window.location = url3;

    });
    $("#Inventory_link").on('click', function() {

        var url4=$("#inventory a:eq(0)").attr("href");

        window.location = url4;

    });
    $("#Budget_link").on('click', function() {

        var url5=$("#budget a:eq(0)").attr("href");

        window.location = url5;

    });
    $("#System_link").on('click', function() {

        var url6=$("#system a:eq(0)").attr("href");

        window.location = url6;

    });

    $("#accounting_link").on('click', function() {

        var url7=$("#accounting a:eq(0)").attr("href");

        window.location = url7;

    });

    $("#admission_link").on('click', function() {

        var url8=$("#admission a:eq(0)").attr("href");

        window.location = url8;

    });

</script>



<script>

    function notification(icon_msg,title_msg,notify_message,type_message) {

        var notify = $.notify({
            // options
            icon: icon_msg,
            title:  title_msg ,
            message: notify_message,
        }, {
            // settings
            element: 'body',
            type: type_message,
            allow_dismiss: true,
            newest_on_top: false,
            showProgressbar: false,
            placement: {
                from: "top",
                align: "left"
            },
            offset: {
                x: 920,
                y: 60
            },

            spacing: 10,
            z_index: 1031,
            animate: {
                enter: 'animated fadeInRight',
                exit: 'animated fadeOutRight'
            },

        });
        return(notify);

    }


</script>

<script>
    $.blockUI({ message: $('#floatingCirclesG'),
        css: { backgroundColor: 'transparent',
            border:'none'
        } });
</script>


<script>
            @if(($errors)!=null)
            @foreach($errors->all() as $error)
    var er = '{{$error}}';
    notification('glyphicon glyphicon-warning-sign', 'Warning', er, 'danger');
    @endforeach
    @endif
</script>
<script>
    @if(isset($succsess))
    notification('glyphicon glyphicon-ok-sign','Congratulations!','Data Saved Succefully.','success');
    console.log('susususususu');
    @endif
</script>






@yield('custom_footer')

















