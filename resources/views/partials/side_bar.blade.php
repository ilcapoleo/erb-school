<!-- LEFT SIDEBAR -->


<style>
    .profile-userpic img {
        float: none;
        margin: 0 auto;
        width: 50%;
        height: 50%;

    }

    .copyright-sidenav{
        text-align: left;
    }

  .side_footer{
      position: absolute;
      bottom: 0px;
      right: 92px;

        margin-bottom: -5px;

      background-color: #2B333E;
  }


    hr {
        -moz-border-bottom-colors: none;
        -moz-border-image: none;
        -moz-border-left-colors: none;
        -moz-border-right-colors: none;
        -moz-border-top-colors: none;
        border-color: #EEEEEE -moz-use-text-color #EEEEEE;
        border-style: solid none;
        border-width: 0.5px 0;
        margin: 11px 0;
        bottom: -15px;
    }



    #pages{
        position: fixed;
        top: 205px;
        margin-left: auto;
        margin-right: auto;
        bottom: 110px;
        overflow: auto;
    }

   .profile-userpic{
       position: absolute;
       top: 0px;
       /* width: 800px; */
       /* height: 100px; */
       overflow: hidden;
   }
</style>
<div id="sidebar-nav" class="sidebar">
    <div class="sidebar-scroll" style="height: 78%!important;    overflow: hidden;
    width: auto;">

            <div class="profile-userpic">
                <img src="{{asset('assets/img/logo-dark.png')}}" alt="Klorofil Logo" class="img-responsive logo">                </div>

             <div id="pages">
            <!--home -->
            <ul class="nav side_sheets  page hide" data-page="home" id="home">
                <li><a id="change_password_side_bar" href="/change_password"><i class="lnr lnr-cog"></i> <span>Settings</span></a></li>

             {{--<li><a  href="#" ><i class="lnr lnr-envelope"></i> <span>Message</span></a></li>--}}



            </ul>

            <!--Hr -->
            <ul class="nav side_sheets page hide" data-page="HR" id="HR">
                <li class="header-menu">
                    <span>Human Resources</span>
                </li>
                @can('employee')
                    <li><a id="admin_adduser_bar" href="/employees" class=""><i class="lnr lnr-user"></i> <span>Employee</span></a></li>
                @endcan

                <li><a id="employees_allowance" href="/employees_allowance" class=""><i class="lnr lnr-user"></i> <span>Employee Allowance</span></a></li>

                <li><a id="subsalary_scale_side_bar" href="/salaries_plan_table" class=""><i class="lnr lnr-map"></i> <span>Salaries Plan</span></a></li>

                <li><a id="allowence_details_side_bar" href="/scale_of_allowence_table" class=""><i class="lnr lnr-sort-amount-asc"></i> <span>Scale of Allowence</span></a></li>

                <li class="header-menu">
                    <span> Leaves</span>
                </li>

                @can('leave-request')
                    <li><a id="leave_request_side_bar" href="/show_leave_request" class=""><i class="lnr lnr-pencil"></i> <span>Leave Request</span></a></li>
                @endcan

                <li class="header-menu">
                    <span> Configration</span>
                </li>

                @can('leave-type')
                    <li><a id="leave_side_bar" href="/show_leave_type" class=""><i class="lnr lnr-sun"></i> <span>Leave Type</span></a></li>
                @endcan

            @can('department')
                    <li><a id="department_side_bar" href="/department_table" class=""><i class="lnr lnr-apartment"></i> <span>Department</span></a></li>
                @endcan

                    @can('job')
                        <li><a id="job_side_bar" href="/jobs_table" class=""><i class="lnr lnr-briefcase"></i> <span>Jobs</span></a></li>
                    @endcan

                    <li><a id="country_side_bar" href="/countries" class=""><i class="lnr lnr-briefcase"></i> <span>Countries</span></a></li>

                    <li><a id="nationality_side_bar" href="/nationalities" class=""><i class="lnr lnr-briefcase"></i> <span>Nationality</span></a></li>

                    @can('salary-scale')
                        <li><a id="salary_scale_side_bar" href="/salary_table" class=""><i class="lnr lnr-line-spacing"></i> <span>Salary Scale</span></a></li>
                    @endcan


                    @can('allowence-deduction')
                        <li><a id="allowence_deduct_side_bar" href="/allowence_deduction/table" class=""><i class="lnr lnr-sort-amount-asc"></i> <span>Allowence & Deduction</span></a></li>
                    @endcan


















                    {{--<li><a id="working_days_side_bar" href="/working_days" class=""><i class="lnr lnr-calendar-full"></i> <span>Working Days</span></a></li>--}}










                @can('borrow')
                {{--<li><a id="borrow_side_bar" href="/borrow_table" class=""><i class="lnr lnr-hand"></i> <span>Borrow</span></a></li>--}}
                @endcan


            </ul>

            <!-- Purshase -->

            <ul class="nav side_sheets page hide" data-page="Purchase" id="Purshase" >
                <li class="header-menu">
                    <span>Purchase</span>
                </li>
                @can('Purchase-order')
                    <li><a id="create_po_side_bar" href="/show_po" class=""><i class="lnr lnr-list"></i> <span>Purchase Order</span></a></li>
                @endcan

                <li><a id="vendor_side_bar" href="/vendors" class=""><i class="lnr lnr-users"></i> <span>Suppliers</span></a></li>
                <li class="header-menu">
                    <span>Products</span>
                </li>


                <li><a id="product_side_bar" href="/products" class=""><i class="lnr lnr-pencil"></i> <span>Products</span></a></li>
                <li class="header-menu">
                    <span>Invoice</span>
                </li>

                @can('create-invoice')
                <li><a id="inventory_acc_side_bar" href="/show_invoice" class=""><i class="lnr lnr-file-add"></i> <span>Creat Invoice</span></a></li>
                @endcan

                @can('pay-invoice')
                {{--<li><a id="pay_invoice_side_bar" href="/pay_invoice" class=""><i class="lnr lnr-map"></i> <span>Pay Purshasing Invoice</span></a></li>--}}
                @endcan



            </ul>


            <!-- inventory -->



            <ul class="nav side_sheets  page hide " data-page="Inventory" id="inventory" >


                <li class="header-menu">
                    <span>Operations</span>
                </li>
                @can('purshase-document')
                <li><a id="inventory_side_bar" href="/show_inventory" class=""><i class="lnr lnr-layers"></i> <span>Purshase Document</span></a></li>
                @endcan

                <li class="header-menu">
                    <span>Products</span>
                </li>


                <li><a id="product_side_bar" href="/products" class=""><i class="lnr lnr-pencil"></i> <span>Products</span></a></li>


                <li class="header-menu">
                    <span>Configration</span>
                </li>


                <li><a id="operation_side_bar" href="/operation" class=""><i class="lnr lnr-pencil"></i> <span>Types of Operation</span></a></li>


            </ul>


                 <!-- Budget -->
                 <ul class="nav side_sheets page hide " data-page="Budget" id="budget" >
                     @can('budget-details')
                     <li><a id="budget_details_side_bar" href="/budget_details" class=""><i class="lnr lnr-chart-bars"></i> <span>Budget Details</span></a></li>
                     @endcan

                     @can('budget-total')
                     <li><a id="budget_total_side_bar" href="/budget_total" class=""><i class="lnr lnr-book"></i> <span>Budget Master</span></a></li>
                     @endcan

                 </ul>


                 <!-- System -->
                 <ul class="nav side_sheets page hide " data-page="System" id="system" >

                     @can('budget-total')
                         <li><a id="system_side_bar" href="/show_users" class=""><i class="lnr lnr-file-add"></i> <span>Add User</span></a></li>
                     @endcan

                 </ul>


                 <!-- Accounting -->
                 <ul class="nav side_sheets page hide " data-page="Accounting" id="accounting" >

                     <li><a id="accounting_side_bar" href="/journals/table" class=""><i class="lnr lnr-layers"></i> <span>Journals</span></a></li>

                     {{--<li><a id="journal_entry_side_bar" href="/journal-entry" class=""><i class="lnr lnr-layers"></i> <span>Journal Entry</span></a></li>--}}



                     {{--<li><a id="costcenter_side_bar" href="/cost_centers" class=""><i class="lnr lnr-apartment"></i> <span>Cost Center</span></a></li>--}}

                     {{--<li><a id="check_side_bar" href="/show_check" class=""><i class="lnr lnr-license"></i> <span>Check</span></a></li>--}}

                 </ul>

                 <!--Admission-->
                 <ul class="nav side_sheets page hide " data-page="Admission" id="admission" >

                     <li><a id="student_side_bar" href="/students_table" class=""><i class="lnr lnr-graduation-hat"></i> <span>Student</span></a></li>
                     <li><a id="country_side_bar" href="/countries" class=""><i class="lnr lnr-briefcase"></i> <span>Countries</span></a></li>

                     <li><a id="nationality_side_bar" href="/nationalities" class=""><i class="lnr lnr-briefcase"></i> <span>Nationality</span></a></li>


                     {{--<li><a id="fees_side_bar" href="/fees_table" class=""><i class="lnr lnr-file-add"></i> <span>Fees Head</span></a></li>--}}

                     {{--<li><a id="scale_fees_side_bar" href="/scale_fees" class=""><i class="lnr lnr-line-spacing"></i> <span>Scale of Fees</span></a></li>--}}


                     {{--<li><a id="classes_side_bar" href="/classes_table" class=""><i class="lnr lnr-enter"></i> <span>Class</span></a></li>--}}

                 </ul>



             </div>


        <div class="nav side_footer" >
            <hr>
            <div class="container-fluid">

                <p class="copyright-sidenav" >Powered By <a href="http://www.octopusis.com/" target="_blank">Octopus  </a></p>

                +2 03 57 44077
                <br>
                +2 010 3386 6279
            </div>
        </div>





    </div>
</div>