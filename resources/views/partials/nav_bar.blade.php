<!-- NAVBAR -->
<nav class="navbar navbar-default navbar-fixed-top">

    <div class="container-fluid ">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>




        <div class="navbar-btn">
            <button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
        </div>
        <div class="collapse navbar-collapse collapse1" id="bs-example-navbar-collapse-1">

            <ul class="nav navbar-nav" id="menu"  class="menu">
                <li id="home_nav_bar"><a href="/" id="home_link"  data-page="home">Home </a></li>
                @hasanyrole('Manager-HR|Accountant-HR|Employee-HR')
              <li id="hr_nav_bar"><a href="/employees" id="hr_link" data-page="HR">HR</a></li>
                @endhasanyrole

                @hasanyrole('Manager-PO|Accountant-PO|Use-Own-PO|User-All-PO')
                <li id="Purchase_nav_bar"><a href="/products" id="Purchase_link" data-page="Purchase">Purchase</a></li>
                @endhasanyrole

                @hasanyrole('Manager-Inventory|Accountant-Inventory|User-Own-Inventory|User-All-Inventory')
                <li id="Inventory_nav_bar"><a href="/show_inventory" id="Inventory_link" data-page="Inventory">Warehouse</a></li>
                @endhasanyrole



                @hasanyrole('Manager-Budget|Accountant-Budget|User-Own-Budget|User-All-Budget')
                {{--<li id="Budget_nav_bar"><a href="/budget_details" id="Budget_link" data-page="Budget">Budget</a></li>--}}
                @endhasanyrole





                <li id="accounting_nav_bar"><a href="/journals" id="accounting_link" data-page="Accounting">Accounting</a></li>

              <li id="admission_nav_bar"><a href="/student" id="admission_link" data-page="Admission">Admission</a></li>



                <li id="System_nav_bar"><a href="/show_users" id="System_link" data-page="System">System</a></li>



            </ul>


            <div id="navbar-menu">

                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown hide">
                        <a href="#" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                            <i class="lnr lnr-alarm"></i>
                            <span class="badge bg-danger">5</span>
                        </a>
                        <ul class="dropdown-menu notifications">
                            <li><a href="#" class="notification-item"><span class="dot bg-warning"></span>System space is almost full</a></li>
                            <li><a href="#" class="notification-item"><span class="dot bg-danger"></span>You have 9 unfinished tasks</a></li>
                            <li><a href="#" class="notification-item"><span class="dot bg-success"></span>Monthly report is available</a></li>
                            <li><a href="#" class="notification-item"><span class="dot bg-warning"></span>Weekly meeting in 1 hour</a></li>
                            <li><a href="#" class="notification-item"><span class="dot bg-success"></span>Your request has been approved</a></li>
                            <li><a href="#" class="more">See all notifications</a></li>
                        </ul>
                    </li>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            @php
                            $x = \App\Employee::where('employee_user_id',auth()->user()->id)->first()->employee_id.'_.png';
                            @endphp
                            @if (file_exists('uploads/employees/'.$x))
                            <img src="{{url('uploads/employees/'.$x)}}" class="img-circle" alt="Avatar">
                            @else
                            <img src="{{url('uploads/employees/default2.png')}}" class="img-circle" alt="Avatar">
                            @endif
                            <span>{{ Auth::user()->name }}</span>
                            <i class="icon-submenu lnr lnr-chevron-down"></i></a>
                        <ul class="dropdown-menu">
                            {{--<li><a href="#"><i class="lnr lnr-envelope"></i> <span>Message</span></a></li>--}}
                            <li><a href="change_password"><i class="lnr lnr-cog"></i> <span>Settings</span></a></li>
                            <li><a href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"  ><i class="lnr lnr-exit"></i> <span>Logout</span></a><form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form></li>
                        </ul>
                    </li>
                    <!-- <li>
                        <a class="update-pro" href="https://www.themeineed.com/downloads/klorofil-pro-bootstrap-admin-dashboard-template/?utm_source=klorofil&utm_medium=template&utm_campaign=KlorofilPro" title="Upgrade to Pro" target="_blank"><i class="fa fa-rocket"></i> <span>UPGRADE TO PRO</span></a>
                    </li> -->
                </ul>
            </div>


        </div> <!--collapse -->

    </div>
</nav>
<!-- END NAVBAR -->



<div id="floatingCirclesG" style="dispaly:none;">
    <div class="f_circleG" id="frotateG_01"></div>
    <div class="f_circleG" id="frotateG_02"></div>
    <div class="f_circleG" id="frotateG_03"></div>
    <div class="f_circleG" id="frotateG_04"></div>
    <div class="f_circleG" id="frotateG_05"></div>
    <div class="f_circleG" id="frotateG_06"></div>
    <div class="f_circleG" id="frotateG_07"></div>
    <div class="f_circleG" id="frotateG_08"></div>
</div>


<div class="se-pre-con"></div>
