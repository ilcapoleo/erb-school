<head>
    <title>The British School Alexandria</title>
    <link rel="icon" href="{{asset('assets/img/logo.ico')}}" type="image/x-icon"> 	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <!-- VENDOR CSS -->

    <link rel="stylesheet" href="{{asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}">

    <link rel="stylesheet" href="{{asset('assets/vendor/font-awesome/css/font-awesome.min.css')}}">

    <link rel="stylesheet" href="{{asset('assets/vendor/linearicons/style.css')}}">

    <link rel="stylesheet" href="{{asset('assets/vendor/chartist/css/chartist-custom.css')}}">

    <link rel="stylesheet" href="{{asset('assets/css/bootstrap-datepicker3.css')}}">
    
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap-datetimepicker.min.css')}}">

    <link rel="stylesheet" href="{{asset('assets/css/animate.css')}}">



    <link rel="stylesheet" href="{{asset('assets/css/bootstrap-iso.css')}}">

    <!-- MAIN CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/main.css')}}">

    <!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
    <link rel="stylesheet" href="{{asset('assets/css/demo.css')}}">

    <!-- GOOGLE FONTS -->
    <link href="{{asset('css/css_google.css')}}" rel="stylesheet">
    <!-- ICONS -->
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('assets/img/apple-icon.png')}}">

    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('assets/img/favicon.png')}}">
    <link rel="stylesheet" href="{{asset('assets/css/smpSortableTable.css')}}">

    <link rel="stylesheet" href="{{asset('css/jquery.dataTables.min.css')}}">
    <style>
        .radios{
            margin-top: 5%;
        }
        .dataTables_filter, .dataTables_info { display: none; !important;}


        /*input:invalid{*/
            /*border-color:red;*/
        /*}*/
        /*select:invalid{*/
            /*border-color:red;*/
        /*}*/
        /*selector:invalid{*/
            /*border-color:red;*/
        /*}*/
        /*textarea:invalid{*/
            /*border-color:red;*/
        /*}*/



        .select2-container--default:required:invalid .select2-selection--single:required:invalid {

            border: 1px solid red;}

    </style>


</head>
