@extends('layouts.app')

@section('content')




    <link rel="icon" href="{{asset('assets/img/logo.ico')}}" type="image/x-icon"> 	<meta charset="utf-8">

    <style>
        footer {
            color: #666;
            background: #222;
            posittion: fixed!important;
            bottom: 0px;
            padding: 17px 0 18px 0;
            border-top: 1px solid #000;
            text-align: center;
            width: 100%;
            bottom: 0px;
        }
        footer > p >a {
            color: #EF8D2A!important;
        }
        footer a:hover {
            color: #EF8D2A;
        }

        /* responsive footer fix by Aalaap Ghag
        This is where the magic happens */
        @media (max-width: 767px) {

            footer  {
                padding-left: 20px;
                padding-right: 20px;
            }
        }

        .container {
            max-width: 940px;
        }
    </style>

    <div id="wrapper">
        <div class="wrapper fadeInDown">
            <div id="formContent">
                <!-- Tabs Titles -->
                <h2 class="active"> Sign In </h2>

                <!-- Icon -->
                <div class="fadeIn first">
                    <img src="{{ asset('assets/img/school.png') }}" id="icon" alt="User Icon" />
                </div>

                <!-- Login Form -->
                <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                    @csrf
                    {{ csrf_field() }}
                    {{--<input type="text" id="login" class="fadeIn second" name="login" placeholder="login">--}}

{{--                        <input id="email" type="email" class="fadeIn second form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus placeholder="Email">

                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                        --}}
                        <input id="name" type="text" class="fadeIn second form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus placeholder="User Name">

                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                        @endif

                    {{--<input type="password" id="password" class="fadeIn third" name="login" placeholder="password">--}}

                        <input id="password" type="password" class="fadeIn third form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Password">

                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif


                    <div class="checkbox">
                        <label>
                            <input type="checkbox" id="rememberMe" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
                        </label>
                    </div>
                    <input value="{{ __('Login') }}" type="submit" class="fadeIn fifth">

                    </input>

                </form>

                <!-- Remind Passowrd -->
                <div id="formFooter">
                    <a class="underlineHover" href="{{ route('password.request') }}">
                        {{ __('Forgot Your Password?') }}
                    </a>
                </div>

            </div>
        </div>
    </div>




    <footer style=" color: #666;
            position: relative;
            bottom: 0px;
            padding: 17px 0 18px 0;
            text-align: center;
            width: 100%;
            bottom: 0px;">
        <div class="container">
            <p > © Copyright 2018 <a href="http://www.octopusis.com/" style="color: #EF8D2A;"> Octopus For Integrated Solutions.</a> All rights reserved.</p>
        </div>
    </footer>



{{--<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                        @csrf
                        {{ csrf_field() }}

                        <div class="form-group row">
                            <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="fadeIn second form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="fadeIn third form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>--}}
    <script src="{{asset('assets/vendor/jquery/jquery.min.js')}}"></script>

    <script>
        $(document).on('change','#name',function () {
            $('#password').val('');
        });
    </script>
@endsection

{{--
@section('custom_footer')



@endsection--}}
