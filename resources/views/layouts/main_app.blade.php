<!doctype html>
<html lang="en">

@include('partials.header')

<body>
<!-- WRAPPER -->
<div id="wrapper">
@include('partials.nav_bar')
@include('partials.side_bar')
    <!-- MAIN -->
@yield('main_content')
    <!-- END MAIN CONTENT -->

</div>
<!-- END MAIN -->

@include('partials.footer')
<!-- intialize datepicker -->


</body>

</html>
