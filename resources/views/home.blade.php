@extends('layouts.main_app')

@section('main_content')

    <div class="main">

        <div class="container">
            <div class="jumbotron">
                <h1>Welcome to The British School!</h1>
                <p>This Dashboard is designed to give a flavour of our school as well as useful information. .</p>
            </div>

        </div>

    </div>
@endsection


@section('custom_footer')
    {{--------------------  active side & nav bar ------------------------}}
    <script>
        $('.side_sheets').addClass('hide');
        $('#home').removeClass('hide');

        $("#menu li").removeClass('active');
        $('#home_nav_bar').addClass('active');



    </script>




@endsection