@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">create permission</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <label> role name </label>
                    <input id="role" type="text">
                        <div class="col-md-6">
                            <select multiple id="permission" type="text" class="form-control{{ $errors->has('permission') ? ' is-invalid' : '' }}" name="permission[]" value="{{ old('permission') }}" required autofocus>
                                @foreach($permissions as $id=>$role)
                                    <option value="{{$id}}">{{$role}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-md-6">
                            <select  id="module" type="text" class="form-control{{ $errors->has('modules') ? ' is-invalid' : '' }}" name="modules" value="{{ old('modules') }}" required autofocus>
                                @foreach($modules as $id=>$role)
                                    <option value="{{$id}}">{{$role}}</option>
                                @endforeach
                            </select>
                        </div>

                    <button id="set_role" >submit</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{asset('js/jquery-1.11.3.min.js')}}"></script>

<script>
    $(document).on('click','#set_role',function(){
       var role = $('#role').val();
       var permissions = $('#permission').val();
       var module = $('#module').val();

       console.log(role,permissions,module);

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/add_new_role/',
            dataType : 'json',
            type: 'get',
            data: {
                role:role,
                permissions:permissions,
                module:module

            },

            success:function(response) {
                $('#role').val('');
                console.log(response);

            }

        });
    });
</script>
@endsection
