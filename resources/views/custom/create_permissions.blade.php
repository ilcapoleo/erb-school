@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">create permission</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <label> permition name </label>
                    <input id="permission" type="text">
                    <button id="set_permistion" >submit</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{asset('js/jquery-1.11.3.min.js')}}"></script>

<script>
    $(document).on('click','#set_permistion',function(){
       var permission = $('#permission').val();

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/add_new_permission/',
            dataType : 'json',
            type: 'get',
            data: {
                permission:permission,
            },

            success:function(response) {
                $('#permission').val('');
                console.log(response);

            }

        });
    });
</script>
@endsection
