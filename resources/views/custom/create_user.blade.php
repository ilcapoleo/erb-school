@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">create permission</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif



                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control" name="password" required>

                                        <strong id="password_error"></strong>

                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                </div>
                            </div>

                        <div class="form-group row">
                            @foreach($moduls as $module)
                                <div id="{{$module->id}}" >{{$module->name}}</div>
                                <select class="roles" id="role" type="text" class="form-control{{ $errors->has('role') ? ' is-invalid' : '' }}" name="role" value="{{ old('role') }}" required autofocus>
                                @foreach($roles as $role)
                                    @if( $role->module_id == $module->id )
                                        <option value="{{$role->name}}">
                                            {{$role->name}}
                                        </option>

                                    @endif
                                @endforeach
                                </select>
                            @endforeach
                        </div>

                                                  {{--   <div class="form-group row">
                                                            <label for="role" class="col-md-4 col-form-label text-md-right">Role</label>

                                                            <div class="col-md-6">
                                                                <select id="role" type="text" class="form-control{{ $errors->has('role') ? ' is-invalid' : '' }}" name="role" value="{{ old('role') }}" required autofocus>
                                                                    @foreach($roles as $id=>$role)
                                                                        <option value="{{$id}}">{{$role}}</option>
                                                                    @endforeach
                                                                </select>

                                                                @if ($errors->has('role'))
                                                                    <span class="invalid-feedback">
                                                                    <strong>{{ $errors->first('role') }}</strong>
                                                                </span>
                                                                @endif
                                                            </div>
                                                        </div>--}}


                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button id="sub_user" class="btn btn-primary">
                                        {{ __('Register') }}
                                    </button>
                                </div>
                            </div>






                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{asset('js/jquery-1.11.3.min.js')}}"></script>

<script>
    $(document).on('click','#sub_user',function(){
       var name = $('#name').val();
       var password = $('#password').val();
       var email = $('#email').val();
       var password_confirmation = $('#password-confirm').val();
       var role = $('#role').val();
       var roles = $('.roles');
       var roles_data = [];

        roles.each(function(){
            var vall = $(this).val();
            roles_data.push(vall);

        });


        console.log(roles_data);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/register_new_users/',
            dataType : 'json',
            type: 'get',
            data: {

                name:name,
                email:email,
                password:password,
                password_confirmation:password_confirmation,
                role:role,
                roles_data:roles_data


            },

            success:function(response) {

                /*console.log(response);*/
                if(response['error'])
                {
                $.each(response['error'], function (key, val)
                {
                    //alert(key + val);
                    console.log(key+'_error');
                    $("#"+key+'_error').html(val[0]);
                    //console.log(val[0]);

                })
                }

            }

        });
    });
</script>
@endsection
