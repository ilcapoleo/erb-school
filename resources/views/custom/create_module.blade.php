@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">create module</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <label> module name </label>
                    <input id="module_name" type="text">

                    <label> module link </label>
                    <input id="module_link" type="text">

                    <button id="set_module" >submit</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{asset('js/jquery-1.11.3.min.js')}}"></script>

<script>
    $(document).on('click','#set_module',function(){
       var module_name = $('#module_name').val();
       var module_link = $('#module_link').val();



        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/add_new_module/',
            dataType : 'json',
            type: 'get',
            data: {
                module_name:module_name,
                module_link:module_link,

            },

            success:function(response) {
                $('#module_name').val('');
                $('#module_link').val('');
                console.log(response);

            }

        });
    });
</script>
@endsection
