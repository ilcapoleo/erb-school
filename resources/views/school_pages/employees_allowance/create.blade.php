@extends('layouts.main_app')

@section('main_content')
    <link href="{{asset('css/fastselect.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/select2.min.css')}}" rel="stylesheet" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        input.col-md-4.text-center {

            border:0;
            background-color: #fff;
        }

        input[type=number]{
            width: 80px;
        }

        .collapse > .row>.col-md-4>.btn, .collapse > .row>.col-md-4>.dropdown>.btn {
            margin-top: 0;

        }


        .third-nav{
            background-color:#eeeeee;
        }

        .panel .table > thead > tr > td:first-child, .panel .table > thead > tr > th:first-child, .panel .table > tbody > tr > td:first-child, .panel .table > tbody > tr > th:first-child, .panel .table > tfoot > tr > td:first-child, .panel .table > tfoot > tr > th:first-child{
            padding-left: 0px;
        }


        .myselect2{
            width:171px;
        }

        span.select2.select2-container.select2-container--default{
            width: 100%!important;
        }

        .error{
            border: 1px solid red  !important;
            border-left: 1px double red !important;
            border-top: 2px double red !important;
        }


        .red-border{
            border: 1px solid red  !important;
        }

    </style>
    <!-- failuer messages -->
    <div class="alert alert-danger errorMessage1" style="position: absolute;">
        Your should fill all fields!

    </div>    <div class="main">
        <!-- Navbar-content -->
        <nav class="navbar navbar-default navbar2">

            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">


                    <!--breadcrumbs-->
                    <div class="row">

                        <div class="col-md-4 col-lg-5">
                        <ol class="breadcrumb breadcrumb_nav">
                            <li><a href="#">Employee Allowance</a></li>
                            <li class="active">
                                @if(isset($department))
                                    @if($department->salary_id != 0)
                                        {{$department->salary_id}}
                                    @else
                                        Draft
                                    @endif
                                @endif


                            </li>
                        </ol>
                        </div>
                            <div class="hide col-md-4 col-lg-5 printo" >
                                <div class="dropdown">
                                    <button class=" btn btn-primary  dropdown-toggle" type="button"
                                            data-toggle="dropdown" id="more">More
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" id="print">Print</a></li>
                                    </ul>
                                </div>
                            </div>


                    </div>
                    <!--End breadcrumbs-->


                    <div class="row">



                    </div> <!--collapse -->

                </div><!--collapse -->

            </div>
        </nav>
        <!-- End Navbar-content -->

        <!-- Success messages -->

        <!--end failuer messages -->

        <!-- MAIN CONTENT -->
        <div class="main-content">

            <div class="container-fluid">

                <!-- INPUTS -->
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Employee Allowance</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">



                            <div class="col-md-3 col-lg-6">
                                <div class="form-group">
                                    {!! Form::label('employee','Employee Name:') !!}
                                    <select class="employee form-control set_select2 js-example-disabled-results FILTER "  name="employee" id="employee">
                                        <option value=""></option>
                                        @foreach($employees as $employee)
                                            @if(isset($selected_employee_allowence))
                                                @if($employee->employee_id == $showen_emp_id)
                                                    <option selected user_type ="{{$employee->user_type}}"value="{{$employee->employee_id}}">{{$employee->employee_name.' '.' '.$employee->middle_name.' '.$employee->last_name}}
                                                    </option>
                                                @else
                                                    <option user_type ="{{$employee->user_type}}"value="{{$employee->employee_id}}">{{$employee->employee_name.' '.' '.$employee->middle_name.' '.$employee->last_name}}
                                                    </option>
                                                @endif
                                            @else
                                                <option user_type ="{{$employee->user_type}}"value="{{$employee->employee_id}}">{{$employee->employee_name.' '.' '.$employee->middle_name.' '.$employee->last_name}}
                                                </option>
                                            @endif

                                        @endforeach
                                    </select>

                                </div>

                            </div>

                        </div>



                        <br>



                        <div class="row">

                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    {!! Form::label('academic_year','Academic Year:') !!}
                                    <select class="academic_year1 form-control set_select2 js-example-disabled-results FILTER"  name="academic_year" id="academic_year">
                                    <option value=""></option>
                                    @foreach($academic_year_table as $acadimic)

                                            @if(isset($selected_employee_allowence))
                                                @if($acadimic->acadimic_id == $showen_year_id)
                                                    <option selected from ="{{$acadimic->start}}" to ="{{$acadimic->end}}" value="{{$acadimic->acadimic_id}}">{{$acadimic->year_name}}
                                                    </option>
                                                @else
                                                    <option from ="{{$acadimic->start}}" to ="{{$acadimic->end}}" value="{{$acadimic->acadimic_id}}">{{$acadimic->year_name}}
                                                    </option>
                                                @endif
                                            @else
                                                <option from ="{{$acadimic->start}}" to ="{{$acadimic->end}}" value="{{$acadimic->acadimic_id}}">{{$acadimic->year_name}}
                                                </option>
                                            @endif

                                    @endforeach
                                    </select>

                                </div>

                            </div>


                        </div>
                        <div class="row">

                            <input hidden name="employee_allowances" id="employee_allowances" value="{{$employee_allowances}}}">
                        </div>


                        <br>


                        <div class="row">

                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#Sub_SalaryScale">Allowance</a></li>


                            </ul>

                            <div class="tab-content">

                                <div id="Sub_SalaryScale" class="tab-pane fade in active">
                                    <br>

                             <table class="table table-bordred table-striped table-bordered table-striped table-hover table-condensed table-responsive "
                                    id="allow_table">
                                    <thead>
                                    <th>Allowance</th>
                                    <th>From</th>
                                    <th>To</th>
                                    <th>Value</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                    </thead>
                                    <tbody>

                                    @if(isset($selected_employee_allowence))
                                        @foreach($selected_employee_allowence as $data)
                                    <tr class="" id="{{$data->id}}">
                                        <td td_type="search" class="allow_name_v" current_value="{{$data->allow_id}}">{{$data->allow_name}}</td>
                                        <td td_type="date" class="from_date_v" current_value="{{$data->from_date}}">{{$data->from_date}}</td>
                                        <td td_type="date" class="to_date_v" current_value="{{$data->to_date}}">{{$data->to_date}}</td>
                                        <td td_type="input" class="value_v" current_value="{{$data->value}}">{{$data->value}}</td>
                                        <td>
                                            <button type="button" class="set_all_disabled btn btn-primary btn-xs edit_row">Edit</button>
                                        </td>
                                        <td>
                                            <button type="button" class="set_all_disabled btn btn-danger btn-xs delete_row">
                                                <span class="lnr lnr-trash"></span>
                                            </button>
                                        </td>
                                    </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                    </table>
                                    <div class="row">
                                    <div class="col-md-4 col-lg-4 ">
                                    <button type="button"  class="btn btn-primary add_row set_all_disabled"
                                    id="add_row" disabled>Add Row
                                    </button>
                                    </div>
                                    </div>
                                    </form>
                                    </div>

                                </div>

                            </div>
                        </div>

                    </div>
                    <br>
                </div>
            </div>

        </div>
        <!-- END INPUTS -->


    </div>

@endsection


@section('custom_footer')
    <script src="{{asset('js/item_name_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/tax_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/select2.min.js')}}"></script>

    {{-------------------  active side bar & nav bar   ---------------------------}}
    <script>
        $('.side_sheets').addClass('hide');
        $('#HR').removeClass('hide');
        $("#menu li").removeClass('active');
        $('#hr_nav_bar').addClass('active');
        $('#pages ul li a').removeClass('active');
        $('#employees_allowance').addClass('active');
    </script>

    {{---- dublicate dep ---}}
    <script>
        $(document).on('click', '#dublicate_dep', function () {

            $('#set_po_id').attr('current_po_id', "");

            $('.set_all_disabled').prop('disabled', false);

            $('#creat_dep').addClass('hide');
            $('#more').addClass('hide');

            $('#edit_dep').prop('disabled', false);
            $('#edit_dep').text("Save");
            $('#edit_dep').attr('id', 'save_dep');
            $('#save_dep').prop('disabled', false);
        });
    </script>
    <!--End add row to table -->


    {{----------------------  delete department ------------------------------------}}

    <script>
        $(document).on('click', '#delete_dep', function () {
            var cur_po_id = $('#set_po_id').attr('current_po_id');

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/delete_salary_scale/',
                dataType: 'json',
                type: 'get',
                data: {
                    cur_po_id: cur_po_id,
                },

                success: function (response) {

                    window.location.href = '/salary_scale';
                }

            });

        });
    </script>


    {{-----------   on leave page -------------------}}
    <script type='text/javascript'>

        $(window).bind('beforeunload', function(){
            if($('#save_dep').length){
                return 'Are you sure you want to leave?';
            }

        });


    </script>




    <!--select salary scale -->

    <script>
        $(".select_2_enable").select2({
            width: 'resolve'

        });

        $(".myselect3").select2({
            width: 'resolve',


        });

    </script>



    <!--End add row to table -->

    {{------------  add new row  ------------------------}}
    <script>
        $(document).on('click', '#add_row', function () {
            $(this).attr('disabled', true);
            $('#save_po').prop('disabled', true);
            $('.edit_row').prop('disabled', true);
            $('.delete_row').prop('disabled', true);
            var table = $(this).closest('.row').siblings('.table').attr('id');


                    var row = '<tr class= "added_now" >' +
                    '<td td_type="search">' +
                    '<select class="allow_name form-control js-example-disabled-results">\n'
                    + '<option value="">' + '' + '</option>'
                    @foreach($allows as $id=>$role)
                    + '<option allow_value ="" value="{{$id}}">' + '{{$role}}'
                    + '</option>'
                    @endforeach
                    + '</select></td>'

                    + '<td td_type="date">'
                    + '<div class="input-group ">'
                    + '<input type="text" id="from_date" class="form-control c_date from_date" name="from_date">'
                    + '<div class="input-group-addon">'
                    + '<i class="fa fa-calendar">'
                    + '</i>'
                    + '</div>'
                    + '</div>'
                    + '</td>'

                    + '<td td_type="date">'
                    + '<div class="input-group ">'
                    + '<input type="text" id="to_date" class="form-control c_date to_date" name="to_date">'
                    + '<div class="input-group-addon">'
                    + '<i class="fa fa-calendar">'
                    + '</i>'
                    + '</div>'
                    + '</div>'
                    + '</td>'

                    + '<td td_type="input">'
                    + '<input class="value row_data pull-left form-control required " value="10" min="1"  onkeypress="return (event.charCode == 8 || event.charCode == 1) ? null : event.charCode >= 48 && event.charCode <= 57 ||  event.charCode == 46" data-role="input" type="number"  placeholder="0"/></td>'
                    + '<td>'
                    + '<button type="button" class="set_all_disabled btn btn-primary btn-xs Save_row">' + 'Save' + '</button>'
                    + '</td>'
                    + '<td>'
                    + '<button type="button" class="set_all_disabled btn btn-danger btn-xs delete_row"  >' + '<span class="lnr lnr-trash"></span>' + '</button>'
                    + '</td>'

                    + '</tr>';
                    $("#allow_table").prepend(row);
                    $(".c_date").datepicker({format: 'yyyy-mm-dd',autoclose: true, minView: 2});
                    $(".js-example-disabled-results").select2();

        });
    </script>

    {{--------   save  row  --------------}}
    <script>

        $(document).on("click", ".Save_row", function () {




            var empty = false;
            var call_save = false;
            var input = $(this).parents("tr").find('input[type="text"],input[type="number"]');
            var selectors = $(this).parents("tr").find('select');

            input.each(function () {
                if (!$(this).val() || $(this).val() == "") {
                    $(this).addClass("error");
                    empty = true;
                } else {
                    $(this).removeClass("error");
                }
            });

            selectors.each(function () {
                if (!$(this).val() || $(this).val() == "" || $(this).val() < 0  ) {
                    $(this).addClass("error");
                    empty = true;
                } else {
                    $(this).removeClass("error");
                }
            });



            $(this).parents("tr").find(".error").first().focus();

                var acadimic_from2 = $('#academic_year').find('option:selected').attr('from');
                var acadimic_to2 = $('#academic_year').find('option:selected').attr('to');
                var from_date = $(this).parents("tr").find('.from_date').val();
                var to_date = $(this).parents("tr").find('.to_date').val();
                var value = $(this).parents("tr").find('.value ').val();



                if (from_date < to_date && from_date > acadimic_from2&& from_date < acadimic_to2 && to_date < acadimic_to2&& to_date > acadimic_from2)
                {

                if (!empty) {

                    $(this).prop('disabled', true);
                    save_table_effect($(this));
                    $('.errorMessage2').hide();
                    $('.errorMessage1').hide();
                    $('.errorMessage3').hide();
                }
                }else {

                    if(from_date > to_date){


                        $( ".from_date" ).addClass( "error" );
                        $( ".to_date" ).addClass( "error" );

                    }
                    else if(from_date<acadimic_from2){

                        $( ".from_date" ).addClass( "error" );


                    }
                    else if(to_date > acadimic_to2){
                        $( ".to_date" ).addClass( "error" );

                    }
                    else if (value != null || value == ""){

                        $( ".value" ).addClass( "error" );

                    }

                }

        });

        function save_table_effect(thiss) {

            $('.disable_on_save').prop('disabled', true);

            var row = [];

            row.push({
                "academic_year_id": $('#academic_year').val(),
                "allow_id": thiss.parents("tr").find('.allow_name').val(),
                "from_date": thiss.parents("tr").find('.from_date').val(),
                "to_date": thiss.parents("tr").find('.to_date').val(),
                "value": thiss.parents("tr").find('.value').val(),
                "employee_id": $('#employee').val(),
            })
            var employee_allowances = $('#employee_allowances').val();
            var academic_year_id = $('#academic_year').val();
            var id = thiss.parents("tr").attr('id');
            var allow_id = thiss.parents("tr").find('.allow_name').val();
            var from_date = thiss.parents("tr").find('.from_date').val();
            var to_date = thiss.parents("tr").find('.to_date').val();
            var value = thiss.parents("tr").find('.value').val();
            var employee_id = $('#employee').val();




            // });

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/employees_allowance',
                dataType: 'json',
                type: 'POST',
                data: {
                    id: id,
                    employee_allowances: employee_allowances,
                    from_date: from_date,
                    academic_year_id: academic_year_id,
                    allow_id: allow_id,
                    employee_id: employee_id,
                    to_date: to_date,
                    value: value,
                },
                success: function (response) {
                    if (response['error']) {
                        $('.errorMessage').addClass('hide');
                        $.each(response['error'], function (key, val) {


                            $("#" + key + '_error').removeClass('hide').html(val[0]);

                        })
                        $('#save_details').removeAttr('disabled');
                        $('.disable_on_save').prop('disabled', false);
                    }
                    else {
                        var table = thiss.parents('table').attr('id');

                        var academic_year = $('#academic_year').val();
                        var allow_name = thiss.parents("tr").find('.allow_name').val();
                        var value = thiss.parents("tr").find('.value').val();
                        var from_date = thiss.parents("tr").find('.from_date').val();
                        var to_date = thiss.parents("tr").find('.to_date').val();




                        /*--------------  all select ------------*/
                        var select_array = ['allow_name_v'];
                        var r = 0;
                        var all_dowp_down = thiss.parents("tr").find('select');
                        all_dowp_down.each(function () {
                            var value_select = $(this).val();
                            var text_select = $(this).find('option:selected').text();
                            $(this).parent("td").addClass(select_array[r]).attr('current_value', value_select).html(text_select);
                            r++;

                        });
                        /*--------- all input  ------------------*/
                        var input_array = ['from_date_v', 'to_date_v', 'value_v'];
                        var all_input = thiss.parents("tr").find('input[type="text"],[type="number"]');

                        var i = 0;
                        all_input.each(function () {

                            var value_select = $(this).val();

                            $(this).parents("td").addClass(input_array[i]).attr('current_value', value_select).html(value_select);

                            i++;
                        });
                        /*--------------------------*/

                        if (thiss.parents("tr").hasClass('added_now')) {
                            thiss.parents("tr").removeClass('added_now');

                        }
                        if (thiss.parents("tr").hasClass('edited_now')) {
                            thiss.parents("tr").removeClass('edited_now');

                        }
                        thiss.parents("tr").attr('id', response['employee_allowance_id']);
                        $('#save_po').prop('disabled', false);
                        $('.add_row').prop('disabled', false);
                        $('.delete_row').prop('disabled', false);
                        $('.edit_row').prop('disabled', false);
                        thiss.text('Edit');
                        thiss.removeClass('Save_row');
                        thiss.addClass('edit_row');
                        thiss.prop('disabled', false);
                        $(".errorTableMessage").hide();

                    }
                }
            });
        }

    </script>

    {{----------------------  edit row --------------------}}
    <script>
        $(document).on("click", ".edit_row", function () {


            $('#priv').attr('disabled', true);
            $('#save_po').prop('disabled', true);
            $('.add_row').prop('disabled', true);
            $('.delete_row').prop('disabled', true);

            var table = $(this).parents('table').attr('id');



                    if (!$('#allow_table tr').hasClass('added_now')) {

                    var input_class_array = ['value'];
                    var s = 0;
                    var date_i = 0;

                    $('#add_row').prop('disabled', true);
                    $(this).parents("tr").addClass('edited_now');

                    $(this).parents("tr").find("td:not(:last-child)").each(function () {
                    if ($(this).attr('td_type') == 'input') {

                    $(this).html('<input class="value row_data pull-left form-control required "  value="' + $(this).text() + '" onkeypress="return (event.charCode == 8 || event.charCode == 1) ? null : event.charCode >= 48 && event.charCode <= 57 ||  event.charCode == 46" data-role="input" type="number"  placeholder="0" value="' + $(this).text() + '"/>');

                    }

                    else if ($(this).attr('td_type') == 'search') {

                    if ($(this).hasClass('allow_name_v')) {
                    $(this).html('<select class="allow_name form-control js-example-disabled-results">\n'
                    + '<option value="">' + '' + '</option>'
                    @foreach($allows as $id=>$role)
                    + '<option allow_value= "" value="{{$id}}">' + '{{$role}}'
                    + '</option>'
                    @endforeach
                    + '</select>');

                    var selected_v = $(this).attr('current_value');
                    $(this).find(".allow_name").val(selected_v);
                    $(".allow_name").select2();
                    $(".allow_name").siblings('.select2').css('width', '71px');
                    }

                    }
                    else if ($(this).attr('td_type') == 'date') {
                    var input_date_class = ['from_date', 'to_date'];


                    if ($(this).attr('td_type') == 'date') {
                    $(this).html('<input class="' + input_date_class[date_i] + ' row_data form-control c_date" data-role="input" type="text" id="date5" value="' + $(this).text() + '"/>');
                    date_i++;


                    $('.c_date').datepicker({format: 'yyyy-mm-dd',autoclose: true, minView: 2});
                    }

                    }

                    });

                    }

            $("#add_row").attr("disabled", "disabled");
            $(this).text('Save');
            $(this).removeClass('edit_row');
            $(this).addClass('Save_row');


        });

    </script>
    <!--delete row in table -->
    <script>

        $(document).on('click', '.delete_row', function () {

            $('#save_po').prop('disabled', true);

            $('.add_row').prop('disabled', true);
            $('.edit_row').prop('disabled', true);

            $(this).hide(function () {
                $(this).closest('td').append('<button class="btn btn-danger set_all_disabled" type="button" id="yes" style="display: none;">yes</button>' +
                    '<button class="btn btn-default set_all_disabled" type="button" id="no" style="display: none;">no</button>');
                $(this).closest('td').find("#yes").show('slow').delay(1000);
                $(this).closest('td').find("#no").show('slow').delay(1000);
            });
        });

        $(document).on('click', '#yes', function () {
            /////////delete request here//////////////
            var id = $(this).parents("tr").attr('id');
            var deleted = $(this);


            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/employees_allowance/'+id,
                dataType: 'json',
                type: 'DELETE',
                data: {
                    id: id,
                },
                success: function (response) {
                    if (response['error']) {
                        $('.errorMessage').addClass('hide');
                        $.each(response['error'], function (key, val) {
                            notification('glyphicon glyphicon-warning-sign','Warning',val,'danger');
                        });
                        $('#save_details').removeAttr('disabled');
                        $('.disable_on_save').prop('disabled', false);

                    }
                    else if (response['Not Valid']){
                        notification('glyphicon glyphicon-warning-sign','Warning','This Row Had Been Used in Salary Calculation','danger');
                        $('#save_details').removeAttr('disabled');
                        $('.disable_on_save').prop('disabled', false);
                        // deleted.closest('td').find(".delete_row").show('slow').delay(1000);
                        deleted.closest('td').find("#yes").hide('slow').delay(1000).remove();
                        deleted.closest('td').find("#no").hide('slow').delay(1000).remove();
                    }
                    else {
                        notification('glyphicon glyphicon-success-sign','Success','Your Row Has Been Deleted','success');
                        deleted.parents('tr').remove();
                        $(".add_row").removeAttr("disabled");
                        $('#save_po').prop('disabled', false);
                        $('.add_row').prop('disabled', false);
                        $('.edit_row').prop('disabled', false);
                    }
                }
            });
        });

        $(document).on('click', '#no', function () {
            $(this).closest('td').find(".delete_row").show('slow').delay(1000);
            $(this).closest('td').find("#yes").hide('slow').delay(1000).remove();
            $(this).closest('td').find("#no").hide('slow').delay(1000).remove();
            $('#save_po').prop('disabled', false);
            $('.add_row').prop('disabled', false);
            $('.edit_row').prop('disabled', false);
        });
    </script>





    <!------------print--------------->
    <script>
        $(document).on('click', '#print', function () {

            //  window.location.href = 'get_po2/276'
            var employee = $('#employee').val();
            var academic_year = $('#academic_year').val();
            window.open('/get_employeeAllow_rpt/'+employee+'/'+academic_year, '_blank');


        });

    </script>
    <script>
        $(".myselect1").select2({
            width: 'resolve'

        });

        $(".myselect2").select2({
            width: 'resolve',


        });
        $(".myselect3").select2({
            width: 'resolve',


        });
        $(".myselect4").select2({
            width: 'resolve',


        });

    </script>


    <script>

        $(document).on('change', '.allow_name', function () {
            var allow = $(this);

            var id = allow.val();
            var type = $('#Foreigner').attr('user_type');
            if (type == "foreign"){ var currency = 1}
            else{var currency = 2}

            $.ajax({
                url: '/get_allow_value/',
                type: 'get',
                dataType: 'json',
                data: {id: id, currency: currency},
                success: function (data) {
                    allow.parents('tr').find('.value').attr('value', data);


                }
            })
        });

    </script>

    {{------------------  acadimic year dropdown ----------------------}}
    <script>
        $(document).on('change','.FILTER',function () {

            var acadimic_year = $('#academic_year').val();
            var employee = $('#employee').val();
            $('.FILTER').attr('disabled',true);



            if(acadimic_year != "" &&   employee != "" && acadimic_year != null && employee != null)
            {
                $("#add_row").prop('disabled',true);
                $("#save_details").prop('disabled',true);
                /*var allow_id = $('#set_po_id').attr('current_po_id');*/
                $.ajax({
                    url: '/employee_allowences_get_table/',
                    dataType : 'json',
                    type: 'get',
                    data: {
                        Acadimic_year:acadimic_year,
                        employee:employee,
                    },
                    success:function(response) {

                        if(response['error'])
                        {
                            $('.errorMessage').addClass('hide');
                            $.each(response['error'], function (key, val)
                            {
                                notification('glyphicon glyphicon-warning-sign',val,'danger');
                                $("#"+key+'_error').removeClass('hide').html(val[0]);

                            });
                            $('#save_details').removeAttr('disabled');
                            $("#add_row").removeAttr('disabled');
                            $('.FILTER').attr('disabled',false);
                        }
                        else{
                            $("#allow_table tbody").empty();

                            for(var i =0;i<response['employee_allowances'].length;i++)
                            {

                                var row = '<tr class="" id="'+response['employee_allowances'][i]['id']+'">'+
                                    '<td td_type="search" class="allow_name_v" current_value="'+response['employee_allowances'][i]['allow_id']+'">'+response['employee_allowances'][i]['allow_name']+'</td>'+
                                    '<td td_type="date" class="from_date_v" current_value="'+response['employee_allowances'][i]['from_date']+'">'+response['employee_allowances'][i]['from_date']+'</td>' +
                                    '<td td_type="date" class="to_date_v" current_value="'+response['employee_allowances'][i]['to_date']+'">'+response['employee_allowances'][i]['to_date']+'</td>' +
                                    '<td td_type="input" class="value_v" current_value="'+response['employee_allowances'][i]['value']+'">'+response['employee_allowances'][i]['value']+'</td>' +
                                    '<td><button class="set_all_disabled btn btn-primary btn-xs edit_row">Edit</button></td>' +
                                    '<td><button class="set_all_disabled btn btn-danger btn-xs delete_row">' +
                                    '<span class="lnr lnr-trash"></span>' +
                                    '</button>' +
                                    '</td>' +
                                    '</tr>';

                                $("#allow_table").append(row);
                            }

                            $('#save_details').removeAttr('disabled');
                            $("#add_row").removeAttr('disabled');
                            $('.FILTER').attr('disabled',false);
                            console.log($("#allow_table tbody  tr").length);
                            if($("#allow_table tbody tr").length > 0)
                            {
                             $('.printo').removeClass('hide');
                            }else{ $('.printo').hide();}
                        }
                    }
                });

            }
            else{
                notification('glyphicon glyphicon-warning-sign','Warning','You Should Select Two FILTERS ');
                $('.printo').addClass('hide');
                $("#allow_table tbody").empty();
                $("#add_row").prop('disabled',true);
                $('.FILTER').attr('disabled',false);

            }

        });
    </script>


    <script>
        $(document).ready(function () {
            $('.set_select2').select2();
        });
        console.log($("#allow_table tbody tr").length );
        if($("#allow_table tbody tr").length > 0)
        {
            $('.printo').removeClass('hide');
        }
    </script>


@endsection