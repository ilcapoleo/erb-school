@extends('layouts.main_app')

@section('main_content')
    <link href="{{asset('css/fastselect.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/select2.min.css')}}" rel="stylesheet" />

    <style>
        input.col-md-4.text-center {

            border:0;
            background-color: #fff;
        }



        .collapse > .row>.col-md-4>.btn, .collapse > .row>.col-md-4>.dropdown>.btn {
            margin-top: 0;

        }



        .third-nav{
            background-color:#eeeeee;
        }





        .myselect2{
            width:171px;
        }

        span.select2.select2-container.select2-container--default{
            width: 100%!important;
        }
        tbody th, tbody td {
            text-align: center;
        }
    </style>
    <div class="main">
        <!-- Navbar-content -->
        <nav class="navbar navbar-default navbar2">

            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                    <!--breadcrumbs-->
                    <div class="row">

                        <div class="col-md-10 col-lg-10">

                        <ol class="breadcrumb breadcrumb_nav">
                            <li><a href="#">Scale of Fees</a></li>
                            <li class="active"></li>
                        </ol>
                        </div>


                        <div class="col-md-2 col-lg-2">
                            <div class="col-md-6">
                                <a href="#">
                                    <span class="glyphicon glyphicon-arrow-left"></span>
                                </a>
                            </div>

                            <div class="col-md-6">
                                <a href="#">
                                    <span class="glyphicon glyphicon-arrow-right">
                                </span>
                                </a>
                            </div>





                        </div>


                    </div>


                    <!--End breadcrumbs-->
                    <div class="row">
                        <div class="col-lg-4  col-md-4 col-sm-6 col-xs-6 buttons">
                            @if(isset($journal))
                                <button id="edit_po" type="button" class="btn btn-danger ">Edit</button>
                                <button type="button" class="btn" id="creat_dep">Create</button>
                            @else
                                <button id="save_po" type="button" class="btn btn-danger ">Save</button>
                                {{--<button id="edit_user" type="button" class="btn btn-danger " >Edit</button>--}}
                                <button type="button" class="hide btn" id="creat_dep">Create</button>
                            @endif
                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-4 ">
                            @if(isset($journal))
                                <div class="dropdown">
                                    <button class=" btn btn-primary  dropdown-toggle" type="button"
                                            data-toggle="dropdown" id="more" hidden>More
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li class="  " data-toggle="modal" data-target="#exampleModal"><a href="#" id="delete_btn">Delete</a></li>
                                        <li><a href="#">Export</a></li>
                                        <li><a href="#" id="dublicate_dep">Duplicate</a></li>
                                        <li><a href="#" id="print">Print</a></li>

                                    </ul>
                                </div>
                            @else
                                <div class="dropdown">
                                    <button class="hide btn btn-primary  dropdown-toggle" type="button"
                                            data-toggle="dropdown" id="more" hidden>More
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li class="  " data-toggle="modal" data-target="#exampleModal"><a href="#" id="delete_btn">Delete</a></li>
                                        <li><a href="#">Export</a></li>
                                        <li><a href="#" id="dublicate_dep">Duplicate</a></li>
                                        <li><a href="#" id="print">Print</a></li>

                                    </ul>
                                </div>
                            @endif
                        </div>
                    </div>

                </div><!--collapse -->

                {{-----------------  modale ------------------}}
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Purchase Order</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                Are You Sure to Delete this PO ?!!
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-danger fa fa-trash" id="delete_po" >Delete</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </nav>
        <!-- End Navbar-content -->
        <!-- MAIN CONTENT -->
        <div class="main-content">

            <div class="container-fluid">

                <!-- INPUTS -->
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Scale of Fees</h3>
                    </div>
                    <div class="panel-body">

                        @if(isset($journal))
                            <div id="set_po_id" current_po_id="{{$journal->journal_id}}" current_po_status="" closed_or_not=""
                                 current_id="{{Auth::user()->id}}"></div>
                        @else
                            <div id="set_po_id" current_po_id="" current_po_status="draft_po" closed_or_not="0"
                                 current_id="{{Auth::user()->id}}"></div>
                        @endif

                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="account_name" class="col-md-6 col-lg-6 control-label">Academic Year </label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <select id="acadimic_year" name="acadimic_year"
                                                    class="validate_tab required get_all_selectors set_all_disabled select_2_enable form-control department_error err_input"
                                                    @if(isset($class)) disabled @endif >
                                                <option value=""></option>
                                                @foreach($acadimic_year as $id=>$role)
                                                    @if(isset($class))
                                                        @if($class->Costid == $id)
                                                            <option selected value="{{$id}}">{{$role}}</option>
                                                        @else
                                                            <option value="{{$id}}">{{$role}}</option>
                                                        @endif
                                                    @else
                                                        <option value="{{$id}}">{{$role}}</option>
                                                    @endif
                                                @endforeach
                                            </select>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="fees_type" class="col-md-6 col-lg-6 control-label">Fees Name </label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">

                                            <select id="fees_type" name="fees_type"
                                                    class=" validate_tab required get_all_selectors set_all_disabled select_2_enable form-control department_error err_input"
                                                    @if(isset($class)) disabled @endif >
                                                <option value=""></option>
                                                @foreach($fees_name as $single_name)
                                                    @if(isset($class))
                                                        @if($class->Costid == $id)
                                                            <option journal_id="{{$single_name->journal_id}}" selected value="{{$single_name->fees_id}}">{{$single_name->Fees_Name}}</option>
                                                        @else
                                                            <option journal_id="{{$single_name->journal_id}}"  value="{{$single_name->fees_id}}">{{$single_name->Fees_Name}}</option>
                                                        @endif
                                                    @else
                                                        <option journal_id="{{$single_name->journal_id}}"  value="{{$single_name->fees_id}}">{{$single_name->Fees_Name}}</option>
                                                    @endif
                                                @endforeach
                                            </select>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>





                        <br>

                        <div class="row">

                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#fees_plan">Scale of Fees</a></li>


                            </ul>
                            <br>

                            <div class="tab-content">

                                <div id="fees_plan" class="tab-pane fade in active">

                                    <div class="row" >
                                        <table  class="table table-bordred table-striped table-bordered table-striped table-hover table-condensed table-responsive " id="op_table" >
                                            <thead>
                                            <th>Class</th>
                                            <th>Currency</th>
                                            <th>Value</th>
                                            <th>No of Deposit</th>
                                            <th>Nationality Type</th>
                                            <th>Document Name</th>


                                            <th>Edit</th>
                                            <th>Delete</th>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                        <div class="row">
                                            <div class="col-md-4 col-lg-4 ">
                                                <button type="button" class="set_all_disabled btn btn-primary" id="add_row" @if(isset($class)) disabled  @endif disabled >Add Item</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>





                            </div>
                        </div>




                    </div>
                    <br>
                </div>
            </div>

        </div>
        <!-- END INPUTS -->


    </div>

@endsection


@section('custom_footer')
    <script src="{{asset('js/item_name_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/tax_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/select2.min.js')}}"></script>




    <!-----------------active link in nav and active link in sidebar ---->
    <script>
        $('.side_sheets').addClass('hide');
        $('#admission').removeClass('hide');

        $("#menu li").removeClass('active');
        $('#admission_nav_bar').addClass('active');

        $('#pages ul li a').removeClass('active');
        $('#scale_fees_side_bar').addClass('active');

    </script>


    {{---------------  active add row  on supplier change --------------------}}
    <script>
        $(document).on('change','.validate_tab',function () {

            enable_disable_add();

        });

        function enable_disable_add(){
            $("#op_table tbody").empty();
            var isValid;
            isValid = true;
            $(".validate_tab").each(function() {
                var element = $(this);
                if (element.val() == "") {
                    isValid = false;
                    /*$('.validate_tab').prop('disabled',false);*/
                }

            });

            console.log('isValid'+isValid);

            if(isValid && !$('#op_table tr').hasClass('added_now'))
            {



                $('#add_row').prop('disabled',true);

                var acadimic_year = $('#acadimic_year').val();
                var fee_name = $('#fees_type').val();
                $('.set_all_disabled').prop('disabled',true);

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/sub_fees_table/',
                    dataType : 'json',
                    type: 'get',
                    data: {
                        acadimic_year:acadimic_year,
                        fee_name:fee_name
                    },

                    success:function(response) {

                        if(response['error'])
                        {
                            $('.errorMessage').addClass('hide');
                            $.each(response['error'], function (key, val)
                            {
                                //alert(key + val);

                                console.log(key+'_error');
                                $("#"+key+'_error').removeClass('hide').html(val[0]);
                                //console.log(val[0]);

                            })
                            $('.set_all_disabled').prop('disabled',false);
                        }
                        else{



                            for(var i =0;i<response['fees_detailes'].length;i++)
                            {

                                var row = '<tr class="get_doc_data set_all_disabled" id="'+response['fees_detailes'][i]['enID']+'">' +
                                    '<td td_type="search" data-select2-id="34" class="classes_v set_all_disabled" current_value="'+response['fees_detailes'][i]['class_id']+'">'+response['fees_detailes'][i]['ClassName']+'</td>' +
                                    '<td td_type="search" data-select2-id="50" class="currency_v set_all_disabled" current_value="'+response['fees_detailes'][i]['Currency_id']+'">'+response['fees_detailes'][i]['Currency_name']+'</td>' +
                                    '<td td_type="input" class="fee_value_v set_all_disabled" current_value="'+response['fees_detailes'][i]['value']+'">'+response['fees_detailes'][i]['value']+'</td>' +
                                    '<td td_type="input" class="deposit_v set_all_disabled" current_value="'+response['fees_detailes'][i]['NO_of_deposits']+'">'+response['fees_detailes'][i]['NO_of_deposits']+'</td>' +
                                    '<td td_type="search" data-select2-id="61" class="nationality_v set_all_disabled" current_value="'+response['fees_detailes'][i]['nationalty_type_id']+'">'+response['fees_detailes'][i]['nationalty_type']+'</td>' +
                                    '<td td_type="search" data-select2-id="69" class="document_name_v set_all_disabled" current_value="'+response['fees_detailes'][i]['journal_doc_id']+'">'+response['fees_detailes'][i]['Document_Name']+'</td>' +

                                    '<td><button class="disable_on_save set_all_disabled btn btn-primary btn-xs edit_row">Edit</button></td>' +
                                    '<td><button class=" disable_on_save set_all_disabled btn btn-danger btn-xs delete_row">' +
                                    '<span class="lnr lnr-trash">' +
                                    '</span></button>' +
                                    '</td>' +
                                    '</tr>';
                                $("#op_table").append(row);
                            }


                            console.log(response);
                            $('.set_all_disabled').prop('disabled',false);
                            $('#add_row').prop('disabled',false);
                        }


                    }

                });



            }

            else{
                $('#add_row').prop('disabled',true);
            }

        }

    </script>

    {{---------------  load document name on change --------------------}}

    <script>
        $(document).on('change','.currency',function () {

            var documents = $(this).parents('tr').find('.document_name');
            get_item_type_drop_down(documents)

        })
    </script>

    <script>

        function get_item_type_drop_down($dropdown)
        {

            $dropdown.parents('tr').addClass('get_doc_data');
            //var $dropdown = $("#doc_type");
            $dropdown.prop('disabled',true);

            var journal_id = $("#fees_type option:selected").attr("journal_id");
            var currency_id = $dropdown.parents('tr').find('.currency').val();

            /*$dropdown.find('option').remove();*/

            console.log('journal_id : ' + journal_id + 'currency_id : ' + currency_id);
           $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/get_journal_document/',
                dataType : 'json',
                type: 'get',
                data: {
                    journal_id:journal_id,
                    currency_id:currency_id
                },

                success:function(response) {

                    console.log(response);

                    var selected_val = $dropdown.parents('td').attr('current_value');

                    console.log('selected drop '+selected_val);

                    $dropdown.find('option').remove();
                    $dropdown.append($("<option />").val("").text(""));
                    for(var i =0;i<response.length;i++)
                    {
                        $dropdown.append($("<option />").val(response[i]['Document_id']).text(response[i]['Document_Name']));
                    }

                    $dropdown.val(selected_val);
                    $dropdown.prop('disabled',false);
                    $(".document_name").select2();
                    $(".document_name").siblings('.select2').css('width', '71px');
                    $dropdown.parents('tr').removeClass('get_doc_data');
                }

            });

        }

    </script>


    {{------------  add new row  ------------------------}}
    <script>
        $(document).on('click','#add_row',function () {
            $(this).attr('disabled',true);

            var row = '<tr id="" class= "added_now" >' +
                '<td td_type="search">' +
                '<select class="classes form-control js-example-disabled-results set_all_disabled">\n'
                +'<option value="">'+''+'</option>'
                    @foreach($classes as $id=>$role)
                +'<option value="{{$id}}">'+'{{$role}}'
                    @endforeach
                +'</option>'
                +'</select></td>' +

                '<td td_type="search">' +
                '<select class="currency form-control js-example-disabled-results set_all_disabled">\n'
                +'<option value="">'+''+'</option>'
                    @foreach($currency as $id=>$role)
                +'<option value="{{$id}}">'+'{{$role}}'
                    @endforeach
                +'</option>'
                +'</select></td>' +

                '<td td_type="input"><input class="fee_value row_data pull-left form-control required set_all_disabled" min="1.00" onkeypress="return (event.charCode == 8 || event.charCode == 1) ? null : event.charCode >= 48 && event.charCode <= 57 ||  event.charCode == 46" data-role="input" type="number"  placeholder="0"/></td>'+

                '<td td_type="input"><input class="deposit row_data pull-left form-control required set_all_disabled" min="1.00" onkeypress="return (event.charCode == 8 || event.charCode == 1) ? null : event.charCode >= 48 && event.charCode <= 57 ||  event.charCode == 46" data-role="input" type="number"  placeholder="0"/></td>'+

                '<td td_type="search">' +
                '<select class="nationality form-control js-example-disabled-results set_all_disabled">\n'
                +'<option value="">'+''+'</option>'
                    @foreach($nationality as $id=>$role)
                +'<option value="{{$id}}">'+'{{$role}}'
                    @endforeach
                +'</option>'
                +'</select></td>' +

                '<td td_type="search">' +
                '<select class="document_name form-control js-example-disabled-results set_all_disabled" disabled>\n' +
                '</select></td>'

                +'<td>'+'<button class=" disable_on_save set_all_disabled btn btn-primary btn-xs Save_row">'+'Save'+'</button>'
                +'</td>'
                +'<td>'
                +'<button class=" disable_on_save set_all_disabled btn btn-danger btn-xs delete_row"  >'+'<span class="lnr lnr-trash"></span>'+'</button>'
                +'</td>'

                +'</tr>';
            $("#op_table").prepend(row);
            $(".js-example-disabled-results").select2();
            //get_master_acc_drop_down($('.master_account'));

            //$(".po_values").siblings('.select2').css('width', '71px');

        });

    </script>


    {{--------   save journal row  --------------}}
    <script>
        $(document).on("click", ".Save_row", function(){

            var empty = false;
            var call_save = false;
            var input = $(this).parents("tr").find('input[type="number"]');
            var selectors = $(this).parents("tr").find('select');


            input.each(function(){
                if(!$(this).val()|| $(this).val()==""){
                    $(this).addClass("error");
                    empty = true;
                } else{
                    $(this).removeClass("error");
                }
            });

            selectors.each(function(){
                if(!$(this).val()|| $(this).val()==""){
                    $(this).addClass("error");
                    empty = true;
                } else{
                    $(this).removeClass("error");
                }
            });

            console.log('empty '+empty);

            $(this).parents("tr").find(".error").first().focus();



            if(!empty ) {

    /*                $(this).prop('disabled',true);*/
                    save_table_effect($(this));

            }


        });

        function save_table_effect(thiss) {

            if(!thiss.parents("tr").hasClass('get_doc_data')) {
                //$('.disable_on_save').prop('disabled',true);
                $('.set_all_disabled').prop('disabled', true);

                var academic_year = $('#acadimic_year').val();
                var fee_name = $('#fees_type').val();


                var row_id = thiss.parents("tr").attr('id');
                var row = [];

                var Class = thiss.parents("tr").find('.classes').val();
                var currency = thiss.parents("tr").find('.currency').val();
                var fee_value = thiss.parents("tr").find('.fee_value').val();
                var deposit = thiss.parents("tr").find('.deposit').val();
                var nationality = thiss.parents("tr").find('.nationality').val();
                var document_name = thiss.parents("tr").find('.document_name').val();

                row.push({
                    "Class": Class,
                    "currency": currency,
                    "fee_value": fee_value,
                    "deposit": deposit,
                    "nationality": nationality,
                    "document_name": document_name
                });


                console.log('row  : ' + row[0] + 'Class :' + Class + 'currency : ' + currency + ' fee_value :' + fee_value + 'deposit :' + deposit + 'nationality :' + nationality + 'document_name :' + document_name);

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/add_new_sub_fee/',
                    dataType: 'json',
                    type: 'get',
                    data: {

                        academic_year: academic_year,
                        fee_name: fee_name,
                        table_rows: row,
                        row_id: row_id


                    },
                    success: function (response) {
                        console.log(response);

                        if (response['error']) {
                            $('.err_input').removeClass('red-border');
                            $('.err_input').parent().find('.select2').removeClass('red-border');

                            $('.errorMessage1').show();
                            $.each(response['error'], function (key, val) {
                                //alert(key + val);

                                console.log(key + '_error');
                                $("." + key + '_error').addClass('red-border');
                                $('.' + key + '_error').parent().find('.select2').addClass('red-border');

                                //console.log(val[0]);

                            })
                            // $('.disable_on_save').prop('disabled', false);
                            $('.set_all_disabled').prop('disabled', false);

                        }
                        else {

                            /*--------------  save row effect in table  ------------------*/

                            /*--------------  all select ------------*/
                            var select_array = ['classes_v', 'currency_v', 'nationality_v', 'document_name_v'];
                            var r = 0;
                            var all_dowp_down = thiss.parents("tr").find('select');
                            all_dowp_down.each(function () {
                                var value_select = $(this).val();
                                var text_select = $(this).find('option:selected').text();
                                $(this).parent("td").addClass(select_array[r]).attr('current_value', value_select).html(text_select);
                                r++;

                            });
                            /*--------- all input  ------------------*/
                            var input_array = ['fee_value_v', 'deposit_v'];
                            var all_input = thiss.parents("tr").find('input[type="number"]');

                            var i = 0;
                            all_input.each(function () {

                                var value_select = $(this).val();
                                $(this).parent("td").addClass(input_array[i]).attr('current_value', value_select).html(value_select);

                                i++;
                            });
                            /*--------------------------*/

                            if (thiss.parents("tr").hasClass('added_now')) {
                                thiss.parents("tr").removeClass('added_now');

                            }
                            if (thiss.parents("tr").hasClass('edited_now')) {
                                thiss.parents("tr").removeClass('edited_now');

                            }

                            //|| thiss.siblings().find('.create_edit_v') ){
                            //console.log(thiss.closest('tr').attr('id'))


                            if ($('.added_now').length == 0) {
                                $("#add_row").removeAttr("disabled");
                            }

                            thiss.parents("tr").attr('id', response['just_add_row_id']);
                            thiss.text('Edit');
                            thiss.removeClass('Save_row');
                            thiss.addClass('edit_row');
                            thiss.prop('disabled', false);
                            //$('.disable_on_save').prop('disabled', false);
                            $('.set_all_disabled').prop('disabled', false);
                        }
                    },
                    error: function (response) {
                        alert(' Cant Save This Row !');
//                    $('.disable_on_save').prop('disabled',false);
                        $('.set_all_disabled').prop('disabled', false);
                    }

                });

                console.log('passed')

            }
        }


    </script>

    {{----------------------  edit row --------------------}}
    <script>
        $(document).on("click", ".edit_row", function(){
            if(!$('#op_table tr').hasClass('added_now'))
            {
                var input_class_array = ['fee_value','deposit'];
                var s=0;

                $('#add_row').prop('disabled',true);
                $(this).parents("tr").addClass('edited_now');

                $(this).parents("tr").find("td:not(:last-child)").each(function(){
                    if($(this).attr('td_type') == 'input')
                    {
                        $(this).html('<input class="'+input_class_array[s]+ '  row_data pull-left form-control required set_all_disabled" min="1.00" onkeypress="return (event.charCode == 8 || event.charCode == 1) ? null : event.charCode >= 48 && event.charCode <= 57 ||  event.charCode == 46" data-role="input" type="number"  placeholder="0" value="' + $(this).text() + '"/>');
                        s++;
                    }

                    else if ($(this).attr('td_type') == 'search')
                    {
                        if($(this).hasClass('classes_v'))
                        {
                            $(this).html('<select class="set_all_disabled classes form-control js-example-disabled-results">\n'
                                +'<option value="">'+''+'</option>'
                                    @foreach($classes as $id=>$role)
                                +'<option value="{{$id}}">'+'{{$role}}'
                                    @endforeach
                                +'</option>'
                                +'</select>');

                            var selected_v = $(this).attr('current_value');
                            $(this).find(".classes").val(selected_v);
                            $(".classes").select2();
                            $(".classes").siblings('.select2').css('width', '71px');
                        }

                        else if($(this).hasClass('currency_v'))
                        {
                            $(this).html('<select class="set_all_disabled currency form-control js-example-disabled-results">\n'
                                +'<option value="">'+''+'</option>'
                                    @foreach($currency as $id=>$role)
                                +'<option value="{{$id}}">'+'{{$role}}'
                                    @endforeach
                                +'</option>'
                                +'</select>');

                            var selected_v = $(this).attr('current_value');
                            $(this).find(".currency").val(selected_v);
                            $(".currency").select2();
                            $(".currency").siblings('.select2').css('width', '71px');
                        }

                        else if($(this).hasClass('nationality_v'))
                        {
                            $(this).html('<select class="set_all_disabled nationality form-control js-example-disabled-results">\n'
                                +'<option value="">'+''+'</option>'
                                    @foreach($nationality as $id=>$role)
                                +'<option value="{{$id}}">'+'{{$role}}'
                                    @endforeach
                                +'</option>'
                                +'</select>');

                            var selected_v = $(this).attr('current_value');
                            $(this).find(".nationality").val(selected_v);
                            $(".nationality").select2();
                            $(".nationality").siblings('.select2').css('width', '71px');
                        }

                        else if($(this).hasClass('document_name_v'))
                        {
                            $(this).html('<select class="set_all_disabled document_name form-control js-example-disabled-results" disabled>\n' +
                                '</select>');

                            var document = $(this).find('.document_name');
                            /*var selected_v = $(this).attr('current_value');
                            $(this).find(".document_name").val(selected_v);
                            */
                            var selected_v = $(this).attr('current_value');

                            get_item_type_drop_down(document);

                            $(this).find(".document_name").val(selected_v);
                            //$(this).find(".document_name").remove();

                            $(".document_name").select2();
                            $(".document_name").siblings('.select2').css('width', '71px');
                        }

                    }


                });

                $("#add_row").attr("disabled", "disabled");
                $(this).text('Save');
                $(this).removeClass('edit_row');
                $(this).addClass('Save_row');

            }
        });

    </script>



    <!--delete row in table -->
    <script>

        $(document).on('click', '.delete_row', function () {

            $(this).hide(function(){
                $(this).closest('td').append('<button class="btn btn-danger set_all_disabled" type="button" id="yes" style="display: none;">yes</button>' +
                    '<button class="btn btn-default set_all_disabled" type="button" id="no" style="display: none;">no</button>');
                $(this).closest('td').find("#yes").show('slow').delay(1000);
                $(this).closest('td').find("#no").show('slow').delay(1000);

            });
        });

        $(document).on('click','#yes',function(){

            var deleted_id = $(this).closest('tr').attr('id');
            var tr = $(this).closest('tr');

            console.log('deleted_id : ' + deleted_id);
            if(deleted_id == "" || !deleted_id)
            {
                tr.remove();
            }
            else{
                $('.set_all_disabled').prop('disabled', true);

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/delete_sub_fee/',
                    dataType: 'json',
                    type: 'get',
                    data: {

                        row: deleted_id,
                    },

                    success: function (response) {
                        console.log(response);

                        if (response['error']) {
                            $('.err_input').removeClass('red-border');
                            $('.err_input').parent().find('.select2').removeClass('red-border');

                            $('.errorMessage1').show();
                            $.each(response['error'], function (key, val) {
                                //alert(key + val);

                                console.log(key + '_error');
                                $("." + key + '_error').addClass('red-border');
                                $('.' + key + '_error').parent().find('.select2').addClass('red-border');

                                //console.log(val[0]);

                            })
                            // $('.disable_on_save').prop('disabled', false);
                            $('.set_all_disabled').prop('disabled', false);

                        }
                        else{

                            /*--------------  save row effect in table  ------------------*/

                            /*--------------  all select ------------*/
                            tr.remove();
                            $('.set_all_disabled').prop('disabled', false);
                        }
                    },
                    error: function (response)
                    {
                        alert(' Cant Delete This Row !');
//                    $('.disable_on_save').prop('disabled',false);
                        $('.set_all_disabled').prop('disabled',false);
                    }

                });

            }

            if($('.added_now').length == 0)
            {
                $("#add_row").removeAttr("disabled");

            }
        });

        $(document).on('click','#no',function(){

            console.log('noo ');

            $(this).closest('td').find(".delete_row").show('slow').delay(1000);
            $(this).closest('td').find("#yes").hide('slow').delay(1000).remove();
            $(this).closest('td').find("#no").hide('slow').delay(1000).remove();
            /*
            $(this).closest('td').find("#yes").hide('slow').delay(1000);
            $(this).closest('td').find("#no").hide('slow').delay(1000);*/


        });


    </script>
    {{------------------------------------------}}
    <script>
        $(document).ready(function () {
            $(".select_2_enable").select2();

        });
    </script>



@endsection