@extends('layouts.main_app')

@section('main_content')

    <link href="{{asset('css/fastselect.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/select2.min.css')}}" rel="stylesheet" />
    <style>
        input.col-md-4.text-center {

            border:0;
        }
        input[type=number]{
            width: 80px;
        }
        .collapse > .row>.col-md-4>.btn, .collapse > .row>.col-md-4>.dropdown>.btn {
            margin-top: 0;

        }
        .btn{
            padding: 3px 10px;
            margin-top: 3%;
        }
        .third-nav{
            background-color:#eeeeee;
        }
        /**on focus of breadcrumb **/
        .breadcrumb:focus {
            color: #fff;
            background-color: #00AAFF;
        }
        .breadcrumb:focus:after{
            border-color: transparent transparent transparent #00AAFF;
        }
        .third-nav
        {


            background: #fff;  /* fallback for old browsers */
            background: -webkit-linear-gradient(to top, #eeeeee, #fff);  /* Chrome 10-25, Safari 5.1-6 */
            background: linear-gradient(to top, #eeeeee, #fff); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
            border: 1px #4d4d4d0a solid;
        }
        .navbar-default .navbar-nav>li>a   {
            color:#fff;
        }
        .navbar-default .brand {
            float: left;
            padding: 0px 73px;
        }
        .navbar-btn{
            color: #fff;
        }
        .navbar-default .brand{
            background: #2B333E;
        }
        .third-nav{
            background-color:#eeeeee;
        }
       .required  + .select2-container--default red-border > .selection > .select2-selection--single{
            background-color: lavender;
        }
        .table-condensed>tbody>tr>td, .table-condensed>tbody>tr>th, .table-condensed>tfoot>tr>td, .table-condensed>tfoot>tr>th, .table-condensed>thead>tr>td, .table-condensed>thead>tr>th{
            padding: 4px;
        }
       td:nth-child(2)>.fstElement{
            background-color: lavender;
        }
        .error{
            border: 1px solid red  !important;
            border-left: 1px double red !important;
            border-top: 2px double red !important;
        }


        .red-border{
            border: 1px solid red  !important;
        }

        #yes,#no{
            display:none;
            margin-right: 5px;
        }
    </style>
    <div class="main">


        <!-- Navbar-content -->
        <nav class="navbar navbar-default navbar2">

            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                    <!--breadcrumbs-->
                    <div class="row">

                        <div class="col-md-10 col-lg-10">
                        <ol class="breadcrumb breadcrumb_nav">
                            <li><a href="/show_po">Purchase Order</a></li>
                           <li class="active" id="second">
                               @if(isset($master_po))
                                   @if($master_po->po_seq != 0)
                                       {{$master_po->po_seq}}
                                   @else
                                       Draft
                                   @endif
                               @endif
                           </li>
                        </ol>
                        </div>




                        @if(isset($master_po))
                            <div class="col-md-2 col-lg-2">
                                @can('see-all-po')
                                @if(@\App\po_report::where('po_id','<',$master_po->po_id)->orderBy('po_id','desc')->first()->po_id)
                                    <div class="col-md-6">
                                        <a href="/create_po/{{@\App\po_report::where('po_id','<',$master_po->po_id)->orderBy('po_id','desc')->first()->po_id}}"><span
                                                    class="glyphicon glyphicon-arrow-left"></span>
                                        </a>
                                    </div>
                                @endif
                                @if(@\App\po_report::where('po_id','>',$master_po->po_id)->orderBy('po_id','asc')->first()->po_id)
                                    <div class="col-md-6">
                                        <a href="/create_po/{{@\App\po_report::where('po_id','>',$master_po->po_id)->orderBy('po_id','asc')->first()->po_id}}">
                                    <span class="glyphicon glyphicon-arrow-right">
                                </span>
                                        </a>
                                    </div>
                                @endif
                                    @elsecannot('see-all-po')
                                    <?php $employee_id = \App\Employee::where('employee_user_id','=',auth()->user()->id)->first()->employee_id; ?>
                                    @if( $employee_id == $master_po->creator_user_id)

                                    @if(@\App\po_report::where('po_id','<',$master_po->po_id)->where('creator_user_id',$employee_id)->orderBy('po_id','desc')->first()->po_id)
                                        <div class="col-md-6">
                                            <a href="/create_po/{{@\App\po_report::where('po_id','<',$master_po->po_id)->where('creator_user_id',$employee_id)->orderBy('po_id','desc')->first()->po_id}}"><span
                                                        class="glyphicon glyphicon-arrow-left"></span>
                                            </a>
                                        </div>
                                    @endif
                                    @if(@\App\po_report::where('po_id','>',$master_po->po_id)->where('creator_user_id',$employee_id)->orderBy('po_id','asc')->first()->po_id)
                                        <div class="col-md-6">
                                            <a href="/create_po/{{@\App\po_report::where('po_id','>',$master_po->po_id)->where('creator_user_id',$employee_id)->orderBy('po_id','asc')->first()->po_id}}">
                                    <span class="glyphicon glyphicon-arrow-right">
                                </span>
                                            </a>
                                        </div>
                                    @endif
                                    @endif
                                    @endcan
                            </div>
                        @endif


                    </div>   <!--End breadcrumbs-->
                    <div class="row">
                        <div class="col-md-4 buttons">
                            {{--<button id="edit_user" type="button" class="btn btn-danger " >Edit</button>--}}
                            @if(isset($master_po))
                                @if($master_po->po_canceled == 1)
                                    <button id="edit_po" type="button" class="btn btn-danger " disabled>Edit</button>
                                    <button type="button" class="btn" id="creat_po" >Create</button>
                                {{--@elseif($master_po->status_id == 3)
                                    <button id="edit_po" type="button" class="btn btn-danger " >Edit</button>
                                    <button type="button" class="btn" id="creat_po" >Create</button>--}}
                                @else
                                    {{--<button id="save_po" type="button" class="btn btn-danger " >Save</button>
                                    <button type="button" class="btn hide" id="creat_po" >Create</button>--}}
                                    <button id="edit_po" type="button" class="btn btn-danger " >Edit</button>
                                    <button type="button" class="btn" id="creat_po" >Create</button>
                                @endif
                            @else
                                <button id="save_po" type="button" class="btn btn-danger " >Save</button>
                                <button type="button" class="btn hide" id="creat_po" >Create</button>
                            @endif
                        </div>

                        <div class="col-md-4">
                            @if(isset($master_po))

                                @if($master_po->po_canceled == 1 || $master_po->status_id == 3 || $master_po->status_id == 2)
                                    <div class="dropdown">
                                        <button class="btn btn-primary  dropdown-toggle" type="button" data-toggle="dropdown" id="po_more"  hidden>More
                                            <span class="caret"></span></button>
                                        <ul class="dropdown-menu">

                                            <li class=" hide  dropdown-toggle"><a href="#" >Delete</a></li>
                                            <!-- temporery -->   {{--<li><a href="#">Export</a></li>--}}
                                            <li><a href="#" id="dublicate_po">Duplicate</a></li>
                                            <li><a href="#" id="print">Print</a></li>

                                        </ul>
                                    </div>
                                @elseif($master_po->status_id == 1)
                                    <div class="dropdown">
                                        <button class="btn btn-primary  dropdown-toggle" type="button" data-toggle="dropdown" id="po_more"  hidden>More
                                            <span class="caret"></span></button>
                                        <ul class="dropdown-menu">

                                            <li class="" data-toggle="modal" data-target="#exampleModal"><a href="#">Delete</a></li>
                                            <li><a href="#">Export</a></li>
                                            <li><a href="#" id="dublicate_po">Duplicate</a></li>
                                            <li><a href="#" id="print">Print</a></li>

                                        </ul>
                                    </div>
                                @else
                                    <div class="dropdown">
                                        <button class="hide btn btn-primary  dropdown-toggle" type="button" data-toggle="dropdown" id="po_more"  hidden>More
                                            <span class="caret"></span></button>
                                        <ul class="dropdown-menu">

                                            <li class="" data-toggle="modal" data-target="#exampleModal"><a href="#">Delete</a></li>
                                            <li><a href="#">Export</a></li>
                                            <li><a href="#" id="dublicate_po">Duplicate</a></li>
                                            <li><a href="#" id="print">Print</a></li>

                                        </ul>
                                    </div>
                                @endif
                            @else
                                <div class="dropdown">
                                    <button class="hide btn btn-primary  dropdown-toggle" type="button" data-toggle="dropdown" id="po_more"  hidden>More
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">

                                        <li class=" hide " data-toggle="modal" data-target="#exampleModal"><a href="#" id="delete_btn">Delete</a></li>
                                        <li><a href="#">Export</a></li>
                                        <li><a href="#" id="dublicate_po">Duplicate</a></li>
                                        <li><a href="#" id="print">Print</a></li>

                                    </ul>
                                </div>
                            @endif
                        </div>
                        <!--search-->
                        <!--
                         <div class="col-md-4">
                             <form class="navbar-form navbar-right" >
                                 <div class="input-group form-group">
                                     <input type="text" value="" class="form-control" placeholder="Search ...">
                                 </div>
                             </form>
                         </div>
                         -->

                        <!-- Button trigger modal -->
                        {{--<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">--}}
                            {{--Launch demo modal--}}
                        {{--</button>--}}

                        <!-- Modal -->
                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Purchase Order</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        Are You Sure to Delete this PO ?!!
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-danger fa fa-trash" id="delete_po" >Delete</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div> <!--collapse -->

                </div><!--collapse -->

                <!--buttons row +second breadcrumb-->

                <div class="row third-nav">


                    <div class="col-md-6 col-lg-6">
                        <div class="col-md-3 col-lg-3">
                            <button  type="button" class="set_all_disabled btn" id="send_mail" >Send by Mail</button>
                        </div>

                        <div class="col-md-3 col-lg-3">
                            <button id="sendto_approved_btn" type="button" class="set_all_disabled btn" @if(isset($master_po)) disabled @endif > To Approve</button>
                        </div>

                        @can('aprrove-po')
                        <div class="col-md-3 col-lg-3  ">
                            <button id="approved_btn" type="button" class="set_all_disabled btn approved " @if(isset($master_po)) disabled @endif>Approve</button>
                        </div>
                        @endcan

                        @can('close-po')
                        @if(isset($master_po))

                            <div class="col-md-3 col-lg-3 ">
                                <button id="close_po_btn" type="button" class="set_all_disabled btn " @if($master_po->status_id == 1)disabled @endif @if(isset($master_po)) disabled @endif>Close PO</button>
                            </div>
                        @else
                            <div class="col-md-3 col-lg-3 ">
                                <button id="close_po_btn" type="button" class="set_all_disabled btn hide" >Close PO</button>
                            </div>
                        @endif
                        @endcan

                    </div>
                    <div class="col-md-6 col-lg-6">

                             <span id="current_choosed_status" coosed_status="" class="breadcrumbs ">
                                 @if(isset($master_po))

                                     <span id="draft_po" href="#" class="@if($master_po->status_id == 1) po-active @endif breadcrumb">Draft </span></li>
                                     <span id="to_approve_po" href="#" class="@if($master_po->status_id == 2) po-active @endif breadcrumb">To Approved </span></li>
                                     <span id="approved_po" href="#" class="@if($master_po->status_id == 3) po-active @endif  breadcrumb ">Approved </span></li>

                                     <span id="closed_po" href="#" class="@if($master_po->status_id == 5) po-active @endif breadcrumb" @if($master_po->status_id == 1)disabled @endif>Closed</span></li>


                                     <span id="done_po" class="breadcrumb">Done</span>
                                 @else
                                     <span id="draft_po" href="#" class="breadcrumb">Draft PO</span></li>
                                     <span id="to_approve_po" href="#" class="breadcrumb">To Approved PO</span></li>
                                     <span id="approved_po" href="#" class="breadcrumb ">Approved PO</span></li>
                                     <span id="closed_po" href="#" class="hide breadcrumb">Closed</span></li>
                                     <span id="done_po" class="breadcrumb">Done</span>
                                 @endif
                            </span>


                    </div>


                </div>



            </div>
        </nav>
        <!-- End Navbar-content -->
        <!-- Success messages -->
        <div class="alert alert-success alert-autocloseable-success" style="display:none;position:absolute;" >
            Your PO Saved Successfully!

        </div>
        <!--end Success messages -->

        <!-- failuer messages -->
        <div class="alert alert-danger errorMessage1" style="display:none;position:absolute;" >
            Your should fill all fields!

        </div>
        <!--end failuer messages -->


        <!-- failuer messages -->
        <div class="alert alert-danger errorMessage2" style="display:none;position:absolute;" >
        PLease fill table!

        </div>

        <div  class="alert alert-danger errorMessage3" style=" display: none;">
        </div>

        <!--end failuer messages -->
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Purchase Order</h3>
                    </div>
                    <div class="panel-body">


                        <div class="row">

                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <div class="col-md-6 col-lg-6">
                                        <div class="form-group">

                                            <label class="col-md-6 col-lg-6 control-label">Supplier</label>



                                            <div class="col-md-6 col-lg-6">

                                                <select id="supplier" name="supplier" class="get_valide_for get_all_selectors set_all_disabled select_2_enable form-control @if(!isset($master_po))required @endif err_input supplier_error" @if(isset($master_po)) required disabled @endif >
                                                    <option value=""> </option>
                                                    @foreach($supplier as $id=>$role)
                                                        @if(isset($master_po))
                                                            @if($master_po->VendorID == $id)
                                                                <option selected value="{{$id}}">{{$role}}</option>
                                                            @else
                                                                <option value="{{$id}}">{{$role}}</option>
                                                            @endif
                                                        @else
                                                            <option value="{{$id}}">{{$role}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">

                                        <div class="bootstrap-iso">
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <div class="col-md-12 col-lg-12 col-sm-6 col-xs-12">


                                                        <form method="post">
                                                            <div class="form-group">
                                                                <label class=" control-label col-md-6 col-lg-6 " for="datetime1" >Order Date</label>
                                                                <div class="input-group col-md-6">
                                                                    <input @if(isset($master_po)) value="{{$master_po->order_date}}" @endif type="text" name="order_date" id="datetime1" class="get_valide_for get_all_date_input set_all_disabled form-control @if(!isset($master_po))required @endif err_input order_date_error" name="date" @if(isset($master_po)) disabled  @endif @if(isset($curr_date))value="{{$curr_date}}" @endif>
                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-calendar">
                                                                        </i>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </form>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>



                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div id="supplier_error" class="alert alert-danger errorMessage hide ">
                                            <strong >Wrong Input!</strong>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div id="order_date_error" class="alert alert-danger errorMessage hide ">
                                            <strong >Wrong Date!</strong>
                                        </div>
                                    </div>

                                </div>

                            </div>
                            <br>
                            <div class="row">
                                {{-- set po sitting--}}
                                @if(isset($master_po))
                                    <div id="set_po_id"  current_po_id="{{$master_po->po_id}}" current_po_status="{{$master_po->status_name}}" closed_or_not="{{$master_po->po_canceled}}" current_id="{{Auth::user()->id}}"></div>
                                @else
                                    <div id="set_po_id"  current_po_id="" current_po_status="draft_po" closed_or_not="0" current_id="{{Auth::user()->id}}"></div>
                                @endif
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">

                                            <label class="col-md-6 control-label">Beneficiary User</label>
                                            <div class="col-md-6">

                                                <select name="benefit_user" id="benefit_user" class="get_all_selectors set_all_disabled select_2_enable form-control" @if(isset($master_po)) disabled  @endif>
                                                    <option value=""> </option>
                                                    @foreach($users_info as $id=>$role)
                                                        <?php $all_emp =  @\App\Employee::find($id) ?>
                                                        @if(isset($master_po))
                                                            @if($master_po->employee_id == $id)
                                                                <option selected value="{{$id}}">{{$role.' '.$all_emp->middle_name .' '.$all_emp->last_name.' '.$all_emp->family_name}}</option>
                                                            @else
                                                                <option value="{{$id}}">{{$role.' '.$all_emp->middle_name .' '.$all_emp->last_name.' '.$all_emp->family_name}}</option>
                                                            @endif
                                                        @else
                                                            <option value="{{$id}}">{{$role.' '.$all_emp->middle_name .' '.$all_emp->last_name.' '.$all_emp->family_name}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">

                                        <div class="bootstrap-iso">
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <div class="col-md-12 col-sm-6 col-xs-12">


                                                        <form method="post">
                                                            <div class="form-group">
                                                                <label class=" control-label col-md-6 " for="datetime2" >Expected Date</label>
                                                                <div class="input-group col-md-6">
                                                                    <input @if(isset($master_po)) value="{{$master_po->expected_date}}" @endif type="text" id="datetime2" class="get_valide_for get_all_date_input set_all_disabled form-control @if(!isset($master_po))required @endif err_input expected_date_error" name="expected_date" @if(isset($master_po)) disabled  @endif>
                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-calendar">
                                                                        </i>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </form>


                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div id="Beneficiary_User_error" class="alert alert-danger errorMessage hide ">
                                            <strong >Wrong Input!</strong>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div id="expected_date_error" class="alert alert-danger errorMessage hide ">
                                            <strong >Wrong Date!</strong>
                                        </div>
                                    </div>

                                </div>

                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <div class="col-md-6 col-lg-6">
                                        <div class="form-group">

                                            <label class="col-md-6 col-lg-6 control-label">Beneficiary management
                                            </label>
                                            <div class="col-md-6 col-lg-6">

                                                <select required name="benefit_management" id="benefit_management" class="get_valide_for get_all_selectors set_all_disabled select_2_enable form-control @if(!isset($master_po))required @endif err_input benefit_management_error" @if(isset($master_po)) disabled  @endif>
                                                    <option value=""> </option>
                                                    @foreach($department_info as $id=>$role)
                                                        @if(isset($master_po))
                                                            @if($master_po->dep_id == $id)
                                                                <option selected value="{{$id}}">{{$role}}</option>
                                                            @else
                                                                <option value="{{$id}}">{{$role}}</option>
                                                            @endif
                                                        @else
                                                            <option value="{{$id}}">{{$role}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-6">


                                        <div class="bootstrap-iso">
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <div class="col-md-12 col-lg-12 col-sm-6 col-xs-12">



                                                        <form method="post">
                                                            <div class="form-group">
                                                                <label class=" control-label col-md-6 " for="datetime3"  >Date Approved</label>
                                                                <div class="input-group col-md-6 col-lg-6">
                                                                    <input @if(isset($master_po)) value="{{$master_po->approved_date}}" @endif type="text" id="datetime3" class="get_all_date_input  form-control" name="date_approved" @if(isset($master_po)) disabled @endif  disabled>
                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-calendar">
                                                                        </i>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </form>



                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>

                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div id="benefit_management_error" class="alert alert-danger errorMessage hide ">
                                            <strong >Wrong Input!</strong>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div id="approved_date_error" class="alert alert-danger errorMessage hide ">
                                            <strong >Wrong Date!</strong>
                                        </div>
                                    </div>

                                </div>

                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <div class="col-md-6 col-lg-6">
                                        <div class="form-group">

                                            <label class="col-md-6 col-lg-6 control-label">Currency</label>
                                            <div class="col-md-6 col-lg-6">

                                                <select name="currency" id="currency" class="get_all_selectors  form-control" @if(isset($master_po)) disabled  @endif disabled>
                                                    <option value=""> </option>
                                                    @foreach($currencies as $id=>$role)
                                                        @if(isset($master_po))
                                                            @if($master_po->Currency_id == $id)
                                                                <option selected value="{{$id}}">{{$role}}</option>
                                                            @else
                                                                <option value="{{$id}}">{{$role}}</option>
                                                            @endif
                                                        @else
                                                            <option value="{{$id}}">{{$role}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div id="currency_error" class="alert alert-danger errorMessage hide ">
                                            <strong >Wrong Input!</strong>
                                        </div>
                                    </div>


                                </div>

                            </div>


                            <br>

                            <div class="row">

                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#products">PO Details</a></li>

                                </ul>

                                <div class="tab-content">
                                    <div id="products" class=" table-responsive tab-pane fade in active">

                                        <table  class="table table-bordred table-striped table-bordered table-striped table-hover table-condensed table-responsive" id="op_table" >
                                            <thead>
                                            <th>Item Type</th>
                                            <th >Item Name</th>
                                            <th >Quantity</th>
                                            <th >Unit Price</th>
                                            <th >Include</th>
                                            <th >Tax</th>
                                            <th id="subtotal_head" column_subtotal="0" >Subtotal</th>
                                            <th id="tax_head" column_tax="0" >Total Tax</th>
                                            <th>Edit</th>
                                            <th>Delete</th>
                                            </thead>
                                            <tbody>
                                            @if(isset($details_po))
                                                <?php $i=0 ?>
                                                @foreach($details_po as $details_po_single)
                                                    <tr class="" data-select2-id="34">
                                                        <td td_type="search" data-select2-id="33" class="item_type_v" current_value="{{$details_po_single->pro_type_id}}">{{$details_po_single->pro_type_name}}</td>
                                                        <td td_type="drop_down" required class="item_name_v " get_value="{{$details_po_single->pro_name}}" current_value="{{$details_po_single->pro_id}}">{{$details_po_single->pro_name}}</td>
                                                        <td td_type="input" class="quantity_value_v "   current_value="{{$details_po_single->quantity}}">{{$details_po_single->quantity}}</td>
                                                        <td td_type="input" class="unit_price_v "  current_value="{{$details_po_single->unit_price}}">{{$details_po_single->unit_price}}</td>
                                                        <td td_type="check_box" current_value="{{$details_po_single->include_tax}}" class="include_check_v check_box_v">
                                                            @if($details_po_single->include_tax == 1)
                                                                Yes
                                                            @else
                                                                No
                                                            @endif
                                                        </td>
                                                        @if(isset($tax_po))

                                                            <td td_type="drop_down" class="tax_value_v " get_value="@if($details_po_single->tax_string){{$details_po_single->tax_string}}@endif" current_value="@if($details_po_single->tax_string){{$details_po_single->tax_string}}@endif">@if($details_po_single->tax_string){{$details_po_single->tax_string}}@endif</td>

                                                        @endif


                                                        <td td_type="input" class="sub_total_v" current_value="{{$sub_total_array[$i]}}">{{$sub_total_array[$i]}}</td>

                                                        <td td_type="input" class="total_row_tax_v" current_value="{{$total_tax_array[$i]}}">{{$total_tax_array[$i]}}</td>

                                                        <?php $i++ ?>


                                                        <td><button class="set_all_disabled btn btn-primary btn-xs edit_row" disabled>Edit</button></td>
                                                        <td>
                                                            <button class="set_all_disabled btn btn-danger btn-xs delete_row" disabled>
                                                                <span class="lnr lnr-trash"></span>
                                                            </button>

                                                            {{--<button class="	btn btn-danger" type="button" id="yes">yes</button>
                                                            <button class="btn btn-default"  type="button" id="no">no</button>--}}
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                            </tbody>
                                        </table>

                                        <div class="row">
                                            <div class="col-md-4 ">
                                                <button type="button" class="set_all_disabled btn btn-primary" id="add_row" @if(isset($master_po)) disabled  @endif>Add Row</button>
                                            </div>
                                        </div>


                                        <hr>

                                        <div class="row">

                                            <div class="col-md-3 col-lg-3 ">
                                                <div class="form-group">
                                                    <label for="comment">Add your note :</label>
                                                    <textarea class="set_all_disabled form-control" rows="5" id="po_comment" @if(isset($master_po)) disabled @endif >@if(isset($master_po)) {{$master_po->po_notes}} @endif</textarea>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-lg-3 ">
                                                <label for="comment"></label>

                                                <table  class="table table-bordred table-striped table-bordered table-striped table-hover table-condensed table-responsive" id="tax_details_table" >
                                                    <thead>
                                                    <th>Tax Name</th>
                                                    <th >Tax Value</th>

                                                    </thead>
                                                    <tbody>
                                                    @if(isset($po_tax_details))
                                                        @foreach($po_tax_details as $po_tax_d)
                                                        <tr>
                                                            <th >{{$po_tax_d->tax_name}}</th>
                                                            <td>{{$po_tax_d->TOATL}}</td>

                                                        </tr>
                                                        @endforeach
                                                    @endif
                                                    </tbody>
                                                </table>



                                            </div>



                                            <div class="col-md-6  col-lg-6 col-sm-6">
                                                <div class="col-md-12 col-lg-12 col-sm-12">
                                                    <div class="col-md-4  col-lg-4 col-sm-4">Untaxes Amount:</div>
                                                    @if(isset($details_po))
                                                        <div id="untaxed_all" class="col-md-4 col-lg-4 col-sm-4 " >{{$sub_total_sum}} </div><span class="currency_value col-md-4  col-lg-4">@if(isset($master_po)) {{$master_po->Currency_name}} @endif</span>
                                                    @else
                                                        <div id="untaxed_all" class="col-md-4 col-lg-4  col-sm-4" >0.00  </div><span class="currency_value col-md-4  col-lg-4"></span>
                                                    @endif

                                                </div>
                                                <br>
                                                <div class="col-md-12 col-lg-12 col-sm-12 ">
                                                    <div class="col-md-4  col-lg-4 col-sm-4">Taxes:</div>

                                                    @if(isset($details_po))
                                                        <div id="taxed_sum" class="col-md-4  col-lg-4 col-sm-4" >{{$tax_sum}} </div><span class="currency_value col-md-4  col-lg-4">@if(isset($master_po)) {{$master_po->Currency_name}} @endif</span>
                                                    @else
                                                        <div id="taxed_sum" class="col-md-4  col-lg-4 col-sm-4" >0.00</div><span class="currency_value col-md-4  col-lg-4"></span>
                                                    @endif
                                                </div>

                                                <hr>
                                                <div class="col-md-12  col-lg-12 col-sm-12">
                                                    <div class="col-md-4 col-lg-4 col-sm-4">Total:</div>
                                                    @if(isset($details_po))
                                                        <div  id="total_sum" class="col-md-4 col-lg-4 col-sm-4" >{{$sub_total_sum + $tax_sum}} </div><span class="currency_value col-md-4  col-lg-4">@if(isset($master_po)) {{$master_po->Currency_name}} @endif</span>
                                                    @else
                                                        <div  id="total_sum" class="col-md-4 col-lg-4 col-sm-4" >0.00 </div><span class="currency_value col-md-4  col-lg-4"></span>
                                                    @endif
                                                </div>




                                            </div>

                                        </div>



                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- END MAIN CONTENT -->
            </div>
            <!-- END MAIN -->


        </div>
    </div>


<div id="file_path" path_file ="{{url('/uploads/jsons/'.Auth::user()->id.'.json')}}"></div>

@endsection


@section('custom_footer')

    <script src="{{asset('assets/scripts/bootstrap-datetimepicker.min.js')}}"></script>

    <script src="{{asset('js/item_name_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/tax_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/select2.min.js')}}"></script>


    {{--------------------  active side & nav bar ------------------------}}
    <script>
        $('.side_sheets').addClass('hide');
        $('#Purshase').removeClass('hide');

        $("#menu li").removeClass('active');
        $('#Purchase_nav_bar').addClass('active');

        $('#pages ul li a').removeClass('active');
        $('#create_po_side_bar').addClass('active');

    </script>



    <!--date/time picker-->

    <script>

        $('#datetime1').datetimepicker({format: 'yyyy-mm-dd',autoclose: true, minView: 2});

        $('#datetime2').datetimepicker({format: 'yyyy-mm-dd',autoclose: true, minView: 2});
        $('#datetime3').datetimepicker({format: 'yyyy-mm-dd',autoclose: true, minView: 2});


    </script>
    <!-------------------------add row to table --------------------------------->
    <script>
        $(document).on('click','#add_row',function () {
            var path = $('#file_path').attr('path_file');
            $(this).attr('disabled',true);

            var row = '<tr class= "added_now" >' +
                '<td td_type="search">' +
                '<select class="item_type form-control js-example-disabled-results">\n' +
                '</select></td>' +
                '<td td_type="drop_down" ><input\n' +
                '    type="text"\n' +
                '    multiple\n' +
                '    class="item_name required"\n' +
                '    value=""\n' +
                '    data-initial-value=\'\'\n' +
                '    data-user-option-allowed="true"\n' +
                '    data-url="'+path+'"\n' +
                '    data-load-once="false"\n' +
                '    name="language"' +
                '    disabled="disabled"' +
                '   style="z-index: -1;"/></td>' +
                '<td td_type="input"><input class="quantity_value row_data pull-left form-control required" min="0.00" onkeypress="return (event.charCode == 8 || event.charCode == 1) ? null : event.charCode >= 48 && event.charCode <= 57 ||  event.charCode == 46" data-role="input" type="number"  placeholder="0"/></td>'+
                '<td td_type="input"><input  class="unit_price pull-left form-control required required" min="0.00" onkeypress="return (event.charCode == 8 || event.charCode == 1) ? null : event.charCode >= 48 && event.charCode <= 57 || event.charCode == 46" data-role="input"  type="number"  placeholder="0"/></td>'+
                '<td td_type="check_box"><input  class="include_check "   data-role="input" type="checkbox"/></td>'+
                '<td td_type="drop_down"><input\n' +
                '    type="text"\n' +
                '    multiple\n' +
                '    class="tax_value"\n' +
                '    value=""\n' +
                '    data-initial-value=\'\'\n' +
                '    data-url="/data_tax.json"\n' +
                '    data-load-once="false"\n' +
                '    name="language"/></td>'+
                '<td td_type="input"><input  class="sub_total pull-left form-control"  disabled data-role="input" type="number" value="0" onkeypress="return (event.charCode == 8 || event.charCode == 1) ? null : event.charCode >= 48 && event.charCode <= 57" done_calculation="0"/></td>'+
                '<td td_type="input"><input  class="total_row_tax pull-left form-control"  disabled data-role="input" type="number" onkeypress="return (event.charCode == 8 || event.charCode == 1) ? null : event.charCode >= 48 && event.charCode <= 57" value="0" /></td>'
                +'<td>'+'<button class="set_all_disabled btn btn-primary btn-xs Save_row">'+'Save'+'</button>'
                +'</td>'
                +'<td>'
                +'<button class="set_all_disabled btn btn-danger btn-xs delete_row"  >'+'<span class="lnr lnr-trash"></span>'+'</button>'
                /*+ '<button class="btn btn-danger"  type="button" id="yes">'+'yes'+'</button>' + '<button class="btn btn-default" type="button" id="no">'+'no'+'</button>'*/

                +'</td>'
                +'</tr>';
            $("#op_table").prepend(row);
            $('.item_name').item_name_fastselect();
            $('.item_name').parents('tr').find('.item_name').siblings('.fstControls').children('.fstQueryInput').prop('disabled',true);
            $('.tax_value').tax_fastselect();
            get_item_type_drop_down($('.item_type'));
            $(".js-example-disabled-results").select2();
            $(".js-example-disabled-results").siblings('.select2').css('width', '71px');
            open_on_add_new();
        });


    </script>

    <!--End add row to table -->
    {{---------------------  save row --------------------------------}}
    <script>
        $(document).on("click", ".Save_row", function(){

            var empty = false;
            var input = $(this).parents("tr").find('input[type="text"],input[type="number"]');
            var classess_array = ['item_name_v','quantity_value_v','unit_price_v','tax_value_v','sub_total_v'];
            //item_type_v
            if( ! $(this).parents('tr').hasClass('add_new_product'))
            {


            if($(this).parents("tr").find('.include_check:checked').length>0)
            {

                $(this).parents("tr").find('.include_check').parent("td").attr('current_value',1).attr('yes_or_no','Yes');
                input.each(function(){
                    if(!$(this).val()|| $(this).val()=="" || $(this).val()==0&&$(this).hasClass('quantity_value') || $(this).val()==0&&$(this).hasClass('unit_price')){
                        $(this).parents('td').addClass("error");

                        empty = true;
                    } else{
                        $(this).parents('td').removeClass("error");


                    }
                });
                if(!$(this).parents("tr").find('.item_type').val() || $(this).parents("tr").find('.item_type').val()=="")
                {
                    $(this).parents("tr").find('.item_type').parent('td').addClass("error");
                    empty = true;
                }
                else{
                    $(this).parents("tr").find('.item_type').parent('td').removeClass("error");
                }


            }
            else{

                $(this).parents("tr").find('.include_check').parent("td").attr('current_value',0).attr('yes_or_no','No');
                input.each(function(){
                    if(!$(this).val()&&!$(this).hasClass('tax_value') || $(this).val()==""&&!$(this).hasClass('tax_value') || $(this).val()==0&&$(this).hasClass('quantity_value') || $(this).val()==0&&$(this).hasClass('unit_price')){
                        $(this).parents('td').addClass("error");

                        empty = true;
                    } else{
                        $(this).parents('td').removeClass("error");

                    }
                });

                if(!$(this).parents("tr").find('.item_type').val() || $(this).parents("tr").find('.item_type').val()=="")
                {
                    $(this).parents("tr").find('.item_type').parent('td').addClass("error");
                    empty = true;

                }
                else{
                    $(this).parents("tr").find('.item_type').parent('td').removeClass("error");
                }
            }

            console.log('empty '+empty);


            $(this).parents("tr").find(".error").first().focus();

            var colum_data = [];
            var row_id = $(this).closest('tr').attr('id');


            if(!empty ) {
                $(this).attr('disabled','disabled');
                get_sub_for_row(input,$(this));


                /*------------ check box ----------------------*/



            }
            }

            else{
                console.log('wait for product to create');
            }


        });

        function save_table_effect(input,thiss) {
            console.log('passed')

            var colum_data = [];
            var check_box = thiss.parents("tr").find('input.include_check');
            var check_val = check_box.parent("td").attr('current_value');
            var check_text_val = check_box.parent("td").attr('yes_or_no');
            var classess_array = ['item_name_v','quantity_value_v','unit_price_v','tax_value_v','sub_total_v','total_row_tax_v'];
            check_box.parent("td").addClass('include_check_v').addClass('check_box_v').html(check_text_val);

            /*--------- select ------------------*/
            var select_drop_down = thiss.parents("tr").find('select.item_type');
            var value_select = select_drop_down.val();
            var text_select = thiss.parents("tr").find('select.item_type option:selected').text();

            select_drop_down.parent("td").addClass('item_type_v').attr('current_value', value_select).html(text_select);
            /*--------------------------*/


            if (thiss.parents("tr").hasClass('added_now')) {
                thiss.parents("tr").removeClass('added_now');

            }
            if (thiss.parents("tr").hasClass('edited_now')) {
                thiss.parents("tr").removeClass('edited_now');

            }

            //|| thiss.siblings().find('.create_edit_v') ){
            //console.log(thiss.closest('tr').attr('id'))

            var i = 0;
            input.each(function () {


                $(this).parent("td").addClass(classess_array[i]);
                $(this).parent("td").attr('current_value', $(this).val());
                $(this).parent("td").html($(this).val());
                //console.log($(this).parent().parent("td").attr('get_value'));
                console.log($(this).siblings().find('.create_edit_v').attr('c_e_value'));

                /*if($(this).siblings().find('.create_edit_v').attr('c_e_value'))
                {
                    $(this).siblings().find('.create_edit_v').each(function () {
                    vall = vall +',' + $(this).attr('c_e_value');
                    //window.set_selected($(this).attr('c_e_value'));
                    console.log($(this).attr('c_e_value'));
                });
                }*/
                if ($(this).hasClass('item_name')) {
                    var vall = $(this).siblings('.fstControls').children('.fstChoiceItem').attr('data-text');
                    var current_v = $(this).val();
                }
                else {
                    var vall = $(this).val();
                    var current_v = $(this).val();
                }
                $(this).parent().parent("td").addClass(classess_array[i]);
                $(this).parent().parent("td").attr('get_value', vall);
                $(this).parent().parent("td").attr('current_value', current_v);
                $(this).parent().parent("td").html(vall);

                i++;
                colum_data.push(vall);

            });

            console.log(colum_data);
            if ($('.added_now').length == 0) {
                $("#add_row").removeAttr("disabled");
            }

            thiss.text('Edit');
            thiss.removeClass('Save_row');
            thiss.addClass('edit_row');

            get_totals();

        }


    </script>
    {{---------------------  get price for choosed item  ----------------------------}}
    <script>
        function get_price_foritem(item)
        {
            var item_id = item.siblings('.fstControls').children('.fstChoiceItem').attr('data-value');
            item.parents('tr').find('.unit_price');//.prop('disabled',true);
            var supplier = $('#supplier').val();
            if(item_id == '' || supplier == '')
            {
                item.parents('tr').find('.unit_price').val('0');
            }
            else{

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/get_price_foritem/',
                dataType : 'json',
                type: 'get',
                data: {
                    item_id:item_id,
                    supplier:supplier,
                },

                success:function(response) {

                    console.log(response);
                    if(response[0] != 0 || response[0] > 0)
                    {
                        item.parents('tr').find('.unit_price').val(response[0]);
                    }
                    else{
                        item.parents('tr').find('.unit_price').val('');
                        item.parents('tr').find('.unit_price').attr('placeholder',0);


                    }

                    item.parents('tr').find('.unit_price').prop('disabled',false);

                }

            });
        }

        }
    </script>

    {{-------------------  select only one item --------------------------------}}
    <script>
        $(document).on('change','.item_name',function () {
            var arr =$(this).siblings('.fstControls').children('.fstChoiceItem');
            console.log(arr.length);
            if(arr.length == 1 || arr.length >= 1)
            {
                console.log(arr.length);

                get_price_foritem($(this));

                $(this).siblings('.fstControls').children('input').attr('disabled','disabled');
                $(this).siblings('.fstControls').parent('div').removeClass('fstResultsOpened fstActive')
            }

            else if(arr.length == 0)
            {
                console.log(arr.length);
                $(this).siblings('.fstControls').children('input').removeAttr('disabled');
            }
        });
        function limit_on_edit(target) {
            target.parents("tr").find('.item_name').siblings('.fstControls').children('input').attr('disabled','disabled');
            target.parents("tr").find('.item_name').siblings('.fstControls').parent('div').removeClass('fstResultsOpened fstActive')
        }
        function open_on_add_new() {

            $('.item_name').siblings('.fstControls').children('input').removeAttr('disabled');
        }
    </script>

    {{---------------------------------------- edit  row_table --------------------------------------------------------}}
    <script>
        $(document).on("click", ".edit_row", function(){

            $(this).parents("tr").addClass('edited_now');
            var input_class_array = ['quantity_value','unit_price','sub_total','total_row_tax'];
            var s=0;
            $(this).parents("tr").find("td:not(:last-child)").each(function(){
                if($(this).attr('td_type') == 'input')
                {
                    if(input_class_array[s] == 'sub_total')
                    {
                        $(this).html('<input disabled done_calculation="0" type="number" class="'+input_class_array[s] +' input_edit_call form-control" value="' + $(this).text() + '">');
                    }
                    else if(input_class_array[s] == 'total_row_tax')
                    {
                        $(this).html('<input  class="'+input_class_array[s] +' pull-left form-control"  disabled data-role="input" type="number" value="' + $(this).text() + '" />');
                    }
                    else
                    {
                        $(this).html('<input min="0" type="number" class="'+input_class_array[s] +' input_edit_call form-control" value="' + $(this).text() + '">');
                    }

                    s++;
                }
                else if ($(this).attr('td_type') == 'drop_down')
                {
                    var string = $(this).attr('get_value'),
                        strx   = string.split(',');

                    var  array  = [];

                    array = array.concat(strx);

                    //var obj = array.reduce(function(o, val) { o[val] = val; return o; }, {});
                    var res = [];

                    for (var i = 0; i < array.length; i += 1) {
                        var o = {};
                        o["text"] = array[i];

                        o["value"] = array[i];

                        res.push(o);
                    }


                    var choosed_item;
                    if(string == ""){
                        choosed_item =null;
                        console.log('null choosed_item');
                    }
                    else
                    {
                        choosed_item =JSON.stringify(res);
                    }

                    console.log('choosed_item :' + choosed_item);


                    if($(this).hasClass('item_name_v'))
                    {

                        var path = $('#file_path').attr('path_file');
                        console.log(JSON.stringify(res));
                        $(this).html('<input' +
                            '    type="text"' +
                            '    multiple' +
                            '    class="item_name"' +
                            '    value="'+ $(this).attr('current_value')+'"'+
                            '    data-initial-value='+"'"+"[{"+'"text"'+":"+'"'+res[0]['text']+'"'+","+'"value"'+":"+'"'+res[0]['value']+'"'+"}]"+"'"+
                            '    data-user-option-allowed="true"' +
                            '    data-url="'+path+'"' +
                            '    data-load-once="false"' +
                            '    name="language"/>');
                        $('.item_name').item_name_fastselect();

                    }
                    else if($(this).hasClass('tax_value_v'))
                    {
                        console.log(JSON.stringify(res));
                        $(this).html('<input\n' +
                            '    type="text"\n' +
                            '    multiple\n' +
                            '    class="tax_value"\n' +
                            '    value="'+$(this).attr('get_value')+'"' +
                            '    data-initial-value='+choosed_item+
                            '    data-url="/data_tax.json"\n' +
                            '    data-load-once="false"\n' +
                            '    name="language"/>');
                        $('.tax_value').tax_fastselect();
                    }

                }
                else if ($(this).attr('td_type') == 'search')
                {
                    $(this).html('<select class="item_type form-control js-example-disabled-results">\n' +
                        '</select>');

                    get_item_type_drop_down($(this).find('.item_type'));
                    var selected_v = $(this).attr('current_value');
                    $(this).find(".js-example-disabled-results").val(selected_v);
                    $(".js-example-disabled-results").select2();
                    $(".js-example-disabled-results").siblings('.select2').css('width', '71px');
                }
                else if ($(this).attr('td_type') == 'check_box')
                {
                    $(this).html('<input  class="include_check  "   data-role="input" type="checkbox"/>');
                    if($(this).attr('current_value')==1)
                    {
                        $(this).find('.include_check').prop('checked', true);
                    }
                    else
                    {
                        $(this).find('.include_check').prop('checked', false);
                    }
                }

            });
            limit_on_edit($(this));
            $("#add_row").attr("disabled", "disabled");
            $(this).text('Save');
            $(this).removeClass('edit_row');
            $(this).addClass('Save_row');
            get_totals();
        });
    </script>

    <!--delete row in table -->
    <script>

        $(document).on('click', '.delete_row', function () {

            $(this).hide(function(){
                $(this).closest('td').append('<button class="btn btn-danger" type="button" id="yes" style="display: none;">yes</button>' +
                    '<button class="btn btn-default" type="button" id="no" style="display: none;">no</button>');
                $(this).closest('td').find("#yes").show('slow').delay(1000);
                $(this).closest('td').find("#no").show('slow').delay(1000);

            });
        });

        $(document).on('click','#yes',function(){

            $(this).closest('tr').remove();
            get_totals();
            if($('.added_now').length == 0)
            {
                $("#add_row").removeAttr("disabled");
            }
        });

        $(document).on('click','#no',function(){

            console.log('noo ');

            $(this).closest('td').find(".delete_row").show('slow').delay(1000);
            $(this).closest('td').find("#yes").hide('slow').delay(1000).remove();
            $(this).closest('td').find("#no").hide('slow').delay(1000).remove();
            /*
            $(this).closest('td').find("#yes").hide('slow').delay(1000);
            $(this).closest('td').find("#no").hide('slow').delay(1000);*/


        });


    </script>
    <!--Edit Row in Table -->
    <script>
        function get_totals() {
            var input = $('#op_table > tbody  > tr');
            var untaxed_v = 0;
            var total_tax_v = 0;
            input.each(function () {
                if (!$(this).hasClass('added_now')) {
                    untaxed_v += parseFloat($(this).find('.sub_total_v').attr('current_value'));
                    total_tax_v += parseFloat($(this).find('.total_row_tax_v').attr('current_value'));

                }


            });

            $('#untaxed_all').html(untaxed_v.toFixed(2));
            $('#taxed_sum').html(total_tax_v.toFixed(2));
            $('#total_sum').html(parseFloat(total_tax_v+untaxed_v).toFixed(2));
        }
        /*untaxed_all*/
    </script>

    <script>

        $('.alert-autocloseable-success').hide();


        $(document).on('click','#save_po',function () {
            var status_po = $('#set_po_id').attr('current_po_status');
            add_new_po_all (status_po);
        });
        $(document).on('click','#sendto_approved_btn',function () {
            var status_po = $('#set_po_id').attr('current_po_status');
            if(status_po != 'approved_po')
            {
                //      $('#set_po_id').attr('current_po_status','to_approve_po');

                //    status_po = $('#set_po_id').attr('current_po_status');
                add_new_po_all ('to_approve_po');
            }
            //add_new_po_all (status_po);

            else{
                add_new_po_all ('approved_po');
            }


        });
        $(document).on('click','#approved_btn',function () {
            //$('#set_po_id').attr('current_po_status','approved_po');
            add_new_po_all ('approved_po');

        });


        $(document).on('click','#close_po_btn',function () {

            add_new_po_all ('closed_po');

        });

        function close_this_po()
        {
            $('#set_po_id').attr('closed_or_not',1);
            var cur_po_id = $('#set_po_id').attr('current_po_id');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/set_po_to_close/',
                dataType : 'json',
                type: 'get',
                data: {
                    cur_po_id:cur_po_id,
                },

                success:function(response) {


                    $('#set_po_id').attr('current_po_status','closed_po');
                    $('#close_po_btn').removeClass('hide');
                    $('#edit_po').prop('disabled',true);

                }

            });
        }



        function add_new_po_all (status_click)
        {
            var check_table = 1 ;
            var inputs_valid = $('.get_valide_for');
            inputs_valid.each(function(){
                if($(this).val() == '' || !$(this).val()) {
                    $(this)
                    $(this).css({
                        "border": "1px solid red",
                    });
                    $(this).siblings('.select2').css({
                        "border": "1px solid red",
                    });
                    check_table = 0;


                    notification('glyphicon glyphicon-warning-sign','Warning!','You Should Fill All Fields!','danger')

                    $(".errorMessage2").hide();
                }
                else{
                    $(this).css({'border' : ""});
                    $(this).siblings('.select2').css({'border' : ""});
                }
            });

            var rowCount = $('#op_table tbody tr:not(.added_now)').length;

            console.log(rowCount);


            if (rowCount == 0 && check_table == 1) {


                $(".errorMessage1").hide();
                notification('glyphicon glyphicon-warning-sign','Warning','Please Fill Table!','danger')


            }


            if(!$('#op_table tr').hasClass('edited_now') && rowCount >0 && check_table == 1)
            {


                $('#save_po').attr('disabled', 'disabled');
                $('#sendto_approved_btn').attr('disabled', 'disabled');
                $('#approved_btn').attr('disabled', 'disabled');

                var status = status_click;
                console.log(status);

                var supplier = $('#supplier').val();
                var beneficiary_user = $('#benefit_user').val();
                var department = $('#benefit_management').val();
                var currency = $('#currency').val();
                var order_date = $('#datetime1').val();
                var expected_date = $('#datetime2').val();
                var approved_date = $('#datetime3').val();
                var po_note = $('#po_comment').val();
                var canceled_po = $('#set_po_id').attr('closed_or_not');

                var current_user_id= $('#set_po_id').attr('current_id');
                var current_po_id = $('#set_po_id').attr('current_po_id');

                var input = $('#op_table > tbody  > tr');
                var row = [];

                input.each(function(){

                    if(!$(this).hasClass('added_now'))
                    {
                        row.push({"item_type": $(this).find('.item_type_v').attr('current_value'),"item_name": $(this).find('.item_name_v').attr('current_value'),"quantity": $(this).find('.quantity_value_v').attr('current_value'),"unit_price": $(this).find('.unit_price_v').attr('current_value'),"Include": $(this).find('.include_check_v').attr('current_value'),"tax": $(this).find('.tax_value_v').attr('current_value'),"subtotal": $(this).find('.sub_total_v').attr('current_value') });
                    }


                });

                console.log(input);
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/add_new_po/',
                    dataType : 'json',
                    type: 'get',
                    data: {

                        supplier:supplier,
                        beneficiary_user:beneficiary_user,
                        benefit_management:department,
                        currency:currency,
                        order_date:order_date,
                        expected_date:expected_date,
                        approved_date:approved_date,
                        po_note:po_note,
                        status:status,
                        current_po_id:current_po_id,
                        table_rows:row,
                        cur_user_id:current_user_id,
                        canceled_po:canceled_po


                    },

                    success:function(response) {
                        $(".errorMessage1").hide();
                        $(".errorMessage2").hide();

                        console.log(response);
                        if(response['error'])
                        {
                            $('.err_input').removeClass('red-border');
                            $('.err_input').parent().find('.select2').removeClass('red-border');

                            $('.errorMessage3').empty();

                            //$('.errorMessage2').show();
                            $.each(response['error'], function (key, val) {
                                /*alert(key + val);*/
                                var row = '<div>'+val+'</div><br>'



                                $('.errorMessage3').append(row);

                                $('.errorMessage3').css("display","block");

                                console.log(key + '_error');
                                $("." + key + '_error').addClass('red-border');
                                $('.' + key + '_error').parent().find('.select2').addClass('red-border');

                                //console.log(val[0]);

                            });

/*
                            $.each(response['error'], function (key, val)
                            {
                                //alert(key + val);

                                console.log(key+'_error');
                                $("."+key+'_error').addClass('red-border');
                                $('.'+key+'_error').parent().find('.select2').addClass('red-border');                               //console.log(val[0]);

                            })*/

                            $('#save_po').removeAttr('disabled');

                            $('#sendto_approved_btn').removeAttr('disabled');
                            $('#approved_btn').removeAttr('disabled');
                        }

                        else
                        {

                            $('#second').text(response['po_seq']);
                            console.log('second :' + response['po_seq'] );

                            $('.errorMessage3').hide();
                            var status_po = $('#set_po_id').attr('current_po_status');
                            if(status_po != 'approved_po' && status_click != 'closed_po')
                            {
                                $('#set_po_id').attr('current_po_status',status_click);

                            }
                            else if(status_click == 'closed_po')
                            {
                                $('#set_po_id').attr('current_po_status',status_click);

                            }

                            if(response['return_approved_date'])
                            {
                                $('#datetime3').val(response['return_approved_date']['date']);
                            }

                            $('#op_table tr.added_now').remove();
                            $('.errorMessage1').hide();
                            $('.err_input').removeClass('red-border');
                            $('.err_input').parent().find('.select2').removeClass('red-border');

                            $('.breadcrumb').removeClass('po-active');
                            $('#'+response['status']).addClass('po-active');
                            $('#'+response['status'])
                            $('#current_choosed_status').attr('coosed_status',response['status']);
                            $('#save_po').text("Edit");
                            $('#save_po').attr('id','edit_po');
                            $('#set_po_id').attr('current_po_id',response['current_po_id']);
                            $('.set_all_disabled').prop('disabled',true);
                            $('#creat_po').removeClass('hide');

                            $('#po_more').removeClass('hide');
                            if($('#set_po_id').attr('current_po_status') == 'draft_po')
                            {
                                $('#delete_btn').parent('li').removeClass('hide');
                                $('#second').val('Draft');
                            }
                            else if($('#set_po_id').attr('current_po_status') == 'closed_po')
                            {
                                $('#delete_btn').parent('li').addClass('hide');
                                $('#closed_po').removeClass('hide');
                                $('#close_po_btn').removeClass('hide');
                                /*$('#edit_po').prop('disabled',true);*/
                                close_this_po();

                            }
                            else
                            {
                                $('#delete_btn').parent('li').addClass('hide');
                                $('#closed_po').removeClass('hide');
                                $('#close_po_btn').removeClass('hide');
                            }
                            if($('#set_po_id').attr('current_po_status') != 'closed_po')
                            {
                                $('#edit_po').removeAttr('disabled');
                            }

                            //po-active
                            console.log(response);
                            $('#tax_details_table tbody tr').remove();

                            for(var i=0; i<response['po_tax_details'].length ;i++)
                            {
                                var row = '<tr>' +
                                    '<th>'+response['po_tax_details'][i]['tax_name']+'</th>' +
                                    '<td>'+response['po_tax_details'][i]['TOATL']+'</td>' +
                                    '</tr>'
                                $("#tax_details_table").prepend(row);
                            }
                            notification('glyphicon glyphicon-ok-sign','Congratulations!','PO Saved Succefully.','success');
                        }

                    },
                    error:function(response){
                        alert('Something went wrong');
                        window.location.href = '/show_po'

                    }

                });

                /*console.log(row);

                console.log(supplier,beneficiary_user,department,currency,order_date,expected_date,approved_date);

               console.log({{--{{\Illuminate\Support\Facades\Auth::user()->id}}--}});*/
            }
            else{
                $('#save_po').removeAttr('disabled');
                $('#sendto_approved_btn').removeAttr('disabled');
                $('#approved_btn').removeAttr('disabled');
            }
        }
    </script>

    <script>
        $(document).on('click','#edit_po',function () {
            $('#edit_po').text("Save");
            $('#edit_po').attr('id','save_po');
            if($('#set_po_id').attr('current_po_status') != 'approved_po')
            {
                $('.set_all_disabled').prop('disabled',false);
            }
            $('#close_po_btn').prop('disabled',false);
            if($('#set_po_id').attr('current_po_status') == 'draft_po')
            {
                $('#close_po_btn').prop('disabled',true);
            }

            console.log($('#set_po_id').attr('current_po_id'));
            $('#creat_po').addClass('hide');

            $('#delete_btn').parent('li').addClass('hide');

            $('#po_more').addClass('hide');
        });
    </script>
    <script>
        $(document).on('click','#creat_po',function () {/*
            $('#edit_po').prop('disabled',false);
            $('#edit_po').text("Save");
            $('#edit_po').attr('id','save_po');
            $('#save_po').prop('disabled',false);

            $('.set_all_disabled').prop('disabled',false);
            $('#set_po_id').attr('current_po_id',"");
            $('#set_po_id').attr('current_po_status',"draft_po");
            $('#creat_po').addClass('hide');
            $('#po_more').addClass('hide');

            $('#delete_po').parent('li').addClass('hide');

            /!*$(".set_all_disabled").val("");*!/
            $('.get_all_selectors').val('');
            //$('.get_all_date_input').val('');

            $(".get_all_selectors option[value='']").attr('selected', true);
            $('.get_all_date_input').val('');
            $('.select2-selection__rendered').html('');

            $('#op_table tbody tr').remove();
            $('.breadcrumb').removeClass('po-active');
            $('#untaxed_all').val('0.00');
            $('#taxed_sum').val('0.00');
            $('#total_sum').val('0.00');
            $('#untaxed_all').html('0.00');
            $('#taxed_sum').html('0.00');
            $('#total_sum').html('0.00');
            $('#tax_head').attr('column_tax',0);
            $('#subtotal_head').attr('column_subtotal',0);
            $('#close_po_btn').addClass('hide');
            $('#closed_po').addClass('hide');
            $('#set_po_id').attr('closed_or_not',0);
            $('#po_comment').val('');*/
            window.location.href = '/po';
        });
    </script>
    {{---- dublicate po ---}}
    <script>
        $(document).on('click','#dublicate_po',function(){

            $('#set_po_id').attr('current_po_id',"");
            $('#set_po_id').attr('current_po_status',"draft_po");
            $('#set_po_id').attr('closed_or_not',0);

            $('.set_all_disabled').prop('disabled',false);

            $('#creat_po').addClass('hide');
            $('#close_po_btn').addClass('hide');
            $('#closed_po').addClass('hide');
            $('#po_more').addClass('hide');
            $('.breadcrumb').removeClass('po-active');

            $('#edit_po').prop('disabled',false);
            $('#edit_po').text("Save");
            $('#edit_po').attr('id','save_po');
            $('#save_po').prop('disabled',false);
        });
    </script>

    {{-- close po btn--}}
    {{--<script>
        $(document).on('click','#close_po_btn',function () {

            $('#set_po_id').attr('closed_or_not',1);
            var cur_po_id = $('#set_po_id').attr('current_po_id');
            console.log(cur_po_id);

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/set_po_to_close/',
                dataType : 'json',
                type: 'get',
                data: {
                    cur_po_id:cur_po_id,
                },

                success:function(response) {



                    add_new_po_all ('closed_po');
                    $('#set_po_id').attr('current_po_status','closed_po');
                    $('#close_po_btn').removeClass('hide');

                }

            });



        });
    </script>--}}
    {{---------------  get_item_type_drop_down ----------------------------}}

    <script>

        function get_item_type_drop_down($dropdown)
        {
            $dropdown.prop('disabled',true);


            //var $dropdown = $("#doc_type");

            /*$dropdown.find('option').remove();*/

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/get_item_type_drop_down/',
                dataType : 'json',
                type: 'get',
                data: {

                },

                success:function(response) {

                    /*console.log(response);*/

                    var selected_val = $dropdown.parents('td').attr('current_value');
                    console.log('selected drop '+selected_val);

                    $dropdown.find('option').remove();

                    for(var i =0;i<response.length;i++)
                    {
                        $dropdown.append($("<option />").val(response[i]['pro_type_id']).text(response[i]['pro_type_name']));
                    }

                    $dropdown.val(selected_val);
                    $dropdown.prop('disabled',false);
                }

            });

        };

    </script>

    {{-------------------  get_item_name ------------------------}}

    <script>
        function get_item_name(item_type)
        {
            console.log(item_type.val());
            console.log(item_type.parents('tr').find('.item_name').attr('class'))
            item_type.parents('tr').find('.item_name').val("");
            /*item_type.parents('tr').find('.item_name').siblings('.fstControls').children('.fstChoiceItem').remove();*/
            item_type.parents('tr').find('.item_name').siblings('.fstControls').children('.fstQueryInput').prop('disabled', true);
            item_type.parents('tr').find('.item_name').siblings('.fstControls').children('.fstQueryInput').show();
            item_type.parents('tr').find('.item_name').siblings('.fstControls').children('.fstQueryInput').val('');
            item_type.parents('tr').find('.item_name').siblings('.fstControls').children('.fstQueryInput').attr('placeholder', 'loading....');
            item_type.parents('tr').find('.item_name').siblings('.fstControls').siblings('.fstResults').empty();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/get_item_name/',
                dataType : 'json',
                type: 'get',
                data: {
                    item_type:item_type.val(),
                },

                success:function(response) {
                    console.log(response);
                    //get_table();
                    item_type.parents('tr').find('.item_name').siblings('.fstControls').children('.fstQueryInput').prop('disabled', false);
                    item_type.parents('tr').find('.item_name').siblings('.fstControls').children('.fstQueryInput').attr('placeholder', 'Choose option');
                    /*item_type.parents('tr').find('.item_name').siblings('.fstControls').children('.fstQueryInput').focus();*/

                }

            });
        }
    </script>

    {{--------------------------  product select  ---------------------------}}
    <script>
        $(document).on('change','.item_type',function () {
            $(this).parents('tr').find('.item_name').siblings('.fstControls').children('.fstChoiceItem').children('.fstChoiceRemove').click();
            var item_type = $(this).parents('tr').find('.item_type');
            get_item_name(item_type)

        })
    </script>

    {{--<script>
        $(document).on('click','.fstQueryInput',function () {
            if($(this).parents('.fstControls').siblings().hasClass('item_name'))
            {
                var item_type = $(this).parents('tr').find('.item_type');
                get_item_name(item_type)
                console.log('changed item name' + item_type);
            }

        })
    </script>--}}

    {{----------------------------------------------------------------------------------}}
    {{---------------------------------  get tax --------------------------------------------}}
    <script>
        function get_tax()
        {

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/get_tax/',
                dataType : 'json',
                type: 'get',
                data: {

                },

                success:function(response) {
                    console.log(response);
                    //get_table();
                }

            });
        }
    </script>
    {{---------- get subtotal -------------}}

    {{-------------------   get subtotal --------------------------------------}}
    <script>
        /* $(document).on('keyup','.quantity_value',function () {
             get_sub_for_row($(this));
             var quantity = $(this).val();
             var unit_price = $(this).closest('tr').find('input.unit_price').val();
             $(this).closest('tr').find('input.sub_total').val(quantity*unit_price);
         });
         $(document).on('keyup','.unit_price',function () {
             get_sub_for_row($(this));
             var unit_price = $(this).val();
             var quantity = $(this).closest('tr').find('input.quantity_value').val();
             $(this).closest('tr').find('input.sub_total').val(quantity*unit_price);
         });*/
        /*$(document).on('change','.quantity_value',function () {
            get_sub_for_row($(this));
        });
        $(document).on('change','.unit_price',function () {
            get_sub_for_row($(this));
        });*/

    </script>


    <Script>
        function get_sub_for_row(inputs,thiss_input)
        {
            //var input = $('#op_table > tbody  > tr');

            var input = thiss_input.parents('tr');
            var check_v = 0;

            if(thiss_input.parents("tr").find('.include_check:checked').length>0)
            {
                check_v = 1;
            }
            else{check_v = 0;}

            var row = [];

            input.each(function(){

                row.push({"quantity": $(this).find('input.quantity_value').val(),"unit_price": $(this).find('.unit_price').val(),"Include": check_v,"tax": $(this).find('.tax_value').val() });


            });


            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/get_subtotal/',
                dataType : 'json',
                type: 'get',
                data: {

                    table_rows:row,

                },

                success:function(response) {

                    /*console.log(response);*/
                    console.log(response);
                    thiss_input.parents('tr').find('input.sub_total').val(response['$sub_val']);
                    thiss_input.parents('tr').find('input.total_row_tax').val(response['$total_tax']);
                    (thiss_input).removeAttr('disabled');
                    save_table_effect(inputs,thiss_input);
                },
                error:function(response) {

                    /*console.log(response);*/
                }

            });

            /*console.log(thiss_input.parents('tr').find('input.quantity_value_v').val());*/
            ////console.log(row);

        }

    </Script>

    <script>
        $(document).on('click','#delete_po',function(){
            var cur_po_id = $('#set_po_id').attr('current_po_id');

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/delete_this_po/',
                dataType : 'json',
                type: 'get',
                data: {
                    cur_po_id:cur_po_id,
                },

                success:function(response) {

                    window.location.href = '/show_po';
                }

            });

        });
    </script>


    {{-- <script>
         $(document).on('change','#currency',function () {
             var cur_symbol = $('#currency option:selected').text();
             $('.currency_value').html(cur_symbol);
         });
     </script>--}}

    {{-------------------  get currency of supplier -------------------------}}

    <script>
        $(document).on('change','#supplier',function () {
            var supplier = $('#supplier').val();
            if(supplier != "")
            {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/get_currency_of_supplier/',
                    dataType : 'json',
                    type: 'get',
                    data: {
                        supplier:supplier,
                    },

                    success:function(response) {

                        console.log(response);
                        $('#currency').val(response);
                        var cur_symbol = $('#currency option:selected').text();
                        $('.currency_value').html(cur_symbol);
                    }

                });
            }
            else{
                $('#currency').val('');
            }

        });
    </script>

    <script>
        $(document).ready(function () {
            $(".select_2_enable").select2();
            get_tax();
            console.log("current_id" + $('#set_po_id').attr('current_po_id'));

            if($('#set_po_id').attr('closed_or_not') == 1 || $('#set_po_id').attr('current_po_status') == 'approved_po')
            {
                $('.set_all_disabled').prop('disabled',true);
            }
        })
    </script>




    {{----------------------------when include checkbox checked color tax field with lavendar----------------}}
    <script>

        $(document).on('change','.include_check ',function(){

            if($(this).is(":checked")) {
                $(this).parents('tr').find('.tax_value').parents('.fstElement').css('background-color', 'lavender');
            }
            else{
                $(this).parents('tr').find('.tax_value').parents('.fstElement').css('background-color', '#fff');
            }
        });
</script>

    {{------------- limit the max of input number  ----------------}}
    <script>
        $(document).on('change','.unit_price',function() {
            var max = parseInt($(this).attr('max'));
            var min = parseInt($(this).attr('min'));
            if ($(this).val() > max)
            {
                $(this).val(max);
            }
            else if ($(this).val() < min)
            {
                $(this).val(min);
            }
        });
        $(document).on('change','.quantity_value',function() {
            var max = parseInt($(this).attr('max'));
            var min = parseInt($(this).attr('min'));
            if ($(this).val() > max)
            {
                $(this).val(max);
            }
            else if ($(this).val() < min)
            {
                $(this).val(min);
            }
        });
    </script>
{{-----------------  get user department ---------------------}}
    <script>
        $(document).on('change','#benefit_user',function(){

            if($(this).val() != "" )
            {
                var user = $(this).val();
                $('#benefit_management').prop('disabled',true);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/get_department_user/',
                dataType : 'json',
                type: 'get',
                data: {
                    user:user,
                },

                success:function(response) {

                    console.log(response);
                    $('#benefit_management').val(response[0]);
                    var cur_symbol = $('#benefit_management option:selected').text();
                    $('#select2-benefit_management-container').html(cur_symbol);
                }

            });
            }
            else{$('#benefit_management').prop('disabled',false);}


        });
    </script>

    {{---------------------------  get users for department  -----------------------------------}}
    <script>
        $(document).on('change','#benefit_management',function(){
            var department_id = $(this).val();
            $('#benefit_user').prop('disabled',true);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/get_users_for_department/',
                dataType : 'json',
                type: 'get',
                data: {
                    department_id:department_id,
                },

                success:function(response) {

                    var $dropdown = $('#benefit_user');
                    var selected_val = $dropdown.parents('td').attr('current_value');

                    $dropdown.find('option').remove();

                    $dropdown.append($("<option />").val('').text(''));



                    console.log(response);
                    $.each(response, function(key, value){

                        $dropdown.append($("<option />").val(key).text(value));

                    });
                    $dropdown.val(selected_val);
                    $('#benefit_user').prop('disabled',false);

                }

            });
        });
    </script>
    {{------------------  refresh suplier dropdown ----------------------------}}
    <script>

        function supplier_refresh()
        {

            var $dropdown = $("#supplier");
            $dropdown.prop('disabled',true);
            $dropdown.select2('destroy');
            var selected_value = $dropdown.val();

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/refresh_supplier/',
                    dataType : 'json',
                    type: 'get',
                    data: {

                    },

                    success:function(response) {

                        $dropdown.find('option').remove();
                        $dropdown.append('<option value=""></option>');

                        $.each(response, function(key, value){

                            $dropdown.append($("<option />").val(key).text(value));

                        });
                        $dropdown.val(selected_value);
                        $dropdown.select2();
                        $dropdown.prop('disabled',false);
                    }

                });


            }

        $(document).on('dblclick','.select2-selection',function () {
            var select_check = $(this).parents('.select2').siblings('select');


            if ($('#save_po').length && select_check.attr('id') == 'supplier') {
                supplier_refresh();
            }
        });
    </script>

    {{----------------------------  refresh users  ----------------------------}}
    <script>

        function refresh_users()
        {

                var $dropdown = $("#benefit_user");
                $dropdown.prop('disabled',true);
                $dropdown.select2('destroy');
                var selected_value = $dropdown.val();

                 var department = $('#benefit_management').val();
                 console.log('department : ' +department);
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: '/refresh_drop_users/',
                        dataType : 'json',
                        type: 'get',
                        data: {
                            department:department,
                        },

                        success:function(response) {

                            $dropdown.find('option').remove();
                            $dropdown.append('<option value=""></option>');


                            $.each(response, function(key, value){

                                $dropdown.append($("<option />").val(key).text(value));

                            });
                            $dropdown.val(selected_value);
                            $dropdown.select2();
                            $dropdown.prop('disabled',false);
                        }

                    });

        }

        $(document).on('dblclick','.select2-selection',function () {
            var select_check = $(this).parents('.select2').siblings('select');


            if ($('#save_po').length && select_check.attr('id') == 'benefit_user') {
                refresh_users();
            }
        });

    </script>

    {{----------------------------  refresh departments --------------------------------------}}
    <script>

        function refresh_department()
        {

                    var is_disabled_dep = 0;
                    var $dropdown = $("#benefit_management");

                    if($dropdown.prop('disabled') == true)
                    {
                        is_disabled_dep = 1;
                    }

                    $dropdown.prop('disabled',true);
                    $dropdown.select2('destroy');
                    var selected_value = $dropdown.val();

                    var user = $('#benefit_user').val();

                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: '/refresh_drop_departments/',
                        dataType : 'json',
                        type: 'get',
                        data: {
                            user:user,
                        },

                        success:function(response) {
                            console.log(response);
                            $dropdown.find('option').remove();
                            $dropdown.append('<option value=""></option>');


                            $.each(response, function(key, value){

                                $dropdown.append($("<option />").val(key).text(value));

                            });

                            $dropdown.val(selected_value);
                            $dropdown.select2();
                            if(is_disabled_dep == 0)
                            {
                                $dropdown.prop('disabled',false);
                            }

                        }

                    });

                }

            $(document).on('dblclick','.select2-selection',function () {
            var select_check = $(this).parents('.select2').siblings('select');


            if ($('#save_po').length && select_check.attr('id') == 'benefit_management') {
                console.log('refresh_department');
                refresh_department();
            }
        });

    </script>

    {{-------------- add new  item  -------------------}}


    {{--<script>
        /*----- remove create and edit value when select the same value from drop down to add it one  -----*/
        function dest_result(check_val,option) {

            console.log('here dest');
            console.log(option);
            if(option){
                console.log($(option.$item[0]).parent('.fstResults').siblings('.fstControls').children('.create_edit_v').html());
                $(option.$item[0]).parent('.fstResults').siblings('.fstControls').children('.create_edit_v').each(function () {

                    if($(this).attr('c_e_value') == check_val)
                    {

                        $(this).remove()
                    }
                })

            }
        }

    </script>--}}

    <script>
        function set_new_dropdown(new_val)
        {
            //dest_result(new_val);
            var item = $('#op_table tbody').find('.item_type');
            var item_id = $('#op_table tbody').find('.item_type').val();

            item.parents('tr').find('.item_name').siblings('.fstControls').children('.fstQueryInput').prop('disabled',true);
            item.parents('tr').find('.item_name').siblings('.fstControls').children('.fstChoiceItem').css("background-color", "#849aad");

            item.parents('tr').addClass('add_new_product');

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/set_new_dropdown/',
                dataType : 'json',
                type: 'get',
                data: {
                    new_product:new_val,
                    item_type:item_id
                },

                success:function(response) {
                    console.log(response);
                    if(response['error'])
                    {
                        console.log('  product already exists ')
                        item.parents('tr').find('.item_name').siblings('.fstControls').children('.fstChoiceItem').children('.fstChoiceRemove').click();

                        item.parents('tr').find('.item_name').siblings('.fstControls').children('.fstQueryInput').prop('disabled',false);
                        item.parents('tr').removeClass('add_new_product');
                        notification('glyphicon glyphicon-ok-sign','Warning',' This product already exists ! ','danger');

                    }
                    else{
                        item.parents('tr').find('.item_name').siblings('.fstControls').children('.fstChoiceItem').css("background-color", "#43A2F3");
                        item.parents('tr').find('.item_name').siblings('.fstControls').children('.fstQueryInput').prop('disabled',false);
                        item.parents('tr').find('.item_name').val(response['product_id']);

                        item.parents('tr').removeClass('add_new_product');

                    }

                }

            });
        }
    </script>
    {{-----------   on leave page -------------------}}
    <script type='text/javascript'>

            $(window).bind('beforeunload', function(){
                if($('#save_po').length){
                    return 'Are you sure you want to leave?';
                }

            });


    </script>




    {{-------- on click on print go to page----------}}
    <script>

        $(document).on('click','#print',function () {


          //  window.location.href = 'get_po2/276'

            var id = $('#set_po_id').attr('current_po_id');
            window.open('/get_po2/'+id+'','_blank');


        });


    </script>

@endsection

