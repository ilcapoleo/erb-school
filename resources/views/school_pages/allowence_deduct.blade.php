@extends('layouts.main_app')

@section('main_content')
    <link href="{{asset('css/fastselect.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/select2.min.css')}}" rel="stylesheet" />
    <style>
        input.col-md-4.text-center {

            border:0;
            background-color: #fff;
        }

        input[type=number]{
            width: 80px;
        }

        .collapse > .row>.col-md-4>.btn, .collapse > .row>.col-md-4>.dropdown>.btn {
            margin-top: 0;

        }

        .btn{
            padding: 3px 10px;
            margin-top: 7%;
        }

        .third-nav{
            background-color:#eeeeee;
        }

        .alert-success{
            display: none;
        }
        .panel .table > thead > tr > td:first-child, .panel .table > thead > tr > th:first-child, .panel .table > tbody > tr > td:first-child, .panel .table > tbody > tr > th:first-child, .panel .table > tfoot > tr > td:first-child, .panel .table > tfoot > tr > th:first-child{
            padding-left: 0px;
        }


        .myselect2{
            width:171px;
        }

        .checkbox-general{
            display:none;
        }



        select2-container{
            width: 85%;
        }

        ul#select2-state-bh-results{
            text-align: center;
        }

        span.select2.select2-container.select2-container--default{
            width: 100%!important;
        }

        .red-border{
            border: 1px solid red  !important;
        }
    </style>
    <div class="main">
        <!-- Navbar-content -->
        <nav class="navbar navbar-default navbar2">

            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <form id="print_form" action="" target="_blank" method="get">
                    {{csrf_field()}}
                    <input type="hidden" name="acadmic_year_print" id="acadmic_year_print" value="" />

                    <button hidden id="get_pdff" type="submit"> print </button>
                </form>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                    <!--breadcrumbs-->
                    <div class="row">
                        <div class="col-md-10 col-lg-10">

                            <ol class="breadcrumb breadcrumb_nav">
                                <li><a href="/allowence_deduction/table">Allowence and Deduction</a></li>
                                <li class="active" id="second">

                                    @if(isset($allow_master))
                                        @if($allow_master->allow_id != 0)
                                            {{$allow_master->allow_id}}
                                        @else
                                            Draft
                                        @endif
                                    @endif

                                </li>
                            </ol>
                        </div>


                        @if(isset($allow_master))
                            <div class="col-md-2 col-lg-2">
                                @if(@\App\Allowence_deduction::where('allow_id','<',$allow_master->allow_id)->orderBy('allow_id','desc')->first()->allow_id)
                                    <div class="col-md-6">
                                        <a href="/allowence_deduction/edit/{{@\App\Allowence_deduction::where('allow_id','<',$allow_master->allow_id)->orderBy('allow_id','desc')->first()->allow_id}}"><span
                                                    class="glyphicon glyphicon-arrow-left"></span>
                                        </a>
                                    </div>
                                @endif
                                @if(@\App\Allowence_deduction::where('allow_id','>',$allow_master->allow_id)->orderBy('allow_id','asc')->first()->allow_id)
                                    <div class="col-md-6">
                                        <a href="/allowence_deduction/edit/{{@\App\Allowence_deduction::where('allow_id','>',$allow_master->allow_id)->orderBy('allow_id','asc')->first()->allow_id}}">
                                    <span class="glyphicon glyphicon-arrow-right">
                                </span>
                                        </a>
                                    </div>
                                @endif
                            </div>
                        @endif

                    </div>   <!--End breadcrumbs-->
                    <div class="row">
                        <div class="col-md-4 col-lg-4 buttons">
                            @if(isset($allow_master))
                                <button id="edit_po" type="button" class="btn btn-danger " >Edit</button>
                                <button type="button" class="btn" id="creat_po" >Create</button>
                            @else
                                <button id="save_po" type="button" class="btn btn-danger disable_on_save" >Save</button>
                                <button type="button" class="btn hide" id="creat_po" >Create</button>
                            @endif
                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-4 ">
                            @if(isset($allow_master))
                                <div class="dropdown">
                                    <button class=" btn btn-primary  dropdown-toggle" type="button"
                                            data-toggle="dropdown" id="more" hidden>More
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">


                                        {{--<li><a href="#">Export</a></li>--}}
                                        <li><a href="#" id="delete_po">Delete</a></li>
                                        <li><a href="#" id="print">Print</a></li>

                                    </ul>
                                </div>
                            @else
                                <div class="dropdown">
                                    <button class="hide btn btn-primary  dropdown-toggle" type="button"
                                            data-toggle="dropdown" id="more" hidden>More
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        {{--<li><a href="#">Export</a></li>--}}
                                        <li><a href="#" id="delete_po">Delete</a></li>
                                        <li><a href="#" id="print">Print</a></li>

                                    </ul>
                                </div>
                            @endif
                        </div>

                    </div> <!--collapse -->

                </div><!--collapse -->

            </div>
        </nav>
        <!-- End Navbar-content -->


        <!-- Success messages -->
        <div class="alert alert-success alert-autocloseable-success" style="display:none;position:absolute;" >
            Your Document Saved Successfully!

        </div>
        <!--end Success messages -->


        <!-- failuer messages -->
        <div class="alert alert-danger errorMessage1" style="display:none;position:absolute;" >
            Your should fill all fields!

        </div>
        <!--end failuer messages -->



        <!-- MAIN CONTENT -->
        <div class="main-content">

            <div class="container-fluid">

                <!-- INPUTS -->
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Allowence and Deduction</h3>
                    </div>
                    <div class="panel-body">

                        <div class="row">

                            @if(isset($allow_master))
                                <div id="set_po_id"  current_po_id="{{$allow_master->allow_id}}" current_po_status="" closed_or_not="" current_id="{{Auth::user()->id}}"></div>
                            @else
                                <div id="set_po_id"  current_po_id="" current_po_status="draft_po" closed_or_not="0" current_id="{{Auth::user()->id}}"></div>
                            @endif

                            <div class="col-md-6 col-lg-6">

                                <div class="form-group">
                                    <label class="col-md-6 col-lg-6  control-label" for="allowence">Type</label>
                                    <div class="col-md-6 col-lg-6">
                                        <label class="radio-inline" for="status-0">
                                            <input type="radio" name="allowence" class="set_all_disabled Type_error err_input" checked id="status-0" value="0" @if(isset($allow_master))  @if($allow_master->allow_type == 0) checked @endif disabled @endif>
                                            Allowence
                                        </label>
                                        <label class="radio-inline" for="status-1">
                                            <input type="radio" name="allowence" class=" set_all_disabled Type_error err_input" id="status-1" value="1" @if(isset($allow_master))  @if($allow_master->allow_type == 1) checked @endif disabled @endif>
                                            Deduction
                                        </label>

                                    </div>
                                </div>
                            </div>
                        </div>


                        <br>


                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="usr" class="col-md-6 col-lg-6 control-label">Name </label>

                                    <div class=" col-md-6 col-lg-6">

                                        @if(isset($allow_master))
                                            <input id="allow_name" class="set_all_disabled required get_all_date_input row_data borrow_value form-control Name_error err_input"   type="text" value="{{$allow_master->allow_name}}" disabled >
                                        @else
                                            <input id="allow_name" class="set_all_disabled required get_all_date_input row_data borrow_value form-control Name_error err_input"   type="text" value="">
                                        @endif


                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>

                        <div class="row">
                            <div class="col-md-12">

                                <div id="name_error" class="alert alert-danger errorMessage hide ">
                                    <strong >Wrong Name!</strong>
                                </div>
                            </div>

                        </div>

                        <div class="row">


                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="usr" class="col-md-6 col-lg-6 control-label">Short Code </label>

                                    <div class=" col-md-6 col-lg-6">

                                        @if(isset($allow_master))
                                            <input id="short_name" class="set_all_disabled required get_all_date_input row_data borrow_value form-control Short_Name_error err_input"   type="text" value="{{$allow_master->allow_short_code}}"disabled >
                                        @else
                                            <input id="short_name" class="set_all_disabled required get_all_date_input row_data borrow_value form-control Short_Name_error err_input"   type="text" value="">
                                        @endif


                                    </div>

                                </div>

                            </div>



                        </div>

                        <br>

                        <div class="row">
                            <div class="col-md-12">

                                <div id="password_error" class="alert alert-danger errorMessage hide ">
                                    <strong>Wrong code!</strong>
                                </div>

                            </div>

                        </div>

                        <div class="row">

                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="usr" class="col-md-6 col-lg-6 control-label">Account Name</label>

                                    <div class=" col-md-6 col-lg-6">

                                        <select name="account_name" id="account_name" class=" required get_all_selectors set_all_disabled select_2_enable form-control err_input Account_Name_error" @if(isset($allow_master))disabled @endif>
                                            <option value=""> </option>
                                            @foreach($accounts as $id=>$role)
                                                @if(isset($allow_master))
                                                    @if($allow_master->account_id == $id)
                                                        <option selected value="{{$id}}">{{$role}}</option>
                                                    @else
                                                        <option value="{{$id}}">{{$role}}</option>
                                                    @endif
                                                @else
                                                    <option value="{{$id}}">{{$role}}</option>
                                                @endif
                                            @endforeach
                                        </select>

                                    </div>

                                </div>

                            </div>


                        </div>


                        <br>

                        <div class="row">
                            <div class="col-md-12">
                                <div id="center_error" class="alert alert-danger errorMessage hide ">
                                    <strong>Wrong !</strong>
                                </div>

                            </div>

                        </div>

                        <br>





                    </div>
                    <br>
                </div>
            </div>

        </div>
        <!-- END INPUTS -->


    </div>

@endsection


@section('custom_footer')
    <script src="{{asset('js/item_name_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/tax_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/select2.min.js')}}"></script>


    {{--------------------  active side & nav bar ------------------------}}
    <script>
        $('.side_sheets').addClass('hide');
        $('#HR').removeClass('hide');

        $("#menu li").removeClass('active');
        $('#hr_nav_bar').addClass('active');

        $('#pages ul li a').removeClass('active');
        $('#allowence_deduct_side_bar').addClass('active');

    </script>
    <!--edit fields -->

    <script>
        $(".select_2_enable").select2();
    </script>

    <script>
        console.log($("input[name='allowence']:checked").val());
    </script>


    {{------------  add new row  ------------------------}}
    <script>
        $(document).on('click','#add_row',function () {

            $(this).attr('disabled',true);
            var row = '<tr class= "added_now">' +

                '<td td_type="search">' +
                '<select class="beneficinary_type form-control js-example-disabled-results">\n' +
                '</select></td>' +
                '<td td_type="search">' +
                '<select class="salary_scale form-control js-example-disabled-results">\n' +
                '</select></td>' +
                '<td td_type="input"><input class="allow_value row_data" min="1"  data-role="input" type="number"  /></td>'+
                '<td td_type="search">' +
                '<select class="currency form-control js-example-disabled-results">\n' +
                '</select></td>'

                +'<td>'+'<button class="disable_on_save set_all_disabled btn btn-primary btn-xs Save_row">'+'Save'+'</button>'
                +'</td>'
                +'<td>'
                +'<button class="disable_on_save set_all_disabled btn btn-danger btn-xs delete_row"  >'+'<span class="lnr lnr-trash"></span>'+'</button>'
                +'</td>'
                +'</tr>';
            $("#op_table").append(row);
            get_type_drop_down($('.beneficinary_type'));
            get_currency_drop_down($('.currency'));
            //get_salary_drop_down($('.salary_scale'));
            $(".js-example-disabled-results").select2();
            $(".js-example-disabled-results").siblings('.select2').css('width', '71px');

        });
    </script>


    {{------------------------ Beneficiary Type	 -------------------------------}}
    <script>

        function get_type_drop_down($dropdown)
        {

            $dropdown.prop('disabled',true);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/allowence_deduction/get_b_type/',
                dataType : 'json',
                type: 'get',
                data: {
                },

                success:function(response) {

                    console.log(response);

                    var selected_val = $dropdown.parents('td').attr('current_value');

                    console.log('selected drop '+selected_val);

                    $dropdown.find('option').remove();

                    for(var i =0;i<response.length;i++)
                    {
                        $dropdown.append($("<option />").val(response[i]['Beneficiary_id']).text(response[i]['Beneficiary_name']));
                    }
                    $dropdown.val(selected_val);

                    $dropdown.prop('disabled',false);

                }

            });

        }

    </script>


    {{------------------------  currency dropdown	 -------------------------------}}
    <script>

        function get_currency_drop_down($dropdown)
        {


            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/allowence_deduction/get_currency/',
                dataType : 'json',
                type: 'get',
                data: {

                },

                success:function(response) {

                    console.log(response);

                    var selected_val = $dropdown.parents('td').attr('current_value');

                    console.log('selected drop '+selected_val);

                    $dropdown.find('option').remove();

                    for(var i =0;i<response.length;i++)
                    {
                        $dropdown.append($("<option />").val(response[i]['Currency_id']).text(response[i]['Currency_name']));
                    }
                    $dropdown.val(selected_val);

                }

            });

        }

    </script>


    {{------------------------ get salary scale -------------------------------}}
    <script>

        function get_salary_drop_down($dropdown,save)
        {

            var input = $('#op_table > tbody  > tr');
            var all_selected_months = [];
            if(save == 1)
            {
                var current_selected_b_type = $dropdown.parents("tr").find('.beneficinary_type').val();
            }
            else{
                var current_selected_b_type = $dropdown.parents("tr").find('.beneficinary_type_v').attr('current_value');
            }


            console.log('current_selected_b_type :'+current_selected_b_type);
            input.each(function(){

                if(!$(this).hasClass('added_now'))
                {
                    if(current_selected_b_type == 3 )
                    {
                        console.log('in current type == 3 ');
                        all_selected_months.push($(this).find('.beneficinary_type_v').parents("tr").find('.salary_scale_v').attr('current_value'));
                    }

                    else{
                        console.log('in else curr types  s d ');

                        all_selected_months.push($(this).find('.beneficinary_type_v[current_value='+current_selected_b_type+']').parents("tr").find('.salary_scale_v').attr('current_value'));
                        all_selected_months.push($(this).find('.beneficinary_type_v[current_value=3]').parents("tr").find('.salary_scale_v').attr('current_value'));
                    }




                    /*$(this).find('.salary_scale_v').attr('current_value');*/
                }


            });
            console.log('all_selected_months : ' +all_selected_months);
            var current_selected_value = $dropdown.parents("tr").find('.salary_scale_v').attr('current_value');


            //var $dropdown = $("#doc_type");

            /*$dropdown.find('option').remove();*/

            console.log('current_selected_value'+current_selected_value);

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/allowence_deduction/get_salary/',
                dataType : 'json',
                type: 'get',
                data: {
                    all_selected_months:all_selected_months,
                    current_selected_value:current_selected_value
                },

                success:function(response) {

                    console.log(response);

                    var selected_val = $dropdown.parents('td').attr('current_value');

                    console.log('selected drop '+selected_val);

                    $dropdown.find('option').remove();

                    for(var i =0;i<response['data'].length;i++)
                    {
                        $dropdown.append($("<option />").val(response['data'][i]['salary_id']).text(response['data'][i]['salary_name']));
                    }

                    for(var i =0;i<response['extra_v'].length;i++)
                    {
                        $dropdown.append($("<option />").val(response['extra_v'][i]['salary_id']).text(response['extra_v'][i]['salary_name']));
                    }
                    $dropdown.val(selected_val);


                    /*
                                        $(".item_product").select2();
                                        $(".item_product").siblings('.select2').css('width', '71px');*/
                }

            }).done(function () {
                $dropdown.prop('disabled',false);
            });

        }

    </script>

    {{-------------------get salary on change of b_type------------------------}}
    <script>
        $(document).on('change','.beneficinary_type',function () {

            var salary = $(this).parents('tr').find('.salary_scale');
            $(this).parents('tr').find('.salary_scale').prop('disabled',true);
            get_salary_drop_down(salary,1);

        });
    </script>


    {{--------------  save row  ----------------------}}

    <script>
        $(document).on("click", ".Save_row", function(){

            var empty = false;
            var input = $(this).parents("tr").find('input[type="text"],input[type="number"]');

            if(!$(this).parents("tr").find('.beneficinary_type').val() || $(this).parents("tr").find('.beneficinary_type').val()=="" || !$(this).parents("tr").find('.salary_scale').val() || $(this).parents("tr").find('.salary_scale').val()=="" || !$(this).parents("tr").find('.currency').val() || $(this).parents("tr").find('.currency').val()=="" )
            {
                empty = true;
            }

            input.each(function(){
                if(!$(this).val()|| $(this).val()==""){
                    $(this).addClass("error");
                    empty = true;
                } else{
                    $(this).removeClass("error");
                }
            });

            console.log('empty '+empty);

            $(this).parents("tr").find(".error").first().focus();

            if(!empty ) {
                $(this).attr('disabled','disabled');
                ($(this)).removeAttr('disabled');
                $(this).prop('disabled',true);
                save_table_effect($(this));
            }


        });

        function save_table_effect(thiss) {
            console.log('passed')


            /*--------- select  po ------------------*/
            /*var select_po_drop_down = thiss.parents("tr").find('select.month_value');
            var value_select = select_po_drop_down.val();
            var text_select = thiss.parents("tr").find('select.month_value option:selected').text();

            select_po_drop_down.parent("td").addClass('month_value_v').attr('current_value', value_select).html(text_select);*/
            /*--------------------------*/
            var select_array = ['beneficinary_type_v','salary_scale_v','currency_v'];
            var r=0;
            var all_dowp_down = thiss.parents("tr").find('select');
            all_dowp_down.each(function () {
                var value_select = $(this).val();
                var text_select = $(this).find('option:selected').text();
                $(this).parent("td").addClass(select_array[r]).attr('current_value', value_select).html(text_select);
                r++;

            });
            /*--------- input quantity ------------------*/
            var quantity = thiss.parents("tr").find('.allow_value');
            var quantity_value = quantity.val();


            console.log('allow_value' +quantity_value)
            quantity.parent("td").addClass('allow_value_v').attr('current_value', quantity_value).html(quantity_value);
            /*--------------------------*/

            if (thiss.parents("tr").hasClass('added_now')) {
                thiss.parents("tr").removeClass('added_now');

            }
            if (thiss.parents("tr").hasClass('edited_now')) {
                thiss.parents("tr").removeClass('edited_now');

            }

            //|| thiss.siblings().find('.create_edit_v') ){
            //console.log(thiss.closest('tr').attr('id'))


            if ($('.added_now').length == 0) {
                $("#add_row").removeAttr("disabled");
            }

            thiss.text('Edit');
            thiss.removeClass('Save_row');
            thiss.addClass('edit_row');
            thiss.prop('disabled',false);
            $('#save_details').click();
        }


    </script>



    {{----------------------  edit row --------------------}}
    <script>
        $(document).on("click", ".edit_row", function(){
            if(!$('#op_table tr').hasClass('added_now'))
            {


                $('#add_row').prop('disabled',true);
                $(this).parents("tr").addClass('edited_now');

                $(this).parents("tr").find("td:not(:last-child)").each(function(){
                    if($(this).attr('td_type') == 'input')
                    {
                        $(this).html('<input class="allow_value row_data form-control" min="1"  data-role="input" type="number"  value="'+$(this).text()+'" />');

                    }

                    else if ($(this).attr('td_type') == 'search')
                    {
                        if($(this).hasClass('beneficinary_type_v'))
                        {
                            $(this).html('<select class="beneficinary_type form-control js-example-disabled-results">\n' +
                                '</select>');
                            get_type_drop_down($(this).find('.beneficinary_type'));
                            var selected_v = $(this).attr('current_value');
                            $(this).find(".beneficinary_type").val(selected_v);
                            $(".beneficinary_type").select2();
                            $(".beneficinary_type").siblings('.select2').css('width', '71px');
                        }

                        else if($(this).hasClass('salary_scale_v'))
                        {
                            $(this).html('<select class="salary_scale form-control js-example-disabled-results">\n' +
                                '</select>');
                            get_salary_drop_down($(this).find('.salary_scale'),0);
                            var selected_v = $(this).attr('current_value');
                            $(this).find(".salary_scale").val(selected_v);
                            $(".salary_scale").select2();
                            $(".salary_scale").siblings('.select2').css('width', '71px');
                        }

                        else if($(this).hasClass('currency_v'))
                        {
                            $(this).html('<select class="currency form-control js-example-disabled-results">\n' +
                                '</select>');
                            get_currency_drop_down($(this).find('.currency'));
                            var selected_v = $(this).attr('current_value');
                            $(this).find(".currency").val(selected_v);
                            $(".currency").select2();
                            $(".currency").siblings('.select2').css('width', '71px');
                        }

                    }


                });

                $("#add_row").attr("disabled", "disabled");
                $(this).text('Save');
                $(this).removeClass('edit_row');
                $(this).addClass('Save_row');

            }
        });

    </script>


    {{-------------  delete table row  --------------------}}
    <script>

        $(document).on('click', '.delete_row', function () {

            $(this).hide(function(){
                $(this).closest('td').append('<button class="btn btn-danger" type="button" id="yes" style="display: none;">yes</button>' +
                    '<button class="btn btn-default" type="button" id="no" style="display: none;">no</button>');
                $(this).closest('td').find("#yes").show('slow').delay(1000);
                $(this).closest('td').find("#no").show('slow').delay(1000);

            });


        });


        $(document).on('click','#yes',function(){


            /************************************/
            var salary = $(this).parents('tr').parents('tbody').find('.salary_scale');
            /*$(this).parents('tr').parents('tbody').find('.salary_scale').remove();*/
            $(this).closest('tr').remove();
            get_salary_drop_down(salary,1);

            if($('.added_now').length == 0)
            {
                $("#add_row").removeAttr("disabled");
            }
            /*******************************/


        });

        $(document).on('click','#no',function(){

            console.log('noo ');

            $(this).closest('td').find(".delete_row").show('slow').delay(1000);
            $(this).closest('td').find("#yes").hide('slow').delay(1000).remove();
            $(this).closest('td').find("#no").hide('slow').delay(1000).remove();
            /*
            $(this).closest('td').find("#yes").hide('slow').delay(1000);
            $(this).closest('td').find("#no").hide('slow').delay(1000);*/


        });






    </script>


    {{-------------------- save new allowence --------------------}}
    <script>

        $('.alert-autocloseable-success').hide();


        $(document).on('click','#save_po',function () {
            if(!$('#op_table tr').hasClass('edited_now')) {
                $("#op_table tbody .added_now").remove();
                $('#save_po').attr('disabled', 'disabled');


                var document_type = $("input[name='allowence']:checked").val();
                var document_name = $('#allow_name').val();
                var document_short_name = $('#short_name').val();
                var account_name = $('#account_name').val();

                var current_user_id = $('#set_po_id').attr('current_id');
                var current_po_id = $('#set_po_id').attr('current_po_id');

                console.log(document_type + ' ' + document_name + ' ' + document_short_name + ' ' + account_name);

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/allowence_deduction/save_new/',
                    dataType: 'json',
                    type: 'get',
                    data: {

                        Type: document_type,
                        Name: document_name,
                        Short_Name: document_short_name,
                        Account_Name: account_name,

                        current_user_id: current_user_id,
                        current_po_id: current_po_id

                    },

                    success: function (response) {

                        console.log(response);
                        if (response['error']) {
                            $('.err_input').removeClass('red-border');
                            $('.err_input').parent().find('.select2').removeClass('red-border');

                            notification('glyphicon glyphicon-warning-sign','Warning','You Should Fill All Fields','danger')


                            $.each(response['error'], function (key, val) {
                                //alert(key + val);

                                console.log(key + '_error');
                                $("."+key+'_error').addClass('red-border');
                                $('.'+key+'_error').parent().find('.select2').addClass('red-border');

                                //console.log(val[0]);

                            })
                            $('#save_po').removeAttr('disabled');

                        }

                        else {

                            $('.errorMessage1').hide();
                            $('.err_input').removeClass('red-border');
                            $('.err_input').parent().find('.select2').removeClass('red-border');
                            $('#save_po').text("Edit");
                            $('#save_po').attr('id', 'edit_po');
                            $('#add_row').prop('disabled', true);
                            $('#set_po_id').attr('current_po_id', response['current_po_id']);
                            $('.set_all_disabled').prop('disabled', true);
                            $('#creat_po').removeClass('hide');

                            $('#more').removeClass('hide');

                            $('#more').removeClass('hide');


                            $('#edit_po').removeAttr('disabled');
                            //po-active
                            console.log(response);

                            notification('glyphicon glyphicon-ok-sign','Congratulations!','Allowence / Deduction Saved Successfully!','success')




                        }

                    }

                });
            }
        });

    </script>


    {{----------------------- edit allownce -----------------------}}
    <script>
        $(document).on('click','#edit_po',function () {

            if($('#acadimic_year').val() != "")
            {
                $('#add_row').removeAttr('disabled');
            }

            $('#edit_po').text("Save");
            $('#edit_po').attr('id','save_po');

            $('.set_all_disabled').prop('disabled',false);


            $('#creat_po').addClass('hide');

            $('#more').addClass('hide');
            $('#allow_details').removeClass('hide');

        });
    </script>

    {{------------------  acadimic year dropdown ----------------------}}
    <script>
        $(document).on('change','#acadimic_year',function () {

            var acadimic_year = $(this).val();

            if(acadimic_year != "" )
            {
                $("#add_row").prop('disabled',true);
                $("#save_details").prop('disabled',true);
                var allow_id = $('#set_po_id').attr('current_po_id');

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/allowence_deduction/get_table_on_drop_down',
                    dataType : 'json',
                    type: 'get',
                    data: {
                        allow_id:allow_id,
                        Acadimic_year:acadimic_year
                    },

                    success:function(response) {

                        if(response['error'])
                        {
                            $('.errorMessage').addClass('hide');
                            $.each(response['error'], function (key, val)
                            {
                                //alert(key + val);

                                console.log(key+'_error');
                                $("#"+key+'_error').removeClass('hide').html(val[0]);
                                //console.log(val[0]);

                            })
                            $('#save_details').removeAttr('disabled');
                            $("#add_row").removeAttr('disabled');
                        }
                        else{
                            $("#op_table tbody").empty();
                            for(var i =0;i<response['allow_detailes'].length;i++)
                            {

                                var row = '<tr class="">' +
                                    '<td td_type="search" data-select2-id="50" class="beneficinary_type_v" current_value="'+response['allow_detailes'][i]['benficinary_id']+'">'+response['allow_detailes'][i]['Beneficiary_name']+'</td>' +
                                    '<td td_type="search" data-select2-id="55" class="salary_scale_v" current_value="'+response['allow_detailes'][i]['salary_id']+'">'+response['allow_detailes'][i]['salary_name']+'</td>' +
                                    '<td td_type="input" class="allow_value_v" current_value="'+response['allow_detailes'][i]['value']+'">'+response['allow_detailes'][i]['value']+'</td>' +
                                    '<td td_type="search" data-select2-id="61" class="currency_v" current_value="'+response['allow_detailes'][i]['currency_id']+'">'+response['allow_detailes'][i]['Currency_name']+'</td>' +
                                    '<td><button class="set_all_disabled btn btn-primary btn-xs edit_row">Edit</button></td>' +
                                    '<td><button class="set_all_disabled btn btn-danger btn-xs delete_row">' +
                                    '<span class="lnr lnr-trash"></span>' +
                                    '</button>' +
                                    '</td>' +
                                    '</tr>';
                                $("#op_table").append(row);
                            }


                            console.log(response);
                            $('#save_details').removeAttr('disabled');
                            if($('#save_po').length)
                            {
                                $("#add_row").removeAttr('disabled');
                            }

                        }


                    }

                });

            }
            else{
                $("#op_table tbody").empty();
                $("#add_row").prop('disabled',true);
                /*$("#op_table tbody").empty();*/
            }

        });
    </script>

    {{------------------------  save details  ----------------------------}}
    <script>

        $(document).on('click','#save_details',function () {

            $('.disable_on_save').prop('disabled',true);
            $('#save_details').attr('disabled','disabled');
            $("#op_table tbody .added_now").remove();
            var input = $('#op_table > tbody  > tr');
            var row = [];

            input.each(function(){

                if(!$(this).hasClass('added_now'))
                {
                    row.push({"beneficinary_type": $(this).find('.beneficinary_type_v').attr('current_value'),"salary_scale": $(this).find('.salary_scale_v').attr('current_value'),"allow_value": $(this).find('.allow_value_v').attr('current_value'),"currency": $(this).find('.currency_v').attr('current_value')});
                }


            });


            var allow_id = $('#set_po_id').attr('current_po_id');
            var acadimic_year_id = $('#acadimic_year').val();

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/allowence_deduction/save_table_row',
                dataType : 'json',
                type: 'get',
                data: {
                    table_rows:row,
                    allow_id:allow_id,
                    Acadimic_year:acadimic_year_id
                },

                success:function(response) {

                    if(response['error'])
                    {
                        $('.errorMessage').addClass('hide');
                        $.each(response['error'], function (key, val)
                        {
                            //alert(key + val);

                            console.log(key+'_error');
                            $("#"+key+'_error').removeClass('hide').html(val[0]);
                            //console.log(val[0]);

                        })
                        $('#save_details').removeAttr('disabled');
                        $('.disable_on_save').prop('disabled',false);

                    }
                    else{
                        console.log('passed')
                        console.log(response);
                        $('#save_details').removeAttr('disabled');
                        $('.disable_on_save').prop('disabled',false);
                    }


                }

            });

        });

    </script>

    {{----------------  create new allonce ------------------}}
    <script>
        $(document).on('click','#creat_po',function () {

            window.location.href = '/allowence_deduction';

        });
    </script>


    {{---------------  delete allowence ------------------------}}
    <script>
        $(document).on('click','#delete_po',function(){
            var cur_po_id = $('#set_po_id').attr('current_po_id');

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/allowence_deduction/delete/',
                dataType : 'json',
                type: 'get',
                data: {
                    cur_po_id:cur_po_id,
                },

                success:function(response) {

                    window.location.href = '/allowence_deduction';
                }

            });

        });
    </script>

    {{-----------   on leave page -------------------}}
    <script type='text/javascript'>

        $(window).bind('beforeunload', function(){
            if($('#save_po').length){
                return 'Are you sure you want to leave?';
            }

        });


    </script>

    <script>

        $(document).on('click', '#print', function () {

            //  window.location.href = 'get_po2/276'
            var id = $('#set_po_id').attr('current_po_id');
            var route = '/get_allowence_rpt/' + id ;
            /*window.open('/get_allowence_rpt/' + id + '', '_blank');*/
            var acadimic_year_v = $('#acadimic_year').val();
            $('#acadmic_year_print').val(acadimic_year_v);
            $('#print_form').attr('action',route);
            $('#get_pdff').click();


        });


        </script>

@endsection