@extends('layouts.main_app')

@section('main_content')


    <style>


        input.col-md-4.text-center {

            border:0;
            background-color: #fff;
        }
        input[type=number]{
    width: 80px;
}
        .collapse > .row>.col-md-4>.btn, .collapse > .row>.col-md-4>.dropdown>.btn {
    margin-top: 0;
            
}
        .btn{
               padding: 3px 10px;
    margin-top: 7%;
        }
        .third-nav{
            background-color:#eeeeee;
        }
        .approved{
            margin-left: 24%;
        }
        .red-border{
            border: 1px solid red  !important;
        }
    </style>
    <div class="main">

        <!-- Navbar-content -->
        <nav class="navbar navbar-default navbar2">

            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                    <!--breadcrumbs-->
                    <div class="row">
                        <ol class="breadcrumb breadcrumb_nav">
                            <li><a href="/">Home</a></li>
                            <li class="active">Change Password</li>
                        </ol>
                    </div>   <!--End breadcrumbs-->
                        <div class="row">
                            <div class="col-md-4 buttons">
                                <button id="save_user" type="button" class="btn btn-danger " >Save</button>
                            </div>


                           <!--search-->
                           <!--
                            <div class="col-md-4">
                                <form class="navbar-form navbar-right" >
                                    <div class="input-group form-group">
                                        <input type="text" value="" class="form-control" placeholder="Search ...">
                                    </div>
                                </form>
                            </div>
                            -->
                        </div> <!--collapse -->
              
                </div><!--collapse -->

            </div>
        </nav>
        <!-- End Navbar-content -->

        <!-- Success messages -->
        <div class="alert alert-success alert-autocloseable-success" style="display:none;position:absolute;" >
            Your Leave Request Saved Successfully!

        </div>
        <!--end Success messages -->




        <!-- failuer messages -->
        <div class="alert alert-danger errorMessage1" style="display:none;position:absolute;" >
            Your should fill all fields!

        </div>
        <!--end failuer messages -->



        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Change Password</h3>
                    </div>
                    <div class="panel-body">

                        <div id="set_po_id"  current_po_id=""  current_id="{{Auth::user()->id}}"></div>

                        <div class="row">

                            <div class="row">
                            <div class="col-md-12 col-lg-12">
                                <div class="col-md-6 col-lg-6">
                                <div class="form-group">

                                    <label class="col-md-6 col-lg-6 control-label">Old Password</label>
                                    <div class="col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <input id="old_password"  class=" disable-all text-center form-control err_input old_password_error" name="old_password"  type="password" value=""  />
                                        </div>

                                    </div>
                                </div>
                                </div>

                            </div>
                            </div>

                            <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">

                                            <label class="col-md-6 control-label">New Password</label>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input id="password"  class=" disable-all text-center form-control err_input password_error" name="password"  type="password" value=""  />
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                            </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <div class="col-md-6 col-lg-6">
                                        <div class="form-group">

                                            <label class="col-md-6 col-lg-6 control-label">Confirm Password
                                            </label>
                                            <div class="col-md-6 col-lg-6">
                                                <div class="form-group">
                                                    <input id="password_confirmation"  class=" disable-all text-center form-control err_input password_confirmation_error" name="password_confirmation"  type="password" value=""  />
                                            </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                            <br>






                    </div>
                           </div>
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN -->




    </div>
    </div>




@endsection


@section('custom_footer')

    {{--------------------  active side & nav bar ------------------------}}
    <script>
        $('.side_sheets').addClass('hide');
        $('#home').removeClass('hide');

        $("#menu li").removeClass('active');
        $('#home_nav_bar').addClass('active');

        $('#pages ul li a').removeClass('active');
        $('#change_password_side_bar').addClass('active');

    </script>




      <!--edit fields -->


    <script>
        $('.alert-autocloseable-success').hide();

        $(document).on('click','#save_user',function(){

            var old_password = $('#old_password').val();
            var password = $('#password').val();
            var password_confirmation = $('#password_confirmation').val();
            $('#save_user').prop('disabled',true);

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/change_old_pass/',
                dataType : 'json',
                type: 'get',
                data: {

                    old_password:old_password,
                    password:password,
                    password_confirmation:password_confirmation

                },

                success:function(response) {

                    /*console.log(response);*/
                    if(response['error'])
                    {
                        $('.err_input').removeClass('red-border');
                        $('.err_input').parent().find('.select2').removeClass('red-border');
                        $('.errorMessage1').show();
                        $.each(response['error'], function (key, val)
                        {
                            //alert(key + val);

                            console.log(key+'_error');

                            $("."+key+'_error').addClass('red-border');
                            $('.'+key+'_error').parent().find('.select2').addClass('red-border');

                            //console.log(val[0]);

                        });
                    }

                    else
                    {
                        $('#save_user').prop('disabled',false);
                        $('.disable-all').prop('disabled',true);
                        $('.errorMessage1').hide();
                        $('.err_input').removeClass('red-border');
                        $('.err_input').parent().find('.select2').removeClass('red-border');
                        $('#save_user').text("Edit");
                        $('#save_user').attr('id','edit_user');


                        {{----SUCCESS ON SAVE -------}}
                        $('#autoclosable-btn-success').prop("disabled", true);
                        $('.alert-autocloseable-success').show('slow');

                        $('.alert-autocloseable-success').delay(3000).fadeOut( "slow", function() {
                            // Animation complete.
                            $('#autoclosable-btn-success').prop("disabled", false);
                        });
                        {{----SUCCESS ON SAVE -------}}
                    }


                }

            }).done(function () {
                $('#save_user').prop('disabled',false);
            });
        });


        $(document).on('click','#edit_user',function () {
            $(this).text("Save");
            $(this).attr('id','save_user');
            $('.disable-all').prop('disabled',false);
        });
    </script>
 



@endsection