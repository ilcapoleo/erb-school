@extends('layouts.main_app')

@section('main_content')
    <link href="{{asset('css/fastselect.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/select2.min.css')}}" rel="stylesheet" />

    <style>
        input.col-md-4.text-center {

            border:0;
            background-color: #fff;
        }



        .collapse > .row>.col-md-4>.btn, .collapse > .row>.col-md-4>.dropdown>.btn {
            margin-top: 0;

        }



        .third-nav{
            background-color:#eeeeee;
        }





        .myselect2{
            width:171px;
        }

        span.select2.select2-container.select2-container--default{
            width: 100%!important;
        }
        tbody th, tbody td {
            text-align: center;
        }
    </style>
    <div class="main">
        <!-- Navbar-content -->
        <nav class="navbar navbar-default navbar2">

            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                    <!--breadcrumbs-->
                    <div class="row">

                        <div class="col-md-10 col-lg-10">

                            <ol class="breadcrumb breadcrumb_nav">
                                <li><a href="/journal-entry">Journal Entry</a></li>
                                <li class="active"></li>
                            </ol>
                        </div>



                    </div>


                    <!--End breadcrumbs-->
                    <div class="row">
                        <div class="col-md-4 col-lg-4 buttons">
                            <button id="save_work"  class="btn btn-danger " >Save</button>
                        </div><!--collapse -->

                </div><!--collapse -->

              </div>
            </div>
        </nav>
        @if(($errors)!=null)
            @foreach($errors->all() as $error)
                <div class="alert alert-danger errorMessage1">
                    {{ nl2br($error) }}<br>
                </div>
            @endforeach
        @endif
        <div class="alert alert-danger errorMessage2" style="display:none;position:absolute;" >
            PLease fill table!
        </div>
        <!-- End Navbar-content -->
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                <!-- INPUTS -->
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Journal Entry</h3>
                    </div>
                    <div class="panel-body">

                        {!! Form::open(['method'=>'POST' ,'id'=>'theform', 'route'=> 'journal-entry.store','file'=>'true' ,'enctype'=>'multipart/form-data']) !!}

                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="date1" class="col-md-6 col-lg-6 control-label">Date </label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <div class="input-group ">
                                                <input type="text" id="date1" class="form-control col-md-6 col-lg-6 required"  name="date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar">
                                                    </i>
                                                </div>
                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="journal" class="col-md-6 col-lg-6 control-label">Journal </label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <select id="journal" class="myselect1 required"   style="width: 50%" name="journal">

                                                <option>journal-1</option>

                                                <option >journal-2</option>

                                                <option >journal-3</option>

                                                <option >journal-4</option>


                                            </select>
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">

                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="document" class="col-md-6 col-lg-6 control-label">Document </label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <select id="document" class="myselect1 required"   style="width: 50%" name="document">

                                                <option>doc-1</option>

                                                <option >doc-2</option>

                                                <option >doc-3</option>

                                                <option >doc-4</option>


                                            </select>
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="curr" class="col-md-6 col-lg-6 control-label">Currency </label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <select id="curr" class="myselect2 required" disabled  style="width: 50%" name="curr">

                                                <option>$</option>

                                                <option >LE</option>

                                            </select>
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">

                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="rate" class="col-md-6 col-lg-6 control-label">Rate </label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <input  class="form-control " disabled type="text" id="rate" >

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="ref" class="col-md-6 col-lg-6 control-label">Reference No </label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <input  class="form-control "  type="text" id="ref" >

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="details" class="col-md-6 col-lg-6 control-label">Details</label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <textarea class="form-control" rows="3" id="details"></textarea>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                        <div class="row">

                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#details">Details Entry</a></li>


                            </ul>

                            <div class="tab-content">

                                <div id="details" class="tab-pane fade in active">
                                    <br>

                                    <div class="row" >
                                        <table  class="table table-bordred table-striped table-bordered table-striped table-hover table-condensed table-responsive " id="op_table" >
                                            <thead>
                                            <th>Account ID</th>
                                            <th>Account Name</th>
                                            <th>Person Type</th>
                                            <th>Person Name</th>
                                            <th>Cost Center</th>
                                            <th>Value</th>
                                            <th>Dr-Cr</th>
                                            <th>Sub Details</th>
                                            <th>Edit</th>
                                            <th>Delete</th>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                        <div class="row">
                                            <div class="col-md-4 col-lg-4 ">
                                                <button type="button" class="btn btn-primary" id="add_row" >Add Item</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class = 'form-group hidden'>
                            {!! Form::submit('create user',['class'=>'btn btn-primary']) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <br>
                </div>
            </div>
        </div>
        <!-- END INPUTS -->


    </div>

@endsection
@section('custom_footer')
    <script src="{{asset('js/item_name_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/tax_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/select2.min.js')}}"></script>

    <!-----------------active link in nav and active link in sidebar ---->
    <script>
        $('.side_sheets').addClass('hide');
        $('#accounting').removeClass('hide');

        $("#menu li").removeClass('active');
        $('#accounting_nav_bar').addClass('active');

        $('#pages ul li a').removeClass('active');
        $('#journal_entry_side_bar').addClass('active');

    </script>


    <script>
        $(document).on('click', '#save_work', function () {
            var rowCount = $('#op_table tbody tr:not(.added_now)').length;
            if (rowCount == 0) {

                $(".errorMessage2").show();
            }
            else{
                $('#save_work').attr('form', 'theform');
                $('#save_work').attr('type', 'submit');
            }

        });
    </script>


    {{------------  add new row  ------------------------}}
    <script>
        var i = 1;
        $(document).on('click', '#add_row', function () {
            $(this).attr('disabled', true)

            var row = '<tr class= "added_now" >' +

                '<td td_type="search">' +
                '<select class="master_account lo form-control js-example-disabled-results" name="account_name[' + i + ']" required>\n'
                + '<option value="">' + '' + '</option>'
                    {{--@foreach($master_acc as $id=>$role)--}}
                    {{--+ '<option value="{{$id}}">' + '{{$role}}'--}}
                    {{--@endforeach--}}
                // + '</option>'
                + '<option value="1">' + 'Dr' + '</option>'
                + '<option value="2">' + 'Cr' + '</option>'
                + '</select></td>' +

                '<td td_type="search">' +
                '<select class="master_account lo form-control js-example-disabled-results" name="account_name[' + i + ']" required>\n'
                + '<option value="">' + '' + '</option>'
                    {{--@foreach($master_acc as $id=>$role)--}}
                {{--+ '<option value="{{$id}}">' + '{{$role}}'--}}
                    {{--@endforeach--}}
                // + '</option>'
                + '<option value="1">' + 'Dr' + '</option>'
                + '<option value="2">' + 'Cr' + '</option>'
                + '</select></td>' +

                '<td td_type="search">' +
                '<select class="transaction_type lo form-control js-example-disabled-results" name="person_type[' + i + ']" required>\n'
                + '<option value="">' + '' + '</option>'
                + '<option value="1">' + 'Dr' + '</option>'
                + '<option value="2">' + 'Cr' + '</option>'
                + '</select></td>' +

                '<td td_type="search">' +
                '<select class="currency_value lo form-control js-example-disabled-results" name="person_name[' + i + ']" required>\n'
                + '<option value="">' + '' + '</option>'
                    {{--@foreach($currencies as $id=>$role)--}}
                {{--+ '<option value="{{$id}}">' + '{{$role}}'--}}
                    {{--@endforeach--}}
                // + '</option>'
                + '<option value="1">' + 'Dr' + '</option>'
                + '<option value="2">' + 'Cr' + '</option>'
                + '</select></td>'+

                '<td td_type="search">' +
                '<select class="currency_value lo form-control js-example-disabled-results" name="costcenter[' + i + ']" required>\n'
                + '<option value="">' + '' + '</option>'
                    {{--@foreach($currencies as $id=>$role)--}}
                {{--+ '<option value="{{$id}}">' + '{{$role}}'--}}
                    {{--@endforeach--}}
                // + '</option>'
                + '<option value="1">' + 'Dr' + '</option>'
                + '<option value="2">' + 'Cr' + '</option>'
                + '</select></td>' +

                '<td td_type="input"><input class="document_name lo row_data form-control" data-role="input" type="number min="0.000001" onkeypress="return (event.charCode == 8 || event.charCode == 1) ? null : event.charCode >= 48 && event.charCode <= 57 ||  event.charCode == 46" data-role="input" type="number"  placeholder="0.00"  name="value[' + i + ']" required/></td>' +


                '<td td_type="search">' +
                '<select class="currency_value lo form-control js-example-disabled-results" name="dr_cr[' + i + ']" required>\n'
                + '<option value="">' + '' + '</option>'
                    {{--@foreach($currencies as $id=>$role)--}}
                {{--+ '<option value="{{$id}}">' + '{{$role}}'--}}
                    {{--@endforeach--}}
                // + '</option>'
                + '<option value="1">' + 'Dr' + '</option>'
                + '<option value="2">' + 'Cr' + '</option>'
                + '</select></td>' +

                '<td td_type="input"><input class="document_name lo row_data form-control" data-role="input" type="text" name="sub_details[' + i + ']" required/></td>' +


                 '<td>' + '<button type="button" class="set_all_disabled btn btn-primary  btn-xs Save_row">' + 'Save' + '</button>'
                + '</td>'
                + '<td>'
                + '<button type="button" class="set_all_disabled btn btn-danger btn-xs  delete_row"  >' + '<span class="lnr lnr-trash"></span>' + '</button>'
                + '</td>'

                + '</tr>'
            i++;

            $("#op_table").prepend(row);

            $(".js-example-disabled-results").select2();


        });

    </script>

    {{--------   save journal row  --------------}}
    <script>
        $(document).on("click", ".Save_row", function () {

            var empty = false;
            var call_save = false;
            var input = $(this).parents("tr").find('input[type="number"]');
            var selectors = $(this).parents("tr").find('select');


            input.each(function () {
                if (!$(this).val() || $(this).val() == "") {
                    $(this).addClass("error");
                    empty = true;
                } else {
                    $(this).removeClass("error");
                }
            });

            selectors.each(function () {
                if (!$(this).val() || $(this).val() == "") {
                    $(this).addClass("error");
                    empty = true;
                } else {
                    $(this).removeClass("error");
                }
            });

            console.log('empty ' + empty);

            $(this).parents("tr").find(".error").first().focus();


            if (!empty) {

                $(this).prop('disabled', true);
                save_table_effect($(this));
            }


        });

        function save_table_effect(thiss) {

            console.log('passed')

            /*--------------  all select ------------*/
            var select_array = ['supplier_v', 'currency_v'];
            var r = 0;
            var all_dowp_down = thiss.parents("tr").find('select');

            all_dowp_down.each(function () {
                var value_select = $(this).val();
                var text_select = $(this).find('option:selected').text();
                $(this).parent("td").addClass(select_array[r]).attr('current_value', value_select).html(text_select);
                r++;

            });
            /*--------------  all select ------------*/
            var select_array = ['account_id', 'account_name','person_type','person_name','cost_center','dr_cr'];
            var r = 0;
            var all_dowp_down = thiss.parents("tr").find('select');
            all_dowp_down.each(function () {
                var value_select = $(this).val();
                var text_select = $(this).find('option:selected').text();
                $(this).parent("td").addClass(select_array[r]).attr('current_value', value_select).html(text_select);
                r++;

            });
            /*--------- all input  ------------------*/
            var input_array1 = ['value' ,'sub_details'];
            var all_input = thiss.parents("tr").find('input');

            var i = 0;
            all_input.each(function () {

                var value_select = $(this).val();
                $(this).parent("td").addClass(input_array1[i]).attr('current_value', value_select).html(value_select);

                i++;
            });
            /*--------- all input  ------------------*/

            /*--------------------------*/

            if (thiss.parents("tr").hasClass('added_now')) {
                thiss.parents("tr").removeClass('added_now');

            }
            if (thiss.parents("tr").hasClass('edited_now')) {
                thiss.parents("tr").removeClass('edited_now');

            }

            if ($('.added_now').length == 0) {
                $("#add_row").removeAttr("disabled");
            }

            thiss.text('Edit');
            thiss.removeClass('Save_row');
            thiss.addClass('edit_row');
            thiss.prop('disabled', false);
        }
    </script>

    {{----------------------  edit row --------------------}}
    <script>

        $(document).on("click", ".edit_row", function () {
            if (!$('#op_table tr').hasClass('added_now') && !$('#op_table tr').hasClass('edited_now') ) {
                var input_class_array = ['cost_price'];
                var s = 0;

                $('#add_row').prop('disabled', true);
                $(this).parents("tr").addClass('edited_now');

                $(this).parents("tr").find("td:not(:last-child)").each(function () {


                    if ($(this).attr('td_type') == 'input') {
                        if ($(this).hasClass('value')) {
                        $(this).html('<td style="display: block;" td_type="input"><input value = "'+$(this).text() +'" class="'+ input_class_array[s] +' row_data pull-left form-control " min="1.00" onkeypress="return (event.charCode == 8 || event.charCode == 1) ? null : event.charCode >= 48 && event.charCode <= 57 ||  event.charCode == 46" data-role="input" type="number"  placeholder="0" /></td>');
                        s++;
                        }
                        if ($(this).hasClass('sub_details')) {
                        $(this).html('<td style="display: block;" td_type="input"><input value = "'+$(this).text() +'" class="'+ input_class_array[s] +' row_data pull-left form-control " min="1.00" onkeypress="return (event.charCode == 8 || event.charCode == 1) ? null : event.charCode >= 48 && event.charCode <= 57 ||  event.charCode == 46" data-role="input" type="number"  placeholder="0" /></td>');
                        s++;
                        }
                    }



                    else if ($(this).attr('td_type') == 'search') {


                        if ($(this).hasClass('supplier_v')) {
                            $(this).html('<select class="supplier form-control js-example-disabled-results">\n'
                                + '<option value="">' + '' + '</option>'
                                + '</option>'
                                + '</select>');


                            var selected_v = $(this).attr('current_value');
                            $(this).find(".supplier").val(selected_v);
                            var all_selected_v = $('.supplier_v');


                            all_selected_v.each(function () {

                                var selectt = $(this).parents('tr').find('.supplier_v').attr('current_value');
                                if(selected_v != selectt){
                                    console.log('selectt : ' + selectt);
                                    $('.'+selectt).remove();
                                }

                            }) ;

                            $(".supplier").select2();
                            $(".supplier").siblings('.select2').css('width', '71px');
                        }

                        else if ($(this).hasClass('currency_v')) {
                            $(this).html('<select class="currency form-control js-example-disabled-results">\n'
                                + '<option value="">' + '' + '</option>'

                                + '</select>');


                            var selected_v = $(this).attr('current_value');
                            $(this).find(".currency").val(selected_v);
                            $(".currency").select2();
                            $(".currency").siblings('.select2').css('width', '71px');
                        }

                    }


                });

                $("#add_row").attr("disabled", "disabled");
                $(this).text('Save');
                $(this).removeClass('edit_row');
                $(this).addClass('Save_row');

            }
        });

    </script>


    <!--delete row in table -->
    <script>

        $(document).on('click', '.delete_row', function () {

            $(this).hide(function () {
                $(this).closest('td').append('<button class="btn btn-danger set_all_disabled" type="button" id="yes" style="display: none;">yes</button>' +
                    '<button class="btn btn-default set_all_disabled" type="button" id="no" style="display: none;">no</button>');
                $(this).closest('td').find("#yes").show('slow').delay(1000);
                $(this).closest('td').find("#no").show('slow').delay(1000);

            });
        });

        $(document).on('click', '#yes', function () {
            $(this).closest('tr').remove();
            if ($('.added_now').length == 0) {
                $("#add_row").removeAttr("disabled");
            }
        });

        $(document).on('click', '#no', function () {


            $(this).closest('td').find(".delete_row").show('slow').delay(1000);
            $(this).closest('td').find("#yes").hide('slow').delay(1000).remove();
            $(this).closest('td').find("#no").hide('slow').delay(1000).remove();



        });


    </script>





    <!--select salary scale -->

    <script>
        $(".myselect1").select2({
            width: 'resolve'

        });

        $(".myselect2").select2({
            width: 'resolve',


        });
        $(".myselect3").select2({
            width: 'resolve',


        });
        $(".myselect4").select2({
            width: 'resolve',


        });

    </script>


@endsection                                                                                                                                                                                                                                                                                                                                                                                                                                                     