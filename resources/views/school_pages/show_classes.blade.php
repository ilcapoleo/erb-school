
@extends('layouts.main_app')

@section('main_content')
    <style>






        th > a
        {
            color: #676a6d;
        }

        .osb-multisearch > div > div > .panel-default{

            margin-top: 5%;
            margin-bottom: 5%;
        }
        .panel .panel-body{
            padding-top: 0px;
            padding-bottom: 0px;
        }
        ul{
            list-style-type: none;
        }

        .col_op{
            padding-top: 3px;
            padding-bottom: 3px;

        }


        .search-bar {
            bottom: -69px!important;
        }

        /* style dropdown of search */

        .search-bar {
            text-align: center;
            margin: 0;
            padding: 0px 4px 0px 0px;
            list-style: none;
            position:absolute;
            bottom: -100px;
            box-shadow: 0 2px 6px rgba(0, 0, 0, 0.08);
            border-radius: 3px;
            border-color: #ddd;
            z-index: 1;
            width: 99.6667%;
        }
        .search-bar li {
            margin-right: -4px;
            /*padding: 15px 20px;*/
            background: #fff;
            cursor: pointer;
            -webkit-transition: all 0.2s;
            -moz-transition: all 0.2s;
            -ms-transition: all 0.2s;
            -o-transition: all 0.2s;
            transition: all 0.2s;
        }
        .search-bar li:hover , .search-bar li:focus {
            background: #555;
            color: #fff;
        }
        .search-bar  ul {
            padding: 0;
            position: absolute;
            top: 18px;
            left: 0;
            width: 300px;
            -webkit-box-shadow: none;
            -moz-box-shadow: none;
            box-shadow: none;
            display: none;
            opacity: 0;
            visibility: hidden;
            -webkit-transiton: opacity 0.2s;
            -moz-transition: opacity 0.2s;
            -ms-transition: opacity 0.2s;
            -o-transition: opacity 0.2s;
            -transition: opacity 0.2s;
        }
        .search-bar  ul li {
            display: block;
            color: #000;
            padding-bottom: 3px;
        }
        .search-bar li ul li:hover {
            background: #666;
        }
        .search-bar li:hover ul {
            display: block;
            opacity: 1;
            visibility: visible;
        }

        .dropdown-content{
            position: relative;
        }


        #users_data > thead{
            background-color:#ccc;
        }



    </style>

    <div class="main">

        <!-- Navbar-content -->
        <nav class="navbar navbar-default navbar2">

            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                    <div class="row">


                        <div class="col-md-4">
                            <a href="/classes" type="button" class="btn btn-danger ">Create</a>
                        </div>
                        <div class="col-md-4">
                            <div class="dropdown">
                                <button class=" btn btn-primary  dropdown-toggle" type="button" data-toggle="dropdown" id="more"  hidden>More
                                    <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    {{--<li><a href="#" id="delete_all_selected">Delete</a></li>--}}
                                    <li><a href="#" id="print_all_selected">Print</a></li>
                                    <!-- temporery -->            <!--  <li><a href="#" id="Duplicate_all_selected">Duplicate</a></li> -->

                                </ul>
                            </div>
                        </div>


                        <div class="col-md-4">
                            <form action="/get_class" target="_blank" method="post">
                                {{csrf_field()}}
                                <input type="hidden" name="print_ids[]" id="print_array" value="" />

                                <button hidden id="get_pdff" type="submit"> print </button>
                            </form>

                            {{--<div id="name-search">

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="panel panel-default">
                                            <div class="panel-body" data-role="selected-list">
                                                <input class="pull-left" data-role="input" type="text" placeholder="Search..."/>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div data-role="picker" class="panel panel-default">
                                    <div class="list-group" data-role="picker-list">
                                    </div>
                                </div>

                            </div>--}}


                            {{---------------   search----------------------------}}
                            <div id="end-inner">

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="panel panel-default">
                                            <div class="panel-body" data-role="selected-list">
                                                <input id="search_multi"   class="pull-left"   data-role="input" type="text" placeholder="Search..."/>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div data-role="picker" class="panel panel-default">
                                    <div class="list-group" data-role="picker-list">
                                    </div>
                                </div>

                                <div class="spacer">
                                    &nbsp;
                                </div>
                                <div class="container1 dropdown-content" hidden>
                                    <ul class="search-bar">
                                        <li  data-role="col_op" class=" defult_select_op col_op" value="|"  tabindex="1">All</li>
                                        @for($i=0;$i<count($database_search_list);$i++)
                                            <?php $index= $i+2 ?>
                                            <li class="col_op" value="|{{$database_search_list[$i]}}"  tabindex="{{$index}}">{{$showed_search_list[$i]}}</li>
                                            {{--<li class="col_op" value="|s_name"  tabindex="2">name</li>
                                            <li class="col_op" value="|s_class" tabindex="3">class</li>
                                            <li class="col_op" value="|s_age" tabindex="4">age</li>--}}
                                            <?php$index++?>
                                        @endfor
                                    </ul>
                                </div>
                            </div>
                            {{------------------------------------------------}}



                        </div>
                    </div> <!--collapse -->
                </div>
            </div>
        </nav>
        <!-- End Navbar-content -->

        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                <div class="panel table-panel">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-lg-12">
                            <h4>Class Table</h4>
                            <div class="row">
                                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">

                                    <div class="col-md-8 col-lg-8 col-sm-6 col-xs-8 ">
                                        Show
                                        <select id="number_of_rows" class="selectpicker">

                                            <option>5</option>
                                            <option>10</option>
                                            <option>20</option>
                                            <option>30</option>
                                            <option>100</option>

                                        </select>
                                        of Enteries
                                    </div>


                                    <div class="col-md-4 col-lg-4 col-sm-6 col-xs-4">

                                        <div class="col-md-3 col-lg-3 col-sm-3  col-xs-3">
                                                <a href="#" class="paginate prev fa fa-arrow-left " id="previous">  </a>
                                        </div>

                                        <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                                            <p><span id="showed_number_start"></span>-<span id="showed_number_end"></span> Of <span id="total_result"></span></p>
                                        </div>



                                        <div class="col-md-3 col-lg-3 col-sm-3 col-xs-3">

                                                <a href="#" class="paginate next fa fa-arrow-right" id="next">  </a>
                                        </div>

                                    </div>


                                </div>
                            </div>

                            <div class="table-responsive">



                                <table role="table" class="table table-bordred table-striped table-responsive thead-light" id="users_data" >
                                    <thead  class="cf">

                                    {{--<th   class=" smp-not-sortable">
                                        <input type="checkbox" id="checkall" />
                                    </th>--}}

                                    <th  class="numeric"><a href="#" class="sort-by"></a></th>
                                    <th  class="numeric"><a href="#" class="sort-by"></a></th>
                                    <th  class="numeric"><a href="#" class="sort-by"></a></th>
                                    </thead>
                                    <tbody role="rowgroup">



                                    </tbody>

                                </table>

                            </div>





                        </div>
                    </div>

                </div>
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN -->

    </div>
@endsection

@section('custom_footer')
    <script src="{{asset('assets/scripts/smpSortableTable.js')}}"></script>

    {{-----------------  multi search ------------------------------}}
    <script src="{{asset('js/jquery-ui.min.js')}}"></script>

    <script src="{{asset('js/lodash.underscore.min.js')}}"></script>
    <script src="{{asset('js/jqueryui-multisearch_products_table.js')}}"></script>


    {{--------------------  active side & nav bar ------------------------}}
    <script>
        $('.side_sheets').addClass('hide');
        $('#admission').removeClass('hide');

        $("#menu li").removeClass('active');
        $('#admission_nav_bar').addClass('active');

        $('#pages ul li a').removeClass('active');
        $('#classes_side_bar').addClass('active');

    </script>

    <script>
        $(function() {
            $("#end-inner").multisearch({
                source: babyNames,
            });


        });
        var babyNames = [];
    </script>

    <script>


        $("#search_multi").keydown(function(e){
            if (e.keyCode == 40 && $(this).val() != "") {
                $('div.container1').show();
                //$('#colums_v').focus();
                $('div.container1').on('focus', 'li', function() {
                    var $this = $(this);
                    $this.addClass('active').siblings().removeClass('active');
                    $this.attr('data-role',"col_op").siblings().removeAttr('data-role');

                    $this.closest('div.container1').scrollTop($this.index() * $this.outerHeight());
                }).on('keydown', 'li', function(e) {
                    var $this = $(this);
                    if (e.keyCode === 40) {
                        $this.next().focus();
                        return false;
                    } else if (e.keyCode === 38) {
                        $this.prev().focus();
                        return false;
                    }
                }).find('li').first().focus();
                /*$('#colums_v').select2('open');*/
            }

        });

        $("#search_multi").keypress(function (e) {
            if (e.keyCode != 13)
            {
                $('div.container1').show();
            }
        });
        $('.col_op').hover( function() {
            //console.log('hover');
            $(this).attr('data-role',"col_op").siblings().removeAttr('data-role');
        })
        /*$(document).on('click', '.col_op', function() {
            var $this = $(this);
            /!*$this.addClass('active').siblings().removeClass('active');*!/
            $this.attr('id','sel_op').siblings().removeAttr('id');
        });*/
        /*$('.col_op').keypress(function (e) {
            if(e.which == 13) {
                console.log($(this).val());
            }

        })*/
    </script>

    <script>
        $('#search_multi').click(function(){
            myDropDown = $(this).next('.dropdown-content')

            if( myDropDown.is(':visible') ) {
                $(this).removeClass('drop-down-open');
                myDropDown.hide();
            } else {
                myDropDown.fadeIn();
                $(this).addClass('drop-down-open');
            }

            return false;
        });

        $('html').click(function(e) {
            $('.dropdown-content').hide();
        });

        $('.dropdown-content').click(function(e){
            e.stopPropagation();
        });


    </script>


    {{------------------------------------}}

    {{--------------- get table  --------------------------}}
    <script>
        function get_table() {
            var ids = [];
            $('a[data-role="selected-item"]').each(function () {
                var searched= $(this).attr('passed_data');
                ids.push(searched);
            });

            if(ids.length == 0)
            {
                ids.push("all|all");
            }

            console.log(ids);
            console.log('get table');

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/search_classes_table/',
                dataType : 'json',
                type: 'get',
                data: {
                    sudent_data:ids,
                },

                success:function(response) {
                    /* var table = $('#users_data').DataTable();
                     table.destroy();*/
                    console.log(response);
                    $("#users_data tbody").empty();
                    $("#users_data thead tr").empty();
                    $("#users_data tfoot tr").empty();

                    /*console.log(response[0]['s_name'])*/
                    for(var i =0;i<response['colums'].length;i++){
                        $("#users_data thead tr").append(

                            '<th class="numeric" id="'+response['colums'][i]+'"><a href="#" class="sort-by">'+response['showed_colums'][i]+'</a></th>'

                        )

                        //  $("#users_data tfoot tr").append(

                        //    '<td style="font-size: 16px;">'+response['showed_colums'][i]+'</td>'
                        //)

                    }



                    /*************************************************/

                    $("#users_data thead tr").prepend(

                        /*'<th class=" smp-not-sortable">'+*/
                        '<input type="checkbox" id="checkall" style="float: left;' + '    margin-left: 8px;' + '    margin-top: 12px;"/>'
                        /*+ '</th>'*/

                    )

                    /*---------------------------------------------------------*/
                    for(var i =0;i<response['name'].length;i++)
                    {
                        /*------------------- put id of the table ----------------------*/
                        $("#users_data tbody").append('<tr role="row" class="clickable-row" id='+response['name'][i]['classid']+'>');



                        /*-------------- header fo responsive _ table  ---------------*/
                        var array_data = response['showed_colums'];
                        var order=0;
                        for (var y = 0; y < response['colums'].length; y++)
                        {
                            $("#users_data tbody tr:last").append(
                                '<td data-title="'+array_data[order]+'" get_value="'+response['name'][i][response['colums'][y]]+'"  td_type="'+response['colums_type'][y]+'" >' + response['name'][i][response['colums'][y]] + '</td>'
                            )



                            order++;

                        }
                        $("#users_data tbody tr:last").prepend(
                            '<td  data-title="Check">'+
                            '<input type="checkbox" class="checkthis" />'+
                            '</td>'
                        )

                        $("#users_data tbody").append('</tr>');

                    }

                    /*  $('#users_data').DataTable();*/
                    console.log(response);
                    /* $('.pagination').remove();
                     $("#users_data tbody").paginathing({
                         insertAfter: $("#users_data"),
                         perPage: 5,
                     });*/

                }

            }).done(function () {
                sort_table();
                paginate_table();
            });

        }
    </script>
    {{--------------------------------------}}



    <script>

        $('#modules').hide();

        $(".link").click(function() {

            var id = $(this).attr("data-rel");
            $('#side_menu_hide ul').hide();
            $("#" + id).show();
        });
    </script>

    <!--Multisearch -->


    <!--check all in table -->

    <script>

        $(document).on('click','#checkall',function () {
            if ($("#users_data #checkall").is(':checked')) {
                $("#more").removeClass('hide');
                $("#users_data tbody > tr:visible input[type=checkbox]").each(function () {
                    $(this).prop("checked", true);
                });

            } else {
                $("#users_data tbody > tr:visible input[type=checkbox]").each(function () {
                    $(this).prop("checked", false);
                    $("#more").addClass('hide');
                });
            }
            $("[data-toggle=tooltip]").tooltip();

        });

        $(document).on('click','#users_data input[type=checkbox]',function () {
            var checked_count = $('.checkthis:checkbox:checked').length;
            //console.log(checked_count);
            if ($(this).is(":checked")) {
                if(checked_count>0){
                    $("#more").removeClass('hide');
                }
            }
            if (checked_count==0)
            {
                $('#checkall').prop("checked", false);
                $("#more").addClass('hide');

            }
            else {
                if(checked_count == 0)
                {$("#more").addClass('hide');}
            }
        });

        $("#more").addClass('hide');


        /*$("#checkall , #users_data input[type=checkbox]").click(function () {
            if ($(this).is(":checked")) {
                $("#more").removeClass('hide');
            } else {
                $("#more").addClass('hide');
            }
        });*/


        $(document).on('click','#delete_all_selected',function () {

            var all_checked = $('.checkthis:checked');

            var all_ids = [];

            all_checked.each(function () {
                var vall = $(this).closest('tr').attr('id');
                /*console.log($(this).closest('tr').attr('id'))*/
                all_ids.push(vall);

            });

            console.log(all_ids);


            $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/delete_all_selected_po/',
                    dataType : 'json',
                    type: 'get',
                    data: {
                        deleted_user_ids:all_ids,
                    },

                    success:function(response)
                    {
                        console.log(response);
                        for (var i =0; i<response['deleted_ids'].length;i++)
                        {
                            console.log('#'+response['deleted_ids'][i]);
                            if(response['deleted'][i] == 1)
                            {
                                $('#'+response['deleted_ids'][i]).remove();
                            }


                        }
                        $("#users_data tbody > tr:visible input[type=checkbox]").each(function () {
                            $(this).prop("checked", false);
                            $("#more").addClass('hide');
                        });
                        $("#users_data #checkall").prop("checked", false);
                        $('#more').addClass('hide');
                        paginate_table();
                    }
                }
            );

        })



    </script>

    <script>
        $(document).on('click','#users_data tr',function() {
            var href = $(this).attr("id");

            if(!$(event.target).hasClass('checkthis')) {
                if(href) {
                    window.location = '/classes/'+href;
                }        }



        });
    </script>



    {{----------------------  sort table -----------------------------}}
    <script type='text/javascript'>

        function sort_table() {
            // Selectors for future use
            var myTable = "#users_data";
            var myTableBody = myTable + " tbody";
            var myTableRows = myTableBody + " tr";
            var myTableColumn = myTable + " th";

            // Starting table state
            function initTable() {

                // Increment the table width for sort icon support
                $(myTableColumn).each(function () {
                    var width = $(this).width();
                    $(this).width(width + 40);
                });

                // Set the first column as sorted ascending
                $(myTableColumn).eq(0).addClass("sorted-asc");

                //Sort the table using the current sorting order
                sortTable($(myTable), 0, "asc");

            }

            // Table starting state
            initTable();

            // Table sorting function
            function sortTable(table, column, order) {
                var asc = order === 'asc';
                var tbody = table.find('tbody');

                // Sort the table using a custom sorting function by switching
                // the rows order, then append them to the table body
                tbody.find('tr').sort(function (a, b) {
                    if (asc) {
                        return $('td:eq(' + column + ')', a).text()
                            .localeCompare($('td:eq(' + column + ')', b).text());
                    } else {
                        return $('td:eq(' + column + ')', b).text()
                            .localeCompare($('td:eq(' + column + ')', a).text());
                    }
                }).appendTo(tbody);

            }

            // Heading click
            $(myTableColumn).click(function () {

                // Remove the sort classes for all the column, but not the first
                $(myTableColumn).not($(this)).removeClass("sorted-asc sorted-desc");

                // Set or change the sort direction
                if ($(this).hasClass("sorted-asc") || $(this).hasClass("sorted-desc")) {
                    $(this).toggleClass("sorted-asc sorted-desc");
                } else {
                    $(this).addClass("sorted-asc");
                }

                //Sort the table using the current sorting order
                sortTable($(myTable),
                    $(this).index(),
                    $(this).hasClass("sorted-asc") ? "asc" : "desc");

            });

        }

    </script>


    {{-------------------  pagination ---------------------------}}

    <script type='text/javascript'>


        function paginate_table() {



            var number_of_row = $('#number_of_rows').val();
            // Selectors for future use
            var myTable = "#users_data";
            var myTableBody = myTable + " tbody";
            var myTableRows = myTableBody + " tr";
            var myTableColumn = myTable + " th";

            // Starting table state
            function initTable() {
                console.log("initTable")
                $(myTableBody).attr("data-pagesize", number_of_row);
                $(myTableBody).attr("data-firstrecord", 0);
                $('#previous').hide();
                $('#next').show();

                // Increment the table width for sort icon support

                $('#total_result').html($(myTableRows).length);
                $('#showed_number_start').html(1);
                var tableRows = $(myTableRows);
                if(parseInt(tableRows.length, 10) < parseInt(number_of_row, 10))
                {
                    console.log('n_of_r > tablelength');
                    $('#showed_number_end').html(parseInt(tableRows.length, 10));
                }
                else
                {
                    console.log('n_of_r < tablelength');
                    $('#showed_number_end').html(parseInt(number_of_row, 10));

                }
                // Start the pagination
                paginate(parseInt($(myTableBody).attr("data-firstrecord"), 10),
                    parseInt($(myTableBody).attr("data-pagesize"), 10));
            }


            // Table sorting function
            function sortTable(table, column, order) {


            }

            // Heading click
            $(myTableColumn).click(function () {


                // Start the pagination
                paginate(parseInt($(myTableBody).attr("data-firstrecord"), 10),
                    parseInt($(myTableBody).attr("data-pagesize"), 10));
            });

            // Pager click


            // Paging function
            var paginate = function (start, size) {
                console.log("paginate")

                var tableRows = $(myTableRows);
                var end = start + size;
                // Hide all the rows
                tableRows.hide();
                // Show a reduced set of rows using a range of indices.
                tableRows.slice(start, end).show();
                // Show the pager
                $(".paginate").show();
                // If the first row is visible hide prev
                if (tableRows.eq(0).is(":visible")) $('#previous').hide();
                // If the last row is visible hide next
                if (tableRows.eq(tableRows.length - 1).is(":visible")) $('#next').hide();
            }


            // Table starting state
            initTable();


        }


        $(document).on('click','a.paginate',function (e) {

            var myTable = "#users_data";
            var myTableBody = myTable + " tbody";
            var myTableRows = myTableBody + " tr";
            var myTableColumn = myTable + " th";
            var number_of_row = $('#number_of_rows').val();
            /*------- hide all checked  -----*/
            $('#checkall').prop("checked", false);
            $("#users_data tbody > tr:visible input[type=checkbox]").each(function () {
                $(this).prop("checked", false);
                $("#more").addClass('hide');
            });
            /*--------------------------------*/

            e.preventDefault();
            var tableRows = $(myTableRows);
            var tmpRec = parseInt($(myTableBody).attr("data-firstrecord"), 10);

            $('#total_result').html(tableRows.length);

            console.log("tmpRec " + tmpRec )
            console.log("tableRows " + tableRows.length )
            var pageSize = parseInt($(myTableBody).attr("data-pagesize"), 10);
            // number_of_row = pageSize;
            console.log("page_size " + pageSize )
            // console.log("number_of_row " + number_of_row )

            // Define the new first record
            if ($(this).attr("id") == "next") {
                tmpRec += pageSize;
            } else {
                tmpRec -= pageSize;
            }
            // The first record is < of 0 or > of total rows
            if (tmpRec < 0 || tmpRec > tableRows.length) return

            $(myTableBody).attr("data-firstrecord", tmpRec);

            $('#showed_number_start').html(tmpRec+1);
            if(parseInt(tableRows.length, 10) < parseInt(number_of_row, 10))
            {
                console.log('n_of_r > tablelength');
                $('#showed_number_end').html(parseInt(tableRows.length, 10));
            }
            else
            {
                console.log('n_of_r < tablelength');
                if(tmpRec+parseInt(number_of_row, 10) < parseInt(tableRows.length, 10))
                {
                    $('#showed_number_end').html(tmpRec+parseInt(number_of_row, 10));
                }
                else
                {
                    $('#showed_number_end').html(parseInt(tableRows.length, 10));

                }


            }



            paginate(tmpRec, pageSize);
            console.log("pager")
            function paginate (start, size) {
                console.log("paginate")

                var tableRows = $(myTableRows);
                var end = start + size;
                // Hide all the rows
                tableRows.hide();
                // Show a reduced set of rows using a range of indices.
                tableRows.slice(start, end).show();
                // Show the pager
                $(".paginate").show();
                // If the first row is visible hide prev
                if (tableRows.eq(0).is(":visible")) $('#previous').hide();
                // If the last row is visible hide next
                if (tableRows.eq(tableRows.length - 1).is(":visible")) $('#next').hide();
            }

        });

        $(document).on('change','#number_of_rows',function () {
            var number_of_row = $('#number_of_rows').val();

            $('#users_data tbody').attr("data-firstrecord",0);
            $('#users_data tbody').attr("data-pagesize", number_of_row);
            $('#checkall').prop("checked", false);
            $("#users_data tbody > tr:visible input[type=checkbox]").each(function () {
                $(this).prop("checked", false);
                $("#more").addClass('hide');
            });

            paginate_table();

        });
    </script>

    {{-------------------------  print po ---------------------------}}
    <script>
        $(document).on('click','#print_all_selected',function () {
            var all_checked = $('.checkthis:checked');
            var all_ids = [];

            all_checked.each(function () {
                /*var vall = $(this).attr('id');*/
                var vall = $(this).closest('tr').attr('id');
                /*console.log($(this).closest('tr').attr('id'))*/
                all_ids.push(vall);
            });

            console.log('all_ids' + all_ids);
            $('#print_array').val(all_ids);
            $('#get_pdff').click();

        });

    </script>

    <script>
        /*$('.dropdown-toggle').dropdown()*/

        $(document).ready(get_table(),$('div.container1').hide());
        /*$('#colums_v').select2()*/
    </script>

    <script>
        $("table th").addClass("headerSortUp");
        $("table th").addClass("headerSortDown");

    </script>



@endsection
