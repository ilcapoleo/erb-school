@extends('layouts.main_app')

@section('main_content')
    <link href="{{asset('css/fastselect.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/select2.min.css')}}" rel="stylesheet" />
   <style>

        .collapse > .row>.col-md-4>.btn, .collapse > .row>.col-md-4>.dropdown>.btn {
    margin-top: 0;
            
}
        

        
        .third-nav{
            background-color:#eeeeee;
        }


        .material-switch > label::before {
            background: rgb(0, 0, 0);
            box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
            border-radius: 8px;
            content: '';
            height: 16px;
            margin-top: 3px;
            position: absolute;
            opacity: 0.3;
            transition: all 0.4s ease-in-out;
            width: 40px;
            left: -29px;
        }


        .material-switch > label::after {
            background: rgb(255, 255, 255);
            border-radius: 16px;
            box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
            content: '';
            height: 24px;
            left: -45px;
            margin-top: -8px;
            position: absolute;
            top: 6px;
            transition: all 0.3s ease-in-out;
            width: 24px;
        }

        .material-switch > input[type="checkbox"]:checked + label::after {
            background: inherit;
            left: -5px;
        }
        .panel .table > thead > tr > td:first-child, .panel .table > thead > tr > th:first-child, .panel .table > tbody > tr > td:first-child, .panel .table > tbody > tr > th:first-child, .panel .table > tfoot > tr > td:first-child, .panel .table > tfoot > tr > th:first-child{
            padding-left: 0px;
        }

        ul{
            margin: 0;
            padding: 0;
            list-style: none;
        }

        span.select2.select2-container.select2-container--default{
            width: 100%!important;
        }


        .red-border{
            border: 1px solid red;
        }


    </style>
    <div class="main">
        <!-- Navbar-content -->
        <nav class="navbar navbar-default navbar2">

            <div class="container-fluid">


                <div class="">
                    <!--breadcrumbs-->
                    <div class="row">

                        <div class="col-md-10 col-lg-10 col-sm-8 col-xs-8">

                        <ol class="breadcrumb breadcrumb_nav">
                            <li><a href="/borrow_table">borrow</a></li>
                            <li class="active" id="second">

                                @if(isset($master_borrow))
                                    @if($master_borrow->borrow_id != 0)
                                        {{$master_borrow->borrow_id}}
                                    @else
                                        Draft
                                    @endif
                                @endif

                            </li>
                        </ol>
                        </div>


                        @if(isset($master_borrow))
                            <div class="col-md-2 col-lg-2 col-sm-4 col-xs-4 arrow">
                                @if(@\App\Borrowing::where('borrow_id','<',$master_borrow->borrow_id)->orderBy('borrow_id','desc')->first()->borrow_id)
                                    <div class=" col-lg-6  col-md-6 col-sm-6 col-xs-6">
                                        <a href="/borrow/edit/{{@\App\Borrowing::where('borrow_id','<',$master_borrow->borrow_id)->orderBy('borrow_id','desc')->first()->borrow_id}}"><span
                                                    class="glyphicon glyphicon-arrow-left"></span>
                                        </a>
                                    </div>
                                @endif
                                @if(@\App\Borrowing::where('borrow_id','>',$master_borrow->borrow_id)->orderBy('borrow_id','asc')->first()->borrow_id)
                                    <div class=" col-lg-6  col-md-6 col-sm-6 col-xs-6">
                                        <a href="/borrow/edit/{{@\App\Borrowing::where('borrow_id','>',$master_borrow->borrow_id)->orderBy('borrow_id','asc')->first()->borrow_id}}">
                                    <span class="glyphicon glyphicon-arrow-right">
                                </span>
                                        </a>
                                    </div>
                                @endif
                            </div>
                        @endif
                    </div>   <!--End breadcrumbs-->
                    <div class="row">
                        <div class="col-md-4 col-lg-4 col-sm-4 col-xs-8 buttons">
                            @if(isset($master_borrow))
                                @if($master_borrow->status_id == 3 || $master_borrow->status_id == 6)
                                    <button id="edit_po" type="button" class="btn btn-danger " >Edit</button>
                                    <button type="button" class="btn" id="creat_po" >Create</button>
                                @else
                                    {{--<button id="save_po" type="button" class="btn btn-danger " >Save</button>
                                    <button type="button" class="btn hide" id="creat_po" >Create</button>--}}
                                    <button id="edit_po" type="button" class="btn btn-danger " >Edit</button>
                                    <button type="button" class="btn" id="creat_po" >Create</button>
                                @endif
                            @else
                                <button id="save_po" type="button" class="btn btn-danger " >Save</button>
                                <button type="button" class="btn hide" id="creat_po" >Create</button>
                            @endif
                        </div>

                        <div class="col-md-4 col-lg-4 col-sm-4 col-xs-4">
                            @if(isset($master_borrow))

                                @if($master_borrow->status_id == 3 || $master_borrow->status_id == 6|| $master_borrow->status_id == 2)
                                    <div class="dropdown">
                                        <button class="btn btn-primary  dropdown-toggle" type="button" data-toggle="dropdown" id="po_more"  hidden>More
                                            <span class="caret"></span></button>
                                        <ul class="dropdown-menu">

                                            <li class="hide"><a href="#" id="delete_po">Delete</a></li>
                                            <!-- temporery --> {{--<li><a href="#">Export</a></li>--}}
                                            {{--<li><a href="#" id="dublicate_po">Duplicate</a></li>--}}
                                            <li><a href="#" id="print">Print</a></li>

                                        </ul>
                                    </div>
                                    @elseif($master_borrow->status_id == 1)
                                    <div class="dropdown">
                                        <button class="btn btn-primary  dropdown-toggle" type="button" data-toggle="dropdown" id="po_more"  hidden>More
                                            <span class="caret"></span></button>
                                        <ul class="dropdown-menu">

                                            <li class="hide"><a href="#" id="delete_po">Delete</a></li>
                                            <!-- temporery --> {{--<li><a href="#">Export</a></li>--}}
                                            {{--<li><a href="#" id="dublicate_po">Duplicate</a></li>--}}
                                            <li><a href="#" id="print">Print</a></li>

                                        </ul>
                                    </div>
                                @else
                                    <div class="dropdown">
                                        <button class="hide btn btn-primary  dropdown-toggle" type="button" data-toggle="dropdown" id="po_more"  hidden>More
                                            <span class="caret"></span></button>
                                        <ul class="dropdown-menu">

                                            <li class=""><a href="#" id="delete_po">Delete</a></li>
                                            <!-- temporery --> {{--<li><a href="#">Export</a></li>--}}
                                            {{--<li><a href="#" id="dublicate_po">Duplicate</a></li>--}}
                                            <li><a href="#" id="print">Print</a></li>

                                        </ul>
                                    </div>
                                @endif
                            @else
                                <div class="dropdown">
                                    <button class="hide btn btn-primary  dropdown-toggle" type="button" data-toggle="dropdown" id="po_more"  hidden>More
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">

                                        <li class="hide"><a href="#" id="delete_po">Delete</a></li>
                                        <!-- temporery -->       {{--<li><a href="#">Export</a></li>--}}
                                        {{--<li><a href="#" id="dublicate_po">Duplicate</a></li>--}}
                                        <li><a href="#" id="print">Print</a></li>

                                    </ul>
                                </div>
                            @endif
                        </div>
                        <!--search-->
                        <!--
                         <div class="col-md-4">
                             <form class="navbar-form navbar-right" >
                                 <div class="input-group form-group">
                                     <input type="text" value="" class="form-control" placeholder="Search ...">
                                 </div>
                             </form>
                         </div>
                         -->
                    </div> <!--collapse -->
              
                </div><!--collapse -->
                
                 <!--buttons row +second breadcrumb-->



                <div class="row third-nav">


                    <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">

                        <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12 ">
                            <button  id="send_mail" type="button" class="{{--set_all_disabled--}} btn btn_third_nav" >Send by Mail</button>
                        </div>

                        <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                            <button id="sendto_approved_btn" type="button" class="set_all_disabled btn btn_third_nav" @if(isset($master_borrow)) disabled @endif >To Approve</button>
                        </div>

                        <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                            <button id="approved_btn" type="button" class="set_all_disabled btn approved btn_third_nav " @if(isset($master_borrow)) disabled @endif >Approve</button>
                        </div>

                        <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                            @if(isset($master_borrow))
                                <button id="refused_btn" type="button" class="set_all_disabled btn approved btn_third_nav @if($master_borrow->status_id == 1) hide @endif "  disabled  >Refuse</button>
                            @else
                                <button id="refused_btn" type="button" class="set_all_disabled btn approved btn_third_nav hide " >Refuse</button>
                            @endif
                        </div>

                    </div>


                    <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">


                            <span id="current_choosed_status" coosed_status="" class="breadcrumbs ">
                                 @if(isset($master_borrow))

                                    <span id="draft_po"  class="@if($master_borrow->status_id == 1) po-active @endif breadcrumb">Draft </span></li>
                                    <span id="to_approve_po"  class="@if($master_borrow->status_id == 2) po-active @endif breadcrumb">To Approved </span></li>
                                    <span id="approved_po"  class="@if($master_borrow->status_id == 3) po-active @endif  breadcrumb ">Approved </span></li>
                                    <span id="refused"  class="@if($master_borrow->status_id == 6) po-active @elseif($master_borrow->status_id== 1) hide @endif  breadcrumb ">Refused</span></li>

                                @else
                                    <span id="draft_po"  class="breadcrumb">Draft </span></li>
                                    <span id="to_approve_po"  class="breadcrumb">To Approved </span></li>
                                    <span id="approved_po"  class="breadcrumb ">Approved </span></li>
                                    <span id="refused" class="hide breadcrumb">Refused</span>
                                @endif
                            </span>



                    </div>



                </div>
            </div>
        </nav>
        <!-- End Navbar-content -->



        <!-- Success messages -->
        <div class="alert alert-success alert-autocloseable-success" style="display:none;position:absolute;" >
            Your Borrow Saved Successfully!

        </div>
        <!--end Success messages -->

        <!-- failuer messages -->
        <div class="alert alert-danger errorMessage1" style="display:none;position:absolute;" >
            Your should fill all fields!

        </div>
        <!--end failuer messages -->


        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Borrowings</h3>
                    </div>
                    <div class="panel-body">

                        
                        <div class="row">

                            @if(isset($master_borrow))
                                <div id="set_po_id"  current_po_id="{{$master_borrow->borrow_id}}" current_po_status="{{$master_borrow->status_name}}" closed_or_not="" current_id="{{Auth::user()->id}}"></div>
                            @else
                                <div id="set_po_id"  current_po_id="" current_po_status="draft_po" closed_or_not="0" current_id="{{Auth::user()->id}}"></div>
                            @endif

                                <ul class="nav nav-tabs">
                                    <li id="borrow_section" class="active"><a href="#borrowing">Borrowing</a></li>
                                    @if(isset($master_borrow))
                                        <li id="deduction_section" class="@if($master_borrow->status_id != 3) hide @endif"><a href="#deduction_information">Deduction Information</a></li>
                                    @else
                                        <li id="deduction_section" class="hide "><a href="#deduction_information">Deduction Information</a></li>
                                    @endif

                                </ul>

                                <div class="tab-content">
                                    <div id="borrowing" class="tab-pane fade in active table-responsive">

                                        <div class="row">
                                            <div class="col-md-6 col-lg-6">
                                                <div class="form-group">

                                                <label class="col-md-6 control-label" >Name</label>



                                            <div class="col-md-6">
                                                <select id="employee" name="employee" class="get_all_selectors set_all_disabled select_2_enable form-control err_input Name_error required" @if(isset($master_borrow)) disabled @endif >
                                                    <option value=""> </option>
                                                    @foreach($emploee as $s_emploee)
                                                        @if(isset($master_borrow))
                                                            @if($master_borrow->employe_id == $s_emploee->employee_id)
                                                                <option selected value="{{$s_emploee->employee_id}}">{{$s_emploee->employee_name}}</option>
                                                            @else
                                                                <option value="{{$s_emploee->employee_id}}">{{$s_emploee->employee_name}}</option>
                                                            @endif
                                                        @else
                                                            @if($s_emploee->employee_user_id == Auth::user()->id)
                                                                <option selected value="{{$s_emploee->employee_id}}">{{$s_emploee->employee_name}}</option>
                                                            @else
                                                                <option value="{{$s_emploee->employee_id}}">{{$s_emploee->employee_name}}</option>
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                            </div>
                                            </div>
                                        </div>

                                        <br>

                                        <div class="row">


                                            <div class="col-md-6 col-lg-6">
                                                <div class="form-group">
                                                        <label for="number" class="col-md-6 col-lg-6 control-label">Value</label>

                                                    <div class=" col-md-6 col-lg-6 ">
                                                        @if(isset($master_borrow))
                                                            <input id="borrow_value" class="Value_error set_all_disabled get_all_date_input row_data borrow_value form-control err_input required"  min="1"    type="number" value="{{$master_borrow->value}}" disabled >
                                                        @else
                                                            <input id="borrow_value" class="Value_error set_all_disabled get_all_date_input row_data borrow_value form-control err_input required"  min="1"    type="number" value="">
                                                        @endif

                                                    </div>

                                                </div>

                                            </div>



                                        </div>


                                        <br>

                                        <div class="row">


                                            <div class="col-md-6 col-lg-6">
                                                <div class="form-group">

                                                    <label for="number" class="col-md-6 col-lg-6 control-label">Currency</label>

                                                    <div class="col-md-6">
                                                        <select name="currency_value" id="currency_value" class="Currency_error err_input required get_all_selectors set_all_disabled select_2_enable form-control" @if(isset($master_borrow)) disabled @endif >
                                                            <option value=""> </option>
                                                            @foreach($currencies as $id=>$role)
                                                                @if(isset($master_borrow))
                                                                    @if($master_borrow->currency_id == $id)
                                                                        <option selected value="{{$id}}">{{$role}}</option>
                                                                    @else
                                                                        <option value="{{$id}}">{{$role}}</option>
                                                                    @endif
                                                                @else
                                                                    <option value="{{$id}}">{{$role}}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>

                                                </div>
                                                </div>

                                            </div>



                                        </div>
                                         <br>
                                         <div class="row">
                                             <div class="col-md-6 col-lg-6">

                                                                     <div class="form-group">
                                                                         <label class=" control-label col-md-6 col-lg-6 " for="datetime1">Date</label>

                                                                         <div  class=" col-md-6 col-lg-6">
                                                                             <div class="form-group">
                                                                         <div class="input-group ">
                                                                             @if(isset($master_borrow))
                                                                                 <input type="text" id="borrow_date" class="Date_error required date_time_picker set_all_disabled  all_date form-control err_input " crrent_date="" name="date" value="{{$master_borrow->date}}" @if(isset($master_borrow)) disabled @endif >
                                                                             @else
                                                                                 <input type="text" id="borrow_date" class="Date_error required date_time_picker set_all_disabled all_date form-control err_input" crrent_date="" name="date" value="">
                                                                             @endif

                                                                             <div class="input-group-addon">
                                                                                 <i class="fa fa-calendar">
                                                                                 </i>
                                                                             </div>
                                                                         </div>
                                                                             </div>
                                                                         </div>
                                                                     </div>

                                             </div>



                                         </div>

                                        <div class="row">
                                            <div class="col-md-6 col-lg-6">
                                                <div class="form-group">

                                                    <label class="col-md-6 control-label" >Rate</label>



                                                    <div  id="rate_value" rate_value="" class="col-md-6">

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>


                                    @if(isset($master_borrow))
                                        @if($master_borrow->status_id =3)

                                    <div id="deduction_information" class="tab-pane fade in ">
                                        <div class="row">
                                            <div class="col-md-12 col-lg-12">
                                                <div class="form-group">

                                                    <label class="col-md-2 control-label" >Deduction Type</label>



                                                    <div class="col-md-2 col-lg-2">
                                                        <select id="deduction_type" name="deduction_type" class="validate_tab get_all_selectors set_all_disabled select_2_enable form-control required"  @if(isset($master_borrow))@if($master_borrow->status_id == 3 || $master_borrow->status_id == 6 ) disabled @endif @endif>
                                                            <option value=""> </option>
                                                            @foreach($deduction as $id=>$role)
                                                                @if(isset($master_borrow))
                                                                    @if($master_borrow->deduction_id == $id)
                                                                        <option selected value="{{$id}}">{{$role}}</option>
                                                                    @else
                                                                        <option value="{{$id}}">{{$role}}</option>
                                                                    @endif
                                                                @else
                                                                    <option value="{{$id}}">{{$role}}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>

                                                    </div>



                                                    <label class=" control-label col-md-2 col-lg-2 " for="datetime1" >Date</label>
                                                    <div class="input-group col-md-2 col-lg-2">
                                                        @if(isset($master_borrow))
                                                            <input type="text" id="deliver_date" class="validate_tab date_time_picker set_all_disabled  all_date form-control required " crrent_date="" name="date" value="{{$master_borrow->deliver_date}}" @if(isset($master_borrow))@if( $master_borrow->status_id == 3 || $master_borrow->status_id == 6 ) disabled @endif @endif>
                                                        @else
                                                            <input type="text" id="deliver_date" class="validate_tab date_time_picker set_all_disabled all_date form-control required " crrent_date="" name="date" value="">
                                                        @endif

                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar">
                                                            </i>
                                                        </div>
                                                </div>


                                                    <label class="col-md-2 control-label" >Acadimic Year</label>



                                                    <div class="col-md-2 col-lg-2">
                                                        <select id="acadimic_year" name="acadimic_year" class="validate_tab get_all_selectors set_all_disabled select_2_enable form-control"  @if(isset($master_borrow))@if($master_borrow->status_id == 3 || $master_borrow->status_id == 6 ) disabled @endif @endif>
                                                            <option value=""> </option>
                                                            @foreach($acadimic_year as $id=>$role)
                                                                @if(isset($master_borrow))
                                                                    @if($master_borrow->accadimic_year_id == $id)
                                                                        <option selected value="{{$id}}">{{$role}}</option>
                                                                    @else
                                                                        <option value="{{$id}}">{{$role}}</option>
                                                                    @endif
                                                                @else
                                                                    <option value="{{$id}}">{{$role}}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>

                                                    </div>
                                            </div>
                                        </div>

                                        <br>
                                            <br>

                                        <table  class="table table-bordred table-striped table-bordered table-striped table-hover table-condensed table-responsive text-center" id="op_table" >
                                            <thead>
                                            <th>Months</th>
                                            <th>Value</th>
                                            <th>Paid</th>
                                            <th>Edit</th>
                                            <th>Delete</th>

                                            </thead>
                                            <tbody>

                                            @if(isset($borrow_details))
                                                @foreach($borrow_details as $borrow_details_single)
                                                <tr class="" paid="{{$borrow_details_single->paid}}">
                                                    <td td_type="search" data-select2-id="24" class="month_value_v" current_value="{{$borrow_details_single->month_id}}">{{$borrow_details_single->month_name}}</td>
                                                    <td td_type="input" class="borrow_value_v" current_value="{{$borrow_details_single->value_ber_month}}">{{$borrow_details_single->value_ber_month}}</td>

                                                    <td td_type="" class="paid" current_value="0" yes_or_no="No">
                                                        @if($borrow_details_single->paid == 1)Yes @else No @endif</td>

                                                    <td><button class="set_all_disabled btn btn-primary btn-xs table_btn edit_row" @if(isset($master_borrow))@if($master_borrow->status_id == 3 || $master_borrow->status_id == 6 || $borrow_details_single->paid == 1 ) disabled @endif @endif>Edit</button></td>
                                                    <td><button class="set_all_disabled btn btn-danger btn-xs table_btn delete_row" @if(isset($master_borrow))@if($master_borrow->status_id == 3 || $master_borrow->status_id == 6 || $borrow_details_single->paid == 1 ) disabled @endif @endif>
                                                            <span class="lnr lnr-trash"></span>
                                                        </button>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            @endif

                                            </tbody>
                                        </table>

                                        <hr>

                                        <div class="row">

                                            <div class="col-md-2 col-lg-2 ">

                                            </div>

                                            <div class="col-md-8  col-lg-8">

                                                <div class="col-md-12  col-lg-12">
                                                    <div class="col-md-2 col-lg-2">Total:</div>


                                                        <div  id="total_borrow" class="col-md-4 col-lg-4" >0.00 </div><span class="currency_value"></span>

                                                </div>




                                            </div>

                                        </div>








                                        <div class="row">
                                            <div class="col-md-4 col-lg-4">
                                                <button type="button" class=" set_all_disabled btn btn-primary" id="add_row" @if(isset($master_borrow))@if($master_borrow->status_id == 3 || $master_borrow->status_id == 6 ) disabled @endif @endif disabled>Add Item</button>
                                            </div>
                                        </div>





                                </div>
                            </div>
                            @endif
                        @endif
                    </div>
                        
                        
                        
                        
                        
                    </div>
                 </div>
            <!-- END MAIN CONTENT -->
            </div>
        <!-- END MAIN -->
    </div>
    </div>

@endsection


@section('custom_footer')

    <script src="{{asset('js/item_name_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/tax_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/select2.min.js')}}"></script>

    <script src="{{asset('assets/scripts/bootstrap-datetimepicker.min.js')}}"></script>


            {{--------------------  active side & nav bar ------------------------}}
            <script>
                $('.side_sheets').addClass('hide');
                $('#HR').removeClass('hide');

                $("#menu li").removeClass('active');
                $('#hr_nav_bar').addClass('active');

                $('#pages ul li a').removeClass('active');
                $('#borrow_side_bar').addClass('active');

            </script>
<!--date/time picker-->
    <script>
        $('#borrow_date').datetimepicker({format: 'yyyy-mm-dd',autoclose: true, minView: 2});
        $('#deliver_date').datetimepicker({format: 'yyyy-mm-dd',autoclose: true, minView: 2});
        $(".select_2_enable").select2();
    </script>

    {{------------  add new row  ------------------------}}
    <script>
        $(document).on('click','#add_row',function () {

            $(this).attr('disabled',true);
            var row = '<tr class= "added_now" paid="0">' +

                '<td td_type="search">' +
                '<select class="month_value form-control js-example-disabled-results">\n' +
                '</select></td>' +
                '<td td_type="input"><input class="borrow_value row_data form-control" min="1"  data-role="input" type="number"  /></td>'+
                '<td td_type="" class="paid" current_value="0" yes_or_no="No" >No</td>'
                +'<td>'+'<button class="set_all_disabled btn btn-primary btn-xs Save_row">'+'Save'+'</button>'
                +'</td>'
                +'<td>'
                +'<button class="set_all_disabled btn btn-danger btn-xs delete_row"  >'+'<span class="lnr lnr-trash"></span>'+'</button>'
                +'</td>'
                +'</tr>';
            $("#op_table").append(row);
            get_months_drop_down($('.month_value'));
            $(".month_value").select2();
            $(".month_value").siblings('.select2').css('width', '71px');

        });


    </script>

    {{------------- limit the max of input number  ----------------}}
    <script>
        $(document).on('change','.borrow_value',function() {
            var max = parseInt($(this).attr('max'));
            var min = parseInt($(this).attr('min'));
            if ($(this).val() > max)
            {
                $(this).val(max);
            }
            else if ($(this).val() < min)
            {
                $(this).val(min);
            }
        });
    </script>


    {{---------------- get total quantity ----------------------}}

    <script>
        function get_totals() {
            var input = $('#op_table > tbody  > tr');
            var total_quantity = 0;

            input.each(function () {
                if (!$(this).hasClass('added_now')) {
                    total_quantity += parseFloat($(this).find('.borrow_value_v').attr('current_value'));
                }


            });

            $('#total_borrow').html(total_quantity);

        }

    </script>

    {{------------------------ get months -------------------------------}}
    <script>

        function get_months_drop_down($dropdown)
        {

            var input = $('#op_table > tbody  > tr');
            var all_selected_months = [];

            input.each(function(){

                if(!$(this).hasClass('added_now'))
                {
                    all_selected_months.push($(this).find('.month_value_v').attr('current_value'));
                }


            });

            var current_selected_value = $dropdown.parents("tr").find('.month_value_v').attr('current_value');

            //var $dropdown = $("#doc_type");

            /*$dropdown.find('option').remove();*/

            console.log('current_selected_value'+current_selected_value);

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/borrow/get_months_drop_down/',
                dataType : 'json',
                type: 'get',
                data: {
                    all_selected_months:all_selected_months,
                    current_selected_value:current_selected_value
                },

                success:function(response) {

                    console.log(response);

                    var selected_val = $dropdown.parents('td').attr('current_value');

                    console.log('selected drop '+selected_val);

                    $dropdown.find('option').remove();

                    for(var i =0;i<response['data'].length;i++)
                    {
                        $dropdown.append($("<option />").val(response['data'][i]['month_id']).text(response['data'][i]['month_name']));
                    }

                    for(var i =0;i<response['extra_v'].length;i++)
                    {
                        $dropdown.append($("<option />").val(response['extra_v'][i]['month_id']).text(response['extra_v'][i]['month_name']));
                    }
                    $dropdown.val(selected_val);
/*
                    $(".item_product").select2();
                    $(".item_product").siblings('.select2').css('width', '71px');*/
                }

            });

        }

    </script>


    {{--------------  save row  ----------------------}}

    <script>
        $(document).on("click", ".Save_row", function(){

            var empty = false;
            var input = $(this).parents("tr").find('input[type="text"],input[type="number"]');

            if(!$(this).parents("tr").find('.month_value').val() || $(this).parents("tr").find('.month_value').val()=="" )
            {
                empty = true;
            }

            input.each(function(){
                if(!$(this).val()|| $(this).val()==""){
                    $(this).addClass("error");
                    empty = true;
                } else{
                    $(this).removeClass("error");
                }
            });

            console.log('empty '+empty);

            $(this).parents("tr").find(".error").first().focus();

            if(!empty ) {
                $(this).attr('disabled','disabled');
                ($(this)).removeAttr('disabled');
                $(this).prop('disabled',true);
                save_table_effect($(this));
            }


        });

        function save_table_effect(thiss) {
            console.log('passed')


            /*--------- select  po ------------------*/
            var select_po_drop_down = thiss.parents("tr").find('select.month_value');
            var value_select = select_po_drop_down.val();
            var text_select = thiss.parents("tr").find('select.month_value option:selected').text();

            select_po_drop_down.parent("td").addClass('month_value_v').attr('current_value', value_select).html(text_select);
            /*--------------------------*/

            /*--------- input quantity ------------------*/
            var quantity = thiss.parents("tr").find('.borrow_value');
            var quantity_value = quantity.val();


            console.log('borrow_value' +quantity_value)
            quantity.parent("td").addClass('borrow_value_v').attr('current_value', quantity_value).html(quantity_value);
            /*--------------------------*/

            //paid
            var check_box = thiss.parents("tr").find('input.paid');
            var check_val = check_box.parents("td").attr('current_value');
            var check_text_val = check_box.parents("td").attr('yes_or_no');

            check_box.parents("td").addClass('include_check_v').addClass('check_box_v').html(check_text_val);

            if (thiss.parents("tr").hasClass('added_now')) {
                thiss.parents("tr").removeClass('added_now');

            }
            if (thiss.parents("tr").hasClass('edited_now')) {
                thiss.parents("tr").removeClass('edited_now');

            }

            //|| thiss.siblings().find('.create_edit_v') ){
            //console.log(thiss.closest('tr').attr('id'))


            if ($('.added_now').length == 0) {
                $("#add_row").removeAttr("disabled");
            }

            thiss.text('Edit');
            thiss.removeClass('Save_row');
            thiss.addClass('edit_row');

            get_totals();
            thiss.prop('disabled',false);
        }


    </script>

    {{----------------------  edit row --------------------}}
    <script>
        $(document).on("click", ".edit_row", function(){
            if(!$('#op_table tr').hasClass('added_now'))
            {


                $('#add_row').prop('disabled',true);
                $(this).parents("tr").addClass('edited_now');

                $(this).parents("tr").find("td:not(:last-child)").each(function(){
                    if($(this).attr('td_type') == 'input')
                    {
                        $(this).html('<input class="borrow_value row_data form-control" min="1"  data-role="input" type="number"  value="'+$(this).text()+'" />');

                    }

                    else if ($(this).attr('td_type') == 'search')
                    {
                        if($(this).hasClass('month_value_v'))
                        {
                            $(this).html('<select class="month_value form-control js-example-disabled-results">\n' +
                                '</select>');
                            get_months_drop_down($(this).find('.month_value'));
                            var selected_v = $(this).attr('current_value');
                            $(this).find(".month_value").val(selected_v);
                            $(".month_value").select2();
                            $(".month_value").siblings('.select2').css('width', '71px');
                        }

                    }


                });

                $("#add_row").attr("disabled", "disabled");
                $(this).text('Save');
                $(this).removeClass('edit_row');
                $(this).addClass('Save_row');
                get_totals();
            }
        });

    </script>

    {{-------------  delete table row  --------------------}}
    <script>

        $(document).on('click', '.delete_row', function () {

            $(this).closest('tr').remove();
            var rowCount = $('#op_table tr').length;
            console.log('rowCount'+ rowCount)
            if(rowCount == 1)
            {
                $('.validate_tab').prop('disabled',false);
                $('#add_row').prop('disabled',true);

            }
            else
            {
                $('.validate_tab').prop('disabled',true);
                $('#add_row').prop('disabled',false);
            }
            get_totals();
            if($('.added_now').length == 0)
            {
                $("#add_row").removeAttr("disabled");
            }
        });

    </script>


    {{---------------  active add row  on supplier change --------------------}}
    <script>
        $(document).on('change','.validate_tab',function () {
            enable_disable_add();
        });

        function enable_disable_add(){
            var isValid;
            isValid = true;
            $(".validate_tab").each(function() {
                var element = $(this);
                if (element.val() == "") {
                    isValid = false;
                $('.validate_tab').prop('disabled',false);
                }

            });

            console.log('isValid'+isValid);

            if(isValid /*&& $('#set_po_id').attr('current_po_status') != 'approved_po'*/&& $('#set_po_id').attr('current_po_status') != 'refused' )
            {

                $('#add_row').prop('disabled',false);
            }

            else{
                $('#add_row').prop('disabled',true);
            }

        }

        $(document).on('click','#add_row',function()
        {
            $('.validate_tab').prop('disabled',true);
        });

    </script>


            {{---------------------------  save data  ------------------------------------------}}
            <script>
                $('.alert-autocloseable-success').hide();


                $(document).on('click','#save_po',function () {
                    var status_po = $('#set_po_id').attr('current_po_status');
                    add_new_po_all (status_po);




                });
                $(document).on('click','#sendto_approved_btn',function () {

                    //add_new_po_all ('to_approve_po');
                    var status_po = $('#set_po_id').attr('current_po_status');
                    if(status_po != 'approved_po')
                    {
                        add_new_po_all ('to_approve_po');
                    }

                    else{
                        add_new_po_all ('approved_po');
                    }

                });
                $(document).on('click','#approved_btn',function () {

                    add_new_po_all ('approved_po');


                });
                $(document).on('click','#refused_btn',function () {

                    add_new_po_all ('refused');


                });



                function add_new_po_all (status_click) {

                    if (!$('#op_table tr').hasClass('edited_now')) {

                        $('#save_po').attr('disabled', 'disabled');
                        $('#sendto_approved_btn').attr('disabled', 'disabled');
                        $('#approved_btn').attr('disabled', 'disabled');


                        var status = status_click;
                        console.log(status);


                        var employee = $('#employee').val();
                        var borrow_value = $('#borrow_value').val();
                        var currency = $('#currency_value').val();
                        var borrow_date = $('#borrow_date').val();
                        var rate_value = $('#rate_value').attr('rate_value');

                        var deduction_type = $('#deduction_type').val();
                        var acadimic_year = $('#acadimic_year').val();
                        var deliver_date = $('#deliver_date').val();


                        var current_user_id = $('#set_po_id').attr('current_id');
                        var current_po_id = $('#set_po_id').attr('current_po_id');

                        console.log('current_po_id' + current_po_id);

                        var input = $('#op_table > tbody  > tr');
                        var row = [];

                        input.each(function () {

                            if (!$(this).hasClass('added_now') && $(this).attr('paid') == 0) {
                                row.push({
                                    "month_id": $(this).find('.month_value_v').attr('current_value'),
                                    "borrow_value": $(this).find('.borrow_value_v').attr('current_value')
                                });
                            }


                        });


                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url: '/borrow/save/',
                            dataType: 'json',
                            type: 'get',
                            data: {


                                Name: employee,
                                Value: borrow_value,
                                Currency: currency,
                                Date: borrow_date,
                                rate_value: rate_value,

                                deduction_type: deduction_type,
                                acadimic_year: acadimic_year,
                                deliver_date: deliver_date,
                                table_rows: row,
                                status: status,
                                current_po_id: current_po_id,
                                cur_user_id: current_user_id,

                            },

                            success: function (response) {

                                console.log(response);
                                if (response['error']) {
                                    $('.err_input').removeClass('red-border');
                                    $('.err_input').parent().find('.select2').removeClass('red-border');

                                    $('.errorMessage1').show();


                                    $.each(response['error'], function (key, val) {
                                        //alert(key + val);

                                        console.log(key + '_error');
                                        /*$('.err_input').addClass('red-border');
                                        $('.err_input + .select2.select2-container.select2-container--default').addClass('red-border');*/
                                        //$("#"+key+'_error').removeClass('hide').html(val[0]);

                                        $("."+key+'_error').addClass('red-border');
                                        $('.'+key+'_error').parent().find('.select2').addClass('red-border');
                                        //console.log(val[0]);

                                    })
                                    $('#save_po').removeAttr('disabled');

                                    $('#sendto_approved_btn').removeAttr('disabled');
                                    $('#approved_btn').removeAttr('disabled');
                                }
                                else {
                                    var status_po = $('#set_po_id').attr('current_po_status');
                                    if (status_po != 'approved_po' && status_click != 'refused') {
                                        $('#set_po_id').attr('current_po_status', status_click);






                                    }
                                    else if (status_click == 'refused') {
                                        $('#set_po_id').attr('current_po_status', status_click);

                                    }

                                    $('.errorMessage1').hide();
                                    $('.err_input').removeClass('red-border');
                                    $('.err_input').parent().find('.select2').removeClass('red-border');


                                    $('.breadcrumb').removeClass('po-active');
                                    $('#' + response['status']).addClass('po-active');
                                    $('#current_choosed_status').attr('coosed_status', response['status']);
                                    $('#save_po').text("Edit");
                                    $('#save_po').attr('id', 'edit_po');
                                    $('#set_po_id').attr('current_po_id', response['current_po_id']);
                                    $('.set_all_disabled').prop('disabled', true);
                                    $('#creat_po').removeClass('hide');

                                    $('#po_more').removeClass('hide');
                                    $('#delete_po').parent('li').addClass('hide');

                                    if ($('#set_po_id').attr('current_po_status') == 'draft_po') {
                                        $('#delete_po').parent('li').removeClass('hide');
                                    }
                                    if ($('#set_po_id').attr('current_po_status') == 'approved_po') {
                                        /*$('#approved_date').val(response['approv_date']['date']);*/
                                        $('#refused').removeClass('hide');
                                        $('#refused_btn').removeClass('hide');
                                        /*$('#deduction_section').removeClass('hide');*/

                                    }

                                    if ($('#set_po_id').attr('current_po_status') == 'to_approve_po') {

                                        $('#refused').removeClass('hide');
                                        $('#refused_btn').removeClass('hide');
                                    }


                                    $('#edit_po').removeAttr('disabled');


                                    //po-active
                                    console.log(response);

                                    {{----SUCCESS ON SAVE -------}}
                                    $('#autoclosable-btn-success').prop("disabled", true);
                                    $('.alert-autocloseable-success').show("slow");

                                    $('.alert-autocloseable-success').delay(3000).fadeOut( "slow", function() {
                                        // Animation complete.
                                        $('#autoclosable-btn-success').prop("disabled", false);
                                    });
                                    {{----SUCCESS ON SAVE -------}}

                                        return false;
                                }

                            }

                        });


                    }

                else{
                    $('#save_po').removeAttr('disabled');
                    $('#sendto_approved_btn').removeAttr('disabled');
                    $('#approved_btn').removeAttr('disabled');
                }
                }
            </script>

            {{--------------------------  edit borrow  --------------------------------}}
            <script>
                $(document).on('click','#edit_po',function () {
                    $('#edit_po').text("Save");
                    $('#edit_po').attr('id','save_po');

                    $('#refused_btn').prop('disabled',false);
                    $('#refused').removeClass('hide');
                    if($('#set_po_id').attr('current_po_status') == 'draft_po')
                    {
                        $('#refused_btn').prop('disabled',true);
                        $('#refused').addClass('hide');
                    }
                    if($('#set_po_id').attr('current_po_status') != 'approved_po' && $('#set_po_id').attr('current_po_status') != 'refused')
                    {
                        $('.set_all_disabled').prop('disabled',false);
                        var rowCount = $('#op_table tr').length;
                        console.log('rowCount'+ rowCount);

                    }
                    if($('#set_po_id').attr('current_po_status') == 'refused'){
                        $('#refused_btn').prop('disabled',true);
                    }
                    if($('#set_po_id').attr('current_po_status') == 'approved_po'){
                        enable_disable_add();
                        $("tr[paid='0']").find('.table_btn').prop('disabled',false);
                    }


                    console.log($('#set_po_id').attr('current_po_id'));


                    $('#delete_po').parent('li').addClass('hide');


                    $('#create_invoice').addClass('hide');
                    $('#creat_po').addClass('hide');
                    $('#po_more').addClass('hide');
                });
            </script>

    {{---------------  delete borrow ------------------------}}
            <script>
                $(document).on('click','#delete_po',function(){
                    var cur_po_id = $('#set_po_id').attr('current_po_id');

                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: '/borrow/delete/',
                        dataType : 'json',
                        type: 'get',
                        data: {
                            cur_po_id:cur_po_id,
                        },

                        success:function(response) {

                            window.location.href = '/borrow';
                        }

                    });

                });
            </script>


        {{----------------  create new borrow ------------------}}
            <script>
                $(document).on('click','#creat_po',function () {

                    window.location.href = '/borrow';

                });
            </script>

            {{-----------   on leave page -------------------}}
            <script type='text/javascript'>

                $(window).bind('beforeunload', function(){
                    if($('#save_po').length){
                        return 'Are you sure you want to leave?';
                    }

                });


            </script>


            <script>

                $(document).on('click', '#print', function () {

                    //  window.location.href = 'get_po2/276'
                    var id = $('#set_po_id').attr('current_po_id');
                    window.open('/get_borrow_rpt/' + id + '', '_blank');


                });


            </script>

    @if(isset($borrow_details))
                <script>
                    $(document).ready(
                        get_totals()
                    );
                </script>
    @endif

@endsection