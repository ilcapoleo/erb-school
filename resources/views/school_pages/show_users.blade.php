
@extends('layouts.main_app')

@section('main_content')
    <style>






        th > a
        {
            color: #676a6d;
        }

        .osb-multisearch > div > div > .panel-default{

            margin-top: 5%;
            margin-bottom: 5%;
        }
        .panel .panel-body{
            padding-top: 0px;
            padding-bottom: 0px;
        }
        ul{
            list-style-type: none;
        }

        .col_op{
            padding-top: 3px;
            padding-bottom: 3px;

        }
        .search-bar {
            bottom: -96px!important;
        }

        .glyphicon-refresh, .refresh{
            position: absolute;
            top: 16px;
            display: inline-block;
            font-style: normal;
            font-weight: 400;
            line-height: 1;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
            right: 45px;
            font-size: 15px;
            background-color: green;
            padding: 6px 12px;
            margin-bottom: 0;
            font-size: 14px;
            font-weight: 400;
            line-height: 1.42857143;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            color:#fff;
            border: 1px solid transparent;
            box-shadow: 0px 1px 2px 0 rgba(0, 0, 0, 0.2);
        }


        /* style dropdown of search */

        .search-bar {
            text-align: center;
            margin: 0;
            padding: 0px 4px 0px 0px;
            list-style: none;
            position:absolute;
            bottom: -96px;
            box-shadow: 0 2px 6px rgba(0, 0, 0, 0.08);
            border-radius: 3px;
            border-color: #ddd;
            z-index: 1;
            width: 99.6667%;
        }
        .search-bar li {
            margin-right: -4px;
            /*padding: 15px 20px;*/
            background: #fff;
            cursor: pointer;
            -webkit-transition: all 0.2s;
            -moz-transition: all 0.2s;
            -ms-transition: all 0.2s;
            -o-transition: all 0.2s;
            transition: all 0.2s;
        }
        .search-bar li:hover , .search-bar li:focus {
            background: #555;
            color: #fff;
        }
        .search-bar  ul {
            padding: 0;
            position: absolute;
            top: 18px;
            left: 0;
            width: 300px;
            -webkit-box-shadow: none;
            -moz-box-shadow: none;
            box-shadow: none;
            display: none;
            opacity: 0;
            visibility: hidden;
            -webkit-transiton: opacity 0.2s;
            -moz-transition: opacity 0.2s;
            -ms-transition: opacity 0.2s;
            -o-transition: opacity 0.2s;
            -transition: opacity 0.2s;
        }
        .search-bar  ul li {
            display: block;
            color: #000;
            padding-bottom: 3px;
        }
        .search-bar li ul li:hover {
            background: #666;
        }
        .search-bar li:hover ul {
            display: block;
            opacity: 1;
            visibility: visible;
        }

        .dropdown-content{
            position: relative;
        }


        #users_data > thead{
            background-color:#ccc;
        }
        .glyphicon-refresh{
            position: absolute;
            top: 16px;
            display: inline-block;
            font-family: 'Glyphicons Halflings';
            font-style: normal;
            font-weight: 400;
            line-height: 1;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
            right: 45px;
            font-size: 29px;
        }


        #more{
            margin-top: 10%;
        }

    </style>

    <div class="main">

        <!-- Navbar-content -->
        <nav class="navbar navbar-default navbar2">

            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                    <div class="row">


                        <div class="col-md-4">
                            <a href="/add_account" type="button" class="btn btn-danger ">Create</a>
                        </div>
                        <div class="col-md-4">
                            <div class="col-md-6 col-lg-6">
                                <div class="dropdown">
                                    <button class=" btn btn-primary  dropdown-toggle" type="button" data-toggle="dropdown" id="more"  hidden>More
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#"  data-toggle="modal" data-target="#exampleModal">Delete</a></li>
                                        <li><a href="#" id="print_all_selected">Print</a></li>
                                        <!-- temporery -->              <!--     <li><a href="#" id="Duplicate_all_selected">Duplicate</a></li> -->

                                    </ul>
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-6 radios">
                                <label class="radio-inline"><input type="radio" name="optradio" value="1" checked>AND</label>
                                <label class="radio-inline"><input type="radio" name="optradio" value="0">OR</label>
                            </div>


                        </div>


                        <div class="col-md-4">
                            <form action="/get_po1" target="_blank" method="post">
                                {{csrf_field()}}
                                <input type="hidden" name="print_ids[]" id="print_array" value="" />

                                <button hidden id="get_pdff" type="submit"> print </button>
                            </form>


                        <!-- Modal -->
                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Students</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            Are You Sure to Delete this row ?!!
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" id="close_modal" data-dismiss="modal">Close</button>
                                            <button type="button" class="btn btn-danger fa fa-trash" id="delete_all_selected" >Delete</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{---------------   search----------------------------}}
                            <div id="end-inner">

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="panel panel-default">
                                            <div class="panel-body" data-role="selected-list">
                                                <input id="search_multi"   class="pull-left"   data-role="input" type="text" placeholder="Search..."/>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div data-role="picker" class="panel panel-default">
                                    <div class="list-group" data-role="picker-list">
                                    </div>
                                </div>

                                <div class="spacer">
                                    &nbsp;
                                </div>
                                <div class="container1 dropdown-content" hidden>
                                    <ul class="search-bar">
                                        <li  data-role="col_op" class=" defult_select_op col_op" value="|all"  tabindex="1">All</li>
                                        @for($i=0;$i<count($database_search_list);$i++)
                                            <?php $index= $i+2 ?>
                                            <li class="col_op" value="|{{$database_search_list[$i]}}"  tabindex="{{$index}}">{{$showed_search_list[$i]}}</li>
                                            {{--<li class="col_op" value="|s_name"  tabindex="2">name</li>
                                            <li class="col_op" value="|s_class" tabindex="3">class</li>
                                            <li class="col_op" value="|s_age" tabindex="4">age</li>--}}
                                            <?php$index++?>
                                        @endfor
                                    </ul>
                                </div>
                            </div>
                            {{------------------------------------------------}}



                        </div>
                    </div> <!--collapse -->
                </div>
            </div>
        </nav>
        <!-- End Navbar-content -->

        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                <div class="panel table-panel">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-lg-12">

                            <h4>Users Table</h4>


                            <div class="table-responsive">

                                {{---------------------------------}}
                                <a  type="button">
                                    <button id="refresh_btn" class="refresh">Refresh Data</button>
                                </a>
                                {{---------------------------------}}

                                <table role="table" class="table table-bordred table-striped table-responsive thead-light" id="users_data" >
                                    <thead  class="cf">
                                    <tr>
                                        <th   class=" smp-not-sortable">
                                            <input type="checkbox" id="checkall" />

                                        </th>

                                        <th ><a  class="sort-by">User Name</a></th>
                                        <th ><a class="sort-by">User Email</a></th>
                                        <th ><a class="sort-by">Employee Name</a></th>
                                        <th ><a  class="sort-by">Created Date</a></th>
                                        <th ><a  class="sort-by">Last Update</a></th>

                                    </tr>

                                    </thead>
                                    <tbody>
                                    @if(isset($users))
                                        @foreach($users as $user)
                                            <tr role="row" class="clickable-row" id="{{$user->id}}">
                                                <td ><input type="checkbox" class="checkthis"></td>
                                                <td >{{$user->name}}</td>
                                                <td >{{$user->email}}</td>
                                                <td >{{$user->employee_name.' '.$user->middle_name.' '.$user->last_name.' '.$user->family_name}}</td>
                                                <td >{{$user->created_at}}</td>
                                                <td >{{$user->updated_at}}</td>

                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>

                                </table>

                            </div>





                        </div>
                    </div>

                </div>
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN -->

    </div>
    <div id="get_user_table" user_v="{{\Illuminate\Support\Facades\Auth::user()->id}}"></div>
@endsection

@section('custom_footer')

    <script src="{{asset('js/jquery-ui.min.js')}}"></script>

    {{----------------------------  datatable -------------------------------}}
    <script src="{{asset('js/jquery.dataTables.min.js')}}"></script>

    {{-----------------  multi search ------------------------------}}
    <script src="{{asset('js/lodash.underscore.min.js')}}"></script>
    <script src="{{asset('js/jqueryui-multisearch_products_table.js')}}"></script>


    {{--------------------  active side & nav bar ------------------------}}
    <script>
        $('.side_sheets').addClass('hide');
        $('#system').removeClass('hide');

        $("#menu li").removeClass('active');
        $('#System_nav_bar').addClass('active');

        $('#pages ul li a').removeClass('active');
        $('#system_side_bar').addClass('active');

    </script>


    <script>
        $(function() {
            $("#end-inner").multisearch({
                source: babyNames,
            });


        });
        var babyNames = [];
    </script>
    {{--------------------------  new search ---------------------------------------}}
    <script>


        $("#search_multi").keydown(function(e){
            if (e.keyCode == 40 && $(this).val() != "") {
                $('div.container1').show();
                //$('#colums_v').focus();
                $('div.container1').on('focus', 'li', function() {
                    var $this = $(this);
                    $this.addClass('active').siblings().removeClass('active');
                    $this.attr('data-role',"col_op").siblings().removeAttr('data-role');

                    $this.closest('div.container1').scrollTop($this.index() * $this.outerHeight());
                }).on('keydown', 'li', function(e) {
                    var $this = $(this);
                    if (e.keyCode === 40) {
                        $this.next().focus();
                        return false;
                    } else if (e.keyCode === 38) {
                        $this.prev().focus();
                        return false;
                    }
                }).find('li').first().focus();
                /*$('#colums_v').select2('open');*/
            }

        });

        $("#search_multi").keypress(function (e) {
            if (e.keyCode != 13)
            {
                $('div.container1').show();
            }
        });
        $('.col_op').hover( function() {
            //console.log('hover');
            $(this).attr('data-role',"col_op").siblings().removeAttr('data-role');
        })

    </script>

    <script>
        $('#search_multi').click(function(){
            myDropDown = $(this).next('.dropdown-content')

            if( myDropDown.is(':visible') ) {
                $(this).removeClass('drop-down-open');
                myDropDown.hide();
            } else {
                myDropDown.fadeIn();
                $(this).addClass('drop-down-open');
            }

            return false;
        });

        $('html').click(function(e) {
            $('.dropdown-content').hide();
        });

        $('.dropdown-content').click(function(e){
            e.stopPropagation();
        });


    </script>


    {{------------------------------------}}

    {{--------------- search  table  --------------------------}}
    <script>
        function get_table() {

            var ids = [];
            var values = [];
            var id_user = $('#get_user_table').attr('user_v');
            $('a[data-role="selected-item"]').each(function () {
                var searched = $(this).attr('passed_data').split('|');
                ids.push(searched[1]);
                values.push(searched[0]);
            });

            var table = $('#users_data').DataTable();
            var search_type = $('input[name=optradio]:checked').val();

            console.log('search_type : '+ search_type)
            if(search_type == 1 )
            {
                /*------------------------  AND search  ---------------------------------------------------------*/
                if (ids.length == 0) {
                    //ids.push("");
                    table
                        .search( '' )
                        .columns().search( '' )
                        .draw();
                    //table.draw();
                }

                table
                    .search( '' )
                    .columns().search( '' )
                    .draw();
                for(var i = 0 ; i<ids.length ; i++)
                {
                    if(ids[i] != 'all')
                    {
                        table
                            .columns( ids[i] )
                            .search( values[i] )
                            .draw();
                    }
                    else{
                        console.log('  in else  ');
                        table
                            .search( values[i] )
                            .draw();
                    }
                }

                console.log(ids);
                console.log(values);
            }

            else{
                /*--------------------------  OR Search  ----------------------------------------*/
                var dataTable = $('#users_data').dataTable();

                table
                    .search( '' )
                    .columns().search( '' )
                    .draw();

                var input = $('a[data-role="selected-item"]');

                if (input.length > 0) {
                    var keywords = values, filter = '';
                    console.log('keywords : ' + keywords);
                    for (var i = 0; i < keywords.length; i++) {
                        filter = (filter !== '') ? filter + '|' + keywords[i] : keywords[i];
                    }

                    dataTable.fnFilter(filter, null, true, false, true, true);
                    //                                ^ Treat as regular expression or not
                } else {
                    dataTable.fnFilter(" ", null, true, false, true, true);
                }

                /*---------------------------------------------------------------------------------------*/
            }

        }


        $('input[type=radio][name=optradio]').change(function() {

            get_table();
        });

    </script>
    {{--------------------------------------}}



    <script>

        $('#modules').hide();
        $(".link").click(function() {

            var id = $(this).attr("data-rel");
            $('#side_menu_hide ul').hide();
            $("#" + id).show();
        });
    </script>

    <!--Multisearch -->


    <!--check all in table -->

    <script>

        $(document).on('click','#checkall',function () {
            if ($("#users_data #checkall").is(':checked')) {
                $("#more").removeClass('hide');
                $("#users_data tbody > tr:visible input[type=checkbox]").each(function () {
                    $(this).prop("checked", true);
                });

            } else {
                $("#users_data tbody > tr:visible input[type=checkbox]").each(function () {
                    $(this).prop("checked", false);
                    $("#more").addClass('hide');
                });
            }
            $("[data-toggle=tooltip]").tooltip();

        });

        $(document).on('click','#users_data input[type=checkbox]',function () {
            var checked_count = $('.checkthis:checkbox:checked').length;
            //console.log(checked_count);
            if ($(this).is(":checked")) {
                if(checked_count>0){
                    $("#more").removeClass('hide');
                }
            }
            if (checked_count==0)
            {
                $('#checkall').prop("checked", false);
                $("#more").addClass('hide');

            }
            else {
                if(checked_count == 0)
                {$("#more").addClass('hide');}
            }
        });

        $("#more").addClass('hide');


        /*$("#checkall , #users_data input[type=checkbox]").click(function () {
            if ($(this).is(":checked")) {
                $("#more").removeClass('hide');
            } else {
                $("#more").addClass('hide');
            }
        });
*/
        /*-----------------------------  end check ------------------------------------------------*/
        $(document).on('click','#delete_all_selected',function () {

            var all_checked = $('.checkthis:checked');

            var all_ids = [];

            all_checked.each(function () {
                var vall = $(this).closest('tr').attr('id');
                /*console.log($(this).closest('tr').attr('id'))*/
                all_ids.push(vall);

            });

            console.log(all_ids);


            $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/delete_all_selected_users/',
                    dataType : 'json',
                    type: 'get',
                    data: {
                        deleted_user_ids:all_ids,
                    },

                    success:function(response)
                    {
                        console.log(response);

                        for (var i =0; i<response['deleted_user_ids'].length;i++)
                        {
                            var table = $('#users_data').DataTable();
                            console.log('#'+response['deleted_user_ids'][i]);
                            //$('#'+response['deleted_user_ids'][i]).remove();
                            table
                                .row(  $('#'+response['deleted_user_ids'][i]) )
                                .remove()
                                .draw();

                        }

                        remove_all_check();
                        /*paginate_table();*/
                        $("#close_modal").click();

                    },error:function(response)
                    {
                        alert('cant delete something went wrong !')
                        remove_all_check();
                        $("#close_modal").click();
                    }
                }
            );

        })



    </script>

    <script>
        $(document).on('click','#users_data tr',function() {
            var href = $(this).attr("id");

            if(!$(event.target).hasClass('checkthis')) {
                if(href) {
                    window.location = 'add_account/'+href;
                }        }



        });
    </script>


    {{---------------  refresh page  ----------------------}}
    <script>
        $(document).on('click','#refresh_btn',function () {
            // load_table();
            location.reload();
        });
    </script>
    {{-------------------------  print po ---------------------------}}
    <script>
        $(document).on('click','#print_all_selected',function () {
            //var all_checked = $('#users_data tbody tr');
            var all_checked = $('.checkthis:checked');
            var all_ids = [];

            all_checked.each(function () {
                /*var vall = $(this).attr('id');*/
                var vall = $(this).closest('tr').attr('id');
                /*console.log($(this).closest('tr').attr('id'))*/
                all_ids.push(vall);
            });

            console.log('all_ids' + all_ids);
            $('#print_array').val(all_ids);
            $('#get_pdff').click();

        });

    </script>



    <script>
        $("table th").addClass("headerSortUp");
        $("table th").addClass("headerSortDown");

    </script>
    {{--------------------------remove_all_check--------------------------------}}
    <script>

        function remove_all_check()
        {
            /*------- hide all checked  -----*/
            $('#checkall').prop("checked", false);
            $("#users_data tbody > tr  input[type=checkbox]").each(function () {
                $(this).prop("checked", false);
                $("#more").addClass('hide');
            });
            /*--------------------------------*/
        }

        function set_datatable()
        {
            $('#users_data')
            /*.on( 'order.dt',  function () { console.log('Order' ); remove_all_check()} )
            .on( 'search.dt', function () {console.log('Search' ); remove_all_check()} )*/
                .on( 'page.dt',   function () { console.log('Page' ); remove_all_check()} )
                .dataTable();

        }

    </script>

    <script>
        /*$('.dropdown-toggle').dropdown()*/

        $(document).ready(/*load_table(),*/set_datatable(),$('div.container1').hide());
        /*$('#colums_v').select2()*/
    </script>
@endsection
