@extends('layouts.main_app')

@section('main_content')
    <link href="{{asset('css/fastselect.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/select2.min.css')}}" rel="stylesheet" />

    <link href="{{asset('css/fullcalendar.min.css')}}" rel="stylesheet" />

    <link href=’https://fullcalendar.io/releases/fullcalendar-scheduler/1.9.3/scheduler.min.css’ rel=’stylesheet’ />

    <!-- claender css files -->
    <link href="{{asset('css/calendar.css')}}" rel="stylesheet" />

    <style>

        .fc-sat, .fc-fri {
            background-color: #f5f5f5!important;
        }
        span.select2.select2-container.select2-container--default{
            width: 100%!important;
        }


        .modal-dialog {
            width: 82%;
        }
        #sendDropDownOptions{
            width: 50%!important;
        }
    </style>
    <div class="main">
        <!-- Navbar-content -->
        <nav class="navbar navbar-default navbar2">

            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                    <!--breadcrumbs-->
                    <div class="row">
                        <ol class="breadcrumb">
                            <li><a href="#">HR</a></li>
                            <li class="active">Leave Request</li>
                        </ol>
                    </div>   <!--End breadcrumbs-->
                    <div class="row">
                        <div class="col-md-4 col-lg-4 buttons">
                            <button id="edit_user" type="button" class="btn btn-danger " >Edit</button>
                            <button type="button" class="btn" id="creat" >Create</button>
                        </div>

                        <div class="col-md-4">
                            <div class="dropdown">
                                <button class="btn btn-primary  dropdown-toggle" type="button" data-toggle="dropdown" id="more"  hidden>More
                                    <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#" id="delete">Delete</a></li>
                                    <li><a href="#">Export</a></li>
                                    <li><a href="#">Duplicate</a></li>
                                    <li><a href="#">Print</a></li>

                                </ul>
                            </div>
                        </div>
                        <!--search-->
                        <!--
                         <div class="col-md-4">
                             <form class="navbar-form navbar-right" >
                                 <div class="input-group form-group">
                                     <input type="text" value="" class="form-control" placeholder="Search ...">
                                 </div>
                             </form>
                         </div>
                         -->
                    </div> <!--collapse -->

                </div><!--collapse -->

                <!--buttons row +second breadcrumb-->

                </div>
        </nav>
        <!-- End Navbar-content -->

        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Leave Request</h3>
                    </div>
                    <div class="panel-body">

                        <div id='calendar'></div>




                        <!--Add event modal-->
                        <div id="createEventModal" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span> <span class="sr-only">close</span></button>
                                        <h4>Leave Request</h4>

                                        <div class="row third-nav">


                                            <div class="col-md-6 col-lg-6">
                                                <div class="col-md-3 col-lg-3 col-sm-12 ">
                                                    <button  type="button" class="btn" >Confirm</button>
                                                </div>

                                                <div class="col-md-3 col-lg-3 col-sm-12">
                                                    <button  type="button" class="btn" >Approve</button>
                                                </div>

                                                <div class="col-md-3 col-lg-3 col-sm-12">
                                                    <button type="button" class="btn " >Refuse</button>
                                                </div>
                                                <div class="col-md-3 col-lg-3 col-sm-12">
                                                    <button type="button" class="btn " >Reset to Draft</button>
                                                </div>


                                            </div>
                                            <div class="col-md-6 col-lg-6 col-sm-12">
                             <span class="breadcrumbs">

                            <a href="#" class="breadcrumb">To Submit</a>
                               <a href="#" class="breadcrumb">To Approved</a>
                               <a href="#" class="breadcrumb">Refused</a>
                                <span class="breadcrumb">Approved</span>

                            </span>


                                            </div>


                                        </div>

                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                        <form id="createAppointmentForm" >
                                                <div class="row">
                                                    <div class="col-md-12 col-lg-12">
                                                        <div class="col-md-6 col-lg-6">
                                                            <div class="form-group">
                                                                <label class=" col-md-6 col-lg-6 control-label" for="leaveName">Description:</label>
                                                                <div class="controls col-md-6 col-lg-6">
                                                                    <input type="text" name="leaveName"  id="leaveName" tyle="margin: 0 auto;" data-provide="typeahead" data-items="4" data-source="[&quot;Value 1&quot;,&quot;Value 2&quot;,&quot;Value 3&quot;]">
                                                                    <input type="hidden" id="apptStartTime"/>
                                                                    <input type="hidden" id="apptEndTime"/>
                                                                    <input type="hidden" id="apptAllDay" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6 col-lg-6">
                                                            <div class="form-group">

                                                                <label class="col-md-6 col-lg-6 control-label">Employee</label>
                                                                <div class="col-md-6 col-lg-6">

                                                                    <select class="myselect1 form-control"  style="width: 50%" name="state">

                                                                        <option>ahmed hamdy</option>

                                                                        <option >Ali Ahmed </option>

                                                                        <option >Ashraf mohamed</option>

                                                                        <option >Mohamed Ali</option>


                                                                    </select>


                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                          <!-- Day Selected -->
                                       <!--    <div class="row">
                                                    <div class="col-md-12 col-lg-12">
                                                        <div class="col-md-6 col-lg-6">

                                                        <div class="control-group">
                                                        <label class="control-label col-md-6 col-lg-6" for="when">Day Selected:</label>
                                                        <div class="controls controls-row col-md-6 col-lg-6" id="when" style="margin-top:5px;">
                                                        </div>
                                                    </div>
                                                    </div>
                                                    </div>
                                                </div>  -->
                                                <br>
                                                <div class="row">

                                                    <div class="col-md-12 col-lg-12">
                                                        <div class="col-md-6 col-lg-6">

                                                            <div class="bootstrap-iso">
                                                                <div class="container-fluid">
                                                                    <div class="row">
                                                                        <div class="col-md-12 col-lg-12 col-sm-6 col-xs-12">


                                                                            <form method="post">
                                                                                <div class="form-group">
                                                                                    <label class=" control-label col-md-6 col-lg-6 " for="datetime1" style=" padding: 0px;

    font-size: 15px;
    color: #676a6d;
        font-weight: 400;font-family: "Source Sans Pro", sans-serif;">Requested Date</label>
                                                                                    <div class="input-group col-md-6 col-lg-6">
                                                                                        <input type="text" id="requested_date" disabled class="form-control " name="date">
                                                                                        <div class="input-group-addon">
                                                                                            <i class="fa fa-calendar">
                                                                                            </i>
                                                                                        </div>
                                                                                    </div>

                                                                                </div>
                                                                            </form>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>



                                                        </div>



                                                        <div class="col-md-6 col-lg-6">

                                                            <div class="bootstrap-iso">
                                                                <div class="container-fluid">
                                                                    <div class="row">
                                                                        <div class="col-md-12 col-lg-12 col-sm-6 col-xs-12">


                                                                            <form method="post">
                                                                                <div class="form-group">
                                                                                    <label class=" control-label col-md-6 col-lg-6 " for="datetime1" style=" padding: 0px;

    font-size: 15px;
    color: #676a6d;
        font-weight: 400;font-family: "Source Sans Pro", sans-serif;">Approved Date</label>
                                                                                    <div class="input-group col-md-6 col-lg-6">
                                                                                        <input type="text" id="approved_date" disabled class="form-control " name="date">
                                                                                        <div class="input-group-addon">
                                                                                            <i class="fa fa-calendar">
                                                                                            </i>
                                                                                        </div>
                                                                                    </div>

                                                                                </div>
                                                                            </form>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>



                                                        </div>

                                                    </div>

                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div class="col-md-12 col-lg-12">
                                                        <div class="col-md-6 col-lg-6">


                                                            <div class="form-group ">
                                                                <label class="col-md-6 col-lg-6">Leave Type:</label>
                                                                <select id="sendDropDownOptions" data-toggle="tooltip"  data-placement="bottom" title="" data-original-title="Send Options" class="form-control col-md-6 col-lg-6" >
                                                                    <option value="Now">  </option>
                                                                    <option value="Datetime">Datetime</option>
                                                                    <option value="Now">Hours</option>
                                                                </select>
                                                            </div>

                                                        </div>
                                                        <div class="col-md-6 col-lg-6">
                                                            <div class="form-group">
                                                                <label for="usr" class="col-md-6 col-lg-6 control-label">Department</label>

                                                                <div class=" col-md-6 col-lg-6">

                                                                    Accounting

                                                                </div>

                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">

                                                    <div class="col-md-12 col-lg-12">

                                                        <div class="col-md-6 col-lg-6">
                                                            <div class="form-group ">
                                                                <div id="sendTD">

                                                                    <label for="startDate1" class="col-md-6 col-lg-6 control-label">From</label>
                                                                    <label for="startDate2" class="col-md-6 col-lg-6 control-label">To</label>

                                                                    <div id="startdatetimeSend1" class="hide col-md-6 col-lg-6">
                                                                        <div class="form-group">
                                                                            <div class='input-group date' id='startDate'>

                                                                                <input type='text' class="form-control"  id="startDate1" />
                                                                                <span class="input-group-addon">
                                                          <span class="fa fa-calendar">
                                                     </span>
                                                  </span>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div id="startdatetimeSend2" class="hide col-md-6 col-lg-6">
                                                                        <div class="form-group">

                                                                            <div class='input-group date' id='startDate'>

                                                                                <input type='text' class="form-control" disabled id="startDate2" />
                                                                                <span class="input-group-addon">
	                        							<span class="glyphicon glyphicon-calendar"></span>
    </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12 col-lg-12">
                                                        <div class="col-md-6 col-lg-6">
                                                            <div class="form-group ">
                                                                <div id="durationHoursSend1" class="hide  col-md-6 col-lg-6 ">
                                                                    <div class="form-group">

                                                                        <div class='input-group date' id='startDate'>


                                                                            <input type='text' class="form-control"  id="hoursInput1" />
                                                                            <span class="input-group-addon">
	                        					                    		<span class="glyphicon glyphicon-calendar"></span>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div id="durationHoursSend2" class="hide  col-md-6 col-lg-6 ">
                                                                    <div class="form-group">
                                                                        <div class='input-group date' id='startDate'>


                                                                            <input type='text' class="form-control" disabled  id="hoursInput2" />
                                                                            <span class="input-group-addon">
	                        							<span class="glyphicon glyphicon-calendar"></span>
    </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>

                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div class="col-md-12 col-lg-12">
                                                        <div class="col-md-6 col-lg-6">
                                                            <div class="form-group">

                                                                <label class="col-md-6 col-lg-6 control-label">Duration</label>
                                                                <div class="col-md-6 col-lg-6">

                                                                    <input id="number" class="row_data" max="10" min="1"   type="number" value="1">
                                                                    <span style="display: none;" id="days"> Days</span>
                                                                    <span  style="display: none;" id="hours"> Hours</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                            <br>
                                            <div class="row">
                                                <div class="col-md-12">

                                                    <div class="col-md-6">
                                                        <div id="errorMessage" class="alert alert-danger" style="display: none;">
                                                            your range between 1 to 10 only!
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                        </form>
                                    </div>
                               </div>
                                    <div class="modal-footer">
                                        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                                        <button type="submit" class="btn btn-primary" id="submitButton">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>









                            <br>
                        </div>







                    </div>
                </div>
                <!-- END MAIN CONTENT -->

            </div>
            <!-- END MAIN -->
        </div>
    </div>

@endsection


@section('custom_footer')

    <script src="{{asset('js/item_name_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/tax_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/select2.min.js')}}"></script>

    <script src="{{asset('assets/scripts/bootstrap-datetimepicker.min.js')}}"></script>

    <!--calender scripts -->
    <script type="text/javascript" src="{{asset('js/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/fullcalendar.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/daterangepicker.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/scheduler.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/gcal.js')}}"></script>

    <script>
        $(document).ready(function() {
            var calendar = $('#calendar').fullCalendar({
                header : {
                    left:   'prev,next today',
                    center: 'title',
                    right:  'month,agendaWeek,agendaDay'
                },
                selectable: true,
                eventLimit: true,
                weekends: true,

                //header and other values
                select: function(start, end, allDay) {


                    endtime = $.fullCalendar.formatDate(end,'YYYY-MM-DD hh:mm ');
                    starttime = $.fullCalendar.formatDate(start,'YYYY-MM-DD hh:mm ');
                    startDate=  $.fullCalendar.formatDate(start,"YYYY-MM-DD");
                    endDate=  $.fullCalendar.formatDate(end,"YYYY-MM-DD");

                    var mywhen = starttime + ' - ' + endtime;
                    var date1   = startDate  ;
                    var date2   = endDate  ;
                    $('#createEventModal #apptStartTime').val(start);
                    $('#createEventModal #apptEndTime').val(end);
                    $('#createEventModal #apptAllDay').val(allDay);
                    $('#createEventModal #when').text(mywhen);


                    $('#createEventModal #startDate1').datepicker({    format: 'yyyy-mm-dd',autoclose: true}).val(date1);
                    $('#createEventModal #requested_date').datepicker({    format: 'yyyy-mm-dd',autoclose: true}).val(date1);
                    $('#createEventModal #startDate2').datepicker({    format: 'yyyy-mm-dd',autoclose: true}).val(date1);
                    $('#createEventModal #hoursInput1').datetimepicker().val(starttime);
                    $('#createEventModal #hoursInput2').datetimepicker().val(endtime);
                    $('#createEventModal').modal('show');
                    $('#number').val("");



                    $("#requested_date").datepicker({    format: 'yyyy-mm-dd',autoclose: true}).datepicker("setDate", new Date());
                    $("#startDate1").datepicker({   format: 'yyyy-mm-dd',autoclose: true}).datepicker("setDate", new Date(date1));
                    $("#startDate2").datepicker({   format: 'yyyy-mm-dd',autoclose: true}).datepicker("setDate", new Date(date1));

                    $("#hoursInput1").datetimepicker({



                    }).datetimepicker("setDate", new Date(starttime));

                    $("#hoursInput2").datetimepicker({

                    }).datetimepicker("setDate", new Date(endtime));

                    $("#sendDropDownOptions").on('change', function() {

                        if ($("#sendDropDownOptions option:selected").text() == "Datetime") {


                            $("#durationHoursSend1").addClass('hide');
                            $("#sendTD").append($("#startdatetimeSend1"))
                            $("#startdatetimeSend1").removeClass('hide');

                            $("#durationHoursSend2").addClass('hide');
                            $("#sendTD").append($("#startdatetimeSend2"))
                            $("#startdatetimeSend2").removeClass('hide');

                            $("#hours").hide();
                            $("#days").show();


                            $("#number, #startDate1").on('input change', function() {
                                var days=$('#number').val();
                                console.log(days);
                                var days2 = $('#startDate1').datepicker('getDate');
                                days2.setDate(days2.getDate() + parseInt(days)-1);
                                $('#startDate2').datepicker('setDate', days2);

                            });

                        } else if ($("#sendDropDownOptions option:selected").text() == "Hours") {
                            $("#startdatetimeSend1").addClass('hide');
                            $("#sendTD").append($("#durationHoursSend1"))
                            $("#durationHoursSend1").removeClass('hide');

                            $("#startdatetimeSend2").addClass('hide');
                            $("#sendTD").append($("#durationHoursSend2"))
                            $("#durationHoursSend2").removeClass('hide');

                            $("#hours").show();
                            $("#days").hide();



                            $("#number ,#hoursInput1").on('input change', function() {

                                var hours1 = $('#hoursInput1').datetimepicker('getDate');
                                $('#hoursInput2').datetimepicker('setDate', hours1);

                                var hourss=$('input[type=number]').val();
                                console.log(hourss);
                                var hours2 = $('#hoursInput1').datetimepicker('getDate');
                                hours2.setHours(hours2.getHours() + parseInt(hourss));
                                $('#hoursInput2').datetimepicker('setDate', hours2);

                            });



                        } else {
                            $("#startdatetimeSend1").addClass('hide');
                            $("#durationHoursSend1").addClass('hide');
                            $("#startdatetimeSend2").addClass('hide');
                            $("#durationHoursSend2").addClass('hide');
                            $("#startDate2").val('');
                        }
                    });



                    /*-----------------------holiday-----------------------------*/
                    var startDate = moment(start),
                        endDate = moment(end),
                        date = startDate.clone(),
                        isWeekend = false;

                    while (date.isBefore(endDate)) {
                        if (date.isoWeekday() == 5 || date.isoWeekday() == 6) {
                            isWeekend = true;
                        }
                        date.add(1, 'day');
                    }

                    if (isWeekend) {
                        alert('can\'t add event - weekend');
                        $("#createEventModal").modal('hide');

                        return false;
                    }

                    this.startDate= startDate.format("YYYY-MM-DD");
                    this.endDate= endDate.format("YYYY-MM-DD");

                    //$('.first.modal').modal('show');

                },



            });

            $('#submitButton').on('click', function(e){
                // We don't want this to act as a link so cancel the link action
                e.preventDefault();

                    if ($("#sendDropDownOptions option:selected").text() == "Datetime") {

                        doSubmit1();

                    } else if ($("#sendDropDownOptions option:selected").text() == "Hours") {


                        doSubmit2();

                    }





            });

            function doSubmit1(){
                $("#createEventModal").modal('hide');

                /*$("#calendar").fullCalendar('renderEvent',
                    {
                        title: $('#leaveName').val(),
                        start: $('#startDate1').val(),
                        end: $('#startDate2').val() ,
                        allDay: ($('#apptAllDay').val() == "true"),

                    }, true);*/
                var myEvent = {
                    title  : $('#leaveName').val(),
                    start  : $('#startDate1').val(),
                    end    : $('#startDate2').val()
                };
                var myCalendar = $('#calendar');
                myCalendar.fullCalendar( 'renderEvent', myEvent );



            }





            function doSubmit2(){
                $("#createEventModal").modal('hide');


                $("#calendar").fullCalendar('renderEvent',
                    {
                        title: $('#leaveName').val(),
                        start: new Date($('#hoursInput1').val()),
                        end: new Date($('#hoursInput2').val()) ,

                        allDay: ($('#apptAllDay').val() == "true"),
                    }, true);



            }



        });





    </script>
    <!--date/time picker-->

    <script>

        $('#datetime1').datetimepicker();

        $('#datetime2').datetimepicker();
        $('#datetime3').datetimepicker();


    </script>
    <!--edit fields -->

    <script>
        $(document).on('click','#edit_user',function(){

            $(this).text("Save");
            $(this).attr('id','save_user');
            $("input[type=text]").attr('disabled', false).css({
                'border': '#eaeaea solid 1px',

                'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                'border-radius':'2px',
                'background-color':'#fff',
                'width':'150px',
                'margin-left':'15px'
            }).focus();

            $("input[type=password]").attr('disabled',false).css({
                'border': '#eaeaea solid 1px',

                'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                'border-radius':'2px',
                'background-color':'#fff',
                'margin-left':'15px'
            });

            $("input[type=email]").attr('disabled',false).css({
                'border': '#eaeaea solid 1px',

                'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                'border-radius':'2px',
                'background-color':'#fff',
                'margin-left':'15px'
            });



            $("select").removeAttr('disabled');
            $("input[type=radio]").attr('disabled', false);
            $("input[type=password]").attr('disabled', false);
            $("input[type=email]").attr('disabled', false);

        });


    </script>
    <!--add row to table -->
    <script>
        $("#add_row").click(function () {

            $("#myTable2").each(function () {

                var tds = '<tr>';
                jQuery.each($('tr:last td', this), function () {
                    tds += '<td>' + $(this).html() + '</td>';
                });
                tds += '</tr>';
                if ($('tbody', this).length > 0) {
                    $('tbody', this).append(tds);
                } else {
                    $(this).append(tds);
                }
            });
        });


    </script>

    <!--End add row to table -->
    <!--delete row in table -->
    <script>

        $(document).on('click', 'button#delete_row', function () {

            $(this).closest('tr').remove();
            return false;
        });

    </script>
    <!--Edit Row in Table -->

    <script>

        $(document).on('click','#editbtn',function(){
            var currentTD = $(this).parents('tr').find('td');
            if ($(this).html() == 'Edit') {
                currentTD = $(this).parents('tr').find('td');
                $.each(currentTD, function () {




                    $("input[type=text]").attr('disabled', false).css({
                        'border': '#eaeaea solid 1px',
                        'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                        'border-radius':'2px',
                        'background-color':'#fff',
                        'width':'150px',
                        'margin-left':'15px'
                    }).focus();

                    $("input[type=number]").attr('disabled',false).css({
                        'border': '#eaeaea solid 1px',

                        'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                        'border-radius':'2px',
                        'background-color':'#fff',
                        'margin-left':'15px'
                    });




                    $("select").removeAttr('disabled');


                });


            } else {
                $.each(currentTD, function () {
                    $("input[type=text]").attr('disabled', true).css({
                        'border': '#eaeaea solid 1px',

                        'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                        'border-radius':'2px',
                        'background-color':'#fff',
                        'width':'150px',
                        'margin-left':'15px'
                    }).focus();

                    $("input[type=number]").attr('disabled',true).css({
                        'border': '#eaeaea solid 1px',

                        'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                        'border-radius':'2px',
                        'background-color':'#fff',
                        'margin-left':'15px'
                    });

                    $("select").prop("disabled", true);

                });


            }

            $(this).html($(this).html() == 'Edit' ? 'Save' : 'Edit')

        });

    </script>
    <!--select salary scale -->

    <script>
        $(".myselect1").select2({

        });

    </script>

    <!-- validation on max Days -->
    <script>
        $("#number").on(' keyup input change',function() {
            if($('#number').val()< 1 || $('#number').val()>10 ){
                $('#errorMessage').show();
            }
            else{
                $('#errorMessage').hide();
            }
        });

    </script>




@endsection