@extends('layouts.main_app')

@section('main_content')
    <link href="{{asset('css/fastselect.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/select2.min.css')}}" rel="stylesheet" />

    <style>
        input.col-md-4.text-center {

            border:0;
            background-color: #fff;
        }



        .collapse > .row>.col-md-4>.btn, .collapse > .row>.col-md-4>.dropdown>.btn {
            margin-top: 0;

        }



        .third-nav{
            background-color:#eeeeee;
        }





        .myselect2{
            width:171px;
        }

        span.select2.select2-container.select2-container--default{
            width: 100%!important;
        }
        tbody th, tbody td {
            text-align: center;
        }
    </style>
    <link href="{{asset('css/select2.min.css')}}" rel="stylesheet"/>

    <div class="main">
        <!-- Navbar-content -->
        <nav class="navbar navbar-default navbar2">

            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                    <!--breadcrumbs-->
                    <div class="row">

                        <div class="col-md-10 col-lg-10">

                        <ol class="breadcrumb breadcrumb_nav">
                            <li><a href="/classes_table">Class</a></li>
                            <li class="active">

                                @if(isset($class))
                                    @if($class->classid != 0)
                                        {{$class->classid}}
                                    @else
                                        Draft
                                    @endif
                                @endif

                            </li>
                        </ol>
                        </div>

                        @if(isset($class))
                            <div class="col-md-2 col-lg-2 col-sm-4 col-xs-4">
                                @if(@\App\Classes::where('classid','<',$class->classid)->orderBy('classid','desc')->first()->classid)
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <a href="/classes/{{@\App\Classes::where('classid','<',$class->classid)->orderBy('classid','desc')->first()->classid}}"><span
                                                    class="glyphicon glyphicon-arrow-left"></span>
                                        </a>
                                    </div>
                                @endif
                                @if(@\App\Classes::where('classid','>',$class->classid)->orderBy('classid','asc')->first()->classid)
                                    <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                                        <a href="/classes/{{@\App\Classes::where('classid','>',$class->classid)->orderBy('classid','asc')->first()->classid}}">
                                    <span class="glyphicon glyphicon-arrow-right">
                                </span>
                                        </a>
                                    </div>
                                @endif
                            </div>
                        @endif


                    </div>


                    <!--End breadcrumbs-->
                    <!--End breadcrumbs-->
                    @if(isset($class))
                        <div id="set_po_id" current_po_id="{{$class->classid}}" current_po_status=""
                             closed_or_not="" current_id="{{Auth::user()->id}}"></div>
                    @else
                        <div id="set_po_id" current_po_id="" current_po_status="draft_po" closed_or_not="0"
                             current_id="{{Auth::user()->id}}"></div>
                    @endif
                    <div class="row">
                        <div class="col-lg-4  col-md-4 col-sm-6 col-xs-6 buttons">
                            @if(isset($class))
                                <button id="edit_po" type="button" class="btn btn-danger ">Edit</button>
                                <button type="button" class="btn" id="creat_dep">Create</button>
                            @else
                                <button id="save_po" type="button" class="btn btn-danger ">Save</button>
                                {{--<button id="edit_user" type="button" class="btn btn-danger " >Edit</button>--}}
                                <button type="button" class="hide btn" id="creat_dep">Create</button>
                            @endif
                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-4 ">
                            @if(isset($class))
                                <div class="dropdown">
                                    <button class=" btn btn-primary  dropdown-toggle" type="button"
                                            data-toggle="dropdown" id="more" hidden>More
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li class="  " data-toggle="modal" data-target="#exampleModal"><a href="#" id="delete_btn">Delete</a></li>
                                        <li><a href="#">Export</a></li>
                                        <li><a href="#" id="dublicate_dep">Duplicate</a></li>
                                        <li><a href="#" id="print">Print</a></li>

                                    </ul>
                                </div>
                            @else
                                <div class="dropdown">
                                    <button class="hide btn btn-primary  dropdown-toggle" type="button"
                                            data-toggle="dropdown" id="more" hidden>More
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li class="  " data-toggle="modal" data-target="#exampleModal"><a href="#" id="delete_btn">Delete</a></li>
                                        <li><a href="#">Export</a></li>
                                        <li><a href="#" id="dublicate_dep">Duplicate</a></li>
                                        <li><a href="#" id="print">Print</a></li>

                                    </ul>
                                </div>
                            @endif
                        </div>
                    </div>


                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Purchase Order</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    Are You Sure to Delete this PO ?!!
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-danger fa fa-trash" id="delete_po" >Delete</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div><!--collapse -->

            </div>
        </nav>
        <!-- End Navbar-content -->
        <!-- MAIN CONTENT -->
        <div class="main-content">

            <div class="container-fluid">

                <!-- INPUTS -->
                <div class="panel">

                    @if(isset($class))
                        <div id="set_po_id" current_po_id="{{$class->classid}}" current_po_status="" closed_or_not=""
                             current_id="{{Auth::user()->id}}"></div>
                    @else
                        <div id="set_po_id" current_po_id="" current_po_status="draft_po" closed_or_not="0"
                             current_id="{{Auth::user()->id}}"></div>
                    @endif

                    <div class="panel-heading">
                        <h3 class="panel-title">Class</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="class_name" class="col-md-6 col-lg-6 control-label">Class Name </label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            @if(isset($class))
                                                <input id="class_name" type="text"
                                                       class="set_all_disabled get_all_date_input form-control required err_input job_name_error "
                                                       value="{{$class->ClassName}}" disabled>
                                            @else
                                                <input id="class_name" type="text"
                                                       class="set_all_disabled get_all_date_input form-control required err_input job_name_error"
                                                       value="">
                                            @endif
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="fees_type" class="col-md-6 col-lg-6 control-label">Cost Center </label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <select id="cost_center" name="cost_center"
                                                    class=" required get_all_selectors set_all_disabled select_2_enable form-control department_error err_input"
                                                    @if(isset($class)) disabled @endif >
                                                <option value=""></option>
                                                @foreach($cost_centers as $id=>$role)
                                                    @if(isset($class))
                                                        @if($class->Costid == $id)
                                                            <option selected value="{{$id}}">{{$role}}</option>
                                                        @else
                                                            <option value="{{$id}}">{{$role}}</option>
                                                        @endif
                                                    @else
                                                        <option value="{{$id}}">{{$role}}</option>
                                                    @endif
                                                @endforeach
                                            </select>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>




                        <div class="row">

                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#fees_plan">Form</a></li>


                            </ul>

                            <div class="tab-content">

                                <div id="fees_plan" class="tab-pane fade in active">

                                    <div class="row" >
                                        <table  class="table table-bordred table-striped table-bordered table-striped table-hover table-condensed table-responsive " id="op_table" >
                                            <thead>
                                            <th>Form Name</th>
                                            <th>Edit</th>
                                            <th>Delete</th>
                                            </thead>
                                            <tbody>
                                            @if(isset($form))
                                                @foreach($form as $documents)
                                                    <tr class="">
                                                        <td td_type="input" class="class_name_v" current_value="{{$documents->form_name}}">{{$documents->form_name}}</td>
                                                        <td><button class="set_all_disabled btn btn-primary btn-xs edit_row" disabled>Edit</button></td>
                                                        <td><button class="set_all_disabled btn btn-danger btn-xs delete_row" disabled>
                                                                <span class="lnr lnr-trash"></span>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                        <div class="row">
                                            <div class="col-md-4 col-lg-4 ">
                                                <button type="button" class="btn btn-primary" id="add_row" @if(isset($class)) disabled  @endif >Add Item</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <br>
                </div>
            </div>

        </div>
        <!-- END INPUTS -->


    </div>

@endsection


@section('custom_footer')
    <script src="{{asset('js/item_name_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/tax_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/select2.min.js')}}"></script>





    <!-----------------active link in nav and active link in sidebar ---->
    <script>
        $('.side_sheets').addClass('hide');
        $('#admission').removeClass('hide');

        $("#menu li").removeClass('active');
        $('#admission_nav_bar').addClass('active');

        $('#pages ul li a').removeClass('active');
        $('#classes_side_bar').addClass('active');

    </script>

    {{------------  add new row  ------------------------}}
    <script>
        $(document).on('click','#add_row',function () {
            $(this).attr('disabled',true);

            var row = '<tr class= "added_now" >' +
                '<td td_type="input"><input class="class_name row_data form-control" data-role="input" type="text"/></td>'

                +'<td>'+'<button class="set_all_disabled btn btn-primary btn-xs Save_row">'+'Save'+'</button>'
                +'</td>'
                +'<td>'
                +'<button class="set_all_disabled btn btn-danger btn-xs delete_row"  >'+'<span class="lnr lnr-trash"></span>'+'</button>'
                +'</td>'

                +'</tr>';
            $("#op_table").prepend(row);
            $(".js-example-disabled-results").select2();
            //get_master_acc_drop_down($('.master_account'));

            //$(".po_values").siblings('.select2').css('width', '71px');

        });

    </script>

    {{--------   save class row  --------------}}
    <script>
        $(document).on("click", ".Save_row", function(){

            var empty = false;
            var call_save = false;
            var input = $(this).parents("tr").find('input[type="text"]');



            input.each(function(){
                if(!$(this).val()|| $(this).val()==""){
                    $(this).addClass("error");
                    empty = true;
                } else{
                    $(this).removeClass("error");
                }
            });
            console.log('empty '+empty);

            $(this).parents("tr").find(".error").first().focus();

            console.log($(input[0]).val() +"  "+ $(input[1]).val());

            var not_unique_name = $('.class_name_v:contains('+$(input[0]).val()+')').length;

            if(!empty ) {
                if(not_unique_name )
                {
                    alert('Class Name aleady exist'); return false;
                }

                else{
                    $(this).prop('disabled',true);
                    save_table_effect($(this));
                }


            }


        });

        function save_table_effect(thiss) {


            var class_name = thiss.parents("tr").find('.class_name').val();


            var current_po_id = $('#set_po_id').attr('current_po_id');


            console.log('class_name : '+class_name );
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/check_unique_class/',
                dataType: 'json',
                type: 'get',
                data: {

                    class_name:class_name,

                    current_po_id: current_po_id

                },

                success: function (response) {

                    console.log(response);
                    if (response['error']) {
                        $('.err_input').removeClass('red-border');
                        $('.err_input').parent().find('.select2').removeClass('red-border');

                        $('.errorMessage1').show();
                        $.each(response['error'], function (key, val) {
                            //alert(key + val);

                            console.log(key + '_error');
                            $("."+key+'_error').addClass('red-border');
                            $('.'+key+'_error').parent().find('.select2').addClass('red-border');

                            //console.log(val[0]);

                        })
                        thiss.prop('disabled',false);
                    }

                    else {

                        console.log('passed')


                        /*--------- all input  ------------------*/
                        var input_array = ['class_name_v'];
                        var all_input = thiss.parents("tr").find('input[type="text"]');

                        var i = 0;
                        all_input.each(function () {

                            var value_select = $(this).val();
                            $(this).parent("td").addClass(input_array[i]).attr('current_value', value_select).html(value_select);

                            i++;
                        });
                        /*--------------------------*/

                        if (thiss.parents("tr").hasClass('added_now')) {
                            thiss.parents("tr").removeClass('added_now');

                        }
                        if (thiss.parents("tr").hasClass('edited_now')) {
                            thiss.parents("tr").removeClass('edited_now');

                        }

                        //|| thiss.siblings().find('.create_edit_v') ){
                        //console.log(thiss.closest('tr').attr('id'))


                        if ($('.added_now').length == 0) {
                            $("#add_row").removeAttr("disabled");
                        }

                        thiss.text('Edit');
                        thiss.removeClass('Save_row');
                        thiss.addClass('edit_row');
                        thiss.prop('disabled',false);

                    }

                }

            });


        }


    </script>

    {{----------------------  edit row --------------------}}
    <script>
        $(document).on("click", ".edit_row", function(){
            if(!$('#op_table tr').hasClass('added_now'))
            {
                var input_class_array = ['class_name'];
                var s=0;

                $('#add_row').prop('disabled',true);
                $(this).parents("tr").addClass('edited_now');

                $(this).parents("tr").find("td:not(:last-child)").each(function(){
                    if($(this).attr('td_type') == 'input')
                    {
                        $(this).html('<input class="'+input_class_array[s]+' row_data form-control" data-role="input" type="text" value="' + $(this).text() + '"/>');
                        s++;
                    }

                });

                $("#add_row").attr("disabled", "disabled");
                $(this).text('Save');
                $(this).removeClass('edit_row');
                $(this).addClass('Save_row');

            }
        });

    </script>


    <!--delete row in table -->
    <script>

        $(document).on('click', '.delete_row', function () {

            $(this).hide(function(){
                $(this).closest('td').append('<button class="btn btn-danger set_all_disabled" type="button" id="yes" style="display: none;">yes</button>' +
                    '<button class="btn btn-default set_all_disabled" type="button" id="no" style="display: none;">no</button>');
                $(this).closest('td').find("#yes").show('slow').delay(1000);
                $(this).closest('td').find("#no").show('slow').delay(1000);

            });
        });

        $(document).on('click','#yes',function(){

            $(this).closest('tr').remove();
            if($('.added_now').length == 0)
            {
                $("#add_row").removeAttr("disabled");
            }
        });

        $(document).on('click','#no',function(){

            console.log('noo ');

            $(this).closest('td').find(".delete_row").show('slow').delay(1000);
            $(this).closest('td').find("#yes").hide('slow').delay(1000).remove();
            $(this).closest('td').find("#no").hide('slow').delay(1000).remove();
            /*
            $(this).closest('td').find("#yes").hide('slow').delay(1000);
            $(this).closest('td').find("#no").hide('slow').delay(1000);*/


        });


    </script>

    {{-------------------- save new class --------------------}}
    <script>

        $('.alert-autocloseable-success').hide();


        $(document).on('click','#save_po',function () {
            var rowCount = $('#op_table tbody tr:not(.added_now)').length;
            $(".errorMessage2").hide();
            $(".errorMessage1").hide();
            if(rowCount==0){

                $(".errorMessage2").show();
            }

            if(!$('#op_table tr').hasClass('edited_now') && rowCount >0) {
                $("#op_table tbody .added_now").remove();
                $('#save_po').attr('disabled', 'disabled');


                var class_name = $("#class_name").val();
                var cost_center = $("#cost_center").val();

                var input = $('#op_table > tbody  > tr');
                var row = [];

                input.each(function(){

                    if(!$(this).hasClass('added_now'))
                    {
                        row.push({"form_name": $(this).find('.class_name_v').attr('current_value')});
                    }

                });


                var current_user_id= $('#set_po_id').attr('current_id');
                var current_po_id = $('#set_po_id').attr('current_po_id');
                console.log(row);

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/add_new_class/',
                    dataType: 'json',
                    type: 'get',
                    data: {

                        class_name: class_name,
                        cost_center: cost_center,
                        table_rows:row,

                        current_user_id: current_user_id,
                        current_po_id: current_po_id
                    },

                    success: function (response) {

                        console.log(response);
                        if (response['error']) {
                            $('.err_input').removeClass('red-border');
                            $('.err_input').parent().find('.select2').removeClass('red-border');

                            $('.errorMessage1').show();
                            $.each(response['error'], function (key, val) {
                                //alert(key + val);

                                console.log(key + '_error');
                                $("."+key+'_error').addClass('red-border');
                                $('.'+key+'_error').parent().find('.select2').addClass('red-border');

                                //console.log(val[0]);

                            })
                            $('#save_po').removeAttr('disabled');

                        }

                        else {

                            $('.errorMessage1').hide();
                            $('.err_input').removeClass('red-border');
                            $('.err_input').parent().find('.select2').removeClass('red-border');
                            $('#save_po').text("Edit");
                            $('#save_po').attr('id', 'edit_po');
                            $('#add_row').prop('disabled', true);
                            $('#set_po_id').attr('current_po_id', response['current_po_id']);
                            $('.set_all_disabled').prop('disabled', true);
                            $('#creat_dep').removeClass('hide');

                            $('#more').removeClass('hide');

                            $('#more').removeClass('hide');


                            $('#edit_po').removeAttr('disabled');
                            //po-active
                            console.log(response);

                            {{----SUCCESS ON SAVE -------}}
                            $('#autoclosable-btn-success').prop("disabled", true);
                            $('.alert-autocloseable-success').show("slow");

                            $('.alert-autocloseable-success').delay(5000).fadeOut("slow", function () {
                                // Animation complete.
                                $('#autoclosable-btn-success').prop("disabled", false);
                            });
                            {{----SUCCESS ON SAVE -------}}
                        }

                    },
                    error: function (response)
                    {
                        alert(' Cant Save This Documents !');
                        $('#save_po').prop('disabled',false);
                    }

                });
            }
        });

    </script>

    {{---------------------  edit journal ----------------------}}
    <script>
        $(document).on('click','#edit_po',function () {
            $('#edit_po').text("Save");
            $('#edit_po').attr('id','save_po');

            $('.set_all_disabled').prop('disabled',false);
            $('#add_row').prop('disabled',false);

            $('#creat_dep').addClass('hide');

            $('#more').addClass('hide');
        });
    </script>

    {{----------------  create new class ------------------}}
    <script>
        $(document).on('click','#creat_dep',function () {

            window.location.href = '/classes';

        });
    </script>

    {{---------------------- delete class ------------------------------}}

    <script>
        $(document).on('click','#delete_po',function(){
            var cur_po_id = $('#set_po_id').attr('current_po_id');

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/delete_class/',
                dataType : 'json',
                type: 'get',
                data: {
                    cur_po_id:cur_po_id,
                },

                success:function(response) {

                    window.location.href = '/classes';
                }

            });

        });
    </script>


    {{---- dublicate dep ---}}
    <script>
        $(document).on('click', '#dublicate_dep', function () {

            $('#set_po_id').attr('current_po_id', "");
            /*$('#set_po_id').attr('current_po_status',"draft_po");*/
            /*$('#set_po_id').attr('closed_or_not',0);*/

            $('.set_all_disabled').prop('disabled', false);
            $('#add_row').prop('disabled',false);

            $('#creat_dep').addClass('hide');
            $('#more').addClass('hide');


            $('#edit_po').prop('disabled', false);
            $('#edit_po').text("Save");
            $('#edit_po').attr('id', 'save_po');
            $('#save_po').prop('disabled', false);
        });
    </script>

    {{-----------   on leave page -------------------}}
    <script type='text/javascript'>

        $(window).bind('beforeunload', function(){
            if($('#save_po').length){
                return 'Are you sure you want to leave?';
            }

        });


    </script>

    {{------------------------------------------}}
    <script>
        $(document).ready(function () {
            $(".select_2_enable").select2();

        });
    </script>


    <script>

        $(document).on('click', '#print', function () {

            //  window.location.href = 'get_po2/276'

            var id = $('#set_po_id').attr('current_po_id');
            window.open('/get_classes_rpt/' + id + '', '_blank');


        });

    </script>






@endsection