@extends('layouts.main_app')

@section('main_content')
    <link href="{{asset('css/fastselect.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/select2.min.css')}}" rel="stylesheet" />


   <style>

        
        .collapse > .row>.col-md-4>.btn, .collapse > .row>.col-md-4>.dropdown>.btn {
    margin-top: 0;
            
}
        .material-switch > label::before {
            background: rgb(0, 0, 0);
            box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
            border-radius: 8px;
            content: '';
            height: 16px;
            margin-top: 3px;
            position: absolute;
            opacity: 0.3;
            transition: all 0.4s ease-in-out;
            width: 40px;
            left: -29px;
        }


        .material-switch > label::after {
            background: rgb(255, 255, 255);
            border-radius: 16px;
            box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
            content: '';
            height: 24px;
            left: -45px;
            margin-top: -8px;
            position: absolute;
            top: 6px;
            transition: all 0.3s ease-in-out;
            width: 24px;
        }

        .material-switch > input[type="checkbox"]:checked + label::after {
            background: inherit;
            left: -5px;
        }
        .panel .table > thead > tr > td:first-child, .panel .table > thead > tr > th:first-child, .panel .table > tbody > tr > td:first-child, .panel .table > tbody > tr > th:first-child, .panel .table > tfoot > tr > td:first-child, .panel .table > tfoot > tr > th:first-child{
            padding-left: 0px;
        }

        ul{
            margin: 0;
            padding: 0;
            list-style: none;
        }


        span.select2.select2-container.select2-container--default{
            width: 100%!important;
        }





       /******************/
        .btn { /* just for this demo. */
            margin-top: 5px;
        }
        .btn-arrow-right,
        .btn-arrow-left {
            position: relative;
            padding-left: 18px;
            padding-right: 18px;
        }
        .btn-arrow-right {
            padding-left: 36px;
        }
        .btn-arrow-left {
            padding-right: 36px;
        }
        .btn-arrow-right:before,
        .btn-arrow-right:after,
        .btn-arrow-left:before,
        .btn-arrow-left:after { /* make two squares (before and after), looking similar to the button */
            content:"";
            position: absolute;
            top: 5px; /* move it down because of rounded corners */
            width: 22px; /* same as height */
            height: 22px; /* button_outer_height / sqrt(2) */
            background: inherit; /* use parent background */
            border: inherit; /* use parent border */
            border-left-color: transparent; /* hide left border */
            border-bottom-color: transparent; /* hide bottom border */
            border-radius: 0px 4px 0px 0px; /* round arrow corner, the shorthand property doesn't accept "inherit" so it is set to 4px */
            -webkit-border-radius: 0px 4px 0px 0px;
            -moz-border-radius: 0px 4px 0px 0px;
        }
        .btn-arrow-right:before,
        .btn-arrow-right:after {
            transform: rotate(45deg); /* rotate right arrow squares 45 deg to point right */
            -webkit-transform: rotate(45deg);
            -moz-transform: rotate(45deg);
            -o-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
        }
        .btn-arrow-left:before,
        .btn-arrow-left:after {
            transform: rotate(225deg); /* rotate left arrow squares 225 deg to point left */
            -webkit-transform: rotate(225deg);
            -moz-transform: rotate(225deg);
            -o-transform: rotate(225deg);
            -ms-transform: rotate(225deg);
        }
        .btn-arrow-right:before,
        .btn-arrow-left:before { /* align the "before" square to the left */
            left: -11px;
        }
        .btn-arrow-right:after,
        .btn-arrow-left:after { /* align the "after" square to the right */
            right: -11px;
        }
        .btn-arrow-right:after,
        .btn-arrow-left:before { /* bring arrow pointers to front */
            z-index: 1;
        }
        .btn-arrow-right:before,
        .btn-arrow-left:after { /* hide arrow tails background */
            background-color: white;
        }



    </style>
    <div class="main">
        <!-- Navbar-content -->
        <nav class="navbar navbar-default navbar2">

            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                    <!--breadcrumbs-->
                    <div class="row">

                        <div class="col-md-10 col-lg-10">

                        <ol class="breadcrumb breadcrumb_nav">
                            <li><a href="#">Pay Invoice</a></li>
                            <li class="active"></li>
                        </ol>
                        </div>

                        {{--@if(isset($master_inventory))--}}
                            {{--<div class="col-md-2 col-lg-2">--}}
                                {{--@if(@\App\inventory_master::where('doc_id','<',$master_inventory->doc_id)->orderBy('doc_id','desc')->first()->doc_id)--}}
                                    {{--<div class="col-md-6">--}}
                                        {{--<a href="/inventory/{{@\App\inventory_master::where('doc_id','<',$master_inventory->doc_id)->orderBy('doc_id','desc')->first()->doc_id}}"><span--}}
                                                    {{--class="glyphicon glyphicon-arrow-left"></span>--}}
                                        {{--</a>--}}
                                    {{--</div>--}}
                                {{--@endif--}}
                                {{--@if(@\App\inventory_master::where('doc_id','>',$master_inventory->doc_id)->orderBy('doc_id','asc')->first()->doc_id)--}}
                                    {{--<div class="col-md-6">--}}
                                        {{--<a href="/inventory/{{@\App\inventory_master::where('doc_id','>',$master_inventory->doc_id)->orderBy('doc_id','asc')->first()->doc_id}}">--}}
                                    {{--<span class="glyphicon glyphicon-arrow-right">--}}
                                {{--</span>--}}
                                        {{--</a>--}}
                                    {{--</div>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--@endif--}}

                    </div>   <!--End breadcrumbs-->
                        <div class="row">
                            <div class="col-md-4 col-lg-4 buttons">
                                <button id="edit_user" type="button" class="btn btn-danger " >Edit</button>
                                <button type="button" class="btn" id="creat" >Create</button>
                            </div>

                            <div class="col-md-4">
                                <div class="dropdown">
                                    <button class="btn btn-primary  dropdown-toggle" type="button" data-toggle="dropdown" id="more"  hidden>More
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" id="delete">Delete</a></li>
                                        <li><a href="#">Export</a></li>
                                        <li><a href="#">Duplicate</a></li>
                                        <li><a href="#">Print</a></li>

                                    </ul>
                                </div>
                            </div>
                           <!--search-->
                           <!--
                            <div class="col-md-4">
                                <form class="navbar-form navbar-right" >
                                    <div class="input-group form-group">
                                        <input type="text" value="" class="form-control" placeholder="Search ...">
                                    </div>
                                </form>
                            </div>
                            -->
                        </div> <!--collapse -->
              
                </div><!--collapse -->
                
                 <!--buttons row +second breadcrumb-->

                    <div class="row third-nav">
                        
                      
                        <div class="col-md-4 col-lg-4">
                        <div class="col-md-4 col-lg-4 col-sm-12 ">
                            <button  type="button" class="btn btn_third_nav" id="send_mail" >Send by Mail</button>
                        </div>

                        <div class="col-md-4 col-lg-4 col-sm-12">
                            <button  type="button" class="btn btn_third_nav" id="pay_invoice" >Approve</button>
                        </div>

                        <div class="col-md-4 col-lg-4 col-sm-12">
                            <button type="button" class="btn btn_third_nav " id="refund_invoice" >To Approve</button>
                        </div>

                           
                        </div>
                        <div class="col-md-8 col-lg-8 col-sm-12">
                             <span class="breadcrumbs">

                         <!--     <a href="#" class="breadcrumb">Draft Payment</a>
                               <a href="#" class="breadcrumb">To Approved</a>
                               <a href="#" class="breadcrumb">Approved </a>
                          -->
                                 <button type="button" class="btn btn-info btn-arrow-right">Info</button>
<button type="button" class="btn btn-warning btn-arrow-right">Warning</button>
<button type="button" class="btn btn-danger btn-arrow-right">Danger</button>

                            </span>
                        
                  
                    </div>
                
                
                
            </div>
        </nav>
        <!-- End Navbar-content -->

        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Pay Purchasing Invoice</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">

                                <div class="col-md-6 col-lg-6">



                                    <div class="form-group">
                                        <label class=" control-label col-md-6 col-lg-6 " for="datetime1">Document Date</label>

                                        <div  class=" col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <div class="input-group ">
                                                    <input type="text"  id="datetime1" class="form-control required " name="date">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar">
                                                        </i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>



                                    </div>

                                </div>
                        </div>
                        <br>
                        <div class="row">


                              <div class="col-md-6 col-lg-6">


                              <div class="form-group ">

                                <label class="col-md-6 col-lg-6 control-label">Journal Name</label>
                                  <div class="col-md-6 col-lg-6">

                                  <input type="text" id="journalName" class="form-control required"  name="journalName" >
                                  </div>


                            </div>
                              </div>

                          </div>
                        <br>
                        <div class="row">

                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="document" class="col-md-6 col-lg-6 control-label">Document Name</label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <select id="document" class="myselect1 required"   style="width: 50%" name="document">

                                                <option>doc-1</option>

                                                <option >doc-2</option>

                                                <option >doc-3</option>

                                                <option >doc-4</option>


                                            </select>
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="curr" class="col-md-6 col-lg-6 control-label">Currency </label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <select id="curr" class="myselect2 required"   style="width: 50%" name="curr">

                                                <option>$</option>

                                                <option >LE</option>

                                            </select>
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">

                                <div class="col-md-6 col-lg-6">

                                <div class="form-group ">

                                <label class="col-md-6 col-lg-6 control-label">Invoice Balance</label>
                                <div class="col-md-6 col-lg-6 ">
                                    0.00
                                </div>
                            </div>
                            </div>

                        </div>
                        <br>
                        <div class="row">

                                <div class="col-md-6 col-lg-6">
                                    <div class="form-group">

                                        <label class="col-md-6 col-lg-6 control-label">Invoice Number</label>
                                        <div class="col-md-6 col-lg-6">

                                            <select class="myselect3 required"  required  name="state">

                                                <option>1</option>

                                                <option >2</option>

                                                <option >3</option>

                                                <option >4</option>

                                                <option >5</option>


                                            </select>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <br>
                        <div class="row">

                                <div class="col-md-6 col-lg-6">

                                <div class="form-group ">

                                <label class="col-md-6 col-lg-6 control-label">Paid Value</label>
                                    <div class="col-md-6 col-lg-6">

                                    <input type="number" class="col-md-6 col-lg-6 form-control required" id="paidValue" value="0" min="1"   name="paidValue" >

                                    </div>
                            </div>
                            </div>

                        </div>

                    </div>
                 </div>
            <!-- END MAIN CONTENT -->
            </div>
        <!-- END MAIN -->
    </div>
    </div>

@endsection


@section('custom_footer')

    <script src="{{asset('js/item_name_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/tax_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/select2.min.js')}}"></script>



    <script src="{{asset('assets/scripts/bootstrap-datetimepicker.min.js')}}"></script>

<!--date/time picker-->
 
              <script>

$('#datetime1').datetimepicker({format: 'yyyy-mm-dd',autoclose: true, minView: 2});

$('#datetime2').datetimepicker({format: 'yyyy-mm-dd',autoclose: true, minView: 2});
$('#datetime3').datetimepicker({format: 'yyyy-mm-dd',autoclose: true, minView: 2});
                  
 
</script>

    {{--------------------  active side & nav bar ------------------------}}
    <script>
        $('.side_sheets').addClass('hide');
        $('#Purshase').removeClass('hide');

        $("#menu li").removeClass('active');
        $('#Purchase_nav_bar').addClass('active');

        $('#pages ul li a').removeClass('active');
        $('#pay_invoice_side_bar').addClass('active');

    </script>





                  
      <!--edit fields -->


    <script>
        $(document).on('click','#edit_user',function(){

            $(this).text("Save");
            $(this).attr('id','save_user');
            $("input[type=text]").attr('disabled', false).css({
                'border': '#eaeaea solid 1px',

                'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                'border-radius':'2px',
                'background-color':'#fff',
                'width':'150px',
                'margin-left':'15px'
            }).focus();

            $("input[type=password]").attr('disabled',false).css({
                'border': '#eaeaea solid 1px',

                'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                'border-radius':'2px',
                'background-color':'#fff',
                'margin-left':'15px'
            });

            $("input[type=email]").attr('disabled',false).css({
                'border': '#eaeaea solid 1px',

                'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                'border-radius':'2px',
                'background-color':'#fff',
                'margin-left':'15px'
            });



            $("select").removeAttr('disabled');
            $("input[type=radio]").attr('disabled', false);
            $("input[type=password]").attr('disabled', false);
            $("input[type=email]").attr('disabled', false);

        });


    </script>


    <script>
        $(".myselect1").select2({
            width: 'resolve'

        });

        $(".myselect2").select2({
            width: 'resolve'

        });

        $(".myselect3").select2({
            width: 'resolve'

        });


    </script>













    <!--add row to table -->
<script>
    $("#add_row").click(function () {

        $("#myTable2").each(function () {

            var tds = '<tr>';
            jQuery.each($('tr:last td', this), function () {
                tds += '<td>' + $(this).html() + '</td>';
            });
            tds += '</tr>';
            if ($('tbody', this).length > 0) {
                $('tbody', this).append(tds);
            } else {
                $(this).append(tds);
            }
        });
    });


</script>

    <!--End add row to table -->


<!--delete row in table -->
    <script>

        $(document).on('click', 'button#delete_row', function () {

            $(this).closest('tr').remove();
            return false;
        });

    </script>
    <!--Edit Row in Table -->

<script>
    
        $(document).on('click','#editbtn',function(){
              var currentTD = $(this).parents('tr').find('td');
              if ($(this).html() == 'Edit') {
                  currentTD = $(this).parents('tr').find('td');
                  $.each(currentTD, function () {
                      
                      
                      
                      
                $("input[type=text]").attr('disabled', false).css({
                'border': '#eaeaea solid 1px',
                'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                'border-radius':'2px',
                'background-color':'#fff',
                'width':'150px',
                'margin-left':'15px'
            }).focus();

            $("input[type=number]").attr('disabled',false).css({
                'border': '#eaeaea solid 1px',

                'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                'border-radius':'2px',
                'background-color':'#fff',
                'margin-left':'15px'
            });

          


            $("select").removeAttr('disabled');

        
                  });
                 
                  
              } else {
                 $.each(currentTD, function () {
               $("input[type=text]").attr('disabled', true).css({
                'border': '#eaeaea solid 1px',

                'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                'border-radius':'2px',
                'background-color':'#fff',
                'width':'150px',
                'margin-left':'15px'
            }).focus();

            $("input[type=number]").attr('disabled',true).css({
                'border': '#eaeaea solid 1px',

                'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                'border-radius':'2px',
                'background-color':'#fff',
                'margin-left':'15px'
            });

                   $("select").prop("disabled", true);
        
                  });
                  
                  
              }
    
              $(this).html($(this).html() == 'Edit' ? 'Save' : 'Edit')
    
          });

   



@endsection