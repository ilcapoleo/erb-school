@extends('layouts.main_app')

@section('main_content')


    <style>

        input.col-md-4.text-center {

            border: 0;
            background-color: #fff;
        }

        input[type=number] {
            width: 80px;
        }

        .collapse > .row > .col-md-4 > .btn, .collapse > .row > .col-md-4 > .dropdown > .btn {
            margin-top: 0;

        }

        .btn {
            padding: 3px 10px;
            margin-top: 7%;
        }

        .third-nav {
            background-color: #eeeeee;
        }

        .approved {
            margin-left: 24%;
        }

        .red-border {
            border: 1px solid red !important;
        }
    </style>
    <link href="{{asset('css/select2.min.css')}}" rel="stylesheet"/>
    <div class="main">


        <!-- Navbar-content -->
        <nav class="navbar navbar-default navbar2">

            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">



                    <!--breadcrumbs-->

                    <div class="row">
                        @if(isset($nationality))
                            <div id="set_po_id" current_po_id="{{$nationality->id}}" current_po_status="" closed_or_not=""
                                 current_id="{{Auth::user()->id}}"></div>
                        @else
                            <div id="set_po_id" current_po_id="" current_po_status="draft_po" closed_or_not="0"
                                 current_id="{{Auth::user()->id}}"></div>
                        @endif

                        <div class="col-md-10 col-lg-10 col-sm-8 col-xs-8">

                            <ol class="breadcrumb breadcrumb_nav">
                                <li><a href="/nationalities">Nationality</a></li>
                                <li class="active" id="second">

                                </li>
                            </ol>
                        </div>

                        @if(isset($nationality))
                            <div class="col-md-2 col-lg-2 col-sm-4 col-xs-4">
                                @if(@\App\Nationality::where('id','<',$nationality->id)->orderBy('id','desc')->first()->id)
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <a href="/nationalities/{{@\App\Nationality::where('id','<',$nationality->id)->orderBy('id','desc')->first()->id}}"><span
                                                    class="glyphicon glyphicon-arrow-left"></span>
                                        </a>
                                    </div>
                                @endif
                                @if(@\App\Nationality::where('id','>',$nationality->id)->orderBy('id','asc')->first()->id)
                                    <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                                        <a href="/nationalities/{{@\App\Nationality::where('id','>',$nationality->id)->orderBy('id','asc')->first()->id}}">
                                    <span class="glyphicon glyphicon-arrow-right">
                                </span>
                                        </a>
                                    </div>
                                @endif
                            </div>
                        @endif


                    </div>

                        <!--End breadcrumbs-->


                    <div class="row">
                        <div class="col-lg-4  col-md-4 col-sm-6 col-xs-6 buttons">
                            @if(isset($nationality))
                                <button id="edit_dep" type="button" class="btn btn-danger ">Edit</button>
                                <button type="button" class="btn" id="creat_dep">Create</button>
                                @else
                            <button id="save_dep" type="button" class="btn btn-danger ">Save</button>
                            {{--<button id="edit_user" type="button" class="btn btn-danger " >Edit</button>--}}
                            <button type="button" class="hide btn" id="creat_dep">Create</button>
                                @endif
                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-4 ">
                            @if(isset($nationality))
                                <div class="dropdown">
                                    <button class=" btn btn-primary  dropdown-toggle" type="button"
                                            data-toggle="dropdown" id="more" hidden>More
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" data-toggle="modal" data-target="#exampleModal">Delete</a></li>
                                        <!-- temporery -->  {{--<li><a href="#">Export</a></li>--}}
                                        {{--<li><a href="#" id="dublicate_dep">Duplicate</a></li>--}}
                                        <li><a href="#" id="print">Print</a></li>

                                    </ul>
                                </div>
                                @else
                            <div class="dropdown">
                                <button class="hide btn btn-primary  dropdown-toggle" type="button"
                                        data-toggle="dropdown" id="more" hidden>More
                                    <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#" data-toggle="modal" data-target="#exampleModal">Delete</a></li>
                                    {{--<li><a href="#">Export</a></li>
                                    <li><a href="#" id="dublicate_dep">Duplicate</a></li>--}}
                                    <li><a href="#" id="print">Print</a></li>

                                </ul>
                            </div>
                                @endif
                        </div>
                        <!--search-->
                        <!--
                         <div class="col-md-4">
                             <form class="navbar-form navbar-right" >
                                 <div class="input-group form-group">
                                     <input type="text" value="" class="form-control" placeholder="Search ...">
                                 </div>
                             </form>
                         </div>
                         -->
                    </div> <!--collapse -->




                </div><!--collapse -->

            </div>
        </nav>
        <!-- End Navbar-content -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
             aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Purchase Order</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        Are You Sure to Delete this Nationality ?!!
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-danger fa fa-trash" id="delete_dep">Delete
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Nationality Details</h3>
                    </div>
                    <div class="panel-body">


                        <div class="row">

                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <div class="col-md-6 col-lg-6">
                                        <div class="form-group">

                                            <label class="col-md-6 col-lg-6 control-label">Nationality Name</label>
                                            <div class="col-md-6 col-lg-6">
                                                <div class="form-group">
                                                    @if(isset($nationality))
                                                        <input id="nationality_name" type="text"
                                                               class="set_all_disabled get_all_date_input form-control required err_input job_name_error"
                                                               value="{{$nationality->name}}" disabled>
                                                    @else
                                                        <input id="nationality_name" type="text"
                                                               class="set_all_disabled get_all_date_input form-control required err_input job_name_error"
                                                               value="">
                                                    @endif

                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- END MAIN CONTENT -->
            </div>
            <!-- END MAIN -->


        </div>
    </div>




@endsection


@section('custom_footer')

    <script src="{{asset('assets/scripts/bootstrap-datetimepicker.min.js')}}"></script>

    <script src="{{asset('js/item_name_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/tax_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/select2.min.js')}}"></script>

    {{--------------------  active side & nav bar ------------------------}}
    <script>
        $('.side_sheets').addClass('hide');
        $('#HR').removeClass('hide');

        $("#menu li").removeClass('active');
        $('#hr_nav_bar').addClass('active');

        $('#pages ul li a').removeClass('active');
        $('#nationality_side_bar').addClass('active');

    </script>

    {{-------------------- save new job --------------------}}
    <script>
        $('.alert-autocloseable-success').hide();

        $(document).on('click', '#save_dep', function () {

            $('#save_dep').attr('disabled', 'disabled');


            var nationality_name = $('#nationality_name').val();
            var current_po_id = $('#set_po_id').attr('current_po_id');

            console.log('current_po_id' + current_po_id)
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/add_new_nationality/',
                dataType: 'json',
                type: 'get',
                data: {

                    nationality_name: nationality_name,
                    current_po_id: current_po_id


                },

                success: function (response) {

                    console.log(response);
                    if (response['error']) {
                        $('.err_input').removeClass('red-border');
                        $('.err_input').parent().find('.select2').removeClass('red-border');



                        $.each(response['error'], function (key, val) {
                            //alert(key + val);

                            console.log(key + '_error');
                            $("." + key + '_error').addClass('red-border');
                            $('.' + key + '_error').parent().find('.select2').addClass('red-border');
                            notification('glyphicon glyphicon-warning-sign','Warning',val+'!','danger')
                            //console.log(val[0]);

                        })
                        $('#save_dep').removeAttr('disabled');

                    }

                    else {
                        $('#set_po_id').attr('current_po_id', response['current_po_id']);
                        $('.errorMessage1').hide();
                        $('.err_input').removeClass('red-border');
                        $('.err_input').parent().find('.select2').removeClass('red-border');


                        $('.breadcrumb').removeClass('po-active');


                        $('#save_dep').text("Edit");
                        $('#save_dep').attr('id', 'edit_dep');

                        $('.set_all_disabled').prop('disabled', true);

                        $('#creat_dep').removeClass('hide');

                        $('#more').removeClass('hide');


                        $('#edit_dep').removeAttr('disabled');
                        //po-active
                        console.log(response);

                        notification('glyphicon glyphicon-ok-sign','Congratulation!','Jobs Saved Succefully.','success')

                    }

                }

            });

        });

    </script>
    {{----------------------- edit department -----------------------}}
    <script>
        $(document).on('click', '#edit_dep', function () {
            $('#edit_dep').text("Save");
            $('#edit_dep').attr('id', 'save_dep');

            $('.set_all_disabled').prop('disabled', false);


            $('#creat_dep').addClass('hide');

            $('#more').addClass('hide');

        });
    </script>

    {{---------------------------- create new  ------------------------------}}
    <script>
        $(document).on('click', '#creat_dep', function () {/*
            $('#edit_dep').prop('disabled', false);
            $('#edit_dep').text("Save");
            $('#edit_dep').attr('id', 'save_dep');
            $('#save_dep').prop('disabled', false);

            $('.set_all_disabled').prop('disabled', false);

            $('#set_po_id').attr('current_po_id', "");
            /!*$('#set_po_id').attr('current_po_status',"draft_po");*!/
            $('#creat_dep').addClass('hide');
            $('#more').addClass('hide');

            $('.get_all_selectors').val('');

            $(".get_all_selectors option[value='']").attr('selected', true);
            $('.get_all_date_input').val('');
            $('.select2-selection__rendered').html('');*/
            window.location.href = '/nationalities/create';

        });
    </script>
    {{---- dublicate dep ---}}
    <script>
        $(document).on('click', '#dublicate_dep', function () {

            $('#set_po_id').attr('current_po_id', "");
            /*$('#set_po_id').attr('current_po_status',"draft_po");*/
            /*$('#set_po_id').attr('closed_or_not',0);*/

            $('.set_all_disabled').prop('disabled', false);


            $('#creat_dep').addClass('hide');
            $('#more').addClass('hide');


            $('#edit_dep').prop('disabled', false);
            $('#edit_dep').text("Save");
            $('#edit_dep').attr('id', 'save_dep');
            $('#save_dep').prop('disabled', false);
        });
    </script>

    {{----------------------  delete department ------------------------------------}}

    <script>
        $(document).on('click', '#delete_dep', function () {
            var cur_po_id = $('#set_po_id').attr('current_po_id');

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/delete_this_nationality/',
                dataType: 'json',
                type: 'get',
                data: {
                    cur_po_id: cur_po_id,
                },

                success: function (response) {

                    window.location.href = '/nationalities';
                }

            });

        });
    </script>

    {{-----------   on leave page -------------------}}
    <script type='text/javascript'>

        $(window).bind('beforeunload', function(){
            if($('#save_dep').length){
                return 'Are you sure you want to leave?';
            }

        });


    </script>
    {{------------------------------------------}}
    <script>
        $(document).ready(function () {
            $(".select_2_enable").select2();

        });
    </script>

    <script>

        $(document).on('click', '#print', function () {

            //  window.location.href = 'get_po2/276'

            var id = $('#set_po_id').attr('current_po_id');
            window.open('/get_nationality_rpt/' + id + '', '_blank');


        });


    </script>



@endsection