@extends('layouts.main_app')

@section('main_content')
   <style>
        input.col-md-4.text-center {

            border:0;
            background-color: #fff;
        }
        

        
        .collapse > .row>.col-md-4>.btn, .collapse > .row>.col-md-4>.dropdown>.btn {
    margin-top: 0;
            
}
        
        .third-nav{
            background-color:#eeeeee;
        }
       
       .alert-success{
           display: none;
       }
        .panel .table > thead > tr > td:first-child, .panel .table > thead > tr > th:first-child, .panel .table > tbody > tr > td:first-child, .panel .table > tbody > tr > th:first-child, .panel .table > tfoot > tr > td:first-child, .panel .table > tfoot > tr > th:first-child{
            padding-left: 0px;
        }

        .row {
            margin-right: -10px;
            margin-left: -10px;
        }

        .red-border{
            border: 1px solid red  !important;
        }

        .error{
            border: 1px solid red  !important;
            border-left: 1px double red !important;
            border-top: 2px double red !important;
        }


        #yes,#no{
            display:none;
            margin-right: 5px;
        }

        .select2-container{
            width: 100%!important;
        }
   </style>

   <link href="{{asset('css/fastselect.min.css')}}" rel="stylesheet">
   <link href="{{asset('css/select2.min.css')}}" rel="stylesheet" />


    <div class="main">
        <!-- Navbar-content -->
        <nav class="navbar navbar-default navbar2">

            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                    <!--breadcrumbs-->
                    <div class="row">

                        <div class="col-md-10 col-lg-10 col-sm-6 col-xs-6">

                        <ol class="breadcrumb breadcrumb_nav">
                            <li><a href="/show_invoice">Create Invoice</a></li>
                            <li class="active" id="second">
                                @if(isset($master_invoice_details))
                                    @if($master_invoice_details->invoice_seq != 0)
                                        {{$master_invoice_details->invoice_seq}}
                                    @else
                                        Draft
                                    @endif
                                @endif
                            </li>
                        </ol>
                        </div>

                        @if(isset($master_invoice_details))
                            <div class="col-md-2 col-lg-2 ">
                                @if(@\App\invoice_master::where('invoice_m_id','<',$master_invoice_details->invoice_m_id)->orderBy('invoice_m_id','desc')->first()->invoice_m_id)
                                    <div class="col-md-6">
                                        <a href="/inventory_acc/{{@\App\invoice_master::where('invoice_m_id','<',$master_invoice_details->invoice_m_id)->orderBy('invoice_m_id','desc')->first()->invoice_m_id}}"><span
                                                    class="glyphicon glyphicon-arrow-left"></span>
                                        </a>
                                    </div>
                                @endif
                                @if(@\App\invoice_master::where('invoice_m_id','>',$master_invoice_details->invoice_m_id)->orderBy('invoice_m_id','asc')->first()->invoice_m_id)
                                    <div class="col-md-6 col-lg-6 col-sm-3 col-xs-3">
                                        <a href="/inventory_acc/{{@\App\invoice_master::where('invoice_m_id','>',$master_invoice_details->invoice_m_id)->orderBy('invoice_m_id','asc')->first()->invoice_m_id}}">
                                    <span class="glyphicon glyphicon-arrow-right">
                                </span>
                                        </a>
                                    </div>
                                @endif
                            </div>
                        @endif


                    </div>   <!--End breadcrumbs-->


                    <div class="row">

                        <div class="col-md-4 col-lg-4 col-sm-6 col-xs-6 buttons">
                            {{--<button id="edit_user" type="button" class="btn btn-danger " >Edit</button>--}}
                            @if(isset($master_invoice_details))
                                @if($master_invoice_details->invoice_doc_status == 3 || $master_invoice_details->invoice_doc_status == 4)
                                    <button id="edit_po" type="button" class="btn btn-danger " >Edit</button>
                                    <button type="button" class="btn" id="creat_po" >Create</button>
                                @else
                                    {{--<button id="save_po" type="button" class="btn btn-danger " >Save</button>
                                    <button type="button" class="btn hide" id="creat_po" >Create</button>--}}
                                    <button id="edit_po" type="button" class="btn btn-danger " >Edit</button>
                                    <button type="button" class="btn" id="creat_po" >Create</button>
                                @endif
                            @else
                                <button id="save_po" type="button" class="btn btn-danger " >Save</button>
                                <button type="button" class="btn hide" id="creat_po" >Create</button>
                            @endif
                        </div>



                        <div class="col-md-4 col-lg-4 col-sm-6 col-xs-6">
                            @if(isset($master_invoice_details))

                                @if($master_invoice_details->invoice_doc_status == 3 || $master_invoice_details->invoice_doc_status == 4|| $master_invoice_details->invoice_doc_status == 2)
                                    <div class="dropdown">
                                        <button class="btn btn-primary  dropdown-toggle" type="button" data-toggle="dropdown" id="po_more"  hidden>More
                                            <span class="caret"></span></button>
                                        <ul class="dropdown-menu">

                                            <li class="hide"><a href="#" id="delete_po">Delete</a></li>
                                            <!-- temporery -->  <!--  <li><a href="#">Export</a></li> -->
                                            {{--<li><a href="#" id="dublicate_po">Duplicate</a></li>--}}
                                            <li><a href="#" id="print">Print</a></li>

                                        </ul>
                                    </div>
                                @elseif($master_invoice_details->invoice_doc_status == 1)
                                    <div class="dropdown">
                                        <button class="btn btn-primary  dropdown-toggle" type="button" data-toggle="dropdown" id="po_more"  hidden>More
                                            <span class="caret"></span></button>
                                        <ul class="dropdown-menu">

                                            <li class=""><a href="#" id="delete_po">Delete</a></li>
                                            <li><a href="#">Export</a></li>
                                            <li><a href="#" id="dublicate_po">Duplicate</a></li>
                                            <li><a href="#" id="print">Print</a></li>

                                        </ul>
                                    </div>
                                @else
                                    <div class="dropdown">
                                        <button class="hide btn btn-primary  dropdown-toggle" type="button" data-toggle="dropdown" id="po_more"  hidden>More
                                            <span class="caret"></span></button>
                                        <ul class="dropdown-menu">

                                            <li class=""><a href="#" id="delete_po">Delete</a></li>
                                            <li><a href="#">Export</a></li>
                                            {{--<li><a href="#" id="dublicate_po">Duplicate</a></li>--}}
                                            <li><a href="#" id="print">Print</a></li>

                                        </ul>
                                    </div>
                                @endif
                            @else
                                <div class="dropdown">
                                    <button class="hide btn btn-primary  dropdown-toggle" type="button" data-toggle="dropdown" id="po_more"  hidden>More
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">

                                        <li class="hide"><a href="#" id="delete_po">Delete</a></li>
                                        <li><a href="#">Export</a></li>
                                        {{--<li><a href="#" id="dublicate_po">Duplicate</a></li>--}}
                                        <li><a href="#" id="print">Print</a></li>

                                    </ul>
                                </div>
                            @endif


                        </div>
                        <!--search-->
                        <!--
                         <div class="col-md-4">
                             <form class="navbar-form navbar-right" >
                                 <div class="input-group form-group">
                                     <input type="text" value="" class="form-control" placeholder="Search ...">
                                 </div>
                             </form>
                         </div>
                         -->
                    </div>
                    <!--collapse -->



                </div><!--collapse -->
                
                 <!--buttons row +second breadcrumb-->

                <div class="row third-nav">


                    <div class="col-md-6 col-lg-6">

                        <div class="col-md-3 col-lg-3 col-sm-12 ">
                            <button id="send_mail"  type="button" class="{{--set_all_disabled--}} btn btn_third_nav" >Send by Mail</button>
                        </div>

                        <div class="col-md-3 col-lg-3 col-sm-12">
                            <button id="sendto_approved_btn" type="button" class="set_all_disabled btn btn_third_nav" @if(isset($master_invoice_details)) disabled @endif >To Approve</button>
                        </div>

                        <div class="col-md-3 col-lg-3 col-sm-12">
                            <button id="approved_btn" type="button" class="set_all_disabled btn approved btn_third_nav " @if(isset($master_invoice_details)) disabled @endif >Approved</button>
                        </div>

                        <div class="col-md-3 col-lg-3 col-sm-12">
                            <button type="button" class="set_all_disabled btn approved btn_third_nav" data-toggle="modal" data-target="#exampleModal">Pay Invoice</button>
                        </div>

                    </div>


                    <div class="col-md-6 col-lg-6 col-sm-12">


                            <span id="current_choosed_status" coosed_status="" class="breadcrumbs ">
                                 @if(isset($master_invoice_details))

                                    <span id="draft_po"  class="@if($master_invoice_details->invoice_doc_status == 1) po-active @endif breadcrumb">Draft Invoice</span></li>
                                    <span id="to_approve_po"  class="@if($master_invoice_details->invoice_doc_status == 2) po-active @endif breadcrumb">To Approved Invoice</span></li>
                                    <span id="approved_po"  class="@if($master_invoice_details->invoice_doc_status == 3) po-active @endif  breadcrumb ">Approved Invoice</span></li>


                                @else
                                    <span id="draft_po"  class="breadcrumb">Draft Invoice</span></li>
                                    <span id="to_approve_po"  class="breadcrumb">To Approved Invoice</span></li>
                                    <span id="approved_po"  class="breadcrumb ">Approved Invoice</span></li>

                                @endif
                            </span>



                    </div>
                </div>
                
                
                
            </div>
        </nav>
        <!-- End Navbar-content -->


        <!-- Success messages -->
        <div class="alert alert-success alert-autocloseable-success" style="display:none;position:absolute;" >
            Your PO Saved Successfully!

        </div>
        <!--end Success messages -->

        <!-- failuer messages -->
        <div class="alert alert-danger errorMessage1" style="display:none;position:absolute;" >
            Your should fill all fields!

        </div>
        <!--end failuer messages -->








        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                 <!--success Message-->
        
        <div class="alert alert-success " role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <strong>Success!</strong> You have been paid in successfully!
                </div>
                
                
                
                
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Purchase Order</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="row">

                                {{-- set po sitting--}}
                                @if(isset($master_invoice_details))
                                    <div id="set_po_id"  current_invoice_id="{{$master_invoice_details->invoice_m_id}}" current_po_id="{{$master_invoice_details->doc_id}}" current_po_status="{{$master_invoice_details->status_name}}" closed_or_not="" current_id="{{Auth::user()->id}}"></div>
                                @else
                                    <div id="set_po_id" current_invoice_id="" current_po_id="" current_po_status="draft_po" closed_or_not="0" current_id="{{Auth::user()->id}}"></div>
                                @endif
                                <div class="row">

                                    <div class="row">
                                        <div class="col-md-12 col-lg-12">
                                        <div class="col-md-6 col-lg-6">
                                            <div class="form-group">

                                                <label class="col-md-6 col-lg-6 control-label">Supplier</label>
                                                <div class="col-md-6 col-lg-6">

                                                    <select id="supplier" name="supplier" class="get_all_selectors  select_2_enable form-control required  err_input supplier_error" @if(isset($master_invoice_details)) disabled  @endif >
                                                        <option value=""> </option>
                                                        @foreach($supplier as $id=>$role)
                                                            @if(isset($master_invoice_details))
                                                                @if($master_invoice_details->supplier_id == $id)
                                                                    <option selected value="{{$id}}">{{$role}}</option>
                                                                @else
                                                                    <option value="{{$id}}">{{$role}}</option>
                                                                @endif
                                                            @elseif(isset($master_inventory_details))
                                                                @if($master_inventory_details->supplier_id == $id)
                                                                    <option selected value="{{$id}}">{{$role}}</option>
                                                                @else
                                                                    <option value="{{$id}}">{{$role}}</option>
                                                                @endif
                                                            @else
                                                                <option value="{{$id}}">{{$role}}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-6">

                                                                <div class="form-group">
                                                                    <label class=" control-label col-md-6 col-lg-6 " for="datetime1" >Document Date </label>

                                                                        <div  class=" col-md-6 col-lg-6">
                                                                            <div class="form-group">

                                                                    <div class="input-group ">

                                                                        @if(isset($master_invoice_details))
                                                                            <input type="text" id="datetime1" class="set_all_disabled form-control required err_input document_date_error " crrent_date="{{$curr_date}}" name="date" value="{{$master_invoice_details->invoice_date}}" @if(isset($master_invoice_details))disabled @endif>
                                                                        @else
                                                                            <input type="text" id="datetime1" class="set_all_disabled form-control  required err_input document_date_error" crrent_date="{{$curr_date}}" name="date" value="{{$curr_date}}">
                                                                        @endif

                                                                        <div class="input-group-addon">
                                                                            <i class="fa fa-calendar">
                                                                            </i>
                                                                        </div>



                                                                    </div>

                                                                            </div>
                                                                        </div>

                                                                </div>



                                        </div>
                                        </div>
                                    </div>
                                {{--------------  error row------------------}}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <div id="supplier_error" class="alert alert-danger errorMessage hide ">
                                                <strong >Wrong Input!</strong>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div id="document_date_error" class="alert alert-danger errorMessage hide ">
                                                <strong >Wrong Input!</strong>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                {{----------------------------------------------}}
                                <div class="row">
                                    <div class="col-md-12 ol-lg-12">
                                        <div class="col-md-6 col-lg-6">
                                            <div class="form-group">

                                                <label class="col-md-6 col-lg-6 control-label">Inventory Document Number</label>
                                                <div class="col-md-6 col-lg-6">

                                                    <select id="inventory_drop_down" name="inventory_drop_down" class="get_all_selectors  select_2_enable form-control required err_input inventory_error" @if(isset($master_invoice_details)) disabled  @endif>
                                                        <option value=""> </option>
                                                        @if(isset($master_invoice_details))
                                                            <option value="{{$master_invoice_details->doc_id}}" selected>{{$master_invoice_details->doc_seq	}}</option>
                                                        @elseif(isset($master_inventory_details))
                                                            <option value="{{$master_inventory_details->doc_id}}" selected>{{$master_inventory_details->doc_seq	}}</option>
                                                        @endif
                                                    </select>

                                                </div>

                                            </div>
                                        </div>


                                        <div class="col-md-6 col-lg-6">
                                            <div class="form-group">

                                                <label class="col-md-6 col-lg-6 control-label">Invoice Reference</label>
                                                <div class="col-md-6 col-lg-6">

                                                    <input id="invoice_reference" type="text" name="invoice_reference" class=" set_all_disabled get_all_date_input  form-control" @if(isset($master_invoice_details)) value="{{$master_invoice_details->invoice_ref}}"  disabled @endif ></input>

                                                </div>
                                            </div>
                                        </div>



                                    </div>

                                </div>

                                {{--------------  error row------------------}}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <div id="inventory_error" class="alert alert-danger errorMessage hide ">
                                                <strong >Wrong Input!</strong>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div id="invoice_reference_error" class="alert alert-danger errorMessage hide ">
                                                <strong >Wrong Input!</strong>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                {{----------------------------------------------}}
                                <br>
                                <div class="row">
                                    <div class="col-md-12 col-lg-12">
                                        <div class="col-md-6 col-lg-6">
                                            <div class="form-group">

                                                <label class="col-md-6 col-lg-6 control-label">Currency</label>
                                                <div class="col-md-6 col-lg-6">

                                                    <select name="currency" id="currency" class="get_all_selectors  form-control err_input currency_error" @if(isset($master_invoice_details)) disabled @endif  disabled>
                                                        <option value=""> </option>
                                                        @foreach($currencies as $id=>$role)
                                                            @if(isset($master_invoice_details))
                                                                @if($master_invoice_details->inv_currency_id == $id)
                                                                    <option selected value="{{$id}}">{{$role}}</option>
                                                                @else
                                                                    <option value="{{$id}}">{{$role}}</option>
                                                                @endif
                                                            @elseif(isset($master_inventory_details))
                                                                @if($master_inventory_details->CurrencyID == $id)
                                                                    <option selected value="{{$id}}">{{$role}}</option>
                                                                @else
                                                                    <option value="{{$id}}">{{$role}}</option>
                                                                @endif
                                                            @else
                                                                <option value="{{$id}}">{{$role}}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-6">
                                        </div>

                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <div id="currency_error" class="alert alert-danger errorMessage hide ">
                                                <strong >Wrong Input!</strong>
                                            </div>
                                        </div>


                                    </div>

                                </div>
                                  </div>


                        <div class="row">

                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#documentDetails">Document Details</a></li>
                                    <li><a href="#PaymentTerms">Payment Terms</a></li>

                                </ul>

                                <div class="tab-content">
                                    <div id="documentDetails" class="tab-pane fade in active table-responsive">

                                        <table  class="table table-bordred table-striped table-bordered table-striped table-hover table-condensed table-responsive " id="op_table" >
                                            <thead>
                                                <th>PO</th>
                                                <th >Items</th>
                                                <th >Quantity</th>
                                                <th >Price</th>
                                                <th >Include</th>
                                                <th >Tax</th>
                                                <th >Sub Total</th>
                                                <th >Total Tax</th>
                                                <th>Edit</th>
                                                <th>Delete</th>

                                            </thead>
                                            <tbody>

                                            @if(isset($inventory_details_data))
                                                <?php $i=0 ?>
                                                @foreach($inventory_details_data as $inventory_details_data_single)
                                                <tr class="" data-select2-id="75">
                                                    <td td_type="" disabled="" hidden="" current_value="0" yes_or_no="No" class="include_check_colsed_v"><div class="include_check_colsed"></div></td>
                                                    <td td_type="search" data-select2-id="74" class="po_values_v" current_value="{{$inventory_details_data_single->po_id}}">{{$inventory_details_data_single->po_seq}}</td>
                                                    <td td_type="search" now_selected="{{$inventory_details_data_single->pro_id}}" data-select2-id="98" class="item_product_v" current_value="{{$inventory_details_data_single->pro_id}}" seq="0">{{$inventory_details_data_single->pro_name}}</td>
                                                    <td td_type="input" maxmum_quan="{{$max_quantity_array[$i] + $inventory_details_data_single->qut}}" class="quantity_value_v" current_value="{{$inventory_details_data_single->qut}}">{{$inventory_details_data_single->qut}}</td>
                                                    <td td_type="input" class="unit_price_v" current_value="{{$inventory_details_data_single->unit_price}}">{{$inventory_details_data_single->unit_price}}</td>
                                                    <td td_type="check_box" current_value="{{$inventory_details_data_single->include_tax}}" yes_or_no="{{$include_string[$i]}}" class="include_check_v check_box_v">{{$include_string[$i]}}</td>
                                                    @if(isset($tax_invoice))

                                                    <td td_type="drop_down" class="tax_value_v" get_value="{{$inventory_details_data_single->tax_string}}" current_value="{{$inventory_details_data_single->tax_string}}">{{$inventory_details_data_single->tax_string}}</td>

                                                    @endif

                                                    <td td_type="input" class="sub_total_v" current_value="{{$sub_total_array[$i]}}">{{$sub_total_array[$i]}}</td>
                                                    <td td_type="input" class="total_row_tax_v" current_value="{{$total_tax_array[$i]}}">{{$total_tax_array[$i]}}</td>

                                                    <?php $i++ ?>
                                                    <td><button class="set_all_disabled btn btn-primary btn-xs edit_row" disabled>Edit</button></td>
                                                    <td><button class="set_all_disabled btn btn-danger btn-xs delete_row" disabled><span class="lnr lnr-trash"></span></button> </td>
                                                </tr>
                                                @endforeach
                                            @endif
                                            </tbody>
                                        </table>

                                           <div class="row">
                                               <div class="col-md-4 col-lg-4 ">
                                                   <button type="button" class=" set_all_disabled btn btn-primary" id="add_row" @if(isset($master_invoice_details)) disabled @endif >Add Row</button>
                                               </div>
                                          </div>

                                        
                                        <hr>

                                        <div class="row">

                                            <div class="col-md-3 col-lg-3 ">
                                                <div class="form-group">
                                                    <label for="comment">Add your note :</label>
                                                    <textarea class="set_all_disabled form-control" rows="5" id="po_comment" @if(isset($master_invoice_details)) disabled @endif >@if(isset($master_invoice_details)) {{$master_invoice_details->invoice_notes}} @endif</textarea>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-lg-3 ">
                                                <label for="comment"></label>

                                                <table  class="table table-bordred table-striped table-bordered table-striped table-hover table-condensed table-responsive" id="tax_details_table" >
                                                    <thead>
                                                    <th>Tax Name</th>
                                                    <th >Tax Value</th>

                                                    </thead>
                                                    <tbody>
                                                    @if(isset($po_details_tax_array))
                                                        @foreach($po_details_tax_array as $key=>$val)
                                                            <tr>
                                                                <th >{{$key}}</th>
                                                                <td>{{$val}}</td>

                                                            </tr>
                                                        @endforeach
                                                    @endif
                                                    </tbody>
                                                </table>




                                            </div>


                                            <div class="col-md-6 col-lg-6 ">
                                                <div class="col-md-12 col-lg-12 ">
                                                    <div class="col-md-4 col-lg-4 ">Untaxes Amount:</div>
                                                    <div id="untaxed_all" class="col-md-4 col-lg-4 " >0.00  </div><span class="currency_value"></span>

                                                </div>
                                                <br>
                                                <div class="col-md-12 col-lg-12 ">
                                                    <div class="col-md-4 col-lg-4 ">Taxes:</div>
                                                    <div id="taxed_sum" class="col-md-4  col-lg-4" >0.00 </div><span class="currency_value"></span>
                                                </div>

                                                <hr>
                                                <div class="col-md-12 col-lg-12 ">
                                                    <div class="col-md-4  col-lg-4">Total:</div>
                                                    <div  id="total_sum" class="col-md-4 col-lg-4" >0.00 </div><span class="currency_value"></span>
                                                </div>




                                            </div>

                                        </div>



                                </div>
                                    
                                    <div id="PaymentTerms" class="tab-pane fade">
                              <table  class="table table-bordred table-striped table-bordered table-striped table-hover table-condensed table-responsive text-center" id="myTable2" >
                                            <thead>
                                            <th>Journal Name</th>
                                            <th >Payment Date</th>
                                            <th >Payment Value</th>
                                            <th>Paid Value</th>
                                           
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td  >
                                                  journal1
                                                </td>
                                                
                                                
                                                
                                                
                                                <td >
                                             
                                  <p><span id="datetime"></span></p>

                                       
                                                </td>
                                                
                                                
                                                
                                                <td >
                                                      0.00                                                     
                                                    
                                                </td>
                                                  <td >
                                                        0.00                                                    
                                                    
                                                </td>
                                               

                                                
                                            </tr>
                                            </tbody>
                                        </table>

                                        
                                        <hr>

                                        <div class="row">

                                            <div class="col-md-6 col-lg-6 ">
                                              
                                            </div>

                                            <div class="col-md-6 col-lg-6">

                                               
                                                <div class="col-md-12 col-lg-12">
                                                    <div class="col-md-4 col-lg-4">Total Payment:</div>
                                                    <div class="col-md-4 col-lg-4" >0.00 </div>
                                                </div>


                                            </div>

                                        </div>



                            </div>
                                    
                            </div>
                        </div>

                        
                    </div>
                 </div>
            <!-- END MAIN CONTENT -->
            </div>
        <!-- END MAIN -->
    </div>
        

        <!--Paid Pop-up -->
        
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content " id="formContent">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pay Invoice</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
                  <form>
                      <div class="row">

                          <div class="col-md-12 col-lg-12">



                              <div class="form-group">
                                  <label class=" control-label col-md-6 col-lg-6 " for="datetime2">Document Date</label>

                                  <div  class=" col-md-6 col-lg-6">
                                      <div class="form-group">
                                          <div class="input-group ">
                                              <input type="text"  id="datetime2" class="form-control required " name="date">
                                              <div class="input-group-addon">
                                                  <i class="fa fa-calendar">
                                                  </i>
                                              </div>
                                          </div>
                                      </div>
                                  </div>



                              </div>

                          </div>
                      </div>
                      <br>
                      <div class="row">


                          <div class="col-md-12 col-lg-12">


                              <div class="form-group ">

                                  <label class="col-md-6 col-lg-6 control-label">Journal Name</label>
                                  <div class="col-md-6 col-lg-6">

                                      <input type="text" id="journalName" class="form-control required"  name="journalName" >
                                  </div>


                              </div>
                          </div>

                      </div>
                      <br>
                      <div class="row">

                          <div class="col-md-12 col-lg-12">
                              <div class="form-group">
                                  <label for="document" class="col-md-6 col-lg-6 control-label">Document Name</label>

                                  <div class=" col-md-6 col-lg-6">

                                      <div class="form-group">
                                          <select id="document" class="myselect1 required"   style="width: 50%" name="document">

                                              <option>doc-1</option>

                                              <option >doc-2</option>

                                              <option >doc-3</option>

                                              <option >doc-4</option>


                                          </select>
                                      </div>

                                  </div>

                              </div>

                          </div>

                      </div>
                      <br>
                      <div class="row">
                          <div class="col-md-12 col-lg-12">
                              <div class="form-group">
                                  <label for="curr" class="col-md-6 col-lg-6 control-label">Currency </label>

                                  <div class=" col-md-6 col-lg-6">

                                      <div class="form-group">
                                          <select id="curr" class="myselect2 required"   style="width: 50%" name="curr">

                                              <option>$</option>

                                              <option >LE</option>

                                          </select>
                                      </div>

                                  </div>

                              </div>

                          </div>

                      </div>
                      <br>
                      <div class="row">

                          <div class="col-md-12 col-lg-12">

                              <div class="form-group ">

                                  <label class="col-md-6 col-lg-6 control-label">Invoice Balance</label>
                                  <div class="col-md-6 col-lg-6 ">
                                      0.00
                                  </div>
                              </div>
                          </div>

                      </div>
                      <br>
                      <div class="row">

                          <div class="col-md-12 col-lg-12">
                              <div class="form-group">

                                  <label class="col-md-6 col-lg-6 control-label">Invoice Number</label>
                                  <div class="col-md-6 col-lg-6">

                                      <select class="myselect3 required"  required  name="state">

                                          <option>1</option>

                                          <option >2</option>

                                          <option >3</option>

                                          <option >4</option>

                                          <option >5</option>


                                      </select>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <br>
                      <div class="row">

                          <div class="col-md-12 col-lg-12">

                              <div class="form-group ">

                                  <label class="col-md-6 col-lg-6 control-label">Paid Value</label>
                                  <div class="col-md-6 col-lg-6">

                                      <input type="number" class="col-md-6 col-lg-6 form-control required" id="paidValue" value="0" min="1"   name="paidValue" >

                                  </div>
                              </div>
                          </div>

                      </div>
             
                </form>
          
          
      </div>
      <div class="modal-footer">
           <button type="button" class="btn btn-secondary fadeIn fifth" data-dismiss="modal">Close</button>
        <button type="button" id="successButton" class="btn btn-primary fadeIn fifth">Pay</button>
          
      </div>
    </div>
  </div>
</div>
        
       
    </div>

@endsection


@section('custom_footer')

    <script src="{{asset('assets/scripts/bootstrap-datetimepicker.min.js')}}"></script>

<!--date/time picker-->

    <script>
        $('#datetime1').datetimepicker({format: 'yyyy-mm-dd',autoclose: true, minView: 2});
        $('#datetime2').datetimepicker({format: 'yyyy-mm-dd',autoclose: true, minView: 2});
        $('#datetime3').datetimepicker({format: 'yyyy-mm-dd',autoclose: true, minView: 2});
    </script>

    <script src="{{asset('js/tax_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/select2.min.js')}}"></script>


    {{--------------------  active side & nav bar ------------------------}}
    <script>
        $('.side_sheets').addClass('hide');
        $('#Purshase').removeClass('hide');

        $("#menu li").removeClass('active');
        $('#Purchase_nav_bar').addClass('active');

        $('#pages ul li a').removeClass('active');
        $('#inventory_acc_side_bar').addClass('active');

    </script>



    {{------------------------  get inventory & currency for supplier ------------------------------}}
    <script>
        $(document).on('change','#supplier',function () {

                $('#inventory_drop_down').attr('disabled','disabled')
                get_items_drop_down($('#inventory_drop_down'));

        });


        function get_items_drop_down($dropdown)
        {

            //var $dropdown = $("#doc_type");
            var supplier = $('#supplier').val();
            /*$dropdown.find('option').remove();*/
            if(supplier == "")
            {
                $dropdown.find('option').remove();
                $dropdown.append('<option value="" > </option>');
                $('#currency').val('');
                $('.currency_value').html('');
                $('#add_row').prop('disabled',true);
            }
        else{
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/get_inventory_for_supplier/',
                    dataType : 'json',
                    type: 'get',
                    data: {
                        supplier:supplier
                    },

                    success:function(response) {

                        console.log(response);


                        $dropdown.find('option').remove();
                        $dropdown.append('<option value="" > </option>');

                        for(var i =0;i<response['inventory'].length;i++)
                        {
                            $dropdown.append($("<option />").val(response['inventory'][i]['doc_id']).text(response['inventory'][i]['doc_seq']));
                        }

                        $('#currency').val(response['currency']);
                        var cur_symbol = $('#currency option:selected').text();
                        $('.currency_value').html(cur_symbol);
                        $('#inventory_drop_down').removeAttr('disabled')

                        $dropdown.select2();

                        if(response['inventory'].length == 0)
                        {
                            $('#add_row').prop('disabled',true);
                        }

                    }

                });
            }


        }

    </script>

    <!-------------------------add row to table --------------------------------->
    <script>
        $(document).on('click','#add_row',function () {
            $(this).attr('disabled',true);

            var row = '<tr class= "added_now" >' +
                '<td td_type=""   disabled hidden><div class="include_check_colsed"></td>'+
                '<td td_type="search">' +
                '<select class="po_values form-control required js-example-disabled-results">\n' +
                '</select></td>' +
                '<td td_type="search" now_selected="">' +
                '<select class="item_product form-control required js-example-disabled-results">\n' +
                '</select></td>' +
                '<td td_type="input"><input class="quantity_value row_data form-control" min="1"  data-role="input" type="number"  disabled/></td>'+
                '<td td_type="input"><input  class="unit_price required form-control"   data-role="input" min="1" type="number" placeholder="0"/></td>'+
                '<td td_type="check_box"><input  class="include_check  "   data-role="input" type="checkbox"/></td>'+
                '<td td_type="drop_down"><input\n' +
                '    type="text"\n' +
                '    multiple\n' +
                '    class="tax_value"\n' +
                '    value=""\n' +
                '    data-initial-value=\'\'\n' +
                '    data-url="/data_tax.json"\n' +
                '    data-load-once="false"\n' +
                '    name="language"/></td>'+
                '<td td_type="input"><input  class="sub_total  form-control"  disabled data-role="input" type="number" value="0" done_calculation="0"/></td>'+
                '<td td_type="input"><input  class="total_row_tax  form-control"  disabled data-role="input" type="number" value="0" /></td>'
                +'<td>'+'<button class="set_all_disabled btn btn-primary btn-xs Save_row">'+'Save'+'</button>'
                +'</td>'
                +'<td>'
                +'<button class="set_all_disabled btn btn-danger btn-xs delete_row"  >'+'<span class="lnr lnr-trash"></span>'+'</button>'
                +'</td>'
                +'</tr>';
            $("#op_table").prepend(row);
            $('.tax_value').tax_fastselect();
            get_item_type_drop_down($('.po_values'));
            $(".po_values").select2();
            $(".po_values").siblings('.select2').css('width', '71px');

        });

    </script>


    {{---------------  active add row  on supplier change --------------------}}



    <script>
        $(document).on('change','#inventory_drop_down',function () {
            $('#op_table tbody tr').empty();
            if($(this).val() == '')
            {
                $('#supplier').prop('disabled',false);
            }
            else{
                get_all_inventory_data();
                enable_disable_add();
            }



        });

        function get_all_inventory_data()
        {
            console.log('all inventory data')
            var inventory_id = $('#inventory_drop_down').val();

            $('#inventory_drop_down').prop('disabled',true);

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/get_all_inventory_data/',
                dataType : 'json',
                type: 'get',
                data: {
                    inventory_id:inventory_id
                },

                success:function(response) {

                    console.log(response);




                  for(var r = 0 ;r<response['inventory_data'].length ;r++)
                  {
                      if(response['inventory_data'][r]['tax_string'] != null && response['inventory_data'][r]['tax_string'] != ''  && response['inventory_data'][r]['tax_string'] != 'NULL')
                      {
                          var row = '<tr class="" data-select2-id="75">' +
                              '<td td_type="" disabled="" hidden="" current_value="0" yes_or_no="No" class="include_check_colsed_v"><div class="include_check_colsed"></div></td>' +
                              '<td td_type="search" data-select2-id="74" class="po_values_v" current_value="'+response['inventory_data'][r]['po_id']+'">'+response['inventory_data'][r]['po_seq']+'</td>' +
                              '<td td_type="search" now_selected="'+response['inventory_data'][r]['pro_id']+'" data-select2-id="98" class="item_product_v" current_value="'+response['inventory_data'][r]['pro_id']+'" seq="0">'+response['inventory_data'][r]['pro_name']+'</td>' +
                              '<td td_type="input" maxmum_quan="'+response['max_quantity_array'][r]+'" class="quantity_value_v" current_value="'+response['inventory_data'][r]['qut']+'">'+response['inventory_data'][r]['qut']+'</td>' +
                              '<td td_type="input" class="unit_price_v" current_value="'+response['inventory_data'][r]['unit_price']+'">'+response['inventory_data'][r]['unit_price']+'</td>' +
                              '<td td_type="check_box" current_value="'+response['inventory_data'][r]['include_tax']+'" yes_or_no="'+response['yesy_no'][r]+'" class="include_check_v check_box_v">'+response['yesy_no'][r]+'</td>' +

                              '<td td_type="drop_down" class="tax_value_v" get_value="'+response['inventory_data'][r]['tax_string']+'" current_value="'+response['inventory_data'][r]['tax_string']+'">'+response['inventory_data'][r]['tax_string']+'</td>' +

                              '<td td_type="input" class="sub_total_v" current_value="'+response['sub_total_array'][r]+'">'+response['sub_total_array'][r]+'</td>' +
                              '<td td_type="input" class="total_row_tax_v" current_value="'+response['total_tax_array'][r]+'">'+response['total_tax_array'][r]+'</td>' +

                              '<td><button class="set_all_disabled btn btn-primary btn-xs edit_row">Edit</button></td>' +
                              '<td><button class="set_all_disabled btn btn-danger btn-xs delete_row"><span class="lnr lnr-trash"></span></button></td>' +
                              '</tr>';
                      }

                      else{
                          var row = '<tr class="" data-select2-id="75">' +
                              '<td td_type="" disabled="" hidden="" current_value="0" yes_or_no="No" class="include_check_colsed_v"><div class="include_check_colsed"></div></td>' +
                              '<td td_type="search" data-select2-id="74" class="po_values_v" current_value="'+response['inventory_data'][r]['po_id']+'">'+response['inventory_data'][r]['po_seq']+'</td>' +
                              '<td td_type="search" now_selected="'+response['inventory_data'][r]['pro_id']+'" data-select2-id="98" class="item_product_v" current_value="'+response['inventory_data'][r]['pro_id']+'" seq="0">'+response['inventory_data'][r]['pro_name']+'</td>' +
                              '<td td_type="input" maxmum_quan="'+response['max_quantity_array'][r]+'" class="quantity_value_v" current_value="'+response['inventory_data'][r]['qut']+'">'+response['inventory_data'][r]['qut']+'</td>' +
                              '<td td_type="input" class="unit_price_v" current_value="'+response['inventory_data'][r]['unit_price']+'">'+response['inventory_data'][r]['unit_price']+'</td>' +
                              '<td td_type="check_box" current_value="'+response['inventory_data'][r]['include_tax']+'" yes_or_no="'+response['yesy_no'][r]+'" class="include_check_v check_box_v">'+response['yesy_no'][r]+'</td>' +

                              '<td td_type="drop_down" class="tax_value_v" get_value="" current_value=""></td>' +

                              '<td td_type="input" class="sub_total_v" current_value="'+response['sub_total_array'][r]+'">'+response['sub_total_array'][r]+'</td>' +
                              '<td td_type="input" class="total_row_tax_v" current_value="'+response['total_tax_array'][r]+'">'+response['total_tax_array'][r]+'</td>' +

                              '<td><button class="set_all_disabled btn btn-primary btn-xs edit_row">Edit</button></td>' +
                              '<td><button class="set_all_disabled btn btn-danger btn-xs delete_row"><span class="lnr lnr-trash"></span></button><button class="btn btn-danger" type="button" id="yes">yes</button> <button class="btn btn-default"  type="button" id="no">no</button></td>' +
                              '</tr>';
                      }

                      $('#inventory_drop_down').prop('disabled',false);
                      $("#op_table").prepend(row);

                  }


                    var rowCount = $('#op_table tr').length;
                    console.log('rowCount'+ rowCount)

                    if(rowCount > 1)
                    {
                        $('#supplier').prop('disabled',true);
                        //$('#inventory_drop_down').prop('disabled',true);
                        $('#add_row').prop('disabled',false);
                    }
                  get_totals();


                 }

            });

        }


        function enable_disable_add(){


            if($('#inventory_drop_down').val() != "" && $('#set_po_id').attr('current_po_status') != 'approved_po'&& $('#set_po_id').attr('current_po_status') != 'invoiced' )
            {

                $('#add_row').prop('disabled',false);
            }

            else{
                $('#add_row').prop('disabled',true);
            }

        }

        $(document).on('click','#add_row',function()
        {
            $('#supplier').prop('disabled',true);
            $('#inventory_drop_down').prop('disabled',true);
        });

    </script>
    {{---------------------------------  save invoice ---------------------------------------------------}}

    <script>
        $('.alert-autocloseable-success').hide();


        $(document).on('click','#save_po',function () {
            var status_po = $('#set_po_id').attr('current_po_status');
            add_new_po_all (status_po);

        });
        $(document).on('click','#sendto_approved_btn',function () {
            /*var status_po = $('#set_po_id').attr('current_po_status');
            if(status_po != 'approved_po')
            {
                $('#set_po_id').attr('current_po_status','to_approve_po');

                status_po = $('#set_po_id').attr('current_po_status');

            }
            add_new_po_all (status_po);
*/
            //add_new_po_all ('to_approve_po');
            var status_po = $('#set_po_id').attr('current_po_status');
            if(status_po != 'approved_po')
            {
                add_new_po_all ('to_approve_po');
            }

            else{
                add_new_po_all ('approved_po');
            }
        });
        $(document).on('click','#approved_btn',function () {
            //$('#set_po_id').attr('current_po_status','approved_po');
            add_new_po_all ('approved_po');


        });



        function add_new_po_all (status_click)
        {
            if(!$('#op_table tr').hasClass('edited_now'))
            {

                $('#save_po').attr('disabled','disabled');
                $('#supplier').attr('disabled','disabled');
                $('#inventory_drop_down').attr('disabled','disabled');
                $('#sendto_approved_btn').attr('disabled','disabled');
                $('#approved_btn').attr('disabled','disabled');


                var status = status_click;
                console.log(status);

                var supplier = $('#supplier').val();
                var Invoice_Reference = $('#invoice_reference').val();
                var currency_id = $('#currency').val();

                var inventory_id = $('#inventory_drop_down').val();
                var doc_date = $('#datetime1').val();
                var doc_note = $('#po_comment').val();

                var current_user_id= $('#set_po_id').attr('current_id');
                var current_po_id = $('#set_po_id').attr('current_po_id');



                //var canceled_po = $('#set_po_id').attr('closed_or_not');



                var input = $('#op_table > tbody  > tr');
                var row = [];

                input.each(function(){

                    if(!$(this).hasClass('added_now'))
                    {
                        row.push({"po_id": $(this).find('.po_values_v').attr('current_value'),"item_product": $(this).find('.item_product_v').attr('current_value'),"quantity": $(this).find('.quantity_value_v').attr('current_value'),"closed": $(this).find('.include_check_colsed_v').attr('current_value'),"unit_price": $(this).find('.unit_price_v').attr('current_value'),"include_check": $(this).find('.include_check_v').attr('current_value'),"tax_value": $(this).find('.tax_value_v').attr('current_value')});
                    }


                });
                console.log('inventory_id'+inventory_id)
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/add_new_invoice/',
                    dataType : 'json',
                    type: 'get',
                    data: {

                        supplier:supplier,
                        Invoice_Reference:Invoice_Reference,
                        currency:currency_id,
                        inventory:inventory_id,
                        document_date:doc_date,
                        document_note:doc_note,
                        status:status,
                        current_po_id:current_po_id,
                        table_rows:row,
                        cur_user_id:current_user_id,

                    },

                    success:function(response) {

                        console.log(response);
                        if(response['error'])
                        {
                            $('.err_input').removeClass('red-border');
                            $('.err_input').parent().find('.select2').removeClass('red-border');


                            notification('glyphicon glyphicon-warning-sign','Warning','You Should Fill All Fields','danger')


                            $.each(response['error'], function (key, val)
                            {
                                //alert(key + val);

                                console.log(key+'_error');
                              //  $("#"+key+'_error').removeClass('hide').html(val[0]);

                                $("."+key+'_error').addClass('red-border');
                                $('.'+key+'_error').parent().find('.select2').addClass('red-border');
                                //console.log(val[0]);

                            })
                            $('#save_po').removeAttr('disabled');
                            $('#supplier').removeAttr('disabled');
                            $('#inventory_drop_down').removeAttr('disabled');

                            $('#sendto_approved_btn').removeAttr('disabled');
                            $('#approved_btn').removeAttr('disabled');
                        }

                        else
                        {

                            $('.errorMessage1').hide();

                            $('.err_input').removeClass('red-border');
                            $('.err_input').parent().find('.select2').removeClass('red-border');

                            var status_po = $('#set_po_id').attr('current_po_status');
                            if(status_po != 'approved_po')
                            {
                                $('#set_po_id').attr('current_po_status',status_click);

                            }

                            $('#op_table tr.added_now').remove();


                            $('.breadcrumb').removeClass('po-active');
                            $('#'+response['status']).addClass('po-active');
                            $('#current_choosed_status').attr('coosed_status',response['status']);
                            $('#save_po').text("Edit");
                            $('#save_po').attr('id','edit_po');
                            $('#set_po_id').attr('current_po_id',response['current_po_id']);
                            $('#set_po_id').attr('current_invoice_id',response['current_invoice_id']);
                            $('.set_all_disabled').prop('disabled',true);
                            $('#creat_po').removeClass('hide');

                            $('#po_more').removeClass('hide');

                            if($('#set_po_id').attr('current_po_status') == 'draft_po')
                            {
                                $('#delete_po').parent('li').removeClass('hide');
                            }

                            else
                            {
                                $('#delete_po').parent('li').addClass('hide');
                                /*$('#closed_po').removeClass('hide');
                                $('#close_po_btn').removeClass('hide');*/
                                $('#create_invoice').removeClass('hide');
                            }

                            $('#edit_po').removeAttr('disabled');
                            //po-active
                            console.log(response);

                            notification('glyphicon glyphicon-ok-sign','Congratulation!','Invoice Saved Successfully!','success')




                        }

                    }

                });

            }
            else{
                $('#save_po').removeAttr('disabled');
                $('#sendto_approved_btn').removeAttr('disabled');
                $('#approved_btn').removeAttr('disabled');
            }

        }
    </script>



    {{--------------------------  edit invoice  --------------------------------}}
    <script>
        $(document).on('click','#edit_po',function () {
            $('#edit_po').text("Save");
            $('#edit_po').attr('id','save_po');
            if($('#set_po_id').attr('current_po_status') != 'approved_po' && $('#set_po_id').attr('current_po_status') != 'invoiced')
            {
                $('.set_all_disabled').prop('disabled',false);
                var rowCount = $('#op_table tr').length;
                console.log('rowCount'+ rowCount)
                /*if(rowCount == 1)
                {
                    $('#supplier').prop('disabled',false);
                    $('#add_row').prop('disabled',false);

                }
                else
                {
                    $('#supplier').prop('disabled',true);
                    $('#add_row').prop('disabled',false);
                }*/

            }
            // $('#close_po_btn').prop('disabled',false);
            /*if($('#set_po_id').attr('current_po_status') == 'draft_po')
            {
                $('#close_po_btn').prop('disabled',true);
            }*/

            console.log($('#set_po_id').attr('current_po_id'));
            $('#creat_po').addClass('hide');

            $('#delete_po').parent('li').addClass('hide');

            $('#po_more').addClass('hide');

            $('#create_invoice').addClass('hide');
        });
    </script>

    {{----------------------  delete row --------------------------------}}
    <script>

        $(document).on('click', '.delete_row', function () {

            $(this).hide(function(){
                $(this).closest('td').append('<button class="btn btn-danger" type="button" id="yes" style="display: none;">yes</button>' +
                    '<button class="btn btn-default" type="button" id="no" style="display: none;">no</button>');
                $(this).closest('td').find("#yes").show('slow').delay(1000);
                $(this).closest('td').find("#no").show('slow').delay(1000);

            });

        });


        $(document).on('click','#yes',function(){

            /********************/
            $(this).closest('tr').remove();

            var rowCount = $('#op_table tbody tr').length;
            console.log('rowCount'+ rowCount)
            if(rowCount == 0)
            {
                $('#supplier').prop('disabled',false);
                $('#inventory_drop_down').prop('disabled',false);
                $('#add_row').prop('disabled',true);

            }
            else
            {
                $('#supplier').prop('disabled',true);
                $('#inventory_drop_down').prop('disabled',true);
                $('#add_row').prop('disabled',false);
            }

            if($('.added_now').length == 0)
            {
                $("#add_row").removeAttr("disabled");
            }
            get_totals();
            /********************/
        });

        $(document).on('click','#no',function(){

            console.log('noo ');

            $(this).closest('td').find(".delete_row").show('slow').delay(1000);
            $(this).closest('td').find("#yes").hide('slow').delay(1000).remove();
            $(this).closest('td').find("#no").hide('slow').delay(1000).remove();
            /*
            $(this).closest('td').find("#yes").hide('slow').delay(1000);
            $(this).closest('td').find("#no").hide('slow').delay(1000);*/


        });






    </script>

    {{-----------  get drop down values --------------}}
    <script>

        function get_item_type_drop_down($dropdown)
        {

            //var $dropdown = $("#doc_type");
            var supplier = $('#supplier').val();
            var curr_selectet_value =  $dropdown.parents('tr').find('.po_values_v').attr('current_value');
            $dropdown.prop('disabled',true);
            console.log('curr_selectet_value : '+curr_selectet_value);
            /*$dropdown.find('option').remove();*/

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/get_po_values_drop_down/',
                dataType : 'json',
                type: 'get',
                data: {
                    supplier:supplier,
                    curr_selectet_value:curr_selectet_value
                },

                success:function(response) {

                    console.log(response);

                    var selected_val = $dropdown.parents('td').attr('current_value');

                    console.log('selected drop '+selected_val);

                    $dropdown.find('option').remove();

                    for(var i =0;i<response['item_type'].length;i++)
                    {
                        $dropdown.append($("<option />").val(response['item_type'][i]['po_id']).text(response['item_type'][i]['po_seq']));
                    }
                    if(response['extra_po_colsed']['po_id'] != "")
                    {
                        $dropdown.append($("<option />").val(response['extra_po_colsed']['po_id']).text(response['extra_po_colsed']['po_seq']));
                        console.log(response['extra_po_colsed']['po_id']);
                    }

                    $dropdown.val(selected_val);
                    if(!$dropdown.parents('tr').hasClass('edited_now'))
                    {
                        $dropdown.prop('disabled',false);
                    }


                    $(".item_product").select2();
                    $(".item_product").siblings('.select2').css('width', '71px');
                }

            });

        }

    </script>


    {{-----------------------------------------------------}}

    <script>
        $(document).on('change','.po_values',function () {
            var po_id = $(this).val();
            var selected_v_pro = $(this).parents('tr').find('.item_product_v').attr('current_value');

            get_item_product($(this),po_id,"");
        });

        function get_item_product(thiss_item,po_id,edited_value){

            var item_selected_array =[];
            var all_selected_v = $('.po_values_v');

            var seq = thiss_item.parents("tr").find('.item_product_v').attr('seq');
            var checked = thiss_item.parents("tr").find('.include_check_v').attr('current_value');
            var product_id = thiss_item.parents("tr").find('.item_product_v').attr('current_value');

            console.log('seq' +seq + 'checked' +checked + 'product_id'+product_id);


            all_selected_v.each(function () {
                if($(this).attr('current_value') == po_id )
                {
                    if($(this).parents('tr').find('.item_product_v').attr('current_value') != edited_value)
                    {
                        var selectt = $(this).parents('tr').find('.item_product_v').attr('current_value');
                        item_selected_array.push(selectt);
                    }

                }


            }) ;

            if(item_selected_array.length == 0)
            {
                item_selected_array = "";
            }

            console.log(item_selected_array);

            var $dropdown = thiss_item.parents("tr").find('.item_product');
            $dropdown.val('');
            $dropdown.attr('disabled','disabled');


            console.log(po_id);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/get_item_product_drop_down/',
                dataType : 'json',
                type: 'get',
                data: {
                    po_id:po_id,
                    item_selected_array:item_selected_array,
                    seq:seq,
                    checked:checked,
                    product_id:product_id
                },

                success:function(response) {

                    console.log(response);
                    /*console.log(response['data'][0]['pro_id']);*/

                    var selected_val = $dropdown.parents('td').attr('current_value');

                    console.log('selected drop '+selected_val);

                    $dropdown.find('option').remove();

                    for(var i =0;i<response['data'].length;i++)
                    {
                        $dropdown.append($("<option />").val(response['data'][i]['pro_id']).text(response['data'][i]['pro_name']));
                    }
                    for(var i =0;i<response['extra_v'].length;i++)
                    {
                        $dropdown.append($("<option />").val(response['extra_v'][i]['pro_id']).text(response['extra_v'][i]['pro_name']));
                    }

                    $dropdown.val(selected_val);
                    if(!$dropdown.parents('tr').hasClass('edited_now'))
                    {
                        $dropdown.prop('disabled',false);
                    }
                    //$dropdown.prop('disabled',false);
                }

            });
        }
    </script>

    {{----------------------------  get item drop down  ---------------------------------------------}}

    <script>
        $(document).on('change','.item_product',function(){

            var val_selected = $(this).val();
            $(this).parent('td').attr('now_selected',val_selected);
            get_quantity('',$(this),0);
        });


        function get_quantity(inputs,item_product,save) {

            var save_btn = item_product;
            var item_pro_id = item_product.parents("tr").find('.item_product').val();
            var po_id = item_product.parents("tr").find('.po_values').val();
            var quantitys = item_product.parents("tr").find('.quantity_value');
            //quantitys.val('');
            quantitys.prop('disabled',true);
            var current_exist_q = quantitys.val();

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/get_quantity_limit/',
                dataType : 'json',
                type: 'get',
                data: {
                    po_id:po_id,
                    item_pro_id:item_pro_id
                },

                success:function(response) {

                    //console.log(response);

                    console.log('quantity ' + response['quantity']);
                    if(save == 0) {
                        if (response['quantity'] > 0) {
                            quantitys.val(response['quantity']);
                            quantitys.attr('value_now', response['quantity']);
                            quantitys.attr('max', response['quantity']);
                            quantitys.parent("td").attr('maxmum_quan', response['quantity']);
                            //quantitys.removeAttr('disabled')

                        }
                    }
                    else if(save == 1)
                    {
                        var cur_q = response['quantity'] - current_exist_q;
                        if(cur_q > 0 )
                        {
                            var row_id = $(this).parents("tr").find('.include_check_colsed').attr('id');
                            item_product.parents("tr").find('.include_check_colsed').parents("td").attr('current_value',0).attr('yes_or_no','No').attr('row_id',row_id);
                            console.log('not closed');
                            save_table_effect(inputs,save_btn);
                        }
                        else{

                            var row_id = $(this).parents("tr").find('.include_check_colsed').attr('id');
                            item_product.parents("tr").find('.include_check_colsed').parents("td").attr('current_value',1).attr('yes_or_no','Yes').attr('row_id',row_id);
                            console.log('closed');
                            save_table_effect(inputs,save_btn);
                        }

                    }

                    /* if(save_or_not == 1)
                     {

                     }*/



                }

            });

        }
    </script>


    {{------------- limit the max of input number  ----------------}}
    <script>
        $(document).on('change ','.quantity_value',function() {
            var max = parseInt($(this).attr('max'));
            var min = parseInt($(this).attr('min'));
            if ($(this).val() > max)
            {
                $(this).val(max);
            }
            else if ($(this).val() < min)
            {
                $(this).val(min);
            }
        });
    </script>

    {{-------------  change attr value_now on change of input -----------------}}
    <script>
        $(document).on(' change','.quantity_value',function () {
            var value_n = $(this).val()
            $(this).attr('value_now',value_n);
        });
    </script>

    {{-------------------------------------------------------------------------}}


    {{---------------------  save row --------------------------------}}
    <script>
        $(document).on("click", ".Save_row", function(){

            var empty = false;
            var input = $(this).parents("tr").find('input[type="text"],input[type="number"]');
            var input_number = $(this).parents("tr").find('input[type="number"]');

            //item_type_v
            if($(this).parents("tr").find('.include_check:checked').length>0)
            {

                $(this).parents("tr").find('.include_check').parent("td").attr('current_value',1).attr('yes_or_no','Yes');
                input.each(function(){
                    if(!$(this).val()|| $(this).val()==""){
                        $(this).parents('td').addClass("error");
                        empty = true;
                    } else{
                        $(this).parents('td').removeClass("error");
                    }
                });


                if(!$(this).parents("tr").find('.po_values').val() || $(this).parents("tr").find('.po_values').val()=="" || !$(this).parents("tr").find('.item_product').val() || $(this).parents("tr").find('.item_product').val()=="")
                {
                    $(this).parents("tr").find('.po_values').parent('td').addClass("error");

                    empty = true;
                }
                else{
                    $(this).parents("tr").find('.po_values').parent('td').removeClass("error");
                }


            }
            else{

                $(this).parents("tr").find('.include_check').parent("td").attr('current_value',0).attr('yes_or_no','No');
                input.each(function(){
                    if(!$(this).val()&&!$(this).hasClass('tax_value') || $(this).val()==""&&!$(this).hasClass('tax_value')){
                        $(this).parents('td').addClass("error");
                        empty = true;
                    } else{
                        $(this).parents('td').removeClass("error");
                    }
                });

                if(!$(this).parents("tr").find('.po_values').val() || $(this).parents("tr").find('.po_values').val()=="" || !$(this).parents("tr").find('.item_product').val() || $(this).parents("tr").find('.item_product').val()=="")
                {
                    $(this).parents("tr").find('.po_values').parent('td').addClass("error");

                    empty = true;
                }

                else{
                    $(this).parents("tr").find('.po_values').parent('td').removeClass("error");
                }

            }

            input_number.each(function(){
                if(!$(this).val()|| $(this).val()==""){
                    $(this).parents('td').addClass("error");
                    empty = true;
                } else{
                    $(this).parents('td').removeClass("error");
                }
            });

            console.log('empty '+empty);

            $(this).parents("tr").find(".error").first().focus();

            var colum_data = [];
            var row_id = $(this).closest('tr').attr('id');


            if(!empty ) {
                $(this).attr('disabled', 'disabled');
                /*get_quantity(1,$(this));*/

                ($(this)).removeAttr('disabled');

                //save_table_effect($(this));
                $(this).prop('disabled', true);
                get_sub_for_row(input, $(this));
                /*get_quantity($(this),1);*/

                /*------------ check box ----------------------*/

            }


        });

        function save_table_effect(input,thiss) {
            console.log('passed')

            //include_check_colsed
            var hidden_column = thiss.parents("tr").find('.include_check_colsed');
            var hidden_column_val = hidden_column.parents("td").attr('current_value');
            hidden_column.parent("td").addClass('include_check_colsed_v').attr('current_value', hidden_column_val);
            /*----------------------------------------------------*/
            var colum_data = [];
            var check_box = thiss.parents("tr").find('input.include_check');
            var check_val = check_box.parent("td").attr('current_value');
            var check_text_val = check_box.parent("td").attr('yes_or_no');
            var classess_array = ['quantity_value_v','unit_price_v','tax_value_v','sub_total_v','total_row_tax_v'];
            check_box.parent("td").addClass('include_check_v').addClass('check_box_v').html(check_text_val);

            /*--------- select  po ------------------*/
            var select_po_drop_down = thiss.parents("tr").find('select.po_values');
            var value_select = select_po_drop_down.val();
            var text_select = thiss.parents("tr").find('select.po_values option:selected').text();

            select_po_drop_down.parent("td").addClass('po_values_v').attr('current_value', value_select).html(text_select);
            /*--------------------------*/


            /*--------- select  products ------------------*/
            var select_product_drop_down = thiss.parents("tr").find('select.item_product');
            var value_select_pro = select_product_drop_down.val();
            var text_select_product = thiss.parents("tr").find('select.item_product option:selected').text();
            var seq = thiss.parents("tr").find('.item_product_v').attr('seq');
            if(!seq || seq == "")
            {
                seq = 0;
            }

            select_product_drop_down.parent("td").addClass('item_product_v').attr('current_value', value_select_pro).attr('seq', seq).html(text_select_product);
            /*--------------------------*/

            /*--------- input quantity ------------------*/
/*
            var quantity = thiss.parents("tr").find('.quantity_value');
            var quantity_value = quantity.val();


            console.log('quantity_value' +quantity_value)
            quantity.parent("td").addClass('quantity_value_v').attr('current_value', quantity_value).html(quantity_value);
            /!*--------------------------*!/
*/



            if (thiss.parents("tr").hasClass('added_now')) {
                thiss.parents("tr").removeClass('added_now');

            }
            if (thiss.parents("tr").hasClass('edited_now')) {
                thiss.parents("tr").removeClass('edited_now');

            }

            //|| thiss.siblings().find('.create_edit_v') ){
            //console.log(thiss.closest('tr').attr('id'))

            var i = 0;
            input.each(function () {


                $(this).parent("td").addClass(classess_array[i]);
                $(this).parent("td").attr('current_value', $(this).val());
                $(this).parent("td").html($(this).val());
                //console.log($(this).parent().parent("td").attr('get_value'));


                /*if($(this).siblings().find('.create_edit_v').attr('c_e_value'))
                {
                    $(this).siblings().find('.create_edit_v').each(function () {
                    vall = vall +',' + $(this).attr('c_e_value');
                    //window.set_selected($(this).attr('c_e_value'));
                    console.log($(this).attr('c_e_value'));
                });
                }*/


                var vall = $(this).val();
                var current_v = $(this).val();
                $(this).parent().parent("td").addClass(classess_array[i]);
                $(this).parent().parent("td").attr('get_value', vall);
                $(this).parent().parent("td").attr('current_value', current_v);
                $(this).parent().parent("td").html(vall);

                i++;
                colum_data.push(vall);

            });

            console.log(colum_data);
            if ($('.added_now').length == 0) {
                $("#add_row").removeAttr("disabled");
            }

            thiss.text('Edit');
            thiss.removeClass('Save_row');
            thiss.addClass('edit_row');

            get_totals();
            thiss.prop('disabled',false);

        }


    </script>
    {{-------------------------------------------------}}


    <Script>
        function get_sub_for_row(inputs,thiss_input)
        {
            //var input = $('#op_table > tbody  > tr');

            var input = thiss_input.parents('tr');
            var check_v = 0;

            if(thiss_input.parents("tr").find('.include_check:checked').length>0)
            {
                check_v = 1;
            }
            else{check_v = 0;}

            var row = [];

            input.each(function(){

                row.push({"quantity": $(this).find('input.quantity_value').val(),"unit_price": $(this).find('.unit_price').val(),"Include": check_v,"tax": $(this).find('.tax_value').val() });


            });


            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/get_subtotal/',
                dataType : 'json',
                type: 'get',
                data: {

                    table_rows:row,

                },

                success:function(response) {

                    /*console.log(response);*/
                    console.log(response);
                    thiss_input.parents('tr').find('input.sub_total').val(response['$sub_val']);
                    thiss_input.parents('tr').find('input.total_row_tax').val(response['$total_tax']);
                    (thiss_input).removeAttr('disabled');

                    get_quantity(inputs,thiss_input,1);

                    //save_table_effect(inputs,thiss_input);
                },
                error:function(response) {

                    /*console.log(response);*/
                }

            });

            /*console.log(thiss_input.parents('tr').find('input.quantity_value_v').val());*/
            ////console.log(row);

        }

    </Script>

    {{---------------------------------------- edit  row_table --------------------------------------------------------}}
    <script>
        $(document).on("click", ".edit_row", function(){
            if(!$('#op_table tr').hasClass('added_now')) {
                $(this).parents("tr").addClass('edited_now');
                var input_class_array = ['quantity_value', 'unit_price', 'sub_total', 'total_row_tax'];
                var s = 0;
                $(this).parents("tr").find("td:not(:last-child)").each(function () {
                    if ($(this).attr('td_type') == 'input') {
                        if (input_class_array[s] == 'sub_total') {
                            $(this).html('<input disabled done_calculation="0" type="number" class="' + input_class_array[s] + ' input_edit_call form-control" value="' + $(this).text() + '">');
                        }
                        else if (input_class_array[s] == 'total_row_tax') {
                            $(this).html('<input  class="' + input_class_array[s] + '  form-control"  disabled data-role="input" type="number" value="' + $(this).text() + '" />');
                        }
                        else if (input_class_array[s] == 'quantity_value') {
                            $(this).html('<input min="1" max="' + $(this).attr('maxmum_quan') +'" type="number" class="' + input_class_array[s] + ' input_edit_call form-control" value="' + $(this).text() + '" disabled >');
                        }
                        else {
                            $(this).html('<input min="1" type="number" class="' + input_class_array[s] + ' input_edit_call form-control" value="' + $(this).text() + '">');
                        }

                        s++;
                    }
                    else if ($(this).attr('td_type') == 'drop_down') {
                        var string = $(this).attr('get_value'),
                            strx = string.split(',');
                        var array = [];

                        array = array.concat(strx);
                        //var obj = array.reduce(function(o, val) { o[val] = val; return o; }, {});
                        var res = [];

                        for (var i = 0; i < array.length; i += 1) {
                            var o = {};
                            o["text"] = array[i];
                            o["value"] = array[i];
                            res.push(o);
                        }
                        var choosed_item;
                        if (string == "") {
                            choosed_item = null;
                            console.log('null choosed_item');
                        }
                        else {
                            choosed_item = JSON.stringify(res);
                        }


                        if ($(this).hasClass('tax_value_v')) {
                            console.log(JSON.stringify(res));
                            $(this).html('<input\n' +
                                '    type="text"\n' +
                                '    multiple\n' +
                                '    class="tax_value"\n' +
                                '    value="' + $(this).attr('get_value') + '"\n' +
                                '    data-initial-value=' + choosed_item + '\n' +
                                '    data-url="/data_tax.json"\n' +
                                '    data-load-once="false"\n' +
                                '    name="language"/>')
                            $('.tax_value').tax_fastselect();
                        }

                    }
                    else if ($(this).attr('td_type') == 'search')
                    {
                        if($(this).hasClass('po_values_v'))
                        {
                            $(this).html('<select class="po_values form-control js-example-disabled-results" >\n' +
                                '</select>');
                            get_item_type_drop_down($(this).find('.po_values'));
                            var selected_v = $(this).attr('current_value');
                            $(this).find(".po_values").val(selected_v);
                            $(".po_values").select2();
                            $(".po_values").siblings('.select2').css('width', '71px');
                            $(".po_values").attr('disabled','disabled');

                        }

                        else if($(this).hasClass('item_product_v'))
                        {

                            $(this).html('<select class="item_product form-control js-example-disabled-results" >\n' +
                                '</select>');
                            var po_id = $(this).parents('tr').find('.po_values_v').attr('current_value');
                            var now_selected = $(this).parents('tr').find('.item_product_v').attr('now_selected');

                            get_item_product($(this).find('.item_product'),po_id,now_selected);
                            var selected_v = $(this).attr('current_value');
                            $(this).find(".item_product").val(selected_v);
                            $(".item_product").select2();
                            $(".item_product").siblings('.select2').css('width', '71px');
                            $(".item_product").attr('disabled','disabled');

                        }

                    }
                    else if ($(this).attr('td_type') == 'check_box') {
                        $(this).html('<input  class="include_check ' +
                            ' "   data-role="input" type="checkbox"/>');
                        if ($(this).attr('current_value') == 1) {
                            $(this).find('.include_check').prop('checked', true);
                        }
                        else {
                            $(this).find('.include_check').prop('checked', false);
                        }
                    }

                });

                $("#add_row").attr("disabled", "disabled");
                $(this).text('Save');
                $(this).removeClass('edit_row');
                $(this).addClass('Save_row');
                get_totals();
            }
        });
    </script>


                {{------------------  refresh suplier dropdown ----------------------------}}
                <script>

                    function supplier_refresh()
                    {

                        var $dropdown = $("#supplier");
                        $dropdown.prop('disabled',true);
                        $dropdown.select2('destroy');
                        var selected_value = $dropdown.val();

                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url: '/refresh_supplier/',
                            dataType : 'json',
                            type: 'get',
                            data: {

                            },

                            success:function(response) {

                                $dropdown.find('option').remove();
                                $dropdown.append('<option value=""></option>');

                                $.each(response, function(key, value){

                                    $dropdown.append($("<option />").val(key).text(value));

                                });
                                $dropdown.val(selected_value);
                                $dropdown.select2();
                                $dropdown.prop('disabled',false);
                            }

                        });


                    }

                    $(document).on('dblclick','.select2-selection',function () {
                        var select_check = $(this).parents('.select2').siblings('select');


                        if ($('#save_po').length && select_check.attr('id') == 'supplier') {
                            supplier_refresh();
                        }
                    });
                </script>

{{--------------------------------  get total ------------------------------}}

    <script>
        function get_totals() {
            var input = $('#op_table > tbody  > tr');
            var untaxed_v = 0;
            var total_tax_v = 0;
            input.each(function () {
                if (!$(this).hasClass('added_now')) {
                    untaxed_v += parseFloat($(this).find('.sub_total_v').attr('current_value'));
                    total_tax_v += parseFloat($(this).find('.total_row_tax_v').attr('current_value'));

                }


            });

            $('#untaxed_all').html(untaxed_v);
            $('#taxed_sum').html(total_tax_v);
            $('#total_sum').html(parseFloat(total_tax_v+untaxed_v));
        }
        /*untaxed_all*/
    </script>

    {{------------------------   create new inventory -----------------------------------------------}}
    <script>
        $(document).on('click','#creat_po',function () {/*
            $('#edit_po').prop('disabled',false);
            $('#edit_po').text("Save");
            $('#edit_po').attr('id','save_po');
            $('#save_po').prop('disabled',false);

            $('.set_all_disabled').prop('disabled',false);
            $('#supplier').prop('disabled',false);
            $('#inventory_drop_down').prop('disabled',false);
            $('#add_row').prop('disabled',true);
            var crrent_date = $('#datetime1').attr('crrent_date');
            $('#datetime1').val(crrent_date);

            $('#set_po_id').attr('current_po_id',"");
            $('#set_po_id').attr('current_po_status',"draft_po");
            $('#creat_po').addClass('hide');
            $('#po_more').addClass('hide');
            $('#create_invoice').addClass('hide');

            $('#delete_po').parent('li').addClass('hide');

            /!*$(".set_all_disabled").val("");*!/
            $('.get_all_selectors').val('');
            //$('.get_all_date_input').val('');

            $(".get_all_selectors option[value='']").attr('selected', true);
            $('.get_all_date_input').val('');
            $('.select2-selection__rendered').html('');

            $('#op_table tbody tr').remove();
            $('.breadcrumb').removeClass('po-active');
            $('#total_quantity').html('0.00');

            $('#po_comment').val('');*/
            window.location.href = '/invoice';
        });
    </script>
    {{---- dublicate po ---}}
    <script>
        $(document).on('click','#dublicate_po',function(){

            $('#set_po_id').attr('current_po_id',"");
            $('#set_po_id').attr('current_po_status',"draft_po");
            $('#set_po_id').attr('closed_or_not',0);

            $('.set_all_disabled').prop('disabled',false);


            $('#creat_po').addClass('hide');
            $('#po_more').addClass('hide');
            $('#create_invoice').addClass('hide');

            $('.breadcrumb').removeClass('po-active');

            $('#edit_po').prop('disabled',false);
            $('#edit_po').text("Save");
            $('#edit_po').attr('id','save_po');
            $('#save_po').prop('disabled',false);
        });
    </script>


    {{----------------------  delete inventory ------------------------------------}}

    <script>
        $(document).on('click','#delete_po',function(){
            var cur_po_id = $('#set_po_id').attr('current_po_id');

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/delete_this_invoice/',
            dataType : 'json',
                type: 'get',
                data: {
                    cur_po_id:cur_po_id,
                },

                success:function(response) {

                    window.location.href = '/inventory_acc';
                }

            });

        });
    </script>

    {{----------------------------------------------------------------}}
    <script>
        $(document).ready(function () {
            $(".select_2_enable").select2();

            if($('#set_po_id').attr('current_po_status') == 'approved_po' /*|| $('#set_po_id').attr('current_po_status') == 'invoiced'*/)
            {
                $('.set_all_disabled').prop('disabled',true);
            }
            get_totals();
        });
    </script>




                {{----------------------------when include checkbox checked color tax field with lavendar----------------}}
                <script>

                    $(document).on('change','.include_check ',function(){

                        if($(this).is(":checked")) {
                            $(this).parents('tr').find('.tax_value').parents('.fstElement').css('background-color', 'lavender');
                        }
                        else{
                            $(this).parents('tr').find('.tax_value').parents('.fstElement').css('background-color', '#fff');
                        }
                    });

               </script>


                <script>

                    $(document).on('click','#print',function () {

                        var id = $('#set_po_id').attr('current_invoice_id');
                        window.open('/get_invoice_rpt/'+ id +'','_blank');

                    });


                </script>


                {{-----------   on leave page -------------------}}
                <script type='text/javascript'>

                    $(window).bind('beforeunload', function(){
                        if($('#save_po').length){
                            return 'Are you sure you want to leave?';
                        }

                    });


                </script>
                <script>
                    $(".myselect1").select2({
                        width: 'resolve'

                    });

                    $(".myselect2").select2({
                        width: 'resolve'

                    });

                    $(".myselect3").select2({
                        width: 'resolve'

                    });


                </script>


    @if(isset($master_inventory_details))
        <script>
            $(document).ready(function () {
                console.log('called');
                get_all_inventory_data();

            });
        </script>

    @endif


@endsection