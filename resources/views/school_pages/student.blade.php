@extends('layouts.main_app')

@section('main_content')
    <!-- ace CSS -->



    <link rel="stylesheet" href="{{asset('assets/css/fonts.googleapis.com.css')}}" />
    <link rel="stylesheet" href="{{asset('assets/css/ace.min.css')}}" class="ace-main-stylesheet" id="main-ace-style" />
    <link rel="stylesheet" href="{{asset('assets/css/ace-skins.min.css')}}" />
    <link rel="stylesheet" href="{{asset('assets/css/ace-rtl.min.css')}}" />

    <link href="{{asset('css/select2.min.css')}}" rel="stylesheet" />
    {{--<link rel="stylesheet" href="{{asset('assets/css/select2.min.css')}}" />--}}
    <style>
        body{

            font-family: Gill Sans,Gill Sans MT,Calibri,sans-serif!important;
            font-size: 15px!important;
            color: #676a6d!important;
        }
        .navbar .navbar-nav>li>a{
            font-size: 15px!important;
        }
        .control-label{
            font-size: 15px!important;
        }

        .navbar-default .navbar-nav>.active>a, .navbar-default .navbar-nav>.active>a:focus, .navbar-default .navbar-nav>.active>a:hover
        {
            background-color: transparent;
            COLOR: #00AAFF!important;
            FONT-SIZE: 20PX!important;
        }
        .side_sheets
        {
            padding: 0;
            margin: 0 1px 10px 0px;
        }

        .navbar-default .navbar-nav>li>a:focus, .navbar-default .navbar-nav>li>a:hover
        {
            color: #bbb!important;
        }
        #student_side_bar{
            padding-right: 123px;
        }

        nav.navbar.navbar-default.navbar-fixed-top{
            background-color: #2B333E;
        }

        .navbar .navbar-nav>li{
            border:0px!important;
        }

        .navbar2{

            background-color: #f8f8f8!important;
            -webkit-box-shadow: 1px 2px 3px rgba(0, 0, 0, 0.1)!important;
        }
        .navbar{
            min-height: 50px!important;

        }

        .main-container:before{
            position: relative!important;

        }
        .breadcrumb>li>a{
            font-size: 15px!important;
        }

        .btn-danger {
            color: #fff;
            background-color: #d9534f;
            border-color: #d43f3a;
        }
        .btn {
            display: inline-block;
            padding: 6px 12px;
            margin-bottom: 0;
            font-size: 14px;
            font-weight: 400;
            line-height: 1.42857143;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            touch-action: manipulation;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-image: none;
            border: 1px solid transparent;
            border-radius: 4px;
            text-shadow: 0 0px 0 rgba(0,0,0,.25);
        }

        .navbar-default {
            -webkit-box-shadow: 1px 2px 3px rgba(0, 0, 0, 0.1) !important;
        }
        .btn-primary
        {
            background-color: #00AAFF!important;
            border-color: #00a0f0!important;
        }
        #creat{
            box-shadow: 0px 1px 2px 0 rgba(0, 0, 0, 0.2);



            color: #676a6d!important;
            background-color: #dddddd!important;
        }
        .main-content {
            padding: 11px  10px;
        }
        .breadcrumb>li+li:before{
            font-family: FontAwesome;
            font-size: 20px;
            content: "/"!important;
            color: #B2B6BF;
            padding: 0;
            margin: 0 8px 0 0;
            position: relative;
            top: 1px;
        }
        .collapse > .row>.col-md-4>.btn, .collapse > .row>.col-md-4>.dropdown>.btn {
            margin-top: 0px;
        }

        .breadcrumb{
            display: inline-block;
            float: left;
            position: relative;
            margin-top: 0;
            margin-right: 0.2em;
            margin-bottom: 0;
            padding: 0.5em;
            color: gray;
            white-space: nowrap;
            margin-left: 0px;
        }

        .dropdown-menu>li.active:hover>a, .dropdown-menu>li.active>a, .dropdown-menu>li:hover>a, .dropdown-menu>li>a:active, .dropdown-menu>li>a:focus
        {
            color: #262626;
            text-decoration: none;
            background-color: #f5f5f5;
        }
        .dropdown-menu>li>a{
            font-size: 14px;
            display: block;
            padding: 3px 20px;
            clear: both;
            font-weight: 400;
            line-height: 1.42857143;
            color: #333;
            white-space: nowrap;
        }

        /*upload photo */



        .avatar-upload {
            position: relative;
            max-width: 205px;

        }
        .avatar-upload .avatar-edit {
            position: absolute;
            right: 12px;
            z-index: 1;
            top: 10px;
        }
        .avatar-upload .avatar-edit input {
            display: none;
        }
        .avatar-upload .avatar-edit input + label {
            display: inline-block;
            width: 34px;
            height: 34px;
            margin-bottom: 0;
            border-radius: 100%;
            background: #eee;
            border: 3px solid #fff;
            box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.12);
            cursor: pointer;
            font-weight: normal;
            transition: all 0.2s ease-in-out;
        }
        .avatar-upload .avatar-edit input + label:hover {
            background: #f1f1f1;
            border-color: #d6d6d6;
        }
        .avatar-upload .avatar-edit input + label:after {
            content: "\f040";
            font-family: "FontAwesome";
            color: #757575;
            position: absolute;
            top: 10px;
            left: 0;
            right: 0;
            text-align: center;
            margin: auto;
        }
        .avatar-upload .avatar-preview {
            width: 192px;
            height: 192px;
            position: relative;
            border-radius: 100%;
            border: 6px solid #eee;
            box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
        }
        .avatar-upload .avatar-preview > div {
            width: 100%;
            height: 100%;
            border-radius: 100%;
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
        }
        input#code{
            width: 100%;
        }



        input[type=checkbox].ace.ace-switch.ace-switch-6:checked+.lbl::after{
            left: 32px;
            background-color: #337ab7;
            border: 4px solid #337ab7;

        }
        input[type=checkbox].ace.ace-switch.ace-switch-6+.lbl::after{
            background: rgb(255, 255, 255);
            border-radius: 16px;
            box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
            content: '';
            height: 24px;
            left: 0px;
            margin-top: -8px;
            position: absolute;
            top: 6px;
            transition: all 0.3s ease-in-out;
            width: 24px;
        }

        input[type=checkbox].ace.ace-switch.ace-switch-6:checked+.lbl::before{
            border-color: #B7D3E5;
            background-color: #1363a9;
        }

        input[type=checkbox].ace.ace-switch.ace-switch-6+.lbl::before{
            background: rgb(0, 0, 0);
            box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
            border-radius: 8px;
            content: '';
            height: 16px;
            margin-top: 3px;
            position: absolute;
            opacity: 0.3;
            transition: all 0.4s ease-in-out;
            width: 40px;
            left: 14px;
        }
        input[type=checkbox].ace.ace-switch.ace-switch-6:checked+.lbl::before{
            content:'';
        }
        input[type=checkbox].ace.ace-switch.ace-switch-6:checked+.lbl::after {
            left: 45px;
            background-color: #337ab7;
            border: 4px solid #337ab7;
        }
        input[type=checkbox].ace.ace-switch.ace-switch-6+.lbl::after{
            border: 4px solid #fff;
        }
        input[type=checkbox].ace.ace-switch {
            width: 71px;
        }
        .input-md, .input-medium{
            width: 170px;
            max-width: 100%;
        }

        .required, .required + .select2-container--default .select2-selection--single {
            background-color: lavender;
        }
        .select2-container{
            width: 208px!important;
        }

        input#parent_first_name,input#parent_second_name,input#parent_third_name,input#parent_family_name,input#parent_id_number,input#parent_passport_id,textarea#parent_address{
            width: 70%;
        }

        input#mother_first_name,input#mother_second_name,input#mother_third_name,input#mother_family_name,input#mother_id_number,input#mother_passport_id,textarea#mother_address{
            width: 70%;
        }
        span.input-icon.input-icon-right{
            width: 70%;
        }
        input#parent_email,input#form_field_phone{
            width:100%;
        }
        input#mother_email,input#mother_phone{
            width:100%;
        }

        textarea#parent_address,textarea#parent_notes{
            width: 69.5%!important;
        }
        textarea#mother_address,textarea#mother_notes{
            width: 69.5%!important;
        }

        .ui-jqgrid .ui-jqgrid-view, .ui-jqgrid .ui-paging-info, .ui-jqgrid .ui-pg-selbox, .ui-jqgrid .ui-pg-table
        {
            font-size: 15px;
        }


        .ui-jqgrid-view{
            width: 200%!important;
        }

        .table>thead>tr{
            background-image: linear-gradient(to bottom,#fff 0,#fff 100%);
        }

        .table{
            margin-bottom: 0px;
        }


        .ui-jqgrid-sortable{
            text-align: center;
            font-size: 15px;
        }


        .ui-jqgrid .ui-jqgrid-labels{
            background-image: linear-gradient(to bottom,#FFF 0,#FFF 100%);
        }

        .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th
        {
            line-height: 14px;
        }
        div#code-error
        {
            margin-left: 254px;
        }

        .help-block {
            display: block;
            margin-top: 5px;
            margin-bottom: 10px;
            color: #D16E6C;
        }

        .steps>li.active .step, .steps>li.active:before, .steps>li.complete .step, .steps>li.complete:before {
            border-color: #00AAFF;
        }

        .steps>li.active .step, .steps>li.active:before, .steps>li.complete .step, .steps>li.complete:before {
            border-color: #00AAFF;
        }
        #creat_dep{
            background-color: #dddddd!important;
            color:#000!important;
        }
        .blue {
            color: #00AAFF!important;
        }


    </style>






    <div class="main">





        <!-- Navbar-content -->
        <nav class="navbar navbar-default navbar2">

            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                @if(isset($query))
                    <div id="set_po_id" current_po_id="{{$query->person_id}}" current_po_status=""
                         closed_or_not="" father_id = "{{$query_father->par_id}}" mother_id = "{{$query_mother->par_id}}"
                         current_id="{{Auth::user()->id}}"  staffs_id="{{--{{$query_father->Relation_Staff_ID}}{{$query_mother->Relation_Staff_ID}}--}}@if(isset($staff_data_all)){{$staff_data_all->employee_id}}@endif" staffs_gender="@if(isset($staff_data_all)){{$staff_data_all->gender}}@endif"></div>
                @else
                    <div id="set_po_id" current_po_id="" current_po_status="draft_po" closed_or_not="0"
                         current_id="{{Auth::user()->id}}"  father_id = "" mother_id = "" staffs_id="" staffs_gender=""></div>
                @endif

                <div id="staff_child_data" child_count="" child_remain="" max_child="" ></div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                    <!--breadcrumbs-->
                    <div class="row">

                        <div class="col-md-10 col-lg-10">

                            <ol class="breadcrumb breadcrumb_nav">
                                <li><a href="/students_table">Student</a></li>
                                <li class="active"></li>
                            </ol>
                        </div>

                        @if(isset($query))
                            <div class="col-md-2 col-lg-2 col-sm-4 col-xs-4">
                                @if(@\App\students::where('person_id','<',$query->person_id)->orderBy('person_id','desc')->first()->person_id)
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <a href="/student/{{@\App\students::where('person_id','<',$query->person_id)->orderBy('person_id','desc')->first()->person_id}}"><span
                                                    class="glyphicon glyphicon-arrow-left"></span>
                                        </a>
                                    </div>
                                @endif
                                @if(@\App\students::where('person_id','>',$query->person_id)->orderBy('person_id','asc')->first()->person_id)
                                    <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                                        <a href="/student/{{@\App\students::where('person_id','>',$query->person_id)->orderBy('person_id','asc')->first()->person_id}}">
                                    <span class="glyphicon glyphicon-arrow-right">
                                </span>
                                        </a>
                                    </div>
                                @endif
                            </div>
                        @endif

                    </div>


                    <!--End breadcrumbs-->
                    <div class="row">
                        <div class="col-lg-4  col-md-4 col-sm-6 col-xs-6 buttons">
                            @if(isset($query))
                                <button id="edit_po" type="button" class="btn btn-danger ">Edit</button>
                                <button type="button" class="btn" id="creat_dep">Create</button>
                            @else
                                <button id="save_po" type="button" class="btn btn-danger ">Save</button>
                                {{--<button id="edit_user" type="button" class="btn btn-danger " >Edit</button>--}}
                                <button type="button" class="hide btn" id="creat_dep">Create</button>
                            @endif
                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-4 ">
                            @if(isset($query))
                                <div class="dropdown">
                                    <button class=" btn btn-primary  dropdown-toggle" type="button"
                                            data-toggle="dropdown" id="more" hidden>More
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li class="  " data-toggle="modal" data-target="#exampleModal"><a href="#"
                                                                                                          id="delete_btn">Delete</a>
                                        </li>
                                        {{--<li><a href="#">Export</a></li>
                                        <li><a href="#" id="dublicate_dep">Duplicate</a></li>--}}
                                        <li><a href="#" id="print">Print</a></li>

                                    </ul>
                                </div>
                            @else
                                <div class="dropdown">
                                    <button class="hide btn btn-primary  dropdown-toggle" type="button"
                                            data-toggle="dropdown" id="more" hidden>More
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li class="  " data-toggle="modal" data-target="#exampleModal"><a href="#"
                                                                                                          id="delete_btn">Delete</a>
                                        </li>
                                        {{--<li><a href="#">Export</a></li>
                                        <li><a href="#" id="dublicate_dep">Duplicate</a></li>--}}
                                        <li><a href="#" id="print">Print</a></li>

                                    </ul>
                                </div>
                            @endif
                        </div>
                    </div>

                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Student</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    Are You Sure to Delete this Student ?!!
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-danger fa fa-trash" id="delete_po">Delete
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{----------------------------------------}}

                </div><!--collapse -->

            </div>
        </nav>
        <!-- End Navbar-content -->
        <!-- Success messages -->
        <div class="alert alert-success alert-autocloseable-success" style="display:none;position:absolute;" >
            You are finished!

        </div>
        <!--end Success messages -->

        <div class="main-content">

            <div class="container-fluid">

                <!-- INPUTS -->
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Student Regesteration</h3>
                    </div>
                    <div class="panel-body">


                        <div class="page-content">
                            <div class="widget-main">
                                <div id="fuelux-wizard-container" class="no-steps-container">
                                    <div>
                                        <ul class="steps" style="margin-left: 0">
                                            <li id="first_step" data-step="1" class="active">
                                                <span class="step">1</span>
                                                <span class="title">Student Info</span>
                                            </li>

                                            <li id="second_step" data-step="2">
                                                <span class="step">2</span>
                                                <span class="title">Father Info</span>
                                            </li>

                                            <li id="third_step" data-step="3">
                                                <span class="step">3</span>
                                                <span class="title">Mother Info</span>
                                            </li>

                                            {{-- <li data-step="4">
                                                 <span class="step">4</span>
                                                 <span class="title">Other Info</span>
                                             </li>--}}
                                        </ul>
                                    </div>


                                    <div class="step-content pos-rel">
                                        <div id="first_step_plane" class="step-pane active" data-step="1">

                                            <form class="" name="step1_validation-form" id="step1_validation-form" method="get" novalidate="novalidate" enctype="multipart/form-data">

                                                <div class="row">

                                                    <div class="avatar-upload">
                                                        <div class="avatar-edit">
                                                            <input type='file' id="id-input-file-3" class="set_all_disabled" name="id-input-file-3"{{--accept=".png, .jpg, .jpeg" --}} @if(isset($query)) disabled @endif/>
                                                            <label for="id-input-file-3"></label>
                                                        </div>
                                                        <div class="avatar-preview">
                                                            <div id="imagePreview" style="background-image:
                                                            @if(isset($query))
                                                            @if(file_exists('uploads/students/'.$query->person_id.'_.png'))
                                                            {{--@if(Storage::disk('uploads')->has('file.jpg'))--}}
                                                                    url({{asset('uploads/students/'.$query->person_id.'_.png')}});
                                                            @else
                                                                    url({{asset('uploads/students/defult_image.jpg')}});
                                                            @endif
                                                            @endif
                                                                    ">
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                                <br>

                                                <div class="row">
                                                    {{-- @if(isset($query))
                                                         <div id="show_student_image">
                                                     <span class="profile-picture " style=" width: 45%;">

                                                         @if(file_exists('uploads/students/'.$query->person_id.'_.jpg'))
                                                         --}}{{--@if(Storage::disk('uploads')->has('file.jpg'))--}}{{--
                                                             <img id="profile_img" style="height: 140px;" class=" img-responsive " alt="Student Pic" src="{{asset('uploads/students/'.$query->person_id.'_.jpg')}}" />
                                                         @else
                                                             <img id="profile_img" style="height: 140px;" class=" img-responsive " alt="defult Student Pic" src="{{asset('uploads/students/defult_image.jpg')}}" />
                                                         @endif
                                                     </span>
                                                         </div>

                                                         <div id="show_add_image" style="display: none;">

                                                             <div class="col-xs-12 col-sm-8">
                                                                 <div class="form-group">
                                                                     <div class="col-xs-12">
                                                                         <input multiple="" type="file" name="id-input-file-3" id="id-input-file-3" />
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                     @else
                                                         <div id="show_add_image">

                                                             <div class="col-xs-12 col-sm-8">
                                                                 <div class="form-group">
                                                                     <div class="col-xs-12">
                                                                         <input multiple="" type="file" name="id-input-file-3" id="id-input-file-3" />
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                     @endif
             --}}
                                                    {{--  <div class="col-xs-12 col-sm-4">
                                                          <span class="profile-picture " style=" width: 45%;">
                                                      <img style="height: 140px;" class=" img-responsive " alt="Student Pic" src="" />
                                                  </span>
                                                          <input name="image_input"  id="image_input" type="file">
                                                      </div>--}}
                                                    <div class="col-md-6 col-lg-6 col-xs-12 col-sm-8">
                                                        <div class="form-group">
                                                            <label class="col-md-6 col-lg-6 control-label  " for="form-field-username">First Name</label>

                                                            <div class="col-md-6 col-lg-6">
                                                                <div class="form-group">

                                                                    @if(isset($query))
                                                                        <input old_img_value="{{$query->First_name}}" edit_student_id="{{$query->person_id}}" class="set_all_disabled form-control required" value="{{$query->First_name}}" type="text" name="first_name" id="first_name" placeholder="First Name" @if(isset($query)) disabled @endif>
                                                                    @else
                                                                        <input class="set_all_disabled form-control required" type="text" name="first_name" id="first_name" placeholder="First Name" @if(isset($query)) disabled @endif>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>



                                                    </div>
                                                </div>

                                                <br>
                                                <div class="row">
                                                    <div class="col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <label class="col-md-6 col-lg-6 col-sm-4 control-label no-padding-right" for="form-field-date">Birth Date</label>

                                                            <div class="col-md-6 col-lg-6 col-sm-8">
                                                                <div class="input-medium">
                                                                    <div class="input-group">

                                                                        @if(isset($query))
                                                                            <input class="set_all_disabled input-medium date-picker required" value="{{$query->Birth_of_date}}" name="birth_date" id="birth_date" type="text" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd" @if(isset($query)) disabled @endif>
                                                                        @else
                                                                            <input class="set_all_disabled input-medium date-picker required" name="birth_date" id="birth_date" type="text" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd" @if(isset($query)) disabled @endif>
                                                                        @endif
                                                                        <span class="input-group-addon">
																				<i class="ace-icon fa fa-calendar"></i>
																			</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <br>
                                                <div class="row">
                                                    <div class="col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <label class="col-md-6 col-lg-6 col-sm-4 control-label no-padding-right">Gender</label>

                                                            <div class="col-md-6 col-lg-6 col-sm-8">

                                                                @if(isset($query))
                                                                    <label class="radio-inline">
                                                                        <input @if ($query->Gander_type == 1)checked @endif name="form_field_radio" gender="1" type="radio" class="set_all_disabled " @if(isset($query)) disabled @endif>
                                                                        <span class="lbl middle"> Male</span>
                                                                    </label>

                                                                    &nbsp; &nbsp; &nbsp;
                                                                    <label class="radio-inline">
                                                                        <input @if ($query->Gander_type == 0) checked @endif name="form_field_radio" gender="0" type="radio" class="set_all_disabled " @if(isset($query)) disabled @endif>
                                                                        <span class="lbl middle"> Female</span>
                                                                    </label>
                                                                @else
                                                                    <label class="radio-inline">
                                                                        <input name="form_field_radio" gender="1" type="radio" class="set_all_disabled " @if(isset($query)) disabled @endif>
                                                                        <span class="lbl middle"> Male</span>
                                                                    </label>
                                                                    &nbsp; &nbsp; &nbsp;
                                                                    <label class="radio-inline">
                                                                        <input name="form_field_radio" gender="0" type="radio" class="set_all_disabled " @if(isset($query)) disabled @endif>
                                                                        <span class="lbl middle"> Female</span>
                                                                    </label>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <br>
                                                <div class="row">
                                                    <div class="col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <label class=" col-md-6 col-lg-6 col-sm-4 control-label no-padding-right" for="form-field-username">Nationality</label>

                                                            <div class="col-md-6 col-lg-6 col-sm-8">
                                                                <select id="nationalty" name="nationalty"
                                                                        class=" validate_tab required get_all_selectors set_all_disabled select_2_enable form-control select2 department_error err_input"
                                                                        @if(isset($query)) disabled @endif >
                                                                    <option value=""></option>
                                                                    @foreach($nationality as $id=>$role)
                                                                        @if(isset($query))
                                                                            @if($query->Nationalty_type_id == $id)
                                                                                <option selected value="{{$id}}">{{$role}}</option>
                                                                            @else
                                                                                <option value="{{$id}}">{{$role}}</option>
                                                                            @endif
                                                                        @else
                                                                            <option value="{{$id}}">{{$role}}</option>
                                                                        @endif
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <br>
                                                <div class="row">
                                                    <div class="col-md-6 col-lg-6">

                                                        <div class="form-group">
                                                            <label class="col-md-6 col-lg-6 col-sm-4 control-label no-padding-right" for="form-field-username">Code</label>

                                                            <div class="col-md-6 col-lg-6 col-sm-8">
                                                                @if(isset($query))
                                                                    <input old_img_value="{{$query->Ismas_code}}" value="{{$query->Ismas_code}}" class=" required set_all_disabled col-xs-12 col-sm-10" type="text" name="code" id="code" placeholder="Code" @if(isset($query)) disabled @endif>
                                                                @else
                                                                    <input class=" required set_all_disabled col-xs-12 col-sm-10" type="text" name="code" id="code" placeholder="Code" @if(isset($query)) disabled @endif>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div class="col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <label class="col-md-6 col-lg-6 col-sm-4 control-label no-padding-right" for="form-field-username">Acadmic Year</label>



                                                            <div class="col-md-6 col-lg-6 col-sm-8">

                                                                <select id="acadmic_year" name="acadmic_year"
                                                                        class="validate_tab required get_all_selectors set_all_disabled select_2_enable form-control select2 department_error err_input"
                                                                        @if(isset($query)) disabled @endif >
                                                                    <option value=""></option>
                                                                    @foreach($acadimic_year as $id=>$role)
                                                                        @if(isset($query))
                                                                            @if($query->reg_acadmic_year == $id)
                                                                                <option selected value="{{$id}}">{{$role}}</option>
                                                                            @else
                                                                                <option value="{{$id}}">{{$role}}</option>
                                                                            @endif
                                                                        @else
                                                                            <option value="{{$id}}">{{$role}}</option>
                                                                        @endif
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div class="col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <label class="col-md-6 col-lg-6 col-sm-4 control-label no-padding-right" for="form-field-date">Registration Date</label>

                                                            <div class="col-md-6 col-lg-6 col-sm-8">
                                                                <div class="input-medium">
                                                                    <div class="input-group">
                                                                        @if(isset($query))
                                                                            <input value="{{$query->registration_date}}" class="required set_all_disabled input-medium date-picker" name="registration_date" id="registration_date" type="text" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd" @if(isset($query)) disabled @endif>
                                                                        @else
                                                                            <input  value="{{$curr_date}}" class="required set_all_disabled input-medium date-picker" name="registration_date" id="registration_date" type="text" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd" @if(isset($query)) disabled @endif>
                                                                        @endif
                                                                        <span class="input-group-addon">
																				<i class="ace-icon fa fa-calendar"></i>
																			</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div class="col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <label class="col-md-6 col-lg-6 col-sm-4 control-label " for="form-field-username">Registration Class</label>

                                                            <div class="col-md-6 col-lg-6 col-sm-8">
                                                                <select id="registration_class" name="registration_class"
                                                                        class="validate_tab required get_all_selectors set_all_disabled select_2_enable form-control select2 department_error err_input"
                                                                        @if(isset($query)) disabled @endif >
                                                                    <option value=""></option>
                                                                    @foreach($class as $id=>$role)
                                                                        @if(isset($query))
                                                                            @if($query->registration_class_id == $id)
                                                                                <option selected value="{{$id}}">{{$role}}</option>
                                                                            @else
                                                                                <option value="{{$id}}">{{$role}}</option>
                                                                            @endif
                                                                        @else
                                                                            <option value="{{$id}}">{{$role}}</option>
                                                                        @endif
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <br>
                                                <div class="row @if(!isset($query)) hide @endif " >
                                                    <div class="col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <label class="col-md-6 col-lg-6 col-sm-4 control-label no-padding-right" for="stock">Leave</label>
                                                            <div class="col-md-6 col-lg-6 col-sm-8">
                                                                @if(isset($query))
                                                                    <input @if($query->is_leave == 1) checked @endif id="leave_value"  get_v="0" data-toggle="toggle" type="checkbox"  data-on="Ready" data-off="Not Ready"  role="checkbox" class="set_all_disabled FormElement ace ace-switch ace-switch-6" @if(isset($query)) disabled @endif>
                                                                @else
                                                                    <input  id="leave_value"  get_v="0" data-toggle="toggle" type="checkbox"  data-on="Ready" data-off="Not Ready"  role="checkbox" class="set_all_disabled FormElement ace ace-switch ace-switch-6" @if(isset($query)) disabled @endif>
                                                                @endif
                                                                <span class="lbl"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <br>
                                                <div class="row">
                                                    <div class="col-md-6 col-lg-6">
                                                        <div class="form-group" id="l_date" @if(isset($query))@if($query->is_leave == 1)style="display: block" @else style="display: none" @endif @else style="display: none" @endif>
                                                            <label class="col-md-6 col-lg-6 col-sm-4 control-label no-padding-right" for="form-field-date">Leave Date</label>

                                                            <div class="col-md-6 col-lg-6 col-sm-8">
                                                                <div class="input-medium">
                                                                    <div class="input-group">
                                                                        @if(isset($query))
                                                                            <input class="set_all_disabled input-medium date-picker" name="leave_date" id="leave_date" type="text" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd" value="{{$query->Leave_date}}" @if(isset($query)) disabled @endif>
                                                                        @else
                                                                            <input class="set_all_disabled input-medium date-picker" name="leave_date" id="leave_date" type="text" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd" value="" >
                                                                        @endif
                                                                        <span class="input-group-addon">
																				<i class="ace-icon fa fa-calendar"></i>
																			</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                                <br>
                                                <div class="row">
                                                    <div class="col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            @if(isset($query))
                                                                <label class="col-md-6 col-lg-6 col-sm-4 control-label no-padding-right" for="stock">Has Sibling</label>
                                                                <div class=" col-md-6 col-lg-6 col-sm-8">
                                                                    <input @if(isset($has_siblings)) @if($has_siblings > 0) checked  sibling_v="1"@endif @endif id="sibling_value"   data-toggle="toggle" type="checkbox"  data-on="Ready" data-off="Not Ready"  role="checkbox" class="set_all_disabled ace ace-switch ace-switch-6" @if(isset($query)) disabled @endif>
                                                                    <span class="lbl"></span>
                                                                </div>
                                                            @else
                                                                <label class="col-md-6 col-lg-6 col-sm-4 control-label no-padding-right" for="stock">Has Sibling</label>
                                                                <div class="col-md-6 col-lg-6 col-sm-8">
                                                                    <input  id="sibling_value"  sibling_v="0" data-toggle="toggle" type="checkbox"  data-on="Ready" data-off="Not Ready"  role="checkbox" class="set_all_disabled ace ace-switch ace-switch-6" @if(isset($query)) disabled @endif>
                                                                    <span class="lbl"></span>
                                                                </div>
                                                            @endif
                                                            <br>

                                                        </div>

                                                    </div>
                                                </div>


                                                {{--------------------  relate to stuff  --------------------------}}
                                                <div class="row">
                                                    <div class="col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            @if(isset($query))
                                                                <label class="col-md-6 col-lg-6 col-sm-4 control-label no-padding-right" for="stock">Relate to staff</label>
                                                                <div class=" col-md-6 col-lg-6 col-sm-8">
                                                                    <input @if(isset($has_staff_siblings)) @if($has_staff_siblings > 0) checked  sibling_v="1"@endif @endif id="relate_to_stuff"   data-toggle="toggle" type="checkbox"  data-on="Ready" data-off="Not Ready"  role="checkbox" class="set_all_disabled ace ace-switch ace-switch-6" @if(isset($query)) disabled @endif>
                                                                    <span class="lbl"></span>
                                                                </div>
                                                            @else
                                                                <label class="col-md-6 col-lg-6 col-sm-4 control-label no-padding-right" for="stock">Relate to staff</label>
                                                                <div class="col-md-6 col-lg-6 col-sm-8">
                                                                    <input  id="relate_to_stuff"  sibling_v="0" data-toggle="toggle" type="checkbox"  data-on="Ready" data-off="Not Ready"  role="checkbox" class="set_all_disabled ace ace-switch ace-switch-6" @if(isset($query)) disabled @endif>
                                                                    <span class="lbl"></span>
                                                                </div>
                                                            @endif
                                                            <br>

                                                        </div>

                                                    </div>
                                                </div>
                                                {{---------------------------------------------------------}}

                                                <div class="row">
                                                    <div class="col-md-6 col-lg-6">
                                                        <div class="form-group">

                                                            {{-------------------  sibling students   -------------------------------------}}
                                                            <div id="all_sibling"  @if(isset($has_siblings)) @if($has_siblings > 0) style="display: block" @else style="display: none"  @endif @else style="display: none" @endif>

                                                                <label class="col-md-6 col-lg-6 col-sm-4 control-label">
                                                                    <input name="same_mother" id="same_mother" same_mother_v="0" type="checkbox" class="set_all_disabled ace" @if(isset($query)) disabled @endif>
                                                                    <span class="lbl"> Has Same Mother</span>
                                                                </label>
                                                                <label  id="sibling_delete_op" delete_option_all="0" ></label>
                                                                <div class="col-md-6 col-lg-6 col-sm-8">
                                                                    <select @if(isset($query)) defult_selected_value="{{$query->person_id}}" @endif id="sibling_selected_v" class="set_all_disabled selectpicker select2" data-live-search="true" @if(isset($query)) disabled @endif>
                                                                        {{--<option data-tokens="1 expenses" value="1">expenses</option>
                                                                        <option data-tokens="2 assets" value="2">assets</option>
                                                                        <option data-tokens="3 revene" value="3">revene</option>--}}
                                                                    </select>
                                                                </div>
                                                                <br>
                                                            </div>

                                                            {{--------------------------------    related stuff    ----------------------------------------------}}
                                                            <div id="all_school_stuff"  @if(isset($has_staff_siblings)) @if($has_staff_siblings > 0) style="display: block" @else style="display: none"  @endif @else style="display: none" @endif>

                                                                <label class="col-md-6 col-lg-6 col-sm-4 control-label"  delete_option_all="0" ></label>
                                                                <div class="col-md-6 col-lg-6 col-sm-8">

                                                                    <select defult_selected_value=""  id="related_stuff_id" class="set_all_disabled selectpicker select2" data-live-search="true" @if(isset($query)) disabled @endif>
                                                                        <option value=""></option>
                                                                        @foreach($staff_drop as $staff_d)
                                                                            @if(isset($staff_data_all))
                                                                                @if($staff_data_all->employee_id == $staff_d->employee_id)
                                                                                    <option selected value="{{$staff_d->employee_id}}">{{$staff_d->employee_name ." ". $staff_d->middle_name ." ". $staff_d->last_name ." ". $staff_d->family_name }}</option>
                                                                                @else
                                                                                    <option value="{{$staff_d->employee_id}}">{{$staff_d->employee_name ." ". $staff_d->middle_name ." ". $staff_d->last_name ." ". $staff_d->family_name }}</option>
                                                                                @endif
                                                                            @else
                                                                                <option value="{{$staff_d->employee_id}}">{{$staff_d->employee_name ." ". $staff_d->middle_name ." ". $staff_d->last_name ." ". $staff_d->family_name }}</option>
                                                                            @endif
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <br>
                                                            </div>
                                                            {{-------------------------------------------------------------------------------------}}




                                                            <div class="ui-jqgrid ui-widget ui-widget-content ui-corner-all open" id="gbox_grid-table" dir="ltr" style="margin-top: 30px;width: 100%; @if(isset($has_siblings)) @if($has_siblings > 0) display: block; @else display: none; @endif @else display: none; @endif" >
                                                                <div class="jqgrid-overlay ui-widget-overlay" id="lui_grid-table">

                                                                </div>


                                                                <div class="ui-jqgrid-view " role="grid" id="gview_grid-table" style="width: 100%;"><div class="" >

                                                                        <span class="control-label">Siblings Table</span>
                                                                    </div>


                                                                    <br>



                                                                    <div class="ui-jqgrid-hdiv ui-state-default" style="width: 100%;"><div>


                                                                            <table id="siblings_table_row" class="table table-bordred table-striped table-bordered table-striped table-hover table-condensed table-responsive " style="width: 100%;" role="presentation" aria-labelledby="gbox_grid-table">
                                                                                <thead>
                                                                                <tr class="" role="row">

                                                                                    <th >#NO</th>
                                                                                    <th >Student name</th>
                                                                                    <th >Code</th>
                                                                                    <th >Class</th>

                                                                                </tr>
                                                                                </thead>
                                                                                <tbody>

                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>

                                            </form>

                                        </div>


                                        <div id="second_step_plane" class="step-pane" data-step="2">
                                            <form class="" id="step2_validation-form" method="get" novalidate="novalidate">

                                                {{--<div id="enable_father_edit" class="form-group" style="display: none;">
                                                    <label class="col-sm-4 control-label no-padding-right" for="stock">Enable Edit</label>
                                                    <div class="col-sm-8">
                                                        <input  id="edit_father_data" one_time_edit_father="0"  edit_father_data="0" data-toggle="toggle" type="checkbox"  data-on="Ready" data-off="Not Ready"  role="checkbox" class="set_all_disabled ace ace-switch ace-switch-6" @if(isset($query)) disabled @endif>
                                                        <span class="lbl"></span>
                                                    </div>
                                                </div>--}}

                                                <br>
                                                <div class="row">
                                                    <div class="col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <label class=" col-sm-4 control-label no-padding-right" for="form-field-username">First Name</label>

                                                            <div class="col-sm-8">
                                                                @if(isset($query_father))

                                                                    @php $myfield = '1name' @endphp
                                                                    <input orignal_sibling_v="{{$query_father->$myfield}}" edit_father_id="{{$query_father->par_id}}" value="{{$query_father->$myfield}}" class="staff_disable_only set_all_disabled dis_able_father_edit col-xs-12 col-sm-10 required" type="text" name="parent_first_name" id="parent_first_name" placeholder="First Name" @if(isset($query)) disabled @endif>
                                                                @else
                                                                    <input class="staff_disable_only set_all_disabled dis_able_father_edit col-xs-12 col-sm-10 required" type="text" name="parent_first_name" id="parent_first_name" placeholder="First Name" @if(isset($query)) disabled @endif edit_father_id="">
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <br>
                                                <div class="row">
                                                    <div class="col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label no-padding-right" for="form-field-username">Second Name</label>

                                                            <div class="col-sm-8">

                                                                @if(isset($query_father))
                                                                    @php $myfield2 = '2name'  @endphp
                                                                    <input orignal_sibling_v="{{$query_father->$myfield2}}" value="{{$query_father->$myfield2}}" class="staff_disable_only set_all_disabled dis_able_father_edit col-xs-12 col-sm-10 required" type="text" name="parent_second_name" id="parent_second_name" placeholder="Second Name" @if(isset($query)) disabled @endif>
                                                                @else
                                                                    <input class="staff_disable_only set_all_disabled dis_able_father_edit col-xs-12 col-sm-10 required" type="text" name="parent_second_name" id="parent_second_name" placeholder="Second Name" @if(isset($query)) disabled @endif>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div class="col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label no-padding-right" for="form-field-username">Third Name</label>

                                                            <div class="col-sm-8">
                                                                @if(isset($query_father))
                                                                    @php $myfield3 = '3name' @endphp
                                                                    <input orignal_sibling_v="{{$query_father->$myfield3}}" value="{{$query_father->$myfield3}}" class="staff_disable_only set_all_disabled dis_able_father_edit col-xs-12 col-sm-10 required" type="text" name="parent_third_name" id="parent_third_name" placeholder="Third Name" @if(isset($query)) disabled @endif>
                                                                @else
                                                                    <input class="staff_disable_only set_all_disabled dis_able_father_edit col-xs-12 col-sm-10 required" type="text" name="parent_third_name" id="parent_third_name" placeholder="Third Name" @if(isset($query)) disabled @endif>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div class="col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label no-padding-right" for="form-field-username">Family Name</label>

                                                            <div class="col-sm-8">
                                                                @if(isset($query_father))
                                                                    <input orignal_sibling_v="{{$query_father->family_name}}" value="{{$query_father->family_name}}" class="staff_disable_only set_all_disabled dis_able_father_edit col-xs-12 col-sm-10 required" type="text" name="parent_family_name" id="parent_family_name" placeholder="Family Name" @if(isset($query)) disabled @endif>
                                                                @else
                                                                    <input class="staff_disable_only set_all_disabled dis_able_father_edit col-xs-12 col-sm-10 required" type="text" name="parent_family_name" id="parent_family_name" placeholder="Family Name" @if(isset($query)) disabled @endif>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div class="col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label no-padding-right" for="form-field-email">Email</label>

                                                            <div class="col-sm-8">
																	<span class="input-icon input-icon-right">
                                                                        @if(isset($query_father))
                                                                            <input orignal_sibling_v="{{$query_father->EMail}}" value="{{$query_father->EMail}}" class="staff_disable_only set_all_disabled dis_able_father_edit required" type="email" name="parent_email" id="parent_email" @if(isset($query)) disabled @endif>
                                                                        @else
                                                                            <input class="staff_disable_only set_all_disabled dis_able_father_edit required" type="email" name="parent_email" id="parent_email" @if(isset($query)) disabled @endif>
                                                                        @endif
                                                                        <i class="ace-icon fa fa-envelope"></i>
																	</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div class="col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label no-padding-right" for="form-field-phone">Phone</label>

                                                            <div class="col-sm-8">
																	<span class="input-icon input-icon-right">
																		@if(isset($query_father))
                                                                            <input orignal_sibling_v="{{$query_father->Contact_NO}}" value="{{$query_father->Contact_NO}}" class="staff_disable_only set_all_disabled dis_able_father_edit input-medium input-mask-phone required" type="text" name="form_field_phone" id="form_field_phone" disabled onkeypress="return (event.charCode == 8 || event.charCode == 1) ? null : event.charCode >= 48 && event.charCode <= 57 ||  event.charCode == 46">
                                                                        @else
                                                                            <input class="staff_disable_only set_all_disabled dis_able_father_edit input-medium input-mask-phone required" type="text" name="form_field_phone" id="form_field_phone" onkeypress="return (event.charCode == 8 || event.charCode == 1) ? null : event.charCode >= 48 && event.charCode <= 57 ||  event.charCode == 46">
                                                                        @endif
                                                                        <i class="ace-icon fa fa-phone fa-flip-horizontal"></i>
																	</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div class="col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label no-padding-right" for="form-field-username">ID Number</label>

                                                            <div class="col-sm-8">
                                                                @if(isset($query_father))
                                                                    <input orignal_sibling_v="{{$query_father->ID_NO}}" value="{{$query_father->ID_NO}}" class="staff_disable_only set_all_disabled dis_able_father_edit col-xs-12 col-sm-10" type="text" name="parent_id_number" id="parent_id_number" placeholder="ID Number" @if(isset($query)) disabled @endif>
                                                                @else
                                                                    <input class="staff_disable_only set_all_disabled dis_able_father_edit col-xs-12 col-sm-10" type="text" name="parent_id_number" id="parent_id_number" placeholder="ID Number" @if(isset($query)) disabled @endif>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div class="col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label no-padding-right" for="form-field-username">Passport ID</label>

                                                            <div class="col-sm-8">
                                                                @if(isset($query_father))
                                                                    <input orignal_sibling_v="{{$query_father->Pass_ID}}" value="{{$query_father->Pass_ID}}" class="staff_disable_only set_all_disabled dis_able_father_edit col-xs-12 col-sm-10" type="text" name="parent_passport_id" id="parent_passport_id" placeholder="Passport ID" @if(isset($query)) disabled @endif>
                                                                @else
                                                                    <input class="staff_disable_only set_all_disabled dis_able_father_edit col-xs-12 col-sm-10" type="text" name="parent_passport_id" id="parent_passport_id" placeholder="Passport ID" @if(isset($query)) disabled @endif>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div class="col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label no-padding-right" for="form-field-comment">Address</label>

                                                            <div class="col-sm-8">
                                                                @if(isset($query_father))
                                                                    <textarea orignal_sibling_v="{{$query_father->Address}}" class="staff_disable_only set_all_disabled dis_able_father_edit required"  name="parent_address" id="parent_address" style="margin: 0px; width: 83.5%; height: 60px;" @if(isset($query)) disabled @endif>{{$query_father->Address}}</textarea>
                                                                @else
                                                                    <textarea class="staff_disable_only set_all_disabled dis_able_father_edit required"  name="parent_address" id="parent_address" style="margin: 0px; width: 83.5%; height: 60px;" @if(isset($query)) disabled @endif></textarea>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div class="col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label no-padding-right" for="form-field-username">Country</label>

                                                            <div class="col-sm-8">
                                                                <select id="country_id" name="country_id"
                                                                        class="staff_disable_only validate_tab required get_all_selectors set_all_disabled select_2_enable form-control select2
                                                     department_error err_input"
                                                                        @if(isset($query_father)) disabled @endif >
                                                                    <option value=""></option>
                                                                    @foreach($country as $id=>$role)
                                                                        @if(isset($query_father))
                                                                            @if($query_father->Country_ID == $id)
                                                                                <option selected value="{{$id}}">{{$role}}</option>
                                                                            @else
                                                                                <option value="{{$id}}">{{$role}}</option>
                                                                            @endif
                                                                        @else
                                                                            <option value="{{$id}}">{{$role}}</option>
                                                                        @endif
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div class="col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label no-padding-right" for="stock">Finance</label>
                                                            <div class="col-sm-8">
                                                                @if(isset($query_father))
                                                                    <input checked orignal_sibling_v="{{$query_father->Finance}}" @if($query_father->Finance == 1) checked @endif id="finance"  finance_v="1" data-toggle="toggle" type="checkbox"  data-on="Ready" data-off="Not Ready"  role="checkbox" class=" staff_disable_only FormElement ace ace-switch ace-switch-5" @if(isset($query)) disabled @endif disabled>
                                                                @else
                                                                    <input checked id="finance"  finance_v="1" data-toggle="toggle" type="checkbox"  data-on="Ready" data-off="Not Ready"  role="checkbox" class="staff_disable_only  FormElement ace ace-switch ace-switch-5" @if(isset($query)) disabled @endif disabled disabled="disabled">
                                                                @endif
                                                                <span class="lbl"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div class="col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label no-padding-right" for="form-field-comment">Other Notes</label>

                                                            <div class="col-sm-8">
                                                                @if(isset($query_father))
                                                                    <textarea orignal_sibling_v="{{$query_father->other_Notes}}" class="staff_disable_only set_all_disabled dis_able_father_edit" id="parent_notes" style="margin: 0px; width: 83.5%; height: 60px;" @if(isset($query)) disabled @endif>{{$query_father->other_Notes}}</textarea>
                                                                @else
                                                                    <textarea class="staff_disable_only set_all_disabled dis_able_father_edit" id="parent_notes" style="margin: 0px; width: 83.5%; height: 60px;" @if(isset($query)) disabled @endif></textarea>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>

                                            </form>

                                        </div>

                                        <div id="third_step_plane" class="step-pane" data-step="3">
                                            <form class="" id="step3_validation-form" method="get" novalidate="novalidate">

                                                {{--<div id="enable_mother_edit" class="form-group" style="display: none;">
                                                    <label class="col-sm-4 control-label no-padding-right" for="stock">Enable Edit</label>
                                                    <div class="col-sm-8">
                                                        <input  id="edit_mother_data" one_time_edit_mother="0"  edit_mother_data="0" data-toggle="toggle" type="checkbox"  data-on="Ready" data-off="Not Ready"  role="checkbox" class="set_all_disabled ace ace-switch ace-switch-6" @if(isset($query)) disabled @endif>
                                                        <span class="lbl"></span>
                                                    </div>
                                                </div>--}}

                                                <br>

                                                <div class="row">
                                                    <div class="col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label no-padding-right" for="form-field-username">First Name</label>

                                                            <div class="col-sm-8">
                                                                @if(isset($query_mother))
                                                                    @php $first_mother = '1name' @endphp
                                                                    <input orignal_sibling_v="{{$query_mother->$first_mother}}" edit_mother_id="{{$query_mother->par_id}}" value="{{$query_mother->$first_mother}}" class="staff_disable_only set_all_disabled dis_able_mother_edit col-xs-12 col-sm-10" type="text" id="mother_first_name" placeholder="First Name"  @if(isset($query)) disabled @endif>
                                                                @else
                                                                    <input class="staff_disable_only set_all_disabled dis_able_mother_edit col-xs-12 col-sm-10" type="text" id="mother_first_name" placeholder="First Name" @if(isset($query)) disabled @endif edit_mother_id="">
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div class="col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label no-padding-right" for="form-field-username">Second Name</label>

                                                            <div class="col-sm-8">
                                                                @if(isset($query_mother))
                                                                    @php $second_mother = '2name' @endphp
                                                                    <input orignal_sibling_v="{{$query_mother->$second_mother}}" value="{{$query_mother->$second_mother}}" class="staff_disable_only set_all_disabled dis_able_mother_edit col-xs-12 col-sm-10" type="text" id="mother_second_name" placeholder="Second Name" @if(isset($query)) disabled @endif>
                                                                @else
                                                                    <input class="staff_disable_only set_all_disabled dis_able_mother_edit col-xs-12 col-sm-10" type="text" id="mother_second_name" placeholder="Second Name" @if(isset($query)) disabled @endif>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div class="col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label no-padding-right" for="form-field-username">Third Name</label>

                                                            <div class="col-sm-8">
                                                                @if(isset($query_mother))
                                                                    @php $last_mother = '3name' @endphp
                                                                    <input orignal_sibling_v="{{$query_mother->$last_mother}}" value="{{$query_mother->$last_mother}}" class="staff_disable_only set_all_disabled dis_able_mother_edit col-xs-12 col-sm-10" type="text" id="mother_third_name" placeholder="Third Name" @if(isset($query)) disabled @endif>
                                                                @else
                                                                    <input class="staff_disable_only set_all_disabled dis_able_mother_edit col-xs-12 col-sm-10" type="text" id="mother_third_name" placeholder="Third Name" @if(isset($query)) disabled @endif>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <br>
                                                <div class="row">
                                                    <div class="col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label no-padding-right" for="form-field-username">Family Name</label>

                                                            <div class="col-sm-8">
                                                                @if(isset($query_mother))
                                                                    <input orignal_sibling_v="{{$query_mother->family_name}}" value="{{$query_mother->family_name}}" class="staff_disable_only set_all_disabled dis_able_mother_edit col-xs-12 col-sm-10" type="text" id="mother_family_name" placeholder="Family Name" @if(isset($query)) disabled @endif>
                                                                @else
                                                                    <input class="staff_disable_only set_all_disabled dis_able_mother_edit col-xs-12 col-sm-10" type="text" id="mother_family_name" placeholder="Family Name" @if(isset($query)) disabled @endif>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div class="col-md-6 col-lg-6">

                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label no-padding-right" for="form-field-email">Email</label>

                                                            <div class="col-sm-8">
																	<span class="input-icon input-icon-right">
                                                                        @if(isset($query_mother))
                                                                            <input orignal_sibling_v="{{$query_mother->EMail}}" value="{{$query_mother->EMail}}" class="staff_disable_only set_all_disabled dis_able_mother_edit" type="email" id="mother_email" @if(isset($query)) disabled @endif>
                                                                        @else
                                                                            <input class="staff_disable_only set_all_disabled dis_able_mother_edit" type="email" id="mother_email" @if(isset($query)) disabled @endif>
                                                                        @endif
                                                                        <i class="ace-icon fa fa-envelope"></i>
																	</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div class="col-md-6 col-lg-6">

                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label no-padding-right" for="form-field-phone">Phone</label>

                                                            <div class="col-sm-8">
																	<span class="input-icon input-icon-right">
                                                                        @if(isset($query_mother))
                                                                            <input orignal_sibling_v="{{$query_mother->Contact_NO}}" value="{{$query_mother->Contact_NO}}" class="staff_disable_only set_all_disabled dis_able_mother_edit input-medium input-mask-phone" type="text" id="mother_phone" @if(isset($query)) disabled @endif onkeypress="return (event.charCode == 8 || event.charCode == 1) ? null : event.charCode >= 48 && event.charCode <= 57 ||  event.charCode == 46">
                                                                        @else
                                                                            <input class="staff_disable_only set_all_disabled dis_able_mother_edit input-medium input-mask-phone" type="text" id="mother_phone" @if(isset($query)) disabled @endif onkeypress="return (event.charCode == 8 || event.charCode == 1) ? null : event.charCode >= 48 && event.charCode <= 57 ||  event.charCode == 46">
                                                                        @endif
                                                                        <i class="ace-icon fa fa-phone fa-flip-horizontal"></i>
																	</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div class="col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label no-padding-right" for="form-field-username">ID Number</label>

                                                            <div class="col-sm-8">
                                                                @if(isset($query_mother))
                                                                    <input orignal_sibling_v="{{$query_mother->ID_NO}}" value="{{$query_mother->ID_NO}}" class="staff_disable_only set_all_disabled dis_able_mother_edit col-xs-12 col-sm-10" type="text" id="mother_id_number" placeholder="ID Number" @if(isset($query)) disabled @endif>
                                                                @else
                                                                    <input class="staff_disable_only set_all_disabled dis_able_mother_edit col-xs-12 col-sm-10" type="text" id="mother_id_number" placeholder="ID Number" @if(isset($query)) disabled @endif>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div class="col-md-6 col-lg-6">

                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label no-padding-right" for="form-field-username">Passport ID</label>

                                                            <div class="col-sm-8">
                                                                @if(isset($query_mother))
                                                                    <input orignal_sibling_v="{{$query_mother->Pass_ID}}" value="{{$query_mother->Pass_ID}}" class="staff_disable_only set_all_disabled dis_able_mother_edit col-xs-12 col-sm-10" type="text" id="mother_passport_id" placeholder="Passport ID" @if(isset($query)) disabled @endif>
                                                                @else
                                                                    <input class="staff_disable_only set_all_disabled dis_able_mother_edit col-xs-12 col-sm-10" type="text" id="mother_passport_id" placeholder="Passport ID" @if(isset($query)) disabled @endif>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div class="col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label no-padding-right" for="form-field-comment">Address</label>

                                                            <div class="col-sm-8">
                                                                @if(isset($query_mother))
                                                                    <textarea orignal_sibling_v="{{$query_mother->Address}}" class="staff_disable_only set_all_disabled dis_able_mother_edit"  id="mother_address" style="margin: 0px; width: 83.5%; height: 60px;" @if(isset($query)) disabled @endif>{{$query_mother->Address}}</textarea>
                                                                @else
                                                                    <textarea class="staff_disable_only set_all_disabled dis_able_mother_edit"  id="mother_address" style="margin: 0px; width: 83.5%; height: 60px;" @if(isset($query)) disabled @endif></textarea>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div class="col-md-6 col-lg-6">

                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label no-padding-right" for="form-field-username">Country</label>

                                                            <div class="col-sm-8">
                                                                <select id="mother_country_id"
                                                                        class="staff_disable_only validate_tab required get_all_selectors set_all_disabled select_2_enable form-control select2 department_error err_input"
                                                                        @if(isset($query_mother)) disabled @endif >
                                                                    <option value=""></option>
                                                                    @foreach($country as $id=>$role)
                                                                        @if(isset($query_mother))
                                                                            @if($query_mother->Country_ID == $id)
                                                                                <option selected value="{{$id}}">{{$role}}</option>
                                                                            @else
                                                                                <option value="{{$id}}">{{$role}}</option>
                                                                            @endif
                                                                        @else
                                                                            <option value="{{$id}}">{{$role}}</option>
                                                                        @endif
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div class="col-md-6 col-lg-6">

                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label no-padding-right" for="stock">Finance</label>
                                                            <div class="col-sm-8">
                                                                @if(isset($query_mother))
                                                                    <input orignal_sibling_v="{{$query_mother->Finance}}" @if($query_mother->Finance == 1) checked @endif id="mother_finance"  finance_v="1" data-toggle="toggle" type="checkbox"  data-on="Ready" data-off="Not Ready"  role="checkbox" class="staff_disable_only set_all_disabled dis_able_mother_edit FormElement ace ace-switch ace-switch-5" @if(isset($query)) disabled @endif>
                                                                @else
                                                                    <input  id="mother_finance"  finance_v="0" data-toggle="toggle" type="checkbox"  data-on="Ready" data-off="Not Ready"  role="checkbox" class="staff_disable_only set_all_disabled dis_able_mother_edit FormElement ace ace-switch ace-switch-5" @if(isset($query)) disabled @endif>
                                                                @endif
                                                                <span class="lbl"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div class="col-md-6 col-lg-6">

                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label no-padding-right" for="form-field-comment">Other Notes</label>

                                                            <div class="col-sm-8">
                                                                @if(isset($query_mother))
                                                                    <textarea orignal_sibling_v="{{$query_mother->other_Notes}}" class="staff_disable_only set_all_disabled dis_able_mother_edit" id="mother_notes" style="margin: 0px; width: 83.5%; height: 60px;" @if(isset($query)) disabled @endif>{{$query_mother->other_Notes}}</textarea>
                                                                @else
                                                                    <textarea class="staff_disable_only set_all_disabled dis_able_mother_edit" id="mother_notes" style="margin: 0px; width: 83.5%; height: 60px;" @if(isset($query)) disabled @endif></textarea>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </form>
                                        </div>

                                        {{--  <div class="step-pane" data-step="4">
                                              <div class="center">
                                                  <h3 class="green">Congrats!</h3>
                                                  Your product is ready to ship! Click finish to continue!
                                              </div>
                                          </div>--}}
                                    </div>
                                </div>




                                <div class="wizard-actions">
                                    <button class="btn btn-prev" disabled="disabled">
                                        <i class="ace-icon fa fa-arrow-left"></i>
                                        Prev
                                    </button>

                                    <button class="btn btn-success btn-next" data-last="Finish">
                                        Next
                                        <i class="ace-icon fa fa-arrow-right icon-on-right"></i>
                                    </button>
                                </div>
                            </div><!-- /.widget-main -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END INPUTS -->


    </div>

@endsection


@section('custom_footer')
    <script src="{{asset('assets/scripts/bootstrap-datepicker.min.js')}}"></script>

    <script src="{{asset('js/select2.min.js')}}"></script>



    <!-----------------active link in nav and active link in sidebar ---->
    <script>
        $('.side_sheets').addClass('hide');
        $('#admission').removeClass('hide');

        $("#menu li").removeClass('active');
        $('#admission_nav_bar').addClass('active');

        $('#pages ul li a').removeClass('active');
        $('#student_side_bar').addClass('active');

    </script>




    <!--- ace Scripts -->
    <script type="text/javascript">
        if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
    </script>

    <script src="{{asset('assets/scripts/ace-extra.min.js')}}"></script>

    <!-- page specific plugin scripts -->
    <script src="{{asset('assets/scripts/wizard.min.js')}}"></script>
    <script src="{{asset('assets/scripts/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/scripts/jquery-additional-methods.min.js')}}"></script>
    <script src="{{asset('assets/scripts/bootbox.js')}}"></script>
    <script src="{{asset('assets/scripts/jquery.maskedinput.min.js')}}"></script>

    <!-- ace scripts -->
    <script src="{{asset('assets/scripts/ace-elements.min.js')}}"></script>
    <script src="{{asset('assets/scripts/ace.min.js')}}"></script>


    {{------------------  validate form  ---------------------}}
    <script type="text/javascript">
        jQuery(function($) {

            $('[data-rel=tooltip]').tooltip();

            $('.select2').css('width','200px').select2({allowClear:true})
                .on('change', function(){
                    $(this).closest('form').validate().element($(this));
                });


            var $validation = true;
            var $validation_father = true;
            $('#fuelux-wizard-container')
                .ace_wizard({
                    //step: 2 //optional argument. wizard will jump to step "2" at first
                    //buttons: '.wizard-actions:eq(0)'
                })
                .on('actionclicked.fu.wizard' , function(e, info){
                    if(info.step == 1 && $validation )  {
                        if(!$('#step1_validation-form').valid()) e.preventDefault();
                    }
                    if(info.step == 2 && $validation_father && info.direction === 'next') {
                        if(!$('#step2_validation-form').valid()) e.preventDefault();
                    }
                    if(info.step == 3 && $validation) {
                        if(!$('#step3_validation-form').valid()) e.preventDefault();
                    }
                })
                //.on('changed.fu.wizard', function() {
                //})
                .on('finished.fu.wizard', function(e) {
                    $('html, body').animate({scrollTop: 0}, 'fast');
                    /*$("#save_po").attr('disabled', false);*/
                    if ($('#save_po').length) {
                        console.log('finished ');
                        notification('glyphicon glyphicon-ok-sign', '', 'Please Press Save ', 'success');
                    }
                }).on('stepclick.fu.wizard', function(e){
                //e.preventDefault();//this will prevent clicking and selecting steps
            });


            //jump to a step
            /**
             var wizard = $('#fuelux-wizard-container').data('fu.wizard')
             wizard.currentStep = 3;
             wizard.setState();
             */

            //determine selected step
            //wizard.selectedItem().step



            //hide or show the other form which requires validation
            //this is for demo only, you usullay want just one form in your application


            //documentation : http://docs.jquery.com/Plugins/Validation/validate


            $.mask.definitions['~']='[+-]';
            $('#phone').mask('(999) 999-9999');

            jQuery.validator.addMethod("phone", function (value, element) {
                return this.optional(element) || /^\(\d{3}\) \d{3}\-\d{4}( x\d{1,6})?$/.test(value);
            }, "Enter a valid phone number.");


            /*------------------------  step1 validate form  ------------------------------------*/
            var validator1 = $('#step1_validation-form').validate({
                errorElement: 'div',
                errorClass: 'help-block',
                focusInvalid: false,
                ignore: "",
                rules: {
                    first_name: {
                        required: true,
                    },
                    form_field_radio: {
                        required: true,
                    },
                    birth_date: {
                        max : '{{$min_age}}',
                        required: true,

                    },
                    nationalty: {
                        required: true,
                    },
                    code: {
                        required: true
                    },
                    acadmic_year: {
                        required: true,
                    },
                    registration_date: {
                        required: true,
                    },
                    registration_class: {
                        required: true
                    },
                    /* image_input: {
                         required: true,
                         extension: "jpg|png"
                     }*/
                },

                messages: {
                    //image: "Please choose .JPG /.PNG Image",
                    email: {
                        required: "Please provide a valid email.",
                        email: "Please provide a valid email."
                    },
                    password: {
                        required: "Please specify a password.",
                        minlength: "Please specify a secure password."
                    },
                    state: "Please choose state",
                    subscription: "Please choose at least one option",
                    gender: "Please choose gender",
                    agree: "Please accept our policy"
                },


                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
                    notification('glyphicon glyphicon-ok-sign','Warning',' This field is required. ','danger');
                },

                success: function (e) {
                    $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
                    $(e).remove();
                },

                errorPlacement: function (error, element) {

                    if(element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
                        var controls = element.closest('div[class*="col-"]');
                        if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
                        else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                    }
                    else if(element.is('.select2')) {
                        error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                    }
                    else if(element.is('.chosen-select')) {
                        error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                    }
                    else error.insertAfter(element.parent());
                },

                submitHandler: function (form) {
                },
                invalidHandler: function (form) {
                }
            });


            /*------------------------  step2 validate form  ------------------------------------*/
            var validator2 = $('#step2_validation-form').validate({
                errorElement: 'div',
                errorClass: 'help-block',
                focusInvalid: false,
                ignore: "",
                rules: {
                    parent_first_name: {
                        required: true,
                    },
                    parent_second_name: {
                        required: true,
                    },
                    parent_third_name: {
                        required: true,

                    },
                    parent_family_name: {
                        required: false,
                    },
                    parent_email: {
                        required: false,
                        email:true
                    },
                    form_field_phone: {
                        required: false,


                    },
                    /* parent_id_number: {
                         required: true,
                     },
                     parent_passport_id: {
                         required: true
                     },*/
                    parent_address: {
                        required: false
                    },
                    country_id: {
                        required: true
                    },
                },

                messages: {
                    email: {
                        required: "Please provide a valid email.",
                        email: "Please provide a valid email."
                    },
                    password: {
                        required: "Please specify a password.",
                        minlength: "Please specify a secure password."
                    },
                    state: "Please choose state",
                    subscription: "Please choose at least one option",
                    gender: "Please choose gender",
                    agree: "Please accept our policy"
                },


                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
                    notification('glyphicon glyphicon-ok-sign','Warning',' This field is required. ','danger');
                },

                success: function (e) {
                    $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
                    $(e).remove();
                },

                errorPlacement: function (error, element) {
                    if(element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
                        var controls = element.closest('div[class*="col-"]');
                        if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
                        else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                    }
                    else if(element.is('.select2')) {
                        error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                    }
                    else if(element.is('.chosen-select')) {
                        error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                    }
                    else error.insertAfter(element.parent());
                },

                submitHandler: function (form) {
                },
                invalidHandler: function (form) {
                }
            });

            /*--------------------------------  step3 validate form -----------------------------------------------*/
            $('#step3_validation-form').validate({
                errorElement: 'div',
                errorClass: 'help-block',
                focusInvalid: false,
                ignore: "",
                rules: {
                    first_name: {
                        required: false,
                    },
                    form_field_radio: {
                        required: false,
                    },
                    birth_date: {
                        required: false,

                    },
                    nationalty: {
                        required: false,
                    },
                    code: {
                        required: false
                    },
                    parent_email: {
                        required: false,
                        email:true
                    },
                    acadmic_year: {
                        required: false,
                    },
                    registration_date: {
                        required: false,
                    },
                    registration_class: {
                        required: false
                    }
                },

                messages: {
                    email: {
                        required: "Please provide a valid email.",
                        email: "Please provide a valid email."
                    },
                    password: {
                        required: "Please specify a password.",
                        minlength: "Please specify a secure password."
                    },
                    state: "Please choose state",
                    subscription: "Please choose at least one option",
                    gender: "Please choose gender",
                    agree: "Please accept our policy"
                },


                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
                },

                success: function (e) {
                    $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
                    $(e).remove();
                },

                errorPlacement: function (error, element) {
                    if(element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
                        var controls = element.closest('div[class*="col-"]');
                        if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
                        else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                    }
                    else if(element.is('.select2')) {
                        error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                    }
                    else if(element.is('.chosen-select')) {
                        error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                    }
                    else error.insertAfter(element.parent());
                },

                submitHandler: function (form) {
                },
                invalidHandler: function (form) {
                }
            });
            /*----------------------------------------------------------------------------------*/



            $('#modal-wizard-container').ace_wizard();
            $('#modal-wizard .wizard-actions .btn[data-dismiss=modal]').removeAttr('disabled');


            /**
             $('#date').datepicker({autoclose:true}).on('changeDate', function(ev) {
					$(this).closest('form').validate().element($(this));
				});

             $('#mychosen').chosen().on('change', function(ev) {
					$(this).closest('form').validate().element($(this));
				});
             */


            $(document).one('ajaxloadstart.page', function(e) {
                //in ajax mode, remove remaining elements before leaving page
                $('[class*=select2]').remove();
            });
        })
    </script>

    {{---------------------- leave switch ---------------------}}
    <script>

        $(document).on('change','#leave_value',function () {
            if ($('#leave_value').prop('checked') == true) {
                $(this).attr('get_v',1);
                $('#l_date').css('display', 'block');
                /*console.log($('#image_input').val());*/


                /*console.log($('input[name=form_field_radio]:checked').attr('gender'));*/

            }
            else{
                $('#l_date').css('display', 'none');
                $(this).attr('get_v',0);

            }
        });


    </script>

    {{--------------------------------- has sibling---------------------------------------------------}}
    <script>

        $(document).on('change','#sibling_value',function () {
            if ($('#sibling_value').prop('checked') == true) {
                //$(this).attr('sibling_v',1);
                $('#all_sibling').css('display', 'block');
                $('#gbox_grid-table').css('display', 'block')
                $('#all_sibling').children('div').addClass('open');
                get_all_student_data();
                console.log($(this).attr('sibling_v'));
                console.log($('#sibling_selected_v').val());

                /*---------------  relate to stuff   -------------------*/
                $('#relate_to_stuff').prop('checked',false);

                $('#all_school_stuff').css('display', 'none');

                clear_return_father_data();
                clear_return_mother_data();
                $("#siblings_table_row tbody").empty();
                /*------------------------------------------------*/
                $('.btn-next').prop('disabled',true);

            }
            else{

                console.log('remove_table');
                $('#siblings_table_row tbody').empty();
                $(this).attr('same_mother_v',0);
                $('input[name=same_mother]').prop('checked', false);

                $('#all_sibling').css('display', 'none');
                $('#gbox_grid-table').css('display', 'none');
                //$(this).attr('sibling_v',0);
                console.log($(this).attr('sibling_v'));
                console.log($('#sibling_selected_v').val());
                $('.btn-next').prop('disabled',false);
                clear_return_father_data();
                clear_return_mother_data();

            }
        });


    </script>




    {{--------------------------------- relate to stuff ---------------------------------------------------}}
    <script>

        $(document).on('change','#relate_to_stuff',function () {
            if ($('#relate_to_stuff').prop('checked') == true) {


                $('#siblings_table_row tbody').empty();
                /*-------------   has siblings part  ---------------------*/
                $('#sibling_value').prop('checked',false);
                $(this).attr('same_mother_v',0);
                $('input[name=same_mother]').prop('checked', false);
                $('#all_sibling').css('display', 'none');
                clear_return_father_data();
                clear_return_mother_data();
                /*------------------------------------*/
                $("#siblings_table_row tbody").empty();
                // staff_data_drop_down();
                $('#all_school_stuff').css('display', 'block');
                $('#gbox_grid-table').css('display', 'block')
                $('.btn-next').prop('disabled',true);

            }
            else{

                $('#related_stuff_id').select2('destroy');
                $('#related_stuff_id').val('');
                $('#related_stuff_id').select2();

                clear_return_father_data();
                clear_return_mother_data();

                $('#set_po_id').attr('staffs_id','');
                $('#set_po_id').attr('staffs_gender','');
                $('#set_po_id').attr('father_id','');
                $('#set_po_id').attr('mother_id','');

                $("#siblings_table_row tbody").empty();
                $('#all_school_stuff').css('display', 'none');
                $('#gbox_grid-table').css('display', 'none');
                $('.btn-next').prop('disabled',false);


            }
        });


    </script>


    {{------------------------------------  enable father edit ------------------------------------------------------------}}
    {{--  <script>

          $(document).on('change','#edit_father_data',function () {
              if ($('#edit_father_data').prop('checked') == true) {
                  $(this).attr('edit_father_data',1);
                  $(this).attr('one_time_edit_father',1);

                  console.log($(this).attr('edit_father_data'));
                  console.log($(this).attr('one_time_edit_father'));

                  $('#step2_validation-form').find('.dis_able_father_edit').removeAttr('disabled');




                  /*console.log($('input[name=form_field_radio]:checked').attr('gender'));*/

              }
              else{

                  $(this).attr('edit_father_data',0);
                  console.log($(this).attr('edit_father_data'));
                  console.log($(this).attr('one_time_edit_father'));

                  $('#step2_validation-form').find('.dis_able_father_edit').attr('disabled','disabled');

              }
          });


      </script>--}}

    {{------------------------------------  enable mother edit ------------------------------------------------------------}}
    {{-- <script>

         $(document).on('change','#edit_mother_data',function () {
             if ($('#edit_mother_data').prop('checked') == true) {
                 $(this).attr('edit_mother_data',1);
                 $(this).attr('one_time_edit_mother',1);

                 console.log($(this).attr('edit_mother_data'));
                 console.log($(this).attr('one_time_edit_mother'));

                 $('#step3_validation-form').find('.dis_able_mother_edit').removeAttr('disabled');




                 /*console.log($('input[name=form_field_radio]:checked').attr('gender'));*/

             }
             else{

                 $(this).attr('edit_mother_data',0);
                 console.log($(this).attr('edit_mother_data'));
                 console.log($(this).attr('one_time_edit_mother'));

                 $('#step3_validation-form').find('.dis_able_mother_edit').attr('disabled','disabled');

             }
         });


     </script>--}}


    {{---------------------------------  load sibling on select change ------------------------------------------------}}
    <script>

        $(document).on('change','#sibling_selected_v',function () {
            if($(this).val() != "nothing")
            {
                $('#sibling_value').attr('sibling_v',1);
                console.log('sibl :' +$('#sibling_value').attr('sibling_v'));
                return_father_data();
                fill_siblings_table();

            }
            else{

                $('#sibling_value').attr('sibling_v',0);
                console.log('sibl :' +$('#sibling_value').attr('sibling_v'));
                $('#set_po_id').attr('father_id','');
                $('#set_po_id').attr('mother_id','');
                $('input[name=same_mother]').prop('checked', false);
                clear_return_father_data();
                clear_return_mother_data();
                $('.btn-next').prop('disabled',true);
                $('#siblings_table_row tbody').empty();

            }


        });


    </script>




    {{---------------------------------  get staff parent and  sibling  ------------------------------------------------}}
    <script>

        function get_sibling_for_stuff()
        {


            $("#siblings_table_row tbody").empty();
            $('.btn-next').prop('disabled',true);
            var staff_selcted_id = $('#related_stuff_id').val();
            var curr_student = $('#set_po_id').attr('current_po_id');


            console.log('staff_selcted_id : ' + staff_selcted_id);
            if(staff_selcted_id != "nothing" )
            {
                $('#save_po').prop('disabled',true);
                console.log("in siblings fill data");
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/fill_siblings_staff_table/',
                    dataType : 'json',
                    type: 'get',
                    data: {
                        staff_selcted_id:staff_selcted_id,
                        curr_student:curr_student

                    },

                    success:function(response) {
                        console.log(response);
                        $("#siblings_table_row tbody").empty();
                        for(var i =0;i<response['siblings_data'].length;i++)
                        {
                            var no = i+1;

                            /*$dropdown.append($("<option />")/!*.data-tokens(response[i]['code']+" "+response[i]['First_name']+" "+ response[i]['fname']+" "+ response[i]['snam']+" "+ response[i]['family_name'])*!/.val(response[i]['code']).text(response[i]['First_name']+" "+ response[i]['fname']+" "+ response[i]['snam']+" "+ response[i]['family_name']));*/
                            /*$t_body.append('<option   value="'+response[i]['person_id']+'" >'+response[i]['First_name']+" "+ response[i]['fname']+" "+ response[i]['snam']+" "+ response[i]['family_name']+'</option>');*/

                            $("#siblings_table_row tbody").append('<tr  class="jqgrow ui-row-ltr ui-widget-content ui-priority-secondary">'+

                                '<td>'+(no)+'</td>'+
                                '<td>'+response['siblings_data'][i]['First_name']+" "+ response['siblings_data'][i]['1name']+" "+ response['siblings_data'][i]['snam']+" "+ response['siblings_data'][i]['family_name']+'</td>'+
                                '<td>'+response['siblings_data'][i]['Ismas_code']+'</td>'+
                                '<td>'+response['siblings_data'][i]['ClassName']+'</td>'+
                                '</tr>');
                        }


                        $('#staff_child_data').attr('child_count',response['count']);
                        $('#staff_child_data').attr('child_remain',response['remain_childrens']);
                        $('#staff_child_data').attr('max_child',response['employee_child_number']);

                        $('#save_po').prop('disabled',false);

                        if(response['remain_childrens'] > 0)
                        {
                            $('.btn-next').prop('disabled',false);
                        }
                        else
                        {
                            $('.btn-next').prop('disabled',true);
                            notification('glyphicon glyphicon-ok-sign','Warning',' cant add another children for this employee ','danger');

                        }

                    }

                });
            }
            else
            {
                $("#siblings_table_row tbody").empty();

            }
        }

        function return_staff_data()
        {


            var employee_selcted_id = $('#related_stuff_id').val();

            console.log("in return  employee data");
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/return_staff_with_partner_data/',
                dataType : 'json',
                type: 'get',
                data: {
                    employee_selcted_id:employee_selcted_id,

                },

                success:function(response) {

                    console.log('pass data ');
                    console.log(response);
                    $('.staff_disable_only').prop('disabled',true);

                    if(response['employee_data'][0]['gender']  == 'male')
                    {
                        /*------------------------------- employee father data   ---------------------------------*/
                        $('#parent_first_name').val(response['employee_data'][0]['employee_name']);
                        $('#parent_second_name').val(response['employee_data'][0]['middle_name']);
                        $('#parent_third_name').val(response['employee_data'][0]['last_name']);
                        $('#parent_family_name').val(response['employee_data'][0]['family_name']);
                        $('#parent_email').val(response['employee_data'][0]['EMail']);
                        $('#form_field_phone').val(response['employee_data'][0]['mobile_work']);

                        //$('#parent_id_number').val(response['employee_data'][0]['ID_NO']);
                        //$('#parent_passport_id').val(response['employee_data'][0]['Pass_ID']);

                        $('#parent_address').val(response['employee_data'][0]['home_address']);

                        $("#country_id").select2('destroy');
                        $('#country_id').val(response['employee_data'][0]['country_id']);
                        $("#country_id").select2();

                        $('#finance').prop('checked',true);
                        $('#finance').attr('finance_v',1);


                        $('#set_po_id').attr('father_id',response['parent_for_employee']);

                        //$('#parent_notes').val(response['employee_data'][0]['other_Notes']);

                        /*--------------------------  partner mother data  -----------------------------------*/

                        $('#mother_first_name').val(response['employee_data'][0]['partner_first_name']);
                        $('#mother_second_name').val(response['employee_data'][0]['partner_middle_name']);
                        $('#mother_third_name').val(response['employee_data'][0]['partner_last_name']);
                        $('#mother_family_name').val(response['employee_data'][0]['partner_family_name']);

                        //$('#mother_email').val(response['employee_data'][0]['Email']);
                        //$('#mother_phone').val(response['employee_data'][0]['Contact_No']);
                        //$('#mother_id_number').val(response['employee_data'][0]['ID_NO']);
                        //$('#mother_passport_id').val(response['employee_data'][0]['Pass_ID']);
                        $('#mother_address').val(response['employee_data'][0]['home_address']);

                        $("#mother_country_id").select2('destroy');
                        $('#mother_country_id').val(response['employee_data'][0]['country_id']);
                        $("#mother_country_id").select2();

                        $('#mother_finance').prop('checked',false);
                        $('#mother_finance').attr('finance_v',0);

                        //$('#mother_notes').val(response['employee_data'][0]['other_Notes']);


                        $('#set_po_id').attr('mother_id',response['partner_parent_id']);


                        /*--------------------------  set employee id  --------------------------------*/
                        $('#set_po_id').attr('staffs_id',response['employee_data'][0]['employee_id']);
                        $('#set_po_id').attr('staffs_gender',response['employee_data'][0]['gender']);
                    }

                    else if (response['employee_data'][0]['gender']  == 'female')
                    {

                        /*------------------------------- employee mother data   ---------------------------------*/
                        $('#mother_first_name').val(response['employee_data'][0]['employee_name']);
                        $('#mother_second_name').val(response['employee_data'][0]['middle_name']);
                        $('#mother_third_name').val(response['employee_data'][0]['last_name']);
                        $('#mother_family_name').val(response['employee_data'][0]['family_name']);
                        $('#mother_email').val(response['employee_data'][0]['EMail']);
                        $('#mother_phone').val(response['employee_data'][0]['mobile_work']);

                        //$('#mother_id_number').val(response['employee_data'][0]['ID_NO']);
                        //$('#mother_passport_id').val(response['employee_data'][0]['Pass_ID']);

                        $('#mother_address').val(response['employee_data'][0]['home_address']);

                        $("#mother_country_id").select2('destroy');
                        $('#mother_country_id').val(response['employee_data'][0]['country_id']);
                        $("#mother_country_id").select2();

                        $('#mother_finance').prop('checked',true);
                        $('#mother_finance').attr('finance_v',1);

                        $('#set_po_id').attr('mother_id',response['parent_for_employee']);

                        //$('#mother_notes').val(response['employee_data'][0]['other_Notes']);

                        /*--------------------------  partner father data  -----------------------------------*/

                        $('#parent_first_name').val(response['employee_data'][0]['partner_first_name']);
                        $('#parent_second_name').val(response['employee_data'][0]['partner_middle_name']);
                        $('#parent_third_name').val(response['employee_data'][0]['partner_last_name']);
                        $('#parent_family_name').val(response['employee_data'][0]['partner_family_name']);

                        //$('#parent_email').val(response['employee_data'][0]['Email']);
                        //$('#form_field_phone').val(response['employee_data'][0]['Contact_No']);
                        //$('#parent_id_number').val(response['employee_data'][0]['ID_NO']);
                        //$('#parent_passport_id').val(response['employee_data'][0]['Pass_ID']);
                        $('#parent_address').val(response['employee_data'][0]['home_address']);

                        $("#country_id").select2('destroy');
                        $('#country_id').val(response['employee_data'][0]['country_id']);
                        $("#country_id").select2();

                        $('#finance').prop('checked',true);
                        $('#finance').attr('finance_v',1);
                        //$('#mother_notes').val(response['employee_data'][0]['other_Notes']);


                        $('#set_po_id').attr('father_id',response['partner_parent_id']);


                        /*--------------------------  set employee id  --------------------------------*/
                        $('#set_po_id').attr('staffs_id',response['employee_data'][0]['employee_id']);

                        $('#set_po_id').attr('staffs_gender',response['employee_data'][0]['gender']);


                    }

                    //$('.btn-next').prop('disabled',false);

                }

            })/*.done(function(){

                });*/

        }
    </script>

    <script>

        $(document).on('change','#related_stuff_id',function () {
            if($(this).val() != "nothing")
            {

                return_staff_data();
                //fill_siblings_table();
                get_sibling_for_stuff();


            }
            else{
                $('#set_po_id').attr('staffs_id','');
                $('#set_po_id').attr('father_id','');
                $('#set_po_id').attr('mother_id','');
                clear_return_father_data();
                clear_return_mother_data();

                $('.btn-next').prop('disabled',true);
                $('#siblings_table_row tbody').empty();
            }


        });


    </script>


    {{------------------------------------ fill siblings table ------------------------------------------}}
    <script>
        function fill_siblings_table()
        {
            $('.btn-next').prop('disabled',true);
            var student_selcted_id = $('#sibling_selected_v').val();

            var has_siblings = $('#sibling_value').attr('sibling_v');

            //var $t_body = $("#siblings_table_row");

            console.log('student_selcted_id : ' + student_selcted_id);
            if(student_selcted_id != "nothing" && has_siblings==1)
            {
                console.log("in siblings fill data");
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/fill_siblings_table/',
                    dataType : 'json',
                    type: 'get',
                    data: {
                        student_selcted_id:student_selcted_id,

                    },

                    success:function(response) {
                        console.log(response);
                        $("#siblings_table_row tbody").empty();
                        for(var i =0;i<response.length;i++)
                        {
                            var no = i+1;

                            /*$dropdown.append($("<option />")/!*.data-tokens(response[i]['code']+" "+response[i]['First_name']+" "+ response[i]['fname']+" "+ response[i]['snam']+" "+ response[i]['family_name'])*!/.val(response[i]['code']).text(response[i]['First_name']+" "+ response[i]['fname']+" "+ response[i]['snam']+" "+ response[i]['family_name']));*/
                            /*$t_body.append('<option   value="'+response[i]['person_id']+'" >'+response[i]['First_name']+" "+ response[i]['fname']+" "+ response[i]['snam']+" "+ response[i]['family_name']+'</option>');*/

                            $("#siblings_table_row tbody").append('<tr  class="jqgrow ui-row-ltr ui-widget-content ui-priority-secondary">'+

                                '<td>'+(no)+'</td>'+
                                '<td>'+response[i]['First_name']+" "+ response[i]['1name']+" "+ response[i]['snam']+" "+ response[i]['family_name']+'</td>'+
                                '<td>'+response[i]['Ismas_code']+'</td>'+
                                '<td>'+response[i]['ClassName']+'</td>'+
                                '</tr>');
                        }


                        $('.btn-next').prop('disabled',false);
                    }

                });
            }
            else
            {
                $('#sibling_delete_op').attr('delete_option_all',1);
                $("#siblings_table_row tbody").empty();
                console.log( 'from' +$('#sibling_selected_v').val());
                console.log( 'has' +$('#sibling_value').attr('sibling_v'));
            }


        }


    </script>

    {{------------------------------------ first fill siblings table ------------------------------------------}}
    <script>
        function first_fill_siblings_table() {

            var student_selcted_id = $('#sibling_selected_v').attr('defult_selected_value');


            console.log(student_selcted_id);
            console.log("in siblings fill data");



            console.log("in siblings fill data");
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/fill_siblings_table_edit/',
                dataType: 'json',
                type: 'get',
                data: {
                    student_selcted_id: student_selcted_id,

                },

                success: function (response) {
                    console.log(response);
                    /*$(document).find('#siblings_table_row tr').remove();*/
                    $('#gbox_grid-table').show();
                    $("#siblings_table_row tbody").empty();

                    for (var i = 0; i < response.length; i++) {
                        var no = i + 1;

                        /*$dropdown.append($("<option />")/!*.data-tokens(response[i]['code']+" "+response[i]['First_name']+" "+ response[i]['fname']+" "+ response[i]['snam']+" "+ response[i]['family_name'])*!/.val(response[i]['code']).text(response[i]['First_name']+" "+ response[i]['fname']+" "+ response[i]['snam']+" "+ response[i]['family_name']));*/
                        /*$t_body.append('<option   value="'+response[i]['person_id']+'" >'+response[i]['First_name']+" "+ response[i]['fname']+" "+ response[i]['snam']+" "+ response[i]['family_name']+'</option>');*/
                        var row = '<tr  class="jqgrow ui-row-ltr ui-widget-content ui-priority-secondary">' +

                            '<td>' + (no) + '</td>' +
                            '<td>' + response[i]['First_name'] + " " + response[i]['1name'] + " " + response[i]['2name'] + " " + response[i]['family_name'] + '</td>' +
                            '<td>' + response[i]['Ismas_code'] + '</td>' +
                            '<td>' + response[i]['ClassName'] + '</td>' +
                            '</tr>';


                        $("#siblings_table_row tbody").append(row);
                    }


                }

            });

        }



    </script>


    {{-------------------------- same mother check box  -------------------------------------}}
    <script>

        $(document).on('change','#same_mother',function () {
            if($('#sibling_selected_v').val() != 'nothing'){
                if ($('input[name=same_mother]:checked').length>0) {
                    $(this).attr('same_mother_v',1);
                    console.log($(this).attr('same_mother_v'));


                    return_mother_data();

                    /*console.log($('input[name=form_field_radio]:checked').attr('gender'));*/

                }
                else{

                    $(this).attr('same_mother_v',0);
                    console.log($(this).attr('same_mother_v'));
                    console.log($('#sibling_selected_v').val());
                    clear_return_mother_data();


                }
            }

        });


    </script>

    {{------------------------------------ father finance switch ------------------------------------------------}}
    <script>

        $(document).on('change','#finance',function () {
            if ($('#finance').prop('checked') == true) {
                $(this).attr('finance_v',1);
                /*  console.log($(this).attr('finance_v'));*/

                /*console.log($('input[name=form_field_radio]:checked').attr('gender'));*/

            }
            else{

                $(this).attr('finance_v',0);
                console.log($(this).attr('finance_v'));
            }
        });


    </script>

    {{-------------------------------------  mother finance switch -------------------------------------------------------}}
    <script>

        $(document).on('change','#mother_finance',function () {
            if ($('#mother_finance').prop('checked') == true) {
                $(this).attr('finance_v',1);
                /*  console.log($(this).attr('finance_v'));*/

                /*console.log($('input[name=form_field_radio]:checked').attr('gender'));*/

            }
            else{

                $(this).attr('finance_v',0);
                console.log($(this).attr('finance_v'));
            }
        });


    </script>


    {{------------------------------------------ get all student data for select siblings-----------------------------------------------------------------------}}
    <script>



        function get_all_student_data()
        {

            var $dropdown = $("#sibling_selected_v");


            $dropdown.find('option').remove();
            $dropdown.append('<option class="btn btn-xs btn-danger " hidden  value="nothing" data-tokens=" "></option>');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/get_all_students_data/',
                dataType : 'json',
                type: 'get',
                data: {

                },

                success:function(response) {

                    console.log(response);
                    /*$dropdown.find('option').remove();*/
                    /*<option data-tokens="1 expenses" value="1">expenses</option>*/
                    for(var i =0;i<response.length;i++)
                    {
                        /*$dropdown.append($("<option />")/!*.data-tokens(response[i]['code']+" "+response[i]['First_name']+" "+ response[i]['fname']+" "+ response[i]['snam']+" "+ response[i]['family_name'])*!/.val(response[i]['code']).text(response[i]['First_name']+" "+ response[i]['fname']+" "+ response[i]['snam']+" "+ response[i]['family_name']));*/
                        $dropdown.append('<option   value="'+response[i]['person_id']+'" data-tokens="'+response[i]['Ismas_code']+" "+response[i]['First_name']+" "+ response[i]['1name']+" "+ response[i]['snam']+" "+ response[i]['family_name'] +'">'+response[i]['First_name']+" "+ response[i]['1name']+" "+ response[i]['snam']+" "+ response[i]['family_name']+'</option>');
                    }
                    //$dropdown.selectpicker('refresh');
                    $dropdown.select2();
                }

            });

        };

        /*$(document).on('focus','#nationalty',function () {
            update_nationality();
        });*/

    </script>

    {{-----------------------------------------   nationality  ---------------------------------------------------------}}
    <script>



        function update_nationality()
        {
            console.log('nationalty updated ');

            var $dropdown = $("#nationalty");
            $dropdown.prop('disabled',true);
            $dropdown.select2('destroy');
            var selected_value = $dropdown.val();



            /* $dropdown.find('option').remove();*/

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/fill_nationality_option/',
                dataType : 'json',
                type: 'get',
                data: {

                },

                success:function(response) {

                    /*console.log(response);*/
                    $dropdown.find('option').remove();
                    $dropdown.append('<option value=""></option>');
                    for(var i =0;i<response.length;i++)
                    {
                        $dropdown.append($("<option />").val(response[i]['nationalty_type_id']).text(response[i]['nationalty_type']));
                    }
                    $dropdown.val(selected_value);
                    $dropdown.select2();
                    $dropdown.prop('disabled',false);
                }

            });

        }

        $(document).on('dblclick','.select2-selection',function () {
            var select_check = $(this).parents('.select2').siblings('select');


            if ($('#save_po').length && select_check.attr('id') == 'nationalty') {
                update_nationality();
            }
        });

    </script>

    {{-------------------------------------------    acadimic -------------------------------------------------------}}
    <script>



        function acadimic_year()
        {

            var $dropdown = $("#acadmic_year");
            $dropdown.prop('disabled',true);
            $dropdown.select2('destroy');
            var selected_value = $dropdown.val();
            /*$dropdown.find('option').remove();*/

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/fill_acadimic_option/',
                dataType : 'json',
                type: 'get',
                data: {

                },

                success:function(response) {

                    $dropdown.find('option').remove();
                    $dropdown.append('<option value=""></option>');

                    for(var i =0;i<response.length;i++)
                    {
                        $dropdown.append($("<option />").val(response[i]['acadimic_id']).text(response[i]['year_name']));
                    }
                    $dropdown.val(selected_value);
                    $dropdown.select2();
                    $dropdown.prop('disabled',false);
                }

            });

        };

        $(document).on('dblclick','.select2-selection',function () {
            var select_check = $(this).parents('.select2').siblings('select');


            if ($('#save_po').length && select_check.attr('id') == 'acadmic_year') {
                acadimic_year();
            }
        });


    </script>

    {{---------------------------------------------   rigistration -----------------------------------------------------}}
    <script>



        function rigistration_class()
        {


            var $dropdown = $("#registration_class");
            $dropdown.prop('disabled',true);
            $dropdown.select2('destroy');
            var selected_value = $dropdown.val();
            /* $dropdown.find('option').remove();*/

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/fill_registration_option/',
                dataType : 'json',
                type: 'get',
                data: {

                },

                success:function(response) {

                    /*console.log(response);*/
                    $dropdown.find('option').remove();
                    $dropdown.append('<option value=""></option>');

                    for(var i =0;i<response.length;i++)
                    {
                        $dropdown.append($("<option />").val(response[i]['classid']).text(response[i]['ClassName']));
                    }
                    $dropdown.val(selected_value);
                    $dropdown.select2();
                    $dropdown.prop('disabled',false);
                }

            });

        };

        $(document).on('dblclick','.select2-selection',function () {
            var select_check = $(this).parents('.select2').siblings('select');


            if ($('#save_po').length && select_check.attr('id') == 'registration_class') {
                rigistration_class();
            }
        });


    </script>
    {{------------------------------------------------  countries --------------------------------------------------}}
    <script>



        function countries_get(id)
        {

            var $dropdown = $("#"+id);
            $dropdown.prop('disabled',true);
            $dropdown.select2('destroy');
            var selected_value = $dropdown.val();

            /*$dropdown.find('option').remove();*/

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/fill_countries_option/',
                dataType : 'json',
                type: 'get',
                data: {

                },

                success:function(response) {

                    /*console.log(response);*/
                    $dropdown.find('option').remove();
                    $dropdown.append('<option value=""></option>');

                    for(var i =0;i<response.length;i++)
                    {
                        $dropdown.append($("<option />").val(response[i]['Country_ID']).text(response[i]['Country_Name']));
                    }
                    $dropdown.val(selected_value);
                    $dropdown.select2();
                    $dropdown.prop('disabled',false);
                }

            });

        };
        $(document).on('dblclick','.select2-selection',function () {
            var select_check = $(this).parents('.select2').siblings('select');


            if ($('#save_po').length && select_check.attr('id') == 'country_id') {
                countries_get('country_id');
            }
            if ($('#save_po').length && select_check.attr('id') == 'mother_country_id') {
                countries_get('mother_country_id');
            }
        });

    </script>
    {{------------------------------------------ get alll related staff -----------------------------------------------------------------------}}
    <script>



        // function staff_data_drop_down()
        // {
        //
        //     var $dropdown = $("#related_stuff_id");
        //
        //
        //     $dropdown.find('option').remove();
        //     $dropdown.append('<option class="btn btn-xs btn-danger " hidden  value="nothing" data-tokens=" "></option>');
        //     $.ajax({
        //         headers: {
        //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //         },
        //         url: '/all_stuff_employee/',
        //         dataType : 'json',
        //         type: 'get',
        //         data: {
        //
        //         },
        //
        //         success:function(response) {
        //
        //             console.log(response);
        //             /*$dropdown.find('option').remove();*/
        //             /*<option data-tokens="1 expenses" value="1">expenses</option>*/
        //             for(var i =0;i<response.length;i++)
        //             {
        //                 /*$dropdown.append($("<option />")/!*.data-tokens(response[i]['code']+" "+response[i]['First_name']+" "+ response[i]['fname']+" "+ response[i]['snam']+" "+ response[i]['family_name'])*!/.val(response[i]['code']).text(response[i]['First_name']+" "+ response[i]['fname']+" "+ response[i]['snam']+" "+ response[i]['family_name']));*/
        //                 $dropdown.append('<option   value="'+response[i]['employee_id']+'" >'+response[i]['employee_name']+" "+ response[i]['middle_name']+" "+ response[i]['last_name']+'</option>');
        //             }
        //             //$dropdown.selectpicker('refresh');
        //             $dropdown.select2();
        //         }
        //
        //     });
        //
        // };

        /*$(document).on('focus','#nationalty',function () {
            update_nationality();
        });*/

    </script>



    {{----------------------------------------  summit all form --------------------------------------------------------------}}
    <script>



        function submit_all()
        {
            /*------------------------------ staff  employee id   ----------------------------------------*/

            var staff_employee_id = $('#set_po_id').attr('staffs_id');
            var staff_employee_gender = $('#set_po_id').attr('staffs_gender');

            /*------------------------------------- student data----------------------------------------------------------------------*/
            var edit_student_id = $('#set_po_id').attr('current_po_id');
            var remove_all_delete = $('#sibling_delete_op').attr('delete_option_all');

            var dt = new Date();
            var time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();

            var first_name = $('#first_name').val();

            var img = $('#image_input').val();


            var birth_day = $('#birth_date').val();
            var gender = $('input[name=form_field_radio]:checked').attr('gender');
            var nationality = $('#nationalty').val();
            var code = $('#code').val();
            var acadmic_year = $('#acadmic_year').val();

            var registration_date = $('#registration_date').val().split(' ')[0]+ " " + time;
            var registration_class = $('#registration_class').val();

            var leave_v = $('#leave_value').attr('get_v');
            var leave_date = $('#leave_date').val();

            var has_siblings = $('#sibling_value').attr('sibling_v');

            var sibling_student_id = $('#sibling_selected_v').val();

            var has_same_mother = $('#same_mother').attr('same_mother_v');

            //var enable_mother_edit = $('#edit_mother_data').attr('one_time_edit_mother');

            //var enable_father_edit = $('#edit_father_data').attr('one_time_edit_father');


            /*console.log(has_siblings,sibling_student_id);*/
            /*-----------------------------------------------------------------------------------------------------------------------*/
            /*--------------------------------- father data ----------------------------------------------------------------*/
            var first_parent_name = $('#parent_first_name').val();
            var second_parent_name = $('#parent_second_name').val();
            var third_parent_name = $('#parent_third_name').val();
            var family_parent_name = $('#parent_family_name').val();
            var parent_email = $('#parent_email').val();
            var parent_phone = $('#form_field_phone').val();
            var parent_id_number = $('#parent_id_number').val();
            var parent_passport_number = $('#parent_passport_id').val();
            var parent_address = $('#parent_address').val();
            var parent_country = $('#country_id').val();

            var main_type = 1;
            var parent_finance = $('#finance').attr('finance_v');

            var parent_note = $('#parent_notes').val();

            //var edit_father_id = $('#parent_first_name').attr('edit_father_id');
            var edit_father_id = $('#set_po_id').attr('father_id');
            /*------------------------------------------------------------------------------------------------------------------------------------------------*/
            /*--------------------------------- mother data ----------------------------------------------------------------*/
            var mother_first_parent_name = $('#mother_first_name').val();
            var mother_second_parent_name = $('#mother_second_name').val();
            var mother_third_parent_name = $('#mother_third_name').val();
            var mother_family_parent_name = $('#mother_family_name').val();
            var mother_email = $('#mother_email').val();
            var mother_phone = $('#mother_phone').val();
            var mother_id_number = $('#mother_id_number').val();
            var mother_passport_number = $('#mother_passport_id').val();
            var mother_address = $('#mother_address').val();
            var mother_country = $('#mother_country_id').val();

            var mother_main_type = 0;
            var mother_finance = $('#mother_finance').attr('finance_v');

            var mother_note = $('#mother_notes').val();

            //var edit_mother_id = $('#mother_first_name').attr('edit_mother_id');
            var edit_mother_id = $('#set_po_id').attr('mother_id');
            /*------------------------------------------------------------------------------------------------------------------------------------------------*/

            /* console.log(class_name);
             console.log(cost_code);*/

            var form = document.getElementById("step1_validation-form");
            var formData = new FormData(form);
            formData.append( '_token', '{{csrf_token()}}' );

            /*------- mother data -------*/
            formData.append('mother_first_parent_name',mother_first_parent_name);
            formData.append('mother_second_parent_name',mother_second_parent_name);
            formData.append('mother_third_parent_name',mother_third_parent_name);

            formData.append('mother_family_parent_name',mother_family_parent_name);
            formData.append('mother_email',mother_email);
            formData.append('mother_phone',mother_phone);
            formData.append('mother_id_number',mother_id_number);
            formData.append('mother_passport_number',mother_passport_number);

            formData.append('mother_address',mother_address);
            formData.append('mother_country',mother_country);
            /*formData.append('mother_country',11);*/
            formData.append('mother_finance',mother_finance);
            formData.append('mother_note',mother_note);
            formData.append('mother_main_type',mother_main_type);
            formData.append('edit_mother_id',edit_mother_id);


            /*------- father data -------*/
            formData.append('first_parent_name',first_parent_name);
            formData.append('second_parent_name',second_parent_name);
            formData.append('third_parent_name',third_parent_name);
            formData.append('family_parent_name',family_parent_name);
            formData.append('parent_email',parent_email);
            formData.append('parent_phone',parent_phone);
            formData.append('parent_id_number',parent_id_number);
            formData.append('parent_passport_number',parent_passport_number);
            formData.append('parent_address',parent_address);
            formData.append('parent_country',parent_country);
            //formData.append('parent_country',11);
            formData.append('parent_finance',parent_finance);
            formData.append('parent_note',parent_note);
            formData.append('main_type',main_type);

            formData.append('edit_father_id',edit_father_id);


            /*------- student data -------*/
            formData.append('first_name',first_name);
            formData.append('img',img);
            formData.append('birth_day',birth_day);
            formData.append('gender',gender);
            formData.append('nationality',nationality);
            formData.append('code',code);
            formData.append('acadmic_year',acadmic_year);
            formData.append('registration_date',registration_date);
            formData.append('registration_class',registration_class);
            formData.append('leave_v',leave_v);

            console.log('leave_v : ' + leave_v);

            formData.append('leave_date',leave_date);
            formData.append('has_siblings',has_siblings);
            formData.append('sibling_student_id',sibling_student_id);
            formData.append('has_same_mother',has_same_mother);
            //formData.append('enable_mother_edit',enable_mother_edit);
            //formData.append('enable_father_edit',enable_father_edit);

            formData.append('edit_student_id',edit_student_id);
            formData.append('remove_all_delete',remove_all_delete);

            /*------------------------------ staff  employee id   ----------------------------------------*/
            formData.append('staff_employee_id',staff_employee_id);
            formData.append('staff_employee_gender',staff_employee_gender);



            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/submit_all_form/',
                dataType : 'json',
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,

                success:function(response) {

                    console.log(response);
                    setTimeout($.unblockUI);
                    if (response['error']) {
                        $('.err_input').removeClass('red-border');
                        $('.err_input').parent().find('.select2').removeClass('red-border');

                        /*$('.errorMessage1').show();*/
                        $.each(response['error'], function (key, val) {
                            //alert(key + val);

                            console.log(key + '_error');
                            $("." + key + '_error').addClass('red-border');
                            $('.' + key + '_error').parent().find('.select2').addClass('red-border');

                            //notification(val,'danger');
                            notification('glyphicon glyphicon-ok-sign','Warning',val,'danger');
                            //console.log(val[0]);

                        });

                    }
                    else{
                        $('.errorMessage1').hide();
                        $('.err_input').removeClass('red-border');
                        $('.err_input').parent().find('.select2').removeClass('red-border');
                        $('#set_po_id').attr('current_po_id', response['student_id']);

                        $('#mother_first_name').attr('edit_mother_id',response['mother_id']);
                        $('#parent_first_name').attr('edit_father_id',response['father_id']);

                        $('#set_po_id').attr('mother_id',response['mother_id']);
                        $('#set_po_id').attr('father_id',response['father_id']);
                        $('#set_po_id').attr('staffs_id',response['staff_id']);
                        $('#set_po_id').attr('staffs_gender',response['staff_gender']);

                        $('#save_po').text("Edit");
                        $('#save_po').attr('id', 'edit_po');
                        $('#add_row').prop('disabled', true);

                        $('.set_all_disabled').prop('disabled', true);
                        $('#creat_dep').removeClass('hide');

                        $('#more').removeClass('hide');


                        $('#edit_po').removeAttr('disabled');
                        //po-active
                        console.log(response);
                        notification('glyphicon glyphicon-ok-sign','Congratulations!','Student Saved Successfully!','success');




                    }

                },
                error: function (response) {
                    alert(' Cant Save This Documents !');
                    $('#save_po').prop('disabled', false);
                }

            });





        }

    </script>

    {{--------------------------------    save button      ---------------------------------------------------------}}
    <script>
        $(document).on('click','#save_po',function(){

            var staff_selected = $('#set_po_id').attr('staffs_id');
            //var can_save_another_child ;

            if(staff_selected != '' && staff_selected != null)
            {
                if($('#staff_child_data').attr('child_remain') == 0 && $('#staff_child_data').attr('child_remain') != '')
                {
                    notification('glyphicon glyphicon-ok-sign','Warning',' cant add another children for this employee ','danger');
                }
                else{
                    submit_all();
                    $.blockUI({ message: $('#floatingCirclesG'),
                        css: { backgroundColor: 'transparent',
                            border:'none'
                        } });
                }

            }
            else{
                submit_all();
                $.blockUI({ message: null });

            }


        });

    </script>
    {{-------------------------------------------- return father data  -----------------------------------------------------}}

    <script>

        function return_father_data()
        {

            var student_selcted_id = $('#sibling_selected_v').val();

            var has_siblings = $('#sibling_value').attr('sibling_v');


            if(student_selcted_id != "nothing" && has_siblings==1)
            {
                console.log("in return data");
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/return_father_data/',
                    dataType : 'json',
                    type: 'get',
                    data: {
                        student_selcted_id:student_selcted_id,

                    },

                    success:function(response) {
                        console.log(response);
                        $('#same_mother').prop('checked',false);
                        clear_return_mother_data();


                        //$('#enable_father_edit').css('display','block');

                        $('#parent_first_name').val(response['father_data'][0]['1name']);
                        $('#parent_second_name').val(response['father_data'][0]['2name']);
                        $('#parent_third_name').val(response['father_data'][0]['3name']);
                        $('#parent_family_name').val(response['father_data'][0]['family_name']);
                        $('#parent_email').val(response['father_data'][0]['EMail']);
                        $('#form_field_phone').val(response['father_data'][0]['Contact_NO']);
                        $('#parent_id_number').val(response['father_data'][0]['ID_NO']);
                        $('#parent_passport_id').val(response['father_data'][0]['Pass_ID']);
                        $('#parent_address').val(response['father_data'][0]['Address']);

                        $("#country_id").select2('destroy');
                        $('#country_id').val(response['father_data'][0]['Country_ID']);
                        $("#country_id").select2();


                        /*$('#finance').val(response['father_data'][0]['Finance']);*/
                        if(response['father_data'][0]['Finance'] == 1)
                        {
                            $('#finance').prop('checked',true);
                        }
                        else{
                            $('#finance').prop('checked',false);
                        }

                        $('#set_po_id').attr('father_id',response['father_data'][0]['par_id']);

                        $('#parent_notes').val(response['father_data'][0]['other_Notes']);


                        /*$('#step2_validation-form').find('.dis_able_father_edit').attr('disabled','disabled');*/



                    }

                });
            }
            else{clear_return_father_data();}

        };

    </script>

    {{------------------------------------  clear father data ------------------------------------------------------------}}
    <script>

        function clear_return_father_data()
        {

            //$('#enable_father_edit').css('display','none');



            $('#sibling_value').attr('sibling_v',0);

            $('#set_po_id').attr('father_id','');

            $('#parent_first_name').val("");
            $('#parent_second_name').val("");
            $('#parent_third_name').val("");
            $('#parent_family_name').val("");
            $('#parent_email').val("");
            $('#form_field_phone').val("");
            $('#parent_id_number').val("");
            $('#parent_passport_id').val("");
            $('#parent_address').val("");

            $("#country_id").select2('destroy');
            $('#country_id').val("");
            $('#country_id').prop("disabled",false);
            $("#country_id").select2();

            /*$('#finance').val(response['father_data'][0]['Finance']);*/


            //$('#finance').prop('checked',false);


            $('#parent_notes').val("");


            $('#step2_validation-form').find('.dis_able_father_edit').removeAttr('disabled');





        }

    </script>


    {{---------------------------------------------  return mother data -----------------------------------------------------------------------}}
    <script>

        function return_mother_data()
        {

            var student_selcted_id = $('#sibling_selected_v').val();


            var has_siblings = $('#sibling_value').attr('sibling_v');


            if(student_selcted_id != "nothing" && has_siblings==1)
            {


                console.log("in return data");

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/return_mother_data/',
                    dataType : 'json',
                    type: 'get',
                    data: {
                        student_selcted_id:student_selcted_id,

                    },

                    success:function(response) {
                        console.log(response);


                        //$('#enable_mother_edit').css('display','block');

                        $('#mother_first_name').val(response['mother_data'][0]['1name']);
                        $('#mother_second_name').val(response['mother_data'][0]['2name']);
                        $('#mother_third_name').val(response['mother_data'][0]['3name']);
                        $('#mother_family_name').val(response['mother_data'][0]['family_name']);
                        $('#mother_email').val(response['mother_data'][0]['EMail']);
                        $('#mother_phone').val(response['mother_data'][0]['Contact_NO']);
                        $('#mother_id_number').val(response['mother_data'][0]['ID_NO']);
                        $('#mother_passport_id').val(response['mother_data'][0]['Pass_ID']);
                        $('#mother_address').val(response['mother_data'][0]['Address']);

                        $("#mother_country_id").select2('destroy');
                        $('#mother_country_id').val(response['mother_data'][0]['Country_ID']);
                        $("#mother_country_id").select2();


                        /*$('#finance').val(response['father_data'][0]['Finance']);*/
                        if(response['mother_data'][0]['Finance'] == 1)
                        {
                            $('#mother_finance').prop('checked',true);
                        }
                        else{
                            $('#mother_finance').prop('checked',false);
                        }

                        $('#mother_notes').val(response['mother_data'][0]['other_Notes']);


                        $('#set_po_id').attr('mother_id',response['mother_data'][0]['par_id']);

                        /*$('#step3_validation-form').find('.dis_able_mother_edit').attr('disabled','disabled');*/

                    }

                });
            }
            else{clear_return_mother_data();}

        };

    </script>


    {{----------------------------------------------- clear mother data -----------------------------------------------}}
    <script>

        function clear_return_mother_data()
        {

            //$('#enable_mother_edit').css('display','none');
            $('#same_mother').attr('same_mother_v',0);

            $('#set_po_id').attr('mother_id','');

            $('#mother_first_name').val("");
            $('#mother_second_name').val("");
            $('#mother_third_name').val("");
            $('#mother_family_name').val("");
            $('#mother_email').val("");
            $('#mother_phone').val("");
            $('#mother_id_number').val("");
            $('#mother_passport_id').val("");
            $('#mother_address').val("");
            $('#mother_country_id').val("");

            $("#mother_country_id").select2('destroy');
            $('#mother_country_id').val("");
            $('#mother_country_id').prop("disabled",false);
            $("#mother_country_id").select2();


            $('#mother_finance').prop('checked',false);

            $('#mother_notes').val("");



            $('#step3_validation-form').find('.dis_able_mother_edit').removeAttr('disabled');



        }

    </script>

    {{--------------------------------------------------------}}
    <script>
        jQuery(function($) {

            $( "#birth_date" ).datepicker({
                dateFormat: 'yy-mm-dd',
                showOtherMonths: true,
                selectOtherMonths: false,
                //isRTL:true,


                /*
                changeMonth: true,
                changeYear: true,

                showButtonPanel: true,
                beforeShow: function() {
                    //change button colors
                    var datepicker = $(this).datepicker( "widget" );
                    setTimeout(function(){
                        var buttons = datepicker.find('.ui-datepicker-buttonpane')
                        .find('button');
                        buttons.eq(0).addClass('btn btn-xs');
                        buttons.eq(1).addClass('btn btn-xs btn-success');
                        buttons.wrapInner('<span class="bigger-110" />');
                    }, 0);
                }
        */
            });});
    </script>
    <script>
        jQuery(function($) {

            $( "#registration_date" ).datepicker({
                dateFormat: 'yy-mm-dd',
                showOtherMonths: true,
                selectOtherMonths: false,
                //isRTL:true,


                /*
                changeMonth: true,
                changeYear: true,

                showButtonPanel: true,
                beforeShow: function() {
                    //change button colors
                    var datepicker = $(this).datepicker( "widget" );
                    setTimeout(function(){
                        var buttons = datepicker.find('.ui-datepicker-buttonpane')
                        .find('button');
                        buttons.eq(0).addClass('btn btn-xs');
                        buttons.eq(1).addClass('btn btn-xs btn-success');
                        buttons.wrapInner('<span class="bigger-110" />');
                    }, 0);
                }
        */
            });});
    </script>

    <script>
        jQuery(function($) {

            $( "#leave_date" ).datepicker({
                dateFormat: 'yy-mm-dd',
                showOtherMonths: true,
                selectOtherMonths: false,
                //isRTL:true,


                /*
                changeMonth: true,
                changeYear: true,

                showButtonPanel: true,
                beforeShow: function() {
                    //change button colors
                    var datepicker = $(this).datepicker( "widget" );
                    setTimeout(function(){
                        var buttons = datepicker.find('.ui-datepicker-buttonpane')
                        .find('button');
                        buttons.eq(0).addClass('btn btn-xs');
                        buttons.eq(1).addClass('btn btn-xs btn-success');
                        buttons.wrapInner('<span class="bigger-110" />');
                    }, 0);
                }
        */
            });});
    </script>


    @if(isset($query))
        <script>
            $(document).ready(first_fill_siblings_table(),get_all_student_data()/*,staff_data_drop_down()*/);
            /*$(document).ready(update_nationality());
            $(document).ready(acadimic_year());
            $(document).ready(rigistration_class());
            $(document).ready(countries_get());
            $(document).ready(m_countries_get());*/
            /*$(document).ready(get_all_student_data());*/



        </script>
    @endif
    {{-- <script>
         $('.select_2_enable').select2();
     </script>--}}

    {{---------------------  edit student ----------------------}}
    <script>
        $(document).on('click', '#edit_po', function () {
            $('#edit_po').text("Save");
            $('#edit_po').attr('id', 'save_po');
            $('.set_all_disabled').prop('disabled', false);

            if($('#set_po_id').attr('staffs_id') != '' && $('#set_po_id').attr('staffs_id') != null)
            {
                $('.staff_disable_only').prop('disabled',true);
            }


            $('#creat_dep').addClass('hide');
            $('#more').addClass('hide');
        });
    </script>

    {{----------------  create new student ------------------}}
    <script>
        $(document).on('click', '#creat_dep', function () {

            window.location.href = '/student';

        });
    </script>

    {{---------------------- delete journal ------------------------------}}

    <script>
        $(document).on('click', '#delete_po', function () {
            var student_id = $('#set_po_id').attr('current_po_id');
            console.log('student_id : '+ student_id);

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/delete_student_row/',
                dataType: 'json',
                type: 'get',
                data: {
                    student_id: student_id,
                },

                success: function (response) {

                    window.location.href = '/student';
                }

            });

        });
    </script>


    {{-----------   on leave page -------------------}}
    <script type='text/javascript'>

        $(window).bind('beforeunload', function () {
            if ($('#save_po').length) {
                return 'Are you sure you want to leave?';
            }

        });


    </script>


    {{-----------------------print----------------}}
    <script>

        $(document).on('click', '#print', function () {

            //  window.location.href = 'get_po2/276'

            var id = $('#set_po_id').attr('current_po_id');
            console.log(id);
            window.open('/get_student_rpt/' + id + '', '_blank');


        });



    </script>

    <!----- upload photo ---->

    <script>

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#imagePreview').css('background-image', 'url('+e.target.result +')');
                    $('#imagePreview').hide();
                    $('#imagePreview').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#id-input-file-3").change(function() {
            readURL(this);
        });
    </script>
@endsection