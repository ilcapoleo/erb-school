@extends('layouts.main_app')

@section('main_content')
   <style>


        .collapse > .row>.col-md-4>.btn, .collapse > .row>.col-md-4>.dropdown>.btn {
    margin-top: 0;

}
        .material-switch > label::before {
            background: rgb(0, 0, 0);
            box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
            border-radius: 8px;
            content: '';
            height: 16px;
            margin-top: 3px;
            position: absolute;
            opacity: 0.3;
            transition: all 0.4s ease-in-out;
            width: 40px;
            left: -29px;
        }


        .material-switch > label::after {
            background: rgb(255, 255, 255);
            border-radius: 16px;
            box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
            content: '';
            height: 24px;
            left: -45px;
            margin-top: -8px;
            position: absolute;
            top: 6px;
            transition: all 0.3s ease-in-out;
            width: 24px;
        }

        .material-switch > input[type="checkbox"]:checked + label::after {
            background: inherit;
            left: -5px;
        }
        .panel .table > thead > tr > td:first-child, .panel .table > thead > tr > th:first-child, .panel .table > tbody > tr > td:first-child, .panel .table > tbody > tr > th:first-child, .panel .table > tfoot > tr > td:first-child, .panel .table > tfoot > tr > th:first-child{
            padding-left: 0px;
        }

        ul{
            margin: 0;
            padding: 0;
            list-style: none;
        }
        .red-border{
            border: 1px solid red  !important;
        }
        .error{
            border: 1px solid red  !important;
            border-left: 1px double red !important;
            border-top: 2px double red !important;
        }

        #yes,#no{
            display:none;
            margin-right: 5px;
        }





   </style>

   <link href="{{asset('css/select2.min.css')}}" rel="stylesheet" />

    <div class="main">
        <!-- Navbar-content -->
        <nav class="navbar navbar-default navbar2">

            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                    <!--breadcrumbs-->
                    <div class="row">

                        <div class="col-md-10 col-lg-10 ">

                        <ol class="breadcrumb breadcrumb_nav">
                            <li><a href="/show_inventory">Purchase Document</a></li>
                            <li class="active" id="second">
                                @if(isset($master_inventory))
                                    @if($master_inventory->doc_seq != 0)
                                        {{$master_inventory->doc_seq}}
                                    @else
                                        Draft
                                    @endif
                                @endif
                            </li>
                        </ol>
                        </div>


                        @if(isset($master_inventory))
                            <div class="col-md-2 col-lg-2">
                                @can('see-all-inventory')
                                @if(@\App\inventory_master::where('doc_id','<',$master_inventory->doc_id)->orderBy('doc_id','desc')->first()->doc_id)
                                    <div class="col-md-6 ">
                                        <a href="/inventory/{{@\App\inventory_master::where('doc_id','<',$master_inventory->doc_id)->orderBy('doc_id','desc')->first()->doc_id}}"><span
                                                    class="glyphicon glyphicon-arrow-left"></span>
                                        </a>
                                    </div>
                                @endif
                                @if(@\App\inventory_master::where('doc_id','>',$master_inventory->doc_id)->orderBy('doc_id','asc')->first()->doc_id)
                                    <div class="col-md-6 ">
                                        <a href="/inventory/{{@\App\inventory_master::where('doc_id','>',$master_inventory->doc_id)->orderBy('doc_id','asc')->first()->doc_id}}">
                                    <span class="glyphicon glyphicon-arrow-right">
                                </span>
                                        </a>
                                    </div>
                                @endif
                                @elsecannot('see-all-inventory')
                                    <?php $employee_id = \App\Employee::where('employee_user_id','=',auth()->user()->id)->first()->employee_id; ?>

                                    @if( $employee_id == $master_inventory->create_by_user_id)
                                        @if(@\App\inventory_master::where('doc_id','<',$master_inventory->doc_id)->where('create_by_user_id',$employee_id)->orderBy('doc_id','desc')->first()->doc_id)
                                            <div class="col-md-6 ">
                                                <a href="/inventory/{{@\App\inventory_master::where('doc_id','<',$master_inventory->doc_id)->where('create_by_user_id',$employee_id)->orderBy('doc_id','desc')->first()->doc_id}}"><span
                                                            class="glyphicon glyphicon-arrow-left"></span>
                                                </a>
                                            </div>
                                        @endif
                                        @if(@\App\inventory_master::where('doc_id','>',$master_inventory->doc_id)->where('create_by_user_id',$employee_id)->orderBy('doc_id','asc')->first()->doc_id)
                                            <div class="col-md-6 ">
                                                <a href="/inventory/{{@\App\inventory_master::where('doc_id','>',$master_inventory->doc_id)->where('create_by_user_id',$employee_id)->orderBy('doc_id','asc')->first()->doc_id}}">
                                    <span class="glyphicon glyphicon-arrow-right"></span>
                                                </a>
                                            </div>
                                        @endif
                                    @endif
                                @endcan
                            </div>
                        @endif

                    </div>   <!--End breadcrumbs-->
                        <div class="row">

                            <div class="col-md-4 col-lg-4 col-sm-6 col-xs-6 buttons">
                                {{--<button id="edit_user" type="button" class="btn btn-danger " >Edit</button>--}}
                                @if(isset($master_inventory))
                                    @if($master_inventory->status_id == 3 || $master_inventory->status_id == 4)
                                        <button id="edit_po" type="button" class="btn btn-danger " >Edit</button>
                                        <button type="button" class="btn" id="creat_po" >Create</button>
                                    @else
                                        {{--<button id="save_po" type="button" class="btn btn-danger " >Save</button>
                                        <button type="button" class="btn hide" id="creat_po" >Create</button>--}}
                                        <button id="edit_po" type="button" class="btn btn-danger " >Edit</button>
                                        <button type="button" class="btn" id="creat_po" >Create</button>
                                    @endif
                                @else
                                    <button id="save_po" type="button" class="btn btn-danger " >Save</button>
                                    <button type="button" class="btn hide" id="creat_po" >Create</button>
                                @endif
                            </div>



                            <div class="col-md-4 col-lg-4 col-sm-6 col-xs-6">
                                @if(isset($master_inventory))

                                    @if($master_inventory->status_id == 3 || $master_inventory->status_id == 4|| $master_inventory->status_id == 2)
                                        <div class="dropdown">
                                            <button class="btn btn-primary  dropdown-toggle" type="button" data-toggle="dropdown" id="po_more"  hidden>More
                                                <span class="caret"></span></button>
                                            <ul class="dropdown-menu">

                                                <li class="hide"><a href="#" id="delete_po">Delete</a></li>
                                                <!-- temporery -->   {{--<li><a href="#">Export</a></li>--}}
                                                {{--<li><a href="#" id="dublicate_po">Duplicate</a></li>--}}
                                                <li><a href="#" id="print">Print</a></li>

                                            </ul>
                                        </div>
                                    @elseif($master_inventory->status_id == 1)
                                        <div class="dropdown">
                                            <button class="btn btn-primary  dropdown-toggle" type="button" data-toggle="dropdown" id="po_more"  hidden>More
                                                <span class="caret"></span></button>
                                            <ul class="dropdown-menu">

                                                <li class=""><a href="#" id="delete_po">Delete</a></li>
                                                <!-- temporery -->   {{--<li><a href="#">Export</a></li>--}}
                                                {{--<li><a href="#" id="dublicate_po">Duplicate</a></li>--}}
                                                <li><a href="#" id="print">Print</a></li>

                                            </ul>
                                        </div>
                                    @else
                                        <div class="dropdown">
                                            <button class="hide btn btn-primary  dropdown-toggle" type="button" data-toggle="dropdown" id="po_more"  hidden>More
                                                <span class="caret"></span></button>
                                            <ul class="dropdown-menu">

                                                <li class=""><a href="#" id="delete_po">Delete</a></li>
                                                <!-- temporery -->   {{--<li><a href="#">Export</a></li>--}}
                                                {{--<li><a href="#" id="dublicate_po">Duplicate</a></li>--}}
                                                <li><a href="#" id="print">Print</a></li>

                                            </ul>
                                        </div>
                                    @endif
                                @else
                                    <div class="dropdown">
                                        <button class="hide btn btn-primary  dropdown-toggle" type="button" data-toggle="dropdown" id="po_more"  hidden>More
                                            <span class="caret"></span></button>
                                        <ul class="dropdown-menu">

                                            <li class="hide"><a href="#" id="delete_po">Delete</a></li>
                                            <!-- temporery --> {{--<li><a href="#">Export</a></li>--}}
                                            {{--<li><a href="#" id="dublicate_po">Duplicate</a></li>--}}
                                            <li><a href="#" id="print">Print</a></li>

                                        </ul>
                                    </div>
                                @endif

                            </div>
                                    <button class="hide btn btn-primary  dropdown-toggle" type="button" id="create_invoice" >Create Invoice</button>

                           <!--search-->
                           <!--
                            <div class="col-md-4">
                                <form class="navbar-form navbar-right" >
                                    <div class="input-group form-group">
                                        <input type="text" value="" class="form-control" placeholder="Search ...">
                                    </div>
                                </form>
                            </div>
                            -->
                        </div> <!--collapse -->
              
                </div><!--collapse -->
                
                 <!--buttons row +second breadcrumb-->

                    <div class="row third-nav">


                        <div class="col-md-6 col-lg-6 ">

                        <div class="col-md-3 col-lg-3 col-sm-3 col-xs-3">
                            <button id="send_mail" type="button" class="{{--set_all_disabled--}} btn btn_third_nav" >Send by Mail</button>
                        </div>

                        <div class="col-md-3 col-lg-3 col-sm-3 col-xs-3">
                            <button id="sendto_approved_btn" type="button" class="set_all_disabled btn btn_third_nav" @if(isset($master_inventory)) disabled @endif >To Approve</button>
                        </div>

                        @can('approve_inventory')
                            <div class="col-md-3 col-lg-3 col-sm-3 col-xs-3">
                                <button id="approved_btn" type="button" class="set_all_disabled btn approved btn_third_nav" @if(isset($master_inventory)) disabled @endif >Approved</button>
                            </div>
                        @endcan

                        </div>


                        <div class="col-md-6 col-lg-6 ">


                            <span id="current_choosed_status" coosed_status="" class="breadcrumbs ">
                                 @if(isset($master_inventory))

                                    <span id="draft_po"  class="@if($master_inventory->status_id == 1) po-active @endif breadcrumb">Draft Inventory</span></li>
                                    <span id="to_approve_po"  class="@if($master_inventory->status_id == 2) po-active @endif breadcrumb">To Approved </span></li>
                                    <span id="approved_po"  class="@if($master_inventory->status_id == 3) po-active @endif  breadcrumb ">Approved </span></li>
                                    <span id="invoiced"  class="@if($master_inventory->status_id == 4) po-active @endif  breadcrumb ">Invoiced</span></li>

                                @else
                                    <span id="draft_po"  class="breadcrumb">Draft Inventory</span></li>
                                    <span id="to_approve_po"  class="breadcrumb">To Approved </span></li>
                                    <span id="approved_po"  class="breadcrumb ">Approved </span></li>
                                    <span id="invoiced" class="breadcrumb">Invoiced</span>
                                @endif
                            </span>



                        </div>
                    </div>
                
                
            </div>
        </nav>
        <!-- End Navbar-content -->

        <!-- Success messages -->
        <div class="alert alert-success alert-autocloseable-success" style="display:none;position:absolute;" >
            Your Purshase Document Saved Successfully!

        </div>
        <!--end Success messages -->



        <!-- failuer messages -->
        <div class="alert alert-danger errorMessage1" style="display:none;position:absolute;" >
            Your should fill all fields!

        </div>
        <!--end failuer messages -->

        <div class="alert alert-danger errorMessage2" style="display:none;position:absolute;" >
            PLease fill Table!

        </div>

        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Purchase Order</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="row">

                                {{-- set po sitting--}}
                                @if(isset($master_inventory))
                                    <div id="set_po_id"  current_po_id="{{$master_inventory->doc_id}}" current_po_status="{{$master_inventory->status_name}}" closed_or_not="" current_id="{{Auth::user()->id}}"></div>
                                @else
                                    <div id="set_po_id"  current_po_id="" current_po_status="draft_po" closed_or_not="0" current_id="{{Auth::user()->id}}"></div>
                                @endif
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <div class="col-md-6 col-lg-6">
                                    <div class="form-group">

                                        <label class="col-md-6 col-lg-6 control-label">Supplier</label>
                                        <div class="col-md-6 col-lg-6">

                                            <select id="supplier" name="supplier" class="get_valide_for get_all_selectors set_all_disabled select_2_enable form-control required supplier_error err_input" @if(isset($master_inventory)) disabled  @endif >
                                                <option value=""> </option>
                                                @foreach($supplier as $id=>$role)
                                                    @if(isset($master_inventory))
                                                        @if($master_inventory->VendorID == $id)
                                                            <option selected value="{{$id}}">{{$role}}</option>
                                                        @else
                                                            <option value="{{$id}}">{{$role}}</option>
                                                        @endif
                                                    @else
                                                        <option value="{{$id}}">{{$role}}</option>
                                                    @endif
                                                @endforeach
                                            </select>

                                        </div>
                                    </div>
                                    </div>
                                    <div class="col-md-6 col-lg-6">

                                        <div class="bootstrap-iso">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-md-12 col-lg-12 col-sm-6 col-xs-12">


                                                    <form method="post">
                                            <div class="form-group">
                                                <label class=" control-label col-md-6 col-lg-6 " for="datetime1">Document Date</label>
                                                <div class="input-group col-md-6 col-lg-6">

                                                    @if(isset($master_inventory))
                                                        <input type="text" id="datetime1" class="set_all_disabled form-control required err_input document_date_error " crrent_date="{{$curr_date}}" name="date" value="{{$master_inventory->doc_date}}" @if(isset($master_inventory)) disabled @endif >
                                                    @else
                                                        <input type="text" id="datetime1" class="set_all_disabled form-control required err_input document_date_error" crrent_date="{{$curr_date}}" name="date" value="{{$curr_date}}">
                                                    @endif

                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar">
                                                        </i>
                                                    </div>
                                                </div>

                                            </div>
                                        </form>

                                                </div>
                                            </div>
                                        </div>
                                    </div>



                                    </div>

                                </div>
                            </div>

                                {{--------------  error row------------------}}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <div id="supplier_error" class="alert alert-danger errorMessage hide ">
                                                <strong >Wrong Input!</strong>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                {{----------------------------------------------}}
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6 col-lg-6">
                                        <div class="form-group">

                                            <label class="col-md-6 col-lg-6 control-label">Document Type</label>
                                            <div class="col-md-6 col-lg-6">

                                                <select id="doc_type" name="doc_type" class="get_valide_for get_all_selectors set_all_disabled select_2_enable form-control required document_type_error err_input" @if(isset($master_inventory)) disabled @endif >
                                                    <option value=""> </option>
                                                    @foreach($document_types as $id=>$role)
                                                        @if(isset($master_inventory))
                                                            @if($master_inventory->doc_type == $id)
                                                                <option selected value="{{$id}}">{{$role}}</option>
                                                            @else
                                                                <option value="{{$id}}">{{$role}}</option>
                                                            @endif
                                                        @else
                                                            <option value="{{$id}}">{{$role}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                                {{--------------  error row------------------}}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <div id="document_type_error" class="alert alert-danger errorMessage hide ">
                                                <strong >Wrong Input!</strong>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                {{----------------------------------------------}}

                            </div>
                            <br>
                         </div>
                        
                        <div class="row">

                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#products">Products</a></li>

                                </ul>

                                <div class="tab-content">
                                    <div id="products" class="tab-pane fade in active table-responsive">

                                        <table  class="table table-bordred table-striped table-bordered table-striped table-hover table-condensed table-responsive text-center" id="op_table" >
                                            <thead>
                                            <th>Close Po</th>
                                            <th>PO</th>
                                            <th >Items</th>
                                            <th >Quantity</th>
                                            <th>Edit</th>
                                            <th>Delete</th>
                                            
                                            </thead>
                                            <tbody>
                                            @if(isset($details_inventory))
                                                <?php $i=0 ?>
                                                @foreach($details_inventory as $details_inventory_single)
                                                    <tr class="">
                                                        <td td_type="check_box" current_value="{{$all_closed_state[$i]}}" @if($all_closed_state[$i] == 1)yes_or_no="Yes"@else yes_or_no="No"@endif row_id="{{$details_inventory_single->sub_doc_id}}" class="remove_error include_check_v check_box_v">
                                                            @if($all_closed_state[$i] == 1)
                                                                Yes
                                                            @else
                                                                No
                                                            @endif
                                                        </td>
                                                        <td td_type="search" data-select2-id="10" class="po_values_v" current_value="{{$details_inventory_single->po_id}}">{{$details_inventory_single->po_seq}}</td>
                                                        <td td_type="search" now_selected="{{$details_inventory_single->pro_id}}" data-select2-id="35" class="item_product_v" current_value="{{$details_inventory_single->pro_id}}" seq="{{$details_inventory_single->sub_doc_id}}">{{$details_inventory_single->pro_name}}</td>
                                                        <td td_type="input" maxmum_quan="{{$all_max_quantity[$i]}}" class="quantity_value_v select_all_remove_error" current_value="{{$details_inventory_single->qut}}">{{$details_inventory_single->qut}}</td>
                                                        <td><button class="set_all_disabled btn btn-primary btn-xs edit_row" @if(isset($master_inventory)) disabled @endif >Edit</button></td>
                                                        <td>
                                                            <button class="set_all_disabled btn btn-danger btn-xs delete_row" @if(isset($master_inventory)) disabled  @endif>
                                                                <span class="lnr lnr-trash">
                                                                </span>
                                                            </button>


                                                        </td>
                                                    </tr>
                                                    <?php $i++ ?>
                                                    @endforeach
                                            @endif
                                            </tbody>
                                        </table>

                                           <div class="row">
                                               <div class="col-md-4 col-lg-4">
                                                   <button type="button" class=" set_all_disabled btn btn-primary" id="add_row" @if(isset($master_inventory)) disabled @endif disabled>Add Row</button>
                                               </div>
                                          </div>

                                        
                                        <hr>

                                        <div class="row">

                                            <div class="col-md-4 col-lg-4 ">
                                                <div class="form-group">
                                                    <label for="comment">Add your note :</label>
                                                    <textarea class="set_all_disabled form-control" rows="5" id="po_comment" @if(isset($master_inventory)) disabled  @endif>@if(isset($master_inventory)) {{$master_inventory->notes}} @endif</textarea>
                                                </div>
                                            </div>

                                            <div class="col-md-8 col-lg-8 ">
                                             
                                                <div class="col-md-12 col-lg-12 ">
                                                    <div class="col-md-4 col-lg-4">Total Quantity:</div>
                                                    <div id="total_quantity" class="col-md-8 col-lg-8" >0.00 </div>
                                                </div>


                                            </div>

                                        </div>

                                </div>
                            </div>
                        </div>
                        
                        
                        
                        
                        
                    </div>
                 </div>
            <!-- END MAIN CONTENT -->
            </div>
        <!-- END MAIN -->
    </div>
    </div>

@endsection


@section('custom_footer')

    <script src="{{asset('assets/scripts/bootstrap-datetimepicker.min.js')}}"></script>

<!--date/time picker-->
 
    <script>

        $('#datetime1').datetimepicker({format: 'yyyy-mm-dd',autoclose: true, minView: 2});
        $('#datetime2').datetimepicker({format: 'yyyy-mm-dd',autoclose: true, minView: 2});
        $('#datetime3').datetimepicker({format: 'yyyy-mm-dd',autoclose: true, minView: 2});


    </script>

    <script src="{{asset('js/select2.min.js')}}"></script>



    {{--------------------  active side & nav bar ------------------------}}
    <script>
        $('.side_sheets').addClass('hide');
        $('#inventory').removeClass('hide');

        $("#menu li").removeClass('active');
        $('#Inventory_nav_bar').addClass('active');

        $('#pages ul li a').removeClass('active');
        $('#inventory_side_bar').addClass('active');

    </script>





    {{------------  add new row  ------------------------}}
    <script>
        $(document).on('click','#add_row',function () {
            $(this).attr('disabled',true);
            var trs =  $('#op_table > tbody  > tr').find('.include_check_v');

            var maximum = 0;
            trs.each(function() {
                var value = parseInt($(this).attr('row_id'));
                maximum = (value > maximum) ? value : maximum;
            });

            var index_t = maximum +1;
            console.log(index_t);
            var row = '<tr class= "added_now" >' +
                '<td td_type="check_box">' +
                    '<div class="material-switch pull-right">\n' +
                    '<input id="'+index_t+'" class="include_check" name="someSwitchOption001" type="checkbox"/>\n' +
                    '<label for="'+index_t+'" class="label-primary"></label>\n' +
                    '</div>'+
                '</td>' +
                '<td td_type="search">' +
                '<select class="po_values form-control js-example-disabled-results">\n' +
                '</select></td>' +
                '<td td_type="search" now_selected="">' +
                '<select class="item_product form-control js-example-disabled-results">\n' +
                '</select></td>' +
                '<td td_type="input"><input class="quantity_value row_data form-control" min="1"  data-role="input" type="number"  disabled/></td>'


                +'<td>'+'<button class="set_all_disabled btn btn-primary btn-xs Save_row">'+'Save'+'</button>'
                +'</td>'
                +'<td>'
                +'<button class="set_all_disabled btn btn-danger btn-xs delete_row"  >'+'<span class="lnr lnr-trash"></span>'
                +'</td>'
                +'</tr>';
            $("#op_table").prepend(row);
            get_item_type_drop_down($('.po_values'));
            $(".po_values").select2();
            $(".po_values").siblings('.select2').css('width', '71px');

        });


    </script>

{{-----------  get drop down values --------------}}
    <script>

        function get_item_type_drop_down($dropdown)
        {

            //var $dropdown = $("#doc_type");
            $dropdown.prop('disabled',true);
            var supplier = $('#supplier').val();
            var curr_selectet_value =  $dropdown.parents('tr').find('.po_values_v').attr('current_value');
            console.log('curr_selectet_value : ' + curr_selectet_value);
            /*$dropdown.find('option').remove();*/

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/get_po_values_drop_down/',
                dataType : 'json',
                type: 'get',
                data: {
                    supplier:supplier,
                    curr_selectet_value:curr_selectet_value
                },

                success:function(response) {

                    console.log(response);

                    var selected_val = $dropdown.parents('td').attr('current_value');

                    console.log('selected drop '+selected_val);

                    $dropdown.find('option').remove();

                    for(var i =0;i<response['item_type'].length;i++)
                    {
                        $dropdown.append($("<option />").val(response['item_type'][i]['po_id']).text(response['item_type'][i]['po_seq']));
                    }

                    if(response['extra_po_colsed']['po_id'] != "")
                    {
                        $dropdown.append($("<option />").val(response['extra_po_colsed']['po_id']).text(response['extra_po_colsed']['po_seq']));
                        console.log(response['extra_po_colsed']['po_id']);
                    }

                    $dropdown.val(selected_val);
                    $dropdown.prop('disabled',false);
                    $(".item_product").select2();
                    $(".item_product").siblings('.select2').css('width', '71px');
                }

            });

        }

    </script>


    <script>
        $(document).on('change','.po_values',function () {
            var po_id = $(this).val();
            var selected_v_pro = $(this).parents('tr').find('.item_product_v').attr('current_value');
            /*var selected_v_pro = '';*/

            //get_item_product($(this),po_id,"");
            get_item_product($(this),po_id,selected_v_pro);
        });

        function get_item_product(thiss_item,po_id,edited_value){

            var item_selected_array =[];
            var all_selected_v = $('.po_values_v');

            var seq = thiss_item.parents("tr").find('.item_product_v').attr('seq');
            var checked = thiss_item.parents("tr").find('.include_check_v').attr('current_value');
            var product_id = thiss_item.parents("tr").find('.item_product_v').attr('current_value');

            console.log('seq' +seq + 'checked' +checked + 'product_id'+product_id);


                all_selected_v.each(function () {
                    if($(this).attr('current_value') == po_id )
                    {
                        if($(this).parents('tr').find('.item_product_v').attr('current_value') != edited_value)
                        {
                            var selectt = $(this).parents('tr').find('.item_product_v').attr('current_value');
                            item_selected_array.push(selectt);
                        }

                    }


                }) ;

            if(item_selected_array.length == 0)
            {
                item_selected_array = "";
            }

            console.log(item_selected_array);

            var $dropdown = thiss_item.parents("tr").find('.item_product');
            $dropdown.val('');
            $dropdown.attr('disabled','disabled');


           console.log(po_id);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/get_item_product_drop_down/',
                dataType : 'json',
                type: 'get',
                data: {
                    po_id:po_id,
                    item_selected_array:item_selected_array,
                    seq:seq,
                    checked:checked,
                    product_id:product_id
                },

                success:function(response) {

                    console.log(response);
                    /*console.log(response['data'][0]['pro_id']);*/

                    var selected_val = $dropdown.parents('td').attr('current_value');

                    $dropdown.parents('td').attr('now_selected',selected_val);
                    console.log('selected drop '+selected_val);

                    $dropdown.find('option').remove();
                    $dropdown.append($("<option />").val('').text(''));
                    for(var i =0;i<response['data'].length;i++)
                    {
                        $dropdown.append($("<option />").val(response['data'][i]['pro_id']).text(response['data'][i]['pro_name']));
                    }
                    for(var i =0;i<response['extra_v'].length;i++)
                    {
                        $dropdown.append($("<option />").val(response['extra_v'][i]['pro_id']).text(response['extra_v'][i]['pro_name']));
                    }

                    $dropdown.val(selected_val);

                    get_quantity($dropdown,0);
                    $dropdown.removeAttr('disabled');
                }

            });
        }
    </script>

        {{---------  get quantity ---------------}}
    <script>
        $(document).on('change','.item_product',function(){

            var val_selected = $(this).val();
            $(this).parent('td').attr('now_selected',val_selected);
            get_quantity($(this),0);
        });


        function get_quantity(item_product,save) {

            var save_btn = item_product;
            var item_pro_id = item_product.parents("tr").find('.item_product').val();
            var po_id = item_product.parents("tr").find('.po_values').val();
            var quantitys = item_product.parents("tr").find('.quantity_value');
            var inventory_id = $('#set_po_id').attr('current_po_id');
            //quantitys.val('');
            quantitys.prop('disabled',true);
            var current_exist_q = quantitys.val();

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/get_quantity_limit/',
                dataType : 'json',
                type: 'get',
                data: {
                    po_id:po_id,
                    item_pro_id:item_pro_id,
                    inventory_id:inventory_id
                },

                success:function(response) {

                    //console.log(response);

                    console.log('quantity ' + response['quantity']);
                    if(save == 0) {
                        if (response['quantity'] > 0) {
                            quantitys.val(response['quantity']);
                            quantitys.attr('value_now', response['quantity']);
                            quantitys.attr('max', response['quantity']);
                            quantitys.parent("td").attr('maxmum_quan', response['quantity']);
                            quantitys.removeAttr('disabled')

                        }
                        else if(response['quantity'] != null && response['quantity'] == 0)
                            {
                                notification('glyphicon glyphicon-ok-sign','Warning','Remain quantity = 0','danger');
                                quantitys.val(response['quantity']);
                            }
                    }
                    else if(save == 1)
                    {
                        var cur_q = response['quantity'] - current_exist_q;
                        if(cur_q > 0 )
                        {
                            /*var row_id = $(this).parents("tr").find('.include_check_colsed').attr('id');
                            item_product.parents("tr").find('.include_check_colsed').parents("td").attr('current_value',0).attr('yes_or_no','No').attr('row_id',row_id);*/
                            console.log('not closed');
                            save_table_effect(save_btn);
                        }
                        else{

                            var row_id = $(this).parents("tr").find('.include_check').attr('id');
                            item_product.parents("tr").find('.include_check').parents("td").attr('current_value',1).attr('yes_or_no','Yes').attr('row_id',row_id);
                            console.log('closed');
                            save_table_effect(save_btn);
                        }

                    }

                }

            });

        }
    </script>


    {{------------- limit the max of input number  ----------------}}
    <script>
        $(document).on('change ','.quantity_value',function() {
            var max = parseInt($(this).attr('max'));
            var min = parseInt($(this).attr('min'));
            if ($(this).val() > max)
            {
                $(this).val(max);
            }
            else if ($(this).val() < min)
            {
                $(this).val(min);
            }
        });
    </script>

    {{-------------  change attr value_now on change of input -----------------}}
    <script>
        $(document).on(' change','.quantity_value',function () {
            var value_n = $(this).val()
            $(this).attr('value_now',value_n);
        });
    </script>

    {{--------------  save row  ----------------------}}

    <script>
        $(document).on("click", ".Save_row", function(){

            var empty = false;
            var input = $(this).parents("tr").find('input[type="text"],input[type="number"]');

            if($(this).parents("tr").find('.include_check:checked').length>0)
            {
                var row_id = $(this).parents("tr").find('.include_check').attr('id');

                $(this).parents("tr").find('.include_check').parents("td").attr('current_value',1).attr('yes_or_no','Yes').attr('row_id',row_id);

            }
            else{
                var row_id = $(this).parents("tr").find('.include_check').attr('id');

                $(this).parents("tr").find('.include_check').parents("td").attr('current_value',0).attr('yes_or_no','No').attr('row_id',row_id);

            }

            if(!$(this).parents("tr").find('.po_values').val() || $(this).parents("tr").find('.po_values').val()=="" || !$(this).parents("tr").find('.item_product').val() || $(this).parents("tr").find('.item_product').val()=="")
            {
                empty = true;
            }

           input.each(function(){
                if(!$(this).val()|| $(this).val()=="" || $(this).val()== 0 ){
                    $(this).addClass("error");
                    empty = true;
                } else{
                    $(this).removeClass("error");
                }
            });

            console.log('empty '+empty);

            $(this).parents("tr").find(".error").first().focus();


            var row_id = $(this).closest('tr').attr('id');


            if(!empty ) {
                $(this).attr('disabled','disabled');
                /*get_quantity(1,$(this));*/

                ($(this)).removeAttr('disabled');

                //save_table_effect($(this));
                $(this).prop('disabled',true);
                get_quantity($(this),1);

                /*------------ check box ----------------------*/

            }


        });

        function save_table_effect(thiss) {
            console.log('passed')


            var check_box = thiss.parents("tr").find('input.include_check');
            var check_val = check_box.parents("td").attr('current_value');
            var check_text_val = check_box.parents("td").attr('yes_or_no');
            var row_id = check_box.parents("td").attr('row_id');

            check_box.parents("td").addClass('include_check_v').addClass('check_box_v').attr('row_id',row_id).html(check_text_val);

            /*--------- select  po ------------------*/
            var select_po_drop_down = thiss.parents("tr").find('select.po_values');
            var value_select = select_po_drop_down.val();
            var text_select = thiss.parents("tr").find('select.po_values option:selected').text();

            select_po_drop_down.parent("td").addClass('po_values_v').attr('current_value', value_select).html(text_select);
            /*--------------------------*/


            /*--------- select  products ------------------*/
            var select_product_drop_down = thiss.parents("tr").find('select.item_product');
            var value_select_pro = select_product_drop_down.val();
            var text_select_product = thiss.parents("tr").find('select.item_product option:selected').text();
            var seq = thiss.parents("tr").find('.item_product_v').attr('seq');
            if(!seq || seq == "")
            {
                seq = 0.1;
            }

            select_product_drop_down.parent("td").addClass('item_product_v').attr('current_value', value_select_pro).attr('seq', seq).html(text_select_product);
            /*--------------------------*/


            /*--------- input quantity ------------------*/
            var quantity = thiss.parents("tr").find('.quantity_value');
            var quantity_value = quantity.val();


            console.log('quantity_value' +quantity_value)
            quantity.parent("td").addClass('select_all_remove_error quantity_value_v').attr('current_value', quantity_value).html(quantity_value);
            /*--------------------------*/


            if (thiss.parents("tr").hasClass('added_now')) {
                thiss.parents("tr").removeClass('added_now');

            }
            if (thiss.parents("tr").hasClass('edited_now')) {
                thiss.parents("tr").removeClass('edited_now');

            }

            if ($('.added_now').length == 0) {
                $("#add_row").removeAttr("disabled");
            }

            thiss.text('Edit');
            thiss.removeClass('Save_row');
            thiss.addClass('edit_row');

            get_totals();
            thiss.prop('disabled',false);
        }


    </script>
    {{----------------------  edit row --------------------}}
    <script>
        $(document).on("click", ".edit_row", function(){
            if(!$('#op_table tr').hasClass('added_now') && !$('#op_table tr').hasClass('edited_now'))
            {



            $(this).parents("tr").addClass('edited_now');

            $(this).parents("tr").find("td:not(:last-child)").each(function(){
                if($(this).attr('td_type') == 'input')
                {
                    //maxmum_quan

                    $(this).html('<input class="quantity_value row_data form-control" min="1" max="' + $(this).attr('maxmum_quan') +'" data-role="input" type="number"  value="'+$(this).text()+'" />');



                }

                else if ($(this).attr('td_type') == 'search')
                {
                    if($(this).hasClass('po_values_v'))
                    {
                        $(this).html('<select class="po_values form-control js-example-disabled-results">\n' +
                            '</select>');
                        get_item_type_drop_down($(this).find('.po_values'));
                        var selected_v = $(this).attr('current_value');
                        $(this).find(".po_values").val(selected_v);
                        $(".po_values").select2();
                        $(".po_values").siblings('.select2').css('width', '71px');
                    }

                    else if($(this).hasClass('item_product_v'))
                    {

                        $(this).html('<select class="item_product form-control js-example-disabled-results">\n' +
                            '</select>');
                        var po_id = $(this).parents('tr').find('.po_values_v').attr('current_value');
                        var now_selected = $(this).parents('tr').find('.item_product_v').attr('now_selected');

                        get_item_product($(this).find('.item_product'),po_id,now_selected);
                        var selected_v = $(this).attr('current_value');
                        $(this).find(".item_product").val(selected_v);
                        $(".item_product").select2();
                        $(".item_product").siblings('.select2').css('width', '71px');
                    }

                }
                else if ($(this).attr('td_type') == 'check_box')
                {
                    $(this).html('<div class="material-switch pull-right">'+
                        '<input id="'+ $(this).attr('row_id')+'" class="include_check" name="someSwitchOption001" type="checkbox"/>'+
                        '<label for="'+$(this).attr('row_id')+'" class="label-primary"></label>'+
                        '</div>');
                    if($(this).attr('current_value')==1)
                    {
                        $(this).find('.include_check').prop('checked', false);
                    }
                    else
                    {
                        $(this).find('.include_check').prop('checked', false);
                    }
                }

            });

            $("#add_row").attr("disabled", "disabled");
            $(this).text('Save');
            $(this).removeClass('edit_row');
            $(this).addClass('Save_row');
            get_totals();
            }
        });

    </script>

{{---------------- get total quantity ----------------------}}

    <script>
        function get_totals() {
            var input = $('#op_table > tbody  > tr');
            var total_quantity = 0;

            input.each(function () {
                if (!$(this).hasClass('added_now')) {
                    total_quantity += parseFloat($(this).find('.quantity_value_v').attr('current_value'));
                }


            });

            $('#total_quantity').html(total_quantity);

        }

    </script>

{{---------------------------}}
    {{---------------  active add row  on supplier change --------------------}}
    <script>
        $(document).on('change','#supplier',function () {
            enable_disable_add();
        });

        function enable_disable_add(){


            if($('#supplier').val() != "" )
            {

                $('#add_row').prop('disabled',false);
            }
            else if( $('#set_po_id').attr('current_po_status') != '')
            {
                $('#add_row').prop('disabled',true);
            }

            else{
                $('#add_row').prop('disabled',true);
            }

        }

        $(document).on('click','#add_row',function()
        {
            $('#supplier').prop('disabled',true);
        });

    </script>
    {{------------------------------------------------------------------------------------}}


    <!--delete row in table -->
    <script>

        $(document).on('click', '.delete_row', function () {

            $(this).hide(function(){
                $(this).closest('td').append('<button class="btn btn-danger set_all_disabled" type="button" id="yes" style="display: none;">yes</button>' +
                    '<button class="btn btn-default set_all_disabled" type="button" id="no" style="display: none;">no</button>');
                   $(this).closest('td').find("#yes").show('slow').delay(1000);
                   $(this).closest('td').find("#no").show('slow').delay(1000);

            });
        });

        $(document).on('click','#yes',function(){

            var rowCount = $('#op_table tbody tr:not(.added_now)').length;

            $(this).closest('tr').remove();

            if($('.added_now').length == 0)
            {
                $("#add_row").removeAttr("disabled");
            }
            if(rowCount == 0)
            {
                $('#supplier').prop('disabled',false);
            }
        });

        $(document).on('click','#no',function(){

            console.log('noo ');
            console.log('quantity : ' + $(this).parents('tr').find('.quantity_value_v').attr('current_value') );
            console.log('pro _id  : ' + $(this).parents('tr').find('.item_product_v').attr('current_value') );


            $(this).closest('td').find(".delete_row").show('slow').delay(1000);
            $(this).closest('td').find("#yes").hide('slow').delay(1000).remove();
            $(this).closest('td').find("#no").hide('slow').delay(1000).remove();
            /*
            $(this).closest('td').find("#yes").hide('slow').delay(1000);
            $(this).closest('td').find("#no").hide('slow').delay(1000);*/


        });


    </script>
    {{------------------------------------------------}}
    {{-----------------   save all data    --------------------------------}}

    <script>

        $('.alert-autocloseable-success').hide();


        $(document).on('click','#save_po',function () {
            var status_po = $('#set_po_id').attr('current_po_status');
            add_new_po_all (status_po);

        });
        $(document).on('click','#sendto_approved_btn',function () {

            //add_new_po_all ('to_approve_po');
            var status_po = $('#set_po_id').attr('current_po_status');
            if(status_po != 'approved_po')
            {
                add_new_po_all ('to_approve_po');
            }

            else{
                add_new_po_all ('approved_po');
            }
        });
        $(document).on('click','#approved_btn',function () {
            //$('#set_po_id').attr('current_po_status','approved_po');
            add_new_po_all ('approved_po');


        });



        function add_new_po_all (status_click)
        {
            var check_table = 1 ;
            var inputs_valid = $('.get_valide_for');
            inputs_valid.each(function(){
                if($(this).val() == '' || !$(this).val()) {
                    $(this)
                    $(this).css({
                        "border": "1px solid red",
                    });
                    $(this).siblings('.select2').css({
                        "border": "1px solid red",
                    });
                    check_table = 0;
                    notification('glyphicon glyphicon-warning-sign','Warning!','You Should Fill All Fields!','danger')
                    $(".errorMessage2").hide();
                }
                else{
                    $(this).css({'border' : ""});
                    $(this).siblings('.select2').css({'border' : ""});
                }
            });

            var rowCount = $('#op_table tbody tr:not(.added_now)').length;


            if (rowCount == 0 && check_table == 1) {


                $(".errorMessage1").hide();
                notification('glyphicon glyphicon-warning-sign','Warning','Please Fill Table!','danger')

            }


            if(!$('#op_table tr').hasClass('edited_now') && rowCount > 0 && !$('#op_table tr').hasClass('added_now') && check_table == 1)
            {

                $('#save_po').attr('disabled','disabled');
                $('#supplier').attr('disabled','disabled');
                $('#sendto_approved_btn').attr('disabled','disabled');
                $('#approved_btn').attr('disabled','disabled');


                var status = status_click;
                console.log(status);

                var supplier = $('#supplier').val();
                var doc_type = $('#doc_type').val();

                var doc_date = $('#datetime1').val();
                var doc_note = $('#po_comment').val();

                var current_user_id= $('#set_po_id').attr('current_id');
                var current_po_id = $('#set_po_id').attr('current_po_id');



                //var canceled_po = $('#set_po_id').attr('closed_or_not');



                var input = $('#op_table > tbody  > tr');
                var row = [];

                input.each(function(){

                    if(!$(this).hasClass('added_now'))
                    {
                        row.push({"po_id": $(this).find('.po_values_v').attr('current_value'),"item_product": $(this).find('.item_product_v').attr('current_value'),"quantity": $(this).find('.quantity_value_v').attr('current_value'),"closed": $(this).find('.include_check_v').attr('current_value')});
                    }


                });

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/add_new_inventory/',
                    dataType : 'json',
                    type: 'get',
                    data: {

                        supplier:supplier,
                        document_type:doc_type,
                        document_date:doc_date,
                        document_note:doc_note,
                        status:status,
                        current_po_id:current_po_id,
                        table_rows:row,
                        cur_user_id:current_user_id,

                    },

                    success:function(response) {

                        console.log(response);
                        if(response['error'])
                        {
                            console.log('in error ');
                            $('.err_input').removeClass('red-border');
                            $('.err_input').parent().find('.select2').removeClass('red-border');
                            notification('glyphicon glyphicon-warning-sign','Warning','You Should Fill All Fields!','danger')
                            $.each(response['error'], function (key, val)
                            {
                                //alert(key + val);

                                console.log(key+'_error');
                                $("."+key+'_error').addClass('red-border');
                                $('.'+key+'_error').parent().find('.select2').addClass('red-border');
                                //console.log(val[0]);

                            })
                            $('#save_po').removeAttr('disabled');
                            $('#supplier').removeAttr('disabled');

                            $('#sendto_approved_btn').removeAttr('disabled');
                            $('#approved_btn').removeAttr('disabled');
                        }

                        else if(response['errors'])
                        {
                            console.log('in errors ');
                            notification('glyphicon glyphicon-warning-sign','Warning','You Should Fill All Fields!','danger')
                            $('.errorMessage1').text('Review  quantities')
                            var all_table_tbody = $('#op_table tbody tr');
                            var i=0;
                            $('.select_all_remove_error').removeClass('error');
                            all_table_tbody.each(function () {
                                if(response['table_error_array'][i] == 0)
                                {
                                    $(this).find('.quantity_value_v').addClass('error');
                                }
                                i++;
                            });
                            /*for(var i=0; i<response['table_error_array'].length;i++)
                            {

                            }*/
                            $('#save_po').removeAttr('disabled');
                            $('#supplier').removeAttr('disabled');

                            $('#sendto_approved_btn').removeAttr('disabled');
                            $('#approved_btn').removeAttr('disabled');
                        }

                        else
                        {

                            $('#second').text(response['po_seq']);

                            console.log('in succcess ');
                            $('.select_all_remove_error').removeClass('error');
                            $('.errorMessage1').hide();

                            $('.err_input').removeClass('red-border');
                            $('.err_input').parent().find('.select2').removeClass('red-border');

                            var status_po = $('#set_po_id').attr('current_po_status');
                            if(status_po != 'approved_po')
                            {
                                $('#set_po_id').attr('current_po_status',status_click);

                            }

                            $('#op_table tr.added_now').remove();
                            $('.breadcrumb').removeClass('po-active');
                            $('#'+response['status']).addClass('po-active');
                            $('#current_choosed_status').attr('coosed_status',response['status']);
                            $('#save_po').text("Edit");
                            $('#save_po').attr('id','edit_po');
                            $('#set_po_id').attr('current_po_id',response['current_po_id']);
                            $('.set_all_disabled').prop('disabled',true);
                            $('#creat_po').removeClass('hide');

                            $('#po_more').removeClass('hide');

                            if($('#set_po_id').attr('current_po_status') == 'draft_po')
                            {
                                $('#delete_po').parent('li').removeClass('hide');
                                $('#second').val('Draft');
                            }

                            else
                            {
                                $('#delete_po').parent('li').addClass('hide');
                                /*$('#closed_po').removeClass('hide');
                                $('#close_po_btn').removeClass('hide');*/
                                $('#create_invoice').removeClass('hide');
                            }

                            $('#edit_po').removeAttr('disabled');
                            //po-active
                            console.log(response);

                            notification('glyphicon glyphicon-ok-sign','Congratulations!','Inventory Saved Succefully.','success')

                        }

                    }

                });

            }
            else{
                $('#save_po').removeAttr('disabled');
                $('#sendto_approved_btn').removeAttr('disabled');
                $('#approved_btn').removeAttr('disabled');
            }

        }
    </script>

    {{--------------------------  edit inventory  --------------------------------}}
    <script>
        $(document).on('click','#edit_po',function () {
            $('#edit_po').text("Save");
            $('#edit_po').attr('id','save_po');
            if($('#set_po_id').attr('current_po_status') != 'approved_po' && $('#set_po_id').attr('current_po_status') != 'invoiced')
            {
                $('.set_all_disabled').prop('disabled',false);
                var rowCount = $('#op_table tr').length;
                console.log('rowCount'+ rowCount)
                /*if(rowCount == 1)
                {
                    $('#supplier').prop('disabled',false);
                    $('#add_row').prop('disabled',false);

                }
                else
                {
                    $('#supplier').prop('disabled',true);
                    $('#add_row').prop('disabled',false);
                }*/

            }
           // $('#close_po_btn').prop('disabled',false);
            /*if($('#set_po_id').attr('current_po_status') == 'draft_po')
            {
                $('#close_po_btn').prop('disabled',true);
            }*/

            console.log($('#set_po_id').attr('current_po_id'));
            $('#creat_po').addClass('hide');

            $('#delete_po').parent('li').addClass('hide');

            $('#po_more').addClass('hide');

            $('#create_invoice').addClass('hide');
        });
    </script>
    {{------------------------   create new inventory -----------------------------------------------}}
    <script>
        $(document).on('click','#creat_po',function () {/*
            $('#edit_po').prop('disabled',false);
            $('#edit_po').text("Save");
            $('#edit_po').attr('id','save_po');
            $('#save_po').prop('disabled',false);

            $('.set_all_disabled').prop('disabled',false);
            $('#supplier').prop('disabled',false);
            $('#add_row').prop('disabled',true);
            var crrent_date = $('#datetime1').attr('crrent_date');
            $('#datetime1').val(crrent_date);

            $('#set_po_id').attr('current_po_id',"");
            $('#set_po_id').attr('current_po_status',"draft_po");
            $('#creat_po').addClass('hide');
            $('#po_more').addClass('hide');
            $('#create_invoice').addClass('hide');

            $('#delete_po').parent('li').addClass('hide');

            /!*$(".set_all_disabled").val("");*!/
            $('.get_all_selectors').val('');
            //$('.get_all_date_input').val('');

            $(".get_all_selectors option[value='']").attr('selected', true);
            $('.get_all_date_input').val('');
            $('.select2-selection__rendered').html('');

            $('#op_table tbody tr').remove();
            $('.breadcrumb').removeClass('po-active');
            $('#total_quantity').html('0.00');

            $('#po_comment').val('');*/
            window.location.href = '/inventory';
        });
    </script>
    {{---- dublicate po ---}}
    <script>
        $(document).on('click','#dublicate_po',function(){

            $('#set_po_id').attr('current_po_id',"");
            $('#set_po_id').attr('current_po_status',"draft_po");
            $('#set_po_id').attr('closed_or_not',0);

            $('.set_all_disabled').prop('disabled',false);


            $('#creat_po').addClass('hide');
            $('#po_more').addClass('hide');
            $('#create_invoice').addClass('hide');

            $('.breadcrumb').removeClass('po-active');

            $('#edit_po').prop('disabled',false);
            $('#edit_po').text("Save");
            $('#edit_po').attr('id','save_po');
            $('#save_po').prop('disabled',false);
        });
    </script>

    {{----------------------  delete inventory ------------------------------------}}

    <script>
        $(document).on('click','#delete_po',function(){
            var cur_po_id = $('#set_po_id').attr('current_po_id');

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/delete_this_inventory/',
                dataType : 'json',
                type: 'get',
                data: {
                    cur_po_id:cur_po_id,
                },

                success:function(response) {

                    window.location.href = '/inventory';
                }

            });

        });
    </script>
    {{---------------------------  create invoice ----------------------------------}}
    <script>
        $(document).on('click','#create_invoice',function(){
            var cur_po_id = $('#set_po_id').attr('current_po_id');


                    window.location.href = '/create_invoice/'+cur_po_id;
        });
    </script>


    {{------------------  refresh suplier dropdown ----------------------------}}
    <script>

        function supplier_refresh()
        {

            var $dropdown = $("#supplier");
            $dropdown.prop('disabled',true);
            $dropdown.select2('destroy');
            var selected_value = $dropdown.val();

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/refresh_supplier/',
                dataType : 'json',
                type: 'get',
                data: {

                },

                success:function(response) {

                    $dropdown.find('option').remove();
                    $dropdown.append('<option value=""></option>');

                    $.each(response, function(key, value){

                        $dropdown.append($("<option />").val(key).text(value));

                    });
                    $dropdown.val(selected_value);
                    $dropdown.select2();
                    $dropdown.prop('disabled',false);
                }

            });


        }

        $(document).on('dblclick','.select2-selection',function () {
            var select_check = $(this).parents('.select2').siblings('select');


            if ($('#save_po').length && select_check.attr('id') == 'supplier') {
                supplier_refresh();
            }
        });
    </script>

    {{-------------------------------------}}

    <script>
        $(document).ready(function () {
            get_totals();
            $(".select_2_enable").select2();

            if($('#set_po_id').attr('current_po_status') == 'approved_po' || $('#set_po_id').attr('current_po_status') == 'invoiced')
            {
                $('.set_all_disabled').prop('disabled',true);
            }
            if($('#supplier').val() != "" && $('#set_po_id').attr('current_po_status') == '')
            {
                enable_disable_add();
            }
        });
    </script>

    {{-----------   on leave page -------------------}}
    <script type='text/javascript'>

        $(window).bind('beforeunload', function(){
            if($('#save_po').length){
                return 'Are you sure you want to leave?';
            }

        });


    </script>



    <script>

        $(document).on('click', '#print', function () {


            //  window.location.href = 'get_po2/276'

            var id = $('#set_po_id').attr('current_po_id');
            window.open('/get_inventory_rpt/' + id + '', '_blank');


        });

    </script>



@endsection