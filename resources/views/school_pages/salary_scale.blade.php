@extends('layouts.main_app')

@section('main_content')
    <link href="{{asset('css/fastselect.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/select2.min.css')}}" rel="stylesheet" />
    <style>
        input.col-md-4.text-center {

            border:0;
            background-color: #fff;
        }

        input[type=number]{
            width: 80px;
        }

        .collapse > .row>.col-md-4>.btn, .collapse > .row>.col-md-4>.dropdown>.btn {
            margin-top: 0;

        }



        .third-nav{
            background-color:#eeeeee;
        }


        .panel .table > thead > tr > td:first-child, .panel .table > thead > tr > th:first-child, .panel .table > tbody > tr > td:first-child, .panel .table > tbody > tr > th:first-child, .panel .table > tfoot > tr > td:first-child, .panel .table > tfoot > tr > th:first-child{
            padding-left: 0px;
        }


        .myselect2{
            width:171px;
        }

        span.select2.select2-container.select2-container--default{
            width: 100%!important;
        }

        .error{
            border: 1px solid red  !important;
            border-left: 1px double red !important;
            border-top: 2px double red !important;
        }


        .red-border{
            border: 1px solid red  !important;
        }

    </style>
    <!-- failuer messages -->
    <div class="alert alert-danger errorMessage1" style="position: absolute;">
        Your should fill all fields!

    </div>    <div class="main">
        <!-- Navbar-content -->
        <nav class="navbar navbar-default navbar2">

            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">


                    <!--breadcrumbs-->
                    <div class="row">

                        <div class="col-md-10 col-lg-10">
                        <ol class="breadcrumb breadcrumb_nav">
                            <li><a href="#">Salary Scale</a></li>
                            <li class="active">
                                @if(isset($department))
                                    @if($department->salary_id != 0)
                                        {{$department->salary_id}}
                                    @else
                                        Draft
                                    @endif
                                @endif


                            </li>
                        </ol>
                        </div>



                        @if(isset($department))
                            <div class="col-md-2 col-lg-2 col-sm-4 col-xs-4">
                                @if(@\App\SalaryScale::where('salary_id','<',$department->salary_id)->orderBy('salary_id','desc')->first()->salary_id)
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <a href="/salary_scal/{{@\App\SalaryScale::where('salary_id','<',$department->salary_id)->orderBy('salary_id','desc')->first()->salary_id}}"><span
                                                    class="glyphicon glyphicon-arrow-left"></span>
                                        </a>
                                    </div>
                                @endif
                                @if(@\App\SalaryScale::where('salary_id','>',$department->salary_id)->orderBy('salary_id','asc')->first()->salary_id)
                                    <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                                        <a href="/salary_scal/{{@\App\SalaryScale::where('salary_id','>',$department->salary_id)->orderBy('salary_id','asc')->first()->salary_id}}">
                                    <span class="glyphicon glyphicon-arrow-right">
                                </span>
                                        </a>
                                    </div>
                                @endif
                            </div>
                        @endif
                    </div>   <!--End breadcrumbs-->





                    @if(isset($department))
                        <div id="set_po_id" current_po_id="{{$department->salary_id}}" current_po_status=""
                             closed_or_not="" current_id="{{Auth::user()->id}}"></div>
                    @else
                        <div id="set_po_id" current_po_id="" current_po_status="draft_po" closed_or_not="0"
                             current_id="{{Auth::user()->id}}"></div>
                    @endif
                    <div class="row">
                        <div class="col-md-4 col-lg-4 buttons">
                            @if(isset($department))
                                <button id="edit_dep" type="button" class="btn btn-danger ">Edit</button>
                                <button type="button" class="btn" id="creat_dep">Create</button>
                            @else
                                <button id="save_dep" type="button" class="btn btn-danger ">Save</button>
                                {{--<button id="edit_user" type="button" class="btn btn-danger " >Edit</button>--}}
                                <button type="button" class="hide btn" id="creat_dep">Create</button>
                            @endif
                        </div>

                        <div class="col-md-4">
                            @if(isset($department))
                                <div class="dropdown">
                                    <button class=" btn btn-primary  dropdown-toggle" type="button"
                                            data-toggle="dropdown" id="more" hidden>More
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" id="delete_dep">Delete</a></li>
                                        {{--<li><a href="#">Export</a></li>--}}
                                        <li><a href="#" id="dublicate_dep">Duplicate</a></li>
                                        <li><a href="#" id="print">Print</a></li>

                                    </ul>
                                </div>
                            @else
                                <div class="dropdown">
                                    <button class="hide btn btn-primary  dropdown-toggle" type="button"
                                            data-toggle="dropdown" id="more" hidden>More
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" id="delete_dep">Delete</a></li>
                                        <li><a href="#">Export</a></li>
                                        <li><a href="#" id="dublicate_dep">Duplicate</a></li>
                                        <li><a href="#" id="print">Print</a></li>

                                    </ul>
                                </div>
                            @endif
                        </div>
                        <!--search-->
                        <!--
                         <div class="col-md-4">
                             <form class="navbar-form navbar-right" >
                                 <div class="input-group form-group">
                                     <input type="text" value="" class="form-control" placeholder="Search ...">
                                 </div>
                             </form>
                         </div>
                         -->
                    </div> <!--collapse -->

                </div><!--collapse -->

            </div>
        </nav>
        <!-- End Navbar-content -->

        <!-- Success messages -->
        <div class="alert alert-success alert-autocloseable-success" style="display:none;position:absolute;">
            Salary Scale Saved Successfully!

        </div>

        <!--end Success messages -->


        <!-- failuer messages -->
        <div class="alert alert-danger errorMessage1" style="display:none;position:absolute;">
            Your should fill all fields!

        </div>

        <div class="alert alert-danger errorMessage2" style="display:none;position:absolute;" >
            PLease fill Table!

        </div>
        <!--end failuer messages -->

        <!-- MAIN CONTENT -->
        <div class="main-content">

            <div class="container-fluid">

                <!-- INPUTS -->
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Salary Scale</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="usr" class="col-md-6 col-lg-6 control-label">Salary Scale Name </label>

                                    <div class=" col-md-6 col-lg-6">

                                        @if(isset($department))
                                            <input id="salary_scale_name" type="text"
                                                   class="get_valide_for set_all_disabled get_all_date_input form-control required err_input salary_name_error"
                                                   value="{{$department->salary_name}}" disabled>
                                        @else
                                            <input id="salary_scale_name" type="text"
                                                   class="get_valide_for set_all_disabled get_all_date_input form-control required err_input salary_name_error"
                                                   value="">
                                        @endif

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>

                        <div class="row">
                            <div class="col-md-12">


                                <div id="department_name_error" class="hide alert alert-danger errorMessage">The
                                    name field is required.
                                </div>
                            </div>

                        </div>

                        <div class="row">


                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="usr" class="col-md-6 col-lg-6 control-label">Short Code </label>

                                    <div class=" col-md-6 col-lg-6">

                                        @if(isset($department))
                                            <input id="short_code_name" type="text"
                                                   class="get_valide_for set_all_disabled get_all_date_input form-control required err_input code_name_error"
                                                   value="{{$department->code_name}}" disabled>
                                        @else
                                            <input id="short_code_name" type="text"
                                                   class="get_valide_for set_all_disabled get_all_date_input form-control required  err_input code_name_error"
                                                   value="">
                                        @endif

                                    </div>

                                </div>

                            </div>



                        </div>

                        <br>

                        <div class="row">
                            <div class="col-md-12">

                                <div id="password_error" class="alert alert-danger errorMessage hide ">
                                    <strong>Wrong code!</strong>
                                </div>

                            </div>

                        </div>

                        <div class="row">

                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="usr" class="col-md-6 col-lg-6 control-label">Cost Center</label>

                                    <div class=" col-md-6 col-lg-6">

                                        <select id="cost_id" name="cost_center"
                                                class="get_valide_for get_all_selectors set_all_disabled select_2_enable form-control err_input required cost_center_error"
                                                @if(isset($department)) disabled @endif >
                                            <option value=""></option>
                                            @foreach($cost_center as $id=>$role)
                                                @if(isset($department))
                                                    @if($department->cost_id == $id)
                                                        <option selected value="{{$id}}">{{$role}}</option>
                                                    @else
                                                        <option value="{{$id}}">{{$role}}</option>
                                                    @endif
                                                @else
                                                    <option value="{{$id}}">{{$role}}</option>
                                                @endif
                                            @endforeach
                                        </select>


                                    </div>

                                </div>

                            </div>


                        </div>


                        <br>

                        <div class="row">
                            <div class="col-md-12">
                                <div id="center_error" class="alert alert-danger errorMessage hide ">
                                    <strong>Wrong !</strong>
                                </div>

                            </div>

                        </div>

                        <br>

                        <div class="row">

                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#Sub_SalaryScale">Sub Salary Scale</a></li>


                            </ul>

                            <div class="tab-content">

                                <div id="Sub_SalaryScale" class="tab-pane fade in active">
                                    <br>

                                    <div class="row" >
                                        <table  class="table table-bordred table-striped table-bordered table-striped table-hover table-condensed table-responsive " id="op_table" >
                                            <thead>
                                            <th>Sub Salary Name</th>

                                            <th>Short code</th>

                                            <th>Edit</th>
                                            <th>Delete</th>
                                            </thead>
                                            <tbody>
                                            @if(isset($form))
                                            @foreach($form as $documents)
                                                    <tr class="">
                                                        <td td_type="input" class="sub_name" id="sub_name" current_value="{{$documents->sub_name}}">{{$documents->sub_name}}</td>
                                                        <td td_type="input" class="sub_code" id= "sub_code" current_value="{{$documents->sub_code}}">{{$documents->sub_code}}</td>
                                                        <td><button class="set_all_disabled btn btn-primary btn-xs edit_row" disabled>Edit</button></td>
                                                        <td><button class="set_all_disabled btn btn-danger btn-xs delete_row" disabled>
                                                                <span class="lnr lnr-trash"></span>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                        <div class="row">
                                            <div class="col-md-4 col-lg-4 ">
                                                <button type="button" class=" set_all_disabled btn btn-primary edit_row"   id="add_row" @if(isset($department)) disabled @endif >Add Item</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>









                            </div>
                        </div>




                    </div>
                    <br>
                </div>
            </div>

        </div>
        <!-- END INPUTS -->


    </div>

@endsection


@section('custom_footer')
    <script src="{{asset('js/item_name_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/tax_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/select2.min.js')}}"></script>

    <script>
        $('.side_sheets').addClass('hide');
        $('#HR').removeClass('hide');

        $("#menu li").removeClass('active');
        $('#hr_nav_bar').addClass('active');

        $('#pages ul li a').removeClass('active');
        $('#salary_scale_side_bar').addClass('active');


    </script>
    {{-------------------- save new department --------------------}}
    <script>
        $('.alert-autocloseable-success').hide();

        $(document).on('click', '#save_dep', function () {

            var check_table = 1;
            var inputs_valid = $('.get_valide_for');
            inputs_valid.each(function () {
                if ($(this).val() == '' || !$(this).val()) {
                    $(this)
                    $(this).css({
                        "border": "1px solid red",
                    });
                    $(this).siblings('.select2').css({
                        "border": "1px solid red",
                    });
                    check_table = 0;


                    notification('glyphicon glyphicon-warning-sign','Warning','You Should Fill All Fields!','danger')



                    $(".errorMessage2").hide();
                }
                else {
                    $(this).css({'border': ""});
                    $(this).siblings('.select2').css({'border': ""});
                }
            });

            var rowCount = $('#op_table tbody tr:not(.added_now)').length;


            if (rowCount == 0 && check_table == 1) {


                $(".errorMessage1").hide();



                notification('glyphicon glyphicon-warning-sign','Warning','Please Fill Table.','danger')



            }


            if (!$('#op_table tr').hasClass('edited_now') && rowCount > 0 && !$('#op_table tr').hasClass('added_now') && check_table == 1) {

            $('#save_dep').attr('disabled', 'disabled');

            $('.set_all_disabled').prop('disabled', true);


            var salary_name = $('#salary_scale_name').val();
            var code_name = $('#short_code_name').val();
            var cost_id = $('#cost_id').val();
            var sub_name = $('#sub_name').val();
            var sub_code = $('#sub_code').val();

            var current_user_id = $('#set_po_id').attr('current_id');
            var current_po_id = $('#set_po_id').attr('current_po_id');
            var row = [];
            var input = $('#op_table > tbody  > tr');

            input.each(function () {

                if (!$(this).hasClass('added_now')) {
                    row.push({
                        "sub_name": $(this).find('.sub_name_added').attr('current_value'),
                        "sub_code": $(this).find('.sub_code_added').attr('current_value')

                    });
                }
                console.log(row);

            });
            console.log('current_po_id' + current_po_id)
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/salary_save/',
                dataType: 'json',
                type: 'get',
                data: {
                    salary_name: salary_name,
                    code_name: code_name,
                    cost_center: cost_id,

                    table_rows: row,
                    current_po_id: current_po_id,
                    cur_user_id: current_user_id,

                },

                success: function (response) {
                    $(".errorMessage1").hide();
                    $(".errorMessage2").hide();
                    console.log(response);
                    if (response['error']) {
                        $('.err_input').removeClass('red-border');
                        $('.err_input').parent().find('.select2').removeClass('red-border');


                        notification('glyphicon glyphicon-warning-sign','Warning','Cost Center is Required.','danger')



                        $.each(response['error'], function (key, val) {
                            //alert(key + val);

                            console.log(key + '_error');
                            $("." + key + '_error').addClass('red-border');
                            $('.' + key + '_error').parent().find('.select2').addClass('red-border');
                            //console.log(val[0]);

                        })
                        $('#save_dep').removeAttr('disabled');
                        $('.set_all_disabled').prop('disabled', false);

                    }

                    else {
                        $('#set_po_id').attr('current_po_id', response['current_po_id']);
                        $('.errorMessage1').hide();
                        $('.err_input').removeClass('red-border');
                        $('.err_input').parent().find('.select2').removeClass('red-border');
                        $('.breadcrumb').removeClass('po-active');


                        $('#save_dep').text("Edit");
                        $('#save_dep').attr('id', 'edit_dep');

                        $('.set_all_disabled').prop('disabled', true);

                        $('#creat_dep').removeClass('hide');

                        $('#more').removeClass('hide');


                        $('#edit_dep').removeAttr('disabled');
                        //po-active
                        console.log(response);



                        notification('glyphicon glyphicon-ok-sign','Congratulation!','Salary Scale Saved Successfully!','success')

                    }

                }

            });
        }
        });

    </script>
    {{----------------------- edit department -----------------------}}

    <script>
        $(document).on('click', '#edit_dep', function () {
            $('#edit_dep').text("Save");
            $('#edit_dep').attr('id', 'save_dep');
            $('#add_row').prop('disabled', false);

            $('.set_all_disabled').prop('disabled', false);


            $('#creat_dep').addClass('hide');

            $('#more').addClass('hide');

        });
    </script>
    {{---------------------------- create new  ------------------------------}}
    <script>
        $(document).on('click', '#creat_dep', function () {
            $("#add_row").prop("disabled",false)
            /*
            $('#edit_dep').prop('disabled', false);
            $('#edit_dep').text("Save");
            $('#edit_dep').attr('id', 'save_dep');
            $('#save_dep').prop('disabled', false);

            $('.set_all_disabled').prop('disabled', false);

            $('#set_po_id').attr('current_po_id', "");
            /!*$('#set_po_id').attr('current_po_status',"draft_po");*!/
            $('#creat_dep').addClass('hide');
            $('#more').addClass('hide');

            $('.get_all_selectors').val('');

            $(".get_all_selectors option[value='']").attr('selected', true);
            $('.get_all_date_input').val('');
            $('.select2-selection__rendered').html('');
*/
            window.location.href = '/salary_scale';
        });
    </script>

    {{---- dublicate dep ---}}
    <script>
        $(document).on('click', '#dublicate_dep', function () {

            $('#set_po_id').attr('current_po_id', "");
            /*$('#set_po_id').attr('current_po_status',"draft_po");*/
            /*$('#set_po_id').attr('closed_or_not',0);*/

            $('.set_all_disabled').prop('disabled', false);


            $('#creat_dep').addClass('hide');
            $('#more').addClass('hide');


            $('#edit_dep').prop('disabled', false);
            $('#edit_dep').text("Save");
            $('#edit_dep').attr('id', 'save_dep');
            $('#save_dep').prop('disabled', false);
        });
    </script>
    <!--End add row to table -->


    {{----------------------  delete department ------------------------------------}}

    <script>
        $(document).on('click', '#delete_dep', function () {
            var cur_po_id = $('#set_po_id').attr('current_po_id');

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/delete_salary_scale/',
                dataType: 'json',
                type: 'get',
                data: {
                    cur_po_id: cur_po_id,
                },

                success: function (response) {

                    window.location.href = '/salary_scale';
                }

            });

        });
    </script>


    {{-----------   on leave page -------------------}}
    <script type='text/javascript'>

        $(window).bind('beforeunload', function(){
            if($('#save_dep').length){
                return 'Are you sure you want to leave?';
            }

        });


    </script>




    <!--select salary scale -->

    <script>
        $(".select_2_enable").select2({
            width: 'resolve'

        });

        $(".myselect2").select2({
            width: 'resolve',


        });

    </script>
    <!-------------------------add row to table --------------------------------->
    <script>
        $(document).on('click','#add_row',function () {
            // var path = $('#file_path').attr('path_file');
            $(this).attr('disabled',true);

            var row = '<tr class= "added_now" >'
                + '<td td_type="input"><input class="sub_name_added row_data form-control" data-role="input" type="text"/></td>'
                + '<td td_type="input"><input class="sub_code_added row_data form-control" data-role="input" type="text"/></td>'


                +'<td>'+'<button class="set_all_disabled btn btn-primary btn-xs Save_row">'+'Save'+'</button>'
                +'</td>'
                +'<td>'
                +'<button class="set_all_disabled btn btn-danger btn-xs delete_row"  >'+'<span class="lnr lnr-trash"></span>'+'</button>'
                /*+ '<button class="btn btn-danger"  type="button" id="yes">'+'yes'+'</button>' + '<button class="btn btn-default" type="button" id="no">'+'no'+'</button>'*/

                +'</td>'
                +'</tr>';
            $("#op_table").prepend(row);
        });


    </script>
    {{---------------------  save row --------------------------------}}
    <script>
        $(document).on("click", ".Save_row", function(){



            var empty = false;
            var input = $(this).parents("tr").find('input[type="text"]');

            input.each(function(){
                if(!$(this).val()|| $(this).val()==""){
                    ///$(this).addClass("alert alert-danger errorMessage1");
                    $(this).addClass("error");
                    $(".Save_row").prop("disabled",false);

                    empty = true;
                } else{
                    $(this).removeClass("error");
                }
            });

            $(this).parents("tr").find(".error").first().focus();

            if(!empty ) {
                $(this).prop('disabled',true);
                save_table_effect($(this));
            }


        });

        function save_table_effect(thiss) {
            console.log('passed')

            var input_array = ['sub_name_added','sub_code_added'];
            var r=0;

            var all_input = thiss.parents("tr").find('input[type="text"]');

            all_input.each(function () {
                var value_select = $(this).val();
                $(this).parent("td").addClass(input_array[r]).attr('current_value', value_select).html(value_select);
                r++;

            });

            if (thiss.parents("tr").hasClass('added_now')) {
                thiss.parents("tr").removeClass('added_now');

            }
            if (thiss.parents("tr").hasClass('edited_now')) {
                thiss.parents("tr").removeClass('edited_now');

            }



            if ($('.added_now').length == 0) {
                $("#add_row").removeAttr("disabled");
            }

            thiss.text('Edit');
            thiss.removeClass('Save_row');
            thiss.addClass('edit_row');
            thiss.prop('disabled',false);
        }
    </script>
    {{----------------------  edit row --------------------}}
    <script>
        $(document).on("click", ".edit_row", function(){
            if(!$('#op_table tr').hasClass('added_now') && !$('#op_table tr').hasClass('edited_now'))
            {



                $(this).parents("tr").addClass('edited_now');

                $(this).parents("tr").find("td:not(:last-child)").each(function(){
                    if($(this).attr('td_type') == 'input')
                    {
                        //maxmum_quan

                        $(this).html('<input class="quantity_value row_data form-control"  + " data-role="input" type="text"  value="'+$(this).text()+'" />');



                    }

                    else if ($(this).attr('td_type') == 'search')
                    {
                        if($(this).hasClass('po_values_v'))
                        {
                            $(this).html('<select class="po_values form-control js-example-disabled-results">\n' +
                                '</select>');
                            get_item_type_drop_down($(this).find('.po_values'));
                            var selected_v = $(this).attr('current_value');
                            $(this).find(".po_values").val(selected_v);
                            $(".po_values").select2();
                            $(".po_values").siblings('.select2').css('width', '71px');
                        }

                        else if($(this).hasClass('item_product_v'))
                        {

                            $(this).html('<select class="item_product form-control js-example-disabled-results">\n' +
                                '</select>');
                            var po_id = $(this).parents('tr').find('.po_values_v').attr('current_value');
                            var now_selected = $(this).parents('tr').find('.item_product_v').attr('now_selected');

                            get_item_product($(this).find('.item_product'),po_id,now_selected);
                            var selected_v = $(this).attr('current_value');
                            $(this).find(".item_product").val(selected_v);
                            $(".item_product").select2();
                            $(".item_product").siblings('.select2').css('width', '71px');
                        }

                    }
                    else if ($(this).attr('td_type') == 'check_box')
                    {
                        $(this).html('<div class="material-switch pull-right">'+
                            '<input id="'+ $(this).attr('row_id')+'" class="include_check" name="someSwitchOption001" type="checkbox"/>'+
                            '<label for="'+$(this).attr('row_id')+'" class="label-primary"></label>'+
                            '</div>');
                        if($(this).attr('current_value')==1)
                        {
                            $(this).find('.include_check').prop('checked', false);
                        }
                        else
                        {
                            $(this).find('.include_check').prop('checked', false);
                        }
                    }

                });

                $("#add_row").attr("disabled", "disabled");
                $(this).text('Save');
                $(this).removeClass('edit_row');
                $(this).addClass('Save_row');
                // get_totals();
            }
        });

    </script>

    <!--delete row in table -->
    <script>

        $(document).on('click', '.delete_row', function () {

            $(this).hide(function(){
                $(this).closest('td').append('<button class="btn btn-danger" type="button" id="yes" style="display: none;">yes</button>' +
                    '<button class="btn btn-default" type="button" id="no" style="display: none;">no</button>');
                $(this).closest('td').find("#yes").show('slow').delay(1000);
                $(this).closest('td').find("#no").show('slow').delay(1000);

            });
        });

        $(document).on('click','#yes',function(){

            $(this).closest('tr').remove();
            // get_totals();
            if($('.added_now').length == 0)
            {
                $("#add_row").removeAttr("disabled");
            }
        });

        $(document).on('click','#no',function(){

            console.log('noo ');

            $(this).closest('td').find(".delete_row").show('slow').delay(1000);
            $(this).closest('td').find("#yes").hide('slow').delay(1000).remove();
            $(this).closest('td').find("#no").hide('slow').delay(1000).remove();

        });


    </script>


    <!--End add row to table -->



    <!------------print--------------->
    <script>

        $(document).on('click', '#print', function () {

            //  window.location.href = 'get_po2/276'

            var id = $('#set_po_id').attr('current_po_id');
            window.open('/get_salary_scale_rpt/' + id + '', '_blank');


        });

    </script>


@endsection