@extends('layouts.main_app')

@section('main_content')
    <link href="{{asset('css/fastselect.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/select2.min.css')}}" rel="stylesheet" />

    <style>


        input.col-md-4.text-center {

            border:0;
            background-color: #fff;
        }



        .collapse > .row>.col-md-4>.btn, .collapse > .row>.col-md-4>.dropdown>.btn {
            margin-top: 0;

        }



        .third-nav{
            background-color:#eeeeee;
        }

        button#more
        {
            margin-bottom: 16px;
        }



        .myselect2{
            width:171px;
        }

        span.select2.select2-container.select2-container--default{
            width: 100%!important;
        }
        tbody th, tbody td {
            text-align: center;
        }


    </style>
    <div class="main">
        <!-- Navbar-content -->
        <nav class="navbar navbar-default navbar2">

            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                    <!--breadcrumbs-->
                    <div class="row">

                        <div class="col-md-10 col-lg-10">

                        <ol class="breadcrumb breadcrumb_nav">
                            <li><a href="/salaries_plan_table">Sub Salary Scale</a></li>
                            <li class="active">
                                @if(isset($journal))
                                    @if($journal->journal_id != 0)
                                        {{$journal->journal_id}}
                                    @else
                                        Draft
                                    @endif
                                @endif


                            </li>
                        </ol>
                        </div>

                        @if(isset($journal))
                            <div class="col-md-2 col-lg-2 col-sm-4 col-xs-4">
                                @if(@\App\Journal::where('journal_id','<',$journal->journal_id)->orderBy('journal_id','desc')->first()->journal_id)
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <a href="/journals/{{@\App\Journal::where('journal_id','<',$journal->journal_id)->orderBy('journal_id','desc')->first()->journal_id}}"><span
                                                    class="glyphicon glyphicon-arrow-left"></span>
                                        </a>
                                    </div>
                                @endif
                                @if(@\App\Journal::where('journal_id','>',$journal->journal_id)->orderBy('journal_id','asc')->first()->journal_id)
                                    <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                                        <a href="/journals/{{@\App\Journal::where('journal_id','>',$journal->journal_id)->orderBy('journal_id','asc')->first()->journal_id}}">
                                    <span class="glyphicon glyphicon-arrow-right">
                                </span>
                                        </a>
                                    </div>
                                @endif
                            </div>
                        @endif

                    </div>


                    <!--End breadcrumbs-->
                    <div class="row">
                        <div class="col-lg-4  col-md-4 col-sm-6 col-xs-6 buttons">
                            @if(isset($journal))
                                <button id="edit_po" type="button" class="btn btn-danger ">Edit</button>
                                <button type="button" class="btn" id="creat_dep">Create</button>
                            @else
                                {{--<button id="save_po" type="button" class="btn btn-danger ">Save</button>--}}
                                {{--<button id="edit_user" type="button" class="btn btn-danger " >Edit</button>--}}
                                <button type="button" class="hide btn" id="creat_dep">Create</button>
                            @endif
                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-4 ">
                            @if(isset($journal))
                                <div class="dropdown">
                                    <button class=" btn btn-primary  dropdown-toggle" type="button"
                                            data-toggle="dropdown" id="more" >More
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li class="  " data-toggle="modal" data-target="#exampleModal"><a href="#" id="delete_btn">Delete</a></li>
                                        <li><a href="#">Export</a></li>
                                        {{--<li><a href="#" id="dublicate_dep">Duplicate</a></li>--}}
                                        <li><a href="#" id="print">Print</a></li>

                                    </ul>
                                </div>
                            @else
                                <div class="dropdown">
                                    <button class=" btn btn-primary  dropdown-toggle" type="button"
                                            data-toggle="dropdown" id="more" >More
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li class="  " data-toggle="modal" data-target="#exampleModal"><a href="#" id="delete_btn">Delete</a></li>
                                        <li><a href="#">Export</a></li>
                                        {{--<li><a href="#" id="dublicate_dep">Duplicate</a></li>--}}
                                        <li><a href="#" id="print">Print</a></li>

                                    </ul>
                                </div>
                            @endif
                        </div>
                    </div>


                </div><!--collapse -->

            </div>
        </nav>
        <!-- End Navbar-content -->
        <!-- MAIN CONTENT -->
        <div class="main-content">

            <div class="container-fluid">

                <!-- INPUTS -->
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Sub Salary Scale</h3>
                    </div>
                    <div class="panel-body">

                        @if(isset($journal))
                            <div id="set_po_id" current_po_id="{{$journal->journal_id}}" current_po_status="" closed_or_not=""
                                 current_id="{{Auth::user()->id}}"></div>
                        @else
                            <div id="set_po_id" current_po_id="" current_po_status="draft_po" closed_or_not="0"
                                 current_id="{{Auth::user()->id}}"></div>
                        @endif

                        <div class="row">

                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="usr" class="col-md-6 col-lg-6 control-label">Academic Year</label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <select id="acadimic_year" name="acadimic_year"
                                                    class="validate_tab required get_all_selectors set_all_disabled select_2_enable form-control department_error err_input"
                                                    >
                                                <option value=""></option>
                                                @foreach($acadimic_year as $id=>$role)
                                                    @if(isset($year_data))
                                                        @if($showen_year == $id)
                                                            <option selected value="{{$id}}">{{$role}}</option>
                                                        @else
                                                            <option value="{{$id}}">{{$role}}</option>
                                                        @endif
                                                    @else
                                                        <option value="{{$id}}">{{$role}}</option>
                                                    @endif
                                                @endforeach
                                            </select>

                                        </div>


                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>




                        <br>

                        <div class="row">

                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#Sub_SalaryScale">Sub Salary Scale</a></li>


                            </ul>

                            <div class="tab-content">

                                <div id="Sub_SalaryScale" class="tab-pane fade in active ">
                                    <br>
                                    <div class="row" >
                                        <table  class="table table-bordred table-striped table-bordered table-striped table-hover table-condensed table-responsive " id="op_table" >
                                            <thead>
                                            <th>Salary Scale</th>
                                            <th>Forigen Salary</th>
                                            <th>Sub Salary Scale Name</th>


                                            <th>Annual Salary</th>
                                            <th>Monthly Salary</th>
                                            <th>Currency</th>
                                            <th>Edit</th>
                                            <th>Delete</th>
                                            </thead>
                                            <tbody>
                                            @if(isset($year_data))
                                                @foreach($year_data as $data)
                                            <tr class="" id="{{$data->plan_id}}">
                                                <td td_type="search" data-select2-id="63" class="salary_scale_v" current_value="{{$data->salary_scale_id}}">{{$data->salary_name}}</td>

                                                <td td_type="check_box" current_value="{{$data->forigen_type}}"
                                                @if($data->forigen_type == 1)
                                                    yes_or_no="Yes" class="forigen_v">Yes</td>
                                                @else
                                                    yes_or_no="No" class="forigen_v">No</td>
                                                @endif
                                                <td td_type="search" class="sub_salary_scale_v" current_value="{{$data->sub_salary_id}}">{{$data->sub_name}}</td>
                                                <td td_type="input" class="annual_salary_v" current_value="{{$data->annual_salary}}">{{$data->annual_salary}}</td>
                                                <td td_type="input" class="monthly_salary_v" current_value="{{$data->monthly_salary}}">{{$data->monthly_salary}}</td>

                                                <td td_type="search" class="currency_value_v" current_value="{{$data->currency_id}}">{{$data->Currency_name}}</td>

                                                <td><button class="set_all_disabled btn btn-primary btn-xs edit_row">Edit</button></td>
                                                <td><button class="set_all_disabled btn btn-danger btn-xs delete_row">
                                                        <span class="lnr lnr-trash">
                                                </span></button>
                                                    </td></tr>
                                                @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                        <div class="row">
                                            <div class="col-md-4 col-lg-4 ">
                                                <button type="button" class="btn btn-primary" id="add_row" @if(isset($journal)) disabled  @endif >Add Item</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>





                            </div>
                        </div>




                    </div>
                    <br>
                </div>
            </div>

        </div>
        <!-- END INPUTS -->


    </div>

@endsection


@section('custom_footer')
    <script src="{{asset('js/item_name_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/tax_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/select2.min.js')}}"></script>

    <script>
        $('.side_sheets').addClass('hide');
        $('#HR').removeClass('hide');

        $("#menu li").removeClass('active');
        $('#hr_nav_bar').addClass('active');

        $('#pages ul li a').removeClass('active');
        $('#subsalary_scale_side_bar').addClass('active');

    </script>

    <script>
        $('#acadimic_year').select2();
    </script>

    {{------------  add new row  ------------------------}}
    <script>
        $(document).on('click','#add_row',function () {
            $(this).attr('disabled',true);

            var row = '<tr class= "added_now" >' +

                '<td td_type="search">' +
                '<select class="salary_scale set_all_disabled form-control js-example-disabled-results">\n'
                +'<option value="">'+''+'</option>'
                    @foreach($salary_sacle as $id=>$role)
                +'<option value="{{$id}}">'+'{{$role}}'
                    @endforeach
                +'</option>'
                +'</select></td>' +

                '<td td_type="check_box"><input  class="forigen set_all_disabled"   data-role="input" type="checkbox"/></td>'+


                '<td td_type="search">' +
                '<select  class="sub_salary_scale set_all_disabled form-control js-example-disabled-results">\n'
                +'<option class = "defult" value="">'+''+'</option>'
                    @foreach($sub_salary_scale as $single)
                +'<option class = "sub_{{$single->salary_id}}" value="{{$single->sub_id}}">'+'{{$single->sub_name}}'
                    @endforeach
                +'</option>'
                +'</select></td>' +


                '<td td_type="input"><input class="annual_salary row_data pull-left form-control required set_all_disabled" min="1.00" onkeypress="return (event.charCode == 8 || event.charCode == 1) ? null : event.charCode >= 48 && event.charCode <= 57 ||  event.charCode == 46" data-role="input" type="number"  placeholder="0"/></td>'+

                '<td td_type="input"><input class="monthly_salary row_data pull-left form-control required set_all_disabled" min="1.00" onkeypress="return (event.charCode == 8 || event.charCode == 1) ? null : event.charCode >= 48 && event.charCode <= 57 ||  event.charCode == 46" data-role="input" type="number"  placeholder="0"/ disabled></td>'+


                '<td td_type="search">' +
                '<select class="set_all_disabled currency_value form-control js-example-disabled-results">\n'
                +'<option value="">'+''+'</option>'
                    @foreach($currencies as $id=>$role)
                +'<option value="{{$id}}">'+'{{$role}}'
                    @endforeach
                +'</option>'
                +'</select></td>'



                +'<td>'+'<button class="set_all_disabled btn btn-primary btn-xs Save_row">'+'Save'+'</button>'
                +'</td>'
                +'<td>'
                +'<button class="set_all_disabled btn btn-danger btn-xs delete_row"  >'+'<span class="lnr lnr-trash"></span>'+'</button>'
                +'</td>'

                +'</tr>';
            $("#op_table").prepend(row);
            $(".js-example-disabled-results").select2();
            //get_master_acc_drop_down($('.master_account'));

            //$(".po_values").siblings('.select2').css('width', '71px');

        });

    </script>


    {{----------------   filter sub salary with salary ---------------------}}
    <script>
        $(document).on('change','.salary_scale',function () {

            var documents = $(this).parents('tr').find('.sub_salary_scale');
            var salary_Scale = $(this).val();

            get_item_type_drop_down(documents,salary_Scale)

        });



        function get_item_type_drop_down($dropdown,salary_Scale)
        {

            var classs = '.sub_'+salary_Scale;

            $dropdown.find('option').remove();


            $dropdown.parents('td').html('<select class="sub_salary_scale set_all_disabled form-control js-example-disabled-results">\n'
                +'<option class="defult" value="">'+''+'</option>'
                    @foreach($sub_salary_scale as $single)
                +'<option class = "sub_{{$single->salary_id}}" value="{{$single->sub_id}}">'+'{{$single->sub_name}}'
                    @endforeach
                +'</option>'
                +'</select>');





            $('.sub_salary_scale').find('option').not(classs).not('.defult').remove();

            $('.sub_salary_scale').select2();
            $('.sub_salary_scale').siblings('.select2').css('width', '71px');

            /*$dropdown.select2('destroy');

            $dropdown.find('.sub_'+salary_Scale).show();


            $dropdown.select2();*/


        }
    </script>



    {{--------   save journal row  --------------}}
    <script>
        $(document).on("click", ".Save_row", function(){

            var empty = false;
            var call_save = false;
            var input = $(this).parents("tr").find('input[type="number"]');


            var selectors = $(this).parents("tr").find('select');



            input.each(function(){
                if(!$(this).val()|| $(this).val()==""){
                    $(this).addClass("error");
                    empty = true;
                } else{
                    $(this).removeClass("error");
                }
            });

            selectors.each(function(){
                if(!$(this).val()|| $(this).val()==""){
                    $(this).addClass("error");
                    empty = true;
                } else{
                    $(this).removeClass("error");
                }
            });

            console.log('empty '+empty);

            $(this).parents("tr").find(".error").first().focus();



            if(!empty ) {

                /*                $(this).prop('disabled',true);*/
                save_table_effect($(this));

            }


        });

        function save_table_effect(thiss) {


            $('.set_all_disabled').prop('disabled', true);

            var academic_year = $('#acadimic_year').val();

            var row_id = thiss.parents("tr").attr('id');
            var row = [];

            var salary_scale = thiss.parents("tr").find('.salary_scale').val();


            if (thiss.parents("tr").find('.forigen:checked').length > 0) {
                var forigen = 1;
            }
            else {
                var forigen = 0;
            }

            var sub_salary_scale = thiss.parents("tr").find('.sub_salary_scale').val();
            var annual_salary = thiss.parents("tr").find('.annual_salary').val();
            var monthly_salary = thiss.parents("tr").find('.monthly_salary').val();
            var currency_value = thiss.parents("tr").find('.currency_value').val();

            row.push({
                "salary_scale": salary_scale,
                "forigen": forigen,
                "sub_salary_scale": sub_salary_scale,
                "annual_salary": annual_salary,
                "monthly_salary": monthly_salary,
                "currency_value": currency_value
            });
                console.log('forigen : ' + forigen);

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/add_new_sub_plane/',
                dataType: 'json',
                type: 'get',
                data: {

                    academic_year: academic_year,
                    table_rows: row,
                    row_id: row_id


                },
                success: function (response) {
                    console.log(response);

                    if (response['error']) {
                        $('.err_input').removeClass('red-border');
                        $('.err_input').parent().find('.select2').removeClass('red-border');



                        notification('glyphicon glyphicon-warning-sign','Warning','You Should Fill All Fields!','danger')


                        $.each(response['error'], function (key, val) {
                            //alert(key + val);

                            console.log(key + '_error');
                            $("." + key + '_error').addClass('red-border');
                            $('.' + key + '_error').parent().find('.select2').addClass('red-border');

                            //console.log(val[0]);

                        });
                        // $('.disable_on_save').prop('disabled', false);
                        $('.set_all_disabled').prop('disabled', false);

                    }
                    else {


                        if (thiss.parents("tr").find('.forigen:checked').length > 0) {
                            thiss.parents("tr").find('.forigen').parent("td").attr('current_value', 1).attr('yes_or_no', 'Yes');
                        }
                        else {
                            thiss.parents("tr").find('.forigen').parent("td").attr('current_value', 0).attr('yes_or_no', 'No');
                        }

                        var check_box = thiss.parents("tr").find('input.forigen');
                        var check_text_val = check_box.parent("td").attr('yes_or_no');
                        check_box.parent("td").addClass('forigen_v').html(check_text_val);

                        /*-----------------  all select -------------------------*/
                        var select_array = ['salary_scale_v', 'sub_salary_scale_v', 'currency_value_v'];
                        var r = 0;
                        var all_dowp_down = thiss.parents("tr").find('select');
                        all_dowp_down.each(function () {
                            var value_select = $(this).val();
                            var text_select = $(this).find('option:selected').text();
                            $(this).parent("td").addClass(select_array[r]).attr('current_value', value_select).html(text_select);
                            r++;

                        });
                        /*--------- all input  ------------------*/
                        var input_array = ['annual_salary_v', 'monthly_salary_v'];
                        var all_input = thiss.parents("tr").find('input[type="number"]');

                        var i = 0;
                        all_input.each(function () {

                            var value_select = $(this).val();
                            $(this).parent("td").addClass(input_array[i]).attr('current_value', value_select).html(value_select);

                            i++;
                        });
                        /*--------------------------*/

                        if (thiss.parents("tr").hasClass('added_now')) {
                            thiss.parents("tr").removeClass('added_now');

                        }
                        if (thiss.parents("tr").hasClass('edited_now')) {
                            thiss.parents("tr").removeClass('edited_now');

                        }

                        //|| thiss.siblings().find('.create_edit_v') ){
                        //console.log(thiss.closest('tr').attr('id'))


                        if ($('.added_now').length == 0) {
                            $("#add_row").removeAttr("disabled");
                        }

                        thiss.parents("tr").attr('id', response['just_add_row_id']);
                        thiss.text('Edit');
                        thiss.removeClass('Save_row');
                        thiss.addClass('edit_row');
                        thiss.prop('disabled', false);
                        //$('.disable_on_save').prop('disabled', false);
                        $('.set_all_disabled').prop('disabled', false);


                    }
                    },
                    error: function (response) {
                            alert(' Cant Save This Row !');
        //                    $('.disable_on_save').prop('disabled',false);
                            $('.set_all_disabled').prop('disabled', false);
                }

            });
        }



    </script>


    {{----------------------  edit row --------------------}}
    <script>
        $(document).on("click", ".edit_row", function(){
            if(!$('#op_table tr').hasClass('added_now') && !$('#op_table tr').hasClass('edited_now'))
            {
                var input_class_array = ['annual_salary','monthly_salary'];
                var disable_array =  ['','disabled'];
                var s=0;

                $('#add_row').prop('disabled',true);
                $(this).parents("tr").addClass('edited_now');

                $(this).parents("tr").find("td:not(:last-child)").each(function(){
                    if($(this).attr('td_type') == 'input')
                    {
                        $(this).html('<input class="'+input_class_array[s]+ '  row_data pull-left form-control required set_all_disabled" min="1.00" onkeypress="return (event.charCode == 8 || event.charCode == 1) ? null : event.charCode >= 48 && event.charCode <= 57 ||  event.charCode == 46" data-role="input" type="number"  placeholder="0" value="' + $(this).text() + '" '+disable_array[s]+'/>');
                        s++;
                    }

                    else if ($(this).attr('td_type') == 'search')
                    {
                        if($(this).hasClass('salary_scale_v'))
                        {
                            $(this).html('<select class="salary_scale set_all_disabled form-control js-example-disabled-results">\n'
                                +'<option value="">'+''+'</option>'
                                    @foreach($salary_sacle as $id=>$role)
                                +'<option value="{{$id}}">'+'{{$role}}'
                                    @endforeach
                                +'</option>'
                                +'</select>');

                            var selected_v = $(this).attr('current_value');
                            $(this).find(".salary_scale").val(selected_v);
                            $(".salary_scale").select2();
                            $(".salary_scale").siblings('.select2').css('width', '71px');
                        }

                        else if($(this).hasClass('sub_salary_scale_v'))
                        {
                            var salary_v = $(this).parents('tr').find(".salary_scale").val();

                            var classs = '.sub_'+salary_v;

                            $('.sub_salary_scale').find('option').remove();

                            $(this).html('<select class="sub_salary_scale set_all_disabled form-control js-example-disabled-results">\n'
                                +'<option class="defult" value="">'+''+'</option>'
                                    @foreach($sub_salary_scale as $single)
                                +'<option class = "sub_{{$single->salary_id}}" value="{{$single->sub_id}}">'+'{{$single->sub_name}}'
                                    @endforeach
                                +'</option>'
                                +'</select>');

                            $('.sub_salary_scale').find('option').not(classs).not('.defult').remove();

                            var selected_v = $(this).attr('current_value');
                            $(this).find(".sub_salary_scale").val(selected_v);
                            $(".sub_salary_scale").select2();
                            $(".sub_salary_scale").siblings('.select2').css('width', '71px');
                        }

                        else if($(this).hasClass('currency_value_v'))
                        {
                            $(this).html('<select class="currency_value form-control js-example-disabled-results">\n'
                                +'<option value="">'+''+'</option>'
                                    @foreach($currencies as $id=>$role)
                                +'<option value="{{$id}}">'+'{{$role}}'
                                    @endforeach
                                +'</option>'
                                +'</select>');

                            var selected_v = $(this).attr('current_value');
                            $(this).find(".currency_value").val(selected_v);
                            $(".currency_value").select2();
                            $(".currency_value").siblings('.select2').css('width', '71px');
                        }

                    }

                    else if ($(this).attr('td_type') == 'check_box')
                    {
                        $(this).html('<input  class="forigen"   data-role="input" type="checkbox"/>');
                        if($(this).attr('current_value')==1)
                        {
                            $(this).find('.forigen').prop('checked', true);
                        }
                        else
                        {
                            $(this).find('.forigen').prop('checked', false);
                        }
                    }


                });

                $("#add_row").attr("disabled", "disabled");
                $(this).text('Save');
                $(this).removeClass('edit_row');
                $(this).addClass('Save_row');

            }
        });

    </script>



    {{-------------------  delete row  -------------------------}}
    <script>

        $(document).on('click', '.delete_row', function () {

            $(this).hide(function () {
                $(this).closest('td').append('<button class="btn btn-danger set_all_disabled" type="button" id="yes" style="display: none;">yes</button>' +
                    '<button class="btn btn-default set_all_disabled" type="button" id="no" style="display: none;">no</button>');
                $(this).closest('td').find("#yes").show('slow').delay(1000);
                $(this).closest('td').find("#no").show('slow').delay(1000);

            });
        });

        $(document).on('click', '#yes', function () {


            var deleted_id = $(this).closest('tr').attr('id');
            var tr = $(this).closest('tr');

            console.log('deleted_id : ' + deleted_id);
            if(deleted_id == "" || !deleted_id)
            {
                tr.remove();
            }
            else{
                $('.set_all_disabled').prop('disabled', true);

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/delete_sub_plane/',
                    dataType: 'json',
                    type: 'get',
                    data: {

                        row: deleted_id,
                    },

                    success: function (response) {
                        console.log(response);

                        if (response['error']) {
                            $('.err_input').removeClass('red-border');
                            $('.err_input').parent().find('.select2').removeClass('red-border');


                            notification('glyphicon glyphicon-warning-sign','Warning','You Should Fill All Fields!','danger')



                            $.each(response['error'], function (key, val) {
                                //alert(key + val);

                                console.log(key + '_error');
                                $("." + key + '_error').addClass('red-border');
                                $('.' + key + '_error').parent().find('.select2').addClass('red-border');

                                //console.log(val[0]);

                            })
                            // $('.disable_on_save').prop('disabled', false);
                            $('.set_all_disabled').prop('disabled', false);

                        }
                        else{

                            /*--------------  save row effect in table  ------------------*/

                            /*--------------  all select ------------*/
                            tr.remove();
                            $('.set_all_disabled').prop('disabled', false);
                        }
                    },
                    error: function (response)
                    {
                        alert(' Cant Delete This Row !');
//                    $('.disable_on_save').prop('disabled',false);
                        $('.set_all_disabled').prop('disabled',false);
                    }

                });

            }

            if($('.added_now').length == 0)
            {
                $("#add_row").removeAttr("disabled");

            }

        });

        $(document).on('click', '#no', function () {

            console.log('noo ');

            $(this).closest('td').find(".delete_row").show('slow').delay(1000);
            $(this).closest('td').find("#yes").hide('slow').delay(1000).remove();
            $(this).closest('td').find("#no").hide('slow').delay(1000).remove();
            /*
            $(this).closest('td').find("#yes").hide('slow').delay(1000);
            $(this).closest('td').find("#no").hide('slow').delay(1000);*/


        });


    </script>



    {{---------------  active add row  on supplier change --------------------}}
    <script>
        $(document).on('change','.validate_tab',function () {

            enable_disable_add();

        });

        function enable_disable_add(){
            $("#op_table tbody").empty();
            var isValid;
            isValid = true;
            $(".validate_tab").each(function() {
                var element = $(this);
                if (element.val() == "") {
                    isValid = false;
                    /*$('.validate_tab').prop('disabled',false);*/
                }

            });

            console.log('isValid'+isValid);

            if(isValid && !$('#op_table tr').hasClass('added_now'))
            {

                $('#add_row').prop('disabled',true);

                var acadimic_year = $('#acadimic_year').val();
                $('.set_all_disabled').prop('disabled',true);

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/sub_plane_table/',
                    dataType : 'json',
                    type: 'get',
                    data: {
                        acadimic_year:acadimic_year,
                    },

                    success:function(response) {

                        if(response['error'])
                        {
                            $('.errorMessage').addClass('hide');
                            $.each(response['error'], function (key, val)
                            {
                                //alert(key + val);

                                console.log(key+'_error');
                                $("#"+key+'_error').removeClass('hide').html(val[0]);
                                //console.log(val[0]);

                            })
                            $('.set_all_disabled').prop('disabled',false);
                        }
                        else{



                            for(var i =0;i<response['plane_detailes'].length;i++)
                            {
                                var forign_string = '';
                                if(response['plane_detailes'][i]['forigen_type'] == 0)
                                {
                                    forign_string = 'No';
                                }
                                else
                                {
                                    forign_string = 'Yes';
                                }

                                var row = '<tr class="" id="'+response['plane_detailes'][i]['plan_id']+'">' +
                                    '<td td_type="search" data-select2-id="63" class="salary_scale_v" current_value="'+response['plane_detailes'][i]['salary_scale_id']+'">'+response['plane_detailes'][i]['salary_name']+'</td>' +

                                    '<td td_type="check_box" current_value="'+response['plane_detailes'][i]['forigen_type']+'" ' + 'yes_or_no="'+forign_string+'" class="forigen_v">'+forign_string+'</td>' +
                                    '<td td_type="search" class="sub_salary_scale_v" current_value="'+response['plane_detailes'][i]['sub_salary_id']+'">'+response['plane_detailes'][i]['sub_name']+'</td>' +
                                    '<td td_type="input" class="annual_salary_v" current_value="'+response['plane_detailes'][i]['annual_salary']+'">'+response['plane_detailes'][i]['annual_salary']+'</td>' +
                                    '<td td_type="input" class="monthly_salary_v" current_value="'+response['plane_detailes'][i]['monthly_salary']+'">'+response['plane_detailes'][i]['monthly_salary']+'</td>' +
                                    '<td td_type="search" class="currency_value_v" current_value="'+response['plane_detailes'][i]['currency_id']+'">'+response['plane_detailes'][i]['Currency_name']+'</td>' +

                                    '<td><button class="set_all_disabled btn btn-primary btn-xs edit_row">Edit</button></td>' +
                                    '<td><button class="set_all_disabled btn btn-danger btn-xs delete_row">' +
                                    '<span class="lnr lnr-trash">' +
                                    '</span></button>' +
                                    '</td></tr>';
                                $("#op_table").append(row);
                            }


                            console.log(response);
                            $('.set_all_disabled').prop('disabled',false);
                            $('#add_row').prop('disabled',false);
                        }


                    }

                });



            }

            else{
                $('#add_row').prop('disabled',true);
            }

        }

    </script>

    {{--------------------------  change monthly with annual  -------------------------------------------}}
    <script>
        $(document).on('keyup','.annual_salary',function(){
            var annual = $(this).val();
            var monthly = annual/12;

            $(this).parents('tr').find('.monthly_salary').val(monthly);
        });
    </script>

    <!------------print--------------->
    <script>

        $(document).on('click', '#print', function () {

         var id = $('#acadimic_year').val();            //  window.location.href = 'get_po2/276'

            // var id = $('#set_po_id').attr('current_po_id');
            window.open('/get_salary_plan_rpt/' +id+ '', '_blank');


        });

    </script>
@endsection