<!DOCTYPE html>

@extends('admin.admin_panel')

@section('content')

    <link rel="stylesheet" href="{{asset('erp_panel/css/bootstrap.min.css')}}" />
    <link rel="stylesheet" href="{{asset('erp_panel/font-awesome/4.5.0/css/font-awesome.min.css')}}" />

    <!-- page specific plugin styles -->
    <link rel="stylesheet" href="{{asset('erp_panel/css/jquery-ui.min.css')}}" />
    <link rel="stylesheet" href="{{asset('erp_panel/css/select2.min.css')}}" />

    <link rel="stylesheet" href="{{asset('erp_panel/css/bootstrap-datepicker3.min.css')}}" />
    <link rel="stylesheet" href="{{asset('erp_panel/css/ui.jqgrid.min.css')}}" />

    <!-- text fonts -->
    <link rel="stylesheet" href="{{asset('erp_panel/css/fonts.googleapis.com.css')}}" />

    <!-- ace styles -->
    <link rel="stylesheet" href="{{asset('erp_panel/css/ace.min.css')}}" class="ace-main-stylesheet" id="main-ace-style" />

    <!--[if lte IE 9]>
    <link rel="stylesheet" href="{{asset('erp_panel/css/ace-part2.min.css')}}" class="ace-main-stylesheet" />
    <![endif]-->
    <link rel="stylesheet" href="{{asset('erp_panel/css/ace-skins.min.css')}}" />
    <link rel="stylesheet" href="{{asset('erp_panel/css/ace-rtl.min.css')}}" />

    <!--[if lte IE 9]>
    <link rel="stylesheet" href="{{asset('erp_panel/css/ace-ie.min.css')}}" />
    <![endif]-->
    <link rel="stylesheet" href="{{asset('erp_panel/css/bootstrap-select.min.css')}}" />

    <!-- inline styles related to this page -->

    {{---  table style ---}}




    <!-- ace settings handler -->
    <script src="{{asset('erp_panel/js/ace-extra.min.js')}}"></script>

    <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->


    <script src="{{asset('erp_panel/js/html5shiv.min.js')}}"></script>
    <script src="{{asset('erp_panel/js/respond.min.js')}}"></script>

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <div class="page-header">
        <h1>
            Basics
            <small>
                <i class="ace-icon fa fa-angle-double-right"></i>
                Admission
                <small>
                    <i class="ace-icon fa fa-angle-double-right"></i>
                   Student
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        Edit Student
                    </small>
                </small>
            </small>
        </h1>
    </div>
    <!-- /.page-header -->




    {{--<div id="user-profile-3" class="user-profile row">
        <div class="col-sm-offset-1 col-sm-10">
            <div class="well well-sm">
                <!-- -
<button type="button" class="close" data-dismiss="alert">&times;</button>
&nbsp; -->
                <div class="inline middle blue bigger-110"> Your profile is 70% complete </div>

                &nbsp; &nbsp; &nbsp;
                <div style="width:200px;" data-percent="70%" class="inline middle no-margin progress progress-striped active pos-rel">
                    <div class="progress-bar progress-bar-success" style="width:70%"></div>
                </div>
            </div><!-- /.well -->

            <div class="space"></div>

            <form class="form-horizontal">
                <div class="tabbable">
                    <ul class="nav nav-tabs padding-16">
                        <li id="student_section" class="active">
                            <a --}}{{--data-toggle="tab"--}}{{-- href="#student_inof" name="student_top">
                                <i class="green ace-icon fa fa-pencil-square-o bigger-125"></i>
                                Basic Info
                            </a>
                        </li>

                        <li id="father_section">
                            <a  --}}{{--data-toggle="tab"--}}{{-- href="#father_info" name="father_top">
                                <i class=" blue ace-icon  fa fa-male bigger-125"></i>
                                Father Info
                            </a>
                        </li>

                        <li id="mother_section" >
                            <a --}}{{--data-toggle="tab"--}}{{-- href="#mother_info" name="mother_top">
                                <i class="red ace-icon fa fa-female bigger-125"></i>
                                Mother Info
                            </a>
                        </li>
                    </ul>

                    <div class="tab-content profile-edit-tab-content">

                        --}}{{----------------------------- first tabe-------------------------------------}}{{--

                        <div id="student_inof"   class="tab-pane in active">
                            <form id="student_form">
                            <h4 class="header blue bolder smaller">General</h4>

                            <div class="row">
                                <div class="col-xs-12 col-sm-4">
                                    <input  id="image_input" type="file">
                                </div>

                                <div class="vspace-12-sm"></div>

                                <div class="col-xs-12 col-sm-8">
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-username">First Name</label>

                                        <div class="col-sm-8">
                                            <input class="col-xs-12 col-sm-10" type="text" id="first_name" placeholder="First Name" >
                                        </div>
                                    </div>



                                </div>
                            </div>

                            <hr>
                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-date">Birth Date</label>

                                <div class="col-sm-8">
                                    <div class="input-medium">
                                        <div class="input-group">
                                            <input class="input-medium date-picker" id="birth_date" type="text" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd">
                                            <span class="input-group-addon">
																				<i class="ace-icon fa fa-calendar"></i>
																			</span>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right">Gender</label>

                                <div class="col-sm-8">
                                    <label class="inline">
                                        <input name="form_field_radio" gender="1" type="radio" class="ace">
                                        <span class="lbl middle"> Male</span>
                                    </label>

                                    &nbsp; &nbsp; &nbsp;
                                    <label class="inline">
                                        <input name="form_field_radio" gender="0" type="radio" class="ace">
                                        <span class="lbl middle"> Female</span>
                                    </label>
                                </div>
                            </div>



                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-username">Nationality</label>

                                <div class="col-sm-8">
                                    <select style="width: 83.5%;" class=" col-sm-9 selectpicker form-control" id="nationalty">
                                    <option></option>
                                    </select>
                                </div>
                            </div>




                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-username">Code</label>

                                <div class="col-sm-8">
                                    <input class="col-xs-12 col-sm-10" type="text" id="code" placeholder="Code" >
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-username">Acadmic Year</label>

                                <div class="col-sm-8">
                                    <select style="width: 83.5%;" class=" col-sm-9 selectpicker form-control" id="acadmic_year">
                                        <option></option>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-date">Registration Date</label>

                                <div class="col-sm-8">
                                    <div class="input-medium">
                                        <div class="input-group">
                                            <input class="input-medium date-picker" id="registration_date" type="text" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd">
                                            <span class="input-group-addon">
																				<i class="ace-icon fa fa-calendar"></i>
																			</span>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-username">Registration Class</label>

                                <div class="col-sm-8">
                                    <select style="width: 83.5%;" class=" col-sm-9 selectpicker form-control" id="registration_class">
                                        <option></option>
                                    </select>
                                </div>
                            </div>



                            <div class="space"></div>
                            <h4 class="header blue bolder smaller">More Info</h4>

                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="stock">Leave</label>
                                <div class="col-sm-8">
                                <input  id="leave_value"  get_v="0" data-toggle="toggle" type="checkbox"  data-on="Ready" data-off="Not Ready"  role="checkbox" class="FormElement ace ace-switch ace-switch-5">
                                <span class="lbl"></span>
                            </div>
                            </div>

                            <div class="form-group" id="l_date" style="display: none">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-date">Leave Date</label>

                                <div class="col-sm-8">
                                    <div class="input-medium">
                                        <div class="input-group">
                                            <input class="input-medium date-picker"  id="leave_date" type="text" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd" value="">
                                            <span class="input-group-addon">
																				<i class="ace-icon fa fa-calendar"></i>
																			</span>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="clearfix form-actions">
                                <div class="col-md-offset-3 col-md-9">

                                    <button  id="reset_btn" class="btn" type="reset">
                                        <i class="ace-icon fa fa-undo bigger-110"></i>
                                        Reset
                                    </button>

                                  <a href="#father_top">  <button  id="father_btn" class="btn btn-success btn-next" type="button">
                                        Next
                                        <i class="ace-icon fa fa-arrow-right icon-on-right"></i>

                                    </button></a>
                                </div>
                            </div>
                            </form>
                    </div>


                        --}}{{--------------------------------------------------------------------------------}}{{--

                        --}}{{-------------------------------- second tabe ----------------------------------------------}}{{--

                        <div id="father_info" class="tab-pane">

                            <form id="father_form" >
                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-username">First Name</label>

                                <div class="col-sm-8">
                                    <input class="col-xs-12 col-sm-10" type="text" id="parent_first_name" placeholder="First Name" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-username">Second Name</label>

                                <div class="col-sm-8">
                                    <input class="col-xs-12 col-sm-10" type="text" id="parent_second_name" placeholder="Second Name" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-username">Third Name</label>

                                <div class="col-sm-8">
                                    <input class="col-xs-12 col-sm-10" type="text" id="parent_third_name" placeholder="Third Name" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-username">Family Name</label>

                                <div class="col-sm-8">
                                    <input class="col-xs-12 col-sm-10" type="text" id="parent_family_name" placeholder="Family Name" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-email">Email</label>

                                <div class="col-sm-8">
																	<span class="input-icon input-icon-right">
																		<input type="email" id="parent_email" >
																		<i class="ace-icon fa fa-envelope"></i>
																	</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-phone">Phone</label>

                                <div class="col-sm-8">
																	<span class="input-icon input-icon-right">
																		<input class="input-medium input-mask-phone" type="text" id="form-field-phone">
																		<i class="ace-icon fa fa-phone fa-flip-horizontal"></i>
																	</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-username">ID Number</label>

                                <div class="col-sm-8">
                                    <input class="col-xs-12 col-sm-10" type="text" id="parent_id_number" placeholder="ID Number" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-username">Passport ID</label>

                                <div class="col-sm-8">
                                    <input class="col-xs-12 col-sm-10" type="text" id="parent_passport_id" placeholder="Passport ID" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-comment">Address</label>

                                <div class="col-sm-8">
                                    <textarea   id="parent_address" style="margin: 0px; width: 83.5%; height: 60px;"></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-username">Country</label>

                                <div class="col-sm-8">
                                    <select style="width: 83.5%;" class=" col-sm-8 selectpicker form-control" id="country_id">
                                        <option></option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="stock">Finance</label>
                                <div class="col-sm-8">
                                <input  id="finance"  finance_v="0" data-toggle="toggle" type="checkbox"  data-on="Ready" data-off="Not Ready"  role="checkbox" class="FormElement ace ace-switch ace-switch-5">
                                <span class="lbl"></span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-comment">Other Notes</label>

                                <div class="col-sm-8">
                                    <textarea id="parent_notes" style="margin: 0px; width: 83.5%; height: 60px;"></textarea>
                                </div>
                            </div>


                            <div class="clearfix form-actions">
                                <div class="col-md-offset-3 col-md-9">
                                    &nbsp;<button id="student_btn_"class="btn btn-prev" type="button">
                                        <i class="ace-icon fa fa-arrow-left"></i>
                                        Prev
                                    </button> &nbsp;
                                    <button id="reset_student_btn" class="btn" type="reset">
                                        <i class="ace-icon fa fa-undo bigger-110"></i>
                                        Reset
                                    </button>

                                    <button  id="mother_btn" class="btn btn-success btn-next" type="button">
                                        Next
                                        <i class="ace-icon fa fa-arrow-right icon-on-right"></i>
                                    </button>
                                </div>
                            </div>
                            </form>
                        </div>

                        --}}{{---------------------------------------------------------------------------------}}{{--

                        --}}{{----------------------------- third tabe --------------------------------------}}{{--

                        <div id="mother_info" class="tab-pane">
                            <form id="mother_form">
                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-username">First Name</label>

                                <div class="col-sm-8">
                                    <input class="col-xs-12 col-sm-10" type="text" id="mother_first_name" placeholder="First Name" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-username">Second Name</label>

                                <div class="col-sm-8">
                                    <input class="col-xs-12 col-sm-10" type="text" id="mother_second_name" placeholder="Second Name" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-username">Third Name</label>

                                <div class="col-sm-8">
                                    <input class="col-xs-12 col-sm-10" type="text" id="mother_third_name" placeholder="Third Name" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-username">Family Name</label>

                                <div class="col-sm-8">
                                    <input class="col-xs-12 col-sm-10" type="text" id="mother_family_name" placeholder="Family Name" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-email">Email</label>

                                <div class="col-sm-8">
																	<span class="input-icon input-icon-right">
																		<input type="email" id="mother_email" >
																		<i class="ace-icon fa fa-envelope"></i>
																	</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-phone">Phone</label>

                                <div class="col-sm-8">
																	<span class="input-icon input-icon-right">
																		<input class="input-medium input-mask-phone" type="text" id="mother_phone">
																		<i class="ace-icon fa fa-phone fa-flip-horizontal"></i>
																	</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-username">ID Number</label>

                                <div class="col-sm-8">
                                    <input class="col-xs-12 col-sm-10" type="text" id="mother_id_number" placeholder="ID Number" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-username">Passport ID</label>

                                <div class="col-sm-8">
                                    <input class="col-xs-12 col-sm-10" type="text" id="mother_passport_id" placeholder="Passport ID" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-comment">Address</label>

                                <div class="col-sm-8">
                                    <textarea   id="mother_address" style="margin: 0px; width: 83.5%; height: 60px;"></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-username">Country</label>

                                <div class="col-sm-8">
                                    <select style="width: 83.5%;" class=" col-sm-8 selectpicker form-control" id="mother_country_id">
                                        <option></option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="stock">Finance</label>
                                <div class="col-sm-8">
                                    <input  id="mother_finance"  finance_v="0" data-toggle="toggle" type="checkbox"  data-on="Ready" data-off="Not Ready"  role="checkbox" class="FormElement ace ace-switch ace-switch-5">
                                    <span class="lbl"></span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-comment">Other Notes</label>

                                <div class="col-sm-8">
                                    <textarea id="mother_notes" style="margin: 0px; width: 83.5%; height: 60px;"></textarea>
                                </div>
                            </div>






                            <div class="clearfix form-actions">
                                <div class="col-md-offset-3 col-md-9">
                                    &nbsp; &nbsp;<button id="father_btn"class="btn btn-prev" type="button">
                                        <i class="ace-icon fa fa-arrow-left"></i>
                                        Prev
                                    </button>
                                    <button id="reset_mother_btn" class="btn" type="reset">
                                        <i class="ace-icon fa fa-undo bigger-110"></i>
                                        Reset
                                    </button>

                                    <button  id="submit_all_form" class="btn btn-info" type="button">
                                        <i class="ace-icon fa fa-check bigger-110"></i>
                                        Submit
                                    </button>
                                </div>
                            </div>
                            </form>
                        </div>


                        --}}{{-------------------------------------------------------------------------------}}{{--

                </div>


                </div>
            </form>
        </div><!-- /.span -->
    </div>--}}



    <div class="widget-box">
        <div class="widget-header widget-header-blue widget-header-flat">
            <h4 class="widget-title lighter">Edit Student Data</h4>

            <div class="widget-toolbar">
            </div>
        </div>

        <div class="widget-body">
            <div class="widget-main">
                <div id="fuelux-wizard-container" class="no-steps-container">
                    <div>
                        <ul class="steps" style="margin-left: 0">
                            <li id="first_step" data-step="1" class="active">
                                <span class="step">1</span>
                                <span class="title">Student Info</span>
                            </li>

                            <li id="second_step" data-step="2">
                                <span class="step">2</span>
                                <span class="title">Father Info</span>
                            </li>

                            <li id="third_step" data-step="3">
                                <span class="step">3</span>
                                <span class="title">Mother Info</span>
                            </li>

                           {{-- <li data-step="4">
                                <span class="step">4</span>
                                <span class="title">Other Info</span>
                            </li>--}}
                        </ul>
                    </div>

                    <hr>

                    <div class="step-content pos-rel">
                        <div id="first_step_plane" class="step-pane active" data-step="1">

                            <form class="form-horizontal" name="step1_validation-form" id="step1_validation-form" method="get" novalidate="novalidate">
                                <h4 class="header blue bolder smaller">General</h4>






                                <div class="row">




                                    <div  class="col-xs-12 col-sm-4">
                                    <div id="show_student_image">
                                    <span class="profile-picture " style=" width: 45%;">
									@if(file_exists('uploads/'.$query->First_name.'_'.$query->code.'.jpg'))
                                            <img id="profile_img" style="height: 140px;" class=" img-responsive " alt="Student Pic" src="{{asset('uploads/'.$query->First_name.'_'.$query->code.'.jpg')}}" />
                                    @else
                                            <img id="profile_img" style="height: 140px;" class=" img-responsive " alt="defult Student Pic" src="{{asset('uploads/defult_image.jpg')}}" />
								    @endif
                                    </span>
                                    </div>

                                        <div id="show_add_image" style="display: none;">

                                            <div class="col-xs-12 col-sm-8">
                                                <div class="form-group">
                                                    <div class="col-xs-12">
                                                        <input multiple="" type="file" name="id-input-file-3" id="id-input-file-3" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <label class="col-sm-5 control-label no-padding-right" for="stock">Change Image</label>
                                        <div class="col-sm-6">
                                            <input  id="change_student_image"   data-toggle="toggle" type="checkbox"  data-on="Ready" data-off="Not Ready"  role="checkbox" class="ace ace-switch ace-switch-6">
                                            <span class="lbl"></span>
                                        </div>

                                    </div>

                                    <div class="vspace-12-sm"></div>

                                    <div class="col-xs-12 col-sm-8">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label no-padding-right" for="form-field-username">First Name</label>

                                            <div class="col-sm-8">
                                                <input old_img_value="{{$query->First_name}}" edit_student_id="{{$query->SID}}" class="col-xs-12 col-sm-10" value="{{$query->First_name}}" type="text" name="first_name" id="first_name" placeholder="First Name" >
                                            </div>
                                        </div>



                                    </div>
                                </div>

                                <hr>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-date">Birth Date</label>

                                    <div class="col-sm-8">
                                        <div class="input-medium">
                                            <div class="input-group">
                                                <input class="input-medium date-picker" value="{{$query->Birth_of_date}}" name="birth_date" id="birth_date" type="text" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd">
                                                <span class="input-group-addon">
																				<i class="ace-icon fa fa-calendar"></i>
																			</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>



                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right">Gender</label>

                                    <div class="col-sm-8">
                                        @if ($query->Gender_type == 1)
                                        <label class="inline">
                                            <input checked name="form_field_radio" gender="1" type="radio" class="ace">
                                            <span class="lbl middle"> Male</span>
                                        </label>

                                        &nbsp; &nbsp; &nbsp;
                                        <label class="inline">
                                            <input name="form_field_radio" gender="0" type="radio" class="ace">
                                            <span class="lbl middle"> Female</span>
                                        </label>
                                            @else
                                            <label class="inline">
                                                <input  name="form_field_radio" gender="1" type="radio" class="ace">
                                                <span class="lbl middle"> Male</span>
                                            </label>

                                            &nbsp; &nbsp; &nbsp;
                                            <label class="inline">
                                                <input checked name="form_field_radio" gender="0" type="radio" class="ace">
                                                <span class="lbl middle"> Female</span>
                                            </label>
                                            @endif
                                    </div>
                                </div>



                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-username">Nationality</label>

                                    <div class="col-sm-8">
                                        <select selected_option="{{$query->Nationality_type_id}}" style="width: 83.5%;" class=" col-sm-9  form-control" name="nationalty" id="nationalty">
                                            <option></option>
                                        </select>
                                    </div>
                                </div>




                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-username">Code</label>

                                    <div class="col-sm-8">
                                        <input old_img_value="{{$query->code}}" value="{{$query->code}}" class="col-xs-12 col-sm-10" type="text" name="code" id="code" placeholder="Code" >
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-username">Acadmic Year</label>

                                    <div class="col-sm-8">
                                        <select  selected_option="{{$query->Acadmic_year}}" style="width: 83.5%;" class=" col-sm-9  form-control" name="acadmic_year" id="acadmic_year">
                                            <option></option>
                                        </select>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-date">Registration Date</label>

                                    <div class="col-sm-8">
                                        <div class="input-medium">
                                            <div class="input-group">
                                                <input value="{{$query->registeration_date}}" class="input-medium date-picker" name="registration_date" id="registration_date" type="text" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd">
                                                <span class="input-group-addon">
																				<i class="ace-icon fa fa-calendar"></i>
																			</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>



                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-username">Registration Class</label>

                                    <div class="col-sm-8">
                                        <select selected_option="{{$query->registration_class_id}}" style="width: 83.5%;" class=" col-sm-9  form-control" name="registration_class" id="registration_class">
                                            <option></option>
                                        </select>
                                    </div>
                                </div>



                                <div class="space"></div>
                                <h4 class="header blue bolder smaller">More Info</h4>

                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="stock">Leave</label>
                                    <div class="col-sm-8">
                                        @if($query->leave == 1)
                                        <input checked id="leave_value"  get_v="0" data-toggle="toggle" type="checkbox"  data-on="Ready" data-off="Not Ready"  role="checkbox" class="FormElement ace ace-switch ace-switch-6">
                                        @else
                                        <input id="leave_value"  get_v="0" data-toggle="toggle" type="checkbox"  data-on="Ready" data-off="Not Ready"  role="checkbox" class="FormElement ace ace-switch ace-switch-6">
                                        @endif
                                        <span class="lbl"></span>
                                    </div>
                                </div>

                                @if($query->leave == 1)
                                <div class="form-group" id="l_date" style="display: block">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-date">Leave Date</label>


                                    <div class="col-sm-8">
                                        <div class="input-medium">
                                            <div class="input-group">
                                                <input value="{{$query->leave_date}}" class="input-medium date-picker" name="leave_date" id="leave_date" type="text" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd" >
                                                <span class="input-group-addon">
																				<i class="ace-icon fa fa-calendar"></i>
																			</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @else
                                    <div class="form-group" id="l_date" style="display: none">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-date">Leave Date</label>

                                        <div class="col-sm-8">
                                            <div class="input-medium">
                                                <div class="input-group">
                                                    <input class="input-medium date-picker" name="leave_date" id="leave_date" type="text" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd" >
                                                    <span class="input-group-addon">
																				<i class="ace-icon fa fa-calendar"></i>
																			</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif


                                {{-------------------- Sibling Options -------------------------}}
                                <div class="form-group" >
                                    <label class="col-sm-4 control-label no-padding-right" for="stock">Change Sibling Options</label>
                                    <div class="col-sm-8">
                                        <input  id="sibling_value"  sibling_v="0" data-toggle="toggle" type="checkbox"  data-on="Ready" data-off="Not Ready"  role="checkbox" class="ace ace-switch ace-switch-6">
                                        <span class="lbl"></span>
                                    </div>

                                    <div id="all_sibling"  style="display: none">


                                        <select defult_selected_value="{{$query->SID}}" id="sibling_selected_v" class="selectpicker" data-live-search="true">

                                            {{--<option data-tokens="1 expenses" value="1">expenses</option>
                                            <option data-tokens="2 assets" value="2">assets</option>
                                            <option data-tokens="3 revene" value="3">revene</option>--}}
                                        </select>

                                        <label>
                                            <input name="same_mother" id="same_mother" same_mother_v="0" type="checkbox" class="ace">
                                            <span class="lbl"> Has Same Mother</span>
                                        </label>


                                        <label  id="sibling_delete_op" delete_option_all="0" ></label>


                                        <br>




                                    </div>

                                </div>


                                {{------------------------------------ sibling table ---------------------------------------------------------}}
                                <div  class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" id="gbox_grid-table" dir="ltr" style="margin-top: 30px;width: 100%;"><div class="jqgrid-overlay ui-widget-overlay" id="lui_grid-table"></div><div class="ui-jqgrid-view " role="grid" id="gview_grid-table" style="width: 100%;"><div class="ui-jqgrid-titlebar ui-jqgrid-caption ui-widget-header ui-corner-top ui-helper-clearfix">

                                            <span class="ui-jqgrid-title">Siblings Table</span></div><div class="ui-jqgrid-hdiv ui-state-default" style="width: 100%;"><div >
                                                <table class="ui-jqgrid-htable ui-common-table " style="width: 100%;" role="presentation" aria-labelledby="gbox_grid-table"><thead><tr class="ui-jqgrid-labels" role="row">


                                                        <th  class="ui-th-column ui-th-ltr ui-state-default" ><span class="ui-jqgrid-resize ui-jqgrid-resize-ltr" style="cursor: col-resize;">&nbsp;</span>
                                                            <div class="ui-jqgrid-sortable">#<span class="s-ico" style="display:none"><span sort="asc" class="ui-grid-ico-sort ui-icon-asc ui-sort-ltr ui-state-disabled ui-icon ui-icon-triangle-1-n"></span><span sort="desc" class="ui-grid-ico-sort ui-icon-desc ui-sort-ltr ui-state-disabled ui-icon ui-icon-triangle-1-s"></span></span></div>
                                                        </th>
                                                        <th  class="ui-th-column ui-th-ltr ui-state-default" ><span class="ui-jqgrid-resize ui-jqgrid-resize-ltr" style="cursor: col-resize;">&nbsp;</span>
                                                            <div class="ui-jqgrid-sortable">Student name<span class="s-ico" style="display:none"><span sort="asc" class="ui-grid-ico-sort ui-icon-asc ui-sort-ltr ui-state-disabled ui-icon ui-icon-triangle-1-n"></span><span sort="desc" class="ui-grid-ico-sort ui-icon-desc ui-sort-ltr ui-state-disabled ui-icon ui-icon-triangle-1-s"></span></span></div>
                                                        </th>
                                                        <th role="columnheader" class="ui-th-column ui-th-ltr ui-state-default" ><span class="ui-jqgrid-resize ui-jqgrid-resize-ltr" style="cursor: col-resize;">&nbsp;</span>
                                                            <div  class="ui-jqgrid-sortable">Code<span class="s-ico" style="display:none"><span sort="asc" class="ui-grid-ico-sort ui-icon-asc ui-sort-ltr ui-state-disabled ui-icon ui-icon-triangle-1-n"></span><span sort="desc" class="ui-grid-ico-sort ui-icon-desc ui-sort-ltr ui-state-disabled ui-icon ui-icon-triangle-1-s"></span></span></div></th>
                                                        <th  class="ui-th-column ui-th-ltr ui-state-default"><span class="ui-jqgrid-resize ui-jqgrid-resize-ltr" style="cursor: col-resize;">&nbsp;</span>
                                                            <div class="ui-jqgrid-sortable">Class<span class="s-ico" style="display:none"><span sort="asc" class="ui-grid-ico-sort ui-icon-asc ui-sort-ltr ui-state-disabled ui-icon ui-icon-triangle-1-n"></span><span sort="desc" class="ui-grid-ico-sort ui-icon-desc ui-sort-ltr ui-state-disabled ui-icon ui-icon-triangle-1-s"></span></span></div>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="ui-jqgrid-bdiv" style="width: 100%;">
                                            <div style="position:relative;"><div></div>
                                                <table class="ui-jqgrid-btable ui-common-table" style="width: 100%;">

                                                    <tbody id="siblings_table_row">


                                                    {{--   <tr  class="jqgrow ui-row-ltr ui-widget-content ui-priority-secondary">

                                                           <td>333</td>
                                                           <td>333</td>
                                                           <td>yyu</td>
                                                           <td>1234</td>
                                                       </tr>
--}}


                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                </div>






                            </form>

                        </div>


                        <div id="second_step_plane" class="step-pane" data-step="2">
                            <form class="form-horizontal" id="step2_validation-form" method="get" novalidate="novalidate">

                                <div id="enable_father_edit" class="form-group" style="display: none;">
                                    <label class="col-sm-4 control-label no-padding-right" for="stock">Enable Edit</label>
                                    <div class="col-sm-8">
                                        <input  id="edit_father_data" one_time_edit_father="0"  edit_father_data="0" data-toggle="toggle" type="checkbox"  data-on="Ready" data-off="Not Ready"  role="checkbox" class="ace ace-switch ace-switch-6">
                                        <span class="lbl"></span>
                                    </div>
                                </div>



                            <div class="form-group">
                                <label class=" col-sm-4 control-label no-padding-right" for="form-field-username">First Name</label>

                                <div class="col-sm-8">
                                    <input orignal_sibling_v="{{$query_father->fname}}" edit_father_id="{{$query_father->par_id}}" value="{{$query_father->fname}}" class=" dis_able_father_edit col-xs-12 col-sm-10" type="text" name="parent_first_name" id="parent_first_name" placeholder="First Name" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-username">Second Name</label>

                                <div class="col-sm-8">
                                    <input orignal_sibling_v="{{$query_father->snam}}" value="{{$query_father->snam}}" class=" dis_able_father_edit col-xs-12 col-sm-10" type="text" name="parent_second_name" id="parent_second_name" placeholder="Second Name" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-username">Third Name</label>

                                <div class="col-sm-8">
                                    <input orignal_sibling_v="{{$query_father->thname}}" value="{{$query_father->thname}}" class=" dis_able_father_edit col-xs-12 col-sm-10" type="text" name="parent_third_name" id="parent_third_name" placeholder="Third Name" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-username">Family Name</label>

                                <div class="col-sm-8">
                                    <input orignal_sibling_v="{{$query_father->family_name}}" value="{{$query_father->family_name}}" class=" dis_able_father_edit col-xs-12 col-sm-10" type="text" name="parent_family_name" id="parent_family_name" placeholder="Family Name" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-email">Email</label>

                                <div class="col-sm-8">
																	<span class="input-icon input-icon-right">
																		<input orignal_sibling_v="{{$query_father->Email}}" value="{{$query_father->Email}}" class="dis_able_father_edit" type="email" name="parent_email" id="parent_email" >
																		<i class="ace-icon fa fa-envelope"></i>
																	</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-phone">Phone</label>

                                <div class="col-sm-8">
																	<span class="input-icon input-icon-right">
																		<input orignal_sibling_v="{{$query_father->Contact_No}}" value="{{$query_father->Contact_No}}" class=" dis_able_father_edit input-medium input-mask-phone" type="text" name="form_field_phone" id="form_field_phone">
																		<i class="ace-icon fa fa-phone fa-flip-horizontal"></i>
																	</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-username">ID Number</label>

                                <div class="col-sm-8">
                                    <input orignal_sibling_v="{{$query_father->ID_NO}}" value="{{$query_father->ID_NO}}" class=" dis_able_father_edit col-xs-12 col-sm-10" type="text" name="parent_id_number" id="parent_id_number" placeholder="ID Number" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-username">Passport ID</label>

                                <div class="col-sm-8">
                                    <input orignal_sibling_v="{{$query_father->Pass_ID}}" value="{{$query_father->Pass_ID}}" class=" dis_able_father_edit col-xs-12 col-sm-10" type="text" name="parent_passport_id" id="parent_passport_id" placeholder="Passport ID" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-comment">Address</label>

                                <div class="col-sm-8">
                                    <textarea orignal_sibling_v="{{$query_father->Address}}" class="dis_able_father_edit"  name="parent_address" id="parent_address" style="margin: 0px; width: 83.5%; height: 60px;">{{$query_father->Address}}</textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-username">Country</label>

                                <div class="col-sm-8">
                                    <select orignal_sibling_v="{{$query_father->Country_ID}}" selected_option="{{$query_father->Country_ID}}"  style="width: 83.5%;" class=" dis_able_father_edit col-sm-8  form-control" name="country_id" id="country_id">
                                        <option></option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="stock">Finance</label>
                                <div class="col-sm-8">
                                    @if($query_father->Finance == 1)
                                    <input orignal_sibling_v="{{$query_father->Finance}}" checked id="finance"  finance_v="1" data-toggle="toggle" type="checkbox"  data-on="Ready" data-off="Not Ready"  role="checkbox" class=" dis_able_father_edit FormElement ace ace-switch ace-switch-5">
                                    @else
                                        <input orignal_sibling_v="{{$query_father->Finance}}" id="finance"  finance_v="0" data-toggle="toggle" type="checkbox"  data-on="Ready" data-off="Not Ready"  role="checkbox" class=" dis_able_father_edit FormElement ace ace-switch ace-switch-5">
                                    @endif
                                    <span class="lbl"></span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-comment">Other Notes</label>

                                <div class="col-sm-8">
                                    <textarea orignal_sibling_v="{{$query_father->other_Notes}}" class="dis_able_father_edit" id="parent_notes" style="margin: 0px; width: 83.5%; height: 60px;">{{$query_father->other_Notes}}</textarea>
                                </div>
                            </div>
                            </form>

                        </div>

                        <div id="third_step_plane" class="step-pane" data-step="3">
                            <form class="form-horizontal" id="step3_validation-form" method="get" novalidate="novalidate">

                                <div id="enable_mother_edit" class="form-group" style="display: none;">
                                    <label class="col-sm-4 control-label no-padding-right" for="stock">Enable Edit</label>
                                    <div class="col-sm-8">
                                        <input  id="edit_mother_data" one_time_edit_mother="0"  edit_mother_data="0" data-toggle="toggle" type="checkbox"  data-on="Ready" data-off="Not Ready"  role="checkbox" class="ace ace-switch ace-switch-6">
                                        <span class="lbl"></span>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-username">First Name</label>

                                    <div class="col-sm-8">
                                        <input orignal_sibling_v="{{$query_mother->fname}}" edit_mother_id="{{$query_mother->par_id}}" value="{{$query_mother->fname}}" class=" dis_able_mother_edit col-xs-12 col-sm-10" type="text" id="mother_first_name" placeholder="First Name" >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-username">Second Name</label>

                                    <div class="col-sm-8">
                                        <input orignal_sibling_v="{{$query_mother->snam}}" value="{{$query_mother->snam}}" class=" dis_able_mother_edit col-xs-12 col-sm-10" type="text" id="mother_second_name" placeholder="Second Name" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-username">Third Name</label>

                                    <div class="col-sm-8">
                                        <input orignal_sibling_v="{{$query_mother->thname}}" value="{{$query_mother->thname}}" class=" dis_able_mother_edit col-xs-12 col-sm-10" type="text" id="mother_third_name" placeholder="Third Name" >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-username">Family Name</label>

                                    <div class="col-sm-8">
                                        <input orignal_sibling_v="{{$query_mother->family_name}}" value="{{$query_mother->family_name}}" class=" dis_able_mother_edit col-xs-12 col-sm-10" type="text" id="mother_family_name" placeholder="Family Name" >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-email">Email</label>

                                    <div class="col-sm-8">
																	<span class="input-icon input-icon-right">
																		<input orignal_sibling_v="{{$query_mother->Email}}" value="{{$query_mother->Email}}" class="dis_able_mother_edit" type="email" id="mother_email" >
																		<i class="ace-icon fa fa-envelope"></i>
																	</span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-phone">Phone</label>

                                    <div class="col-sm-8">
																	<span class="input-icon input-icon-right">
																		<input orignal_sibling_v="{{$query_mother->Contact_No}}" value="{{$query_mother->Contact_No}}" class=" dis_able_mother_edit input-medium input-mask-phone" type="text" id="mother_phone">
																		<i class="ace-icon fa fa-phone fa-flip-horizontal"></i>
																	</span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-username">ID Number</label>

                                    <div class="col-sm-8">
                                        <input orignal_sibling_v="{{$query_mother->ID_NO}}" value="{{$query_mother->ID_NO}}" class=" dis_able_mother_edit col-xs-12 col-sm-10" type="text" id="mother_id_number" placeholder="ID Number" >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-username">Passport ID</label>

                                    <div class="col-sm-8">
                                        <input orignal_sibling_v="{{$query_mother->Pass_ID}}" value="{{$query_mother->Pass_ID}}" class=" dis_able_mother_edit col-xs-12 col-sm-10" type="text" id="mother_passport_id" placeholder="Passport ID" >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-comment">Address</label>

                                    <div class="col-sm-8">
                                        <textarea orignal_sibling_v="{{$query_mother->Address}}" class="dis_able_mother_edit"  id="mother_address" style="margin: 0px; width: 83.5%; height: 60px;">{{$query_mother->Address}}</textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-username">Country</label>

                                    <div class="col-sm-8">
                                        <select orignal_sibling_v="{{$query_mother->Country_ID}}" selected_option="{{$query_mother->Country_ID}}" style="width: 83.5%;" class=" dis_able_mother_edit col-sm-8  form-control" id="mother_country_id">
                                            <option></option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="stock">Finance</label>
                                    <div class="col-sm-8">
                                        @if($query_mother->Finance == 1)
                                        <input orignal_sibling_v="{{$query_mother->Finance}}" checked  id="mother_finance"  finance_v="1" data-toggle="toggle" type="checkbox"  data-on="Ready" data-off="Not Ready"  role="checkbox" class=" dis_able_mother_edit FormElement ace ace-switch ace-switch-5">
                                        @else
                                        <input orignal_sibling_v="{{$query_mother->Finance}}" id="mother_finance"  finance_v="0" data-toggle="toggle" type="checkbox"  data-on="Ready" data-off="Not Ready"  role="checkbox" class=" dis_able_mother_edit FormElement ace ace-switch ace-switch-5">
                                        @endif
                                        <span class="lbl"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-comment">Other Notes</label>

                                    <div class="col-sm-8">
                                        <textarea orignal_sibling_v="{{$query_mother->other_Notes}}" class="dis_able_mother_edit" id="mother_notes" style="margin: 0px; width: 83.5%; height: 60px;">{{$query_mother->other_Notes}}</textarea>
                                    </div>
                                </div>

                            </form>
                        </div>

                      {{--  <div class="step-pane" data-step="4">
                            <div class="center">
                                <h3 class="green">Congrats!</h3>
                                Your product is ready to ship! Click finish to continue!
                            </div>
                        </div>--}}
                    </div>
                </div>

                <hr>
                <div class="wizard-actions">
                    <button class="btn btn-prev" disabled="disabled">
                        <i class="ace-icon fa fa-arrow-left"></i>
                        Prev
                    </button>

                    <button class="btn btn-success btn-next" data-last="Finish">
                        Next
                        <i class="ace-icon fa fa-arrow-right icon-on-right"></i>
                    </button>
                </div>
            </div><!-- /.widget-main -->
        </div><!-- /.widget-body -->
    </div>





    <!-- basic scripts -->

    <!--[if !IE]> -->
    <script src="{{asset('erp_panel/js/jquery-2.1.4.min.js')}}"></script>

    <!-- <![endif]-->

    <!--[if IE]>
    <script src="{{asset('erp_panel/js/jquery-1.11.3.min.js')}}"></script>
    <![endif]-->
    <script type="text/javascript">
        if('ontouchstart' in document.documentElement) document.write("<script src='{{asset('erp_panel/js/jquery.mobile.custom.min.js')}}'>"+"<"+"/script>");
    </script>
    <script src="{{asset('erp_panel/js/bootstrap.min.js')}}"></script>

    <!-- page specific plugin scripts -->
    <script src="{{asset('erp_panel/js/wizard.min.js')}}"></script>

    <script src="{{asset('erp_panel/js/jquery.validate.min.js')}}"></script>
    <script src="{{asset('erp_panel/js/jquery-additional-methods.min.js')}}"></script>
    <script src="{{asset('erp_panel/js/bootbox.js')}}"></script>
    <script src="{{asset('erp_panel/js/jquery.maskedinput.min.js')}}"></script>



    <script src="{{asset('erp_panel/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('erp_panel/js/jquery.jqGrid.min.js')}}"></script>
    <script src="{{asset('erp_panel/js/grid.locale-en.js')}}"></script>



    <script src="{{asset('erp_panel/js/jquery-ui.custom.min.js')}}"></script>
    <script src="{{asset('erp_panel/js/jquery.ui.touch-punch.min.js')}}"></script>
    <script src="{{asset('erp_panel/js/jquery.gritter.min.js')}}"></script>

    <script src="{{asset('erp_panel/js/jquery.easypiechart.min.js')}}"></script>
    <script src="{{asset('erp_panel/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('erp_panel/js/jquery.hotkeys.index.min.js')}}"></script>
    <script src="{{asset('erp_panel/js/bootstrap-wysiwyg.min.js')}}"></script>
    <script src="{{asset('erp_panel/js/select2.min.js')}}"></script>
    <script src="{{asset('erp_panel/js/spinbox.min.js')}}"></script>
    <script src="{{asset('erp_panel/js/bootstrap-editable.min.js')}}"></script>
    <script src="{{asset('erp_panel/js/ace-editable.min.js')}}"></script>


    <!-- ace scripts -->
    <script src="{{asset('erp_panel/js/ace-elements.min.js')}}"></script>
    <script src="{{asset('erp_panel/js/ace.min.js')}}"></script>

    <script src="{{asset('erp_panel/js/bootstrap-select.min.js')}}"></script>

    <script type="text/javascript">
        jQuery(function($) {






            // *** editable avatar *** //
            try {//ie8 throws some harmless exceptions, so let's catch'em

                //first let's add a fake appendChild method for Image element for browsers that have a problem with this
                //because editable plugin calls appendChild, and it causes errors on IE at unpredicted points
                try {
                    document.createElement('IMG').appendChild(document.createElement('B'));
                } catch(e) {
                    Image.prototype.appendChild = function(el){}
                }

                var last_gritter
                $('#avatar').editable({
                    type: 'image',
                    name: 'avatar',
                    value: null,
                    //onblur: 'ignore',  //don't reset or hide editable onblur?!
                    image: {
                        //specify ace file input plugin's options here
                        btn_choose: 'Change Avatar',
                        droppable: true,
                        maxSize: 110000,//~100Kb

                        //and a few extra ones here
                        name: 'avatar',//put the field name here as well, will be used inside the custom plugin
                        on_error : function(error_type) {//on_error function will be called when the selected file has a problem
                            if(last_gritter) $.gritter.remove(last_gritter);
                            if(error_type == 1) {//file format error
                                last_gritter = $.gritter.add({
                                    title: 'File is not an image!',
                                    text: 'Please choose a jpg|gif|png image!',
                                    class_name: 'gritter-error gritter-center'
                                });
                            } else if(error_type == 2) {//file size rror
                                last_gritter = $.gritter.add({
                                    title: 'File too big!',
                                    text: 'Image size should not exceed 100Kb!',
                                    class_name: 'gritter-error gritter-center'
                                });
                            }
                            else {//other error
                            }
                        },
                        on_success : function() {
                            $.gritter.removeAll();
                        }
                    },
                    url: function(params) {
                        // ***UPDATE AVATAR HERE*** //
                        //for a working upload example you can replace the contents of this function with
                        //examples/profile-avatar-update.js

                        var deferred = new $.Deferred

                        var value = $('#avatar').next().find('input[type=hidden]:eq(0)').val();
                        if(!value || value.length == 0) {
                            deferred.resolve();
                            return deferred.promise();
                        }


                        //dummy upload
                        setTimeout(function(){
                            if("FileReader" in window) {
                                //for browsers that have a thumbnail of selected image
                                var thumb = $('#avatar').next().find('img').data('thumb');
                                if(thumb) $('#avatar').get(0).src = thumb;
                            }

                            deferred.resolve({'status':'OK'});

                            if(last_gritter) $.gritter.remove(last_gritter);
                            /*last_gritter = $.gritter.add({
                                title: 'Avatar Updated!',
                                text: 'Uploading to server can be easily implemented. A working example is included with the template.',
                                class_name: 'gritter-info gritter-center'
                            });
*/
                        } , parseInt(Math.random() * 800 + 800))

                        return deferred.promise();

                        // ***END OF UPDATE AVATAR HERE*** //
                    },

                    success: function(response, newValue) {
                    }
                })
            }catch(e) {}

            ////////////////////
            //change profile
            $('[data-toggle="buttons"] .btn').on('click', function(e){
                var target = $(this).find('input[type=radio]');
                var which = parseInt(target.val());
                $('.user-profile').parent().addClass('hide');
                $('#user-profile-'+which).parent().removeClass('hide');
            });



            /////////////////////////////////////

        });
    </script>




    {{------------------  validate form  ---------------------}}
    <script type="text/javascript">
        jQuery(function($) {

            $('[data-rel=tooltip]').tooltip();

            $('.select2').css('width','200px').select2({allowClear:true})
                .on('change', function(){
                    $(this).closest('form').validate().element($(this));
                });


            var $validation = true;
            var $validation_father = true;
            $('#fuelux-wizard-container')
                .ace_wizard({
                    //step: 2 //optional argument. wizard will jump to step "2" at first
                    //buttons: '.wizard-actions:eq(0)'
                })
                .on('actionclicked.fu.wizard' , function(e, info){
                    if(info.step == 1 && $validation )  {
                        if(!$('#step1_validation-form').valid()) e.preventDefault();
                    }
                    if(info.step == 2 && $validation_father && info.direction === 'next') {
                        if(!$('#step2_validation-form').valid()) e.preventDefault();
                    }
                    /*if(info.step == 3 && $validation) {
                        if(!$('#step3_validation-form').valid()) e.preventDefault();
                    }*/
                })
                //.on('changed.fu.wizard', function() {
                //})
                .on('finished.fu.wizard', function(e) {
                    submit_all();

                }).on('stepclick.fu.wizard', function(e){
                //e.preventDefault();//this will prevent clicking and selecting steps
            });


            //jump to a step
            /**
             var wizard = $('#fuelux-wizard-container').data('fu.wizard')
             wizard.currentStep = 3;
             wizard.setState();
             */

            //determine selected step
            //wizard.selectedItem().step



            //hide or show the other form which requires validation
            //this is for demo only, you usullay want just one form in your application


            //documentation : http://docs.jquery.com/Plugins/Validation/validate


            $.mask.definitions['~']='[+-]';
            $('#phone').mask('(999) 999-9999');

            jQuery.validator.addMethod("phone", function (value, element) {
                return this.optional(element) || /^\(\d{3}\) \d{3}\-\d{4}( x\d{1,6})?$/.test(value);
            }, "Enter a valid phone number.");


        /*------------------------  step1 validate form  ------------------------------------*/
            var validator1 = $('#step1_validation-form').validate({
                errorElement: 'div',
                errorClass: 'help-block',
                focusInvalid: false,
                ignore: "",
                rules: {
                    first_name: {
                        required: true,
                    },
                    form_field_radio: {
                        required: true,
                    },
                    birth_date: {
                        required: true,

                    },
                    nationalty: {
                        required: true,
                    },
                    code: {
                        required: true
                    },
                    acadmic_year: {
                        required: true,
                    },
                    registration_date: {
                        required: true,
                    },
                    registration_class: {
                        required: true
                    },

                },

                messages: {
                    image: "Please choose .JPG /.PNG Image",
                    email: {
                        required: "Please provide a valid email.",
                        email: "Please provide a valid email."
                    },
                    password: {
                        required: "Please specify a password.",
                        minlength: "Please specify a secure password."
                    },
                    state: "Please choose state",
                    subscription: "Please choose at least one option",
                    gender: "Please choose gender",
                    agree: "Please accept our policy"
                },


                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
                },

                success: function (e) {
                    $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
                    $(e).remove();
                },

                errorPlacement: function (error, element) {
                    if(element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
                        var controls = element.closest('div[class*="col-"]');
                        if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
                        else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                    }
                    else if(element.is('.select2')) {
                        error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                    }
                    else if(element.is('.chosen-select')) {
                        error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                    }
                    else error.insertAfter(element.parent());
                },

                submitHandler: function (form) {
                },
                invalidHandler: function (form) {
                }
            });
            /*validator1.showErrors({
                "image_input": "Please choose .JPG /.PNG Image"
            });
*/
        /*------------------------  step2 validate form  ------------------------------------*/
          var validator2 = $('#step2_validation-form').validate({
                errorElement: 'div',
                errorClass: 'help-block',
                focusInvalid: false,
                ignore: "",
                rules: {
                    parent_first_name: {
                        required: true,
                    },
                    parent_second_name: {
                        required: true,
                    },
                    parent_third_name: {
                        required: true,

                    },
                    parent_family_name: {
                        required: true,
                    },
                    parent_email: {
                        required: true,
                        email:true
                    },
                    form_field_phone: {
                        required: true,

                    },
                   /* parent_id_number: {
                        required: true,
                    },
                    parent_passport_id: {
                        required: true
                    },*/
                    parent_address: {
                        required: true
                    },
                    country_id: {
                        required: true
                    },
                },

                messages: {
                    email: {
                        required: "Please provide a valid email.",
                        email: "Please provide a valid email."
                    },
                    password: {
                        required: "Please specify a password.",
                        minlength: "Please specify a secure password."
                    },
                    state: "Please choose state",
                    subscription: "Please choose at least one option",
                    gender: "Please choose gender",
                    agree: "Please accept our policy"
                },


                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
                },

                success: function (e) {
                    $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
                    $(e).remove();
                },

                errorPlacement: function (error, element) {
                    if(element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
                        var controls = element.closest('div[class*="col-"]');
                        if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
                        else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                    }
                    else if(element.is('.select2')) {
                        error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                    }
                    else if(element.is('.chosen-select')) {
                        error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                    }
                    else error.insertAfter(element.parent());
                },

                submitHandler: function (form) {
                },
                invalidHandler: function (form) {
                }
            });

            /*--------------------------------  step3 validate form -----------------------------------------------*/
           /* $('#step3_validation-form').validate({
                errorElement: 'div',
                errorClass: 'help-block',
                focusInvalid: false,
                ignore: "",
                rules: {
                    first_name: {
                        required: true,
                    },
                    form_field_radio: {
                        required: true,
                    },
                    birth_date: {
                        required: true,

                    },
                    nationalty: {
                        required: true,
                    },
                    code: {
                        required: true
                    },
                    acadmic_year: {
                        required: true,
                    },
                    registration_date: {
                        required: true,
                    },
                    registration_class: {
                        required: true
                    }
                },

                messages: {
                    email: {
                        required: "Please provide a valid email.",
                        email: "Please provide a valid email."
                    },
                    password: {
                        required: "Please specify a password.",
                        minlength: "Please specify a secure password."
                    },
                    state: "Please choose state",
                    subscription: "Please choose at least one option",
                    gender: "Please choose gender",
                    agree: "Please accept our policy"
                },


                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
                },

                success: function (e) {
                    $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
                    $(e).remove();
                },

                errorPlacement: function (error, element) {
                    if(element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
                        var controls = element.closest('div[class*="col-"]');
                        if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
                        else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                    }
                    else if(element.is('.select2')) {
                        error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                    }
                    else if(element.is('.chosen-select')) {
                        error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                    }
                    else error.insertAfter(element.parent());
                },

                submitHandler: function (form) {
                },
                invalidHandler: function (form) {
                }
            });*/
            /*----------------------------------------------------------------------------------*/



            $('#modal-wizard-container').ace_wizard();
            $('#modal-wizard .wizard-actions .btn[data-dismiss=modal]').removeAttr('disabled');


            /**
             $('#date').datepicker({autoclose:true}).on('changeDate', function(ev) {
					$(this).closest('form').validate().element($(this));
				});

             $('#mychosen').chosen().on('change', function(ev) {
					$(this).closest('form').validate().element($(this));
				});
             */


            $(document).one('ajaxloadstart.page', function(e) {
                //in ajax mode, remove remaining elements before leaving page
                $('[class*=select2]').remove();
            });
        })
    </script>
    {{---------------------------------------------------------------}}






    {{-----------------  side bar active ---------------------}}
    <script>

        $('#basics').addClass('open');
        $('#admission').addClass('open');
        $('#student').addClass('active');
        $('.submenu nav-show').css('display', 'block');

    </script>

    {{---------------------- leave switch ---------------------}}
    <script>

        $(document).on('change','#leave_value',function () {
                if ($('#leave_value').prop('checked') == true) {
                      $(this).attr('get_v',1);
                    $('#l_date').css('display', 'block');
                    /*console.log($('#image_input').val());*/


                    /*console.log($('input[name=form_field_radio]:checked').attr('gender'));*/

                }
                else{
                    $('#l_date').css('display', 'none');
                    $(this).attr('get_v',0);

                }
        });


    </script>
{{--------------------------------- has sibling---------------------------------------------------}}
    <script>

        $(document).on('change','#sibling_value',function () {
                if ($('#sibling_value').prop('checked') == true) {
                      $(this).attr('sibling_v',1);
                    $('#all_sibling').css('display', 'block');
                    $('#all_sibling').children('div').addClass('open');
                    get_all_student_data();
                    console.log($(this).attr('sibling_v'));
                    console.log($('#sibling_selected_v').val());




                    /*console.log($('input[name=form_field_radio]:checked').attr('gender'));*/

                }
                else{
                    $('#all_sibling').css('display', 'none');
                    $(this).attr('sibling_v',0);
                    console.log($(this).attr('sibling_v'));
                    console.log($('#sibling_selected_v').val());
                    /*clear_return_father_data();
                    clear_return_mother_data();*/
                    get_orignal_siblings_data()
                    first_fill_siblings_table();

                }
        });


    </script>


    {{----------------------------------  change image switch ---------------------------------------------}}
    <script>

        $(document).on('change','#change_student_image',function () {
                if ($('#change_student_image').prop('checked') == true) {

                    $('#show_student_image').css('display', 'none');
                    $('#show_add_image').css('display', 'block');

                }
                else
                {
                    $('#show_student_image').css('display', 'block');
                    $('#show_add_image').css('display', 'none');
                    $('#id-input-file-3').val('');

                }
        });


    </script>


{{------------------------------------  enable father edit ------------------------------------------------------------}}
    <script>

        $(document).on('change','#edit_father_data',function () {
            if ($('#edit_father_data').prop('checked') == true) {
                $(this).attr('edit_father_data',1);
                $(this).attr('one_time_edit_father',1);

                console.log($(this).attr('edit_father_data'));
                console.log($(this).attr('one_time_edit_father'));

                $('#step2_validation-form').find('.dis_able_father_edit').removeAttr('disabled');




                /*console.log($('input[name=form_field_radio]:checked').attr('gender'));*/

            }
            else{

                $(this).attr('edit_father_data',0);
                console.log($(this).attr('edit_father_data'));
                console.log($(this).attr('one_time_edit_father'));

                $('#step2_validation-form').find('.dis_able_father_edit').attr('disabled','disabled');

            }
        });


    </script>


 {{------------------------------------  enable mother edit ------------------------------------------------------------}}
    <script>

        $(document).on('change','#edit_mother_data',function () {
            if ($('#edit_mother_data').prop('checked') == true) {
                $(this).attr('edit_mother_data',1);
                $(this).attr('one_time_edit_mother',1);

                console.log($(this).attr('edit_mother_data'));
                console.log($(this).attr('one_time_edit_mother'));

                $('#step3_validation-form').find('.dis_able_mother_edit').removeAttr('disabled');




                /*console.log($('input[name=form_field_radio]:checked').attr('gender'));*/

            }
            else{

                $(this).attr('edit_mother_data',0);
                console.log($(this).attr('edit_mother_data'));
                console.log($(this).attr('one_time_edit_mother'));

                $('#step3_validation-form').find('.dis_able_mother_edit').attr('disabled','disabled');

            }
        });


    </script>



{{---------------------------------  load sibling on select change ------------------------------------------------}}
    <script>

        $(document).on('change','#sibling_selected_v',function () {

            return_father_data();
            fill_siblings_table();


        });


    </script>

    {{------------------------------------ fill siblings table ------------------------------------------}}
    <script>
        function fill_siblings_table()
        {
            var student_selcted_id = $('#sibling_selected_v').val();
            /*var student_selcted_id = $('#sibling_selected_v').attr('defult_selected_value');*/


            var has_siblings = $('#sibling_value').attr('sibling_v');

            console.log(student_selcted_id);

            var $t_body = $("#siblings_table_row");


         if(student_selcted_id != "nothing"  &&  has_siblings==1)
            {
                console.log("in siblings fill data");
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/fill_siblings_table_edit/',
                    dataType : 'json',
                    type: 'get',
                    data: {
                        student_selcted_id:student_selcted_id,

                    },

                    success:function(response) {
                        console.log(response);

                        $('#sibling_delete_op').attr('delete_option_all',0);

                        $t_body.find('tr').remove();
                        for(var i =0;i<response.length;i++)
                        {
                            var no = i+1;


                            $t_body.append('<tr  class="jqgrow ui-row-ltr ui-widget-content ui-priority-secondary">'+

                                '<td>'+(no)+'</td>'+
                                '<td>'+response[i]['First_name']+" "+ response[i]['fname']+" "+ response[i]['snam']+" "+ response[i]['family_name']+'</td>'+
                                '<td>'+response[i]['code']+'</td>'+
                                '<td>'+response[i]['classname']+'</td>'+
                                '</tr>');
                        }



                    }

                });
            }
            else
                {
                    $('#sibling_delete_op').attr('delete_option_all',1);
                    $t_body.find('tr').remove();
                    console.log( 'from' +$('#sibling_selected_v').val());
                    console.log( 'has' +$('#sibling_value').attr('sibling_v'));
                }


        }


    </script>


    {{------------------------------------ first fill siblings table ------------------------------------------}}
    <script>
        function first_fill_siblings_table() {

            var student_selcted_id = $('#sibling_selected_v').attr('defult_selected_value');


            console.log(student_selcted_id);
            console.log("in siblings fill data");
            var $t_body = $("#siblings_table_row");


            console.log("in siblings fill data");
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/fill_siblings_table_edit/',
                dataType: 'json',
                type: 'get',
                data: {
                    student_selcted_id: student_selcted_id,

                },

                success: function (response) {
                    console.log(response);
                    $t_body.find('tr').remove();
                    for (var i = 0; i < response.length; i++) {
                        var no = i + 1;

                        /*$dropdown.append($("<option />")/!*.data-tokens(response[i]['code']+" "+response[i]['First_name']+" "+ response[i]['fname']+" "+ response[i]['snam']+" "+ response[i]['family_name'])*!/.val(response[i]['code']).text(response[i]['First_name']+" "+ response[i]['fname']+" "+ response[i]['snam']+" "+ response[i]['family_name']));*/
                        /*$t_body.append('<option   value="'+response[i]['SID']+'" >'+response[i]['First_name']+" "+ response[i]['fname']+" "+ response[i]['snam']+" "+ response[i]['family_name']+'</option>');*/

                        $t_body.append('<tr  class="jqgrow ui-row-ltr ui-widget-content ui-priority-secondary">' +

                            '<td>' + (no) + '</td>' +
                            '<td>' + response[i]['First_name'] + " " + response[i]['fname'] + " " + response[i]['snam'] + " " + response[i]['family_name'] + '</td>' +
                            '<td>' + response[i]['code'] + '</td>' +
                            '<td>' + response[i]['classname'] + '</td>' +
                            '</tr>');
                    }


                }

            });

        }



    </script>


    {{-------------------------- same mother check box  -------------------------------------}}
    <script>

        $(document).on('change','#same_mother',function () {
                if ($('input[name=same_mother]:checked').length>0) {
                      $(this).attr('same_mother_v',1);
                    console.log($(this).attr('same_mother_v'));
                    return_mother_data();

                    /*console.log($('input[name=form_field_radio]:checked').attr('gender'));*/

                }
                else{

                    $(this).attr('same_mother_v',0);
                    console.log($(this).attr('same_mother_v'));
                    clear_return_mother_data();


                }
        });


    </script>
    {{------------------------------------ father finance switch ------------------------------------------------}}
    <script>

        $(document).on('change','#finance',function () {
                if ($('#finance').prop('checked') == true) {
                      $(this).attr('finance_v',1);
                     /*  console.log($(this).attr('finance_v'));*/

                    /*console.log($('input[name=form_field_radio]:checked').attr('gender'));*/

                }
                else{

                    $(this).attr('finance_v',0);
                    console.log($(this).attr('finance_v'));
                }
        });


    </script>


    {{-------------------------------------  mother finance switch -------------------------------------------------------}}
    <script>

        $(document).on('change','#mother_finance',function () {
                if ($('#mother_finance').prop('checked') == true) {
                      $(this).attr('finance_v',1);
                     /*  console.log($(this).attr('finance_v'));*/

                    /*console.log($('input[name=form_field_radio]:checked').attr('gender'));*/

                }
                else{

                    $(this).attr('finance_v',0);
                    console.log($(this).attr('finance_v'));
                }
        });


    </script>

    {{-----------------------------------------   nationality  ---------------------------------------------------------}}
    <script>



        function update_nationality()
        {

            var $dropdown = $("#nationalty");

           /* $dropdown.find('option').remove();*/

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/fill_nationality_option/',
                dataType : 'json',
                type: 'get',
                data: {

                },

                success:function(response) {

                    /*console.log(response);*/
                    $dropdown.find('option').remove();

                    for(var i =0;i<response.length;i++)
                    {
                        if(response[i]['id'] == $dropdown.attr('selected_option')){
                        $dropdown.append($("<option selected />").val(response[i]['id']).text(response[i]['type']));
                        }
                        else{
                            $dropdown.append($("<option />").val(response[i]['id']).text(response[i]['type']));
                        }
                    }
                }

            });

        };

        $(document).on('focus','#nationalty',function () {
            update_nationality();
        });

    </script>

    {{-------------------------------------------    acadimic -------------------------------------------------------}}
    <script>



        function acadimic_year()
        {

            var $dropdown = $("#acadmic_year");

            /*$dropdown.find('option').remove();*/

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/fill_acadimic_option/',
                dataType : 'json',
                type: 'get',
                data: {

                },

                success:function(response) {

                    /*console.log(response);*/
                    $dropdown.find('option').remove();

                    for(var i =0;i<response.length;i++)
                    {
                        if(response[i]['acadimic_id'] == $dropdown.attr('selected_option'))
                        {
                        $dropdown.append($("<option selected />").val(response[i]['acadimic_id']).text(response[i]['year_name']));
                        }
                        else
                            {
                                $dropdown.append($("<option/>").val(response[i]['acadimic_id']).text(response[i]['year_name']));
                            }

                    }
                }

            });

        };

        $(document).on('focus','#acadmic_year',function () {
            acadimic_year();
        });

    </script>
    {{---------------------------------------------   rigistration -----------------------------------------------------}}
    <script>



        function rigistration_class()
        {

            var $dropdown = $("#registration_class");

           /* $dropdown.find('option').remove();*/

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/fill_registration_option/',
                dataType : 'json',
                type: 'get',
                data: {

                },

                success:function(response) {

                    /*console.log(response);*/
                    $dropdown.find('option').remove();

                    for(var i =0;i<response.length;i++)
                    {
                        if(response[i]['classid'] == $dropdown.attr('selected_option'))
                        {
                        $dropdown.append($("<option selected/>").val(response[i]['classid']).text(response[i]['classname']));
                        }
                        else
                            {
                                $dropdown.append($("<option />").val(response[i]['classid']).text(response[i]['classname']));
                            }
                    }
                }

            });

        };

        $(document).on('focus','#registration_class',function () {
            rigistration_class();
        });

    </script>
    {{------------------------------------------------  countries --------------------------------------------------}}
    <script>



        function countries_get()
        {

            var $dropdown = $("#country_id");


            /*$dropdown.find('option').remove();*/

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/fill_countries_option/',
                dataType : 'json',
                type: 'get',
                data: {

                },

                success:function(response) {

                    /*console.log(response);*/
                    $dropdown.find('option').remove();

                    for(var i =0;i<response.length;i++)
                    {
                        if(response[i]['id'] == $dropdown.attr('selected_option'))
                        {
                        $dropdown.append($("<option selected />").val(response[i]['id']).text(response[i]['name']));
                        }
                        else
                            {
                                $dropdown.append($("<option />").val(response[i]['id']).text(response[i]['name']));
                            }
                    }
                }

            });

        };

        $(document).on('focus','#country_id',function () {
            countries_get();
        });

    </script>

    {{--------------------------------------------------  mother_countries  ------------------------------------------------------------}}

    <script>



        function m_countries_get()
        {

            var $dropdown = $("#mother_country_id");


            /*$dropdown.find('option').remove();*/

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/fill_mother_countries_option/',
                dataType : 'json',
                type: 'get',
                data: {

                },

                success:function(response) {

                    /*console.log(response);*/
                    $dropdown.find('option').remove();

                    for(var i =0;i<response.length;i++)
                    {
                        if(response[i]['id'] == $dropdown.attr('selected_option'))
                        {
                        $dropdown.append($("<option selected />").val(response[i]['id']).text(response[i]['name']));
                        }
                        else
                            {
                                $dropdown.append($("<option />").val(response[i]['id']).text(response[i]['name']));
                            }
                    }
                }

            });

        };

        $(document).on('focus','#mother_country_id',function () {
            m_countries_get();
        });

    </script>

    {{------------------------------------------ get all student data for select siblings-----------------------------------------------------------------------}}
    <script>



        function get_all_student_data()
        {

            var $dropdown = $("#sibling_selected_v");


            var current_student = $('#sibling_selected_v').attr('defult_selected_value');


            console.log('in fill select');
             $dropdown.find('option').remove();

            $dropdown.append('<option style="display: none;"  data-tokens=" ">'+""+'</option>');
            $dropdown.append('<option  class="btn btn-xs btn-danger "  value="nothing" data-tokens=" Remove cancel">'+"Remove Siblings"+'</option>');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/get_all_students_datafor_edit/',
                dataType : 'json',
                type: 'get',
                data: {
                    current_student:current_student,
                },

                success:function(response) {

                    console.log(response);
                    /*$dropdown.find('option').remove();*/
                /*<option data-tokens="1 expenses" value="1">expenses</option>*/
                    for(var i =0;i<response.length;i++)
                    {
                        /*$dropdown.append($("<option />")/!*.data-tokens(response[i]['code']+" "+response[i]['First_name']+" "+ response[i]['fname']+" "+ response[i]['snam']+" "+ response[i]['family_name'])*!/.val(response[i]['code']).text(response[i]['First_name']+" "+ response[i]['fname']+" "+ response[i]['snam']+" "+ response[i]['family_name']));*/
                        $dropdown.append('<option   value="'+response[i]['SID']+'" data-tokens="'+response[i]['code']+" "+response[i]['First_name']+" "+ response[i]['fname']+" "+ response[i]['snam']+" "+ response[i]['family_name'] +'">'+response[i]['First_name']+" "+ response[i]['fname']+" "+ response[i]['snam']+" "+ response[i]['family_name']+'</option>');
                    }
                    $dropdown.selectpicker('refresh');
                }

            });

        };

        /*$(document).on('focus','#nationalty',function () {
            update_nationality();
        });*/

    </script>



    {{----------------------------------------  summit all form --------------------------------------------------------------}}
    <script>

        // convert form to json

        function toJSONString( form ) {
            var obj = {};
            var elements = form.querySelectorAll( "input, select, textarea" );
            for( var i = 0; i < elements.length; ++i ) {
                var element = elements[i];
                var name = element.name;
                var value = element.value;

                if( name ) {
                    obj[ name ] = value;
                }
            }

            return JSON.stringify( obj );
        }




        function submit_all()
        {
/*------------------------------------- student data----------------------------------------------------------------------*/

            var get_only_the_date =   $('#registration_date').val().split(' ')[0];

            var dt = new Date();
            var time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();

            var first_name = $('#first_name').val();

            var img = $('#id-input-file-3').val();

            var edit_student_id = $('#first_name').attr('edit_student_id');


            var birth_day = $('#birth_date').val();
            var gender = $('input[name=form_field_radio]:checked').attr('gender');
            var nationality = $('#nationalty').val();
            var code = $('#code').val();
            var acadmic_year = $('#acadmic_year').val();
            var registration_date = get_only_the_date+ " " + time;
            var registration_class = $('#registration_class').val();

            var leave_v = $('#leave_value').attr('get_v');
            var leave_date = $('#leave_date').val();

            var has_siblings = $('#sibling_value').attr('sibling_v');
            var sibling_student_id = $('#sibling_selected_v').val();

            var has_same_mother = $('#same_mother').attr('same_mother_v');

            var enable_mother_edit = $('#edit_mother_data').attr('one_time_edit_mother');

            var enable_father_edit = $('#edit_father_data').attr('one_time_edit_father');

            var remove_all_delete = $('#sibling_delete_op').attr('delete_option_all');


            /*console.log(has_siblings,sibling_student_id);*/
/*-----------------------------------------------------------------------------------------------------------------------*/
/*--------------------------------- father data ----------------------------------------------------------------*/
            var first_parent_name = $('#parent_first_name').val();
            var second_parent_name = $('#parent_second_name').val();
            var third_parent_name = $('#parent_third_name').val();
            var family_parent_name = $('#parent_family_name').val();
            var parent_email = $('#parent_email').val();
            var parent_phone = $('#form_field_phone').val();
            var parent_id_number = $('#parent_id_number').val();
            var parent_passport_number = $('#parent_passport_id').val();
            var parent_address = $('#parent_address').val();
            var parent_country = $('#country_id').val();

            var main_type = 1;
            var parent_finance = $('#finance').attr('finance_v');

            var parent_note = $('#parent_notes').val();

            var edit_father_id = $('#parent_first_name').attr('edit_father_id');

/*------------------------------------------------------------------------------------------------------------------------------------------------*/
/*--------------------------------- mother data ----------------------------------------------------------------*/
            var mother_first_parent_name = $('#mother_first_name').val();
            var mother_second_parent_name = $('#mother_second_name').val();
            var mother_third_parent_name = $('#mother_third_name').val();
            var mother_family_parent_name = $('#mother_family_name').val();
            var mother_email = $('#mother_email').val();
            var mother_phone = $('#mother_phone').val();
            var mother_id_number = $('#mother_id_number').val();
            var mother_passport_number = $('#mother_passport_id').val();
            var mother_address = $('#mother_address').val();
            var mother_country = $('#mother_country_id').val();

            var mother_main_type = 0;
            var mother_finance = $('#mother_finance').attr('finance_v');

            var mother_note = $('#mother_notes').val();

            var edit_mother_id = $('#mother_first_name').attr('edit_mother_id');

/*------------------------------------------------------------------------------------------------------------------------------------------------*/

            /* console.log(class_name);
             console.log(cost_code);*/



            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/edit_all_form/',
                dataType : 'json',
                type: 'get',
                data: {

                    /*------- mother data -------*/
                    mother_first_parent_name:mother_first_parent_name,
                    mother_second_parent_name:mother_second_parent_name,
                    mother_third_parent_name:mother_third_parent_name,
                    mother_family_parent_name:mother_family_parent_name,
                    mother_email:mother_email,
                    mother_phone:mother_phone,
                    mother_id_number:mother_id_number,
                    mother_passport_number:mother_passport_number,
                    mother_address:mother_address,
                    mother_country:mother_country,
                    mother_finance:mother_finance,
                    mother_note:mother_note,
                    mother_main_type:mother_main_type,
                    edit_mother_id:edit_mother_id,



                    /*------- father data -------*/
                    first_parent_name:first_parent_name,
                    second_parent_name:second_parent_name,
                    third_parent_name:third_parent_name,
                    family_parent_name:family_parent_name,
                    parent_email:parent_email,
                    parent_phone:parent_phone,
                    parent_id_number:parent_id_number,
                    parent_passport_number:parent_passport_number,
                    parent_address:parent_address,
                    parent_country:parent_country,
                    parent_finance:parent_finance,
                    parent_note:parent_note,
                    main_type:main_type,
                    edit_father_id:edit_father_id,


                    /*------- student data -------*/
                    first_name:first_name,
                    img:img,
                    birth_day:birth_day,
                    gender:gender,
                    nationality:nationality,
                    code:code,
                    acadmic_year:acadmic_year,
                    registration_date:registration_date,
                    registration_class:registration_class,
                    leave_v:leave_v,
                    leave_date:leave_date,
                    has_siblings:has_siblings,
                    sibling_student_id:sibling_student_id,
                    has_same_mother:has_same_mother,
                    enable_mother_edit:enable_mother_edit,
                    enable_father_edit:enable_father_edit,
                    edit_student_id:edit_student_id,
                    remove_all_delete:remove_all_delete,


                },

                success:function(response) {
                    console.log(response);

                    var old_stu_name = $('#first_name').attr(('old_img_value'));
                    var old_stu_code = $('#code').attr(('old_img_value'));
                    var old_img = old_stu_name+'_'+old_stu_code+'.jpg';

                    var form = document.getElementById("step1_validation-form");
                    var formData = new FormData(form);
                    formData.append( '_token', '{{csrf_token()}}' );
                    formData.append('old_image',old_img);

                    var request = new XMLHttpRequest();
                    request.open("POST", "{{route('image_send')}}");

                    request.send(formData);

                    console.log(old_img);




                    /*$('#reset_btn').click();
                    $('#reset_student_btn').click();
                    $('#reset_mother_btn').click();
                    $('#student_btn_').click();*/
                   /* $('#first_step').addClass('active');
                    $('#second_step').removeClass('complete active');
                    $('#third_step').removeClass('complete active');
                    $('#first_step').removeClass('complete');


                    $('#first_step_plane').addClass('active');
                    $('#second_step_plane').removeClass('active');
                    $('#third_step_plane').removeClass('active');
                    */
                    bootbox.dialog({
                        message: "student successfully updated ",
                        buttons: {
                            "success" : {
                                "label" : "OK",
                                "className" : "btn-sm btn-primary",
                                "callback": function() {
                                    location.href='/all_students/';
                                }
                            }
                        }
                    });



                    /*$('#show_img').attr('src',response['image_s'])*/
                    /*window.location = window.location;*/




                    /*$('#gbox_grid-table').load(' #gbox_grid-table');*/
                    /* call_table();*/




                },
                error: function() {

                }

            });





        }

    </script>
    {{-------------------------------------------- return father data  -----------------------------------------------------}}

    <script>

        function return_father_data()
        {

            var student_selcted_id = $('#sibling_selected_v').val();
            var has_siblings = $('#sibling_value').attr('sibling_v');
            console.log('selected value'+student_selcted_id);

            /*if(student_selcted_id == "nothing"&&student_selcted_id != "defult" && has_siblings==1)
            {
                $('#sibling_delete_op').attr('delete_option_all',1);
                console .log('delete'+$('#sibling_delete_op').attr('delete_option_all'));
                var $t_body = $("#siblings_table_row");
                $t_body.find('tr').remove();

            }*/
        if(student_selcted_id != "nothing"&& has_siblings==1)
            {
                console.log("in return data");
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/return_father_data/',
                dataType : 'json',
                type: 'get',
                data: {
                    student_selcted_id:student_selcted_id,

                },

                success:function(response) {

                    $('#sibling_delete_op').attr('delete_option_all',0);

                    console.log(response);
                    $('#same_mother').prop('checked',false);
                    clear_return_mother_data();


                    $('#enable_father_edit').css('display','block');

                    $('#parent_first_name').val(response['father_data'][0]['fname']);
                    $('#parent_second_name').val(response['father_data'][0]['snam']);
                    $('#parent_third_name').val(response['father_data'][0]['thname']);
                    $('#parent_family_name').val(response['father_data'][0]['family_name']);
                    $('#parent_email').val(response['father_data'][0]['Email']);
                    $('#form_field_phone').val(response['father_data'][0]['Contact_No']);
                    $('#parent_id_number').val(response['father_data'][0]['ID_NO']);
                    $('#parent_passport_id').val(response['father_data'][0]['Pass_ID']);
                    $('#parent_address').val(response['father_data'][0]['Address']);
                    $('#country_id').val(response['father_data'][0]['Country_ID']);

                    /*$('#finance').val(response['father_data'][0]['Finance']);*/
                    if(response['father_data'][0]['Finance'] == 1)
                    {
                    $('#finance').prop('checked',true);
                        $('#finance').attr('finance_v',1);
                    }
                    else{
                        $('#finance').prop('checked',false);
                        $('#finance').attr('finance_v',0);
                    }

                    $('#parent_notes').val(response['father_data'][0]['other_Notes']);


                    $('#step2_validation-form').find('.dis_able_father_edit').attr('disabled','disabled');





                }

            });
            }
            else
                {
                    $('#sibling_delete_op').attr('delete_option_all',1);
            clear_return_father_data();
            clear_return_mother_data();
        }

        };

    </script>
    {{------------------------------------  clear father data ------------------------------------------------------------}}
    <script>

        function clear_return_father_data()
        {

            $('#enable_father_edit').css('display','none');
            /*$('#sibling_value').attr('sibling_v',0);*/

                        $('#parent_first_name').val("");
                        $('#parent_second_name').val("");
                        $('#parent_third_name').val("");
                        $('#parent_family_name').val("");
                        $('#parent_email').val("");
                        $('#form_field_phone').val("");
                        $('#parent_id_number').val("");
                        $('#parent_passport_id').val("");
                        $('#parent_address').val("");
                        $('#country_id').val("");

                        /*$('#finance').val(response['father_data'][0]['Finance']);*/


                            $('#finance').prop('checked',false);
                            $('#finance').attr('finance_v',0);


                        $('#parent_notes').val("");


                        $('#step2_validation-form').find('.dis_able_father_edit').removeAttr('disabled');





        }

    </script>


    {{---------------------------------------------  return mother data -----------------------------------------------------------------------}}
    <script>

        function return_mother_data()
        {

            var student_selcted_id = $('#sibling_selected_v').val();
            var has_siblings = $('#sibling_value').attr('sibling_v');

            if(student_selcted_id != "nothing" && has_siblings==1)
            {
                console.log("in return data");
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/return_mother_data/',
                    dataType : 'json',
                    type: 'get',
                    data: {
                        student_selcted_id:student_selcted_id,

                    },

                    success:function(response) {
                        console.log(response);

                        $('#enable_mother_edit').css('display','block');

                        $('#mother_first_name').val(response['mother_data'][0]['fname']);
                        $('#mother_second_name').val(response['mother_data'][0]['snam']);
                        $('#mother_third_name').val(response['mother_data'][0]['thname']);
                        $('#mother_family_name').val(response['mother_data'][0]['family_name']);
                        $('#mother_email').val(response['mother_data'][0]['Email']);
                        $('#mother_phone').val(response['mother_data'][0]['Contact_No']);
                        $('#mother_id_number').val(response['mother_data'][0]['ID_NO']);
                        $('#mother_passport_id').val(response['mother_data'][0]['Pass_ID']);
                        $('#mother_address').val(response['mother_data'][0]['Address']);
                        $('#mother_country_id').val(response['mother_data'][0]['Country_ID']);

                        /*$('#finance').val(response['father_data'][0]['Finance']);*/
                        if(response['mother_data'][0]['Finance'] == 1)
                        {
                            $('#mother_finance').prop('checked',true);
                            $('#mother_finance').attr('finance_v',1);
                        }
                        else{
                            $('#mother_finance').prop('checked',false);
                            $('#mother_finance').attr('finance_v',0);
                        }

                        $('#mother_notes').val(response['mother_data'][0]['other_Notes']);


                        $('#step3_validation-form').find('.dis_able_mother_edit').attr('disabled','disabled');

                    }

                });
            }
            else{clear_return_mother_data();}

        };

    </script>


    {{----------------------------------------------- clear mother data -----------------------------------------------}}
    <script>

        function clear_return_mother_data()
        {

            $('#enable_mother_edit').css('display','none');
            $('#same_mother').attr('same_mother_v',0);

            $('#mother_first_name').val("");
            $('#mother_second_name').val("");
            $('#mother_third_name').val("");
            $('#mother_family_name').val("");
            $('#mother_email').val("");
            $('#mother_phone').val("");
            $('#mother_id_number').val("");
            $('#mother_passport_id').val("");
            $('#mother_address').val("");
            $('#mother_country_id').val("");

            $('#mother_finance').prop('checked',false);
            $('#mother_finance').attr('finance_v',0);

            $('#mother_notes').val("");



            $('#step3_validation-form').find('.dis_able_mother_edit').removeAttr('disabled');



        }

    </script>
    {{-----------------------------------  get orignal sibling data -----------------------------------------------------}}
    <script>
        function get_orignal_siblings_data()
        {
            /*-------------------------  father orginal values --------------------------------------------*/
            orignal_father_fname = $('#parent_first_name').attr('orignal_sibling_v');
            orignal_father_secname = $('#parent_second_name').attr('orignal_sibling_v');
            orignal_father_thirdname = $('#parent_third_name').attr('orignal_sibling_v');

            orignal_father_familyname = $('#parent_family_name').attr('orignal_sibling_v');
            orignal_father_email = $('#parent_email').attr('orignal_sibling_v');
            orignal_father_phone = $('#form_field_phone').attr('orignal_sibling_v');
            orignal_father_id_number = $('#parent_id_number').attr('orignal_sibling_v');

            orignal_father_passport_number = $('#parent_passport_id').attr('orignal_sibling_v');
            orignal_father_address = $('#parent_address').attr('orignal_sibling_v');

            orignal_father_country_id = $('#country_id').attr('orignal_sibling_v');
            orignal_father_finance = $('#finance').attr('orignal_sibling_v');
            orignal_father_notes = $('#parent_notes').attr('orignal_sibling_v');
            /*----------------------------------------------------------------------------------------------*/


            /*-------------------------  mother orginal values --------------------------------------------*/
            orignal_mother_fname = $('#mother_first_name').attr('orignal_sibling_v');
            orignal_mother_secname = $('#mother_second_name').attr('orignal_sibling_v');
            orignal_mother_thirdname = $('#mother_third_name').attr('orignal_sibling_v');

            orignal_mother_familyname = $('#mother_family_name').attr('orignal_sibling_v');
            orignal_mother_email = $('#mother_email').attr('orignal_sibling_v');
            orignal_mother_phone = $('#mother_phone').attr('orignal_sibling_v');
            orignal_mother_id_number = $('#mother_id_number').attr('orignal_sibling_v');

            orignal_mother_passport_number = $('#mother_passport_id').attr('orignal_sibling_v');
            orignal_mother_address = $('#mother_address').attr('orignal_sibling_v');

            orignal_mother_country_id = $('#mother_country_id').attr('orignal_sibling_v');
            orignal_mother_finance = $('#mother_finance').attr('orignal_sibling_v');
            orignal_mother_notes = $('#mother_notes').attr('orignal_sibling_v');
            /*----------------------------------------------------------------------------------------------*/

            /*--------------------------  set orginal mother values ---------------------------------------------------------------------*/
            $('#mother_first_name').val(orignal_mother_fname);
            $('#mother_second_name').val(orignal_mother_secname);
            $('#mother_third_name').val(orignal_mother_thirdname);
            $('#mother_family_name').val(orignal_mother_familyname);
            $('#mother_email').val(orignal_mother_email);
            $('#mother_phone').val(orignal_mother_phone);
            $('#mother_id_number').val(orignal_mother_id_number);
            $('#mother_passport_id').val(orignal_mother_passport_number);
            $('#mother_address').val(orignal_mother_address);

            $('#mother_country_id').val(orignal_mother_country_id);




            if(orignal_mother_finance == 1)
            {
                $('#mother_finance').prop('checked',true);
                $('#mother_finance').attr('finance_v',1);

            }
            else{
                $('#mother_finance').prop('checked',false);
                $('#mother_finance').attr('finance_v',0);
            }

            $('#mother_notes').val(orignal_mother_notes);

            $('#step2_validation-form').find('.dis_able_father_edit').removeAttr('disabled');
            $('#enable_father_edit').css('display','none');

            /*------------------------------------------------------------------------------------*/


            /*--------------------------  set orginal father values ---------------------------------------------------------------------*/
            $('#parent_first_name').val(orignal_father_fname);
            $('#parent_second_name').val(orignal_father_secname);
            $('#parent_third_name').val(orignal_father_thirdname);
            $('#parent_family_name').val(orignal_father_familyname);
            $('#parent_email').val(orignal_father_email);
            $('#form_field_phone').val(orignal_father_phone);
            $('#parent_id_number').val(orignal_father_id_number);
            $('#parent_passport_id').val(orignal_father_passport_number);
            $('#parent_address').val(orignal_father_address);

            $('#country_id').val(orignal_father_country_id);




            if(orignal_father_finance == 1)
            {
                $('#finance').prop('checked',true);
                $('#finance').attr('finance_v',1);

            }
            else{
                $('#finance').prop('checked',false);
                $('#finance').attr('finance_v',0);
            }

            $('#parent_notes').val(orignal_father_notes);

            $('#step2_validation-form').find('.dis_able_father_edit').removeAttr('disabled');
            $('#step3_validation-form').find('.dis_able_mother_edit').removeAttr('disabled');


            $('#enable_father_edit').css('display','none');
            $('#enable_mother_edit').css('display','none');

            /*------------------------------------------------------------------------------------*/


        }
    </script>


{{-------------------------------------------------------------------------------------------}}
    <script>
        $(document).ready(first_fill_siblings_table());
        $(document).ready(update_nationality());
        $(document).ready(acadimic_year());
        $(document).ready(rigistration_class());
        $(document).ready(countries_get());
        $(document).ready(m_countries_get());
        /*$(document).ready(get_all_student_data());*/



    </script>

    <script>
        jQuery(function($) {

            $( "#birth_date" ).datepicker({
                dateFormat: 'yy-mm-dd',
                showOtherMonths: true,
                selectOtherMonths: false,
                //isRTL:true,


                /*
                changeMonth: true,
                changeYear: true,

                showButtonPanel: true,
                beforeShow: function() {
                    //change button colors
                    var datepicker = $(this).datepicker( "widget" );
                    setTimeout(function(){
                        var buttons = datepicker.find('.ui-datepicker-buttonpane')
                        .find('button');
                        buttons.eq(0).addClass('btn btn-xs');
                        buttons.eq(1).addClass('btn btn-xs btn-success');
                        buttons.wrapInner('<span class="bigger-110" />');
                    }, 0);
                }
        */
            });});
    </script>
    <script>
        jQuery(function($) {

            $( "#registration_date" ).datepicker({
                dateFormat: 'yy-mm-dd',
                showOtherMonths: true,
                selectOtherMonths: false,
                //isRTL:true,


                /*
                changeMonth: true,
                changeYear: true,

                showButtonPanel: true,
                beforeShow: function() {
                    //change button colors
                    var datepicker = $(this).datepicker( "widget" );
                    setTimeout(function(){
                        var buttons = datepicker.find('.ui-datepicker-buttonpane')
                        .find('button');
                        buttons.eq(0).addClass('btn btn-xs');
                        buttons.eq(1).addClass('btn btn-xs btn-success');
                        buttons.wrapInner('<span class="bigger-110" />');
                    }, 0);
                }
        */
            });});
    </script>

    <script>
        jQuery(function($) {

            $( "#leave_date" ).datepicker({
                dateFormat: 'yy-mm-dd',
                showOtherMonths: true,
                selectOtherMonths: false,
                //isRTL:true,


                /*
                changeMonth: true,
                changeYear: true,

                showButtonPanel: true,
                beforeShow: function() {
                    //change button colors
                    var datepicker = $(this).datepicker( "widget" );
                    setTimeout(function(){
                        var buttons = datepicker.find('.ui-datepicker-buttonpane')
                        .find('button');
                        buttons.eq(0).addClass('btn btn-xs');
                        buttons.eq(1).addClass('btn btn-xs btn-success');
                        buttons.wrapInner('<span class="bigger-110" />');
                    }, 0);
                }
        */
            });});
    </script>


    <script>

        //pre-show a file name, for example a previously selected file
        //$('#id-input-file-1').ace_file_input('show_file_list', ['myfile.txt'])


        $('#id-input-file-3').ace_file_input({
            style: 'well',
            btn_choose: 'Drop file here or click to choose',
            btn_change: null,
            no_icon: 'ace-icon fa fa-picture-o',
            droppable: true,
            thumbnail: 'small',


        }).on('change', function(){
            //console.log($(this).data('ace_input_files'));
            //console.log($(this).data('ace_input_method'));
        });

        $(document).ready(function() {
            var whitelist_ext, whitelist_mime;
            var btn_choose
            var no_icon

            btn_choose = "Drop images here or click to choose";
            no_icon = "ace-icon fa fa-picture-o";

            whitelist_ext = ["jpeg", "jpg", "png", "gif" , "bmp"];
            whitelist_mime = ["image/jpg", "image/jpeg", "image/png", "image/gif", "image/bmp"];

            var file_input = $('#id-input-file-3');
            file_input
                .ace_file_input('update_settings',
                    {
                        'btn_choose': btn_choose,
                        'no_icon': no_icon,
                        'allowExt': whitelist_ext,
                        'allowMime': whitelist_mime
                    })
            file_input.ace_file_input('reset_input');

            file_input
                .off('file.error.ace')
                .on('file.error.ace', function(e, info) {

                });



        });
    </script>




@endsection