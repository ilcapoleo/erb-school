@extends('layouts.main_app')

@section('main_content')
    <!-- ace CSS -->
    <link rel="stylesheet" href="{{asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/select2.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/css/fonts.googleapis.com.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/css/ace.min.css')}}" class="ace-main-stylesheet" id="main-ace-style"/>
    <link rel="stylesheet" href="{{asset('assets/css/ace-skins.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/css/ace-rtl.min.css')}}"/>
    <link href="{{asset('css/fastselect.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/select2.min.css')}}" rel="stylesheet"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        body {

            font-family: Gill Sans, Gill Sans MT, Calibri, sans-serif !important;
            font-size: 15px !important;
            color: #676a6d !important;
        }

        .navbar .navbar-nav > li > a {
            font-size: 15px !important;
        }

        .control-label {
            font-size: 15px !important;
        }

        .navbar-default .navbar-nav > .active > a, .navbar-default .navbar-nav > .active > a:focus, .navbar-default .navbar-nav > .active > a:hover {
            background-color: transparent;
            COLOR: #00AAFF !important;
            FONT-SIZE: 20PX !important;
        }

        .side_sheets {
            padding: 0;
            margin: 0 1px 10px 0px;
        }

        .navbar-default .navbar-nav > li > a:focus, .navbar-default .navbar-nav > li > a:hover {
            color: #bbb !important;
        }

        #student_side_bar {
            padding-right: 123px;
        }

        nav.navbar.navbar-default.navbar-fixed-top {
            background-color: #2B333E;
        }

        .navbar .navbar-nav > li {
            border: 0px !important;
        }

        .navbar2 {

            background-color: #f8f8f8 !important;
            -webkit-box-shadow: 1px 2px 3px rgba(0, 0, 0, 0.1) !important;
        }

        .navbar {
            min-height: 50px !important;

        }

        .main-container:before {
            position: relative !important;

        }

        .breadcrumb > li > a {
            font-size: 15px !important;
        }

        .btn-danger {
            color: #fff;
            background-color: #d9534f;
            border-color: #d43f3a;
        }

        .btn {
            display: inline-block;
            padding: 6px 12px;
            margin-bottom: 0;
            font-size: 14px;
            font-weight: 400;
            line-height: 1.42857143;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            touch-action: manipulation;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-image: none;
            border: 1px solid transparent;
            border-radius: 4px;
            text-shadow: 0 0px 0 rgba(0, 0, 0, .25);
        }

        .navbar-default {
            -webkit-box-shadow: 1px 2px 3px rgba(0, 0, 0, 0.1) !important;
        }

        .btn-primary {
            background-color: #00AAFF !important;
            border-color: #00a0f0 !important;
        }

        #creat {
            box-shadow: 0px 1px 2px 0 rgba(0, 0, 0, 0.2);

            color: #676a6d !important;
            background-color: #dddddd !important;
        }

        .main-content {
            padding: 60px 10px;
        }

        /*upload photo */

        .avatar-upload {
            position: relative;
            max-width: 205px;

        }

        .avatar-upload .avatar-edit {
            position: absolute;
            right: 12px;
            z-index: 1;
            top: 10px;
        }

        .avatar-upload .avatar-edit input {
            display: none;
        }

        .avatar-upload .avatar-edit input + label {
            display: inline-block;
            width: 34px;
            height: 34px;
            margin-bottom: 0;
            border-radius: 100%;
            background: #ffffff;
            border: 1px solid transparent;
            box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.12);
            cursor: pointer;
            font-weight: normal;
            transition: all 0.2s ease-in-out;
        }

        .avatar-upload .avatar-edit input + label:hover {
            background: #f1f1f1;
            border-color: #d6d6d6;
        }

        .avatar-upload .avatar-edit input + label:after {
            content: "\f040";
            font-family: "FontAwesome";
            color: #757575;
            position: absolute;
            top: 10px;
            left: 0;
            right: 0;
            text-align: center;
            margin: auto;
        }

        .avatar-upload .avatar-preview {
            width: 192px;
            height: 192px;
            position: relative;
            border-radius: 100%;
            border: 6px solid #f8f8f8;
            box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
        }

        .avatar-upload .avatar-preview > div {
            width: 100%;
            height: 100%;
            border-radius: 100%;
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
        }

        span.select2.select2-container.select2-container--default {
            width: 100% !important;
        }

        .breadcrumb > li + li:before {
            font-family: FontAwesome;
            font-size: 20px;
            content: "/" !important;
            color: #B2B6BF;
            padding: 0;
            margin: 0 8px 0 0;
            position: relative;
            top: 1px;
        }

        .collapse > .row > .col-md-4 > .btn, .collapse > .row > .col-md-4 > .dropdown > .btn {
            margin-top: 0px;
        }

        .dropdown-menu > li.active:hover > a, .dropdown-menu > li.active > a, .dropdown-menu > li:hover > a, .dropdown-menu > li > a:active, .dropdown-menu > li > a:focus {
            color: #262626;
            text-decoration: none;
            background-color: #f5f5f5;
        }

        .breadcrumb {
            display: inline-block;
            float: left;
            position: relative;
            margin-top: 0;
            margin-right: 0.2em;
            margin-bottom: 0;
            padding: 0.5em;
            color: gray;
            white-space: nowrap;
            margin-left: 0px;
        }

        .dropdown-menu > li > a {
            font-size: 14px;
            display: block;
            padding: 3px 20px;
            clear: both;
            font-weight: 400;
            line-height: 1.42857143;
            color: #333;
            white-space: nowrap;
        }

        .form-horizontal .form-group {
            margin-right: 0px;
            margin-left: 0px;
        }



        #employee_name-error{
            color: #D16E6C;
            margin-left: 254px;
        }

        #academic_year2-error{
            color: #D16E6C;
        }

        .form-group.has-error .control-label{
            color: #676a6d;
        }

        .has_error{
            border: 1px solid #D16E6C!important;
        }



        .error{
            border: 1px solid red  !important;
            border-left: 1px double red !important;
            border-top: 2px double red !important;
        }


        .red-border{
            border: 1px solid red  !important;
        }


        .required, .required + .select2-container--default .select2-selection--single {
            background-color: lavender!important;
        }
        input#parent_email
        {
            width: 208px;
        }

        #middle_name-error,#last_name-error,#last_name-error,#family_name-error
        {
            margin-left:253px;
        }

        input[type=checkbox].ace.ace-switch.ace-switch-6:checked+.lbl::after{
            left: 32px;
            background-color: #337ab7;
            border: 4px solid #337ab7;

        }
        input[type=checkbox].ace.ace-switch.ace-switch-6+.lbl::after{
            background: rgb(255, 255, 255);
            border-radius: 16px;
            box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
            content: '';
            height: 24px;
            left: 0px;
            margin-top: -8px;
            position: absolute;
            top: 6px;
            transition: all 0.3s ease-in-out;
            width: 24px;
        }

        input[type=checkbox].ace.ace-switch.ace-switch-6:checked+.lbl::before{
            border-color: #B7D3E5;
            background-color: #1363a9;
        }

        input[type=checkbox].ace.ace-switch.ace-switch-6+.lbl::before{
            background: rgb(0, 0, 0);
            box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
            border-radius: 8px;
            content: '';
            height: 16px;
            margin-top: 3px;
            position: absolute;
            opacity: 0.3;
            transition: all 0.4s ease-in-out;
            width: 40px;
            left: 14px;
        }
        input[type=checkbox].ace.ace-switch.ace-switch-6:checked+.lbl::before{
            content:'';
        }
        input[type=checkbox].ace.ace-switch.ace-switch-6:checked+.lbl::after {
            left: 45px;
            background-color: #337ab7;
            border: 4px solid #337ab7;
        }
        input[type=checkbox].ace.ace-switch.ace-switch-6+.lbl::after{
            border: 4px solid #fff;
        }
        input[type=checkbox].ace.ace-switch {
            width: 71px;
        }

        .panel
        {
            margin-top: -47px;
        }

        #children_number-error
        {
            margin-left: 253px;
            color: #D16E6C;
        }
        .steps>li.active .step, .steps>li.active:before, .steps>li.complete .step, .steps>li.complete:before {
            border-color: #00AAFF;
        }
        #creat_dep{
            background-color: #dddddd!important;
            color:#000!important;
        }
        .blue {
            color: #00AAFF!important;
        }
        .search-bar {
            bottom: -204px!important;
        }
    </style>






    <div class="main">


        @if(isset($employee))
            <div id="set_po_id" current_po_id="{{$employee->employee_id}}" current_po_status=""
                 closed_or_not=""></div>
        @else
            <div id="set_po_id" current_po_id="" current_po_status="draft_po" closed_or_not="0"></div>
    @endif




    <!-- Navbar-content -->
        <nav class="navbar navbar-default navbar2">

            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                    <!--breadcrumbs-->
                    <div class="row">

                        <div class="col-md-10 col-lg-10">

                            <ol class="breadcrumb breadcrumb_nav">
                                <li><a href="/employees">Employee</a></li>
                                <li class="active">
                                    @if(isset($employee))
                                        @if($employee->employee_id != 0)
                                            {{$employee->employee_id}}
                                        @else
                                            Draft
                                        @endif
                                    @endif
                                </li>
                            </ol>
                        </div>
                        @if(isset($employee))
                            <div class="col-md-2 col-lg-2 col-sm-4 col-xs-4">
                                @if(@\App\Employee::where('employee_id','<',$employee->employee_id)->orderBy('employee_id','desc')->first()->employee_id)
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <a href="/employees/{{@\App\Employee::where('employee_id','<',$employee->employee_id)->orderBy('employee_id','desc')->first()->employee_id}}"><span
                                                    class="glyphicon glyphicon-arrow-left"></span>
                                        </a>
                                    </div>
                                @endif
                                @if(@\App\Employee::where('employee_id','>',$employee->employee_id)->orderBy('employee_id','asc')->first()->employee_id)
                                    <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                                        <a href="/employees/{{@\App\Employee::where('employee_id','>',$employee->employee_id)->orderBy('employee_id','asc')->first()->employee_id}}">
                                    <span class="glyphicon glyphicon-arrow-right">
                                </span>
                                        </a>
                                    </div>
                                @endif
                            </div>
                        @endif


                    </div>


                    <!--End breadcrumbs-->
                    <div class="row">
                        <div class="col-md-4 col-lg-4 buttons">
                            @if(isset($employee))
                                <button id="edit_po" type="button" class="btn btn-danger ">Edit</button>
                                <button type="button" class="btn" id="creat_dep">Create</button>
                            @else
                                <button id="save_po" type="button" class="btn btn-danger ">Save</button>
                                {{--<button id="edit_user" type="button" class="btn btn-danger " >Edit</button>--}}
                                <button type="button" class="hide btn" id="creat_dep">Create</button>
                            @endif
                        </div>

                        <div class="col-md-4 col-lg-4">
                            @if(isset($employee))
                                <div class="dropdown">
                                    <button class=" btn btn-primary  dropdown-toggle" type="button"
                                            data-toggle="dropdown" id="more" hidden>More
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">

                                        <li><a href="#">Export</a></li>
                                        <li><a href="#" id="dublicate_dep">Duplicate</a></li>
                                        <li><a href="#" id="print">Print</a></li>

                                    </ul>
                                </div>

                            @else
                                <div class="dropdown">
                                    <button class="hide btn btn-primary  dropdown-toggle" type="button"
                                            data-toggle="dropdown" id="more" hidden>More
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">

                                        <li><a href="#">Export</a></li>
                                        <li><a href="#" id="dublicate_dep">Duplicate</a></li>
                                        <li><a href="#" id="print">Print</a></li>

                                    </ul>
                                </div>
                            @endif

                        </div>


                    </div> <!--collapse -->

                </div><!--collapse -->

            </div>
        </nav>
        <!-- End Navbar-content -->



        <!-- show and hide fronted Error -->

        <div class="regionOfError">
            <!-- Success messages -->
            <div class="alert alert-success alert-autocloseable-success" style="display:none;position:absolute;">
                You are finished!

            </div>
            <!--end Success messages -->
            <!-- failuer messages 1 -->
            <div class="alert alert-danger errorController" style="display:none;" >

            </div>


            <div class="alert alert-danger errorMessage0" style="display:none;position:absolute;" >
                Please Fill Required Fields!
            </div>

            <!-- failuer messages 1 -->
            <div class="alert alert-danger errorMessage1" style="display:none;position:absolute;" >
                Invalid Start Date!
            </div>
            <!--end failuer messages 1-->


            <!-- failuer messages 2 -->
            <div class="alert alert-danger errorMessage2" style="display:none;position:absolute;" >
                Invalid interval Date!
            </div>

            <div class="alert alert-danger errorMessage4" style="display:none;position:absolute;" >
                Please Fill Sallary Scale Table!
            </div>
            <!--end failuer messages 2  -->


            <!-- failuer messages 1 -->
            <div class="alert alert-danger errorMessage3" style="display:none;position:absolute;" >
                Invalid End Date!
            </div>
            <!--end failuer messages 1-->
            <!-- failuer messages 1 -->
            <div class="alert alert-danger errorTableMessage" style="display:none;position:absolute;" >
                Please Finish This Table !
            </div>
            <!--end failuer messages 1-->
        </div>


        <!-- MAIN CONTENT -->
        <div class="main-content">

            <div class="container-fluid">

                <!-- INPUTS -->
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Employee</h3>
                    </div>
                    <div class="panel-body">


                        <div class="page-content">

                            <div class="row">

                                <form id="image_form" enctype="multipart/form-data">
                                    <div class="avatar-upload">
                                        <div class="avatar-edit">
                                            <input class="set_all_disabled" type='file' id="imageUpload" name="profile_image"
                                                   accept=".png, .jpg, .jpeg" @if(isset($employee))disabled @endif />
                                            <label for="imageUpload"></label>
                                        </div>
                                        <div class="avatar-preview">
                                            <div id="imagePreview"
                                                 style="background-image:
                                                 @if(isset($employee))

                                                 @if(file_exists('uploads/employees/'.$employee->employee_id.'_.png'))

                                                         url({{asset('uploads/employees/'.$employee->employee_id.'_.png')}});
                                                 @else
                                                         url({{asset('uploads/employees/default2.png')}});
                                                 @endif
                                                 @else
                                                         url({{asset('uploads/employees/default2.png')}});
                                                 @endif
                                                         ">
                                            </div>
                                        </div>
                                    </div>
                                </form>

                            </div>

                            <br>
                            <div class="row">

                                <div class="col-md-6 col-lg-6">
                                    <label class="col-md-6 col-lg-6  control-label">Related User:</label>

                                    <div class="col-md-6 col-lg-6">
                                        @if(isset($employee))
                                            <input class="set_all_disabled" id="user_name" value="{{$user}}" type="text"
                                                   style="border:0;background-color: #fff;"
                                                   name="user_name" required>
                                        @else
                                            <input class="set_all_disabled" id="user_name" type="text"
                                                   style="border:0;background-color: #fff;"
                                                   name="user_name" required>
                                        @endif
                                    </div>


                                </div>


                            </div>

                            <div class="row">
                                <div class="col-xs-12">
                                    <!-- PAGE CONTENT BEGINS -->


                                    <div class="widget-body">
                                        <div class="widget-main">
                                            <div id="fuelux-wizard-container">
                                                <div>
                                                    <ul class="steps">
                                                        <li data-step="1" class="active">
                                                            <span class="step">1</span>
                                                            <span class="title">Personal Information</span>
                                                        </li>

                                                        <li data-step="2">
                                                            <span class="step">2</span>
                                                            <span class="title">HR Settings</span>
                                                        </li>


                                                        <li data-step="3">
                                                            <span class="step">3</span>
                                                            <span class="title">Sallary Scale</span>
                                                        </li>



                                                        {{--<li data-step="4">--}}
                                                        {{--<span class="step">5</span>--}}
                                                        {{--<span class="title">Finish</span>--}}
                                                        {{--</li>--}}


                                                    </ul>
                                                </div>


                                                <div class="step-content pos-rel">

                                                    <div class="step-pane active " id="" data-step="1">
                                                        <form class="form-horizontal" id="step1_validation-form"
                                                              novalidate="novalidate">

                                                            <div class="center">
                                                                <h3 class="blue lighter">Personal Information</h3>
                                                            </div>
                                                            <br>
                                                            <div class="form_group">
                                                                <div class="row">

                                                                    <div class="col-md-6 col-lg-6">

                                                                        <div class="form-group">
                                                                            <label class="col-md-6 col-lg-6 control-label">First Name:</label>
                                                                            <div class="col-md-6 col-lg-6">
                                                                                @if(isset($employee))
                                                                                    <input old_img_value="{{$employee->employee_name}}"
                                                                                           edit_student_id="{{$employee->employee_name}}"
                                                                                           class="set_all_disabled form-control validate_tab required get_all_selectors   department_error err_input required"
                                                                                           value="{{$employee->employee_name}}"
                                                                                           type="text" name="employee_name"
                                                                                           id="employee_name"
                                                                                           placeholder="employee_name"
                                                                                           @if(isset($employee)) disabled @endif>
                                                                                @else
                                                                                    <input class="form-control set_all_disabled validate_tab required get_all_selectors   department_error err_input"
                                                                                           id="employee_name" type="text"
                                                                                           name="employee_name" required>

                                                                                @endif

                                                                                <br>
                                                                            </div>
                                                                            <br>
                                                                            <label class="col-md-6 col-lg-6 control-label">Middle Name:</label>
                                                                            <div class="col-md-6 col-lg-6">
                                                                                @if(isset($employee))
                                                                                    <input old_img_value="{{$employee->middle_name}}"
                                                                                           edit_student_id="{{$employee->middle_name}}"
                                                                                           class="set_all_disabled form-control validate_tab required get_all_selectors   department_error err_input required"
                                                                                           value="{{$employee->middle_name}}"
                                                                                           type="text" name="middle_name"
                                                                                           id="middle_name"
                                                                                           placeholder="middle_name"
                                                                                           @if(isset($employee)) disabled @endif>
                                                                                @else
                                                                                    <input class="form-control set_all_disabled validate_tab required get_all_selectors   department_error err_input"
                                                                                           id="middle_name" type="text"
                                                                                           name="middle_name" required>
                                                                                @endif
                                                                                <br>
                                                                            </div>
                                                                            <br>
                                                                            <label class="col-md-6 col-lg-6 control-label">Last Name:</label>
                                                                            <div class="col-md-6 col-lg-6">
                                                                                @if(isset($employee))
                                                                                    <input old_img_value="{{$employee->last_name}}"
                                                                                           edit_student_id="{{$employee->last_name}}"
                                                                                           class="set_all_disabled form-control validate_tab required get_all_selectors   department_error err_input required"
                                                                                           value="{{$employee->last_name}}"
                                                                                           type="text" name="last_name"
                                                                                           id="last_name"
                                                                                           placeholder="last_name"
                                                                                           @if(isset($employee)) disabled @endif>
                                                                                @else
                                                                                    <input class="form-control set_all_disabled validate_tab required get_all_selectors   department_error err_input"
                                                                                           id="last_name" type="text"
                                                                                           name="last_name" required>
                                                                                @endif
                                                                                <br>
                                                                            </div>



                                                                            <br>
                                                                            <label class="col-md-6 col-lg-6 control-label">Family Name:</label>
                                                                            <div class="col-md-6 col-lg-6">
                                                                                @if(isset($employee))
                                                                                    <input old_img_value="{{$employee->family_name}}"
                                                                                           edit_student_id="{{$employee->family_name}}"
                                                                                           class="set_all_disabled form-control validate_tab required get_all_selectors   department_error err_input required"
                                                                                           value="{{$employee->family_name}}"
                                                                                           type="text" name="family_name"
                                                                                           id="family_name"
                                                                                           placeholder="family_name"
                                                                                           @if(isset($employee)) disabled @endif>
                                                                                @else
                                                                                    <input class="form-control set_all_disabled validate_tab required get_all_selectors   department_error err_input"
                                                                                           id="family_name" type="text"
                                                                                           name="family_name" required>
                                                                                @endif
                                                                                <br>

                                                                                <br>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>


                                                            <br>


                                                            <div class="row">

                                                                <div class="col-md-6 col-lg-6">


                                                                    <div class="form-group">

                                                                        <label class="col-md-6 col-lg-6 control-label"
                                                                               for="Gender">Gender</label>

                                                                        <div class="col-md-6 col-lg-6">
                                                                            @if(isset($employee))
                                                                                <label class="radio-inline"
                                                                                       for="Gender-0">
                                                                                    <input @if ($employee->gender == "male")checked
                                                                                           @endif class="set_all_disabled"
                                                                                           name="gender" id="gender"
                                                                                           value="male" type="radio"
                                                                                           @if(isset($employee)) disabled @endif>
                                                                                    Male
                                                                                </label>

                                                                                <label class="radio-inline"
                                                                                       for="Gender-1">
                                                                                    <input @if ($employee->gender == "female")checked
                                                                                           @endif class="set_all_disabled"
                                                                                           name="gender" id="gender"
                                                                                           value="female" type="radio"
                                                                                           @if(isset($employee)) disabled @endif>
                                                                                    Female
                                                                                </label>
                                                                            @else
                                                                                <label class="radio-inline"
                                                                                       for="Gender-0">
                                                                                    <input class="set_all_disabled"
                                                                                           name="gender" id="gender"
                                                                                           value="male" type="radio" checked
                                                                                           @if(isset($employee)) disabled @endif>
                                                                                    Male
                                                                                </label>

                                                                                <label class="radio-inline"
                                                                                       for="Gender-1">
                                                                                    <input class="set_all_disabled"
                                                                                           name="gender" id="gender"
                                                                                           value="female" type="radio"
                                                                                           @if(isset($employee)) disabled @endif>
                                                                                    Female
                                                                                </label>
                                                                            @endif
                                                                        </div>

                                                                    </div>

                                                                </div>

                                                            </div>
                                                            <br>

                                                            <div class="row">

                                                                <div class="col-md-6 col-lg-6">

                                                                    <div class="form-group">
                                                                        <label class="col-md-6 col-lg-6 control-label ">Nationality</label>
                                                                        <div class="col-md-6 col-lg-6">

                                                                            <select class="form-control select2 set_all_disabled required"
                                                                                    id="nation" name="nation"
                                                                                    class="validate_tab required get_all_selectors set_all_disabled select_2_enable form-control department_error err_input"
                                                                                    @if(isset($employee)) disabled @endif >
                                                                                <option value=""></option>
                                                                                @foreach($nation as $id=>$role)
                                                                                    @if(isset($employee))
                                                                                        @if($employee->nationality_id == $id)
                                                                                            <option selected
                                                                                                    value="{{$id}}">{{$role}}</option>
                                                                                        @else
                                                                                            <option value="{{$id}}">{{$role}}</option>
                                                                                        @endif
                                                                                    @else
                                                                                        <option value="{{$id}}">{{$role}}</option>
                                                                                    @endif
                                                                                @endforeach
                                                                            </select>
                                                                        </div>


                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <br>
                                                            <div class="row">

                                                                <div class="col-md-6 col-lg-6">


                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-6 col-lg-6"
                                                                               for="date1">Date of Birth</label>

                                                                        <div class=" col-md-6 col-lg-6">
                                                                            <div class="form-group">


                                                                                <div class="input-group ">
                                                                                    @if(isset($employee))
                                                                                        <input value="{{$employee->date_of_birth}}"
                                                                                               type="text" id="date_birth"
                                                                                               class="form-control set_all_disabled datae_pic required "
                                                                                               name="dateofbirth"
                                                                                               @if(isset($employee)) disabled @endif>
                                                                                    @else
                                                                                        <input type="text" id="date_birth"
                                                                                               class="form-control set_all_disabled required "
                                                                                               name="dateofbirth"
                                                                                               @if(isset($employee)) disabled @endif>
                                                                                    @endif
                                                                                    <div class="input-group-addon">
                                                                                        <i class="fa fa-calendar">
                                                                                        </i>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                        </div>

                                                                    </div>


                                                                </div>

                                                            </div>
                                                            <br>
                                                            <div class="row">

                                                                <div class="col-md-6 col-lg-6">
                                                                    <div class="form-group">

                                                                        <label class="col-md-6 col-lg-6  control-label">Work
                                                                            Mobile</label>
                                                                        <div class="col-md-6 col-lg-6">
                                                                            @if(isset($employee))
                                                                                <input value="{{$employee->mobile_work}}" id="phone"
                                                                                       class="form-control set_all_disabled "
                                                                                       type="tel"
                                                                                       class="col-md-4 "
                                                                                       name="phone"

                                                                                       @if(isset($employee)) disabled @endif>
                                                                            @else
                                                                                <input id="phone"
                                                                                       class="form-control set_all_disabled"
                                                                                       type="tel"
                                                                                       type="text" class="col-md-4"
                                                                                       name="phone"

                                                                                       @if(isset($employee)) disabled @endif>
                                                                            @endif

                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <br>
                                                            <div class="row">
                                                                <div class="col-md-6 col-lg-6">
                                                                    <div class="form-group">
                                                                        <label class="col-md-6 col-lg-6 control-label no-padding-right" for="form-field-email">Email</label>

                                                                        <div class="col-md-6 col-lg-6">
                                                                            	<span class="input-icon input-icon-right">
                                                                            @if(isset($employee))
                                                                                        <input value="{{$employee->email}}" id="parent_email"
                                                                                               class="form-control set_all_disabled required"
                                                                                               type="email"
                                                                                               class="col-md-4 "
                                                                                               name="parent_email"

                                                                                               @if(isset($employee)) disabled @endif>
                                                                                    @else
                                                                                        <input id="parent_email"
                                                                                               class="form-control set_all_disabled required"
                                                                                               type="email"
                                                                                               class="col-md-4"
                                                                                               name="parent_email"

                                                                                               @if(isset($employee)) disabled @endif>
                                                                                    @endif
                                                                                    <i class="ace-icon fa fa-envelope"></i>
</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <br>
                                                            <div class="row">

                                                                <div class="col-md-6 col-lg-6">
                                                                    <div class="form-group">

                                                                        <label class="col-md-6 col-lg-6 control-label ">Office
                                                                            Location</label>
                                                                        <div class="col-md-6 col-lg-6">

                                                                            <select class="form-control select2 set_all_disabled "
                                                                                    name="office_loc" id="office_loc"
                                                                                    @if(isset($employee)) disabled @endif>
                                                                                @if(isset($employee))
                                                                                    <option selected
                                                                                            value="{{$employee->office_location}}">{{$employee->office_location}}</option>
                                                                                    <option value="floor1">Floor1
                                                                                    </option>
                                                                                    <option value="floor2"> Floor2
                                                                                    </option>
                                                                                    <option value="floor3"> Floor3
                                                                                    </option>
                                                                                @else
                                                                                    <option value=""></option>
                                                                                    <option value="floor1">Floor1
                                                                                    </option>
                                                                                    <option value="floor2"> Floor2
                                                                                    </option>
                                                                                    <option value="floor3"> Floor3
                                                                                    </option>
                                                                                @endif
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <br>

                                                            <div class="row">

                                                                <div class="col-md-6 col-lg-6">
                                                                    <div class="form-group">

                                                                        <label class="col-md-6 col-lg-6 control-label ">Country</label>
                                                                        <div class="col-md-6 col-lg-6">

                                                                            <select class="form-control select2 set_all_disabled required"
                                                                                    id="country" name="country"
                                                                                    class="validate_tab required get_all_selectors set_all_disabled select_2_enable form-control department_error err_input"
                                                                                    @if(isset($employee)) disabled @endif >
                                                                                <option value=""></option>
                                                                                @foreach($country as $id=>$role)
                                                                                    @if(isset($employee))
                                                                                        @if($employee->country_id == $id)
                                                                                            <option selected value="{{$id}}">{{$role}}</option>
                                                                                        @else
                                                                                            <option value="{{$id}}">{{$role}}</option>
                                                                                        @endif
                                                                                    @else
                                                                                        <option value="{{$id}}">{{$role}}</option>
                                                                                    @endif
                                                                                @endforeach
                                                                            </select>





                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="row">

                                                                <div class="col-md-6 col-lg-6">
                                                                    <div class="form-group ">
                                                                        <label class="col-md-6 col-lg-6 control-label col-xs-12"
                                                                               for="Permanent Address">Home
                                                                            Address</label>
                                                                        <div class="col-md-6 col-lg-6">
                                                                            @if(isset($employee))
                                                                                <input id="address" value="{{$employee->home_address}}"
                                                                                       class="form-control set_all_disabled "
                                                                                       type="text" class="col-md-4 "
                                                                                       name="address"

                                                                                       @if(isset($employee)) disabled @endif>
                                                                            @else
                                                                                <input id="address"
                                                                                       class="form-control set_all_disabled "
                                                                                       type="text" class="col-md-4 "
                                                                                       name="address"

                                                                                       @if(isset($employee)) disabled @endif>
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <br>
                                                        </form>
                                                    </div>

                                                    <div class="step-pane" id="" data-step="2">
                                                        <form class="form-horizontal" id="step2_validation-form"
                                                              novalidate="novalidate">
                                                            <div class="center">
                                                                <h3 class="blue lighter">HR Settings</h3>
                                                            </div>
                                                            <br>
                                                            <div class="row">

                                                                <div class="col-md-6 col-lg-6">
                                                                    <div class=" form-group">
                                                                        <label class="col-md-6 col-lg-6 control-label"
                                                                               for="radios">Contract Type</label>
                                                                        <div class="col-md-6 col-lg-6">
                                                                            @if(isset($employee))
                                                                                <label class="radio-inline set_all_disabled"
                                                                                       for="Foreigner-0">
                                                                                    <input @if ($employee->user_type == "local")checked
                                                                                           @endif class="set_all_disabled"
                                                                                           type="radio" name="foreigner"
                                                                                           value="local"
                                                                                           id="Foreigner"
                                                                                           @if(isset($employee)) disabled @endif>
                                                                                    Local
                                                                                </label>
                                                                                <label class="radio-inline set_all_disabled"
                                                                                       for="Foreigner-1">
                                                                                    <input @if ($employee->user_type == "foreign")checked
                                                                                           @endif class="set_all_disabled"
                                                                                           type="radio" name="foreigner"
                                                                                           value="foreign"
                                                                                           id="Foreigner"
                                                                                           @if(isset($employee)) disabled @endif>
                                                                                    Foreign

                                                                                </label>
                                                                            @else
                                                                                <label class="radio-inline set_all_disabled"
                                                                                       for="Foreigner-0">
                                                                                    <input class="set_all_disabled"
                                                                                           type="radio" name="foreigner"
                                                                                           value="local"
                                                                                           id="Foreigner" checked
                                                                                           @if(isset($employee)) disabled @endif>
                                                                                    Local
                                                                                </label>
                                                                                <label class="radio-inline set_all_disabled"
                                                                                       for="Foreigner-1">
                                                                                    <input class="set_all_disabled"
                                                                                           type="radio" name="foreigner"
                                                                                           value="foreign"
                                                                                           id="Foreigner"
                                                                                           @if(isset($employee)) disabled @endif>
                                                                                    Foreign

                                                                                </label>
                                                                            @endif
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <br>

                                                            <div class="row">
                                                                <div class="col-md-6 col-lg-6 ">
                                                                    <div class="form-group">
                                                                        <label class="col-md-6 col-lg-6 control-label date"
                                                                               for="date2">Date of contract</label>
                                                                        <div class="col-md-6 col-lg-6">
                                                                            <div class="input-group">
                                                                                @if(isset($employee))
                                                                                    <input value="{{$employee->date_of_contract}}"
                                                                                           type="text"
                                                                                           class="form-control set_all_disabled required"
                                                                                           id="date2"
                                                                                           name="dateofcontract"
                                                                                           @if(isset($employee)) disabled @endif>
                                                                                @else
                                                                                    <input type="text"
                                                                                           class="form-control set_all_disabled required"
                                                                                           id="date2"
                                                                                           name="dateofcontract"
                                                                                           @if(isset($employee)) disabled @endif>
                                                                                @endif
                                                                                <div class="input-group-addon">
                                                                                    <i class="fa fa-calendar">
                                                                                    </i>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <br>

                                                            <div class="row">
                                                                <div class="col-md-6 col-lg-6 ">

                                                                    <label class="col-md-6 col-lg-6 control-label"
                                                                           for="radios">Contract Years Number</label>
                                                                    <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                                                                        @if(is                                                                                   onkeypress="return (event.charCode == 8 || event.charCode == 1) ? null : event.charCode >= 48 && event.charCode <= 57 ||  event.charCode == 46" data-role="input"
set($employee))
                                                                            <input value="{{$employee->contract_years}}"
                                                                                   id="contract_years"
                                                                                   class="row_data form-control set_all_disabled required"
                                                                                   type="number"
                                                                                   name="contract_years"
                                                                                   @if(isset($employee)) disabled @endif>
                                                                        @else
                                                                            <input id="contract_years"
                                                                                   class="row_data form-control set_all_disabled required"
                                                                                   type="number" value="1"
                                                                                   onkeypress="return (event.charCode == 8 || event.charCode == 1) ? null : event.charCode >= 48 && event.charCode <= 57 ||  event.charCode == 46" data-role="input"

                                                                                   name="contract_years"
                                                                                   @if(isset($employee)) disabled @endif>
                                                                        @endif
                                                                    </div>

                                                                </div>


                                                            </div>
                                                            <br>

                                                            <div class="row">

                                                                <div class="col-md-6 col-lg-6">

                                                                    <div class="form-group">
                                                                        <label class="col-md-6 col-lg-6 control-label set_all_disabled"
                                                                               for="radios">Marital Status</label>
                                                                        <div class="col-md-6 col-lg-6 ">
                                                                            @if(isset($employee))
                                                                                <label class="radio-inline set_all_disabled"
                                                                                       for="status-0">
                                                                                    <input @if ($employee->marital_status == "single")checked
                                                                                           @endif  class="set_all_disabled"
                                                                                           type="radio" name="status"
                                                                                           id="status-0" value="single"
                                                                                           @if(isset($employee)) disabled @endif>
                                                                                    single
                                                                                </label>
                                                                                <label class="radio-inline"
                                                                                       for="status-1">
                                                                                    <input @if ($employee->marital_status == "married")checked
                                                                                           @endif class="set_all_disabled"
                                                                                           type="radio" name="status"
                                                                                           id="status-1" value="married"
                                                                                           @if(isset($employee)) disabled @endif>
                                                                                    married
                                                                                </label>

                                                                            @else
                                                                                <label class="radio-inline set_all_disabled"
                                                                                       for="status-0">
                                                                                    <input class="set_all_disabled"
                                                                                           type="radio" name="status"
                                                                                           id="status-0" value="single" checked
                                                                                           @if(isset($employee)) disabled @endif>
                                                                                    single
                                                                                </label>
                                                                                <label class="radio-inline"
                                                                                       for="status-1">
                                                                                    <input class="set_all_disabled"
                                                                                           type="radio" name="status"
                                                                                           id="status-1" value="married"
                                                                                           @if(isset($employee)) disabled @endif>
                                                                                    married
                                                                                </label>

                                                                            @endif
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <br>


                                                            <div class="row  with_family ">
                                                                <div class="col-md-6 col-lg-6">
                                                                    <div class="partner form-group set_all_disabled">
                                                                        <label class="col-md-6 col-lg-6 control-label"
                                                                               for="radios">With Family</label>
                                                                        <div class="col-md-6 col-lg-6">

                                                                            <div class="material-switch set_all_disabled">
                                                                                @if(isset($employee))
                                                                                    <input id="family"  value="{{$employee->family}}" class="include_check set_all_disabled" name="someSwitchOption001" type="checkbox" disabled   @if( $employee->family == "yes") checked  @endif>
                                                                                @else
                                                                                    <input id="family"  value="no" class="include_check set_all_disabled" name="someSwitchOption001" type="checkbox">
                                                                                @endif

                                                                                <label for="family" class="label-primary"></label>
                                                                            </div>

                                                                        </div>


                                                                    </div>

                                                                </div>

                                                            </div>

                                                            <br>
                                                            <div class="row   partner_info">

                                                                <div class="col-md-6 col-lg-6">

                                                                    <div class="form-group">
                                                                        <label class="col-md-6 col-lg-6 control-label">Partner First Name:</label>
                                                                        <div class="col-md-6 col-lg-6">
                                                                            @if(isset($employee))
                                                                                <input
                                                                                        class="set_all_disabled form-control validate_tab required get_all_selectors   department_error err_input required"
                                                                                        value="{{$employee->partner_first_name}}"
                                                                                        type="text" name="partner_first_name"
                                                                                        id="partner_first_name"
                                                                                        placeholder="partner_first_name"
                                                                                        @if(isset($employee)) disabled @endif>
                                                                            @else
                                                                                <input class="form-control set_all_disabled validate_tab required get_all_selectors   department_error err_input"
                                                                                       id="partner_first_name" type="text"
                                                                                       name="partner_first_name" >
                                                                            @endif
                                                                            <br>
                                                                        </div>
                                                                        <br>
                                                                        <label class="col-md-6 col-lg-6 control-label">Partner Middle Name:</label>
                                                                        <div class="col-md-6 col-lg-6">
                                                                            @if(isset($employee))
                                                                                <input old_img_value="{{$employee->partner_middle_name}}"
                                                                                       edit_student_id="{{$employee->partner_middle_name}}"
                                                                                       class="set_all_disabled form-control validate_tab required get_all_selectors   department_error err_input required"
                                                                                       value="{{$employee->partner_middle_name}}"
                                                                                       type="text" name="partner_middle_name"
                                                                                       id="partner_middle_name"
                                                                                       placeholder="partner_middle_name"
                                                                                       @if(isset($employee)) disabled @endif>
                                                                            @else
                                                                                <input class="form-control set_all_disabled validate_tab required get_all_selectors   department_error err_input"
                                                                                       id="partner_middle_name" type="text"
                                                                                       name="partner_middle_name" >
                                                                            @endif
                                                                            <br>
                                                                        </div>
                                                                        <br>
                                                                        <label class="col-md-6 col-lg-6 control-label">Partner Last Name:</label>
                                                                        <div class="col-md-6 col-lg-6">
                                                                            @if(isset($employee))
                                                                                <input old_img_value="{{$employee->partner_last_name}}"
                                                                                       edit_student_id="{{$employee->partner_last_name}}"
                                                                                       class="set_all_disabled form-control validate_tab required get_all_selectors   department_error err_input required"
                                                                                       value="{{$employee->partner_last_name}}"
                                                                                       type="text" name="partner_last_name"
                                                                                       id="partner_last_name"
                                                                                       placeholder="partner_last_name"
                                                                                       @if(isset($employee)) disabled @endif>
                                                                            @else
                                                                                <input class="form-control set_all_disabled validate_tab required get_all_selectors   department_error err_input"
                                                                                       id="partner_last_name" type="text"
                                                                                       name="partner_last_name" >
                                                                            @endif
                                                                            <br>
                                                                        </div>

                                                                        <br>
                                                                        <label class="col-md-6 col-lg-6 control-label">Partner Family Name:</label>
                                                                        <div class="col-md-6 col-lg-6">
                                                                            @if(isset($employee))
                                                                                <input old_img_value="{{$employee->partner_family_name}}"
                                                                                       edit_student_id="{{$employee->partner_family_name}}"
                                                                                       class="set_all_disabled form-control validate_tab required get_all_selectors   department_error err_input required"
                                                                                       value="{{$employee->partner_family_name}}"
                                                                                       type="text" name="partner_family_name"
                                                                                       id="partner_family_name"
                                                                                       placeholder="partner_family_name"
                                                                                       @if(isset($employee)) disabled @endif>
                                                                            @else
                                                                                <input class="form-control set_all_disabled validate_tab required get_all_selectors   department_error err_input"
                                                                                       id="partner_family_name" type="text"
                                                                                       name="partner_family_name" >
                                                                            @endif
                                                                        </div>
                                                                        <br>

                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <br>
                                                            <div class="row children partner_info">
                                                                <div class="col-md-6 col-lg-6  ">
                                                                    <label class="col-md-6 col-lg-6 control-label"
                                                                           for="radios">Number of Children</label>
                                                                    <div class="col-md-6 col-lg-6">
                                                                        @if(isset($employee))
                                                                            <input value="{{$employee->children_number}}"
                                                                                   class="set_all_disabled form-control validate_tab required get_all_selectors   department_error err_input required"
                                                                                   onkeypress="return (event.charCode == 8 || event.charCode == 1) ? null : event.charCode >= 48 && event.charCode <= 57 ||  event.charCode == 46" data-role="input"

                                                                                   value="{{$employee->children_number}}"
                                                                                   type="number" name="children_number"
                                                                                   id="children_number"
                                                                                   placeholder="children_number"
                                                                                   @if(isset($employee)) disabled @endif>
                                                                        @else
                                                                            <input class="form-control set_all_disabled validate_tab required get_all_selectors   department_error err_input required"
                                                                                   id="children_number" type="number"
                                                                                   onkeypress="return (event.charCode == 8 || event.charCode == 1) ? null : event.charCode >= 48 && event.charCode <= 57 ||  event.charCode == 46" data-role="input"

                                                                                   name="children_number" >
                                                                        @endif
                                                                        <br>
                                                                    </div>

                                                                </div>
                                                            </div>


                                                            <br>
                                                            <div class="row">
                                                                <div class="col-md-6 col-lg-6 ">

                                                                    <label class="col-md-6 col-lg-6 control-label"
                                                                           for="radios">Percentage Salary</label>
                                                                    <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                                                                        @if(isset($employee))
                                                                            <input value="{{$employee->percentage_salary}}"
                                                                                   id="number"
                                                                                   class="row_data form-control set_all_disabled required"
                                                                                   type="number"
                                                                                   name="percentage_Salary"
                                                                                   @if(isset($employee)) disabled @endif>
                                                                        @else
                                                                            <input id="number"
                                                                                   class="row_data form-control set_all_disabled required"
                                                                                   type="number" value="100"
                                                                                   name="percentage_Salary"
                                                                                   @if(isset($employee)) disabled @endif>
                                                                        @endif
                                                                    </div>

                                                                </div>
                                                                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-2"
                                                                     style="line-height: 37px;"> %
                                                                </div>

                                                            </div>
                                                            <br>
                                                            <div class="row">

                                                                <div class="col-md-6 col-lg-6 ">

                                                                    <label class="col-md-6 col-lg-6 control-label"
                                                                           for="radios">Academic Year</label>
                                                                    <div class="col-md-6 col-lg-6">
                                                                        <select class="form-control required select2 set_all_disabled"
                                                                                id="academic_year2"
                                                                                name="academic_year2"
                                                                                class="form-control select2 set_all_disabled required"
                                                                                @if(isset($employee)) disabled @endif >
                                                                            <option value="" class="required"></option>
                                                                            @foreach($academic_year as $id=>$role)
                                                                                @if(isset($employee))
                                                                                    @if($employee->acadimic_year_id == $id)
                                                                                        <option class="required" selected
                                                                                                value="{{$id}}">{{$role}}</option>
                                                                                    @else
                                                                                        <option class="required" value="{{$id}}">{{$role}}</option>
                                                                                    @endif
                                                                                @else
                                                                                    <option class="required" value="{{$id}}">{{$role}}</option>
                                                                                @endif
                                                                            @endforeach
                                                                        </select>

                                                                    </div>
                                                                </div>


                                                            </div>
                                                            <br>
                                                            <div class="row">
                                                                <div class="col-md-6 col-lg-6 ">
                                                                    <div class="form-group">
                                                                        <label class="col-md-6 col-lg-6 control-label">Department:</label>
                                                                        <div class="col-md-6 col-lg-6">
                                                                            <select class="form-control select2 required set_all_disabled"
                                                                                    id="depart" name="depart"
                                                                                    class="form-control select2 set_all_disabled required" @if(isset($employee)) disabled @endif>
                                                                                <option value=""></option>
                                                                                @foreach($depart as $id=>$role)
                                                                                    @if(isset($employee))
                                                                                        @if($employee->depart_id == $id)
                                                                                            <option selected
                                                                                                    value="{{$id}}">{{$role}}</option>
                                                                                        @else
                                                                                            <option value="{{$id}}">{{$role}}</option>
                                                                                        @endif
                                                                                    @else
                                                                                        <option value="{{$id}}">{{$role}}</option>
                                                                                    @endif
                                                                                @endforeach
                                                                            </select>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <br>
                                                            <div class="row">
                                                                <div class="col-md-6 col-lg-6 ">
                                                                    <div class="form-group">
                                                                        <label class="col-md-6 col-lg-6 control-label">Jop
                                                                            Title</label>
                                                                        <div class="col-md-6 col-lg-6">
                                                                            <select class="form-control required select2 set_all_disabled"
                                                                                    id="job" name="job"
                                                                                    class="form-control select2 set_all_disabled required"
                                                                                    @if(isset($employee)) disabled @endif >
                                                                                @if(isset($employee))
                                                                                    @if(isset($employee))
                                                                                        <option selected
                                                                                                value="{{$employee->job_id}}">{{@\App\Job::find($employee->job_id)->job_name}}</option>
                                                                                    @else
                                                                                        <option></option>
                                                                                    @endif
                                                                                @else
                                                                                    <option></option>
                                                                                @endif

                                                                            </select>


                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <br>
                                                            <div class="row">
                                                                <div class="col-md-6 col-lg-6 ">
                                                                    <div class="form-group">
                                                                        <label class="col-md-6 col-lg-6 control-label">Manager</label>
                                                                        <div class="col-md-6 col-lg-6">
                                                                            <select class="form-control select2 set_all_disabled required"
                                                                                    name="manager" id="manager"
                                                                                    @if(isset($employee)) disabled @endif>
                                                                                @if(isset($employee))
                                                                                    @if(isset($employee))
                                                                                        <option selected
                                                                                                value="{{$employee->manager_id}}">{{@\App\Employee::find($employee->manager_id)->employee_name}}</option>
                                                                                    @else
                                                                                        <option></option>
                                                                                    @endif
                                                                                @else
                                                                                    <option></option>
                                                                                @endif
                                                                            </select>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <br>

                                                            <div class="row" @if(!isset($employee)) style="visibility: hidden" @endif>
                                                                <div class="col-md-6 col-lg-6 ">
                                                                    <div class="form-group">

                                                                        <label class="col-md-6 col-lg-6 control-label"
                                                                               for="leave">leave</label>
                                                                        @if(isset($employee))
                                                                            <div class="col-md-2 col-lg-2">

                                                                                <label class="radio-inline" for="yes">
                                                                                    <input @if ($employee->leave == "yes")checked
                                                                                           @endif class="set_all_disabled"
                                                                                           name="leave" id="yes7"
                                                                                           value="yes"
                                                                                           type="radio"
                                                                                           @if(isset($employee)) disabled @endif>
                                                                                    Yes
                                                                                </label>

                                                                            </div>

                                                                            <div class="col-md-2 col-lg-2">

                                                                                <label class="radio-inline" for="no">
                                                                                    <input @if ($employee->leave == "no")checked
                                                                                           @endif class="set_all_disabled"
                                                                                           name="leave" id="no7"
                                                                                           value="no"
                                                                                           type="radio"
                                                                                           @if(isset($employee)) disabled @endif>
                                                                                    No
                                                                                </label>


                                                                            </div>
                                                                        @else
                                                                            <div class="col-md-2 col-lg-2">

                                                                                <label class="radio-inline" for="yes">
                                                                                    <input class="set_all_disabled"
                                                                                           name="leave" id="yes7"
                                                                                           value="yes"
                                                                                           type="radio"
                                                                                           @if(isset($employee)) disabled @endif>
                                                                                    Yes
                                                                                </label>

                                                                            </div>

                                                                            <div class="col-md-2 col-lg-2">

                                                                                <label class="radio-inline" for="no">
                                                                                    <input class="set_all_disabled"
                                                                                           name="leave" id="no7"
                                                                                           value="no"
                                                                                           type="radio" checked
                                                                                           @if(isset($employee)) disabled @endif>
                                                                                    No
                                                                                </label>

                                                                            </div>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <br>

                                                            <div class="row">
                                                                <div class="set_all_disabled col-md-6 col-lg-6 @if(isset($employee))  @if($employee->leave == 'no') hide @endif  @else hide @endif   leaving ">


                                                                    <div class="form-group">
                                                                        <label class="col-md-6 control-label date"
                                                                               for="date3">Date of Leaving:</label>


                                                                        <div class=" col-md-6 col-lg-6">
                                                                            <div class="form-group">
                                                                                <div class="input-group">
                                                                                    @if(isset($employee) && $employee->leave == "yes")
                                                                                        <input type="text" value="{{$employee->date_of_leave}}"
                                                                                               class="form-control set_all_disabled"
                                                                                               id="date3"
                                                                                               name="dateofleave" disabled>
                                                                                    @else
                                                                                        <input type="text"
                                                                                               class="form-control set_all_disabled"
                                                                                               id="date3"
                                                                                               name="dateofleave">
                                                                                    @endif
                                                                                    <div class="input-group-addon">
                                                                                        <i class="fa fa-calendar">
                                                                                        </i>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </form>
                                                    </div>



                                                    <div class="step-pane" id="" data-step="3">
                                                        <form class="form-horizontal" id="step4_validation-form"
                                                              novalidate="novalidate">

                                                            <div class="center">
                                                                <h3 class="blue lighter">Sallary Scale</h3>
                                                            </div>
                                                            <br>
                                                            <table class="table table-bordred table-striped table-bordered table-striped table-hover table-condensed table-responsive "
                                                                   id="salary_table">
                                                                <thead>
                                                                <th>Salary Scale</th>
                                                                <th>Academic Year</th>
                                                                <th>Start From</th>
                                                                <th>Edit</th>
                                                                <th>Delete</th>

                                                                </thead>
                                                                <tbody>

                                                                @if(isset($employee))
                                                                    @foreach($employee_salary as $salary)
                                                                        <tr class="">
                                                                            <td td_type="search" data-select2-id="26" class="sub_name_v" current_value="{{@\App\SubSalary::find($salary->sub_salary_id)->sub_id}}">{{@\App\SubSalary::find($salary->sub_salary_id)->sub_name}}</td>
                                                                            <td td_type="search" data-select2-id="47" class="academic_year2_v" current_value="{{@\App\AcademicYear::find($salary->academic_year_id)->acadimic_id}}">{{@\App\AcademicYear::find($salary->academic_year_id)->year_name}}</td>
                                                                            <td td_type="date" class="start_in_v" current_value="{{$salary->start_in}}">{{$salary->start_in}}</td>
                                                                            <td><button  type="button" class="set_all_disabled btn btn-primary btn-xs edit_row" disabled>Edit</button></td>
                                                                            <td><button  type="button" class="set_all_disabled btn btn-danger btn-xs delete_row" disabled><span class="lnr lnr-trash"></span></button></td>
                                                                        </tr>

                                                                    @endforeach
                                                                    {{--@else--}}
                                                                    {{--<tr class="added_now">--}}
                                                                    {{--<td td_type="search">--}}
                                                                    {{--<select class="sub_name form-control js-example-disabled-results myselect4 required" required>--}}
                                                                    {{--<option value=""></option>--}}
                                                                    {{--@foreach($sub_salary as $id=>$role)--}}
                                                                    {{--<option value="{{$id}}">{{$role}}--}}
                                                                    {{--</option>--}}
                                                                    {{--@endforeach--}}
                                                                    {{--</select></td>--}}
                                                                    {{--<td td_type="search">--}}
                                                                    {{--<select class="academic_year2 form-control js-example-disabled-results myselect5 required" required>--}}
                                                                    {{--<option value=""></option>--}}
                                                                    {{--@foreach($academic_year_table as $acadimic)--}}
                                                                    {{--<option from="{{$acadimic->start}}"--}}
                                                                    {{--to="{{$acadimic->end}}"--}}
                                                                    {{--value="{{$acadimic->acadimic_id}}">{{$acadimic->year_name}}--}}
                                                                    {{--</option>--}}
                                                                    {{--@endforeach--}}
                                                                    {{--</select></td>--}}

                                                                    {{--<td td_type="date">--}}
                                                                    {{--<div class="input-group required">--}}
                                                                    {{--<input type="text"--}}
                                                                    {{--class="form-control  start_in required alldate" required>--}}
                                                                    {{--<div class="input-group-addon">--}}
                                                                    {{--<i class="fa fa-calendar">--}}
                                                                    {{--</i>--}}
                                                                    {{--</div>--}}
                                                                    {{--</div>--}}
                                                                    {{--</td>--}}

                                                                    {{--<td>--}}
                                                                    {{--<button class="set_all_disabled btn btn-primary btn-xs Save_row" type="button">--}}
                                                                    {{--Save--}}
                                                                    {{--</button>--}}
                                                                    {{--</td>--}}
                                                                    {{--<td>--}}
                                                                    {{--<button type="button" class="set_all_disabled btn btn-danger btn-xs delete_row">--}}
                                                                    {{--<span class="lnr lnr-trash"></span>--}}
                                                                    {{--</button>--}}
                                                                    {{--</td>--}}

                                                                    {{--</tr>--}}

                                                                @endif

                                                                </tbody>
                                                            </table>
                                                            <div class="row">
                                                                <div class="col-md-4 col-lg-4 ">
                                                                    <button type="button" class="btn btn-primary add_row set_all_disabled"
                                                                            id="add_row" @if(isset($employee)) disabled @endif>Add Row
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>

                                                    {{--<div class="step-pane" id="" data-step="4">--}}
                                                    {{--<form class="form-horizontal" id="step4_validation-form"--}}
                                                    {{--novalidate="novalidate">--}}

                                                    {{--<div class="center">--}}
                                                    {{--<h3 class="blue lighter">Congratulation !  Please Save Your Data.  </h3>--}}
                                                    {{--</div>--}}
                                                    {{--</form>--}}
                                                    {{--</div>--}}

                                                </div>
                                            </div>

                                            <div class="wizard-actions">
                                                <button class="btn btn-prev" id="priv">
                                                    <i class="ace-icon fa fa-arrow-left"></i>
                                                    Prev
                                                </button>

                                                <button class="btn btn-success btn-next" id="finish" data-last="Finish">
                                                    Next
                                                    <i class="ace-icon fa fa-arrow-right icon-on-right"></i>
                                                </button>
                                            </div>
                                        </div><!-- /.widget-main -->
                                    </div><!-- /.widget-body -->


                                    <div id="modal-wizard" class="modal">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div id="modal-wizard-container">
                                                    <div class="modal-header">
                                                        <ul class="steps">
                                                            <li data-step="1" class="active">
                                                                <span class="step">1</span>
                                                                <span class="title">Validation states</span>
                                                            </li>

                                                            <li data-step="2">
                                                                <span class="step">2</span>
                                                                <span class="title">Alerts</span>
                                                            </li>


                                                            <li data-step="3">
                                                                <span class="step">3</span>
                                                                <span class="title">Payment Info</span>
                                                            </li>
                                                        </ul>
                                                    </div>

                                                    <div class="modal-body step-content">
                                                        <div class="step-pane active" data-step="1">
                                                            <div class="center">
                                                                <h4 class="blue">Step 1</h4>
                                                            </div>
                                                        </div>

                                                        <div class="step-pane" data-step="2">
                                                            <div class="center">
                                                                <h4 class="blue">Step 2</h4>
                                                            </div>
                                                        </div>



                                                        <div class="step-pane" data-step="3">
                                                            <div class="center">
                                                                <h4 class="blue">Step 3</h4>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="modal-footer wizard-actions">
                                                    <button class="btn btn-sm btn-prev">
                                                        <i class="ace-icon fa fa-arrow-left"></i>
                                                        Prev
                                                    </button>

                                                    <button class="btn btn-success btn-sm btn-next" data-last="Finish">
                                                        Next
                                                        <i class="ace-icon fa fa-arrow-right icon-on-right"></i>
                                                    </button>

                                                    <button class="btn btn-danger btn-sm pull-left"
                                                            data-dismiss="modal">
                                                        <i class="ace-icon fa fa-times"></i>
                                                        Cancel
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- PAGE CONTENT ENDS -->
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div><!-- /.page-content -->


                    </div>
                    <br>
                </div>
            </div>

        </div>
        <!-- END INPUTS -->


    </div>


    <div id="throbber" style="display:none">


        <img src="{{asset('assets/img/loading.gif')}}" alt="loading" >
    </div>
@endsection


@section('custom_footer')

    {{----------------------------------------  summit all form --------------------------------------------------------------}}
    <script>

        $(document).on('click', '#save_po', function () {
            submit_all();

            $.blockUI({message: $('#throbber'),
                css: { backgroundColor: 'transparent',
                    border:'none'
                }
            });
        });

        function submit_all() {


            var edit_employee_id = $('#set_po_id').attr('current_po_id');
            /*------------------------------------- employee data----------------------------------------------------------------------*/

            var employee_name = $('#employee_name').val();
            var middle_name = $('#middle_name').val();
            var family_name = $('#family_name').val();
            var parent_email = $('#parent_email').val();
            var country = $('#country').val();
            var partner_first_name = $('#partner_first_name').val();
            var partner_middle_name = $('#partner_middle_name').val();
            var partner_last_name = $('#partner_last_name').val();
            var partner_family_name = $('#partner_family_name').val();
            var children_number = $('#children_number').val();

            var last_name = $('#last_name').val();
            var user_name = $('#user_name').val();
            var gender = $('input[name=gender]:checked').val();
            var nation = $('#nation').val();
            var dateofbirth = $('#date_birth').val();
            var phone = $('#phone').val();
            var office_loc = $('#office_loc').val();
            var address = $('#address').val();

            /*---------------------------------------------*/

            var user_type = $('input[name=foreigner]:checked').val();

            var Marital_Status = $('input[name=status]:checked').val();

            var family = $('#family').val();

            var inputss = $('#emp_family > tbody  > tr');
            var emp_family_row = [];


            var Percentage_Salary = $('#number').val();
            var contract_years = $('#contract_years').val();
            var academic_year2 = $('#academic_year2').val();

            var depart = $('#depart').val();
            var job = $('#job').val();
            var manager = $('#manager').val();


            var Date_of_contract = $('#date2').val();

            var leave = $('input[name=leave]:checked').val();

            var Date_Leaving = $('#date3').val();


            var input2 = $('#allow_table > tbody  > tr');
            var allow_table_row = [];


            var input3 = $('#salary_table > tbody  > tr');
            var salary_table_row = [];


            /*------------------------------------------------------------------------------------------------------------------------------------------------*/

            /* console.log(class_name);
             console.log(cost_code);*/

            var form = document.getElementById("image_form");
            var formData = new FormData(form);
            formData.append('_token', '{{csrf_token()}}');




            /*------- employee data -------*/
            formData.append('edit_employee_id', edit_employee_id);
            formData.append('employee_name', employee_name);
            formData.append('middle_name', middle_name);
            formData.append('last_name', last_name);
            formData.append('children_number', children_number);
            formData.append('family_name', family_name);
            formData.append('country', country);
            formData.append('parent_email', parent_email);
            formData.append('partner_first_name', partner_first_name);
            formData.append('partner_family_name', partner_family_name);
            formData.append('partner_middle_name', partner_middle_name);
            formData.append('partner_last_name', partner_last_name);

            formData.append('user_name', user_name);
            formData.append('gender', gender);
            formData.append('nation', nation);
            formData.append('dateofbirth', dateofbirth);
            formData.append('phone', phone);
            formData.append('office_loc', office_loc);
            formData.append('address', address);
            formData.append('user_type', user_type);
            formData.append('Marital_Status', Marital_Status);
            // formData.append('With_partner', With_partner);
            formData.append('family', family);

            var i = 0;
            var j = 0;
            var k = 0;

            inputss.each(function () {
                if (!$(this).hasClass('added_now')) {
                    formData.append('emp_family_row[' + i + '][' + 0 + ']', $(this).find('.child_name_v').attr('current_value'));
                    formData.append('emp_family_row[' + i + '][' + 1 + ']', $(this).find('.child_middle_name_v').attr('current_value'));
                    formData.append('emp_family_row[' + i + '][' + 2 + ']', $(this).find('.child_last_name_v').attr('current_value'));
                    formData.append('emp_family_row[' + i + '][' + 3 + ']', $(this).find('.class_name_v').attr('current_value'));
                }
                i++;
            });

            input2.each(function () {

                if (!$(this).hasClass('added_now')) {
                    formData.append('allow_table_row[' + k + '][' + 0 + ']', $(this).find('.allow_name_v').attr('current_value'));
                    formData.append('allow_table_row[' + k + '][' + 1 + ']', $(this).find('.academic_year1_v').attr('current_value'));
                    formData.append('allow_table_row[' + k + '][' + 2 + ']', $(this).find('.from_date_v').attr('current_value'));
                    formData.append('allow_table_row[' + k + '][' + 3 + ']', $(this).find('.to_date_v').attr('current_value'));
                    formData.append('allow_table_row[' + k + '][' + 4 + ']', $(this).find('.value_v').attr('current_value'));

                }
                k++

            });

            input3.each(function () {

                if (!$(this).hasClass('added_now')) {
                    formData.append('salary_table_row[' + j + '][' + 0 + ']', $(this).find('.sub_name_v').attr('current_value'));
                    formData.append('salary_table_row[' + j + '][' + 1 + ']', $(this).find('.academic_year2_v').attr('current_value'));
                    formData.append('salary_table_row[' + j + '][' + 2 + ']', $(this).find('.start_in_v').attr('current_value'));

                }
                j++;
            });


            formData.append('Percentage_Salary', Percentage_Salary);
            formData.append('contract_years', contract_years);
            formData.append('academic_year2', academic_year2);
            formData.append('depart', depart);
            formData.append('job', job);
            formData.append('manager', manager);
            formData.append('Date_of_contract', Date_of_contract);
            formData.append('leave', leave);
            formData.append('Date_Leaving', Date_Leaving);


            console.log(formData);

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/employees/',
                dataType: 'json',
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,

                success: function (response) {
                    console.log(response);
                    if (response['error']) {
                        $('.err_input').removeClass('red-border');
                        $('.err_input').parent().find('.select2').removeClass('red-border');
                        $('.errorController').empty();
                        // $('.errorMessage0').show();

                        $('.errorMessage1').hide();
                        $.each(response['error'], function (key, val) {

                            notification('glyphicon glyphicon-warning-sign','Warning',val,'danger');
                            // var row = '<div>'+val+'</div><br>'
                            // $('.errorController').append(row);
                            // $('.errorController').css("display","block");

                            console.log(key + '_error');
                            $("." + key + '_error').addClass('red-border');
                            $('.' + key + '_error').parent().find('.select2').addClass('red-border');

                        })
                        setTimeout($.unblockUI, 500);
                    }
                    else {
                        $('.errorMessage0').hide();

                        $('.errorMessage1').hide();
                        $('.err_input').removeClass('red-border');
                        $('.err_input').parent().find('.select2').removeClass('red-border');
                        $('#set_po_id').attr('current_po_id', response['employee_id']);

                        $('#save_po').text("Edit");
                        $('#save_po').attr('id', 'edit_po');
                        $('#add_row').prop('disabled', true);

                        $('.set_all_disabled').prop('disabled', true);
                        $('#creat_dep').removeClass('hide');

                        $('#more').removeClass('hide');


                        $('#edit_po').removeAttr('disabled');
                        //po-active
                        console.log(response);

                        notification('glyphicon glyphicon-ok-sign','Congratulations!','Employee Saved Succefully.','success')
                        setTimeout($.unblockUI, 500);

                    }

                },
                error: function (response) {
                    alert(' Cant Save This Documents !');
                    $('#save_po').prop('disabled', false);
                    setTimeout($.unblockUI, 500);
                }

            });


        }

    </script>



    {{-------------------------------------------- return father data  -----------------------------------------------------}}



    <script src="{{asset('assets/scripts/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('js/item_name_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/tax_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/select2.min.js')}}"></script>


    <!-----------------active link in nav and active link in sidebar ---->
    {{--------------------  active side & nav bar ------------------------}}
    <script>
        $('.side_sheets').addClass('hide');
        $('#HR').removeClass('hide');

        $("#menu li").removeClass('active');
        $('#hr_nav_bar').addClass('active');

        $('#pages ul li a').removeClass('active');
        $('#admin_adduser_bar').addClass('active');

    </script>




    <!--- ace Scripts -->
    <script type="text/javascript">
        if ('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
    </script>

    <script src="{{asset('assets/scripts/ace-extra.min.js')}}"></script>

    <!-- page specific plugin scripts -->
    <script src="{{asset('assets/scripts/wizard.min.js')}}"></script>
    <script src="{{asset('assets/scripts/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/scripts/jquery-additional-methods.min.js')}}"></script>
    <script src="{{asset('assets/scripts/bootbox.js')}}"></script>
    <script src="{{asset('assets/scripts/jquery.maskedinput.min.js')}}"></script>
    <!-- ace scripts -->
    <script src="{{asset('assets/scripts/ace-elements.min.js')}}"></script>
    <script src="{{asset('assets/scripts/ace.min.js')}}"></script>


    {{-----------  check on next  -----------}}
    <script>
        $(document).on('click','#finish',function(){

            var all_steps =  $(this).parents('.widget-main').find('.step-content');
            var step = all_steps.find(".step-pane:visible");
            console.log('data-step : ' + step.attr('data-step'));

        });
    </script>

    {{------------  add new row  ------------------------}}
    <script>
        $(document).on('click', '#add_row', function () {
            $(this).attr('disabled', true);
            $('#priv').attr('disabled', true);
            $('#save_po').prop('disabled', true);
            $('.edit_row').prop('disabled', true);
            $('.delete_row').prop('disabled', true);
            var table = $(this).closest('.row').siblings('.table').attr('id');

            console.log($(this).closest('.row').siblings('.table').attr('id'));

            if (table == "emp_family") {

                var row = '<tr class= "added_now" >' +
                    '<td td_type="input"><input class="child_name row_data form-control" data-role="input" type="text"/></td>' +
                    '<td td_type="input"><input class="child_middle_name row_data form-control" data-role="input" type="text"/></td>' +
                    '<td td_type="input"><input class="child_last_name row_data form-control" data-role="input" type="text"/></td>' +
                    '<td td_type="search">' +
                    '<select class="class_name form-control js-example-disabled-results">\n'
                    + '<option value="">' + '' + '</option>'
                        @foreach($class as $id=>$role)
                    + '<option value="{{$id}}">' + '{{$role}}'
                        @endforeach
                    + '</select></td>'
                    + '<td>'
                    + '<button type="button" class="set_all_disabled btn btn-primary btn-xs Save_row">' + 'Save' + '</button>'
                    + '</td>'
                    + '<td>'
                    + '<button type="button" class="set_all_disabled btn btn-danger btn-xs delete_row"  >' + '<span class="lnr lnr-trash"></span>' + '</button>'
                    + '</td>'

                    + '</tr>';
                $("#emp_family").prepend(row);
                $(".js-example-disabled-results").select2();

            }
            {{--else if (table == "allow_table") {--}}

                    {{--var row = '<tr class= "added_now" >' +--}}
                    {{--'<td td_type="search">' +--}}
                    {{--'<select class="allow_name form-control js-example-disabled-results">\n'--}}
                    {{--+ '<option value="">' + '' + '</option>'--}}
                    {{--@foreach($allows as $id=>$role)--}}
                    {{--+ '<option allow_value ="" value="{{$id}}">' + '{{$role}}'--}}
                    {{--+ '</option>'--}}
                    {{--@endforeach--}}
                    {{--+ '</select></td>' +--}}
                    {{--'<td td_type="search">' +--}}
                    {{--'<select class="academic_year1 form-control js-example-disabled-results">\n'--}}
                    {{--+ '<option value="">' + '' + '</option>'--}}
                    {{--@foreach($academic_year_table as $acadimic)--}}
                    {{--+ '<option from ="{{$acadimic->start}}" to ="{{$acadimic->end}}" value="{{$acadimic->acadimic_id}}">' + '{{$acadimic->year_name}}'--}}
                    {{--+ '</option>'--}}
                    {{--@endforeach--}}
                    {{--+ '</select></td>'--}}

                    {{--+ '<td td_type="date">'--}}
                    {{--+ '<div class="input-group ">'--}}
                    {{--+ '<input type="text" id="date3" class="form-control c_date from_date" name="from_date">'--}}
                    {{--+ '<div class="input-group-addon">'--}}
                    {{--+ '<i class="fa fa-calendar">'--}}
                    {{--+ '</i>'--}}
                    {{--+ '</div>'--}}
                    {{--+ '</div>'--}}
                    {{--+ '</td>'--}}


                    {{--+ '<td td_type="date">'--}}
                    {{--+ '<div class="input-group ">'--}}
                    {{--+ '<input type="text" id="date3" class="form-control c_date to_date" name="to_date">'--}}
                    {{--+ '<div class="input-group-addon">'--}}
                    {{--+ '<i class="fa fa-calendar">'--}}
                    {{--+ '</i>'--}}
                    {{--+ '</div>'--}}
                    {{--+ '</div>'--}}
                    {{--+ '</td>'--}}


                    {{--+ '<td td_type="input">'--}}
                    {{--+ '<input class="value row_data pull-left form-control required " value="10" disabled  onkeypress="return (event.charCode == 8 || event.charCode == 1) ? null : event.charCode >= 48 && event.charCode <= 57 ||  event.charCode == 46" data-role="input" type="number"  placeholder="0"/></td>' +--}}

                    {{--'<td>'--}}
                    {{--+ '<button type="button" class="set_all_disabled btn btn-primary btn-xs Save_row">' + 'Save' + '</button>'--}}
                    {{--+ '</td>'--}}
                    {{--+ '<td>'--}}
                    {{--+ '<button type="button" class="set_all_disabled btn btn-danger btn-xs delete_row"  >' + '<span class="lnr lnr-trash"></span>' + '</button>'--}}
                    {{--+ '</td>'--}}

                    {{--+ '</tr>';--}}
                    {{--$("#allow_table").prepend(row);--}}
                    {{--$(".c_date").datepicker({format: 'yyyy-mm-dd',autoclose: true, minView: 2});--}}
                    {{--$(".js-example-disabled-results").select2();--}}
                    {{--//get_master_acc_drop_down($('.master_account'));--}}

                    {{--//$(".po_values").siblings('.select2').css('width', '71px');--}}


                    {{--}--}}
            else if (table == "salary_table") {
                var row = '<tr class= "added_now" >' +
                    '<td td_type="search">' +
                    '<select class="sub_name required form-control js-example-disabled-results">\n'
                    + '<option value="">' + '' + '</option>'
                        @foreach($sub_salary as $id=>$role)
                    + '<option value="{{$id}}">' + '{{$role}}'
                    + '</option>'
                        @endforeach
                    + '</select></td>' +
                    '<td td_type="search">' +
                    '<select class="academic_year2 required form-control js-example-disabled-results">\n'
                    + '<option value="">' + '' + '</option>'
                        @foreach($academic_year_table as $acadimic)
                    + '<option from ="{{$acadimic->start}}" to ="{{$acadimic->end}}" value="{{$acadimic->acadimic_id}}">' + '{{$acadimic->year_name}}'
                    + '</option>'
                        @endforeach
                    + '</select></td>'

                    + '<td td_type="date">'
                    + '<div class="input-group ">'
                    + '<input type="text" id="date3" class="form-control required c_date start_in">'
                    + '<div class="input-group-addon">'
                    + '<i class="fa fa-calendar">'
                    + '</i>'
                    + '</div>'
                    + '</div>'
                    + '</td>'

                    + '<td>'
                    + '<button  type="button" class="set_all_disabled btn btn-primary btn-xs Save_row">' + 'Save' + '</button>'
                    + '</td>'
                    + '<td>'
                    + '<button type="button" class="set_all_disabled btn btn-danger btn-xs delete_row">' + '<span class="lnr lnr-trash"></span>' + '</button>'
                    + '</td>'

                    + '</tr>';
                $("#salary_table").prepend(row);
                $(".c_date").datepicker({format: 'yyyy-mm-dd',autoclose: true, minView: 2});

                $(".js-example-disabled-results").select2();



            }


        });
    </script>

    {{--------   save  row  --------------}}
    <script>
        $('.errorMessage1').hide();
        $('.errorMessage2').hide();
        $('.errorMessage3').hide();
        $(document).on("click", ".Save_row", function () {

            console.log('save clicked');

            var empty = false;
            var call_save = false;
            var input = $(this).parents("tr").find('input[type="text"],input[type="number"]');
            var selectors = $(this).parents("tr").find('select');

            input.each(function () {
                if (!$(this).val() || $(this).val() == "") {
                    $(this).addClass("error");
                    empty = true;
                    notification('glyphicon glyphicon-warning-sign','Warning','Please Fill This Field','danger');

                } else {
                    $(this).removeClass("error");
                }
            });

            selectors.each(function () {
                if (!$(this).val() || $(this).val() == "" || $(this).val() < 0  ) {
                    $(this).addClass("error");
                    $(this).siblings().addClass('error');
                    empty = true;
                    notification('glyphicon glyphicon-warning-sign','Warning','PLease Choose This Field','danger');

                } else {
                    $(this).removeClass("error");
                }
            });

            console.log('empty ' + empty);

            $(this).parents("tr").find(".error").first().focus();

            console.log($(input[0]).val() + "  " + $(input[1]).val());

            var table = $(this).parents('table').attr('id');

            console.log(table)

            if(table == "salary_table" && empty !=true){
                var acadimic_from1 = $(this).parents("tr").find('.academic_year2').find('option:selected').attr('from');
                var acadimic_to1 = $(this).parents("tr").find('.academic_year2').find('option:selected').attr('to');
                var start_in = $(this).parents("tr").find('.start_in').val();
                var contract_date = $('#date2').val();
                console.log(start_in > acadimic_from1 );
                console.log(start_in);
                console.log(acadimic_from1 );
                console.log(acadimic_to1 );
                console.log(contract_date );
                if(start_in >= acadimic_from1 && start_in <= acadimic_to1 && start_in >= contract_date && contract_date >= acadimic_from1 && contract_date <= acadimic_to1) {

                    if (!empty) {
                        console.log('pass empty');
                        $(this).prop('disabled', true);
                        save_table_effect($(this));
                        $('.errorMessage1').hide();
                        $('.errorMessage2').hide();
                        $('.errorMessage3').hide();
                    }
                }else if(start_in < contract_date) {
                    $( ".start_in" ).addClass( "error" );
                    notification('glyphicon glyphicon-warning-sign','Warning','Choose Correct Start Date After Date Of Contract ','danger');
                }else if(acadimic_from1 > contract_date || acadimic_to1 < contract_date ) {
                    $( ".academic_year2" ).siblings().addClass( "error" );
                    notification('glyphicon glyphicon-warning-sign','Warning','Choose Correct  Academic Year Responsive To Date Of Contract ','danger');
                }else {
                    $( ".start_in" ).addClass( "error" );
                    $( ".academic_year2" ).siblings().addClass( "error" );
                    notification('glyphicon glyphicon-warning-sign','Warning','Choose Correct Start Date in Same Academic Year Interval ','danger');
                }

            }

            else {
                if (!empty) {
                    console.log('pass empty');
                    $(this).prop('disabled', true);
                    save_table_effect($(this));
                    $('.errorMessage2').hide();
                    $('.errorMessage1').hide();
                    $('.errorMessage3').hide();
                }
            }


        });

        function save_table_effect(thiss) {

            var table = thiss.parents('table').attr('id');

            console.log('table in save :' + table);


            if (table == "emp_family") {
                var child_name = thiss.parents("tr").find('.child_name').val();
                var child_name = thiss.parents("tr").find('.child_middle_name').val();
                var child_name = thiss.parents("tr").find('.child_last_name').val();
                var class_name = thiss.parents("tr").find('.class_name').val();

                console.log('child_name : ' + child_name + 'class_name : ' + class_name);
                console.log('passed')

                /*--------------  all select ------------*/
                var select_array = ['class_name_v'];
                var r = 0;
                var all_dowp_down = thiss.parents("tr").find('select');
                all_dowp_down.each(function () {
                    var value_select = $(this).val();
                    var text_select = $(this).find('option:selected').text();
                    $(this).parent("td").addClass(select_array[r]).attr('current_value', value_select).html(text_select);
                    r++;

                });
                /*--------- all input  ------------------*/
                var input_array = ['child_name_v','child_middle_name','child_last_name'];
                var all_input = thiss.parents("tr").find('input[type="text"]');

                var i = 0;
                all_input.each(function () {

                    var value_select = $(this).val();
                    $(this).parent("td").addClass(input_array[i]).attr('current_value', value_select).html(value_select);

                    i++;
                });
                /*--------------------------*/

            }




            else if (table == "salary_table") {


                var sub_name = thiss.parents("tr").find('.sub_name').val();
                var start_in = thiss.parents("tr").find('.start_in').val();
                var academic_year = thiss.parents("tr").find('.academic_year2').val();

                console.log('sub_name: ' + sub_name + 'start_in: ' + start_in + 'academic_year: ' + academic_year);
                console.log('passed')

                /*--------------  all select ------------*/
                var select_array = ['sub_name_v', 'academic_year2_v'];
                var r = 0;
                var all_dowp_down = thiss.parents("tr").find('select');
                all_dowp_down.each(function () {
                    var value_select = $(this).val();
                    var text_select = $(this).find('option:selected').text();
                    $(this).parent("td").addClass(select_array[r]).attr('current_value', value_select).html(text_select);
                    r++;
                });

                /*--------- all input  ------------------*/
                var input_array = ['start_in_v'];
                var all_input = thiss.parents("tr").find('input[type="text"]');

                var i = 0;
                all_input.each(function () {

                    var value_select = $(this).val();
                    $(this).parents("td").addClass(input_array[i]).attr('current_value', value_select).html(value_select);

                    i++;
                });
                /*--------------------------*/


            }


            if (thiss.parents("tr").hasClass('added_now')) {
                thiss.parents("tr").removeClass('added_now');

            }
            if (thiss.parents("tr").hasClass('edited_now')) {
                thiss.parents("tr").removeClass('edited_now');

            }


            // if ($('.added_now').length == 0) {
            $(".add_row").removeAttr("disabled");
            // }
            $('#priv').attr('disabled', false);
            $('#date2').attr('disabled', true);
            $('#save_po').prop('disabled', false);
            $('.add_row').prop('disabled', false);
            $('.delete_row').prop('disabled', false);
            $('.edit_row').prop('disabled', false);
            $('.delete_row').prop('disabled', false);
            thiss.text('Edit');
            thiss.removeClass('Save_row');
            thiss.addClass('edit_row');
            thiss.prop('disabled', false);
            $(".errorTableMessage").hide();

        }

    </script>

    {{----------------------  edit row --------------------}}
    <script>
        $(document).on("click", ".edit_row", function () {


            $('#priv').attr('disabled', true);
            $('#save_po').prop('disabled', true);
            $('.add_row').prop('disabled', true);
            $('.delete_row').prop('disabled', true);

            var table = $(this).parents('table').attr('id');

            console.log(table);

            if (table == "emp_family") {


                if (!$('#emp_family tr').hasClass('added_now')) {

                    var input_class_array = ['child_name','child_middle_name','child_last_name', 'class_name'];
                    var s = 0;

                    $('#add_row').prop('disabled', true);
                    $(this).parents("tr").addClass('edited_now');

                    $(this).parents("tr").find("td:not(:last-child)").each(function () {

                        if ($(this).attr('td_type') == 'input') {
                            $(this).html('<input class="' + input_class_array[s] + ' row_data form-control" data-role="input" type="text" value="' + $(this).text() + '"/>');
                            s++;
                        }

                        else if ($(this).attr('td_type') == 'search') {
                            if ($(this).hasClass('class_name_v')) {
                                $(this).html('<select class="class_name form-control js-example-disabled-results">\n'
                                    + '<option value="">' + '' + '</option>'
                                        @foreach($class as $id=>$role)
                                    + '<option value="{{$id}}">' + '{{$role}}'
                                        @endforeach
                                    + '</option>'
                                    + '</select>');

                                var selected_v = $(this).attr('current_value');
                                $(this).find(".class_name").val(selected_v);
                                $(".calss_name").select2();
                                $(".class_name").siblings('.select2').css('width', '71px');
                            }


                        }


                    });


                }

            }

            else if (table == "salary_table") {

                if (!$('#salary_table tr').hasClass('added_now')) {

                    var input_class_array = ['sub_name', 'academic_year2'];
                    var s = 0;

                    $('#add_row').prop('disabled', true);
                    $(this).parents("tr").addClass('edited_now');

                    $(this).parents("tr").find("td:not(:last-child)").each(function () {
                        if ($(this).attr('td_type') == 'date') {
                            var i =6
                            $(this).html('<input class="start_in row_data form-control alldate" data-role="input"  type="text" value="' + $(this).text() + '"/>');

                            $('.alldate').datepicker({format: 'yyyy-mm-dd',autoclose: true, minView: 2 });
                        }

                        else if ($(this).attr('td_type') == 'search') {
                            if ($(this).hasClass('sub_name_v')) {
                                $(this).html('<select class="sub_name form-control js-example-disabled-results">\n'
                                    + '<option value="">' + '' + '</option>'
                                        @foreach($sub_salary as $id=>$role)
                                    + '<option value="{{$id}}">' + '{{$role}}'
                                    + '</option>'
                                        @endforeach
                                    + '</option>'
                                    + '</select>');

                                var selected_v = $(this).attr('current_value');
                                $(this).find(".sub_name").val(selected_v);
                                $(this).find(".sub_name").select2();
                                $(this).find(".sub_name").siblings('.select2').css('width', '71px');
                            }
                            else if ($(this).hasClass('academic_year2_v')) {
                                $(this).html('<select class="academic_year2 form-control js-example-disabled-results">\n'
                                    + '<option value="">' + '' + '</option>'
                                        @foreach($academic_year_table as $acadimic)
                                    + '<option from ="{{$acadimic->start}}" to ="{{$acadimic->end}}" value="{{$acadimic->acadimic_id}}">' + '{{$acadimic->year_name}}'
                                    + '</option>'
                                        @endforeach
                                    + '</option>'
                                    + '</select>');

                                var selected_v = $(this).attr('current_value');
                                $(this).find(".academic_year2").val(selected_v);
                                $(this).find(".academic_year2").select2();
                                $(this).find(".academic_year2").siblings('.select2').css('width', '71px');
                            }

                            s++;
                        }


                    });


                }

            }

            $("#add_row").attr("disabled", "disabled");
            $(this).text('Save');
            $(this).removeClass('edit_row');
            $(this).addClass('Save_row');
        });

    </script>
    <!--delete row in table -->
    <script>

        $(document).on('click', '.delete_row', function () {

            $('#priv').attr('disabled', true);
            $('#save_po').prop('disabled', true);
            $('#finish').prop('disabled', true);
            $('.add_row').prop('disabled', true);
            $('.edit_row').prop('disabled', true);

            $(this).hide(function () {
                $(this).closest('td').append('<button class="btn btn-danger set_all_disabled" type="button" id="yes" style="display: none;">yes</button>' +
                    '<button class="btn btn-default set_all_disabled" type="button" id="no" style="display: none;">no</button>');
                $(this).closest('td').find("#yes").show('slow').delay(1000);
                $(this).closest('td').find("#no").show('slow').delay(1000);

            });
        });

        $(document).on('click', '#yes', function () {

            var rowCount = $("#salary_table > tbody").children.length -1;
            console.log('rowCount :' + rowCount);
            if (rowCount == 0) {
                $('#date2').attr('disabled', false);
            }
            $(this).closest('tr').remove();
            $(".add_row").removeAttr("disabled");
            $('#priv').attr('disabled', false);
            $('#save_po').prop('disabled', false);
            $('#finish').prop('disabled', false);
            $('.add_row').prop('disabled', false);
            $('.edit_row').prop('disabled', false);
            $('.edit_row').prop('disabled', false);
        });

        $(document).on('click', '#no', function () {

            console.log('noo');

            $(this).closest('td').find(".delete_row").show('slow').delay(1000);
            $(this).closest('td').find("#yes").hide('slow').delay(1000).remove();
            $(this).closest('td').find("#no").hide('slow').delay(1000).remove();
            $('#priv').attr('disabled', false);
            $('#save_po').prop('disabled', false);
            $('#finish').prop('disabled', false);
            $('.add_row').prop('disabled', false);
            $('.edit_row').prop('disabled', false);


        });


    </script>
    <!-- inline scripts related to this page    form ace_master -->
    <script type="text/javascript">
        jQuery(function ($) {

            $('[data-rel=tooltip]').tooltip();

            $('.select2').css('width', '200px').select2({allowClear: true})
                .on('change', function () {
                    $(this).closest('form').validate().element($(this));
                });


            var $validation = true;
            var $validation_father = true;
            $('#fuelux-wizard-container')
                .ace_wizard({

                })
                .on('actionclicked.fu.wizard', function (e, info) {

                    if (info.step == 1 && $validation){}

                    if (!$('#step1_validation-form').valid())
                    {
                        notification('glyphicon glyphicon-warning-sign','Warning','You Should Fill All Fields','danger');
                        e.preventDefault();
                    }








                    if (info.step == 2 && $validation_father && info.direction === 'next') {


                        if (!$('#step2_validation-form').valid()) {

                            notification('glyphicon glyphicon-warning-sign','Warning','You Should Fill All Fields','danger');

                            e.preventDefault();
                        }
                    }


                    if (info.step == 3 && $validation_father && info.direction === 'next') {

                        var rowCount = $('#salary_table tbody .added_now').length;
                        var saved_row_Count = $('#salary_table tbody tr:not(.added_now)').length;
                        var edited_row_Count = $('#salary_table tbody .edited_now').length;

                        console.log('rowCount :' + rowCount);
                        if (rowCount > 0) {
                            e.preventDefault();
                            notification('glyphicon glyphicon-warning-sign','Warning','You Should Fill All Table Field','danger');

                        }
                        else if (saved_row_Count == 0) {
                            e.preventDefault();
                            $(".alert-danger").hide();
                            notification('glyphicon glyphicon-warning-sign','Warning','You Should Save Table Row ' ,'danger');
                        }
                        else if (edited_row_Count > 0) {
                            e.preventDefault();
                            notification('glyphicon glyphicon-warning-sign','Warning','You Should Complete Edit Process','danger');                     }

                        else {
                            $(".alert-danger").hide();
                            $(".errorTableMessage").hide();
                            if (!$('#step4_validation-form').valid()) e.preventDefault();
                        }
                    }


                }).on('finished.fu.wizard', function(e) {

                if ($('#save_po').length) {
                    console.log('finished ');
                    notification('glyphicon glyphicon-ok-sign', '', 'Please Press Save ', 'success');
                }
            }).on('stepclick.fu.wizard', function (e) {
                //e.preventDefault();//this will prevent clicking and selecting steps

            });


            /*------------------------  step1 vali  form  ------------------------------------*/
            var validator1 = $('#step1_validation-form').validate({
                errorElement: 'div',
                errorClass: 'help-block',
                focusInvalid: false,
                ignore: "",
                rules: {
                    employee_name: {
                        required: true,

                    },
                    parent_email: {
                        required: true,

                    },
                    last_name: {
                        required: true,

                    },
                    country: {
                        required:true,
                    },
                    email: {
                        required:true,
                    },
                    middle_name	: {
                        required: true,

                    },
                    family_name	: {
                        required: true,

                    },gender: {
                        required: true,
                    },
                    form_field_radio: {
                        required: true,
                    },
                    dateofbirth: {
                        max: '{{$min_age}}',
                        min: '',
                        required: true,
                    },
                    nation: {
                        required: true,
                    },
                },
                messages: {
                    gender: "Please choose gender",
                    employee_name: "Please Enter Employee First Name",
                    middle_name: "Please Enter Employee Middle Name",
                    last_name: "Please Enter Employee Last Name",
                    family_name: "Please Enter Employee Family Name",
                    acadmic_year: "Please choose Academic year",
                    country: "Please Choose Country",
                    email: "Please Enter Valid E-mail",
                },

                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
                },

                success: function (e) {
                    $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
                    $(e).remove();
                },

                errorPlacement: function (error, element) {
                    if (element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
                        var controls = element.closest('div[class*="col-"]');
                        if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                        else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                    }
                    else if (element.is('.select2')) {
                        error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                    }
                    else if (element.is('.chosen-select')) {
                        error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                    }
                    else error.insertAfter(element.parent());
                },

                submitHandler: function (form) {
                },
                invalidHandler: function (form) {
                }
            });


            /*------------------------  step2 validate form  ------------------------------------*/

            var validator2 = $('#step2_validation-form').validate({
                errorElement: 'div',
                errorClass: 'help-block',
                focusInvalid: false,
                ignore: "",
                rules: {
                    foreigner: {
                        required: true,
                    },
                    children_number: {
                        required: false,
                    },
                    partner_first_name: {
                        required: false,
                    },
                    partner_middle_name: {
                        required: false,
                    },
                    partner_last_name: {
                        required: false,
                    },
                    partner_family_name: {
                        required: false,
                    },
                    status: {
                        required: true,
                    },
                    percentage_salary: {
                        required: true,

                    },
                    class: {
                        required: true,
                    },
                    academic_year2: {
                        required: true,
                    },
                    depart: {
                        required: true,

                    },
                    job: {
                        required: true,
                    },
                    dateofcontract: {
                        required: true
                    },
                    contract_years: {
                        required: true,
                        min: '1',

                    },
                    manager: {
                        required: true
                    },
                    leave: {
                        required: true
                    },

                },

                messages: {

                    foreigner: "Please choose Foreigner",
                    leave: "Please Choose Leave",
                    manager: "Please Choose Manager",
                    dateofcontract: "Please Choose Date Of Contract",
                    job: "Please choose Job",
                    contract_years: "Please choose Contract Years",
                    department: "Please Choose Department",
                    state: "Please Choose State",
                },


                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
                },

                success: function (e) {
                    $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
                    $(e).remove();
                },

                errorPlacement: function (error, element) {
                    if (element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
                        var controls = element.closest('div[class*="col-"]');
                        if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                        else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                    }
                    else if (element.is('.select2')) {
                        error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                    }
                    else if (element.is('.chosen-select')) {
                        error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                    }
                    else error.insertAfter(element.parent());
                },

                submitHandler: function (form) {
                },
                invalidHandler: function (form) {
                }
            });



            /*--------------------------------  step3 validate form -----------------------------------------------*/
            var validator3 = $('#step3_validation-form').validate({
                errorElement: 'div',
                errorClass: 'help-block',
                focusInvalid: false,
                ignore: "",
                rules: {

                },
                messages: {
                },

                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
                },

                success: function (e) {
                    $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
                    $(e).remove();
                },

                errorPlacement: function (error, element) {
                    if (element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
                        var controls = element.closest('div[class*="col-"]');
                        if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                        else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                    }
                    else if (element.is('.select2')) {
                        error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                    }
                    else if (element.is('.chosen-select')) {
                        error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                    }
                    else error.insertAfter(element.parent());
                },

                submitHandler: function (form) {
                },
                invalidHandler: function (form) {
                }
            });
            /*----------------------------------------------------------------------------------*/

            $('#modal-wizard-container').ace_wizard();
            $('#modal-wizard .wizard-actions .btn[data-dismiss=modal]').removeAttr('disabled');
            $(document).one('ajaxloadstart.page', function (e) {
                //in ajax mode, remove remaining elements before leaving page
                $('[class*=select2]').remove();
            });
        })
    </script>

    <!--show and hide sibiling-->

    <script>
        $(function () {
            $("input[name='sibling']").click(function () {
                if ($("#yes").is(":checked")) {
                    $(".code").show();
                    $(".first").show();
                    $(".sir").show();
                } else {
                    $(".code").hide();
                    $(".first").hide();
                    $(".sir").hide();
                }
            });
        });
    </script>

    <script>
        $(".staff_namef").hide();


        $(function () {

            $("input[name='rel']").click(function () {
                if ($("#yes3").is(":checked")) {
                    $(".staff_namef").show();

                } else {
                    $(".staff_namef").hide();

                }
            });
        });

    </script>

    <script>
        $(".staff_namem").hide();
        $(function () {
            $("input[name='rel1']").click(function () {
                if ($("#yes4").is(":checked")) {
                    $(".staff_namem").show();

                } else {
                    $(".staff_namem").hide();

                }
            });
        });

    </script>

    <!--select  -->

    <script>
        $(".myselect1").select2({
            width: 'resolve'

        });

        $(".myselect2").select2({
            width: 'resolve',


        });
        $(".myselect3").select2({
            width: 'resolve',


        });
        $(".myselect4").select2({
            width: 'resolve',


        });
        $(".myselect5").select2({
            width: 'resolve'

        });

        $(".myselect6").select2({
            width: 'resolve',


        });
        $(".myselect7").select2({
            width: 'resolve',


        });
        $(".myselect8").select2({
            width: 'resolve',


        });

    </script>
    <!--radio buttons checked on load -->
    <script>

        $(function () {
            var $radios = $('input:radio[name=finance]');
            if ($radios.is(':checked') === false) {
                $radios.filter('[value=yes5]').prop('checked', true);
            }
        });

        $(function () {
            var $radios1 = $('input:radio[name=finance1]');
            if ($radios1.is(':checked') === false) {
                $radios1.filter('[value=no6]').prop('checked', true);
            }
        });

        $("#yes5").prop('checked', true).trigger("change");
        $("#no1").prop('checked', true).trigger("change");

    </script>

    <!----- upload photo ---->
    <script>

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#imagePreview').css('background-image', 'url(' + e.target.result + ')');
                    $('#imagePreview').hide();
                    $('#imagePreview').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#imageUpload").change(function () {
            readURL(this);
        });
    </script>

    <!-- show and hide partner for married   -->

    <!-- show and hide date of leaving -->
    <script>

        $(function () {

            $("input[name='leave']").click(function () {
                if ($("#yes7").is(":checked")) {
                    $(".leaving").show();
                    $(".leaving").removeClass('hide');
                } else {
                    $(".leaving").hide();
                }
            });
        });

    </script>
    <!-- show and hide family for Employee -->



    <!-- ajax request propagate -->
    <script>

        $(document).ready(function () {
            $(document).on('change', '#depart', function () {
                var id = $(this).val()
                $.ajax({
                    url: '/get_job/',
                    type: 'get',
                    dataType: 'html',
                    data: {id: id},
                    success: function (data) {
                        $('#job').html(data)
                    }
                })
            });
        });
    </script>

    <script>

        $(document).ready(function () {
            $(document).on('change', '#depart', function () {
                var id = $(this).val()
                $.ajax({
                    url: '/get_manager/',
                    type: 'get',
                    dataType: 'html',
                    data: {id: id},
                    success: function (data) {
                        $('#manager').html(data)
                    }
                })
            });
        });
    </script>

    <script>


        $(document).on('change', '.allow_name', function () {
            var allow = $(this);

            var id = allow.val();
            var type = $('#Foreigner').val();
            if (type == "foreign"){ var currency = 1}
            else{var currency = 2}
            console.log( id + type +currency )
            $.ajax({
                url: '/get_allow_value/',
                type: 'get',
                dataType: 'json',
                data: {id: id, currency: currency},
                success: function (data) {
                    allow.parents('tr').find('.value').attr('value', data);
                    console.log('pass');
                    console.log(allow.parents("tr").attr('class'));
                }
            })
        });

    </script>

    <!-- show and edit page convertation-->

    <script>
        $(document).on('click', '#edit_po', function () {
            $('#edit_po').text("Save");
            $('#edit_po').attr('id', 'save_po');
            $('.add_row').prop('disabled', false);
            $('.set_all_disabled').prop('disabled', false);
            $('#creat_dep').addClass('hide');
            $('#more').addClass('hide');
        });
    </script>

    <!-- on click finished - show message -->
    <script>
        $("#save_po").attr('disabled', true);
        $('.alert-autocloseable-success').hide();
        $(document).on('click', '#finish', function () {
            if ($(this).text() == 'Finish') {
                $('html, body').animate({scrollTop: 0}, 'fast');
                $("#save_po").attr('disabled', false);

            }

        });

    </script>

    {{-----------------------print----------------}}
    <script>

        $(document).on('click', '#print', function () {

            var id = $('#set_po_id').attr('current_po_id');
            window.open('/get_employee_rpt/' + id + '', '_blank');

        });

    </script>


    {{-----------------------print----------------}}
    <script>
        $(document).on('click', '#creat_dep', function () {
            window.open('/employees/create', '_self');
        });
    </script>

    <script>
        $('#date_birth').datepicker({format: 'yyyy-mm-dd',autoclose: true, minView: 2 ,todayBtn: true ,todayHighlight:true});

    </script>

    <!-- show married or family or single @edit -->
    <script>



        $(document).ready(function() {

            @if(isset($employee))

            @if($employee->marital_status =="married" && $employee->family =="yes")
            $(".with_family").show();
            $('.partner_info').show();
            @elseif($employee->marital_status =="married")
            $('.with_family').show();
            @else
            $(".with_family").hide();
            $('.partner_info').hide();
            @endif
            @else
            $(".with_family").hide();
            $('.partner_info').hide();

            @endif



            $('input[type="radio"]').click(function () {
                if ($(this).attr("value") == "single") {

                    $(".with_family").hide();
                    $(".partner_info").hide();

                }
                if ($(this).attr("value") == "married"){

                    $(".with_family").show();

                }

            });

            $('#family').click(function() {

                if ($(this).is(':checked')) {
                    $('.partner_info').show();
                    $('#family').attr('value',"yes");
                    console.log($('#family').val());

                } else {

                    $('.partner_info').hide();
                    $('#family').attr('value',"no");
                    console.log($('#family').val());
                }
            });
        });





    </script>
@endsection@extends('layouts.main_app')

@section('main_content')
    <!-- ace CSS -->
    <link rel="stylesheet" href="{{asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/select2.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/css/fonts.googleapis.com.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/css/ace.min.css')}}" class="ace-main-stylesheet" id="main-ace-style"/>
    <link rel="stylesheet" href="{{asset('assets/css/ace-skins.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/css/ace-rtl.min.css')}}"/>
    <link href="{{asset('css/fastselect.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/select2.min.css')}}" rel="stylesheet"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        body {

            font-family: Gill Sans, Gill Sans MT, Calibri, sans-serif !important;
            font-size: 15px !important;
            color: #676a6d !important;
        }

        .navbar .navbar-nav > li > a {
            font-size: 15px !important;
        }

        .control-label {
            font-size: 15px !important;
        }

        .navbar-default .navbar-nav > .active > a, .navbar-default .navbar-nav > .active > a:focus, .navbar-default .navbar-nav > .active > a:hover {
            background-color: transparent;
            COLOR: #00AAFF !important;
            FONT-SIZE: 20PX !important;
        }

        .side_sheets {
            padding: 0;
            margin: 0 1px 10px 0px;
        }

        .navbar-default .navbar-nav > li > a:focus, .navbar-default .navbar-nav > li > a:hover {
            color: #bbb !important;
        }

        #student_side_bar {
            padding-right: 123px;
        }

        nav.navbar.navbar-default.navbar-fixed-top {
            background-color: #2B333E;
        }

        .navbar .navbar-nav > li {
            border: 0px !important;
        }

        .navbar2 {

            background-color: #f8f8f8 !important;
            -webkit-box-shadow: 1px 2px 3px rgba(0, 0, 0, 0.1) !important;
        }

        .navbar {
            min-height: 50px !important;

        }

        .main-container:before {
            position: relative !important;

        }

        .breadcrumb > li > a {
            font-size: 15px !important;
        }

        .btn-danger {
            color: #fff;
            background-color: #d9534f;
            border-color: #d43f3a;
        }

        .btn {
            display: inline-block;
            padding: 6px 12px;
            margin-bottom: 0;
            font-size: 14px;
            font-weight: 400;
            line-height: 1.42857143;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            touch-action: manipulation;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-image: none;
            border: 1px solid transparent;
            border-radius: 4px;
            text-shadow: 0 0px 0 rgba(0, 0, 0, .25);
        }

        .navbar-default {
            -webkit-box-shadow: 1px 2px 3px rgba(0, 0, 0, 0.1) !important;
        }

        .btn-primary {
            background-color: #00AAFF !important;
            border-color: #00a0f0 !important;
        }

        #creat {
            box-shadow: 0px 1px 2px 0 rgba(0, 0, 0, 0.2);

            color: #676a6d !important;
            background-color: #dddddd !important;
        }

        .main-content {
            padding: 60px 10px;
        }

        /*upload photo */

        .avatar-upload {
            position: relative;
            max-width: 205px;

        }

        .avatar-upload .avatar-edit {
            position: absolute;
            right: 12px;
            z-index: 1;
            top: 10px;
        }

        .avatar-upload .avatar-edit input {
            display: none;
        }

        .avatar-upload .avatar-edit input + label {
            display: inline-block;
            width: 34px;
            height: 34px;
            margin-bottom: 0;
            border-radius: 100%;
            background: #ffffff;
            border: 1px solid transparent;
            box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.12);
            cursor: pointer;
            font-weight: normal;
            transition: all 0.2s ease-in-out;
        }

        .avatar-upload .avatar-edit input + label:hover {
            background: #f1f1f1;
            border-color: #d6d6d6;
        }

        .avatar-upload .avatar-edit input + label:after {
            content: "\f040";
            font-family: "FontAwesome";
            color: #757575;
            position: absolute;
            top: 10px;
            left: 0;
            right: 0;
            text-align: center;
            margin: auto;
        }

        .avatar-upload .avatar-preview {
            width: 192px;
            height: 192px;
            position: relative;
            border-radius: 100%;
            border: 6px solid #f8f8f8;
            box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
        }

        .avatar-upload .avatar-preview > div {
            width: 100%;
            height: 100%;
            border-radius: 100%;
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
        }

        span.select2.select2-container.select2-container--default {
            width: 100% !important;
        }

        .breadcrumb > li + li:before {
            font-family: FontAwesome;
            font-size: 20px;
            content: "/" !important;
            color: #B2B6BF;
            padding: 0;
            margin: 0 8px 0 0;
            position: relative;
            top: 1px;
        }

        .collapse > .row > .col-md-4 > .btn, .collapse > .row > .col-md-4 > .dropdown > .btn {
            margin-top: 0px;
        }

        .dropdown-menu > li.active:hover > a, .dropdown-menu > li.active > a, .dropdown-menu > li:hover > a, .dropdown-menu > li > a:active, .dropdown-menu > li > a:focus {
            color: #262626;
            text-decoration: none;
            background-color: #f5f5f5;
        }

        .breadcrumb {
            display: inline-block;
            float: left;
            position: relative;
            margin-top: 0;
            margin-right: 0.2em;
            margin-bottom: 0;
            padding: 0.5em;
            color: gray;
            white-space: nowrap;
            margin-left: 0px;
        }

        .dropdown-menu > li > a {
            font-size: 14px;
            display: block;
            padding: 3px 20px;
            clear: both;
            font-weight: 400;
            line-height: 1.42857143;
            color: #333;
            white-space: nowrap;
        }

        .form-horizontal .form-group {
            margin-right: 0px;
            margin-left: 0px;
        }



        #employee_name-error{
            color: #D16E6C;
            margin-left: 254px;
        }

        #academic_year2-error{
            color: #D16E6C;
        }

        .form-group.has-error .control-label{
            color: #676a6d;
        }

        .has_error{
            border: 1px solid #D16E6C!important;
        }



        .error{
            border: 1px solid red  !important;
            border-left: 1px double red !important;
            border-top: 2px double red !important;
        }


        .red-border{
            border: 1px solid red  !important;
        }


        .required, .required + .select2-container--default .select2-selection--single {
            background-color: lavender!important;
        }
        input#parent_email
        {
            width: 208px;
        }

        #middle_name-error,#last_name-error,#last_name-error,#family_name-error
        {
            margin-left:253px;
        }

        input[type=checkbox].ace.ace-switch.ace-switch-6:checked+.lbl::after{
            left: 32px;
            background-color: #337ab7;
            border: 4px solid #337ab7;

        }
        input[type=checkbox].ace.ace-switch.ace-switch-6+.lbl::after{
            background: rgb(255, 255, 255);
            border-radius: 16px;
            box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
            content: '';
            height: 24px;
            left: 0px;
            margin-top: -8px;
            position: absolute;
            top: 6px;
            transition: all 0.3s ease-in-out;
            width: 24px;
        }

        input[type=checkbox].ace.ace-switch.ace-switch-6:checked+.lbl::before{
            border-color: #B7D3E5;
            background-color: #1363a9;
        }

        input[type=checkbox].ace.ace-switch.ace-switch-6+.lbl::before{
            background: rgb(0, 0, 0);
            box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
            border-radius: 8px;
            content: '';
            height: 16px;
            margin-top: 3px;
            position: absolute;
            opacity: 0.3;
            transition: all 0.4s ease-in-out;
            width: 40px;
            left: 14px;
        }
        input[type=checkbox].ace.ace-switch.ace-switch-6:checked+.lbl::before{
            content:'';
        }
        input[type=checkbox].ace.ace-switch.ace-switch-6:checked+.lbl::after {
            left: 45px;
            background-color: #337ab7;
            border: 4px solid #337ab7;
        }
        input[type=checkbox].ace.ace-switch.ace-switch-6+.lbl::after{
            border: 4px solid #fff;
        }
        input[type=checkbox].ace.ace-switch {
            width: 71px;
        }

        .panel
        {
            margin-top: -47px;
        }

        #children_number-error
        {
            margin-left: 253px;
            color: #D16E6C;
        }
        .steps>li.active .step, .steps>li.active:before, .steps>li.complete .step, .steps>li.complete:before {
            border-color: #00AAFF;
        }
        #creat_dep{
            background-color: #dddddd!important;
            color:#000!important;
        }
        .blue {
            color: #00AAFF!important;
        }
        .search-bar {
            bottom: -204px!important;
        }
    </style>






    <div class="main">


        @if(isset($employee))
            <div id="set_po_id" current_po_id="{{$employee->employee_id}}" current_po_status=""
                 closed_or_not=""></div>
        @else
            <div id="set_po_id" current_po_id="" current_po_status="draft_po" closed_or_not="0"></div>
    @endif




    <!-- Navbar-content -->
        <nav class="navbar navbar-default navbar2">

            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                    <!--breadcrumbs-->
                    <div class="row">

                        <div class="col-md-10 col-lg-10">

                            <ol class="breadcrumb breadcrumb_nav">
                                <li><a href="/employees">Employee</a></li>
                                <li class="active">
                                    @if(isset($employee))
                                        @if($employee->employee_id != 0)
                                            {{$employee->employee_id}}
                                        @else
                                            Draft
                                        @endif
                                    @endif
                                </li>
                            </ol>
                        </div>
                        @if(isset($employee))
                            <div class="col-md-2 col-lg-2 col-sm-4 col-xs-4">
                                @if(@\App\Employee::where('employee_id','<',$employee->employee_id)->orderBy('employee_id','desc')->first()->employee_id)
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <a href="/employees/{{@\App\Employee::where('employee_id','<',$employee->employee_id)->orderBy('employee_id','desc')->first()->employee_id}}"><span
                                                    class="glyphicon glyphicon-arrow-left"></span>
                                        </a>
                                    </div>
                                @endif
                                @if(@\App\Employee::where('employee_id','>',$employee->employee_id)->orderBy('employee_id','asc')->first()->employee_id)
                                    <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                                        <a href="/employees/{{@\App\Employee::where('employee_id','>',$employee->employee_id)->orderBy('employee_id','asc')->first()->employee_id}}">
                                    <span class="glyphicon glyphicon-arrow-right">
                                </span>
                                        </a>
                                    </div>
                                @endif
                            </div>
                        @endif


                    </div>


                    <!--End breadcrumbs-->
                    <div class="row">
                        <div class="col-md-4 col-lg-4 buttons">
                            @if(isset($employee))
                                <button id="edit_po" type="button" class="btn btn-danger ">Edit</button>
                                <button type="button" class="btn" id="creat_dep">Create</button>
                            @else
                                <button id="save_po" type="button" class="btn btn-danger ">Save</button>
                                {{--<button id="edit_user" type="button" class="btn btn-danger " >Edit</button>--}}
                                <button type="button" class="hide btn" id="creat_dep">Create</button>
                            @endif
                        </div>

                        <div class="col-md-4 col-lg-4">
                            @if(isset($employee))
                                <div class="dropdown">
                                    <button class=" btn btn-primary  dropdown-toggle" type="button"
                                            data-toggle="dropdown" id="more" hidden>More
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">

                                        <li><a href="#">Export</a></li>
                                        <li><a href="#" id="dublicate_dep">Duplicate</a></li>
                                        <li><a href="#" id="print">Print</a></li>

                                    </ul>
                                </div>

                            @else
                                <div class="dropdown">
                                    <button class="hide btn btn-primary  dropdown-toggle" type="button"
                                            data-toggle="dropdown" id="more" hidden>More
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">

                                        <li><a href="#">Export</a></li>
                                        <li><a href="#" id="dublicate_dep">Duplicate</a></li>
                                        <li><a href="#" id="print">Print</a></li>

                                    </ul>
                                </div>
                            @endif

                        </div>


                    </div> <!--collapse -->

                </div><!--collapse -->

            </div>
        </nav>
        <!-- End Navbar-content -->



        <!-- show and hide fronted Error -->

        <div class="regionOfError">
            <!-- Success messages -->
            <div class="alert alert-success alert-autocloseable-success" style="display:none;position:absolute;">
                You are finished!

            </div>
            <!--end Success messages -->
            <!-- failuer messages 1 -->
            <div class="alert alert-danger errorController" style="display:none;" >

            </div>


            <div class="alert alert-danger errorMessage0" style="display:none;position:absolute;" >
                Please Fill Required Fields!
            </div>

            <!-- failuer messages 1 -->
            <div class="alert alert-danger errorMessage1" style="display:none;position:absolute;" >
                Invalid Start Date!
            </div>
            <!--end failuer messages 1-->


            <!-- failuer messages 2 -->
            <div class="alert alert-danger errorMessage2" style="display:none;position:absolute;" >
                Invalid interval Date!
            </div>

            <div class="alert alert-danger errorMessage4" style="display:none;position:absolute;" >
                Please Fill Sallary Scale Table!
            </div>
            <!--end failuer messages 2  -->


            <!-- failuer messages 1 -->
            <div class="alert alert-danger errorMessage3" style="display:none;position:absolute;" >
                Invalid End Date!
            </div>
            <!--end failuer messages 1-->
            <!-- failuer messages 1 -->
            <div class="alert alert-danger errorTableMessage" style="display:none;position:absolute;" >
                Please Finish This Table !
            </div>
            <!--end failuer messages 1-->
        </div>


        <!-- MAIN CONTENT -->
        <div class="main-content">

            <div class="container-fluid">

                <!-- INPUTS -->
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Employee</h3>
                    </div>
                    <div class="panel-body">


                        <div class="page-content">

                            <div class="row">

                                <form id="image_form" enctype="multipart/form-data">
                                    <div class="avatar-upload">
                                        <div class="avatar-edit">
                                            <input class="set_all_disabled" type='file' id="imageUpload" name="profile_image"
                                                   accept=".png, .jpg, .jpeg" @if(isset($employee))disabled @endif />
                                            <label for="imageUpload"></label>
                                        </div>
                                        <div class="avatar-preview">
                                            <div id="imagePreview"
                                                 style="background-image:
                                                 @if(isset($employee))

                                                 @if(file_exists('uploads/employees/'.$employee->employee_id.'_.png'))

                                                         url({{asset('uploads/employees/'.$employee->employee_id.'_.png')}});
                                                 @else
                                                         url({{asset('uploads/employees/default2.png')}});
                                                 @endif
                                                 @else
                                                         url({{asset('uploads/employees/default2.png')}});
                                                 @endif
                                                         ">
                                            </div>
                                        </div>
                                    </div>
                                </form>

                            </div>

                            <br>
                            <div class="row">

                                <div class="col-md-6 col-lg-6">
                                    <label class="col-md-6 col-lg-6  control-label">Related User:</label>

                                    <div class="col-md-6 col-lg-6">
                                        @if(isset($employee))
                                            <input class="set_all_disabled" id="user_name" value="{{$user}}" type="text"
                                                   style="border:0;background-color: #fff;"
                                                   name="user_name" required>
                                        @else
                                            <input class="set_all_disabled" id="user_name" type="text"
                                                   style="border:0;background-color: #fff;"
                                                   name="user_name" required>
                                        @endif
                                    </div>


                                </div>


                            </div>

                            <div class="row">
                                <div class="col-xs-12">
                                    <!-- PAGE CONTENT BEGINS -->


                                    <div class="widget-body">
                                        <div class="widget-main">
                                            <div id="fuelux-wizard-container">
                                                <div>
                                                    <ul class="steps">
                                                        <li data-step="1" class="active">
                                                            <span class="step">1</span>
                                                            <span class="title">Personal Information</span>
                                                        </li>

                                                        <li data-step="2">
                                                            <span class="step">2</span>
                                                            <span class="title">HR Settings</span>
                                                        </li>


                                                        <li data-step="3">
                                                            <span class="step">3</span>
                                                            <span class="title">Sallary Scale</span>
                                                        </li>



                                                        {{--<li data-step="4">--}}
                                                        {{--<span class="step">5</span>--}}
                                                        {{--<span class="title">Finish</span>--}}
                                                        {{--</li>--}}


                                                    </ul>
                                                </div>


                                                <div class="step-content pos-rel">

                                                    <div class="step-pane active " id="" data-step="1">
                                                        <form class="form-horizontal" id="step1_validation-form"
                                                              novalidate="novalidate">

                                                            <div class="center">
                                                                <h3 class="blue lighter">Personal Information</h3>
                                                            </div>
                                                            <br>
                                                            <div class="form_group">
                                                                <div class="row">

                                                                    <div class="col-md-6 col-lg-6">

                                                                        <div class="form-group">
                                                                            <label class="col-md-6 col-lg-6 control-label">First Name:</label>
                                                                            <div class="col-md-6 col-lg-6">
                                                                                @if(isset($employee))
                                                                                    <input old_img_value="{{$employee->employee_name}}"
                                                                                           edit_student_id="{{$employee->employee_name}}"
                                                                                           class="set_all_disabled form-control validate_tab required get_all_selectors   department_error err_input required"
                                                                                           value="{{$employee->employee_name}}"
                                                                                           type="text" name="employee_name"
                                                                                           id="employee_name"
                                                                                           placeholder="employee_name"
                                                                                           @if(isset($employee)) disabled @endif>
                                                                                @else
                                                                                    <input class="form-control set_all_disabled validate_tab required get_all_selectors   department_error err_input"
                                                                                           id="employee_name" type="text"
                                                                                           name="employee_name" required>

                                                                                @endif

                                                                                <br>
                                                                            </div>
                                                                            <br>
                                                                            <label class="col-md-6 col-lg-6 control-label">Middle Name:</label>
                                                                            <div class="col-md-6 col-lg-6">
                                                                                @if(isset($employee))
                                                                                    <input old_img_value="{{$employee->middle_name}}"
                                                                                           edit_student_id="{{$employee->middle_name}}"
                                                                                           class="set_all_disabled form-control validate_tab required get_all_selectors   department_error err_input required"
                                                                                           value="{{$employee->middle_name}}"
                                                                                           type="text" name="middle_name"
                                                                                           id="middle_name"
                                                                                           placeholder="middle_name"
                                                                                           @if(isset($employee)) disabled @endif>
                                                                                @else
                                                                                    <input class="form-control set_all_disabled validate_tab required get_all_selectors   department_error err_input"
                                                                                           id="middle_name" type="text"
                                                                                           name="middle_name" required>
                                                                                @endif
                                                                                <br>
                                                                            </div>
                                                                            <br>
                                                                            <label class="col-md-6 col-lg-6 control-label">Last Name:</label>
                                                                            <div class="col-md-6 col-lg-6">
                                                                                @if(isset($employee))
                                                                                    <input old_img_value="{{$employee->last_name}}"
                                                                                           edit_student_id="{{$employee->last_name}}"
                                                                                           class="set_all_disabled form-control validate_tab required get_all_selectors   department_error err_input required"
                                                                                           value="{{$employee->last_name}}"
                                                                                           type="text" name="last_name"
                                                                                           id="last_name"
                                                                                           placeholder="last_name"
                                                                                           @if(isset($employee)) disabled @endif>
                                                                                @else
                                                                                    <input class="form-control set_all_disabled validate_tab required get_all_selectors   department_error err_input"
                                                                                           id="last_name" type="text"
                                                                                           name="last_name" required>
                                                                                @endif
                                                                                <br>
                                                                            </div>



                                                                            <br>
                                                                            <label class="col-md-6 col-lg-6 control-label">Family Name:</label>
                                                                            <div class="col-md-6 col-lg-6">
                                                                                @if(isset($employee))
                                                                                    <input old_img_value="{{$employee->family_name}}"
                                                                                           edit_student_id="{{$employee->family_name}}"
                                                                                           class="set_all_disabled form-control validate_tab required get_all_selectors   department_error err_input required"
                                                                                           value="{{$employee->family_name}}"
                                                                                           type="text" name="family_name"
                                                                                           id="family_name"
                                                                                           placeholder="family_name"
                                                                                           @if(isset($employee)) disabled @endif>
                                                                                @else
                                                                                    <input class="form-control set_all_disabled validate_tab required get_all_selectors   department_error err_input"
                                                                                           id="family_name" type="text"
                                                                                           name="family_name" required>
                                                                                @endif
                                                                                <br>

                                                                                <br>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>


                                                            <br>


                                                            <div class="row">

                                                                <div class="col-md-6 col-lg-6">


                                                                    <div class="form-group">

                                                                        <label class="col-md-6 col-lg-6 control-label"
                                                                               for="Gender">Gender</label>

                                                                        <div class="col-md-6 col-lg-6">
                                                                            @if(isset($employee))
                                                                                <label class="radio-inline"
                                                                                       for="Gender-0">
                                                                                    <input @if ($employee->gender == "male")checked
                                                                                           @endif class="set_all_disabled"
                                                                                           name="gender" id="gender"
                                                                                           value="male" type="radio"
                                                                                           @if(isset($employee)) disabled @endif>
                                                                                    Male
                                                                                </label>

                                                                                <label class="radio-inline"
                                                                                       for="Gender-1">
                                                                                    <input @if ($employee->gender == "female")checked
                                                                                           @endif class="set_all_disabled"
                                                                                           name="gender" id="gender"
                                                                                           value="female" type="radio"
                                                                                           @if(isset($employee)) disabled @endif>
                                                                                    Female
                                                                                </label>
                                                                            @else
                                                                                <label class="radio-inline"
                                                                                       for="Gender-0">
                                                                                    <input class="set_all_disabled"
                                                                                           name="gender" id="gender"
                                                                                           value="male" type="radio" checked
                                                                                           @if(isset($employee)) disabled @endif>
                                                                                    Male
                                                                                </label>

                                                                                <label class="radio-inline"
                                                                                       for="Gender-1">
                                                                                    <input class="set_all_disabled"
                                                                                           name="gender" id="gender"
                                                                                           value="female" type="radio"
                                                                                           @if(isset($employee)) disabled @endif>
                                                                                    Female
                                                                                </label>
                                                                            @endif
                                                                        </div>

                                                                    </div>

                                                                </div>

                                                            </div>
                                                            <br>

                                                            <div class="row">

                                                                <div class="col-md-6 col-lg-6">

                                                                    <div class="form-group">
                                                                        <label class="col-md-6 col-lg-6 control-label ">Nationality</label>
                                                                        <div class="col-md-6 col-lg-6">

                                                                            <select class="form-control select2 set_all_disabled required"
                                                                                    id="nation" name="nation"
                                                                                    class="validate_tab required get_all_selectors set_all_disabled select_2_enable form-control department_error err_input"
                                                                                    @if(isset($employee)) disabled @endif >
                                                                                <option value=""></option>
                                                                                @foreach($nation as $id=>$role)
                                                                                    @if(isset($employee))
                                                                                        @if($employee->nationality_id == $id)
                                                                                            <option selected
                                                                                                    value="{{$id}}">{{$role}}</option>
                                                                                        @else
                                                                                            <option value="{{$id}}">{{$role}}</option>
                                                                                        @endif
                                                                                    @else
                                                                                        <option value="{{$id}}">{{$role}}</option>
                                                                                    @endif
                                                                                @endforeach
                                                                            </select>
                                                                        </div>


                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <br>
                                                            <div class="row">

                                                                <div class="col-md-6 col-lg-6">


                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-6 col-lg-6"
                                                                               for="date1">Date of Birth</label>

                                                                        <div class=" col-md-6 col-lg-6">
                                                                            <div class="form-group">


                                                                                <div class="input-group ">
                                                                                    @if(isset($employee))
                                                                                        <input value="{{$employee->date_of_birth}}"
                                                                                               type="text" id="date_birth"
                                                                                               class="form-control set_all_disabled datae_pic required "
                                                                                               name="dateofbirth"
                                                                                               @if(isset($employee)) disabled @endif>
                                                                                    @else
                                                                                        <input type="text" id="date_birth"
                                                                                               class="form-control set_all_disabled required "
                                                                                               name="dateofbirth"
                                                                                               @if(isset($employee)) disabled @endif>
                                                                                    @endif
                                                                                    <div class="input-group-addon">
                                                                                        <i class="fa fa-calendar">
                                                                                        </i>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                        </div>

                                                                    </div>


                                                                </div>

                                                            </div>
                                                            <br>
                                                            <div class="row">

                                                                <div class="col-md-6 col-lg-6">
                                                                    <div class="form-group">

                                                                        <label class="col-md-6 col-lg-6  control-label">Work
                                                                            Mobile</label>
                                                                        <div class="col-md-6 col-lg-6">
                                                                            @if(isset($employee))
                                                                                <input value="{{$employee->mobile_work}}" id="phone"
                                                                                       class="form-control set_all_disabled "
                                                                                       type="tel"
                                                                                       class="col-md-4 "
                                                                                       name="phone"

                                                                                       @if(isset($employee)) disabled @endif>
                                                                            @else
                                                                                <input id="phone"
                                                                                       class="form-control set_all_disabled"
                                                                                       type="tel"
                                                                                       type="text" class="col-md-4"
                                                                                       name="phone"

                                                                                       @if(isset($employee)) disabled @endif>
                                                                            @endif

                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <br>
                                                            <div class="row">
                                                                <div class="col-md-6 col-lg-6">
                                                                    <div class="form-group">
                                                                        <label class="col-md-6 col-lg-6 control-label no-padding-right" for="form-field-email">Email</label>

                                                                        <div class="col-md-6 col-lg-6">
                                                                            	<span class="input-icon input-icon-right">
                                                                            @if(isset($employee))
                                                                                        <input value="{{$employee->email}}" id="parent_email"
                                                                                               class="form-control set_all_disabled required"
                                                                                               type="email"
                                                                                               class="col-md-4 "
                                                                                               name="parent_email"

                                                                                               @if(isset($employee)) disabled @endif>
                                                                                    @else
                                                                                        <input id="parent_email"
                                                                                               class="form-control set_all_disabled required"
                                                                                               type="email"
                                                                                               class="col-md-4"
                                                                                               name="parent_email"

                                                                                               @if(isset($employee)) disabled @endif>
                                                                                    @endif
                                                                                    <i class="ace-icon fa fa-envelope"></i>
</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <br>
                                                            <div class="row">

                                                                <div class="col-md-6 col-lg-6">
                                                                    <div class="form-group">

                                                                        <label class="col-md-6 col-lg-6 control-label ">Office
                                                                            Location</label>
                                                                        <div class="col-md-6 col-lg-6">

                                                                            <select class="form-control select2 set_all_disabled "
                                                                                    name="office_loc" id="office_loc"
                                                                                    @if(isset($employee)) disabled @endif>
                                                                                @if(isset($employee))
                                                                                    <option selected
                                                                                            value="{{$employee->office_location}}">{{$employee->office_location}}</option>
                                                                                    <option value="floor1">Floor1
                                                                                    </option>
                                                                                    <option value="floor2"> Floor2
                                                                                    </option>
                                                                                    <option value="floor3"> Floor3
                                                                                    </option>
                                                                                @else
                                                                                    <option value=""></option>
                                                                                    <option value="floor1">Floor1
                                                                                    </option>
                                                                                    <option value="floor2"> Floor2
                                                                                    </option>
                                                                                    <option value="floor3"> Floor3
                                                                                    </option>
                                                                                @endif
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <br>

                                                            <div class="row">

                                                                <div class="col-md-6 col-lg-6">
                                                                    <div class="form-group">

                                                                        <label class="col-md-6 col-lg-6 control-label ">Country</label>
                                                                        <div class="col-md-6 col-lg-6">

                                                                            <select class="form-control select2 set_all_disabled required"
                                                                                    id="country" name="country"
                                                                                    class="validate_tab required get_all_selectors set_all_disabled select_2_enable form-control department_error err_input"
                                                                                    @if(isset($employee)) disabled @endif >
                                                                                <option value=""></option>
                                                                                @foreach($country as $id=>$role)
                                                                                    @if(isset($employee))
                                                                                        @if($employee->country_id == $id)
                                                                                            <option selected value="{{$id}}">{{$role}}</option>
                                                                                        @else
                                                                                            <option value="{{$id}}">{{$role}}</option>
                                                                                        @endif
                                                                                    @else
                                                                                        <option value="{{$id}}">{{$role}}</option>
                                                                                    @endif
                                                                                @endforeach
                                                                            </select>





                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="row">

                                                                <div class="col-md-6 col-lg-6">
                                                                    <div class="form-group ">
                                                                        <label class="col-md-6 col-lg-6 control-label col-xs-12"
                                                                               for="Permanent Address">Home
                                                                            Address</label>
                                                                        <div class="col-md-6 col-lg-6">
                                                                            @if(isset($employee))
                                                                                <input id="address" value="{{$employee->home_address}}"
                                                                                       class="form-control set_all_disabled "
                                                                                       type="text" class="col-md-4 "
                                                                                       name="address"

                                                                                       @if(isset($employee)) disabled @endif>
                                                                            @else
                                                                                <input id="address"
                                                                                       class="form-control set_all_disabled "
                                                                                       type="text" class="col-md-4 "
                                                                                       name="address"

                                                                                       @if(isset($employee)) disabled @endif>
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <br>
                                                        </form>
                                                    </div>

                                                    <div class="step-pane" id="" data-step="2">
                                                        <form class="form-horizontal" id="step2_validation-form"
                                                              novalidate="novalidate">
                                                            <div class="center">
                                                                <h3 class="blue lighter">HR Settings</h3>
                                                            </div>
                                                            <br>
                                                            <div class="row">

                                                                <div class="col-md-6 col-lg-6">
                                                                    <div class=" form-group">
                                                                        <label class="col-md-6 col-lg-6 control-label"
                                                                               for="radios">Contract Type</label>
                                                                        <div class="col-md-6 col-lg-6">
                                                                            @if(isset($employee))
                                                                                <label class="radio-inline set_all_disabled"
                                                                                       for="Foreigner-0">
                                                                                    <input @if ($employee->user_type == "local")checked
                                                                                           @endif class="set_all_disabled"
                                                                                           type="radio" name="foreigner"
                                                                                           value="local"
                                                                                           id="Foreigner"
                                                                                           @if(isset($employee)) disabled @endif>
                                                                                    Local
                                                                                </label>
                                                                                <label class="radio-inline set_all_disabled"
                                                                                       for="Foreigner-1">
                                                                                    <input @if ($employee->user_type == "foreign")checked
                                                                                           @endif class="set_all_disabled"
                                                                                           type="radio" name="foreigner"
                                                                                           value="foreign"
                                                                                           id="Foreigner"
                                                                                           @if(isset($employee)) disabled @endif>
                                                                                    Foreign

                                                                                </label>
                                                                            @else
                                                                                <label class="radio-inline set_all_disabled"
                                                                                       for="Foreigner-0">
                                                                                    <input class="set_all_disabled"
                                                                                           type="radio" name="foreigner"
                                                                                           value="local"
                                                                                           id="Foreigner" checked
                                                                                           @if(isset($employee)) disabled @endif>
                                                                                    Local
                                                                                </label>
                                                                                <label class="radio-inline set_all_disabled"
                                                                                       for="Foreigner-1">
                                                                                    <input class="set_all_disabled"
                                                                                           type="radio" name="foreigner"
                                                                                           value="foreign"
                                                                                           id="Foreigner"
                                                                                           @if(isset($employee)) disabled @endif>
                                                                                    Foreign

                                                                                </label>
                                                                            @endif
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <br>

                                                            <div class="row">
                                                                <div class="col-md-6 col-lg-6 ">
                                                                    <div class="form-group">
                                                                        <label class="col-md-6 col-lg-6 control-label date"
                                                                               for="date2">Date of contract</label>
                                                                        <div class="col-md-6 col-lg-6">
                                                                            <div class="input-group">
                                                                                @if(isset($employee))
                                                                                    <input value="{{$employee->date_of_contract}}"
                                                                                           type="text"
                                                                                           class="form-control set_all_disabled required"
                                                                                           id="date2"
                                                                                           name="dateofcontract"
                                                                                           @if(isset($employee)) disabled @endif>
                                                                                @else
                                                                                    <input type="text"
                                                                                           class="form-control set_all_disabled required"
                                                                                           id="date2"
                                                                                           name="dateofcontract"
                                                                                           @if(isset($employee)) disabled @endif>
                                                                                @endif
                                                                                <div class="input-group-addon">
                                                                                    <i class="fa fa-calendar">
                                                                                    </i>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <br>

                                                            <div class="row">
                                                                <div class="col-md-6 col-lg-6 ">

                                                                    <label class="col-md-6 col-lg-6 control-label"
                                                                           for="radios">Contract Years Number</label>
                                                                    <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                                                                        @if(is                                                                                   onkeypress="return (event.charCode == 8 || event.charCode == 1) ? null : event.charCode >= 48 && event.charCode <= 57 ||  event.charCode == 46" data-role="input"
set($employee))
                                                                            <input value="{{$employee->contract_years}}"
                                                                                   id="contract_years"
                                                                                   class="row_data form-control set_all_disabled required"
                                                                                   type="number"
                                                                                   name="contract_years"
                                                                                   @if(isset($employee)) disabled @endif>
                                                                        @else
                                                                            <input id="contract_years"
                                                                                   class="row_data form-control set_all_disabled required"
                                                                                   type="number" value="1"
                                                                                   onkeypress="return (event.charCode == 8 || event.charCode == 1) ? null : event.charCode >= 48 && event.charCode <= 57 ||  event.charCode == 46" data-role="input"

                                                                                   name="contract_years"
                                                                                   @if(isset($employee)) disabled @endif>
                                                                        @endif
                                                                    </div>

                                                                </div>


                                                            </div>
                                                            <br>

                                                            <div class="row">

                                                                <div class="col-md-6 col-lg-6">

                                                                    <div class="form-group">
                                                                        <label class="col-md-6 col-lg-6 control-label set_all_disabled"
                                                                               for="radios">Marital Status</label>
                                                                        <div class="col-md-6 col-lg-6 ">
                                                                            @if(isset($employee))
                                                                                <label class="radio-inline set_all_disabled"
                                                                                       for="status-0">
                                                                                    <input @if ($employee->marital_status == "single")checked
                                                                                           @endif  class="set_all_disabled"
                                                                                           type="radio" name="status"
                                                                                           id="status-0" value="single"
                                                                                           @if(isset($employee)) disabled @endif>
                                                                                    single
                                                                                </label>
                                                                                <label class="radio-inline"
                                                                                       for="status-1">
                                                                                    <input @if ($employee->marital_status == "married")checked
                                                                                           @endif class="set_all_disabled"
                                                                                           type="radio" name="status"
                                                                                           id="status-1" value="married"
                                                                                           @if(isset($employee)) disabled @endif>
                                                                                    married
                                                                                </label>

                                                                            @else
                                                                                <label class="radio-inline set_all_disabled"
                                                                                       for="status-0">
                                                                                    <input class="set_all_disabled"
                                                                                           type="radio" name="status"
                                                                                           id="status-0" value="single" checked
                                                                                           @if(isset($employee)) disabled @endif>
                                                                                    single
                                                                                </label>
                                                                                <label class="radio-inline"
                                                                                       for="status-1">
                                                                                    <input class="set_all_disabled"
                                                                                           type="radio" name="status"
                                                                                           id="status-1" value="married"
                                                                                           @if(isset($employee)) disabled @endif>
                                                                                    married
                                                                                </label>

                                                                            @endif
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <br>


                                                            <div class="row  with_family ">
                                                                <div class="col-md-6 col-lg-6">
                                                                    <div class="partner form-group set_all_disabled">
                                                                        <label class="col-md-6 col-lg-6 control-label"
                                                                               for="radios">With Family</label>
                                                                        <div class="col-md-6 col-lg-6">

                                                                            <div class="material-switch set_all_disabled">
                                                                                @if(isset($employee))
                                                                                    <input id="family"  value="{{$employee->family}}" class="include_check set_all_disabled" name="someSwitchOption001" type="checkbox" disabled   @if( $employee->family == "yes") checked  @endif>
                                                                                @else
                                                                                    <input id="family"  value="no" class="include_check set_all_disabled" name="someSwitchOption001" type="checkbox">
                                                                                @endif

                                                                                <label for="family" class="label-primary"></label>
                                                                            </div>

                                                                        </div>


                                                                    </div>

                                                                </div>

                                                            </div>

                                                            <br>
                                                            <div class="row   partner_info">

                                                                <div class="col-md-6 col-lg-6">

                                                                    <div class="form-group">
                                                                        <label class="col-md-6 col-lg-6 control-label">Partner First Name:</label>
                                                                        <div class="col-md-6 col-lg-6">
                                                                            @if(isset($employee))
                                                                                <input
                                                                                        class="set_all_disabled form-control validate_tab required get_all_selectors   department_error err_input required"
                                                                                        value="{{$employee->partner_first_name}}"
                                                                                        type="text" name="partner_first_name"
                                                                                        id="partner_first_name"
                                                                                        placeholder="partner_first_name"
                                                                                        @if(isset($employee)) disabled @endif>
                                                                            @else
                                                                                <input class="form-control set_all_disabled validate_tab required get_all_selectors   department_error err_input"
                                                                                       id="partner_first_name" type="text"
                                                                                       name="partner_first_name" >
                                                                            @endif
                                                                            <br>
                                                                        </div>
                                                                        <br>
                                                                        <label class="col-md-6 col-lg-6 control-label">Partner Middle Name:</label>
                                                                        <div class="col-md-6 col-lg-6">
                                                                            @if(isset($employee))
                                                                                <input old_img_value="{{$employee->partner_middle_name}}"
                                                                                       edit_student_id="{{$employee->partner_middle_name}}"
                                                                                       class="set_all_disabled form-control validate_tab required get_all_selectors   department_error err_input required"
                                                                                       value="{{$employee->partner_middle_name}}"
                                                                                       type="text" name="partner_middle_name"
                                                                                       id="partner_middle_name"
                                                                                       placeholder="partner_middle_name"
                                                                                       @if(isset($employee)) disabled @endif>
                                                                            @else
                                                                                <input class="form-control set_all_disabled validate_tab required get_all_selectors   department_error err_input"
                                                                                       id="partner_middle_name" type="text"
                                                                                       name="partner_middle_name" >
                                                                            @endif
                                                                            <br>
                                                                        </div>
                                                                        <br>
                                                                        <label class="col-md-6 col-lg-6 control-label">Partner Last Name:</label>
                                                                        <div class="col-md-6 col-lg-6">
                                                                            @if(isset($employee))
                                                                                <input old_img_value="{{$employee->partner_last_name}}"
                                                                                       edit_student_id="{{$employee->partner_last_name}}"
                                                                                       class="set_all_disabled form-control validate_tab required get_all_selectors   department_error err_input required"
                                                                                       value="{{$employee->partner_last_name}}"
                                                                                       type="text" name="partner_last_name"
                                                                                       id="partner_last_name"
                                                                                       placeholder="partner_last_name"
                                                                                       @if(isset($employee)) disabled @endif>
                                                                            @else
                                                                                <input class="form-control set_all_disabled validate_tab required get_all_selectors   department_error err_input"
                                                                                       id="partner_last_name" type="text"
                                                                                       name="partner_last_name" >
                                                                            @endif
                                                                            <br>
                                                                        </div>

                                                                        <br>
                                                                        <label class="col-md-6 col-lg-6 control-label">Partner Family Name:</label>
                                                                        <div class="col-md-6 col-lg-6">
                                                                            @if(isset($employee))
                                                                                <input old_img_value="{{$employee->partner_family_name}}"
                                                                                       edit_student_id="{{$employee->partner_family_name}}"
                                                                                       class="set_all_disabled form-control validate_tab required get_all_selectors   department_error err_input required"
                                                                                       value="{{$employee->partner_family_name}}"
                                                                                       type="text" name="partner_family_name"
                                                                                       id="partner_family_name"
                                                                                       placeholder="partner_family_name"
                                                                                       @if(isset($employee)) disabled @endif>
                                                                            @else
                                                                                <input class="form-control set_all_disabled validate_tab required get_all_selectors   department_error err_input"
                                                                                       id="partner_family_name" type="text"
                                                                                       name="partner_family_name" >
                                                                            @endif
                                                                        </div>
                                                                        <br>

                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <br>
                                                            <div class="row children partner_info">
                                                                <div class="col-md-6 col-lg-6  ">
                                                                    <label class="col-md-6 col-lg-6 control-label"
                                                                           for="radios">Number of Children</label>
                                                                    <div class="col-md-6 col-lg-6">
                                                                        @if(isset($employee))
                                                                            <input value="{{$employee->children_number}}"
                                                                                   class="set_all_disabled form-control validate_tab required get_all_selectors   department_error err_input required"
                                                                                   onkeypress="return (event.charCode == 8 || event.charCode == 1) ? null : event.charCode >= 48 && event.charCode <= 57 ||  event.charCode == 46" data-role="input"

                                                                                   value="{{$employee->children_number}}"
                                                                                   type="number" name="children_number"
                                                                                   id="children_number"
                                                                                   placeholder="children_number"
                                                                                   @if(isset($employee)) disabled @endif>
                                                                        @else
                                                                            <input class="form-control set_all_disabled validate_tab required get_all_selectors   department_error err_input required"
                                                                                   id="children_number" type="number"
                                                                                   onkeypress="return (event.charCode == 8 || event.charCode == 1) ? null : event.charCode >= 48 && event.charCode <= 57 ||  event.charCode == 46" data-role="input"

                                                                                   name="children_number" >
                                                                        @endif
                                                                        <br>
                                                                    </div>

                                                                </div>
                                                            </div>


                                                            <br>
                                                            <div class="row">
                                                                <div class="col-md-6 col-lg-6 ">

                                                                    <label class="col-md-6 col-lg-6 control-label"
                                                                           for="radios">Percentage Salary</label>
                                                                    <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                                                                        @if(isset($employee))
                                                                            <input value="{{$employee->percentage_salary}}"
                                                                                   id="number"
                                                                                   class="row_data form-control set_all_disabled required"
                                                                                   type="number"
                                                                                   name="percentage_Salary"
                                                                                   @if(isset($employee)) disabled @endif>
                                                                        @else
                                                                            <input id="number"
                                                                                   class="row_data form-control set_all_disabled required"
                                                                                   type="number" value="100"
                                                                                   name="percentage_Salary"
                                                                                   @if(isset($employee)) disabled @endif>
                                                                        @endif
                                                                    </div>

                                                                </div>
                                                                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-2"
                                                                     style="line-height: 37px;"> %
                                                                </div>

                                                            </div>
                                                            <br>
                                                            <div class="row">

                                                                <div class="col-md-6 col-lg-6 ">

                                                                    <label class="col-md-6 col-lg-6 control-label"
                                                                           for="radios">Academic Year</label>
                                                                    <div class="col-md-6 col-lg-6">
                                                                        <select class="form-control required select2 set_all_disabled"
                                                                                id="academic_year2"
                                                                                name="academic_year2"
                                                                                class="form-control select2 set_all_disabled required"
                                                                                @if(isset($employee)) disabled @endif >
                                                                            <option value="" class="required"></option>
                                                                            @foreach($academic_year as $id=>$role)
                                                                                @if(isset($employee))
                                                                                    @if($employee->acadimic_year_id == $id)
                                                                                        <option class="required" selected
                                                                                                value="{{$id}}">{{$role}}</option>
                                                                                    @else
                                                                                        <option class="required" value="{{$id}}">{{$role}}</option>
                                                                                    @endif
                                                                                @else
                                                                                    <option class="required" value="{{$id}}">{{$role}}</option>
                                                                                @endif
                                                                            @endforeach
                                                                        </select>

                                                                    </div>
                                                                </div>


                                                            </div>
                                                            <br>
                                                            <div class="row">
                                                                <div class="col-md-6 col-lg-6 ">
                                                                    <div class="form-group">
                                                                        <label class="col-md-6 col-lg-6 control-label">Department:</label>
                                                                        <div class="col-md-6 col-lg-6">
                                                                            <select class="form-control select2 required set_all_disabled"
                                                                                    id="depart" name="depart"
                                                                                    class="form-control select2 set_all_disabled required" @if(isset($employee)) disabled @endif>
                                                                                <option value=""></option>
                                                                                @foreach($depart as $id=>$role)
                                                                                    @if(isset($employee))
                                                                                        @if($employee->depart_id == $id)
                                                                                            <option selected
                                                                                                    value="{{$id}}">{{$role}}</option>
                                                                                        @else
                                                                                            <option value="{{$id}}">{{$role}}</option>
                                                                                        @endif
                                                                                    @else
                                                                                        <option value="{{$id}}">{{$role}}</option>
                                                                                    @endif
                                                                                @endforeach
                                                                            </select>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <br>
                                                            <div class="row">
                                                                <div class="col-md-6 col-lg-6 ">
                                                                    <div class="form-group">
                                                                        <label class="col-md-6 col-lg-6 control-label">Jop
                                                                            Title</label>
                                                                        <div class="col-md-6 col-lg-6">
                                                                            <select class="form-control required select2 set_all_disabled"
                                                                                    id="job" name="job"
                                                                                    class="form-control select2 set_all_disabled required"
                                                                                    @if(isset($employee)) disabled @endif >
                                                                                @if(isset($employee))
                                                                                    @if(isset($employee))
                                                                                        <option selected
                                                                                                value="{{$employee->job_id}}">{{@\App\Job::find($employee->job_id)->job_name}}</option>
                                                                                    @else
                                                                                        <option></option>
                                                                                    @endif
                                                                                @else
                                                                                    <option></option>
                                                                                @endif

                                                                            </select>


                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <br>
                                                            <div class="row">
                                                                <div class="col-md-6 col-lg-6 ">
                                                                    <div class="form-group">
                                                                        <label class="col-md-6 col-lg-6 control-label">Manager</label>
                                                                        <div class="col-md-6 col-lg-6">
                                                                            <select class="form-control select2 set_all_disabled required"
                                                                                    name="manager" id="manager"
                                                                                    @if(isset($employee)) disabled @endif>
                                                                                @if(isset($employee))
                                                                                    @if(isset($employee))
                                                                                        <option selected
                                                                                                value="{{$employee->manager_id}}">{{@\App\Employee::find($employee->manager_id)->employee_name}}</option>
                                                                                    @else
                                                                                        <option></option>
                                                                                    @endif
                                                                                @else
                                                                                    <option></option>
                                                                                @endif
                                                                            </select>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <br>

                                                            <div class="row" @if(!isset($employee)) style="visibility: hidden" @endif>
                                                                <div class="col-md-6 col-lg-6 ">
                                                                    <div class="form-group">

                                                                        <label class="col-md-6 col-lg-6 control-label"
                                                                               for="leave">leave</label>
                                                                        @if(isset($employee))
                                                                            <div class="col-md-2 col-lg-2">

                                                                                <label class="radio-inline" for="yes">
                                                                                    <input @if ($employee->leave == "yes")checked
                                                                                           @endif class="set_all_disabled"
                                                                                           name="leave" id="yes7"
                                                                                           value="yes"
                                                                                           type="radio"
                                                                                           @if(isset($employee)) disabled @endif>
                                                                                    Yes
                                                                                </label>

                                                                            </div>

                                                                            <div class="col-md-2 col-lg-2">

                                                                                <label class="radio-inline" for="no">
                                                                                    <input @if ($employee->leave == "no")checked
                                                                                           @endif class="set_all_disabled"
                                                                                           name="leave" id="no7"
                                                                                           value="no"
                                                                                           type="radio"
                                                                                           @if(isset($employee)) disabled @endif>
                                                                                    No
                                                                                </label>


                                                                            </div>
                                                                        @else
                                                                            <div class="col-md-2 col-lg-2">

                                                                                <label class="radio-inline" for="yes">
                                                                                    <input class="set_all_disabled"
                                                                                           name="leave" id="yes7"
                                                                                           value="yes"
                                                                                           type="radio"
                                                                                           @if(isset($employee)) disabled @endif>
                                                                                    Yes
                                                                                </label>

                                                                            </div>

                                                                            <div class="col-md-2 col-lg-2">

                                                                                <label class="radio-inline" for="no">
                                                                                    <input class="set_all_disabled"
                                                                                           name="leave" id="no7"
                                                                                           value="no"
                                                                                           type="radio" checked
                                                                                           @if(isset($employee)) disabled @endif>
                                                                                    No
                                                                                </label>

                                                                            </div>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <br>

                                                            <div class="row">
                                                                <div class="set_all_disabled col-md-6 col-lg-6 @if(isset($employee))  @if($employee->leave == 'no') hide @endif  @else hide @endif   leaving ">


                                                                    <div class="form-group">
                                                                        <label class="col-md-6 control-label date"
                                                                               for="date3">Date of Leaving:</label>


                                                                        <div class=" col-md-6 col-lg-6">
                                                                            <div class="form-group">
                                                                                <div class="input-group">
                                                                                    @if(isset($employee) && $employee->leave == "yes")
                                                                                        <input type="text" value="{{$employee->date_of_leave}}"
                                                                                               class="form-control set_all_disabled"
                                                                                               id="date3"
                                                                                               name="dateofleave" disabled>
                                                                                    @else
                                                                                        <input type="text"
                                                                                               class="form-control set_all_disabled"
                                                                                               id="date3"
                                                                                               name="dateofleave">
                                                                                    @endif
                                                                                    <div class="input-group-addon">
                                                                                        <i class="fa fa-calendar">
                                                                                        </i>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </form>
                                                    </div>



                                                    <div class="step-pane" id="" data-step="3">
                                                        <form class="form-horizontal" id="step4_validation-form"
                                                              novalidate="novalidate">

                                                            <div class="center">
                                                                <h3 class="blue lighter">Sallary Scale</h3>
                                                            </div>
                                                            <br>
                                                            <table class="table table-bordred table-striped table-bordered table-striped table-hover table-condensed table-responsive "
                                                                   id="salary_table">
                                                                <thead>
                                                                <th>Salary Scale</th>
                                                                <th>Academic Year</th>
                                                                <th>Start From</th>
                                                                <th>Edit</th>
                                                                <th>Delete</th>

                                                                </thead>
                                                                <tbody>

                                                                @if(isset($employee))
                                                                    @foreach($employee_salary as $salary)
                                                                        <tr class="">
                                                                            <td td_type="search" data-select2-id="26" class="sub_name_v" current_value="{{@\App\SubSalary::find($salary->sub_salary_id)->sub_id}}">{{@\App\SubSalary::find($salary->sub_salary_id)->sub_name}}</td>
                                                                            <td td_type="search" data-select2-id="47" class="academic_year2_v" current_value="{{@\App\AcademicYear::find($salary->academic_year_id)->acadimic_id}}">{{@\App\AcademicYear::find($salary->academic_year_id)->year_name}}</td>
                                                                            <td td_type="date" class="start_in_v" current_value="{{$salary->start_in}}">{{$salary->start_in}}</td>
                                                                            <td><button  type="button" class="set_all_disabled btn btn-primary btn-xs edit_row" disabled>Edit</button></td>
                                                                            <td><button  type="button" class="set_all_disabled btn btn-danger btn-xs delete_row" disabled><span class="lnr lnr-trash"></span></button></td>
                                                                        </tr>

                                                                    @endforeach
                                                                    {{--@else--}}
                                                                    {{--<tr class="added_now">--}}
                                                                    {{--<td td_type="search">--}}
                                                                    {{--<select class="sub_name form-control js-example-disabled-results myselect4 required" required>--}}
                                                                    {{--<option value=""></option>--}}
                                                                    {{--@foreach($sub_salary as $id=>$role)--}}
                                                                    {{--<option value="{{$id}}">{{$role}}--}}
                                                                    {{--</option>--}}
                                                                    {{--@endforeach--}}
                                                                    {{--</select></td>--}}
                                                                    {{--<td td_type="search">--}}
                                                                    {{--<select class="academic_year2 form-control js-example-disabled-results myselect5 required" required>--}}
                                                                    {{--<option value=""></option>--}}
                                                                    {{--@foreach($academic_year_table as $acadimic)--}}
                                                                    {{--<option from="{{$acadimic->start}}"--}}
                                                                    {{--to="{{$acadimic->end}}"--}}
                                                                    {{--value="{{$acadimic->acadimic_id}}">{{$acadimic->year_name}}--}}
                                                                    {{--</option>--}}
                                                                    {{--@endforeach--}}
                                                                    {{--</select></td>--}}

                                                                    {{--<td td_type="date">--}}
                                                                    {{--<div class="input-group required">--}}
                                                                    {{--<input type="text"--}}
                                                                    {{--class="form-control  start_in required alldate" required>--}}
                                                                    {{--<div class="input-group-addon">--}}
                                                                    {{--<i class="fa fa-calendar">--}}
                                                                    {{--</i>--}}
                                                                    {{--</div>--}}
                                                                    {{--</div>--}}
                                                                    {{--</td>--}}

                                                                    {{--<td>--}}
                                                                    {{--<button class="set_all_disabled btn btn-primary btn-xs Save_row" type="button">--}}
                                                                    {{--Save--}}
                                                                    {{--</button>--}}
                                                                    {{--</td>--}}
                                                                    {{--<td>--}}
                                                                    {{--<button type="button" class="set_all_disabled btn btn-danger btn-xs delete_row">--}}
                                                                    {{--<span class="lnr lnr-trash"></span>--}}
                                                                    {{--</button>--}}
                                                                    {{--</td>--}}

                                                                    {{--</tr>--}}

                                                                @endif

                                                                </tbody>
                                                            </table>
                                                            <div class="row">
                                                                <div class="col-md-4 col-lg-4 ">
                                                                    <button type="button" class="btn btn-primary add_row set_all_disabled"
                                                                            id="add_row" @if(isset($employee)) disabled @endif>Add Row
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>

                                                    {{--<div class="step-pane" id="" data-step="4">--}}
                                                    {{--<form class="form-horizontal" id="step4_validation-form"--}}
                                                    {{--novalidate="novalidate">--}}

                                                    {{--<div class="center">--}}
                                                    {{--<h3 class="blue lighter">Congratulation !  Please Save Your Data.  </h3>--}}
                                                    {{--</div>--}}
                                                    {{--</form>--}}
                                                    {{--</div>--}}

                                                </div>
                                            </div>

                                            <div class="wizard-actions">
                                                <button class="btn btn-prev" id="priv">
                                                    <i class="ace-icon fa fa-arrow-left"></i>
                                                    Prev
                                                </button>

                                                <button class="btn btn-success btn-next" id="finish" data-last="Finish">
                                                    Next
                                                    <i class="ace-icon fa fa-arrow-right icon-on-right"></i>
                                                </button>
                                            </div>
                                        </div><!-- /.widget-main -->
                                    </div><!-- /.widget-body -->


                                    <div id="modal-wizard" class="modal">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div id="modal-wizard-container">
                                                    <div class="modal-header">
                                                        <ul class="steps">
                                                            <li data-step="1" class="active">
                                                                <span class="step">1</span>
                                                                <span class="title">Validation states</span>
                                                            </li>

                                                            <li data-step="2">
                                                                <span class="step">2</span>
                                                                <span class="title">Alerts</span>
                                                            </li>


                                                            <li data-step="3">
                                                                <span class="step">3</span>
                                                                <span class="title">Payment Info</span>
                                                            </li>
                                                        </ul>
                                                    </div>

                                                    <div class="modal-body step-content">
                                                        <div class="step-pane active" data-step="1">
                                                            <div class="center">
                                                                <h4 class="blue">Step 1</h4>
                                                            </div>
                                                        </div>

                                                        <div class="step-pane" data-step="2">
                                                            <div class="center">
                                                                <h4 class="blue">Step 2</h4>
                                                            </div>
                                                        </div>



                                                        <div class="step-pane" data-step="3">
                                                            <div class="center">
                                                                <h4 class="blue">Step 3</h4>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="modal-footer wizard-actions">
                                                    <button class="btn btn-sm btn-prev">
                                                        <i class="ace-icon fa fa-arrow-left"></i>
                                                        Prev
                                                    </button>

                                                    <button class="btn btn-success btn-sm btn-next" data-last="Finish">
                                                        Next
                                                        <i class="ace-icon fa fa-arrow-right icon-on-right"></i>
                                                    </button>

                                                    <button class="btn btn-danger btn-sm pull-left"
                                                            data-dismiss="modal">
                                                        <i class="ace-icon fa fa-times"></i>
                                                        Cancel
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- PAGE CONTENT ENDS -->
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div><!-- /.page-content -->


                    </div>
                    <br>
                </div>
            </div>

        </div>
        <!-- END INPUTS -->


    </div>


    <div id="throbber" style="display:none">


        <img src="{{asset('assets/img/loading.gif')}}" alt="loading" >
    </div>
@endsection


@section('custom_footer')

    {{----------------------------------------  summit all form --------------------------------------------------------------}}
    <script>

        $(document).on('click', '#save_po', function () {
            submit_all();

            $.blockUI({message: $('#throbber'),
                css: { backgroundColor: 'transparent',
                    border:'none'
                }
            });
        });

        function submit_all() {


            var edit_employee_id = $('#set_po_id').attr('current_po_id');
            /*------------------------------------- employee data----------------------------------------------------------------------*/

            var employee_name = $('#employee_name').val();
            var middle_name = $('#middle_name').val();
            var family_name = $('#family_name').val();
            var parent_email = $('#parent_email').val();
            var country = $('#country').val();
            var partner_first_name = $('#partner_first_name').val();
            var partner_middle_name = $('#partner_middle_name').val();
            var partner_last_name = $('#partner_last_name').val();
            var partner_family_name = $('#partner_family_name').val();
            var children_number = $('#children_number').val();

            var last_name = $('#last_name').val();
            var user_name = $('#user_name').val();
            var gender = $('input[name=gender]:checked').val();
            var nation = $('#nation').val();
            var dateofbirth = $('#date_birth').val();
            var phone = $('#phone').val();
            var office_loc = $('#office_loc').val();
            var address = $('#address').val();

            /*---------------------------------------------*/

            var user_type = $('input[name=foreigner]:checked').val();

            var Marital_Status = $('input[name=status]:checked').val();

            var family = $('#family').val();

            var inputss = $('#emp_family > tbody  > tr');
            var emp_family_row = [];


            var Percentage_Salary = $('#number').val();
            var contract_years = $('#contract_years').val();
            var academic_year2 = $('#academic_year2').val();

            var depart = $('#depart').val();
            var job = $('#job').val();
            var manager = $('#manager').val();


            var Date_of_contract = $('#date2').val();

            var leave = $('input[name=leave]:checked').val();

            var Date_Leaving = $('#date3').val();


            var input2 = $('#allow_table > tbody  > tr');
            var allow_table_row = [];


            var input3 = $('#salary_table > tbody  > tr');
            var salary_table_row = [];


            /*------------------------------------------------------------------------------------------------------------------------------------------------*/

            /* console.log(class_name);
             console.log(cost_code);*/

            var form = document.getElementById("image_form");
            var formData = new FormData(form);
            formData.append('_token', '{{csrf_token()}}');




            /*------- employee data -------*/
            formData.append('edit_employee_id', edit_employee_id);
            formData.append('employee_name', employee_name);
            formData.append('middle_name', middle_name);
            formData.append('last_name', last_name);
            formData.append('children_number', children_number);
            formData.append('family_name', family_name);
            formData.append('country', country);
            formData.append('parent_email', parent_email);
            formData.append('partner_first_name', partner_first_name);
            formData.append('partner_family_name', partner_family_name);
            formData.append('partner_middle_name', partner_middle_name);
            formData.append('partner_last_name', partner_last_name);

            formData.append('user_name', user_name);
            formData.append('gender', gender);
            formData.append('nation', nation);
            formData.append('dateofbirth', dateofbirth);
            formData.append('phone', phone);
            formData.append('office_loc', office_loc);
            formData.append('address', address);
            formData.append('user_type', user_type);
            formData.append('Marital_Status', Marital_Status);
            // formData.append('With_partner', With_partner);
            formData.append('family', family);

            var i = 0;
            var j = 0;
            var k = 0;

            inputss.each(function () {
                if (!$(this).hasClass('added_now')) {
                    formData.append('emp_family_row[' + i + '][' + 0 + ']', $(this).find('.child_name_v').attr('current_value'));
                    formData.append('emp_family_row[' + i + '][' + 1 + ']', $(this).find('.child_middle_name_v').attr('current_value'));
                    formData.append('emp_family_row[' + i + '][' + 2 + ']', $(this).find('.child_last_name_v').attr('current_value'));
                    formData.append('emp_family_row[' + i + '][' + 3 + ']', $(this).find('.class_name_v').attr('current_value'));
                }
                i++;
            });

            input2.each(function () {

                if (!$(this).hasClass('added_now')) {
                    formData.append('allow_table_row[' + k + '][' + 0 + ']', $(this).find('.allow_name_v').attr('current_value'));
                    formData.append('allow_table_row[' + k + '][' + 1 + ']', $(this).find('.academic_year1_v').attr('current_value'));
                    formData.append('allow_table_row[' + k + '][' + 2 + ']', $(this).find('.from_date_v').attr('current_value'));
                    formData.append('allow_table_row[' + k + '][' + 3 + ']', $(this).find('.to_date_v').attr('current_value'));
                    formData.append('allow_table_row[' + k + '][' + 4 + ']', $(this).find('.value_v').attr('current_value'));

                }
                k++

            });

            input3.each(function () {

                if (!$(this).hasClass('added_now')) {
                    formData.append('salary_table_row[' + j + '][' + 0 + ']', $(this).find('.sub_name_v').attr('current_value'));
                    formData.append('salary_table_row[' + j + '][' + 1 + ']', $(this).find('.academic_year2_v').attr('current_value'));
                    formData.append('salary_table_row[' + j + '][' + 2 + ']', $(this).find('.start_in_v').attr('current_value'));

                }
                j++;
            });


            formData.append('Percentage_Salary', Percentage_Salary);
            formData.append('contract_years', contract_years);
            formData.append('academic_year2', academic_year2);
            formData.append('depart', depart);
            formData.append('job', job);
            formData.append('manager', manager);
            formData.append('Date_of_contract', Date_of_contract);
            formData.append('leave', leave);
            formData.append('Date_Leaving', Date_Leaving);


            console.log(formData);

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/employees/',
                dataType: 'json',
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,

                success: function (response) {
                    console.log(response);
                    if (response['error']) {
                        $('.err_input').removeClass('red-border');
                        $('.err_input').parent().find('.select2').removeClass('red-border');
                        $('.errorController').empty();
                        // $('.errorMessage0').show();

                        $('.errorMessage1').hide();
                        $.each(response['error'], function (key, val) {

                            notification('glyphicon glyphicon-warning-sign','Warning',val,'danger');
                            // var row = '<div>'+val+'</div><br>'
                            // $('.errorController').append(row);
                            // $('.errorController').css("display","block");

                            console.log(key + '_error');
                            $("." + key + '_error').addClass('red-border');
                            $('.' + key + '_error').parent().find('.select2').addClass('red-border');

                        })
                        setTimeout($.unblockUI, 500);
                    }
                    else {
                        $('.errorMessage0').hide();

                        $('.errorMessage1').hide();
                        $('.err_input').removeClass('red-border');
                        $('.err_input').parent().find('.select2').removeClass('red-border');
                        $('#set_po_id').attr('current_po_id', response['employee_id']);

                        $('#save_po').text("Edit");
                        $('#save_po').attr('id', 'edit_po');
                        $('#add_row').prop('disabled', true);

                        $('.set_all_disabled').prop('disabled', true);
                        $('#creat_dep').removeClass('hide');

                        $('#more').removeClass('hide');


                        $('#edit_po').removeAttr('disabled');
                        //po-active
                        console.log(response);

                        notification('glyphicon glyphicon-ok-sign','Congratulations!','Employee Saved Succefully.','success')
                        setTimeout($.unblockUI, 500);

                    }

                },
                error: function (response) {
                    alert(' Cant Save This Documents !');
                    $('#save_po').prop('disabled', false);
                    setTimeout($.unblockUI, 500);
                }

            });


        }

    </script>



    {{-------------------------------------------- return father data  -----------------------------------------------------}}



    <script src="{{asset('assets/scripts/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('js/item_name_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/tax_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/select2.min.js')}}"></script>


    <!-----------------active link in nav and active link in sidebar ---->
    {{--------------------  active side & nav bar ------------------------}}
    <script>
        $('.side_sheets').addClass('hide');
        $('#HR').removeClass('hide');

        $("#menu li").removeClass('active');
        $('#hr_nav_bar').addClass('active');

        $('#pages ul li a').removeClass('active');
        $('#admin_adduser_bar').addClass('active');

    </script>




    <!--- ace Scripts -->
    <script type="text/javascript">
        if ('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
    </script>

    <script src="{{asset('assets/scripts/ace-extra.min.js')}}"></script>

    <!-- page specific plugin scripts -->
    <script src="{{asset('assets/scripts/wizard.min.js')}}"></script>
    <script src="{{asset('assets/scripts/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/scripts/jquery-additional-methods.min.js')}}"></script>
    <script src="{{asset('assets/scripts/bootbox.js')}}"></script>
    <script src="{{asset('assets/scripts/jquery.maskedinput.min.js')}}"></script>
    <!-- ace scripts -->
    <script src="{{asset('assets/scripts/ace-elements.min.js')}}"></script>
    <script src="{{asset('assets/scripts/ace.min.js')}}"></script>


    {{-----------  check on next  -----------}}
    <script>
        $(document).on('click','#finish',function(){

            var all_steps =  $(this).parents('.widget-main').find('.step-content');
            var step = all_steps.find(".step-pane:visible");
            console.log('data-step : ' + step.attr('data-step'));

        });
    </script>

    {{------------  add new row  ------------------------}}
    <script>
        $(document).on('click', '#add_row', function () {
            $(this).attr('disabled', true);
            $('#priv').attr('disabled', true);
            $('#save_po').prop('disabled', true);
            $('.edit_row').prop('disabled', true);
            $('.delete_row').prop('disabled', true);
            var table = $(this).closest('.row').siblings('.table').attr('id');

            console.log($(this).closest('.row').siblings('.table').attr('id'));

            if (table == "emp_family") {

                var row = '<tr class= "added_now" >' +
                    '<td td_type="input"><input class="child_name row_data form-control" data-role="input" type="text"/></td>' +
                    '<td td_type="input"><input class="child_middle_name row_data form-control" data-role="input" type="text"/></td>' +
                    '<td td_type="input"><input class="child_last_name row_data form-control" data-role="input" type="text"/></td>' +
                    '<td td_type="search">' +
                    '<select class="class_name form-control js-example-disabled-results">\n'
                    + '<option value="">' + '' + '</option>'
                        @foreach($class as $id=>$role)
                    + '<option value="{{$id}}">' + '{{$role}}'
                        @endforeach
                    + '</select></td>'
                    + '<td>'
                    + '<button type="button" class="set_all_disabled btn btn-primary btn-xs Save_row">' + 'Save' + '</button>'
                    + '</td>'
                    + '<td>'
                    + '<button type="button" class="set_all_disabled btn btn-danger btn-xs delete_row"  >' + '<span class="lnr lnr-trash"></span>' + '</button>'
                    + '</td>'

                    + '</tr>';
                $("#emp_family").prepend(row);
                $(".js-example-disabled-results").select2();

            }
            {{--else if (table == "allow_table") {--}}

                    {{--var row = '<tr class= "added_now" >' +--}}
                    {{--'<td td_type="search">' +--}}
                    {{--'<select class="allow_name form-control js-example-disabled-results">\n'--}}
                    {{--+ '<option value="">' + '' + '</option>'--}}
                    {{--@foreach($allows as $id=>$role)--}}
                    {{--+ '<option allow_value ="" value="{{$id}}">' + '{{$role}}'--}}
                    {{--+ '</option>'--}}
                    {{--@endforeach--}}
                    {{--+ '</select></td>' +--}}
                    {{--'<td td_type="search">' +--}}
                    {{--'<select class="academic_year1 form-control js-example-disabled-results">\n'--}}
                    {{--+ '<option value="">' + '' + '</option>'--}}
                    {{--@foreach($academic_year_table as $acadimic)--}}
                    {{--+ '<option from ="{{$acadimic->start}}" to ="{{$acadimic->end}}" value="{{$acadimic->acadimic_id}}">' + '{{$acadimic->year_name}}'--}}
                    {{--+ '</option>'--}}
                    {{--@endforeach--}}
                    {{--+ '</select></td>'--}}

                    {{--+ '<td td_type="date">'--}}
                    {{--+ '<div class="input-group ">'--}}
                    {{--+ '<input type="text" id="date3" class="form-control c_date from_date" name="from_date">'--}}
                    {{--+ '<div class="input-group-addon">'--}}
                    {{--+ '<i class="fa fa-calendar">'--}}
                    {{--+ '</i>'--}}
                    {{--+ '</div>'--}}
                    {{--+ '</div>'--}}
                    {{--+ '</td>'--}}


                    {{--+ '<td td_type="date">'--}}
                    {{--+ '<div class="input-group ">'--}}
                    {{--+ '<input type="text" id="date3" class="form-control c_date to_date" name="to_date">'--}}
                    {{--+ '<div class="input-group-addon">'--}}
                    {{--+ '<i class="fa fa-calendar">'--}}
                    {{--+ '</i>'--}}
                    {{--+ '</div>'--}}
                    {{--+ '</div>'--}}
                    {{--+ '</td>'--}}


                    {{--+ '<td td_type="input">'--}}
                    {{--+ '<input class="value row_data pull-left form-control required " value="10" disabled  onkeypress="return (event.charCode == 8 || event.charCode == 1) ? null : event.charCode >= 48 && event.charCode <= 57 ||  event.charCode == 46" data-role="input" type="number"  placeholder="0"/></td>' +--}}

                    {{--'<td>'--}}
                    {{--+ '<button type="button" class="set_all_disabled btn btn-primary btn-xs Save_row">' + 'Save' + '</button>'--}}
                    {{--+ '</td>'--}}
                    {{--+ '<td>'--}}
                    {{--+ '<button type="button" class="set_all_disabled btn btn-danger btn-xs delete_row"  >' + '<span class="lnr lnr-trash"></span>' + '</button>'--}}
                    {{--+ '</td>'--}}

                    {{--+ '</tr>';--}}
                    {{--$("#allow_table").prepend(row);--}}
                    {{--$(".c_date").datepicker({format: 'yyyy-mm-dd',autoclose: true, minView: 2});--}}
                    {{--$(".js-example-disabled-results").select2();--}}
                    {{--//get_master_acc_drop_down($('.master_account'));--}}

                    {{--//$(".po_values").siblings('.select2').css('width', '71px');--}}


                    {{--}--}}
            else if (table == "salary_table") {
                var row = '<tr class= "added_now" >' +
                    '<td td_type="search">' +
                    '<select class="sub_name required form-control js-example-disabled-results">\n'
                    + '<option value="">' + '' + '</option>'
                        @foreach($sub_salary as $id=>$role)
                    + '<option value="{{$id}}">' + '{{$role}}'
                    + '</option>'
                        @endforeach
                    + '</select></td>' +
                    '<td td_type="search">' +
                    '<select class="academic_year2 required form-control js-example-disabled-results">\n'
                    + '<option value="">' + '' + '</option>'
                        @foreach($academic_year_table as $acadimic)
                    + '<option from ="{{$acadimic->start}}" to ="{{$acadimic->end}}" value="{{$acadimic->acadimic_id}}">' + '{{$acadimic->year_name}}'
                    + '</option>'
                        @endforeach
                    + '</select></td>'

                    + '<td td_type="date">'
                    + '<div class="input-group ">'
                    + '<input type="text" id="date3" class="form-control required c_date start_in">'
                    + '<div class="input-group-addon">'
                    + '<i class="fa fa-calendar">'
                    + '</i>'
                    + '</div>'
                    + '</div>'
                    + '</td>'

                    + '<td>'
                    + '<button  type="button" class="set_all_disabled btn btn-primary btn-xs Save_row">' + 'Save' + '</button>'
                    + '</td>'
                    + '<td>'
                    + '<button type="button" class="set_all_disabled btn btn-danger btn-xs delete_row">' + '<span class="lnr lnr-trash"></span>' + '</button>'
                    + '</td>'

                    + '</tr>';
                $("#salary_table").prepend(row);
                $(".c_date").datepicker({format: 'yyyy-mm-dd',autoclose: true, minView: 2});

                $(".js-example-disabled-results").select2();



            }


        });
    </script>

    {{--------   save  row  --------------}}
    <script>
        $('.errorMessage1').hide();
        $('.errorMessage2').hide();
        $('.errorMessage3').hide();
        $(document).on("click", ".Save_row", function () {

            console.log('save clicked');

            var empty = false;
            var call_save = false;
            var input = $(this).parents("tr").find('input[type="text"],input[type="number"]');
            var selectors = $(this).parents("tr").find('select');

            input.each(function () {
                if (!$(this).val() || $(this).val() == "") {
                    $(this).addClass("error");
                    empty = true;
                    notification('glyphicon glyphicon-warning-sign','Warning','Please Fill This Field','danger');

                } else {
                    $(this).removeClass("error");
                }
            });

            selectors.each(function () {
                if (!$(this).val() || $(this).val() == "" || $(this).val() < 0  ) {
                    $(this).addClass("error");
                    $(this).siblings().addClass('error');
                    empty = true;
                    notification('glyphicon glyphicon-warning-sign','Warning','PLease Choose This Field','danger');

                } else {
                    $(this).removeClass("error");
                }
            });

            console.log('empty ' + empty);

            $(this).parents("tr").find(".error").first().focus();

            console.log($(input[0]).val() + "  " + $(input[1]).val());

            var table = $(this).parents('table').attr('id');

            console.log(table)

            if(table == "salary_table" && empty !=true){
                var acadimic_from1 = $(this).parents("tr").find('.academic_year2').find('option:selected').attr('from');
                var acadimic_to1 = $(this).parents("tr").find('.academic_year2').find('option:selected').attr('to');
                var start_in = $(this).parents("tr").find('.start_in').val();
                var contract_date = $('#date2').val();
                console.log(start_in > acadimic_from1 );
                console.log(start_in);
                console.log(acadimic_from1 );
                console.log(acadimic_to1 );
                console.log(contract_date );
                if(start_in >= acadimic_from1 && start_in <= acadimic_to1 && start_in >= contract_date && contract_date >= acadimic_from1 && contract_date <= acadimic_to1) {

                    if (!empty) {
                        console.log('pass empty');
                        $(this).prop('disabled', true);
                        save_table_effect($(this));
                        $('.errorMessage1').hide();
                        $('.errorMessage2').hide();
                        $('.errorMessage3').hide();
                    }
                }else if(start_in < contract_date) {
                    $( ".start_in" ).addClass( "error" );
                    notification('glyphicon glyphicon-warning-sign','Warning','Choose Correct Start Date After Date Of Contract ','danger');
                }else if(acadimic_from1 > contract_date || acadimic_to1 < contract_date ) {
                    $( ".academic_year2" ).siblings().addClass( "error" );
                    notification('glyphicon glyphicon-warning-sign','Warning','Choose Correct  Academic Year Responsive To Date Of Contract ','danger');
                }else {
                    $( ".start_in" ).addClass( "error" );
                    $( ".academic_year2" ).siblings().addClass( "error" );
                    notification('glyphicon glyphicon-warning-sign','Warning','Choose Correct Start Date in Same Academic Year Interval ','danger');
                }

            }

            else {
                if (!empty) {
                    console.log('pass empty');
                    $(this).prop('disabled', true);
                    save_table_effect($(this));
                    $('.errorMessage2').hide();
                    $('.errorMessage1').hide();
                    $('.errorMessage3').hide();
                }
            }


        });

        function save_table_effect(thiss) {

            var table = thiss.parents('table').attr('id');

            console.log('table in save :' + table);


            if (table == "emp_family") {
                var child_name = thiss.parents("tr").find('.child_name').val();
                var child_name = thiss.parents("tr").find('.child_middle_name').val();
                var child_name = thiss.parents("tr").find('.child_last_name').val();
                var class_name = thiss.parents("tr").find('.class_name').val();

                console.log('child_name : ' + child_name + 'class_name : ' + class_name);
                console.log('passed')

                /*--------------  all select ------------*/
                var select_array = ['class_name_v'];
                var r = 0;
                var all_dowp_down = thiss.parents("tr").find('select');
                all_dowp_down.each(function () {
                    var value_select = $(this).val();
                    var text_select = $(this).find('option:selected').text();
                    $(this).parent("td").addClass(select_array[r]).attr('current_value', value_select).html(text_select);
                    r++;

                });
                /*--------- all input  ------------------*/
                var input_array = ['child_name_v','child_middle_name','child_last_name'];
                var all_input = thiss.parents("tr").find('input[type="text"]');

                var i = 0;
                all_input.each(function () {

                    var value_select = $(this).val();
                    $(this).parent("td").addClass(input_array[i]).attr('current_value', value_select).html(value_select);

                    i++;
                });
                /*--------------------------*/

            }




            else if (table == "salary_table") {


                var sub_name = thiss.parents("tr").find('.sub_name').val();
                var start_in = thiss.parents("tr").find('.start_in').val();
                var academic_year = thiss.parents("tr").find('.academic_year2').val();

                console.log('sub_name: ' + sub_name + 'start_in: ' + start_in + 'academic_year: ' + academic_year);
                console.log('passed')

                /*--------------  all select ------------*/
                var select_array = ['sub_name_v', 'academic_year2_v'];
                var r = 0;
                var all_dowp_down = thiss.parents("tr").find('select');
                all_dowp_down.each(function () {
                    var value_select = $(this).val();
                    var text_select = $(this).find('option:selected').text();
                    $(this).parent("td").addClass(select_array[r]).attr('current_value', value_select).html(text_select);
                    r++;
                });

                /*--------- all input  ------------------*/
                var input_array = ['start_in_v'];
                var all_input = thiss.parents("tr").find('input[type="text"]');

                var i = 0;
                all_input.each(function () {

                    var value_select = $(this).val();
                    $(this).parents("td").addClass(input_array[i]).attr('current_value', value_select).html(value_select);

                    i++;
                });
                /*--------------------------*/


            }


            if (thiss.parents("tr").hasClass('added_now')) {
                thiss.parents("tr").removeClass('added_now');

            }
            if (thiss.parents("tr").hasClass('edited_now')) {
                thiss.parents("tr").removeClass('edited_now');

            }


            // if ($('.added_now').length == 0) {
            $(".add_row").removeAttr("disabled");
            // }
            $('#priv').attr('disabled', false);
            $('#date2').attr('disabled', true);
            $('#save_po').prop('disabled', false);
            $('.add_row').prop('disabled', false);
            $('.delete_row').prop('disabled', false);
            $('.edit_row').prop('disabled', false);
            $('.delete_row').prop('disabled', false);
            thiss.text('Edit');
            thiss.removeClass('Save_row');
            thiss.addClass('edit_row');
            thiss.prop('disabled', false);
            $(".errorTableMessage").hide();

        }

    </script>

    {{----------------------  edit row --------------------}}
    <script>
        $(document).on("click", ".edit_row", function () {


            $('#priv').attr('disabled', true);
            $('#save_po').prop('disabled', true);
            $('.add_row').prop('disabled', true);
            $('.delete_row').prop('disabled', true);

            var table = $(this).parents('table').attr('id');

            console.log(table);

            if (table == "emp_family") {


                if (!$('#emp_family tr').hasClass('added_now')) {

                    var input_class_array = ['child_name','child_middle_name','child_last_name', 'class_name'];
                    var s = 0;

                    $('#add_row').prop('disabled', true);
                    $(this).parents("tr").addClass('edited_now');

                    $(this).parents("tr").find("td:not(:last-child)").each(function () {

                        if ($(this).attr('td_type') == 'input') {
                            $(this).html('<input class="' + input_class_array[s] + ' row_data form-control" data-role="input" type="text" value="' + $(this).text() + '"/>');
                            s++;
                        }

                        else if ($(this).attr('td_type') == 'search') {
                            if ($(this).hasClass('class_name_v')) {
                                $(this).html('<select class="class_name form-control js-example-disabled-results">\n'
                                    + '<option value="">' + '' + '</option>'
                                        @foreach($class as $id=>$role)
                                    + '<option value="{{$id}}">' + '{{$role}}'
                                        @endforeach
                                    + '</option>'
                                    + '</select>');

                                var selected_v = $(this).attr('current_value');
                                $(this).find(".class_name").val(selected_v);
                                $(".calss_name").select2();
                                $(".class_name").siblings('.select2').css('width', '71px');
                            }


                        }


                    });


                }

            }

            else if (table == "salary_table") {

                if (!$('#salary_table tr').hasClass('added_now')) {

                    var input_class_array = ['sub_name', 'academic_year2'];
                    var s = 0;

                    $('#add_row').prop('disabled', true);
                    $(this).parents("tr").addClass('edited_now');

                    $(this).parents("tr").find("td:not(:last-child)").each(function () {
                        if ($(this).attr('td_type') == 'date') {
                            var i =6
                            $(this).html('<input class="start_in row_data form-control alldate" data-role="input"  type="text" value="' + $(this).text() + '"/>');

                            $('.alldate').datepicker({format: 'yyyy-mm-dd',autoclose: true, minView: 2 });
                        }

                        else if ($(this).attr('td_type') == 'search') {
                            if ($(this).hasClass('sub_name_v')) {
                                $(this).html('<select class="sub_name form-control js-example-disabled-results">\n'
                                    + '<option value="">' + '' + '</option>'
                                        @foreach($sub_salary as $id=>$role)
                                    + '<option value="{{$id}}">' + '{{$role}}'
                                    + '</option>'
                                        @endforeach
                                    + '</option>'
                                    + '</select>');

                                var selected_v = $(this).attr('current_value');
                                $(this).find(".sub_name").val(selected_v);
                                $(this).find(".sub_name").select2();
                                $(this).find(".sub_name").siblings('.select2').css('width', '71px');
                            }
                            else if ($(this).hasClass('academic_year2_v')) {
                                $(this).html('<select class="academic_year2 form-control js-example-disabled-results">\n'
                                    + '<option value="">' + '' + '</option>'
                                        @foreach($academic_year_table as $acadimic)
                                    + '<option from ="{{$acadimic->start}}" to ="{{$acadimic->end}}" value="{{$acadimic->acadimic_id}}">' + '{{$acadimic->year_name}}'
                                    + '</option>'
                                        @endforeach
                                    + '</option>'
                                    + '</select>');

                                var selected_v = $(this).attr('current_value');
                                $(this).find(".academic_year2").val(selected_v);
                                $(this).find(".academic_year2").select2();
                                $(this).find(".academic_year2").siblings('.select2').css('width', '71px');
                            }

                            s++;
                        }


                    });


                }

            }

            $("#add_row").attr("disabled", "disabled");
            $(this).text('Save');
            $(this).removeClass('edit_row');
            $(this).addClass('Save_row');
        });

    </script>
    <!--delete row in table -->
    <script>

        $(document).on('click', '.delete_row', function () {

            $('#priv').attr('disabled', true);
            $('#save_po').prop('disabled', true);
            $('#finish').prop('disabled', true);
            $('.add_row').prop('disabled', true);
            $('.edit_row').prop('disabled', true);

            $(this).hide(function () {
                $(this).closest('td').append('<button class="btn btn-danger set_all_disabled" type="button" id="yes" style="display: none;">yes</button>' +
                    '<button class="btn btn-default set_all_disabled" type="button" id="no" style="display: none;">no</button>');
                $(this).closest('td').find("#yes").show('slow').delay(1000);
                $(this).closest('td').find("#no").show('slow').delay(1000);

            });
        });

        $(document).on('click', '#yes', function () {

            var rowCount = $("#salary_table > tbody").children.length -1;
            console.log('rowCount :' + rowCount);
            if (rowCount == 0) {
                $('#date2').attr('disabled', false);
            }
            $(this).closest('tr').remove();
            $(".add_row").removeAttr("disabled");
            $('#priv').attr('disabled', false);
            $('#save_po').prop('disabled', false);
            $('#finish').prop('disabled', false);
            $('.add_row').prop('disabled', false);
            $('.edit_row').prop('disabled', false);
            $('.edit_row').prop('disabled', false);
        });

        $(document).on('click', '#no', function () {

            console.log('noo');

            $(this).closest('td').find(".delete_row").show('slow').delay(1000);
            $(this).closest('td').find("#yes").hide('slow').delay(1000).remove();
            $(this).closest('td').find("#no").hide('slow').delay(1000).remove();
            $('#priv').attr('disabled', false);
            $('#save_po').prop('disabled', false);
            $('#finish').prop('disabled', false);
            $('.add_row').prop('disabled', false);
            $('.edit_row').prop('disabled', false);


        });


    </script>
    <!-- inline scripts related to this page    form ace_master -->
    <script type="text/javascript">
        jQuery(function ($) {

            $('[data-rel=tooltip]').tooltip();

            $('.select2').css('width', '200px').select2({allowClear: true})
                .on('change', function () {
                    $(this).closest('form').validate().element($(this));
                });


            var $validation = true;
            var $validation_father = true;
            $('#fuelux-wizard-container')
                .ace_wizard({

                })
                .on('actionclicked.fu.wizard', function (e, info) {

                    if (info.step == 1 && $validation){}

                    if (!$('#step1_validation-form').valid())
                    {
                        notification('glyphicon glyphicon-warning-sign','Warning','You Should Fill All Fields','danger');
                        e.preventDefault();
                    }








                    if (info.step == 2 && $validation_father && info.direction === 'next') {


                        if (!$('#step2_validation-form').valid()) {

                            notification('glyphicon glyphicon-warning-sign','Warning','You Should Fill All Fields','danger');

                            e.preventDefault();
                        }
                    }


                    if (info.step == 3 && $validation_father && info.direction === 'next') {

                        var rowCount = $('#salary_table tbody .added_now').length;
                        var saved_row_Count = $('#salary_table tbody tr:not(.added_now)').length;
                        var edited_row_Count = $('#salary_table tbody .edited_now').length;

                        console.log('rowCount :' + rowCount);
                        if (rowCount > 0) {
                            e.preventDefault();
                            notification('glyphicon glyphicon-warning-sign','Warning','You Should Fill All Table Field','danger');

                        }
                        else if (saved_row_Count == 0) {
                            e.preventDefault();
                            $(".alert-danger").hide();
                            notification('glyphicon glyphicon-warning-sign','Warning','You Should Save Table Row ' ,'danger');
                        }
                        else if (edited_row_Count > 0) {
                            e.preventDefault();
                            notification('glyphicon glyphicon-warning-sign','Warning','You Should Complete Edit Process','danger');                     }

                        else {
                            $(".alert-danger").hide();
                            $(".errorTableMessage").hide();
                            if (!$('#step4_validation-form').valid()) e.preventDefault();
                        }
                    }


                }).on('finished.fu.wizard', function(e) {

                if ($('#save_po').length) {
                    console.log('finished ');
                    notification('glyphicon glyphicon-ok-sign', '', 'Please Press Save ', 'success');
                }
            }).on('stepclick.fu.wizard', function (e) {
                //e.preventDefault();//this will prevent clicking and selecting steps

            });


            /*------------------------  step1 vali  form  ------------------------------------*/
            var validator1 = $('#step1_validation-form').validate({
                errorElement: 'div',
                errorClass: 'help-block',
                focusInvalid: false,
                ignore: "",
                rules: {
                    employee_name: {
                        required: true,

                    },
                    parent_email: {
                        required: true,

                    },
                    last_name: {
                        required: true,

                    },
                    country: {
                        required:true,
                    },
                    email: {
                        required:true,
                    },
                    middle_name	: {
                        required: true,

                    },
                    family_name	: {
                        required: true,

                    },gender: {
                        required: true,
                    },
                    form_field_radio: {
                        required: true,
                    },
                    dateofbirth: {
                        max: '{{$min_age}}',
                        min: '',
                        required: true,
                    },
                    nation: {
                        required: true,
                    },
                },
                messages: {
                    gender: "Please choose gender",
                    employee_name: "Please Enter Employee First Name",
                    middle_name: "Please Enter Employee Middle Name",
                    last_name: "Please Enter Employee Last Name",
                    family_name: "Please Enter Employee Family Name",
                    acadmic_year: "Please choose Academic year",
                    country: "Please Choose Country",
                    email: "Please Enter Valid E-mail",
                },

                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
                },

                success: function (e) {
                    $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
                    $(e).remove();
                },

                errorPlacement: function (error, element) {
                    if (element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
                        var controls = element.closest('div[class*="col-"]');
                        if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                        else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                    }
                    else if (element.is('.select2')) {
                        error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                    }
                    else if (element.is('.chosen-select')) {
                        error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                    }
                    else error.insertAfter(element.parent());
                },

                submitHandler: function (form) {
                },
                invalidHandler: function (form) {
                }
            });


            /*------------------------  step2 validate form  ------------------------------------*/

            var validator2 = $('#step2_validation-form').validate({
                errorElement: 'div',
                errorClass: 'help-block',
                focusInvalid: false,
                ignore: "",
                rules: {
                    foreigner: {
                        required: true,
                    },
                    children_number: {
                        required: false,
                    },
                    partner_first_name: {
                        required: false,
                    },
                    partner_middle_name: {
                        required: false,
                    },
                    partner_last_name: {
                        required: false,
                    },
                    partner_family_name: {
                        required: false,
                    },
                    status: {
                        required: true,
                    },
                    percentage_salary: {
                        required: true,

                    },
                    class: {
                        required: true,
                    },
                    academic_year2: {
                        required: true,
                    },
                    depart: {
                        required: true,

                    },
                    job: {
                        required: true,
                    },
                    dateofcontract: {
                        required: true
                    },
                    contract_years: {
                        required: true,
                        min: '1',

                    },
                    manager: {
                        required: true
                    },
                    leave: {
                        required: true
                    },

                },

                messages: {

                    foreigner: "Please choose Foreigner",
                    leave: "Please Choose Leave",
                    manager: "Please Choose Manager",
                    dateofcontract: "Please Choose Date Of Contract",
                    job: "Please choose Job",
                    contract_years: "Please choose Contract Years",
                    department: "Please Choose Department",
                    state: "Please Choose State",
                },


                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
                },

                success: function (e) {
                    $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
                    $(e).remove();
                },

                errorPlacement: function (error, element) {
                    if (element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
                        var controls = element.closest('div[class*="col-"]');
                        if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                        else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                    }
                    else if (element.is('.select2')) {
                        error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                    }
                    else if (element.is('.chosen-select')) {
                        error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                    }
                    else error.insertAfter(element.parent());
                },

                submitHandler: function (form) {
                },
                invalidHandler: function (form) {
                }
            });



            /*--------------------------------  step3 validate form -----------------------------------------------*/
            var validator3 = $('#step3_validation-form').validate({
                errorElement: 'div',
                errorClass: 'help-block',
                focusInvalid: false,
                ignore: "",
                rules: {

                },
                messages: {
                },

                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
                },

                success: function (e) {
                    $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
                    $(e).remove();
                },

                errorPlacement: function (error, element) {
                    if (element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
                        var controls = element.closest('div[class*="col-"]');
                        if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                        else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                    }
                    else if (element.is('.select2')) {
                        error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                    }
                    else if (element.is('.chosen-select')) {
                        error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                    }
                    else error.insertAfter(element.parent());
                },

                submitHandler: function (form) {
                },
                invalidHandler: function (form) {
                }
            });
            /*----------------------------------------------------------------------------------*/

            $('#modal-wizard-container').ace_wizard();
            $('#modal-wizard .wizard-actions .btn[data-dismiss=modal]').removeAttr('disabled');
            $(document).one('ajaxloadstart.page', function (e) {
                //in ajax mode, remove remaining elements before leaving page
                $('[class*=select2]').remove();
            });
        })
    </script>

    <!--show and hide sibiling-->

    <script>
        $(function () {
            $("input[name='sibling']").click(function () {
                if ($("#yes").is(":checked")) {
                    $(".code").show();
                    $(".first").show();
                    $(".sir").show();
                } else {
                    $(".code").hide();
                    $(".first").hide();
                    $(".sir").hide();
                }
            });
        });
    </script>

    <script>
        $(".staff_namef").hide();


        $(function () {

            $("input[name='rel']").click(function () {
                if ($("#yes3").is(":checked")) {
                    $(".staff_namef").show();

                } else {
                    $(".staff_namef").hide();

                }
            });
        });

    </script>

    <script>
        $(".staff_namem").hide();
        $(function () {
            $("input[name='rel1']").click(function () {
                if ($("#yes4").is(":checked")) {
                    $(".staff_namem").show();

                } else {
                    $(".staff_namem").hide();

                }
            });
        });

    </script>

    <!--select  -->

    <script>
        $(".myselect1").select2({
            width: 'resolve'

        });

        $(".myselect2").select2({
            width: 'resolve',


        });
        $(".myselect3").select2({
            width: 'resolve',


        });
        $(".myselect4").select2({
            width: 'resolve',


        });
        $(".myselect5").select2({
            width: 'resolve'

        });

        $(".myselect6").select2({
            width: 'resolve',


        });
        $(".myselect7").select2({
            width: 'resolve',


        });
        $(".myselect8").select2({
            width: 'resolve',


        });

    </script>
    <!--radio buttons checked on load -->
    <script>

        $(function () {
            var $radios = $('input:radio[name=finance]');
            if ($radios.is(':checked') === false) {
                $radios.filter('[value=yes5]').prop('checked', true);
            }
        });

        $(function () {
            var $radios1 = $('input:radio[name=finance1]');
            if ($radios1.is(':checked') === false) {
                $radios1.filter('[value=no6]').prop('checked', true);
            }
        });

        $("#yes5").prop('checked', true).trigger("change");
        $("#no1").prop('checked', true).trigger("change");

    </script>

    <!----- upload photo ---->
    <script>

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#imagePreview').css('background-image', 'url(' + e.target.result + ')');
                    $('#imagePreview').hide();
                    $('#imagePreview').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#imageUpload").change(function () {
            readURL(this);
        });
    </script>

    <!-- show and hide partner for married   -->

    <!-- show and hide date of leaving -->
    <script>

        $(function () {

            $("input[name='leave']").click(function () {
                if ($("#yes7").is(":checked")) {
                    $(".leaving").show();
                    $(".leaving").removeClass('hide');
                } else {
                    $(".leaving").hide();
                }
            });
        });

    </script>
    <!-- show and hide family for Employee -->



    <!-- ajax request propagate -->
    <script>

        $(document).ready(function () {
            $(document).on('change', '#depart', function () {
                var id = $(this).val()
                $.ajax({
                    url: '/get_job/',
                    type: 'get',
                    dataType: 'html',
                    data: {id: id},
                    success: function (data) {
                        $('#job').html(data)
                    }
                })
            });
        });
    </script>

    <script>

        $(document).ready(function () {
            $(document).on('change', '#depart', function () {
                var id = $(this).val()
                $.ajax({
                    url: '/get_manager/',
                    type: 'get',
                    dataType: 'html',
                    data: {id: id},
                    success: function (data) {
                        $('#manager').html(data)
                    }
                })
            });
        });
    </script>

    <script>


        $(document).on('change', '.allow_name', function () {
            var allow = $(this);

            var id = allow.val();
            var type = $('#Foreigner').val();
            if (type == "foreign"){ var currency = 1}
            else{var currency = 2}
            console.log( id + type +currency )
            $.ajax({
                url: '/get_allow_value/',
                type: 'get',
                dataType: 'json',
                data: {id: id, currency: currency},
                success: function (data) {
                    allow.parents('tr').find('.value').attr('value', data);
                    console.log('pass');
                    console.log(allow.parents("tr").attr('class'));
                }
            })
        });

    </script>

    <!-- show and edit page convertation-->

    <script>
        $(document).on('click', '#edit_po', function () {
            $('#edit_po').text("Save");
            $('#edit_po').attr('id', 'save_po');
            $('.add_row').prop('disabled', false);
            $('.set_all_disabled').prop('disabled', false);
            $('#creat_dep').addClass('hide');
            $('#more').addClass('hide');
        });
    </script>

    <!-- on click finished - show message -->
    <script>
        $("#save_po").attr('disabled', true);
        $('.alert-autocloseable-success').hide();
        $(document).on('click', '#finish', function () {
            if ($(this).text() == 'Finish') {
                $('html, body').animate({scrollTop: 0}, 'fast');
                $("#save_po").attr('disabled', false);

            }

        });

    </script>

    {{-----------------------print----------------}}
    <script>

        $(document).on('click', '#print', function () {

            var id = $('#set_po_id').attr('current_po_id');
            window.open('/get_employee_rpt/' + id + '', '_blank');

        });

    </script>


    {{-----------------------print----------------}}
    <script>
        $(document).on('click', '#creat_dep', function () {
            window.open('/employees/create', '_self');
        });
    </script>

    <script>
        $('#date_birth').datepicker({format: 'yyyy-mm-dd',autoclose: true, minView: 2 ,todayBtn: true ,todayHighlight:true});

    </script>

    <!-- show married or family or single @edit -->
    <script>



        $(document).ready(function() {

            @if(isset($employee))

            @if($employee->marital_status =="married" && $employee->family =="yes")
            $(".with_family").show();
            $('.partner_info').show();
            @elseif($employee->marital_status =="married")
            $('.with_family').show();
            @else
            $(".with_family").hide();
            $('.partner_info').hide();
            @endif
            @else
            $(".with_family").hide();
            $('.partner_info').hide();

            @endif



            $('input[type="radio"]').click(function () {
                if ($(this).attr("value") == "single") {

                    $(".with_family").hide();
                    $(".partner_info").hide();

                }
                if ($(this).attr("value") == "married"){

                    $(".with_family").show();

                }

            });

            $('#family').click(function() {

                if ($(this).is(':checked')) {
                    $('.partner_info').show();
                    $('#family').attr('value',"yes");
                    console.log($('#family').val());

                } else {

                    $('.partner_info').hide();
                    $('#family').attr('value',"no");
                    console.log($('#family').val());
                }
            });
        });





    </script>
@endsection