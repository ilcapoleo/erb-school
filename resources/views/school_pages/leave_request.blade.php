@extends('layouts.main_app')

@section('main_content')
    <link href="{{asset('css/fastselect.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/select2.min.css')}}" rel="stylesheet" />
    <style>
        input.col-md-4.text-center {

            border:0;
            background-color: #fff;
        }




        /*responsive third nav buttons*/

        @media all and(min-width:500px){
            #sendto_approved_btn{
                margin-left: 429%;
            }
            #approved_btn{
                margin-left: 806%;
            }

            button#send_mail{
                background-color: red;
            }
        }


        .collapse > .row>.col-md-4>.btn, .collapse > .row>.col-md-4>.dropdown>.btn {
            margin-top: 0;

        }

        .material-switch > label::before {
            background: rgb(0, 0, 0);
            box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
            border-radius: 8px;
            content: '';
            height: 16px;
            margin-top: 3px;
            position: absolute;
            opacity: 0.3;
            transition: all 0.4s ease-in-out;
            width: 40px;
            left: -29px;
        }


        .material-switch > label::after {
            background: rgb(255, 255, 255);
            border-radius: 16px;
            box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
            content: '';
            height: 24px;
            left: -45px;
            margin-top: -8px;
            position: absolute;
            top: 6px;
            transition: all 0.3s ease-in-out;
            width: 24px;
        }

        .material-switch > input[type="checkbox"]:checked + label::after {
            background: inherit;
            left: -5px;
        }
        .panel .table > thead > tr > td:first-child, .panel .table > thead > tr > th:first-child, .panel .table > tbody > tr > td:first-child, .panel .table > tbody > tr > th:first-child, .panel .table > tfoot > tr > td:first-child, .panel .table > tfoot > tr > th:first-child{
            padding-left: 0px;
        }

        ul{
            margin: 0;
            padding: 0;
            list-style: none;
        }


        #sendDropDownOptions{
            width: 50%!important;
        }




        span.select2.select2-container.select2-container--default{
            width: 100%!important;
        }

        #hours , #days{

            line-height: 32px;
        }


        .red-border{
            border: 1px solid red  !important;
        }
    </style>
    <div class="main">
        <!-- Navbar-content -->
        <nav class="navbar navbar-default navbar2">

            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                    <!--breadcrumbs-->
                    <div class="row">

                        <div class="col-md-10 col-lg-10">

                        <ol class="breadcrumb breadcrumb_nav">
                            <li><a href="/show_leave_request">Leave Request</a></li>
                            <li class="active" id="second">
                                @if(isset($leave_request))
                                    @if($leave_request->leave_r_id != 0)
                                        {{$leave_request->leave_r_id}}
                                    @else
                                        Draft
                                    @endif
                                @endif
                            </li>
                        </ol>
                        </div>

                        @if(isset($leave_request))
                            <div class="col-md-2 col-lg-2">
                                @if(@\App\Leave_request::where('leave_r_id','<',$leave_request->leave_r_id)->orderBy('leave_r_id','desc')->first()->leave_r_id)
                                    <div class="col-md-6">
                                        <a href="/leave_request/edit/{{@\App\Leave_request::where('leave_r_id','<',$leave_request->leave_r_id)->orderBy('leave_r_id','desc')->first()->leave_r_id}}"><span
                                                    class="glyphicon glyphicon-arrow-left"></span>
                                        </a>
                                    </div>
                                @endif
                                @if(@\App\Leave_request::where('leave_r_id','>',$leave_request->leave_r_id)->orderBy('leave_r_id','asc')->first()->leave_r_id)
                                    <div class="col-md-6">
                                        <a href="/leave_request/edit/{{@\App\Leave_request::where('leave_r_id','>',$leave_request->leave_r_id)->orderBy('leave_r_id','asc')->first()->leave_r_id}}">
                                    <span class="glyphicon glyphicon-arrow-right">
                                </span>
                                        </a>
                                    </div>
                                @endif
                            </div>
                        @endif

                    </div>   <!--End breadcrumbs-->
                    <div class="row">
                        <div class="col-md-4 col-lg-4 buttons">
                            @if(isset($leave_request))
                                @if($leave_request->request_status_id == 3 || $leave_request->request_status_id == 6)
                                    <button id="edit_po" type="button" class="btn btn-danger " >Edit</button>
                                    <button type="button" class="btn" id="creat_po" >Create</button>
                                @else
                                    {{--<button id="save_po" type="button" class="btn btn-danger " >Save</button>
                                    <button type="button" class="btn hide" id="creat_po" >Create</button>--}}
                                    <button id="edit_po" type="button" class="btn btn-danger " >Edit</button>
                                    <button type="button" class="btn" id="creat_po" >Create</button>
                                @endif
                            @else
                                <button id="save_po" type="button" class="btn btn-danger " >Save</button>
                                <button type="button" class="btn hide" id="creat_po" >Create</button>
                            @endif
                        </div>

                        <div class="col-md-4">
                            @if(isset($leave_request))

                                @if($leave_request->request_status_id == 3 || $leave_request->request_status_id == 6|| $leave_request->request_status_id == 2)
                                    <div class="dropdown">
                                        <button class="btn btn-primary  dropdown-toggle" type="button" data-toggle="dropdown" id="po_more"  hidden>More
                                            <span class="caret"></span></button>
                                        <ul class="dropdown-menu">

                                            <li class="hide"><a href="#" id="delete_po">Delete</a></li>
                                            <!-- temporery -->  {{--<li><a href="#">Export</a></li>--}}
                                            {{--<li><a href="#" id="dublicate_po">Duplicate</a></li>--}}
                                            <li><a href="#" id="print">Print</a></li>

                                        </ul>
                                    </div>
                                @elseif($leave_request->request_status_id == 1)
                                    <div class="dropdown">
                                        <button class="btn btn-primary  dropdown-toggle" type="button" data-toggle="dropdown" id="po_more"  hidden>More
                                            <span class="caret"></span></button>
                                        <ul class="dropdown-menu">

                                            <li class=""><a href="#" id="delete_po">Delete</a></li>
                                            <!-- temporery --> {{--<li><a href="#">Export</a></li>--}}
                                            {{--<li><a href="#" id="dublicate_po">Duplicate</a></li>--}}
                                            <li><a href="#" id="print">Print</a></li>

                                        </ul>
                                    </div>
                                @else
                                    <div class="dropdown">
                                        <button class=" btn btn-primary  dropdown-toggle" type="button" data-toggle="dropdown" id="po_more"  hidden>More
                                            <span class="caret"></span></button>
                                        <ul class="dropdown-menu">

                                            <li class=""><a href="#" id="delete_po">Delete</a></li>
                                            {{--<li><a href="#">Export</a></li>--}}
                                            {{--<li><a href="#" id="dublicate_po">Duplicate</a></li>--}}
                                            <li><a href="#">Print</a></li>

                                        </ul>
                                    </div>
                                @endif
                            @else
                                <div class="dropdown">
                                    <button class="hide btn btn-primary  dropdown-toggle" type="button" data-toggle="dropdown" id="po_more"  hidden>More
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">

                                        <li class="hide"><a href="#" id="delete_po">Delete</a></li>
                                        {{--<li><a href="#">Export</a></li>--}}
                                        {{--<li><a href="#" id="dublicate_po">Duplicate</a></li>--}}
                                        <li><a href="#" id="print">Print</a></li>

                                    </ul>
                                </div>
                            @endif
                        </div>
                        <!--search-->
                        <!--
                         <div class="col-md-4">
                             <form class="navbar-form navbar-right" >
                                 <div class="input-group form-group">
                                     <input type="text" value="" class="form-control" placeholder="Search ...">
                                 </div>
                             </form>
                         </div>
                         -->
                    </div> <!--collapse -->

                </div><!--collapse -->

                <!--buttons row +second breadcrumb-->

                <div class="row third-nav">


                    <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">

                        <div class="col-md-3 col-lg-3 col-sm-3 col-xs-3 ">
                            <button  id="send_mail" type="button" class="{{--set_all_disabled--}} btn btn_third_nav" >Send by Mail</button>
                        </div>

                        <div class="col-md-3 col-lg-3 col-sm-3 col-xs-3">
                            <button id="sendto_approved_btn" type="button" class="set_all_disabled btn btn_third_nav" @if(isset($leave_request)) disabled  @endif>To Approve</button>
                        </div>

                        <div class="col-md-3 col-lg-3 col-sm-3 col-xs-3">
                            <button id="approved_btn" type="button" class="set_all_disabled btn approved btn_third_nav " @if(isset($leave_request))disabled  @endif>Approved</button>
                        </div>

                        <div class="col-md-3 col-lg-3 col-sm-3 col-xs-3">
                            @if(isset($leave_request))
                            <button id="refused_btn" type="button" class="set_all_disabled btn approved btn_third_nav @if($leave_request->request_status_id == 1) hide @endif "  disabled  >Refuse</button>
                              @else
                                <button id="refused_btn" type="button" class="set_all_disabled btn approved hide btn_third_nav" >Refuse</button>
                            @endif
                        </div>

                    </div>


                    <div class="col-md-6 col-lg-6 col-sm-12">


                            <span id="current_choosed_status" coosed_status="" class="breadcrumbs ">
                                 @if(isset($leave_request))

                                    <span id="draft_po"  class="@if($leave_request->request_status_id == 1) po-active @endif breadcrumb">Draft </span></li>
                                    <span id="to_approve_po"  class="@if($leave_request->request_status_id == 2) po-active @endif breadcrumb">To Approved </span></li>
                                    <span id="approved_po"  class="@if($leave_request->request_status_id == 3) po-active @endif  breadcrumb ">Approved </span></li>
                                    <span id="refused"  class="@if($leave_request->request_status_id == 6) po-active @elseif($leave_request->request_status_id == 1) hide @endif  breadcrumb ">Refused</span></li>

                                @else
                                    <span id="draft_po"  class="breadcrumb">Draft </span></li>
                                    <span id="to_approve_po"  class="breadcrumb">To Approved </span></li>
                                    <span id="approved_po"  class="breadcrumb ">Approved </span></li>
                                    <span id="refused" class="hide breadcrumb">Refused</span>
                                @endif
                            </span>



                    </div>



                </div>
            </div>
        </nav>
        <!-- End Navbar-content -->

        <!-- Success messages -->
        <div class="alert alert-success alert-autocloseable-success" style="display:none;position:absolute;" >
            Your Leave Request Saved Successfully!

        </div>
        <!--end Success messages -->


        <!-- failuer messages -->
        <div class="alert alert-danger errorMessage1" style="display:none;position:absolute;" >
            Your should fill all fields!

        </div>
        <!--end failuer messages -->






        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Leave Request</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">

                            {{-- set po sitting--}}
                            @if(isset($leave_request))
                                <div id="set_po_id"  current_po_id="{{$leave_request->leave_r_id}}" current_po_status="{{$leave_request->status_name}}" closed_or_not="" current_id="{{Auth::user()->id}}"></div>
                            @else
                                <div id="set_po_id"  current_po_id="" current_po_status="draft_po" closed_or_not="0" current_id="{{Auth::user()->id}}"></div>
                            @endif

                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <!--  Employee   -->
                                    <div class="col-md-6 col-lg-6">
                                        <div class="form-group">

                                            <label class="col-md-6 col-lg-6 control-label">Employee</label>
                                            <div class="col-md-6 col-lg-6">

                                                <select id="employee" name="employee" class="get_all_selectors set_all_disabled select_2_enable form-control required employee_error err_input" @if(isset($leave_request))  disabled  @endif >
                                                    <option value=""> </option>
                                                    @foreach($emploee as $s_emploee)
                                                        @if(isset($leave_request))
                                                            @if($leave_request->employee_id == $s_emploee->employee_id)
                                                                <option selected value="{{$s_emploee->employee_id}}">{{$s_emploee->employee_name}}</option>
                                                            @else
                                                                <option value="{{$s_emploee->employee_id}}">{{$s_emploee->employee_name}}</option>
                                                            @endif
                                                        @else
                                                            @if($s_emploee->employee_user_id == Auth::user()->id)
                                                                <option selected value="{{$s_emploee->employee_id}}">{{$s_emploee->employee_name}}</option>
                                                            @else
                                                                <option value="{{$s_emploee->employee_id}}">{{$s_emploee->employee_name}}</option>
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                </select>


                                            </div>
                                        </div>
                                    </div>


                                    <!--Requested date -->

                                    <div class="col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <label class=" control-label col-md-6 col-lg-6 " for="datetime1"  >Requested Date</label>

                                            <div  class=" col-md-6 col-lg-6">
                                                <div class="form-group">


                                                    <div class="input-group ">

                                                        @if(isset($leave_request))
                                                            <input type="text" id="requested_date" class=" all_date form-control " crrent_date="{{$curr_date}}" name="date" value="{{$leave_request->request_date}}" @if(isset($master_inventory)) disabled @endif  disabled>
                                                        @else
                                                            <input type="text" id="requested_date" class=" all_date form-control  " crrent_date="{{$curr_date}}" name="date" value="{{$curr_date}}" disabled>
                                                        @endif


                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar">
                                                            </i>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>





                                        </div>


                                    </div>





                                        </div>
                                    </div>
                            <br>
                            <div class="row">

                                <div class="col-md-12 col-lg-12">


                                    <div class="col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <label for="usr" class="col-md-6 col-lg-6 control-label">Department</label>

                                            <div id="department_name" class=" col-md-6 col-lg-6">
                                                @if(isset($leave_request))
                                                    {{$leave_request->dep_name}}
                                                @else
                                                    @foreach($emploee as $s_emploee)
                                                        @if($s_emploee->employee_user_id == Auth::user()->id)
                                                            {{$s_emploee->dep_name}}
                                                        @endif

                                                    @endforeach
                                                @endif
                                            </div>

                                        </div>

                                    </div>



                                    <!--Approved date -->

                                    <div class="col-md-6 col-lg-6">

                                                        <div class="form-group">
                                                            <label class=" control-label col-md-6 col-lg-6 " for="datetime1">Approved Date</label>
                                                            <div  class=" col-md-6 col-lg-6">
                                                                <div class="form-group">


                                                                <div class="input-group ">


                                                                @if(isset($leave_request))
                                                                    <input disabled type="text" id="approved_date " class=" form-control" crrent_date="{{$curr_date}}" name="date" value="{{$leave_request->approved_date}}" @if(isset($leave_request)) disabled  @endif>
                                                                @else
                                                                    <input disabled type="text" id="approved_date" class=" form-control" crrent_date="{{$curr_date}}" name="date" value="">
                                                                @endif

                                                                <div class="input-group-addon">
                                                                    <i class="fa fa-calendar">
                                                                    </i>
                                                                </div>
                                                            </div>

                                                        </div>
                                                            </div>

                                                        </div>



                                </div>

                                </div>

                            </div>


                            <br>

                            <div class="row">
                                <div class="col-md-12 col-lg-12">

                                    <div class="col-md-6 col-lg-6">


                                        <div class="form-group ">
                                            <label class="col-md-6 col-lg-6  control-label">Leave Type</label>
                                            <div class="col-md-6 col-lg-6">

                                                <select id="leave_type" name="leave_type" class="get_all_selectors set_all_disabled select_2_enable form-control col-md-6 col-lg-6 required err_input leave_type_error" @if(isset($leave_request))  disabled  @endif >
                                                    <option value=""> </option>
                                                    @foreach($leave_types as $leave_type)
                                                        @if(isset($leave_request))
                                                            @if($leave_request->leave_type_id == $leave_type->leave_id)
                                                                <option selected max_unit="{{$leave_type->max_unit}}" leave_unit="{{$leave_type->leave_unit}}" value="{{$leave_type->leave_id}}">{{$leave_type->leave_name}}</option>
                                                            @else
                                                                <option max_unit="{{$leave_type->max_unit}}" leave_unit="{{$leave_type->leave_unit}}" value="{{$leave_type->leave_id}}">{{$leave_type->leave_name}}</option>                                                        @endif
                                                        @else
                                                            <option max_unit="{{$leave_type->max_unit}}" leave_unit="{{$leave_type->leave_unit}}" value="{{$leave_type->leave_id}}">{{$leave_type->leave_name}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                    </div>


                                </div>
                            </div>

                            <br>
                            <div class="row">

                                <div class="col-md-12 col-lg-12">

                                    <div class="form-group ">
                                        <div id="sendTD">
                                            <!--From -->
                                            <div class="col-md-6 col-lg-6">
                                                <div class="form-group ">

                                                    <label for="startDate1" class="col-md-6 col-lg-6 control-label">From</label>
                                                    <div id="startdatetimeSend1" class=" col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class='input-group date' id='startDate'>

                                                                @if(isset($leave_request))
                                                                    <input type="text" id="startDate1" class="hide date_time_picker set_all_disabled  all_date form-control required " crrent_date="{{$curr_date}}" name="date" value="{{$leave_request->leave_start}}" @if(isset($leave_request)) disabled  @endif>
                                                                    <input type="text" id="startDate1" class="hide only_date_picker set_all_disabled  all_date form-control required " crrent_date="{{$curr_date}}" name="date" value="{{$leave_request->leave_start}}" @if(isset($leave_request)) disabled  @endif>
                                                                @else
                                                                    <input type="text" id="startDate1" class="hide date_time_picker set_all_disabled all_date form-control required start_date_error err_input" crrent_date="{{$curr_date}}" name="date" value="{{$curr_date}}">
                                                                    <input type="text" id="startDate1" class="hide only_date_picker set_all_disabled all_date form-control required start_date_error err_input" crrent_date="{{$curr_date}}" name="date" value="{{$curr_date}}">
                                                                @endif

                                                                <span class="input-group-addon">
                                                          <span class="fa fa-calendar">
                                                     </span>
                                            </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {{--<div id="durationHoursSend1" class="hide  col-md-6 col-lg-6 ">
                                                        <div class="form-group">

                                                            <div class='input-group date' id='startDate'>

                                                                @if(isset($leave_request))
                                                                    <input type="text" id="hoursInput1" class="set_all_disabled form-control " crrent_date="{{$curr_date}}" name="date" value="{{$master_inventory->doc_date}}" @if(isset($master_inventory))@if( $master_inventory->status_id == 3 || $master_inventory->status_id == 6 ) disabled @endif @endif>
                                                                @else
                                                                    <input type="text" id="hoursInput1" class="set_all_disabled form-control " crrent_date="{{$curr_date}}" name="date" value="{{$curr_date}}">
                                                                @endif
                                                                <span class="input-group-addon">
                                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>--}}




                                                </div>
                                            </div>
                                            <!--To-->
                                            <div class="col-md-6 col-lg-6">
                                                <div class="form-group ">
                                                    <label for="startDate2" class="col-md-6 col-lg-6 control-label">To</label>

                                                    <div id="startdatetimeSend2" class=" col-md-6 col-lg-6">
                                                        <div class="form-group">

                                                            <div class='input-group date' id='startDate'>

                                                                @if(isset($leave_request))
                                                                    <input  type="text" id="startDate2" class="hide date_time_picker form-control " crrent_date="{{$curr_date}}" name="date" value="{{$leave_request->leave_end}}" @if(isset($leave_request))  disabled @endif  disabled>
                                                                    <input  type="text" id="startDate2" class="hide only_date_picker form-control " crrent_date="{{$curr_date}}" name="date" value="{{$leave_request->leave_end}}" @if(isset($leave_request))  disabled @endif   disabled>
                                                                @else
                                                                    <input type="text" id="startDate2" class="hide date_time_picker form-control " crrent_date="{{$curr_date}}" name="date" value="{{$curr_date}}" disabled>
                                                                    <input type="text" id="startDate2" class="hide only_date_picker form-control " crrent_date="{{$curr_date}}" name="date" value="{{$curr_date}}" disabled>
                                                                @endif

                                                                <span class="input-group-addon">
	                        							<span class="fa fa-calendar"></span>
    </span>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    {{--<div id="durationHoursSend2" class="hide  col-md-6 col-lg-6 ">
                                                        <div class="form-group">

                                                            <div class='input-group date' id='startDate'>
                                                                @if(isset($leave_request))
                                                                    <input type="text" id="hoursInput2" class="set_all_disabled form-control " crrent_date="{{$curr_date}}" name="date" value="{{$master_inventory->doc_date}}" @if(isset($master_inventory))@if( $master_inventory->status_id == 3 || $master_inventory->status_id == 6 ) disabled @endif @endif>
                                                                @else
                                                                    <input type="text" id="hoursInput2" class="set_all_disabled form-control " crrent_date="{{$curr_date}}" name="date" value="{{$curr_date}}">
                                                                @endif

                                                                    <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
        </span>
                                                            </div>
                                                        </div>
                                                    </div>--}}




                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>

                            <br>
                            <!--Duration -->
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <div class="col-md-6 col-lg-6">
                                        <div class="form-group">

                                            <label class="col-md-6 col-lg-6 control-label">Duration</label>
                                            <div class="col-md-4 col-lg-4">
                                                @if(isset($leave_request))
                                                    <input id="duration" class="set_all_disabled get_all_date_input row_data form-control required err_input duration_error"  max="" min="1"   type="number" value="{{$leave_request->leave_duration}}" @if(isset($leave_request))  disabled  @endif >
                                                @else
                                                    <input id="duration" class="set_all_disabled get_all_date_input row_data form-control required err_input duration_error"  max="" min="1"   type="number" value="">
                                                @endif

                                            </div>
                                            <div class="col-md-2 col-lg-2">

                                                <span class="hide" id="days"> Days</span>
                                                <span class="hide"  id="hours"> Hours</span>

                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                            <br>
                            <!--Duration Error -->
                            <div class="row">
                                <div class="col-md-12 col-lg-12">

                                    <div class="col-md-6 col-lg-6">
                                        <div id="errorMessage" class="alert alert-danger hide" >
                                            your limit is  only <span id="duration_l"> </span>  <span id="kind_of_l"> </span> !
                                        </div>
                                    </div>

                                    <!--Leave Type -->




                                </div>

                            </div>




                            <br>

                            <div class="row">
                            <div class="col-md-12 col-lg-12">


                                <div class="col-md-6 col-lg-6">
                                    <div class="form-group">

                                        <label class="col-md-6 col-lg-6 control-label">Description</label>
                                        <div class="col-md-6 col-lg-6">
                                            @if(isset($leave_request))
                                                <textarea id="leave_description" type="text"  class="set_all_disabled get_all_date_input form-control"   @if(isset($leave_request))  disabled  @endif > {{$leave_request->description}}</textarea>
                                            @else
                                                <textarea id="leave_description" type="text"  class="set_all_disabled get_all_date_input form-control" value=""  > </textarea>
                                            @endif

                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>

                        </div>



                    </div>
                </div>

            </div> <!-- END MAIN CONTENT -->
    </div> <!-- END MAIN -->

@endsection

@section('custom_footer')

    <script src="{{asset('js/item_name_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/tax_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/select2.min.js')}}"></script>

    <script src="{{asset('assets/scripts/bootstrap-datetimepicker.min.js')}}"></script>

            {{--------------------  active side & nav bar ------------------------}}
            <script>
                $('.side_sheets').addClass('hide');
                $('#HR').removeClass('hide');

                $("#menu li").removeClass('active');
                $('#hr_nav_bar').addClass('active');

                $('#pages ul li a').removeClass('active');
                $('#leave_request_side_bar').addClass('active');

            </script>


{{---------------------------  get department on employee change --------------------------------}}
    <script>
          $(document).on('change','#employee',function () {
             var employee_id = $(this).val();

             console.log(employee_id);
              $.ajax({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  url: '/leave_request/get_department/',
                  dataType : 'json',
                  type: 'get',
                  data: {
                      employee_id:employee_id,
                  },

                  success:function(response) {
                      console.log(response['department']);
                      $('#department_name').html(response['department']);
                  }

              });
          });
    </script>

    {{-------------------  change duration unit  -----------------------------}}
        <script>
            $(document).on('change','#leave_type',function(){

                var duration_unit = $('option:selected', this).attr('leave_unit');
                var max_unit = $('option:selected', this).attr('max_unit');
                calculate_end_date(duration_unit)
                console.log(duration_unit +' '+ max_unit);
                $('#duration').val('');
                $('#duration').attr('max',max_unit);
                $('#duration_l').html(max_unit);


                if(duration_unit == 1)
                {
                    $('#days').removeClass('hide');
                    $('#hours').addClass('hide');
                    $('#kind_of_l').html('Days');

                    $('.only_date_picker').removeClass('hide');
                    $('.date_time_picker').addClass('hide');

                }
                else{
                    $('#hours').removeClass('hide');
                    $('#days').addClass('hide');
                    $('#kind_of_l').html('Hours');

                    $('.date_time_picker').removeClass('hide');
                    $('.only_date_picker').addClass('hide');
                }


            });
        </script>

            {{------------- limit the max of input number  ----------------}}
            <script>
                $(document).on('change input','#duration',function() {
                    var max = parseInt($(this).attr('max'));
                    var min = parseInt($(this).attr('min'));

                    var duration_unit = $('#leave_type option:selected').attr('leave_unit');
                    console.log('duration_unit'+ duration_unit);
                    calculate_end_date(duration_unit);
                    $('#errorMessage').addClass('hide');
                    if ($(this).val() > max)
                    {
                        $(this).val(max);
                        $('#errorMessage').removeClass('hide');
                        calculate_end_date(duration_unit);

                    }
                    else if ($(this).val() < min)
                    {
                        $(this).val(min);
                    }
                });

                $(document).on('change','#startDate1',function() {
                    var duration_unit = $('#leave_type option:selected').attr('leave_unit');
                    calculate_end_date(duration_unit);
                });


                function calculate_end_date(day)
                {
                    if(day == 1)
                    {

                      /*  $("#startDate1").datepicker('destroy');
                        $("#startDate2").datepicker('destroy');
*/
                        /*$('#startDate1').data('DateTimePicker').destroy();*/





                        var days=$('#duration').val();
                        console.log(days);
                        if(days <= 0)
                        {
                            days = 1;
                        }
                        var days2 = $('#startDate1.only_date_picker').datepicker('getDate');
                        days2.setDate(days2.getDate() + parseInt(days)-1);
                        $('#startDate2.only_date_picker').datepicker('setDate', days2);
                    }

                    else
                            {

                           /* $("#startDate1").datepicker('destroy');
                            $("#startDate2").datepicker('destroy');*/

                                /*$('#startDate1').data('DateTimePicker').destroy();*/


                                /*
                                                            $("#startDate1").datetimepicker("destroy");
                                                            $("#startDate2").datetimepicker("destroy");
                                */




                            var hourss=$('#duration').val();
                            console.log(hourss);
                            if(hourss <= 0)
                            {
                                hourss = 1;
                            }
                            var hours2 = $('#startDate1.date_time_picker').datetimepicker('getDate');
                            hours2.setHours(hours2.getHours() + parseInt(hourss));
                            $('#startDate2.date_time_picker').datetimepicker('setDate', hours2);
                        }
                }

            </script>


        {{----------------------------  --------------------------------}}
        {{--<script>
            $(document).on('click','#save_po',function () {
               var start_date = $('#startDate1').val();
               var end_date = $('#startDate2').val();


                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/leave_request/check_date/',
                    dataType : 'json',
                    type: 'get',
                    data: {
                        start_date:start_date,
                        end_date:end_date,
                    },

                    success:function(response) {
                        console.log(response['holiday']);

                    }

                });
            });
        </script>--}}
        {{---------------------------  save data  ------------------------------------------}}
            <script>


                $('.alert-autocloseable-success').hide();

                $(document).on('click','#save_po',function () {
                    var status_po = $('#set_po_id').attr('current_po_status');
                    add_new_po_all (status_po);

                });
                $(document).on('click','#sendto_approved_btn',function () {

                    //add_new_po_all ('to_approve_po');
                    var status_po = $('#set_po_id').attr('current_po_status');
                    if(status_po != 'approved_po')
                    {
                        add_new_po_all ('to_approve_po');
                    }

                    else{
                        add_new_po_all ('approved_po');
                    }
                });
                $(document).on('click','#approved_btn',function () {

                    add_new_po_all ('approved_po');


                });
                $(document).on('click','#refused_btn',function () {

                    add_new_po_all ('refused');


                });



                function add_new_po_all (status_click)
                {


                        $('#save_po').attr('disabled','disabled');
                        $('#sendto_approved_btn').attr('disabled','disabled');
                        $('#approved_btn').attr('disabled','disabled');


                        var status = status_click;
                        console.log(status);


                        var duration_unit = $('#leave_type option:selected').attr('leave_unit');
                        var start_date = $('#startDate1:not(.hide)').val();
                        var end_date = $('#startDate2:not(.hide)').val();
                        var leave_description = $('#leave_description').val();
                        var requested_date = $('#requested_date').val();
                        var employee = $('#employee').val();
                        var leave_type = $('#leave_type').val();
                        var duration = $('#duration').val();



                        var current_user_id= $('#set_po_id').attr('current_id');
                        var current_po_id = $('#set_po_id').attr('current_po_id');

                        console.log('start_date'+start_date+'end_date'+end_date+'leave_description'+leave_description+'requested_date'+requested_date+'employee'+employee+'leave_type'+leave_type+'duration'+duration+'current_user_id'+current_user_id+'current_po_id'+current_po_id+'status'+status)

                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url: '/leave_request/save/',
                            dataType : 'json',
                            type: 'get',
                            data: {


                                start_date:start_date,
                                end_date:end_date,
                                leave_description:leave_description,
                                requested_date:requested_date,
                                employee:employee,
                                leave_type:leave_type,
                                duration:duration,
                                duration_unit:duration_unit,

                                status:status,
                                current_po_id:current_po_id,
                                cur_user_id:current_user_id,

                            },

                            success:function(response) {

                                console.log(response);
                                if(response['error'])
                                {
                                    $('.err_input').removeClass('red-border');
                                    $('.err_input').parent().find('.select2').removeClass('red-border');
                                    $('.errorMessage1').show();
                                    $.each(response['error'], function (key, val)
                                    {
                                        //alert(key + val);

                                        console.log(key+'_error');

                                        $("."+key+'_error').addClass('red-border');
                                        $('.'+key+'_error').parent().find('.select2').addClass('red-border');

                                        //console.log(val[0]);

                                    })
                                    $('#save_po').removeAttr('disabled');

                                    $('#sendto_approved_btn').removeAttr('disabled');
                                    $('#approved_btn').removeAttr('disabled');
                                }

                                else if (response['inturapt'])
                                {
                                    console.log(response['inturapt']);
                                    $('#save_po').removeAttr('disabled');

                                    $('#sendto_approved_btn').removeAttr('disabled');
                                    $('#approved_btn').removeAttr('disabled');
                                    $('.errorMessage1').html("something wrong");



                                }
                                 else if(response['holiday'])
                                 {
                                     console.log(response['holiday']);
                                     $('#save_po').removeAttr('disabled');

                                     $('#sendto_approved_btn').removeAttr('disabled');
                                     $('#approved_btn').removeAttr('disabled');
                                     $('.errorMessage1').html("something wrong");
                                 }
                                else
                                {
                                    var status_po = $('#set_po_id').attr('current_po_status');
                                    if(status_po != 'approved_po' && status_click != 'refused')
                                    {
                                        $('#set_po_id').attr('current_po_status',status_click);

                                    }
                                    else if(status_click == 'refused')
                                    {
                                        $('#set_po_id').attr('current_po_status',status_click);

                                    }

                                    $('.errorMessage1').hide();
                                    $('.err_input').removeClass('red-border');
                                    $('.err_input').parent().find('.select2').removeClass('red-border');

                                    $('.breadcrumb').removeClass('po-active');
                                    $('#'+response['status']).addClass('po-active');
                                    $('#current_choosed_status').attr('coosed_status',response['status']);
                                    $('#save_po').text("Edit");
                                    $('#save_po').attr('id','edit_po');
                                    $('#set_po_id').attr('current_po_id',response['current_po_id']);
                                    $('.set_all_disabled').prop('disabled',true);
                                    $('#creat_po').removeClass('hide');

                                    $('#po_more').removeClass('hide');
                                    $('#delete_po').parent('li').addClass('hide');

                                    if($('#set_po_id').attr('current_po_status') == 'draft_po')
                                    {
                                        $('#delete_po').parent('li').removeClass('hide');
                                    }
                                    if($('#set_po_id').attr('current_po_status') == 'approved_po')
                                    {
                                        $('#approved_date').val(response['approv_date']['date']);
                                        $('#refused').removeClass('hide');
                                        $('#refused_btn').removeClass('hide');
                                    }

                                    if($('#set_po_id').attr('current_po_status') == 'to_approve_po')
                                    {

                                        $('#refused').removeClass('hide');
                                        $('#refused_btn').removeClass('hide');
                                    }



                                    $('#edit_po').removeAttr('disabled');


                                    //po-active
                                    console.log(response);

                                    {{----SUCCESS ON SAVE -------}}
                                    $('#autoclosable-btn-success').prop("disabled", true);
                                    $('.alert-autocloseable-success').show('slow');

                                    $('.alert-autocloseable-success').delay(3000).fadeOut( "slow", function() {
                                        // Animation complete.
                                        $('#autoclosable-btn-success').prop("disabled", false);
                                    });
                                    {{----SUCCESS ON SAVE -------}}
                                }

                            }

                        });




                }
            </script>

            {{--------------------------  edit inventory  --------------------------------}}
            <script>
                $(document).on('click','#edit_po',function () {
                    $('#edit_po').text("Save");
                    $('#edit_po').attr('id','save_po');

                    $('#refused_btn').prop('disabled',false);
                    $('#refused').removeClass('hide');
                    if($('#set_po_id').attr('current_po_status') == 'draft_po')
                    {
                        $('#refused_btn').prop('disabled',true);
                        $('#refused').addClass('hide');
                    }
                    if($('#set_po_id').attr('current_po_status') != 'approved_po' && $('#set_po_id').attr('current_po_status') != 'refused')
                    {
                        $('.set_all_disabled').prop('disabled',false);
                        var rowCount = $('#op_table tr').length;
                        console.log('rowCount'+ rowCount);

                    }
                    if($('#set_po_id').attr('current_po_status') == 'refused'){
                        $('#refused_btn').prop('disabled',true);
                    }


                    console.log($('#set_po_id').attr('current_po_id'));


                    $('#delete_po').parent('li').addClass('hide');


                    $('#create_invoice').addClass('hide');
                });
            </script>

            <script>
                $(document).on('click','#creat_po',function () {
                    /*$('#edit_po').prop('disabled',false);
                    $('#edit_po').text("Save");
                    $('#edit_po').attr('id','save_po');
                    $('#save_po').prop('disabled',false);

                    $('.set_all_disabled').prop('disabled',false);

                    var crrent_date = $('#datetime1').attr('crrent_date');
                    $('#datetime1').val(crrent_date);
                    $('#datetime2').val('');

                    $('#set_po_id').attr('current_po_id',"");
                    $('#set_po_id').attr('current_po_status',"draft_po");
                    $('#creat_po').addClass('hide');
                    $('#po_more').addClass('hide');


                    $('#delete_po').parent('li').addClass('hide');

                    /!*$(".set_all_disabled").val("");*!/
                    $('.get_all_selectors').val('');
                    //$('.get_all_date_input').val('');

                    $(".get_all_selectors option[value='']").attr('selected', true);
                    $('.get_all_date_input').val('');
                    $('.select2-selection__rendered').html('');

                    $('#refused_btn').addClass('hide');
                    $('#refused').addClass('hide');

                    $('.breadcrumb').removeClass('po-active');*/
                    window.location.href = '/leave_request';

                });
            </script>




            {{---- dublicate po ---}}
            <script>
                $(document).on('click','#dublicate_po',function(){

                    $('#set_po_id').attr('current_po_id',"");
                    $('#set_po_id').attr('current_po_status',"draft_po");
                    $('#set_po_id').attr('closed_or_not',0);

                    $('.set_all_disabled').prop('disabled',false);

                    $('#refused_btn').addClass('hide');
                    $('#refused').addClass('hide');

                    $('#creat_po').addClass('hide');
                    $('#po_more').addClass('hide');

                    $('.breadcrumb').removeClass('po-active');

                    $('#edit_po').prop('disabled',false);
                    $('#edit_po').text("Save");
                    $('#edit_po').attr('id','save_po');
                    $('#save_po').prop('disabled',false);
                });
            </script>



        {{---------  delete request ---------------}}
            <script>
                $(document).on('click','#delete_po',function(){
                    var cur_po_id = $('#set_po_id').attr('current_po_id');

                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: '/leave_request/delete/',
                        dataType : 'json',
                        type: 'get',
                        data: {
                            cur_po_id:cur_po_id,
                        },

                        success:function(response) {

                            window.location.href = '/leave_request';
                        }

                    });

                });
            </script>
{{-------------------------------  ready function ----------------------------------------------}}
            <script>
                $(document).ready(function () {
                    $(".select_2_enable").select2();

                    if($('#set_po_id').attr('current_po_status') == 'approved_po' || $('#set_po_id').attr('current_po_status') == 'refused')
                    {
                        $('.set_all_disabled').prop('disabled',true);
                    }

                    $(".only_date_picker").datepicker({format: 'yyyy-mm-dd',autoclose: true});
                    $(".date_time_picker").datetimepicker({format: 'yyyy-mm-dd hh:mm',autoclose: true});

                   /* $("#startDate1").datetimepicker({format: 'yyyy-mm-dd hh:mm'}).datetimepicker("setDate", new Date());
                    $("#startDate2").datetimepicker({format: 'yyyy-mm-dd hh:mm'}).datetimepicker("setDate", new Date());
*/
                });
            </script>

            @if(isset($leave_request))
            <script>
                var duration_unit = $('#leave_type option:selected').attr('leave_unit');
                var max_unit = $('#leave_type option:selected').attr('max_unit');
                /*calculate_end_date(duration_unit)*/
                console.log(duration_unit +' '+ max_unit);
                $('#duration').attr('max',max_unit);
                $('#duration_l').html(max_unit);


                if(duration_unit == 1)
                {
                    $('#days').removeClass('hide');
                    $('#hours').addClass('hide');
                    $('#kind_of_l').html('Days');

                    $('.only_date_picker').removeClass('hide');
                    $('.date_time_picker').addClass('hide');

                }
                else{
                    $('#hours').removeClass('hide');
                    $('#days').addClass('hide');
                    $('#kind_of_l').html('Hours');

                    $('.date_time_picker').removeClass('hide');
                    $('.only_date_picker').addClass('hide');
                }
            </script>
            @endif

            {{-----------   on leave page -------------------}}
            <script type='text/javascript'>

                $(window).bind('beforeunload', function(){
                    if($('#save_po').length){
                        return 'Are you sure you want to leave?';
                    }

                });


            </script>

            <script>

                $(document).on('click', '#print', function () {


                    //  window.location.href = 'get_po2/276'

                    var id = $('#set_po_id').attr('current_po_id');
                    window.open('/get_leaveRequest_rpt/' + id + '', '_blank');


                });


            </script>
@endsection