
@extends('layouts.main_app')

@section('main_content')
<style>
    input.col-md-4.text-center {

        border:0;
        background-color: #fff;
    }
    .error{
        border: 1px solid red  !important;
        border-left: 1px double red !important;
        border-top: 2px double red !important;
    }


    .red-border{
        border: 1px solid red  !important;
    }


</style>
<link href="{{asset('css/select2.min.css')}}" rel="stylesheet" />

    <div class="main">


        <!-- Navbar-content -->
        <nav class="navbar navbar-default navbar2">

            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                    <!--breadcrumbs-->
                    <div class="row">

                        <div class="col-md-10 col-lg-10">
                        <ol class="breadcrumb breadcrumb_nav">
                            <li><a href="/show_users">Users</a></li>
                            <li class="active"></li>
                        </ol>
                        </div>


                        @if(isset($user))
                            <div class="col-md-2 col-lg-2 col-sm-4 col-xs-4">
                                @if(@\App\User::where('id','<',$user->id)->orderBy('id','desc')->first()->id)
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <a href="/add_account/{{@\App\User::where('id','<',$user->id)->orderBy('id','desc')->first()->id}}"><span
                                                    class="glyphicon glyphicon-arrow-left"></span>
                                        </a>
                                    </div>
                                @endif
                                @if(@\App\User::where('id','>',$user->id)->orderBy('id','asc')->first()->id)
                                    <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                                        <a href="/add_account/{{@\App\User::where('id','>',$user->id)->orderBy('id','asc')->first()->id}}">
                                    <span class="glyphicon glyphicon-arrow-right">
                                </span>
                                        </a>
                                    </div>
                                @endif
                            </div>
                        @endif



                    </div>   <!--End breadcrumbs-->




                    @if(isset($user))
                        <div id="set_po_id"  current_po_id="{{$user->id}}"  current_id="{{Auth::user()->id}}"></div>
                    @else
                        <div id="set_po_id"  current_po_id=""  current_id="{{Auth::user()->id}}"></div>
                    @endif

                    {{--@if(isset($user))--}}
                    <div class="row">


                        <div class="col-md-4 buttons">
                            @if(isset($user))
                                <button id="edit_user" type="button" class="btn btn-danger " >Edit</button>
                                <button type="button" class="btn" id="creat_po" >Create</button>
                            @else
                                <button id="save_user" type="button" class="btn btn-danger ">Save</button>
                                <button type="button" class="btn hide" id="creat_po" >Create</button>
                            @endif
                        </div>





                        {{--<div class="col-md-4">

                            <form class="navbar-form navbar-right" >
                                <div class="input-group form-group">
                                    <input type="text" value="" class="form-control" placeholder="Search ...">
                                </div>
                            </form>
                        </div>--}}
                    </div> <!--collapse -->
                </div>
            </div>
        </nav>
        <!-- End Navbar-content -->
        <!-- Success messages -->
        <div class="alert alert-success alert-autocloseable-success" style="display:none;position:absolute;" >
            Your PO Saved Successfully!

        </div>
        <!--end Success messages -->

        <!-- failuer messages -->
        <div class="alert alert-danger errorMessage1" style="display:none;position:absolute;" >
            Your should fill all fields!

        </div>
        <!--end failuer messages -->


        <!-- failuer messages -->
        <div class="alert alert-danger errorMessage2" style="display:none;position:absolute;" >
            PLease fill table!

        </div>
        <!--end failuer messages -->

        <!-- MAIN CONTENT -->
        <div class="main-content">

            <div class="container-fluid">

                <!-- INPUTS -->
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Permission Users</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">

                                    <label class="col-md-6 control-label" >Employee</label>



                                    <div class="col-md-6">
                                        <select id="employee" name="employee" class="get_all_selectors set_all_disabled select_2_enable form-control required" @if(isset($user))disabled  @endif>
                                            <option value=""> </option>
                                            @foreach($emploee as $s_emploee)
                                                @if(isset($selected_employee))
                                                    @if($selected_employee->employee_id == $s_emploee->employee_id)
                                                        <option selected value="{{$s_emploee->employee_id}}">{{$s_emploee->employee_name}}</option>
                                                    @else
                                                        <option value="{{$s_emploee->employee_id}}">{{$s_emploee->employee_name}}</option>
                                                    @endif
                                                @else
                                                    @if($s_emploee->employee_user_id == Auth::user()->id)
                                                        <option selected value="{{$s_emploee->employee_id}}">{{$s_emploee->employee_name}}</option>
                                                    @else
                                                        <option value="{{$s_emploee->employee_id}}">{{$s_emploee->employee_name}}</option>
                                                    @endif
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>


                        <div class="row">


                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                <label class="col-md-6 col-lg-6 control-label">Login name</label>

                                    <div class="col-md-6 col-lg-6">
                                @if(isset($user))
                                    <input current_user="{{$user->id}}" id="name" type="text" style="background-color: #fff;" class="col-md-6 text-center form-control" name="name" @if(isset($user->name))value="{{$user->name}}"@endif required disabled>
                                @else
                                    <input current_user="" id="name" type="text" style="background-color: #fff;" class="col-md-6 text-center form-control" name="name" value="">
                                @endif
                                    </div>
                                </div>
                            </div>




                        </div>


                        <br>

                        <div class="row">
                            <div class="col-md-12">

                                <div id="name_error" class="alert alert-danger errorMessage hide ">
                                    <strong >Wrong Name!</strong>
                                </div>
                            </div>

                        </div>

                        <div class="row">

                            <div class="col-md-6 col-lg-6 ">
                                <div class="form-group">


                                <label class="col-md-6 col-lg-6 control-label">Password</label>

                                <div class="col-md-6 col-lg-6">
                                <input style="background-color: #fff;" class="col-md-2 text-center form-control" id="password" name="password" type="password" value="" @if(isset($user))disabled @endif />
                              </div>
                                </div>

                            </div>

                        </div>

                        <br>

                        <div class="row">
                            <div class="col-md-12">

                                <div id="password_error" class="alert alert-danger errorMessage hide ">
                                    <strong>Wrong Password!</strong>
                                </div>

                            </div>

                        </div>

                        <div class="row">

                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                <label class="col-md-6 col-lg-6 control-label">Confirm Password</label>

                                    <div class="col-md-6 col-lg-6">
                                <input id="password-confirm" style="background-color: #fff;" class="col-md-2 text-center form-control" name="password_confirmation"  type="password" value="" @if(isset($user))disabled @endif />
                                    </div>
                                  </div>
                            </div>
                        </div>


                        <br>

                        <div class="row">
                            <div class="col-md-12">
                                <div id="password_confirmation_error" class="alert alert-danger errorMessage hide ">
                                    <strong>Ops!</strong> The password you enterd is not the same
                                </div>

                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-6">

                                <label class="errorMessage hide">you password wrong  </label>

                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-6">


                                <h3>Modules:</h3>
                                @if(isset($moduls))
                                @foreach($moduls as $module)
                                <label id="{{$module->id}}" class="col-md-6 control-label ">{{$module->name}}</label>

                                <div class="col-md-6">
                                    <select {{--id="role" --}} class="roles form-control select_2_enable" @if(isset($user))disabled @endif>
                                        <option value="" selected>

                                        </option>
                                        @foreach($roles as $role)
                                            @if(isset($user))
                                                @if( $role->module_id == $module->id )
                                                    @if(\Illuminate\Support\Facades\DB::table('userss')
                                                        ->join('model_has_roles','userss.id','=','model_has_roles.model_id')
                                                        ->join('roles','model_has_roles.role_id','=','roles.id')
                                                        ->where('userss.id','=',$user->id)
                                                        ->where('roles.id','=',$role->id)
                                                        ->orderBy('roles.id','asc')
                                                        ->exists())
                                                    <option value="{{$role->name}}" selected>
                                                        {{$role->name}}
                                                    </option>
                                                        @else
                                                        <option value="{{$role->name}}">
                                                            {{$role->name}}
                                                        </option>
                                                        @endif
                                                    @endif
                                            @else
                                                @if( $role->module_id == $module->id )
                                                <option value="{{$role->name}}">
                                                    {{$role->name}}
                                                </option>
                                                @endif
                                            @endif
                                        @endforeach
                                    </select>

                                </div>

                                <br>
                                    <br>
                                @endforeach
                                @endif

                            </div>
                           {{-- @if(isset($selected_roles))
                            @foreach($selected_roles as $selected_role)
                                {{$selected_role->id}}
                            @endforeach @endif--}}




                        </div>
                        <br>


                        <div class="row">
                            <div class="col-md-12">

                                <div id="email_error" class="alert alert-danger errorMessage hide ">
                                    <strong >Wrong Name!</strong>
                                </div>
                            </div>

                        </div>



                        </div>

                        <br>


                    </div>

                </div>
            {{--@endif--}}
            </div>
            <!-- END INPUTS -->





        </div>
    </div>



@endsection

@section('custom_footer')

    <script src="{{asset('js/select2.min.js')}}"></script>
    <script>$(".select_2_enable").select2()</script>
    <script>
        $(document).on('click','#edit_user',function(){

            $(this).text("Save");
            $(this).attr('id','save_user');
            $("input[type=text]").attr('disabled', false).css({
                'border': '#eaeaea solid 1px',

                'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                'border-radius':'2px',
                'background-color':'#fff',


            }).focus();

            $("input[type=password]").attr('disabled',false).css({
                'border': '#eaeaea solid 1px',

                'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                'border-radius':'2px',
                'background-color':'#fff',

            });

            $("input[type=email]").attr('disabled',false).css({
                'border': '#eaeaea solid 1px',

                'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                'border-radius':'2px',
                'background-color':'#fff',

            });



            $("select").removeAttr('disabled');
            $("input[type=radio]").attr('disabled', false);
            $("input[type=password]").attr('disabled', false);
            $("input[type=email]").attr('disabled', false);
            $('#creat_po').addClass('hide');

        });


    </script>
    <script>
        $(document).on('click','#save_user',function(){

            var employee = $('#employee').val();
            var user_id = $('#name').attr('current_user');
            var name = $('#name').val();
            var password = $('#password').val();
            var email = $('#email').val();
            var password_confirmation = $('#password-confirm').val();
            /*var role = $('#role').val();*/
            var roles = $('.roles');
            var roles_data = [];

            var current_po_id = $('#set_po_id').attr('current_po_id');

            $('#save_user').attr('disabled', 'disabled');
            roles.each(function(){
                var vall = $(this).val();
                if(vall != null && vall !="")
                {
                    roles_data.push(vall);
                }

            });


            console.log(user_id,name,email,password,password_confirmation,roles_data,current_po_id);

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/create_new_account/',
                dataType : 'json',
                type: 'get',
                data: {

                    user_id:user_id,
                    name:name,
                    email:email,
                    password:password,
                    password_confirmation:password_confirmation,
                    /*role:role,*/
                    roles_data:roles_data,
                    current_po_id:current_po_id,
                    employee:employee


                },

                success:function(response) {

                    /*console.log(response);*/
                    if(response['error'])
                    {
                        $('.err_input').removeClass('red-border');
                        $('.err_input').parent().find('.select2').removeClass('red-border');

                        $('.errorMessage1').show();
                        $.each(response['error'], function (key, val) {
                            //alert(key + val);

                            console.log(key + '_error');
                            $("." + key + '_error').addClass('red-border');
                            $('.' + key + '_error').parent().find('.select2').addClass('red-border');

                            //console.log(val[0]);

                        })
                        $('#save_user').removeAttr('disabled');

                    }

                    else
                    {
                        $('.errorMessage1').hide();
                        $('.err_input').removeClass('red-border');
                        $('.err_input').parent().find('.select2').removeClass('red-border');

                        $('#save_user').text("Edit");
                        $('#save_user').attr('id','edit_user');

                        $('#creat_po').removeClass('hide');
                        $('#set_po_id').attr('current_po_id',response['current_po_id']);

                        $("input[type=text]").attr('disabled', true).css({
                            'border': '#eaeaea solid 1px',

                            'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                            'border-radius':'2px',
                            'background-color':'#fff',
                        });
                        $("input[type=password]").attr('disabled', true).css({
                            'border': '#eaeaea solid 1px',

                            'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                            'border-radius':'2px',
                            'background-color':'#fff',
                        });
                        $("input[type=email]").attr('disabled', true).css({
                            'border': '#eaeaea solid 1px',

                            'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                            'border-radius':'2px',
                            'background-color':'#fff',
                        });


                        $("select").prop("disabled", true);
                        $("input[type=radio]").attr('disabled', true);
                        $("input[type=password]").attr('disabled', true);
                        $("input[type=email]").attr('disabled', true);

                        $('#edit_user').removeAttr('disabled');
                        notification('glyphicon glyphicon-ok-sign','Congratulations!','Student Saved Successfully!','success');
                    }

                }

            });
        });

    </script>

    {{---------------------------  create btn  ------------------------------------------}}
    <script>
        $(document).on('click','#creat_po',function () {

            window.location.href = '/add_account';
        });
    </script>
    {{--------------------  active side & nav bar ------------------------}}
    <script>
        $('.side_sheets').addClass('hide');
        $('#system').removeClass('hide');

        $("#menu li").removeClass('active');
        $('#System_nav_bar').addClass('active');

        $('#pages ul li a').removeClass('active');
        $('#system_side_bar').addClass('active');

    </script>


    {{-----------   on leave page -------------------}}
    <script type='text/javascript'>

        $(window).bind('beforeunload', function () {
            if ($('#save_user').length) {
                return 'Are you sure you want to leave?';
            }

        });


    </script>
@endsection
