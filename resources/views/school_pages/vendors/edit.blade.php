@extends('layouts.main_app')

@section('main_content')
    <link href="{{asset('css/fastselect.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/select2.min.css')}}" rel="stylesheet" />

    <style>
        input.col-md-4.text-center {

            border:0;
            background-color: #fff;
        }



        .collapse > .row>.col-md-4>.btn, .collapse > .row>.col-md-4>.dropdown>.btn {
            margin-top: 0;

        }
        child_name
        input.invalid {
            border-color: red;
        }


        .third-nav{
            background-color:#eeeeee;
        }


        .main-content {
            padding: 34px 0 28px 5px;        }


        .myselect2{
            width:171px;
        }

        span.select2.select2-container.select2-container--default{
            width: 100%!important;
        }
        tbody th, tbody td {
            text-align: center;
        }
    </style>
    <div class="main">
        <!-- Navbar-content -->
        <nav class="navbar navbar-default navbar2">

            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                    <!--breadcrumbs-->
                    <div class="row">

                        <div class="col-md-10 col-lg-10">

                        <ol class="breadcrumb breadcrumb_nav">
                            <li><a href="/vendors">Vendors</a></li>
                            <li class="active" id="second">
                                @if(isset($vendor))
                                    @if($vendor->VendorID != 0)
                                        {{$vendor->VendorID}}
                                    @else
                                        Draft
                                    @endif
                                @endif
                            </li>
                        </ol>
                        </div>

                        <div class="col-md-2 col-lg-2">
                            @if(isset($vendor))
                                <div class="col-md-2 col-lg-2 col-sm-4 col-xs-4">
                                    @if(@\App\Vendor::where('VendorID','<',$vendor->VendorID)->orderBy('VendorID','desc')->first()->VendorID)
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <a href="/vendors/{{@\App\Vendor::where('VendorID','<',$vendor->VendorID)->orderBy('VendorID','desc')->first()->VendorID}}/edit"><span
                                                        class="glyphicon glyphicon-arrow-left"></span>
                                            </a>
                                        </div>
                                    @endif
                                    @if(@\App\Vendor::where('VendorID','>',$vendor->VendorID)->orderBy('VendorID','asc')->first()->VendorID)
                                        <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                                            <a href="/vendors/{{@\App\Vendor::where('VendorID','>',$vendor->VendorID)->orderBy('VendorID','asc')->first()->VendorID}}/edit">
                                    <span class="glyphicon glyphicon-arrow-right">
                                </span>
                                            </a>
                                        </div>
                                    @endif
                                </div>
                            @endif





                        </div>



                        </div>


                    <!--End breadcrumbs-->
                    <div class="row">
                        <div class="col-md-4 col-lg-4 buttons">
                            <button id="edit_work"  type="button" class="btn btn-danger" >Edit</button>
                            <button type="button" class="btn" id="create_work" >Create</button>
                        </div>

                        <div class="col-md-4 col-lg-4">
                            <div class="dropdown">
                                <button class="btn btn-primary  dropdown-toggle" type="button" data-toggle="dropdown" id="more"  hidden>More
                                    <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li>

                                        <a data-toggle="modal" data-target="#delete{{ $vendor->VendorID }}"
                                           >delete</a>


                                    </li>
                                    <li><a href="#">Export</a></li>
                                    <li><a href="#">Duplicate</a></li>
                                    <li><a href="#" id="print">Print</a></li>

                                </ul>
                                <div id="delete{{ $vendor->VendorID }}" class="modal fade" role="dialog">
                                    <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Delete vendor</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>{{$vendor->VendorID. ' ' .$vendor->VendorName. ' ' .$vendor->VendorCode}}</p>
                                            </div>
                                            <div class="modal-footer">
                                                {!! Form::open(['method'=>'DELETE','route'=>['vendors.destroy',$vendor->VendorID]]) !!}
                                                <button type="button" class="btn btn-default btn-flat"
                                                        data-dismiss="modal">close
                                                </button>
                                                <button type="submit"
                                                        class="btn btn-danger btn-flat">delete
                                                </button>
                                                {!! Form::close() !!}
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div> <!--collapse -->

                </div><!--collapse -->

            </div>
        </nav>
        <!-- End Navbar-content -->
        <!-- MAIN CONTENT -->






        <div class="main-content">



            <div class="container-fluid">

                <!-- INPUTS -->
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Vendors</h3>
                    </div>
                    <div class="panel-body">

            {!! Form::open(['method'=>'put' ,'id'=>'theform', 'action'=> ['VendorsController@update',$vendor->VendorID],'file'=>'true' ,'enctype'=>'multipart/form-data']) !!}


            <div class="row">



                <div class="col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="vendor_code" class="col-md-6 col-lg-6 control-label">Vendor Code</label>

                        <div class=" col-md-6 col-lg-6">

                            <div class="form-group">
                                <input  class="form-control required set_all_disabled" name="vendor_code" type="text" id="vendor_code" value="{{$vendor->VendorCode}}" disabled required>

                            </div>

                        </div>

                    </div>

                </div>

            </div>
            <br>
            <div class="row">



                <div class="col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="vendor_name" class="col-md-6 col-lg-6 control-label">Vendor Name </label>

                        <div class=" col-md-6 col-lg-6">

                            <div class="form-group">
                                <input  class="form-control required set_all_disabled"  type="text" id="vendor_name" name="vendor_name" value="{{$vendor->VendorName}}" disabled required>

                            </div>

                        </div>

                    </div>

                </div>

            </div>
            <br>
            <div class="row">

                <div class="col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="arabic_vendor_name" class="col-md-6 col-lg-6 control-label">Arabic Vendor Name</label>

                        <div class=" col-md-6 col-lg-6">

                            <div class="form-group">
                                <input  class="form-control required set_all_disabled"   type="text" id="arabic_vendor_name" name="arabic_vendor_name" value="{{$vendor->VendorNameAR}}" disabled  required>

                            </div>

                        </div>

                    </div>

                </div>

            </div>
            <br>
            <div class="row">



                <div class="col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="usr" class="col-md-6 col-lg-6 control-label">Address </label>

                        <div class=" col-md-6 col-lg-6">

                            <div class="form-group">
                                <textarea class="form-control set_all_disabled" rows="3" id="address" name="address" value="{{$vendor->VendorAddress}}" disabled></textarea>


                            </div>

                        </div>

                    </div>

                </div>

            </div>
            <br>
            <div class="row">



                <div class="col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="usr" class="col-md-6 col-lg-6 control-label">Arabic Address</label>

                        <div class=" col-md-6 col-lg-6">

                            <div class="form-group">
                                <textarea class="form-control set_all_disabled" rows="3" id="address_ar" name="address_ar" value="{{$vendor->VendorAddressAR}}" disabled></textarea>


                            </div>

                        </div>

                    </div>

                </div>

            </div>
            <br>
            <div class="row">



                <div class="col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="url" class="col-md-6 col-lg-6 control-label">Website</label>

                        <div class=" col-md-6 col-lg-6">

                            <div class="form-group">
                                <input type="url" name="url" class="form-control set_all_disabled" id="website" name="website"
                                       pattern="https://.*"  placeholder="https://.*"  value="{{$vendor->website}}" disabled/>

                            </div>

                        </div>

                    </div>

                </div>

            </div>
            <br>
            <div class="row">



                <div class="col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="url" class="col-md-6 col-lg-6 control-label">E-mail </label>

                        <div class=" col-md-6 col-lg-6">

                            <div class="form-group">
                                <input type="email" class="form-control set_all_disabled"  id="email" name="email" value="{{$vendor->Email}}" disabled>


                            </div>

                        </div>

                    </div>

                </div>

            </div>
            <br>
            <div class="row">



                <div class="col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="contact_school" class="col-md-6 col-lg-6 control-label">Contact School Person </label>

                        <div class=" col-md-6 col-lg-6">

                            <div class="form-group">
                                {!! Form::select('contact_school_person',[''=>'Choose Option']+ $employees,$vendor->ContactSchoolPerson,['class'=>'form-control myselect1 set_all_disabled','id'=>'contact_school','disabled'])  !!}

                            </div>

                        </div>

                    </div>

                </div>

            </div>
            <br>
            <div class="row">



                <div class="col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="contact_vendor_person" class="col-md-6 col-lg-6 control-label">Contact Vendor Person</label>

                        <div class=" col-md-6 col-lg-6">

                            <div class="form-group">
                                <input  class="form-control set_all_disabled"  type="text" id="contact_vendor_person" name="contact_vendor_person" value="{{$vendor->ContactVendorPerson}}" disabled>

                            </div>

                        </div>

                    </div>

                </div>

            </div>
            <br>
            <div class="row">



                <div class="col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="phonenum" class="col-md-6 col-lg-6 control-label">Phone 1 </label>

                        <div class=" col-md-6 col-lg-6">

                            <div class="form-group">
                                <input id="phonenum" name="phonenum" type="number" class="form-control set_all_disabled" placeholder="0000-000-0000" value="{{$vendor->Phone1}}" disabled >
                            </div>

                        </div>

                    </div>

                </div>

            </div>
            <br>
            <div class="row">



                <div class="col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="phonenum2" class="col-md-6 col-lg-6 control-label">Phone 2 </label>

                        <div class=" col-md-6 col-lg-6">

                            <div class="form-group">
                                <input id="phonenum2" name="phonenum2" type="number" class="form-control set_all_disabled"  placeholder="0000-000-0000" value="{{$vendor->Phone2}}" disabled >
                            </div>

                        </div>

                    </div>

                </div>

            </div>
            <br>
            <div class="row">



                <div class="col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="tax_file_number" class="col-md-6 col-lg-6 control-label">Tax File Number</label>

                        <div class=" col-md-6 col-lg-6">

                            <div class="form-group">
                                <input  class="form-control set_all_disabled"  type="text" id="tax_file_number" name="tax_file_number"  value="{{$vendor->TaxFileNumber}}" disabled>

                            </div>

                        </div>

                    </div>

                </div>

            </div>
            <br>
            <div class="row">



                <div class="col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="tax_office_name" class="col-md-6 col-lg-6 control-label">Tax Office Name</label>

                        <div class=" col-md-6 col-lg-6">

                            <div class="form-group">
                                <input  class="form-control set_all_disabled"  type="text" id="tax_office_name" name="tax_office_name" value="{{$vendor->TaxOfficeName}}" disabled>

                            </div>

                        </div>

                    </div>

                </div>

            </div>
            <br>
            <div class="row">



                <div class="col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="tax_office_code" class="col-md-6 col-lg-6 control-label">Tax Office Code</label>

                        <div class=" col-md-6 col-lg-6">

                            <div class="form-group">
                                <input  class="form-control set_all_disabled"  type="text" id="tax_office_code" name="tax_office_code" value="{{$vendor->TaxOfficeCode}}" disabled>

                            </div>

                        </div>

                    </div>

                </div>

            </div>
            <br>
            <div class="row">



                <div class="col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="reg_code" class="col-md-6 col-lg-6 control-label">Commercial Regesteration Code</label>

                        <div class=" col-md-6 col-lg-6">

                            <div class="form-group">
                                <input  class="form-control set_all_disabled"  type="text" id="reg_code" name="reg_code" value="{{$vendor->CommercialRegNo}}" disabled >

                            </div>

                        </div>

                    </div>

                </div>

            </div>
            <br>
            <div class="row">



                <div class="col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="tax_reg_number" class="col-md-6 col-lg-6 control-label">Tax Regesteration Number</label>

                        <div class=" col-md-6 col-lg-6">

                            <div class="form-group">
                                <input  class="form-control set_all_disabled"  type="text" id="tax_reg_number" name="tax_reg_number" value="{{$vendor->TaxRegNumber}}" disabled >

                            </div>

                        </div>

                    </div>

                </div>

            </div>
            <br>
            <div class="row">



                <div class="col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="tax_reg_number" class="col-md-6 col-lg-6 control-label">Vat Regesteration Number</label>

                        <div class=" col-md-6 col-lg-6">

                            <div class="form-group">
                                <input  class="form-control set_all_disabled"  type="text" id="vat_reg_number" name="vat_reg_number" value="{{$vendor->SalesTax}}" disabled>

                            </div>

                        </div>

                    </div>

                </div>

            </div>
            <br>
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="acc_name" class="col-md-6 col-lg-6 control-label">Currency</label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">

                                            {!! Form::select('currancy',[''=>'Choose Option'] +$currency ,$vendor->CurrencyID,['class'=>'form-control myselect4 required set_all_disabled','id'=>'currancy','disabled' , 'required']) !!}
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                   <br>
                        <div class="row">



                <div class="col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="acc_name" class="col-md-6 col-lg-6 control-label">Account Name</label>

                        <div class=" col-md-6 col-lg-6">

                            <div class="form-group">

                                {!! Form::select('acc_name',[''=>'Choose Option']+ $accounts,$vendor->account_id,['class'=>'form-control myselect2 required set_all_disabled','id'=>'acc_name','disabled' , 'required']) !!}
                            </div>

                        </div>

                    </div>

                </div>

            </div>

            <div class = 'form-group hidden'>
                {!! Form::submit('create user',['class'=>'btn btn-primary']) !!}
            </div>
            {!! Form::close() !!}
        </div>
                </div>
            </div>
        </div>
        <!-- END INPUTS -->


    </div>

@endsection


@section('custom_footer')
    <script src="{{asset('js/item_name_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/tax_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/select2.min.js')}}"></script>

    <script>
        $(document).on('click', '#edit_work', function () {
            $('#edit_work').text("Save");
            $('#edit_work').attr('id', 'save_work');
            $('#save_work').attr('form', 'theform');
            $('.set_all_disabled').prop('disabled', false);
            $('#create_work').addClass('hide');
            $('#more').addClass('hide');
        });
    </script>
    <script>
        $(document).on('click', '#save_work', function () {
            $('#save_work').attr('type', 'submit');

        });
    </script>
    <script>
        $(document).on('click', '#create_work', function () {

            window.location.href = '/vendors/create'
        });
    </script>


    <!-----------------active link in nav and active link in sidebar ---->
    <script>
        $('.side_sheets').addClass('hide');
        $('#Purshase').removeClass('hide');

        $("#menu li").removeClass('active');
        $('#Purchase_nav_bar').addClass('active');

        $('#pages ul li a').removeClass('active');
        $('#vendor_side_bar').addClass('active');

    </script>

    <!--edit fields -->


    <script>
        $(document).on('click','#edit_user',function(){

            $(this).text("Save");
            $(this).attr('id','save_user');
            $("input[type=text]").attr('disabled', false).css({
                'border': '#eaeaea solid 1px',

                'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                'border-radius':'2px',
                'background-color':'#fff',
                'width':'150px',
                'margin-left':'15px'
            }).focus();

            $("input[type=password]").attr('disabled',false).css({
                'border': '#eaeaea solid 1px',

                'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                'border-radius':'2px',
                'background-color':'#fff',
                'margin-left':'15px'
            });

            $("input[type=email]").attr('disabled',false).css({
                'border': '#eaeaea solid 1px',

                'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                'border-radius':'2px',
                'background-color':'#fff',
                'margin-left':'15px'
            });



            $("select").removeAttr('disabled');
            $("input[type=radio]").attr('disabled', false);
            $("input[type=password]").attr('disabled', false);
            $("input[type=email]").attr('disabled', false);

        });


    </script>


    <!--add row to table -->
    <script>
        $("#add_row").click(function () {

            $("#myTable2").each(function () {

                var tds = '<tr>';
                jQuery.each($('tr:last td', this), function () {
                    tds += '<td>' + $(this).html() + '</td>';
                });
                tds += '</tr>';
                if ($('tbody', this).length > 0) {
                    $('tbody', this).append(tds);
                } else {
                    $(this).append(tds);
                }
            });
        });


    </script>

    <!--End add row to table -->


    <!--delete row in table -->
    <script>

        $(document).on('click', 'button#delete_row', function () {

            $(this).closest('tr').remove();
            return false;
        });

    </script>
    <!--Edit Row in Table -->

    <script>

        $(document).on('click','#editbtn',function(){
            var currentTD = $(this).parents('tr').find('td');
            if ($(this).html() == 'Edit') {
                currentTD = $(this).parents('tr').find('td');
                $.each(currentTD, function () {




                    $("input[type=text]").attr('disabled', false).css({
                        'border': '#eaeaea solid 1px',
                        'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                        'border-radius':'2px',
                        'background-color':'#fff',
                        'width':'150px',
                        'margin-left':'15px'
                    }).focus();

                    $("input[type=number]").attr('disabled',false).css({
                        'border': '#eaeaea solid 1px',
                        'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                        'border-radius':'2px',
                        'background-color':'#fff',
                        'margin-left':'15px'
                    });
                    $("select").removeAttr('disabled');
                });
            } else {
                $.each(currentTD, function () {
                    $("input[type=text]").attr('disabled', true).css({
                        'border': '#eaeaea solid 1px',

                        'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                        'border-radius':'2px',
                        'background-color':'#fff',
                        'width':'150px',
                        'margin-left':'15px'
                    }).focus();
                    $("input[type=number]").attr('disabled',true).css({
                        'box-shadow':'none',
                        'border': 'none',
                        'background-color':'fff',
                        'text-align':'center'
                    });
                    $("select").prop("disabled", true);
                });

            }

            $(this).html($(this).html() == 'Edit' ? 'Save' : 'Edit')

        });

    </script>





    <!--select salary scale -->

    <script>
        $(".myselect1").select2({
            width: 'resolve'

        });

        $(".myselect2").select2({
            width: 'resolve',


        });
        $(".myselect3").select2({
            width: 'resolve',


        });
        $(".myselect4").select2({
            width: 'resolve',


        });

    </script>

    {{-----------------------print----------------}}
    <script>

        $(document).on('click', '#print', function () {


            var id = $('#set_po_id').attr('current_po_id');
            window.open('/get_vendor_rpt/'+'{{$vendor->VendorID}}', '_blank');


        });

    </script>

@endsection