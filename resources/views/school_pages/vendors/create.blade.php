@extends('layouts.main_app')

@section('main_content')

    <link href="{{asset('css/fastselect.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/select2.min.css')}}" rel="stylesheet" />

    <style>
        input.col-md-4.text-center {

            border:0;
            background-color: #fff;
        }

        .main-content {
            padding: 34px 0 28px 5px;        }

        .collapse > .row>.col-md-4>.btn, .collapse > .row>.col-md-4>.dropdown>.btn {
            margin-top: 0;

        }

        input.invalid {
            border-color: red;
        }
        .third-nav{
            background-color:#eeeeee;
        }




        .myselect2{
            width:171px;
        }

        span.select2.select2-container.select2-container--default{
            width: 100%!important;
        }
        tbody th, tbody td {
            text-align: center;
        }
    </style>
    <div class="main">





        <!-- Navbar-content -->
        <nav class="navbar navbar-default navbar2">

            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                    <!--breadcrumbs-->
                    <div class="row">

                        <div class="col-md-10 col-lg-10">

                        <ol class="breadcrumb breadcrumb_nav">
                            <li><a href="/vendors">Vendors</a></li>
                            <li class="active"></li>
                        </ol>
                        </div>

                        <div class="col-md-2 col-lg-2">






                        </div>



                        </div>


                    <!--End breadcrumbs-->
                    <div class="row">
                        <div class="col-md-4 col-lg-4 buttons">
                            <button id="save_work" type="submit" form="theform" class="btn btn-danger " >Save</button>
                        </div>


                    </div> <!--collapse -->

                </div><!--collapse -->

            </div>
        </nav>
        <!-- End Navbar-content -->




        <!-- MAIN CONTENT -->
        <div class="main-content">

            <div class="container-fluid">

                <!-- INPUTS -->
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Vendors</h3>
                    </div>
                    <div class="panel-body">

                        {!! Form::open(['method'=>'POST' ,'id'=>'theform', 'action'=> 'VendorsController@store','file'=>'true' ,'enctype'=>'multipart/form-data']) !!}


                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="vendor_code" class="col-md-6 col-lg-6 control-label">Vendor Code</label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <input  class="form-control required " name="vendor_code" type="text" id="vendor_code"  required>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="vendor_name" class="col-md-6 col-lg-6 control-label">Vendor Name </label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <input  class="form-control required "  type="text" id="vendor_name" name="vendor_name" required>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">

                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="arabic_vendor_name" class="col-md-6 col-lg-6 control-label">Arabic Vendor Name</label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <input  class="form-control required "   type="text" id="arabic_vendor_name" name="arabic_vendor_name" required>
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="usr" class="col-md-6 col-lg-6 control-label">Address </label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <textarea class="form-control" rows="3" id="address" name="address"></textarea>


                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="usr" class="col-md-6 col-lg-6 control-label">Arabic Address</label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <textarea class="form-control" rows="3" id="address_ar" name="address_ar"></textarea>


                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="url" class="col-md-6 col-lg-6 control-label">Website</label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <input type="url" name="url" class="form-control " id="website" name="website"
                                                   pattern="https://.*"  placeholder="https://.*" />

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="url" class="col-md-6 col-lg-6 control-label">E-mail </label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <input type="email" class="form-control "  id="email" name="email">


                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="contact_school" class="col-md-6 col-lg-6 control-label">Contact School Person </label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">

                                            {!! Form::select('contact_school_person',[''=>''] + $employees,null,['class'=>'form-control myselect2 set_all_disabled','id'=>'contact_school']) !!}

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="contact_vendor_person" class="col-md-6 col-lg-6 control-label">Contact Vendor Person</label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <input  class="form-control "  type="text" id="contact_vendor_person" name="contact_vendor_person" >

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="phonenum" class="col-md-6 col-lg-6 control-label">Phone 1 </label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <input id="phonenum" name="phonenum" type="number" class="form-control " placeholder="0000-000-0000"  >
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="phonenum2" class="col-md-6 col-lg-6 control-label">Phone 2 </label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <input id="phonenum2" name="phonenum2" type="number" class="form-control "  placeholder="0000-000-0000"  >
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="tax_file_number" class="col-md-6 col-lg-6 control-label">Tax File Number</label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <input  class="form-control "  type="text" id="tax_file_number" name="tax_file_number" >

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="tax_office_name" class="col-md-6 col-lg-6 control-label">Tax Office Name</label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <input  class="form-control "  type="text" id="tax_office_name" name="tax_office_name" >

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="tax_office_code" class="col-md-6 col-lg-6 control-label">Tax Office Code</label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <input  class="form-control "  type="text" id="tax_office_code" name="tax_office_code" >

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="reg_code" class="col-md-6 col-lg-6 control-label">Commercial Regesteration Code</label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <input  class="form-control "  type="text" id="reg_code" name="reg_code" >

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="tax_reg_number" class="col-md-6 col-lg-6 control-label">Tax Regesteration Number</label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <input  class="form-control "  type="text" id="tax_reg_number" name="tax_reg_number" >

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="tax_reg_number" class="col-md-6 col-lg-6 control-label">Vat Regesteration Number</label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <input  class="form-control "  type="text" id="vat_reg_number" name="vat_reg_number" >

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="acc_name" class="col-md-6 col-lg-6 control-label">Currency</label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">

                                            {!! Form::select('currency',[''=>'']+ $currency, null,['class'=>'form-control myselect2 required set_all_disabled','id'=>'currency','required']) !!}
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="acc_name" class="col-md-6 col-lg-6 control-label">Account Name</label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">

                                            {!! Form::select('acc_name',[''=>''] + $accounts,null,['class'=>'form-control  myselect1 required set_all_disabled','id'=>'acc_name','required' ]) !!}
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                        <div class = 'form-group hidden'>
                            {!! Form::submit('create user',['class'=>'btn btn-primary']) !!}
                        </div>
                        {!! Form::close() !!}


                    </div>
                    <br>
                </div>
            </div>

        </div>
        <!-- END INPUTS -->


    </div>

@endsection


@section('custom_footer')
    <script src="{{asset('js/item_name_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/tax_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/select2.min.js')}}"></script>




    <script>
        $( document ).ready(function() {
         if($(".errorMessage1").isEmpty)  {
             
         }
        });

    </script>



    <!-----------------active link in nav and active link in sidebar ---->
    <script>

        $('.side_sheets').addClass('hide');
        $('#Purshase').removeClass('hide');

        $("#menu li").removeClass('active');
        $('#Purchase_nav_bar').addClass('active');

        $('#pages ul li a').removeClass('active');
        $('#vendor_side_bar').addClass('active');

    </script>









    <!--select salary scale -->

    <script>
        $(".myselect1").select2({
            width: 'resolve'

        });

        $(".myselect2").select2({
            width: 'resolve',


        });
        $(".myselect3").select2({
            width: 'resolve',


        });
        $(".myselect4").select2({
            width: 'resolve',


        });

    </script>




     @endsection