@extends('layouts.main_app')

@section('main_content')


    <style>

        input.col-md-4.text-center {

            border: 0;
            background-color: #fff;
        }

        input[type=number] {
            width: 80px;
        }

        .collapse > .row > .col-md-4 > .btn, .collapse > .row > .col-md-4 > .dropdown > .btn {
            margin-top: 0;

        }

        .btn {
            padding: 3px 10px;
            margin-top: 7%;
        }

        .third-nav {
            background-color: #eeeeee;
        }

        .approved {
            margin-left: 24%;
        }

        .red-border {
            border: 1px solid red !important;
        }
    </style>
    <link href="{{asset('css/select2.min.css')}}" rel="stylesheet"/>
    <div class="main">


        <!-- Navbar-content -->
        <nav class="navbar navbar-default navbar2">

            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">



                    <!--breadcrumbs-->

                    <div class="row">

                        <div class="col-md-10 col-lg-10 col-sm-8 col-xs-8">

                            <ol class="breadcrumb breadcrumb_nav">
                                <li><a href="/operation">Operation Details</a></li>
                                <li class="active" id="second"></li>
                            </ol>
                        </div>

                        @if(isset($operation))
                            <div class="col-md-2 col-lg-2 col-sm-4 col-xs-4">
                                @if(@\App\Operation::where('inv_doc_id','<',$operation->inv_doc_id)->orderBy('inv_doc_id','desc')->first()->inv_doc_id)
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <a href="/operation/{{@\App\Operation::where('inv_doc_id','<',$operation->inv_doc_id)->orderBy('inv_doc_id','desc')->first()->inv_doc_id}}/edit"><span
                                                    class="glyphicon glyphicon-arrow-left"></span>
                                        </a>
                                    </div>
                                @endif
                                @if(@\App\Operation::where('inv_doc_id','>',$operation->inv_doc_id)->orderBy('inv_doc_id','asc')->first()->inv_doc_id)
                                    <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                                        <a href="/operation/{{@\App\Operation::where('inv_doc_id','>',$operation->inv_doc_id)->orderBy('inv_doc_id','asc')->first()->inv_doc_id}}/edit">
                                    <span class="glyphicon glyphicon-arrow-right">
                                </span>
                                        </a>
                                    </div>
                                @endif
                            </div>
                        @endif


                    </div>

                    <!--End breadcrumbs-->


                    <div class="row">
                        <div class="col-lg-4  col-md-4 col-sm-6 col-xs-6 buttons">
                            @if(isset($operation))
                                <button id="edit_dep" type="button" class="btn btn-danger ">Edit</button>
                                <button type="button" class="btn" id="creat_dep">Create</button>
                            @else
                                <button form="theform" type="submit" class="btn btn-danger">Save</button>
                                {{--<button id="edit_user" type="button" class="btn btn-danger " >Edit</button>--}}
                                <button type="button" class="hide btn" id="creat_dep">Create</button>
                            @endif
                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-4 ">
                            @if(isset($operation))
                                <div class="dropdown">
                                    <button class=" btn btn-primary  dropdown-toggle" type="button"
                                            data-toggle="dropdown" id="more" hidden>More
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" id="delete_dep">Delete</a></li>
                                        <li><a href="#" id="dublicate_dep">Duplicate</a></li>
                                        <li><a href="/get_operation_rpt/{{$operation->inv_doc_id}}" id="print">Print</a></li>

                                    </ul>
                                </div>
                            @else
                                <div class="dropdown">
                                    <button class="hide btn btn-primary  dropdown-toggle" type="button"
                                            data-toggle="dropdown" id="more" hidden>More
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" id="delete_dep">Delete</a></li>
                                        <li><a href="#">Export</a></li>
                                        <li><a href="#" id="dublicate_dep">Duplicate</a></li>
                                        <li><a href="#" id="print">Print</a></li>

                                    </ul>
                                </div>
                            @endif
                        </div>
                        <!--search-->
                        <!--
                         <div class="col-md-4">
                             <form class="navbar-form navbar-right" >
                                 <div class="input-group form-group">
                                     <input type="text" value="" class="form-control" placeholder="Search ...">
                                 </div>
                             </form>
                         </div>
                         -->
                    </div> <!--collapse -->




                </div><!--collapse -->

            </div>
        </nav>
        <!-- End Navbar-content -->

        <!-- Success messages -->
        <div class="alert alert-success alert-autocloseable-success" style="display:none;position:absolute;">
            Operation Details Saved Successfully!

        </div>
        <!--end Success messages -->


        <!-- failuer messages -->
        <div class="alert alert-danger errorMessage1" style="display:none;position:absolute;">
            Your should fill all fields!

        </div>
        <!--end failuer messages -->

        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Operation Type</h3>
                    </div>
                    <div class="panel-body">

                        @if(isset($operation))
                            <div id="set_po_id" current_po_id="{{$operation->inv_doc_id}}" current_po_status="" closed_or_not=""
                                 current_id="{{Auth::user()->id}}"></div>
                        @else
                            <div id="set_po_id" current_po_id="" current_po_status="draft_po" closed_or_not="0"
                                 current_id="{{Auth::user()->id}}"></div>
                        @endif

                        <div class="row">
                            @if(isset($operation))
                            {!! Form::open(['method'=>'put' , 'id'=>'theform', 'action'=> ['OperationController@update',$operation->inv_doc_id]  ]) !!}
                            @else
                                {!! Form::open(['method'=>'POST' ,'id'=>'theform', 'action'=> 'OperationController@store']) !!}
                            @endif
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <div class="col-md-6 col-lg-6">

                                        <div class = 'form-group'>
                                            {!! Form::label('doc_type_name','Operation Name',['class'=>'col-md-6 col-lg-6 control-label']) !!}


                                            <div class="col-md-6 col-lg-6">
                                                <div class = 'form-group'>

                                                @if(isset($operation))
                                            {!! Form::text('doc_type_name', $operation->doc_type_name,['class'=>'form-control set_all_disabled col-md-6 required ','disabled']) !!}
                                            @else
                                                {!! Form::text('doc_type_name', null,['class'=>'form-control set_all_disabled col-md-6 required ','required']) !!}
                                            @endif
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>
                <br>
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <div class="col-md-6 col-lg-6">

                                        <div class = 'form-group'>
                                            {!! Form::label('result','Operation Type',['class'=>'col-md-6 col-lg-6 control-label']) !!}

                                            <div class="col-md-6 col-lg-6">
                                                <div class = 'form-group'>
                                            @if(isset($operation))
                                                {{ Form::radio('result',$operation->in_out=='in'? true :false,['class'=>'set_all_disabled']) }}
                                                        {{ Form::label('result', 'In',['class'=>'radio-inline']) }}

                                                {{ Form::radio('result' ,$operation->in_out=='out'? true :false,['class'=>'set_all_disabled']) }}

                                                        {{ Form::label('result', 'Out',['class'=>'radio-inline']) }}

                                                    @else

                                                {{ Form::radio('result', 'in' , true,['class'=>'set_all_disabled']) }}

                                                        {{ Form::label('result', 'In',['class'=>'radio-inline']) }}


                                                {{ Form::radio('result', 'out' , false,['class'=>'set_all_disabled']) }}

                                                        {{ Form::label('result', 'Out',['class'=>'radio-inline']) }}


                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">

                                    <div id="department_name_error" class="hide alert alert-danger errorMessage">The
                                        name field is required.
                                    </div>
                                </div>

                            </div>

                            <div class = 'form-group hidden'>
                                {!! Form::submit('create user',['class'=>'btn btn-primary']) !!}
                            </div>
                            {!! Form::close() !!}

                        </div>
                    </div>
                </div>
                <!-- END MAIN CONTENT -->
            </div>
            <!-- END MAIN -->


        </div>
    </div>




@endsection


@section('custom_footer')

    <script src="{{asset('assets/scripts/bootstrap-datetimepicker.min.js')}}"></script>

    <script src="{{asset('js/item_name_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/tax_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/select2.min.js')}}"></script>

    {{--------------------  active side & nav bar ------------------------}}
    <script>
        $('.side_sheets').addClass('hide');
        $('#inventory').removeClass('hide');

        $("#menu li").removeClass('active');
        $('#Inventory_nav_bar').addClass('active');

        $('#pages ul li a').removeClass('active');
        $('#operation_side_bar').addClass('active');

    </script>


    {{-------------------- save new operation --------------------}}
    {{----------------------- edit department -----------------------}}
    <script>
        $(document).on('click', '#edit_dep', function () {
            $('#edit_dep').text("Save");
            $('#edit_dep').attr('id', 'save_dep');

            $('#save_dep').attr('form', 'theform');


            $('.set_all_disabled').prop('disabled', false);


            $('#creat_dep').addClass('hide');

            $('#more').addClass('hide');

        });
    </script>
    <script>
        $(document).on('click', '#save_dep', function () {
            $('#save_dep').attr('type', 'submit');

        });
    </script>

    {{---------------------------- create new  ------------------------------}}
    {{---- dublicate dep ---}}
    <script>
        $(document).on('click', '#dublicate_dep', function () {

            $('#set_po_id').attr('current_po_id', "");
            /*$('#set_po_id').attr('current_po_status',"draft_po");*/
            /*$('#set_po_id').attr('closed_or_not',0);*/

            $('.set_all_disabled').prop('disabled', false);


            $('#creat_dep').addClass('hide');
            $('#more').addClass('hide');


            $('#edit_dep').prop('disabled', false);
            $('#edit_dep').text("Save");
            $('#edit_dep').attr('id', 'save_dep');
            $('#save_dep').prop('disabled', false);
        });
    </script>

    {{----------------------  delete department ------------------------------------}}

    <script>
        $(document).on('click', '#delete_dep', function () {
            var cur_po_id = $('#set_po_id').attr('current_po_id');

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/operation/delete/',
                dataType: 'json',
                type: 'get',
                data: {
                    cur_po_id: cur_po_id,
                },

                success: function (response) {

                    window.location.href = '/operation';
                }

            });

        });
    </script>

    {{-----------   on leave page -------------------}}
    {{--<script type='text/javascript'>
        $(window).bind('beforeunload', function(e){
            if($('#save_dep').length ){
                return 'Are you sure you want to leave?';
            }
        });


    </script>--}}
    {{------------------------------------------}}
    <script>
        $(document).ready(function () {
            $(".select_2_enable").select2();

        });
    </script>


    {{---------------------------- create new  ------------------------------}}
    <script>
        $(document).on('click', '#creat_dep', function () {
            window.location.href = '/operation/create';
        });
    </script>



@endsection