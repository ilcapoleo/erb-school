@extends('layouts.main_app')

@section('main_content')
    <link href="{{asset('css/fastselect.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/select2.min.css')}}" rel="stylesheet" />

    <style>
        input.col-md-4.text-center {

            border:0;
            background-color: #fff;
        }



        .collapse > .row>.col-md-4>.btn, .collapse > .row>.col-md-4>.dropdown>.btn {
            margin-top: 0;

        }



        .third-nav{
            background-color:#eeeeee;
        }

        .btn {
            padding: 3px 10px;
            margin-top: 3%;
        }



        .myselect2{
            width:171px;
        }

        span.select2.select2-container.select2-container--default{
            width: 100%!important;
        }
        tbody th, tbody td {
            text-align: center;
        }
    </style>
    <div class="main">
        <!-- Navbar-content -->
        <nav class="navbar navbar-default navbar2">

            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                    <!--breadcrumbs-->
                    <div class="row">

                        <div class="col-md-10 col-lg-10">

                        <ol class="breadcrumb breadcrumb_nav">
                            <li><a href="#">Check Paid</a></li>
                            <li class="active"></li>
                        </ol>
                        </div>

                        <div class="col-md-2 col-lg-2">
                            <div class="col-md-6">
                                <a href="#">
                                    <span class="glyphicon glyphicon-arrow-left"></span>
                                </a>
                            </div>

                            <div class="col-md-6">
                                <a href="#">
                                    <span class="glyphicon glyphicon-arrow-right">
                                </span>
                                </a>
                            </div>





                        </div>



                        </div>


                    <!--End breadcrumbs-->
                    <div class="row">
                        <div class="col-md-4 col-lg-4 buttons">
                            <button id="edit_user" type="button" class="btn btn-danger " >Edit</button>
                            <button type="button" class="btn" id="creat" >Create</button>
                        </div>

                        <div class="col-md-4 col-lg-4">
                            <div class="dropdown">
                                <button class="btn btn-primary  dropdown-toggle" type="button" data-toggle="dropdown" id="more"  hidden>More
                                    <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#" id="delete">Delete</a></li>
                                    <li><a href="#">Export</a></li>
                                    <li><a href="#">Duplicate</a></li>
                                    <li><a href="#">Print</a></li>

                                </ul>
                            </div>
                        </div>
                        <!--search-->
                        <!--
                         <div class="col-md-4">
                             <form class="navbar-form navbar-right" >
                                 <div class="input-group form-group">
                                     <input type="text" value="" class="form-control" placeholder="Search ...">
                                 </div>
                             </form>
                         </div>
                         -->
                    </div> <!--collapse -->

                </div><!--collapse -->


                <!--buttons row +second breadcrumb-->

                <div class="row third-nav">


                    <div class="col-md-6 col-lg-6">
                        <div class="col-md-3 col-lg-3">
                            <button  type="button" class="set_all_disabled btn" id="send_mail" >Send by Mail</button>
                        </div>

                        <div class="col-md-3 col-lg-3">
                            <button id="sendto_approved_btn" type="button" class="set_all_disabled btn"  disabled  > To Approve</button>
                        </div>

                            <div class="col-md-3 col-lg-3  ">
                                <button id="approved_btn" type="button" class="set_all_disabled btn approved "  disabled >Approve</button>
                            </div>



                    </div>
                    <div class="col-md-6 col-lg-6">

                             <span id="current_choosed_status" class="breadcrumbs ">

                                     <span id="draft_check" href="#" class="   breadcrumb">Draft </span></li>
                                     <span id="to_approve_check" href="#" class="   breadcrumb">To Approved </span></li>
                                     <span id="approved_check" href="#" class="   breadcrumb ">Approved </span></li>

                                     <span id="paid_check" href="#" class="   breadcrumb" disabled >Paid</span></li>



                            </span>


                    </div>


                </div>

            </div>
        </nav>
        <!-- End Navbar-content -->
        <!-- MAIN CONTENT -->
        <div class="main-content">

            <div class="container-fluid">

                <!-- INPUTS -->
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Check Paid</h3>
                    </div>
                    <div class="panel-body">

                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="journal" class="col-md-6 col-lg-6 control-label">Journal</label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <select id="journal" class="myselect1 required"   style="width: 50%" name="journal">

                                                <option>journal-1</option>

                                                <option >journal-2</option>

                                                <option >journal-3</option>

                                                <option >journal-4</option>


                                            </select>
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="document" class="col-md-6 col-lg-6 control-label">Document</label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <select id="document" class="myselect1 "   style="width: 50%" name="document">

                                                <option>document-1</option>

                                                <option >document-2</option>

                                                <option >document-3</option>

                                                <option >document-4</option>


                                            </select>
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                        <label for="currency" class="col-md-6 col-lg-6 control-label">Currency</label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <select id="currency" class="myselect2 required"   style="width: 50%" name="currency">

                                                <option>$</option>

                                                <option >LE</option>




                                            </select>
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">
                        <div class="col-md-6 col-lg-6">

                            <div class="form-group">
                                <label for="rate" class="col-md-6 col-lg-6 control-label">Rate</label>

                                <div class=" col-md-6 col-lg-6">

                                    <div class="form-group">
                                        <input disabled class="form-control required"  type="text" id="rate" >

                                    </div>

                                </div>

                            </div>

                        </div>
                        </div>
                        <br>
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="usr" class="col-md-6 col-lg-6 control-label">Created Date </label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <div class="input-group ">
                                                <input type="text" id="date1" class="form-control col-md-6 col-lg-6 required"  name="date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar">
                                                    </i>
                                                </div>
                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="check_number" class="col-md-6 col-lg-6 control-label">Check Number</label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <input  class="form-control required"  type="text" id="check_number" >

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="details" class="col-md-6 col-lg-6 control-label">Details </label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <textarea class="form-control" rows="3" id="details"></textarea>


                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="url" class="col-md-6 col-lg-6 control-label">Check Date </label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <div class="input-group ">
                                                <input type="text" id="date2" class="form-control col-md-6 col-lg-6 required"  name="date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar">
                                                    </i>
                                                </div>
                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="check_value" class="col-md-6 col-lg-6 control-label">Check Value</label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <input  class="form-control required"  type="text" id="check_value" >

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="bank" class="col-md-6 col-lg-6 control-label">Bank</label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <select id="bank" class="myselect1 required"   style="width: 50%" name="bank">

                                                <option>QNB</option>

                                                <option >FAISAL</option>

                                                <option >Ahly</option>

                                                <option >Masr</option>


                                            </select>
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="ben_type" class="col-md-6 col-lg-6 control-label">Benfeciary Type</label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <select id="ben_type" class="myselect1 required"   style="width: 50%" name="ben_type">

                                                <option>Employee</option>

                                                <option >Vendor</option>




                                            </select>
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="ben_name" class="col-md-6 col-lg-6 control-label">Benfeciary Name</label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <select id="ben_name" class="myselect1 required"   style="width: 50%" name="ben_name">

                                                <option>Kamal Saad</option>

                                                <option >Ahmed sabry</option>

                                            </select>
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="invoice_no" class="col-md-6 col-lg-6 control-label">Invoice Number</label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <select id="invoice_no" class="myselect1"   style="width: 50%" name="invoice_no">

                                                <option>invoice-1</option>

                                                <option >invoice-2</option>

                                                <option >invoice-3</option>

                                                <option >invoice-4</option>



                                            </select>
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="acc_no" class="col-md-6 col-lg-6 control-label">Account Name</label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <select id="acc_no" class="myselect1 required"   style="width: 50%" name="acc_no">

                                                <option>acc-1</option>

                                                <option >acc-2</option>

                                                <option >acc-3</option>

                                                <option >acc-4</option>



                                            </select>
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>
                    <br>
                </div>
            </div>

        </div>
        <!-- END INPUTS -->


    </div>

@endsection


@section('custom_footer')
    <script src="{{asset('js/item_name_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/tax_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/select2.min.js')}}"></script>




    <!-----------------active link in nav and active link in sidebar ---->
    <script>
        $('.side_sheets').addClass('hide');
        $('#accounting').removeClass('hide');

        $("#menu li").removeClass('active');
        $('#accounting_nav_bar').addClass('active');

        $('#pages ul li a').removeClass('active');
        $('#check_side_bar').addClass('active');

    </script>

    <!--edit fields -->


    <script>
        $(document).on('click','#edit_user',function(){

            $(this).text("Save");
            $(this).attr('id','save_user');
            $("input[type=text]").attr('disabled', false).css({
                'border': '#eaeaea solid 1px',

                'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                'border-radius':'2px',
                'background-color':'#fff',
                'width':'150px',
                'margin-left':'15px'
            }).focus();

            $("input[type=password]").attr('disabled',false).css({
                'border': '#eaeaea solid 1px',

                'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                'border-radius':'2px',
                'background-color':'#fff',
                'margin-left':'15px'
            });

            $("input[type=email]").attr('disabled',false).css({
                'border': '#eaeaea solid 1px',

                'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                'border-radius':'2px',
                'background-color':'#fff',
                'margin-left':'15px'
            });



            $("select").removeAttr('disabled');
            $("input[type=radio]").attr('disabled', false);
            $("input[type=password]").attr('disabled', false);
            $("input[type=email]").attr('disabled', false);

        });


    </script>


    <!--add row to table -->
    <script>
        $("#add_row").click(function () {

            $("#myTable2").each(function () {

                var tds = '<tr>';
                jQuery.each($('tr:last td', this), function () {
                    tds += '<td>' + $(this).html() + '</td>';
                });
                tds += '</tr>';
                if ($('tbody', this).length > 0) {
                    $('tbody', this).append(tds);
                } else {
                    $(this).append(tds);
                }
            });
        });


    </script>

    <!--End add row to table -->


    <!--delete row in table -->
    <script>

        $(document).on('click', 'button#delete_row', function () {

            $(this).closest('tr').remove();
            return false;
        });

    </script>
    <!--Edit Row in Table -->

    <script>

        $(document).on('click','#editbtn',function(){
            var currentTD = $(this).parents('tr').find('td');
            if ($(this).html() == 'Edit') {
                currentTD = $(this).parents('tr').find('td');
                $.each(currentTD, function () {




                    $("input[type=text]").attr('disabled', false).css({
                        'border': '#eaeaea solid 1px',
                        'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                        'border-radius':'2px',
                        'background-color':'#fff',
                        'width':'150px',
                        'margin-left':'15px'
                    }).focus();

                    $("input[type=number]").attr('disabled',false).css({
                        'border': '#eaeaea solid 1px',
                        'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                        'border-radius':'2px',
                        'background-color':'#fff',
                        'margin-left':'15px'
                    });
                    $("select").removeAttr('disabled');
                });
            } else {
                $.each(currentTD, function () {
                    $("input[type=text]").attr('disabled', true).css({
                        'border': '#eaeaea solid 1px',

                        'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                        'border-radius':'2px',
                        'background-color':'#fff',
                        'width':'150px',
                        'margin-left':'15px'
                    }).focus();
                    $("input[type=number]").attr('disabled',true).css({
                        'box-shadow':'none',
                        'border': 'none',
                        'background-color':'fff',
                        'text-align':'center'
                    });
                    $("select").prop("disabled", true);
                });

            }

            $(this).html($(this).html() == 'Edit' ? 'Save' : 'Edit')

        });

    </script>





    <!--select salary scale -->

    <script>
        $(".myselect1").select2({
            width: 'resolve'

        });

        $(".myselect2").select2({
            width: 'resolve',


        });
        $(".myselect3").select2({
            width: 'resolve',


        });
        $(".myselect4").select2({
            width: 'resolve',


        });

    </script>


@endsection