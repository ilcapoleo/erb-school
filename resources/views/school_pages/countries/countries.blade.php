@extends('layouts.main_app')

@section('main_content')


    <style>

        input.col-md-4.text-center {

            border: 0;
            background-color: #fff;
        }

        input[type=number] {
            width: 80px;
        }

        .collapse > .row > .col-md-4 > .btn, .collapse > .row > .col-md-4 > .dropdown > .btn {
            margin-top: 0;

        }

        .btn {
            padding: 3px 10px;
            margin-top: 7%;
        }

        .third-nav {
            background-color: #eeeeee;
        }

        .approved {
            margin-left: 24%;
        }

        .red-border {
            border: 1px solid red !important;
        }
    </style>
    <link href="{{asset('css/select2.min.css')}}" rel="stylesheet"/>
    <div class="main">


        <!-- Navbar-content -->
        <nav class="navbar navbar-default navbar2">

            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">



                    <!--breadcrumbs-->

                    <div class="row">

                        <div class="col-md-10 col-lg-10 col-sm-8 col-xs-8">

                            <ol class="breadcrumb breadcrumb_nav">
                                <li><a href="/countries">Country</a></li>
                                <li class="active" id="second">

                                </li>
                            </ol>
                        </div>

                        @if(isset($country))
                            <div class="col-md-2 col-lg-2 col-sm-4 col-xs-4">
                                @if(@\App\Country::where('Country_ID','<',$country->Country_ID)->orderBy('Country_ID','desc')->first()->Country_ID)
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <a href="/countries/{{@\App\Country::where('Country_ID','<',$country->Country_ID)->orderBy('Country_ID','desc')->first()->Country_ID}}.'/edit"><span
                                                    class="glyphicon glyphicon-arrow-left"></span>
                                        </a>
                                    </div>
                                @endif
                                @if(@\App\Country::where('Country_ID','>',$country->Country_ID)->orderBy('Country_ID','asc')->first()->Country_ID)
                                    <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                                        <a href="/countries/{{@\App\Country::where('Country_ID','>',$country->Country_ID)->orderBy('Country_ID','asc')->first()->Country_ID}}.'/edit">
                                    <span class="glyphicon glyphicon-arrow-right">
                                </span>
                                        </a>
                                    </div>
                                @endif
                            </div>
                        @endif


                    </div>

                        <!--End breadcrumbs-->


                    <div class="row">
                        <div class="col-lg-4  col-md-4 col-sm-6 col-xs-6 buttons">
                            @if(isset($country))
                                <button id="edit_dep" type="button" class="btn btn-danger ">Edit</button>
                                <button type="button" class="btn" id="creat_dep">Create</button>
                                @else
                            <button id="save_dep"  type="submit" form="theform" type="button" class="btn btn-danger ">Save</button>
                            {{--<button id="edit_user" type="button" class="btn btn-danger " >Edit</button>--}}
                            <button type="button" class="hide btn" id="creat_dep">Create</button>
                                @endif
                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-4 ">
                            @if(isset($country))
                                <div class="dropdown">
                                    <button class=" btn btn-primary  dropdown-toggle" type="button"
                                            data-toggle="dropdown" id="more" hidden>More
                                        <span class="caret"></span></button>

                                    <ul class="dropdown-menu">
                                        <li>

                                            <a data-toggle="modal" data-target="#delete{{ $country->Country_ID }}"
                                            >delete</a>


                                        </li>
                                        <li><a href="#">Export</a></li>
                                        <li><a href="#">Duplicate</a></li>
                                        <li><a href="/get_country_rpt/{{$country->Country_ID}}" id="print">Print</a></li>

                                    </ul>
                                    <div id="delete{{ $country->Country_ID }}" class="modal fade" role="dialog">
                                        <div class="modal-dialog">

                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Delete vendor</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p>{{$country->Country_ID. ' ' .$country->Country_Name}}</p>
                                                </div>
                                                <div class="modal-footer">
                                                    {!! Form::open(['method'=>'DELETE','route'=>['countries.destroy',$country->Country_ID]]) !!}
                                                    <button type="button" class="btn btn-default btn-flat"
                                                            data-dismiss="modal">close
                                                    </button>
                                                    <button type="submit"
                                                            class="btn btn-danger btn-flat">delete
                                                    </button>
                                                    {!! Form::close() !!}
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                @else
                            <div class="dropdown">
                                <button class="hide btn btn-primary  dropdown-toggle" type="button"
                                        data-toggle="dropdown" id="more" hidden>More
                                    <span class="caret"></span></button>
                                <ul class="dropdown-menu">


                                </ul>
                            </div>
                                @endif
                        </div>

                    </div>




                </div><!--collapse -->

            </div>
        </nav>
        <!-- End Navbar-content -->


        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Country Details</h3>
                    </div>
                    <div class="panel-body">


                        <div class="row">
                            @if(isset($country))
                            {!! Form::open(['method'=>'put' ,'id'=>'theform', 'action'=> ['CountriesController@update',$country->Country_ID]  ]) !!}
                            @else
                            {!! Form::open(['method'=>'POST' ,'id'=>'theform', 'action'=> 'CountriesController@store']) !!}
                            @endif
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <div class="col-md-6 col-lg-6">
                                        <div class="form-group">

                                            <label class="col-md-6 col-lg-6 control-label">Country Name</label>
                                            <div class="col-md-6 col-lg-6">
                                                <div class="form-group">

                                                    @if(isset($country))
                                                        <input id="country_name"  name="country_name" type="text"
                                                               class="get_valide_for set_all_disabled get_all_date_input form-control required err_input job_name_error has_error "
                                                               value="{{$country->Country_Name}}" disabled required>
                                                    @else
                                                        <input id="country_name" type="text" name="country_name"
                                                               class="set_all_disabled get_all_date_input form-control required err_input job_name_error"
                                                               value="" required>
                                                    @endif


                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class = 'form-group hidden'>
                                {!! Form::submit('create user',['class'=>'btn btn-primary']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <!-- END MAIN CONTENT -->
            </div>
            <!-- END MAIN -->


        </div>
    </div>




@endsection


@section('custom_footer')

    <script src="{{asset('assets/scripts/bootstrap-datetimepicker.min.js')}}"></script>
    <script src="{{asset('js/item_name_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/tax_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/select2.min.js')}}"></script>

    {{--------------------  active side & nav bar ------------------------}}
    <script>
        $('.side_sheets').addClass('hide');
        $('#HR').removeClass('hide');

        $("#menu li").removeClass('active');
        $('#hr_nav_bar').addClass('active');

        $('#pages ul li a').removeClass('active');
        $('#country_side_bar').addClass('active');

    </script>



    {{----------------------- edit department -----------------------}}
    <script>
        $(document).on('click', '#edit_dep', function () {
            $('#edit_dep').text("Save");
            $('#edit_dep').attr('id', 'save_dep');
            $('#save_dep').attr('form', 'theform');

            $('.set_all_disabled').prop('disabled', false);
            $('#creat_dep').addClass('hide');
            $('#more').addClass('hide');

        });
    </script>

    <script>
        $(document).on('click', '#save_dep', function () {
        $('#save_dep').attr('type', 'submit');
        });
    </script>

    <script>
        $(document).on('click', '#dublicate_dep', function () {

            $('#set_po_id').attr('current_po_id', "");

            $('.set_all_disabled').prop('disabled', false);

            $('#creat_dep').addClass('hide');
            $('#more').addClass('hide');
            $('#edit_dep').prop('disabled', false);
            $('#edit_dep').text("Save");
            $('#edit_dep').attr('id', 'save_dep');
            $('#save_dep').prop('disabled', false);

        });
    </script>
    {{----}}
    {{-----------   on leave page -------------------}}
    {{--<script type='text/javascript'>--}}

        {{--$(window).bind('beforeunload', function(){--}}
            {{--if($('#save_dep').length){--}}
                {{--return 'Are you sure you want to leave?';--}}
            {{--}--}}

        {{--});--}}
        {{----}}
    {{--</script>--}}
    {{------------------------------------------}}
    <script>
        $(document).ready(function () {
            $(".select_2_enable").select2();

        });
    </script>


    {{---------------------------- create new  ------------------------------}}
    <script>
        $(document).on('click', '#creat_dep', function () {
            window.location.href = '/countries/create';
        });
    </script>



@endsection