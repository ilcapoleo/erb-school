@extends('layouts.main_app')

@section('main_content')
    <link href="{{asset('css/fastselect.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/select2.min.css')}}" rel="stylesheet" />
    <style>
        input.col-md-4.text-center {

            border:0;
            background-color: #fff;
        }

        .collapse > .row>.col-md-4>.btn, .collapse > .row>.col-md-4>.dropdown>.btn {
            margin-top: 0;

        }

        .btn{
            padding: 3px 10px;
            margin-top: 7%;
        }

        .third-nav{
            background-color:#eeeeee;
        }

        .alert-success{
            display: none;
        }
        .panel .table > thead > tr > td:first-child, .panel .table > thead > tr > th:first-child, .panel .table > tbody > tr > td:first-child, .panel .table > tbody > tr > th:first-child, .panel .table > tfoot > tr > td:first-child, .panel .table > tfoot > tr > th:first-child{
            padding-left: 0px;
        }


        .myselect2{
            width:171px;
        }

        span.select2.select2-container.select2-container--default{
            width: 100%!important;
        }

        input#number{
            width: 100%;
        }


        .red-border{
            border: 1px solid red  !important;
        }



        #checkall{

        }
    </style>

    <div class="main">
        <!-- Navbar-content -->
        <nav class="navbar navbar-default navbar2">

            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                    <!--breadcrumbs-->
                    <div class="row">

                        <div class="col-md-10 col-lg-10">

                        <ol class="breadcrumb breadcrumb_nav">
                            <li><a href="/show_leave_type">Leave</a></li>
                            <li class="active" id="second">
                                @if(isset($leave_type))
                                    @if($leave_type->leave_id != 0)
                                        {{$leave_type->leave_id}}
                                    @else
                                        Draft
                                    @endif
                                @endif
                            </li>
                        </ol>

                        </div>
                        @if(isset($leave_type))
                            <div class="col-md-2 col-lg-2">
                                @if(@\App\Leave_type::where('leave_id','<',$leave_type->leave_id)->orderBy('leave_id','desc')->first()->leave_id)
                                <div class="col-md-6">
                                    <a href="/leave/{{@\App\Leave_type::where('leave_id','<',$leave_type->leave_id)->orderBy('leave_id','desc')->first()->leave_id}}"><span
                                                class="glyphicon glyphicon-arrow-left"></span>
                                    </a>
                                </div>
                                @endif
                                @if(@\App\Leave_type::where('leave_id','>',$leave_type->leave_id)->orderBy('leave_id','asc')->first()->leave_id)
                                <div class="col-md-6">
                                    <a href="/leave/{{@\App\Leave_type::where('leave_id','>',$leave_type->leave_id)->orderBy('leave_id','asc')->first()->leave_id}}">
                                    <span class="glyphicon glyphicon-arrow-right">
                                </span>
                                    </a>
                                </div>
                                @endif
                            </div>
                        @endif

                        </div>   <!--End breadcrumbs-->
                    <div class="row">
                        <div class="col-md-4 col-lg-4 buttons">
                            @if(isset($leave_type))
                                <button id="edit_dep" type="button" class="btn btn-danger ">Edit</button>
                                <button type="button" class="btn" id="creat_dep">Create</button>
                                @else
                            <button id="save_dep" type="button" class="btn btn-danger " >Save</button>
                            {{--<button id="edit_user" type="button" class="btn btn-danger " >Edit</button>--}}
                            <button type="button" class="hide btn" id="creat_dep" >Create</button>
                                @endif
                        </div>

                        <div class="col-md-4 col-lg-4">
                            @if(isset($leave_type))
                                <div class="dropdown">
                                    <button class=" btn btn-primary  dropdown-toggle" type="button" data-toggle="dropdown" id="more"  hidden>More
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" id="delete_dep">Delete</a></li>
                                        <!-- temporery -->      {{--<li><a href="#">Export</a></li>--}}
                                        <li><a href="#" id="dublicate_dep">Duplicate</a></li>
                                        <li><a href="#" id="print">Print</a></li>
                                    </ul>
                                </div>
                                @else
                            <div class="dropdown">
                                <button class="hide btn btn-primary  dropdown-toggle" type="button" data-toggle="dropdown" id="more"  hidden>More
                                    <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#" id="delete_dep">Delete</a></li>
                                    <li><a href="#">Export</a></li>
                                    <li><a href="#" id="dublicate_dep">Duplicate</a></li>
                                    <li><a href="#" id="print">Print</a></li>

                                </ul>
                            </div>
                                @endif
                        </div>
                        <!--search-->
                        <!--
                         <div class="col-md-4">
                             <form class="navbar-form navbar-right" >
                                 <div class="input-group form-group">
                                     <input type="text" value="" class="form-control" placeholder="Search ...">
                                 </div>
                             </form>
                         </div>
                         -->
                    </div> <!--collapse -->

                </div><!--collapse -->

            </div>
        </nav>
        <!-- End Navbar-content -->


        <!-- Success messages -->
        <div class="alert alert-success alert-autocloseable-success" style="display:none;position:absolute;" >
            Your Leave Saved Successfully!

        </div>
        <!--end Success messages -->

        <!-- failuer messages -->
        <div class="alert alert-danger errorMessage1" style="display:none;position:absolute;" >
            Your should fill all fields!

        </div>
        <!--end failuer messages -->




        <!-- MAIN CONTENT -->
        <div class="main-content">

            <div class="container-fluid">

                <!-- INPUTS -->
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Leave</h3>
                    </div>
                    <div class="panel-body">
                        {{--{{$leave_type->leave_id}}--}}
                        @if(isset($leave_type))
                            <div id="set_po_id"  current_po_id="{{$leave_type->leave_id}}" current_po_status="" closed_or_not="" current_id="{{Auth::user()->id}}"></div>
                        @else
                            <div id="set_po_id"  current_po_id="" current_po_status="draft_po" closed_or_not="0" current_id="{{Auth::user()->id}}"></div>
                        @endif

                        <div class="row">
                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="usr" class="col-md-6 col-lg-6 control-label">Leave Name </label>

                                    <div class=" col-md-6 col-lg-6">

                                        @if(isset($leave_type))
                                            <input id="leave_name" type="text"  class="set_all_disabled get_all_date_input form-control required err_input leave_name_error" value="{{$leave_type->leave_name}}"  disabled>
                                        @else
                                            <input id="leave_name" type="text"  class="set_all_disabled get_all_date_input form-control required err_input leave_name_error" value=""  >
                                        @endif




                                    </div>

                                </div>

                            </div>

                        </div>

                            <br>
                            <div class="row">
                                <div class="col-md-12">

                                    <div id="leave_name_error" class="alert alert-danger errorMessage hide ">
                                        <strong >Wrong Name!</strong>
                                    </div>
                                </div>

                            </div>
                            <br>
                            <div class="row">
                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="usr" class="col-md-6 col-lg-6 control-label">Short Code </label>

                                    <div class=" col-md-6 col-lg-6">

                                        @if(isset($leave_type))
                                            <input id="short_name" type="text"  class="set_all_disabled get_all_date_input form-control err_input short_name_error required" value="{{$leave_type->short_code}}"  disabled>
                                        @else
                                            <input id="short_name" type="text"  class="set_all_disabled get_all_date_input form-control err_input short_name_error required" value=""  >
                                        @endif



                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>

                        <div class="row">
                            <div class="col-md-12">

                                <div id="short_name_error" class="alert alert-danger errorMessage hide ">
                                    <strong >Wrong Name!</strong>
                                </div>
                            </div>

                        </div>



                        <br>
                        <div class="row">

                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                <label for="number" class="col-md-6 col-lg-6 control-label">Leave Unit</label>

                                <div class="col-md-6 col-lg-6">
                                    <form>
                                        @if(isset($leave_type))
                                                <div id="hours_or_days"  hours_or_days="{{$leave_type->leave_unit}}"></div>
                                                <label class="radio-inline">
                                                    <input  type="radio" value="Days" @if($leave_type->leave_unit == 1)  checked @endif name="optradio" class="set_all_disabled get_all_date_input" disabled>Days
                                                </label>

                                                <label class="radio-inline">
                                                    <input  type="radio" value="Hours" @if($leave_type->leave_unit == 0)  checked @endif name="optradio" class="set_all_disabled get_all_date_input" disabled>Hours
                                                </label>
                                            @else
                                                <div id="hours_or_days"  hours_or_days="1"></div>
                                                <label class="radio-inline">
                                                    <input  type="radio" value="Days" checked name="optradio" class="set_all_disabled get_all_date_input">Days
                                                </label>

                                                <label class="radio-inline">
                                                    <input  type="radio" value="Hours" name="optradio" class="set_all_disabled get_all_date_input">Hours
                                                </label>
                                         @endif
                                    </form>


                                </div>
                                </div>
                            </div>



                        </div>
                         <br>
                        <div class="row">


                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="number" class="col-md-6 col-lg-6 control-label">Max <span id="days">Days</span> <span id="hours" style="display: none;" >Hours</span> </label>

                                    <div class=" col-md-6 col-lg-6 ">
                                        @if(isset($leave_type))
                                            <input id="get_max" min="1"  class="set_all_disabled get_all_date_input form-control required err_input max_error" value="{{$leave_type->max_unit}}"   type="number" disabled>
                                        @else
                                            <input id="get_max" min="1"  class="set_all_disabled get_all_date_input form-control required err_input max_error" value="1"   type="number">
                                        @endif


                                    </div>

                                </div>

                            </div>



                        </div>

                        <br>

                        <br>

                         <div class="row">
                             <div class="col-md-6 col-lg-6">
                             <div class="form-group">

                                 <label class="col-md-6 col-lg-6 control-label" for="leave">Paid Type</label>

                                 <div class="col-md-6 col-lg-6">

                                     <label class="radio-inline" for="unpaid">
                                         @if(isset($leave_type))
                                             <input unpaid_or_not="1" @if($leave_type->paid == 0) checked @endif name="leave" id="unpaid" value="yes" type="checkbox" class="set_all_disabled get_all_date_input" disabled>
                                         @else
                                             <input unpaid_or_not="1"  name="leave" id="unpaid" value="yes" type="checkbox" class="set_all_disabled get_all_date_input">
                                         @endif
                                        Unpaid
                                     </label>


                                 </div>



                             </div>

                             </div>
                         </div>

                       <br>

                            @if(isset($leave_type))
                                <div class="row deduction" @if($leave_type->paid == 1) style="display: none"@endif  disabled>
                            @else
                                <div class="row deduction" style="display: none">
                            @endif
                            <div class="col-md-6 col-lg-6">
                            <label class="col-md-6 col-lg-6 control-label" for="radios">Deduction</label>



                            <div class="col-md-6 col-lg-6">
                                <select id="cost_center" name="cost_center" class=" required get_all_selectors set_all_disabled select_2_enable form-control Deduction_error err_input"  @if(isset($leave_type))disabled @endif>
                                    <option value=""> </option>
                                    @foreach($deduction as $id=>$role)
                                        @if(isset($leave_type))
                                            @if($leave_type->deduction_id == $id)
                                                <option selected value="{{$id}}">{{$role}}</option>
                                            @else
                                                <option value="{{$id}}">{{$role}}</option>
                                            @endif
                                        @else
                                            <option value="{{$id}}">{{$role}}</option>
                                        @endif
                                    @endforeach
                                </select>

                            </div>

                        </div>

                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">

                                <div id="Deduction_error" class="alert alert-danger errorMessage hide ">
                                    <strong >Wrong Name!</strong>
                                </div>
                            </div>

                        </div>
                    </div>
                    <br>
                </div>
            </div>

        </div>
        <!-- END INPUTS -->


    </div>

@endsection



@section('custom_footer')
    <script src="{{asset('js/item_name_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/tax_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/select2.min.js')}}"></script>

    {{--------------------  active side & nav bar ------------------------}}
    <script>
        $('.side_sheets').addClass('hide');
        $('#HR').removeClass('hide');

        $("#menu li").removeClass('active');
        $('#hr_nav_bar').addClass('active');

        $('#pages ul li a').removeClass('active');
        $('#leave_side_bar').addClass('active');

    </script>

    {{-------------------- save new department --------------------}}
    <script>
        $('.alert-autocloseable-success').hide();

        $(document).on('click','#save_dep',function () {

            $('#save_dep').attr('disabled','disabled');


            var leave_name = $('#leave_name').val();
            var short_name = $('#short_name').val();
            var leave_unit = $('#hours_or_days').attr('hours_or_days');
            var max_unit = $('#get_max').val();
            var paid = $('#unpaid').attr('unpaid_or_not');
            var Deduction = $('#cost_center').val();

            if(Deduction == "" || paid == 1)
            {
                Deduction = null;
            }


            var current_user_id= $('#set_po_id').attr('current_id');
            var current_po_id = $('#set_po_id').attr('current_po_id');

            console.log(leave_name +' '+short_name+' '+ leave_unit+' '+ max_unit+' '+paid+' '+ Deduction+' '+ current_user_id+' '+ current_po_id)
            console.log('current_po_id'+current_po_id)
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/add_new_leave_type/',
                dataType : 'json',
                type: 'get',
                data: {

                    leave_name:leave_name,
                    short_name:short_name,
                    leave_unit:leave_unit,
                    max:max_unit,
                    paid:paid,
                    Deduction:Deduction,


                    current_po_id:current_po_id,
                    cur_user_id:current_user_id,

                },

                success:function(response) {

                    console.log(response);
                    if(response['error'])
                    {
                        $('.err_input').removeClass('red-border');
                        $('.err_input').parent().find('.select2').removeClass('red-border');
                        $('.errorMessage1').show();
                        $.each(response['error'], function (key, val)
                        {
                            //alert(key + val);

                            console.log(key+'_error');
                            $("."+key+'_error').addClass('red-border');
                            $('.'+key+'_error').parent().find('.select2').addClass('red-border');


                            //console.log(val[0]);

                        })
                        $('#save_dep').removeAttr('disabled');

                    }

                    else
                    {

                        $('.errorMessage1').hide();

                        $('.err_input').removeClass('red-border');
                        $('.err_input').parent().find('.select2').removeClass('red-border');
                        $('#set_po_id').attr('current_po_id',response['current_po_id']);
                        $('.breadcrumb').removeClass('po-active');


                        $('#save_dep').text("Edit");
                        $('#save_dep').attr('id','edit_dep');

                        $('.set_all_disabled').prop('disabled',true);

                        $('#creat_dep').removeClass('hide');

                        $('#more').removeClass('hide');


                        $('#edit_dep').removeAttr('disabled');
                        //po-active
                        console.log(response);

                        {{----SUCCESS ON SAVE -------}}
                        $('#autoclosable-btn-success').prop("disabled", true);
                        $('.alert-autocloseable-success').show('slow');

                        $('.alert-autocloseable-success').delay(3000).fadeOut( "slow", function() {
                            // Animation complete.
                            $('#autoclosable-btn-success').prop("disabled", false);
                        });
                        {{----SUCCESS ON SAVE -------}}
                    }

                }

            });

        });

    </script>
    {{----------------------- edit department -----------------------}}
    <script>
        $(document).on('click','#edit_dep',function () {
            $('#edit_dep').text("Save");
            $('#edit_dep').attr('id','save_dep');

            $('.set_all_disabled').prop('disabled',false);


            $('#creat_dep').addClass('hide');

            $('#more').addClass('hide');

        });
    </script>

    {{---------------------------- create new  ------------------------------}}
    <script>
        $(document).on('click','#creat_dep',function () {/*
            $('#edit_dep').prop('disabled',false);
            $('#edit_dep').text("Save");
            $('#edit_dep').attr('id','save_dep');
            $('#save_dep').prop('disabled',false);

            $('#unpaid').attr('unpaid_or_not',1);
            $('#unpaid').attr('checked',false);
            $(".deduction").hide();

            /!*$("input[name=optradio][value='Days']").attr('checked', 'checked');*!/

            /!*$('#hours_or_days').attr('hours_or_days',1);*!/

            $('.set_all_disabled').prop('disabled',false);

            $('#set_po_id').attr('current_po_id',"");
            /!*$('#set_po_id').attr('current_po_status',"draft_po");*!/
            $('#creat_dep').addClass('hide');
            $('#more').addClass('hide');

            $('.get_all_selectors').val('');

            $(".get_all_selectors option[value='']").attr('selected', true);
            $('.get_all_date_input').val('');
            $('.select2-selection__rendered').html('');*/
            window.location.href = '/leave';

        });
    </script>
    {{---- dublicate dep ---}}
    <script>
        $(document).on('click','#dublicate_dep',function(){

            $('#set_po_id').attr('current_po_id',"");
            /*$('#set_po_id').attr('current_po_status',"draft_po");*/
            /*$('#set_po_id').attr('closed_or_not',0);*/

            $('.set_all_disabled').prop('disabled',false);


            $('#creat_dep').addClass('hide');
            $('#more').addClass('hide');



            $('#edit_dep').prop('disabled',false);
            $('#edit_dep').text("Save");
            $('#edit_dep').attr('id','save_dep');
            $('#save_dep').prop('disabled',false);
        });
    </script>

    {{----------------------  delete department ------------------------------------}}

    <script>
        $(document).on('click','#delete_dep',function(){
            var cur_po_id = $('#set_po_id').attr('current_po_id');

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/delete_this_leave_type/',
                dataType : 'json',
                type: 'get',
                data: {
                    cur_po_id:cur_po_id,
                },

                success:function(response) {

                    window.location.href = '/leave';
                }

            });

        });
    </script>


    <!--show deduction div on "unpaid"  checked -->

    <script>
        $(function () {
            $("input[name='leave']").click(function () {
                if ($("#unpaid").is(":checked")) {
                    $(".deduction").show();
                    $(this).attr('unpaid_or_not',0);
                } else {
                    $(".deduction").hide();
                    $(this).attr('unpaid_or_not',1);
                }
            });
        });
    </script>

    <!-- show max days on days selected /// show max hours on hours selected -->
    <script>
        $('input[type="radio"]').click(function(){
            if($(this).attr("value")=="Days"){
                $("#hours").hide();
                $("#days").show();
                $('#hours_or_days').attr('hours_or_days',1);
            }
            if($(this).attr("value")=="Hours"){
                $("#days").hide();
                $("#hours").show();
                $('#hours_or_days').attr('hours_or_days',0);
            }
        });


    </script>

    {{-------------------  limit max days/hours -------------------------}}
    <script>
        $(document).on('change keyup','#get_max',function() {
            var max = parseInt($(this).attr('max'));
            var min = parseInt($(this).attr('min'));
            if ($(this).val() > max)
            {
                $(this).val(max);
            }
            else if ($(this).val() < min)
            {
                $(this).val(min);
            }
        });
    </script>

    {{-------------------------- enable select 2 for deduction selector ----------------------------}}
    <script>
        $(document).ready(function () {
            $(".select_2_enable").select2();

        });
    </script>

            {{-----------   on leave page -------------------}}
            <script type='text/javascript'>

                $(window).bind('beforeunload', function(){
                    if($('#save_dep').length){
                        return 'Are you sure you want to leave?';
                    }

                });


            </script>

    {{------------------------ chang color for input when focus --------------------}}
<script>

    $('input:text').focus(
        function(){
            $(this).css({'background-color' : '#fff'});
        });

    </script>
            <script>
                $(document).on('click', '#print', function () {
                    //  window.location.href = 'get_po2/276'
                    var id = $('#set_po_id').attr('current_po_id');
                    window.open('/get_leave_rpt/' + id + '', '_blank');
                });

            </script>
@endsection