@extends('layouts.main_app')

@section('main_content')
    <link href="{{asset('css/fastselect.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/select2.min.css')}}" rel="stylesheet"/>

    <style>
        input.col-md-4.text-center {

            border: 0;
            background-color: #fff;
        }

        .collapse > .row > .col-md-4 > .btn, .collapse > .row > .col-md-4 > .dropdown > .btn {
            margin-top: 0;

        }

        .third-nav {
            background-color: #eeeeee;
        }

        .myselect2 {
            width: 171px;
        }

        span.select2.select2-container.select2-container--default {
            width: 100% !important;
        }

        tbody th, tbody td {
            text-align: center;
        }
    </style>
    <link href="{{asset('css/select2.min.css')}}" rel="stylesheet"/>

    <div class="main">
        <!-- Navbar-content -->
        <nav class="navbar navbar-default navbar2">

            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                    <!--breadcrumbs-->
                    <div class="row">

                        <div class="col-md-10 col-lg-10">

                            <ol class="breadcrumb breadcrumb_nav">
                                <li><a href="/journals/table">Journals</a></li>
                                <li class="active">
                                    @if(isset($journal))
                                        @if($journal->journal_id != 0)
                                            {{$journal->journal_id}}
                                        @else
                                            Draft
                                        @endif
                                    @endif
                                </li>
                            </ol>
                        </div>


                        @if(isset($journal))
                            <div class="col-md-2 col-lg-2 col-sm-4 col-xs-4">
                                @if(@\App\Journal::where('journal_id','<',$journal->journal_id)->orderBy('journal_id','desc')->first()->journal_id)
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <a href="/journals/{{@\App\Journal::where('journal_id','<',$journal->journal_id)->orderBy('journal_id','desc')->first()->journal_id}}"><span
                                                    class="glyphicon glyphicon-arrow-left"></span>
                                        </a>
                                    </div>
                                @endif
                                @if(@\App\Journal::where('journal_id','>',$journal->journal_id)->orderBy('journal_id','asc')->first()->journal_id)
                                    <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                                        <a href="/journals/{{@\App\Journal::where('journal_id','>',$journal->journal_id)->orderBy('journal_id','asc')->first()->journal_id}}">
                                    <span class="glyphicon glyphicon-arrow-right">
                                </span>
                                        </a>
                                    </div>
                                @endif
                            </div>
                        @endif


                    </div>


                    <!--End breadcrumbs-->
                    <div class="row">
                        <div class="col-lg-4  col-md-4 col-sm-6 col-xs-6 buttons">
                            @if(isset($journal))
                                <button id="edit_po" type="button" class="btn btn-danger ">Edit</button>
                                <button type="button" class="btn" id="creat_dep">Create</button>
                            @else
                                <button id="save_po" type="button" class="btn btn-danger ">Save</button>
                                {{--<button id="edit_user" type="button" class="btn btn-danger " >Edit</button>--}}
                                <button type="button" class="hide btn" id="creat_dep">Create</button>
                            @endif
                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-4 ">
                            @if(isset($journal))
                                <div class="dropdown">
                                    <button class=" btn btn-primary  dropdown-toggle" type="button"
                                            data-toggle="dropdown" id="more" hidden>More
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li class="  " data-toggle="modal" data-target="#exampleModal"><a href="#"
                                                                                                          id="delete_btn">Delete</a>
                                        </li>
                                        <li><a href="#">Export</a></li>
                                        <li><a href="#" id="dublicate_dep">Duplicate</a></li>
                                        <li><a href="#" id="print">Print</a></li>

                                    </ul>
                                </div>
                            @else
                                <div class="dropdown">
                                    <button class="hide btn btn-primary  dropdown-toggle" type="button"
                                            data-toggle="dropdown" id="more" hidden>More
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li class="  " data-toggle="modal" data-target="#exampleModal"><a href="#"
                                                                                                          id="delete_btn">Delete</a>
                                        </li>
                                        <li><a href="#">Export</a></li>
                                        <li><a href="#" id="dublicate_dep">Duplicate</a></li>
                                        <li><a href="#" id="print">Print</a></li>

                                    </ul>
                                </div>
                            @endif
                        </div>
                    </div>

                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Purchase Order</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    Are You Sure to Delete this Journal ?!!
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-danger fa fa-trash" id="delete_po">Delete
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{----------------------------------------}}


                </div><!--collapse -->

            </div>
        </nav>
        <!-- End Navbar-content -->
        <!-- MAIN CONTENT -->
        <div class="main-content">

            <div class="container-fluid">

                <!-- INPUTS -->
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Journals</h3>
                    </div>
                    <div class="panel-body">

                        @if(isset($journal))
                            <div id="set_po_id" current_po_id="{{$journal->journal_id}}" current_po_status=""
                                 closed_or_not=""
                                 current_id="{{Auth::user()->id}}"></div>
                        @else
                            <div id="set_po_id" current_po_id="" current_po_status="draft_po" closed_or_not="0"
                                 current_id="{{Auth::user()->id}}"></div>
                        @endif

                        <div class="row">


                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="usr" class="col-md-6 col-lg-6 control-label">Journal Name </label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            @if(isset($journal))
                                                <input id="journal_name" type="text"
                                                       class="set_all_disabled get_all_date_input form-control required err_input job_name_error "
                                                       value="{{$journal->Journal_name}}" disabled>
                                            @else
                                                <input id="journal_name" type="text"
                                                       class="set_all_disabled get_all_date_input form-control required err_input job_name_error"
                                                       value="">
                                            @endif
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">


                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="usr" class="col-md-6 col-lg-6 control-label">Short Name </label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">

                                            @if(isset($journal))
                                                <input id="short_name" type="text"
                                                       class="set_all_disabled get_all_date_input form-control required err_input job_name_error "
                                                       value="{{$journal->journal_short_code}}" disabled>
                                            @else
                                                <input id="short_name" type="text"
                                                       class="set_all_disabled get_all_date_input form-control required err_input job_name_error"
                                                       value="">
                                            @endif

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>


                        <br>

                        <div class="row">

                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#document">Document</a></li>


                            </ul>

                            <div class="tab-content">

                                <div id="Sub_SalaryScale" class="tab-pane fade in active">
                                    <br>

                                    <div class="row">
                                        <table class="table table-bordred table-striped table-bordered table-striped table-hover table-condensed table-responsive "
                                               id="op_table">
                                            <thead>
                                            <th>Document Name</th>

                                            <th>Short Name</th>
                                            <th>Master Account</th>
                                            <th>Transaction Type</th>
                                            <th>Currency</th>
                                            <th>Edit</th>
                                            <th>Delete</th>
                                            </thead>
                                            <tbody>

                                            @if(isset($journal_documents))
                                                @foreach($journal_documents as $documents)
                                                    <tr class="">
                                                        <td td_type="input" class="document_name_v"
                                                            current_value="{{$documents->Document_Name}}">{{$documents->Document_Name}}</td>
                                                        <td td_type="input" class="document_short_name_v"
                                                            current_value="{{$documents->document_short_code}}">{{$documents->document_short_code}}</td>
                                                        <td td_type="search" data-select2-id="11" class="master_acc_v"
                                                            current_value="{{$documents->master_acc_id}}">{{$documents->child_name}}</td>

                                                        @if($documents->account_depet == 1)
                                                            <td td_type="search" data-select2-id="31"
                                                                class="tans_type_v" current_value="1">Dr
                                                            </td>
                                                        @else
                                                            <td td_type="search" data-select2-id="31"
                                                                class="tans_type_v" current_value="2">Cr
                                                            </td>
                                                        @endif

                                                        <td td_type="search" data-select2-id="36" class="currency_v"
                                                            current_value="{{$documents->Currency_id}}">{{$documents->Currency_name}}</td>
                                                        <td>
                                                            <button class="set_all_disabled btn btn-primary btn-xs edit_row "
                                                                    disabled>Edit
                                                            </button>
                                                        </td>
                                                        <td>
                                                            <button class="set_all_disabled btn btn-danger btn-xs delete_row "
                                                                    disabled>
                                                                <span class="lnr lnr-trash"></span>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                        <div class="row">
                                            <div class="col-md-4 col-lg-4 ">
                                                <button type="button" class="set_all_disabled btn btn-primary" id="add_row"
                                                        @if(isset($journal)) disabled @endif >Add Item
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>


                    </div>
                    <br>
                </div>
            </div>

        </div>
        <!-- END INPUTS -->


    </div>

@endsection


@section('custom_footer')


    <script src="{{asset('js/select2.min.js')}}"></script>

    <script>
        $('.side_sheets').addClass('hide');
        $('#accounting').removeClass('hide');

        $("#menu li").removeClass('active');
        $('#accounting_nav_bar').addClass('active');

        $('#pages ul li a').removeClass('active');
        $('#accounting_side_bar').addClass('active');

    </script>


    {{------------  add new row  ------------------------}}
    <script>
        $(document).on('click', '#add_row', function () {
            $(this).attr('disabled', true);

            var row = '<tr class= "added_now" >' +
                '<td td_type="input"><input class="document_name row_data form-control" data-role="input" type="text"/></td>' +
                '<td td_type="input"><input class="document_short_name row_data form-control" data-role="input" type="text"/></td>' +

                '<td td_type="search">' +
                '<select class="master_account form-control js-example-disabled-results">\n'
                + '<option value="">' + '' + '</option>'
                    @foreach($master_acc as $id=>$role)
                + '<option value="{{$id}}">' + '{{$role}}'
                    @endforeach
                + '</option>'
                + '</select></td>' +

                '<td td_type="search">' +
                '<select class="transaction_type form-control js-example-disabled-results">\n'
                + '<option value="">' + '' + '</option>'
                + '<option value="1">' + 'Dr' + '</option>'
                + '<option value="2">' + 'Cr' + '</option>'
                + '</select></td>' +

                '<td td_type="search">' +
                '<select class="currency_value form-control js-example-disabled-results">\n'
                + '<option value="">' + '' + '</option>'
                    @foreach($currencies as $id=>$role)
                + '<option value="{{$id}}">' + '{{$role}}'
                    @endforeach
                + '</option>'
                + '</select></td>'


                + '<td>' + '<button class="set_all_disabled btn btn-primary btn-xs Save_row">' + 'Save' + '</button>'
                + '</td>'
                + '<td>'
                + '<button class="set_all_disabled btn btn-danger btn-xs delete_row"  >' + '<span class="lnr lnr-trash"></span>' + '</button>'
                + '</td>'

                + '</tr>';
            $("#op_table").prepend(row);
            $(".js-example-disabled-results").select2();
            //get_master_acc_drop_down($('.master_account'));

            //$(".po_values").siblings('.select2').css('width', '71px');

        });

    </script>

    {{--------   save journal row  --------------}}
    <script>
        $(document).on("click", ".Save_row", function () {

            var empty = false;
            var call_save = false;
            var input = $(this).parents("tr").find('input[type="text"]');
            var selectors = $(this).parents("tr").find('select');


            input.each(function () {
                if (!$(this).val() || $(this).val() == "") {
                    $(this).addClass("error");
                    empty = true;
                } else {
                    $(this).removeClass("error");
                }
            });

            selectors.each(function () {
                if (!$(this).val() || $(this).val() == "") {
                    $(this).addClass("error");
                    empty = true;
                } else {
                    $(this).removeClass("error");
                }
            });

            console.log('empty ' + empty);

            $(this).parents("tr").find(".error").first().focus();

            console.log($(input[0]).val() + "  " + $(input[1]).val());

            var not_unique_name = $('.document_name_v:contains(' + $(input[0]).val() + ')').length;

            var not_unique_code = $('.document_short_name_v:contains(' + $(input[1]).val() + ')').length;


            if (!empty) {
                if (not_unique_name) {
                    alert('Document Name aleady exist');
                    return false;
                }
                if (not_unique_code) {
                    alert('Short Name aleady exist');
                    return false;
                }
                else {
                    $(this).prop('disabled', true);
                    save_table_effect($(this));
                }


            }


        });

        function save_table_effect(thiss) {


            var document_name = thiss.parents("tr").find('.document_name').val();
            var short_name = thiss.parents("tr").find('.document_short_name').val();

            var current_po_id = $('#set_po_id').attr('current_po_id');


            console.log('document_name : ' + document_name + 'short_name : ' + short_name);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/check_unique/',
                dataType: 'json',
                type: 'get',
                data: {

                    Document_Name: document_name,
                    Short_Name: short_name,

                    current_po_id: current_po_id

                },

                success: function (response) {

                    console.log(response);
                    if (response['error']) {
                        $('.err_input').removeClass('red-border');
                        $('.err_input').parent().find('.select2').removeClass('red-border');

                        $('.errorMessage1').show();
                        $.each(response['error'], function (key, val) {
                            //alert(key + val);

                            console.log(key + '_error');
                            $("." + key + '_error').addClass('red-border');
                            $('.' + key + '_error').parent().find('.select2').addClass('red-border');

                            //console.log(val[0]);

                        })
                        thiss.prop('disabled', false);
                    }

                    else {

                        console.log('passed')

                        /*--------------  all select ------------*/
                        var select_array = ['master_acc_v', 'tans_type_v', 'currency_v'];
                        var r = 0;
                        var all_dowp_down = thiss.parents("tr").find('select');
                        all_dowp_down.each(function () {
                            var value_select = $(this).val();
                            var text_select = $(this).find('option:selected').text();
                            $(this).parent("td").addClass(select_array[r]).attr('current_value', value_select).html(text_select);
                            r++;

                        });
                        /*--------- all input  ------------------*/
                        var input_array = ['document_name_v', 'document_short_name_v'];
                        var all_input = thiss.parents("tr").find('input[type="text"]');

                        var i = 0;
                        all_input.each(function () {

                            var value_select = $(this).val();
                            $(this).parent("td").addClass(input_array[i]).attr('current_value', value_select).html(value_select);

                            i++;
                        });
                        /*--------------------------*/

                        if (thiss.parents("tr").hasClass('added_now')) {
                            thiss.parents("tr").removeClass('added_now');

                        }
                        if (thiss.parents("tr").hasClass('edited_now')) {
                            thiss.parents("tr").removeClass('edited_now');

                        }

                        //|| thiss.siblings().find('.create_edit_v') ){
                        //console.log(thiss.closest('tr').attr('id'))


                        if ($('.added_now').length == 0) {
                            $("#add_row").removeAttr("disabled");
                        }

                        thiss.text('Edit');
                        thiss.removeClass('Save_row');
                        thiss.addClass('edit_row');
                        thiss.prop('disabled', false);

                    }

                }

            });


        }


    </script>

    {{----------------------  edit row --------------------}}
    <script>
        $(document).on("click", ".edit_row", function () {
            if (!$('#op_table tr').hasClass('added_now')) {
                var input_class_array = ['document_name', 'document_short_name'];
                var s = 0;

                $('#add_row').prop('disabled', true);
                $(this).parents("tr").addClass('edited_now');

                $(this).parents("tr").find("td:not(:last-child)").each(function () {
                    if ($(this).attr('td_type') == 'input') {
                        $(this).html('<input class="' + input_class_array[s] + ' row_data  form-control" data-role="input" type="text" value="' + $(this).text() + '"/>');
                        s++;
                    }

                    else if ($(this).attr('td_type') == 'search') {
                        if ($(this).hasClass('master_acc_v')) {
                            $(this).html('<select class="master_account form-control js-example-disabled-results">\n'
                                + '<option value="">' + '' + '</option>'
                                    @foreach($master_acc as $id=>$role)
                                + '<option value="{{$id}}">' + '{{$role}}'
                                    @endforeach
                                + '</option>'
                                + '</select>');

                            var selected_v = $(this).attr('current_value');
                            $(this).find(".master_account").val(selected_v);
                            $(".master_account").select2();
                            $(".master_account").siblings('.select2').css('width', '71px');
                        }

                        else if ($(this).hasClass('tans_type_v')) {
                            $(this).html('<select class="transaction_type form-control js-example-disabled-results">\n'
                                + '<option value="">' + '' + '</option>'
                                + '<option value="1">' + 'Dr' + '</option>'
                                + '<option value="2">' + 'Cr' + '</option>'
                                + '</select>');

                            var selected_v = $(this).attr('current_value');
                            $(this).find(".transaction_type").val(selected_v);
                            $(".transaction_type").select2();
                            $(".transaction_type").siblings('.select2').css('width', '71px');
                        }

                        else if ($(this).hasClass('currency_v')) {
                            $(this).html('<select class="currency_value form-control js-example-disabled-results">\n'
                                + '<option value="">' + '' + '</option>'
                                    @foreach($currencies as $id=>$role)
                                + '<option value="{{$id}}">' + '{{$role}}'
                                    @endforeach
                                + '</option>'
                                + '</select>');

                            var selected_v = $(this).attr('current_value');
                            $(this).find(".currency_value").val(selected_v);
                            $(".currency_value").select2();
                            $(".currency_value").siblings('.select2').css('width', '71px');
                        }

                    }


                });

                $("#add_row").attr("disabled", "disabled");
                $(this).text('Save');
                $(this).removeClass('edit_row');
                $(this).addClass('Save_row');

            }
        });

    </script>


    <!--delete row in table -->
    <script>

        $(document).on('click', '.delete_row', function () {

            $(this).hide(function () {
                $(this).closest('td').append('<button class="btn btn-danger set_all_disabled" type="button" id="yes" style="display: none;">yes</button>' +
                    '<button class="btn btn-default set_all_disabled" type="button" id="no" style="display: none;">no</button>');
                $(this).closest('td').find("#yes").show('slow').delay(1000);
                $(this).closest('td').find("#no").show('slow').delay(1000);

            });
        });

        $(document).on('click', '#yes', function () {

            $(this).closest('tr').remove();
            if ($('.added_now').length == 0) {
                $("#add_row").removeAttr("disabled");
            }
        });

        $(document).on('click', '#no', function () {

            console.log('noo ');

            $(this).closest('td').find(".delete_row").show('slow').delay(1000);
            $(this).closest('td').find("#yes").hide('slow').delay(1000).remove();
            $(this).closest('td').find("#no").hide('slow').delay(1000).remove();
            /*
            $(this).closest('td').find("#yes").hide('slow').delay(1000);
            $(this).closest('td').find("#no").hide('slow').delay(1000);*/


        });


    </script>


    {{-------------------- save new journal --------------------}}
    <script>

        $('.alert-autocloseable-success').hide();


        $(document).on('click', '#save_po', function () {
            var rowCount = $('#op_table tbody tr:not(.added_now)').length;
            $(".errorMessage2").hide();
            $(".errorMessage1").hide();
            if (rowCount == 0) {

                $(".errorMessage2").show();
            }


            if (!$('#op_table tr').hasClass('edited_now') && rowCount > 0) {
                $("#op_table tbody .added_now").remove();
                $('#save_po').attr('disabled', 'disabled');
                $('.set_all_disabled').prop('disabled', true);


                var journal_name = $("#journal_name").val();
                var short_name = $("#short_name").val();

                var input = $('#op_table > tbody  > tr');
                var row = [];

                input.each(function () {

                    if (!$(this).hasClass('added_now')) {
                        row.push({
                            "document_name": $(this).find('.document_name_v').attr('current_value'),
                            "document_short_name": $(this).find('.document_short_name_v').attr('current_value'),
                            "master_acc": $(this).find('.master_acc_v').attr('current_value'),
                            "tans_type": $(this).find('.tans_type_v').attr('current_value'),
                            "currency": $(this).find('.currency_v').attr('current_value')
                        });
                    }

                });


                var current_user_id = $('#set_po_id').attr('current_id');
                var current_po_id = $('#set_po_id').attr('current_po_id');
                console.log(row);

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/add_new_journal/',
                    dataType: 'json',
                    type: 'get',
                    data: {

                        journal_name: journal_name,
                        short_name: short_name,
                        table_rows: row,

                        current_user_id: current_user_id,
                        current_po_id: current_po_id
                    },

                    success: function (response) {

                        console.log(response);
                        if (response['error']) {
                            $('.err_input').removeClass('red-border');
                            $('.err_input').parent().find('.select2').removeClass('red-border');

                            $('.errorMessage1').show();
                            $.each(response['error'], function (key, val) {
                                //alert(key + val);

                                console.log(key + '_error');
                                $("." + key + '_error').addClass('red-border');
                                $('.' + key + '_error').parent().find('.select2').addClass('red-border');

                                //console.log(val[0]);

                            })
                            $('#save_po').removeAttr('disabled');
                            $('.set_all_disabled').prop('disabled', false);

                        }

                        else {

                            $('.errorMessage1').hide();
                            $('.err_input').removeClass('red-border');
                            $('.err_input').parent().find('.select2').removeClass('red-border');
                            $('#save_po').text("Edit");
                            $('#save_po').attr('id', 'edit_po');
                            $('#add_row').prop('disabled', true);
                            $('#set_po_id').attr('current_po_id', response['current_po_id']);
                            $('.set_all_disabled').prop('disabled', true);
                            $('#creat_dep').removeClass('hide');

                            $('#more').removeClass('hide');


                            $('#edit_po').removeAttr('disabled');
                            //po-active
                            console.log(response);

                            {{----SUCCESS ON SAVE -------}}
                            $('#autoclosable-btn-success').prop("disabled", true);
                            $('.alert-autocloseable-success').show("slow");

                            $('.alert-autocloseable-success').delay(5000).fadeOut("slow", function () {
                                // Animation complete.
                                $('#autoclosable-btn-success').prop("disabled", false);
                            });
                            {{----SUCCESS ON SAVE -------}}
                        }

                    },
                    error: function (response) {
                        alert(' Cant Save This Documents !');
                        $('#save_po').prop('disabled', false);
                    }

                });
            }
        });

    </script>



    {{---------------------  edit journal ----------------------}}
    <script>
        $(document).on('click', '#edit_po', function () {
            $('#edit_po').text("Save");
            $('#edit_po').attr('id', 'save_po');

            $('.set_all_disabled').prop('disabled', false);
            $('#add_row').prop('disabled', false);

            $('#creat_dep').addClass('hide');

            $('#more').addClass('hide');
        });
    </script>


    {{----------------  create new journal ------------------}}
    <script>
        $(document).on('click', '#creat_dep', function () {

            window.location.href = '/journals';

        });
    </script>

    {{---------------------- delete journal ------------------------------}}

    <script>
        $(document).on('click', '#delete_po', function () {
            var cur_po_id = $('#set_po_id').attr('current_po_id');

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/delete_journal/',
                dataType: 'json',
                type: 'get',
                data: {
                    cur_po_id: cur_po_id,
                },

                success: function (response) {

                    window.location.href = '/journals';
                }

            });

        });
    </script>

    {{---- dublicate dep ---}}
    <script>
        $(document).on('click', '#dublicate_dep', function () {

            $('#set_po_id').attr('current_po_id', "");
            /*$('#set_po_id').attr('current_po_status',"draft_po");*/
            /*$('#set_po_id').attr('closed_or_not',0);*/

            $('.set_all_disabled').prop('disabled', false);
            $('#add_row').prop('disabled', false);

            $('#creat_dep').addClass('hide');
            $('#more').addClass('hide');


            $('#edit_po').prop('disabled', false);
            $('#edit_po').text("Save");
            $('#edit_po').attr('id', 'save_po');
            $('#save_po').prop('disabled', false);
        });
    </script>


    {{-----------   on leave page -------------------}}
    <script type='text/javascript'>

        $(window).bind('beforeunload', function () {
            if ($('#save_po').length) {
                return 'Are you sure you want to leave?';
            }

        });


    </script>


    {{-----------------------print----------------}}
    <script>

        $(document).on('click', '#print', function () {

            //  window.location.href = 'get_po2/276'

            var id = $('#set_po_id').attr('current_po_id');
            window.open('/get_journals_rpt/' + id + '', '_blank');


        });

    </script>


@endsection