@extends('layouts.main_app')

@section('main_content')
    <link href="{{asset('css/fastselect.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/select2.min.css')}}" rel="stylesheet" />

    <style>
        input.col-md-4.text-center {

            border:0;
            background-color: #fff;
        }



        .collapse > .row>.col-md-4>.btn, .collapse > .row>.col-md-4>.dropdown>.btn {
            margin-top: 0;

        }



        .third-nav{
            background-color:#eeeeee;
        }





        .myselect2{
            width:171px;
        }

        span.select2.select2-container.select2-container--default{
            width: 100%!important;
        }
        tbody th, tbody td {
            text-align: center;
        }
    </style>
    <div class="main">
        <!-- Navbar-content -->
        <nav class="navbar navbar-default navbar2">

            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                    <!--breadcrumbs-->
                    <div class="row">

                        <div class="col-md-10 col-lg-10">

                        <ol class="breadcrumb breadcrumb_nav">
                            <li><a href="#">Fees Head</a></li>
                            <li class="active">
                                @if(isset($department))
                                    @if($department->fees_id != 0)
                                        {{$department->fees_id}}
                                    @else
                                        Draft
                                    @endif
                                @endif
                            </li>
                        </ol>
                        </div>

                        @if(isset($department))
                            <div class="col-md-2 col-lg-2 col-sm-4 col-xs-4">
                                @if(@\App\fees::where('fees_id','<',$department->fees_id)->orderBy('fees_id','desc')->first()->fees_id)
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <a href="/fees/{{@\App\fees::where('fees_id','<',$department->fees_id)->orderBy('fees_id','desc')->first()->fees_id}}"><span
                                                    class="glyphicon glyphicon-arrow-left"></span>
                                        </a>
                                    </div>
                                @endif
                                @if(@\App\fees::where('fees_id','>',$department->fees_id)->orderBy('fees_id','asc')->first()->fees_id)
                                    <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                                        <a href="/fees/{{@\App\fees::where('fees_id','>',$department->fees_id)->orderBy('fees_id','asc')->first()->fees_id}}">
                                    <span class="glyphicon glyphicon-arrow-right">
                                </span>
                                        </a>
                                    </div>
                                @endif
                            </div>
                        @endif


                    </div>


                    <!--End breadcrumbs-->
                    @if(isset($department))
                        <div id="set_po_id" current_po_id="{{$department->fees_id}}" current_po_status=""
                             closed_or_not="" current_id="{{Auth::user()->id}}"></div>
                    @else
                        <div id="set_po_id" current_po_id="" current_po_status="draft_po" closed_or_not="0"
                             current_id="{{Auth::user()->id}}"></div>
                    @endif
                    <div class="row">
                        <div class="col-md-4 col-lg-4 buttons">
                            @if(isset($department))
                                <button id="edit_dep" type="button" class="btn btn-danger ">Edit</button>
                                <button type="button" class="btn" id="creat_dep">Create</button>
                            @else
                                <button id="save_dep" type="button" class="btn btn-danger ">Save</button>
                                {{--<button id="edit_user" type="button" class="btn btn-danger " >Edit</button>--}}
                                <button type="button" class="hide btn" id="creat_dep">Create</button>
                            @endif
                        </div>

                        <div class="col-md-4">
                            @if(isset($department))
                                <div class="dropdown">
                                    <button class=" btn btn-primary  dropdown-toggle" type="button"
                                            data-toggle="dropdown" id="more" hidden>More
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" id="delete_dep">Delete</a></li>
                                        {{--<li><a href="#">Export</a></li>--}}
                                        <li><a href="#" id="dublicate_dep">Duplicate</a></li>
                                        <li><a href="#" id="print">Print</a></li>

                                    </ul>
                                </div>
                            @else
                                <div class="dropdown">
                                    <button class="hide btn btn-primary  dropdown-toggle" type="button"
                                            data-toggle="dropdown" id="more" hidden>More
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" id="delete_dep">Delete</a></li>
                                        <li><a href="#">Export</a></li>
                                        <li><a href="#" id="dublicate_dep">Duplicate</a></li>
                                        <li><a href="#" id="print">Print</a></li>

                                    </ul>
                                </div>
                            @endif
                        </div>
                        <!--search-->
                        <!--
                         <div class="col-md-4">
                             <form class="navbar-form navbar-right" >
                                 <div class="input-group form-group">
                                     <input type="text" value="" class="form-control" placeholder="Search ...">
                                 </div>
                             </form>
                         </div>
                         -->
                    </div> <!--collapse -->

                </div><!--collapse -->

            </div>
        </nav>
        <!-- End Navbar-content -->

        <!-- Success messages -->
        <div class="alert alert-success alert-autocloseable-success" style="display:none;position:absolute;">
            fees Details Saved Successfully!

        </div>

        <!--end Success messages -->


        <!-- failuer messages -->
        <div class="alert alert-danger errorMessage1" style="display:none;position:absolute;">
            Your should fill all fields!

        </div>
        <!-- MAIN CONTENT -->
        <div class="main-content">

            <div class="container-fluid">

                <!-- INPUTS -->
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Fees Head</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="fees_name" class="col-md-6 col-lg-6 control-label">Fees Name </label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            @if(isset($department))
                                                <input id="fees_name" type="text"
                                                       class="set_all_disabled get_all_date_input form-control required err_input department_code_error"
                                                       value="{{$department->Fees_Name}}" disabled>
                                            @else
                                                <input id="fees_name" type="text"
                                                       class="set_all_disabled get_all_date_input form-control required  err_input department_code_error"
                                                       value="">
                                            @endif

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                        <br>
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="fees_type" class="col-md-6 col-lg-6 control-label">Fees Type </label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <select id="fees_type" name="fees_type"
                                                    class="get_all_selectors set_all_disabled select_2_enable form-control err_input required cost_center_error"
                                                    @if(isset($department)) disabled @endif >
                                                <option value=""></option>
                                                @foreach($fees_type as $id=>$role)
                                                    @if(isset($department))
                                                        @if($department->Fees_type == $id)
                                                            <option selected value="{{$id}}">{{$role}}</option>
                                                        @else
                                                            <option value="{{$id}}">{{$role}}</option>
                                                        @endif
                                                    @else
                                                        <option value="{{$id}}">{{$role}}</option>
                                                    @endif
                                                @endforeach
                                            </select>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="account_name" class="col-md-6 col-lg-6 control-label">Account Name </label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <select id="account_name" name="account_name"
                                                    class="get_all_selectors set_all_disabled select_2_enable form-control err_input required cost_center_error"
                                                    @if(isset($department)) disabled @endif >
                                                <option value=""></option>
                                                @foreach($accounts as $id=>$role)
                                                    @if(isset($department))
                                                        @if($department->account_id == $id)
                                                            <option selected value="{{$id}}">{{$role}}</option>
                                                        @else
                                                            <option value="{{$id}}">{{$role}}</option>
                                                        @endif
                                                    @else
                                                        <option value="{{$id}}">{{$role}}</option>
                                                    @endif
                                                @endforeach
                                            </select>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="journal_name" class="col-md-6 col-lg-6 control-label">Journal Name </label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <select id="journal_name" name="journal_name"
                                                    class="get_all_selectors set_all_disabled select_2_enable form-control err_input required cost_center_error"
                                                    @if(isset($department)) disabled @endif >
                                                <option value=""></option>
                                                @foreach($journals as $id=>$role)
                                                    @if(isset($department))
                                                        @if($department->journal_id == $id)
                                                            <option selected value="{{$id}}">{{$role}}</option>
                                                        @else
                                                            <option value="{{$id}}">{{$role}}</option>
                                                        @endif
                                                    @else
                                                        <option value="{{$id}}">{{$role}}</option>
                                                    @endif
                                                @endforeach
                                            </select>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>






                    </div>
                    <br>
                </div>
            </div>

        </div>
        <!-- END INPUTS -->


    </div>

@endsection


@section('custom_footer')
    <script src="{{asset('js/item_name_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/tax_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/select2.min.js')}}"></script>




    <!-----------------active link in nav and active link in sidebar ---->
    <script>
        $('.side_sheets').addClass('hide');
        $('#admission').removeClass('hide');

        $("#menu li").removeClass('active');
        $('#admission_nav_bar').addClass('active');

        $('#pages ul li a').removeClass('active');
        $('#fees_side_bar').addClass('active');

    </script>
    {{-------------------- save new department --------------------}}
    <script>
        $('.alert-autocloseable-success').hide();

        $(document).on('click', '#save_dep', function () {

            $('#save_dep').attr('disabled', 'disabled');

            $('#add_row').addClass("disabled");


            var fees_name = $('#fees_name').val();
            var fees_type = $('#fees_type').val();
            var account_name = $('#account_name').val();
            var journal_name = $('#journal_name').val();

            var current_user_id = $('#set_po_id').attr('current_id');
            var current_po_id = $('#set_po_id').attr('current_po_id');

            console.log('current_po_id' + current_po_id)
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/fees_save/',
                dataType: 'json',
                type: 'get',
                data: {

                    fees_name: fees_name,
                    fees_type: fees_type,
                    account_name: account_name,

                    journal_name: journal_name,
                    current_po_id: current_po_id,
                    cur_user_id: current_user_id,

                },

                success: function (response) {

                    console.log(fees_name +"  "+fees_type+" "+account_name );
                    if (response['error']) {
                        $('.err_input').removeClass('red-border');
                        $('.err_input').parent().find('.select2').removeClass('red-border');
                        $('.errorMessage1').show();
                        $.each(response['error'], function (key, val) {
                            //alert(key + val);

                            console.log(key + '_error');
                            $("." + key + '_error').addClass('red-border');
                            $('.' + key + '_error').parent().find('.select2').addClass('red-border');
                            //console.log(val[0]);

                        })
                        $('#save_dep').removeAttr('disabled');

                    }

                    else {
                        $('#set_po_id').attr('current_po_id', response['current_po_id']);
                        $('.errorMessage1').hide();
                        $('.err_input').removeClass('red-border');
                        $('.err_input').parent().find('.select2').removeClass('red-border');
                        $('.breadcrumb').removeClass('po-active');


                        $('#save_dep').text("Edit");
                        $('#save_dep').attr('id', 'edit_dep');

                        $('.set_all_disabled').prop('disabled', true);

                        $('#creat_dep').removeClass('hide');

                        $('#more').removeClass('hide');


                        $('#edit_dep').removeAttr('disabled');
                        //po-active
                        console.log(response);

                        {{----SUCCESS ON SAVE -------}}
                        $('#autoclosable-btn-success').prop("disabled", true);
                        $('.alert-autocloseable-success').show("slow");

                        $('.alert-autocloseable-success').delay(3000).fadeOut("slow", function () {
                            // Animation complete.
                            $('#autoclosable-btn-success').prop("disabled", false);
                        });
                        {{----SUCCESS ON SAVE -------}}
                    }

                }

            });

        });

    </script>


    <script>
        $(document).on('click', '#edit_dep', function () {
            $('#edit_dep').text("Save");
            $('#edit_dep').attr('id', 'save_dep');
            $('#add_row').prop('disabled', false);

            $('.set_all_disabled').prop('disabled', false);


            $('#creat_dep').addClass('hide');

            $('#more').addClass('hide');

        });
    </script>

    <script>
        $(document).on('click','#edit_user',function(){

            $(this).text("Save");
            $(this).attr('id','save_user');
            $("input[type=text]").attr('disabled', false).css({
                'border': '#eaeaea solid 1px',

                'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                'border-radius':'2px',
                'background-color':'#fff',
                'width':'150px',
                'margin-left':'15px'
            }).focus();

            $("input[type=password]").attr('disabled',false).css({
                'border': '#eaeaea solid 1px',

                'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                'border-radius':'2px',
                'background-color':'#fff',
                'margin-left':'15px'
            });

            $("input[type=email]").attr('disabled',false).css({
                'border': '#eaeaea solid 1px',

                'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                'border-radius':'2px',
                'background-color':'#fff',
                'margin-left':'15px'
            });



            $("select").removeAttr('disabled');
            $("input[type=radio]").attr('disabled', false);
            $("input[type=password]").attr('disabled', false);
            $("input[type=email]").attr('disabled', false);

        });


    </script>



    {{---------------------------- create new  ------------------------------}}
    <script>
        $(document).on('click', '#creat_dep', function () {
            $("#add_row").prop("disabled",false)
            /*
            $('#edit_dep').prop('disabled', false);
            $('#edit_dep').text("Save");
            $('#edit_dep').attr('id', 'save_dep');
            $('#save_dep').prop('disabled', false);

            $('.set_all_disabled').prop('disabled', false);

            $('#set_po_id').attr('current_po_id', "");
            /!*$('#set_po_id').attr('current_po_status',"draft_po");*!/
            $('#creat_dep').addClass('hide');
            $('#more').addClass('hide');

            $('.get_all_selectors').val('');

            $(".get_all_selectors option[value='']").attr('selected', true);
            $('.get_all_date_input').val('');
            $('.select2-selection__rendered').html('');
*/
            window.location.href = '/fees';
        });
    </script>

    <!--End add row to table -->


    {{---- dublicate dep ---}}
    <script>
        $(document).on('click', '#dublicate_dep', function () {

            $('#set_po_id').attr('current_po_id', "");
            /*$('#set_po_id').attr('current_po_status',"draft_po");*/
            /*$('#set_po_id').attr('closed_or_not',0);*/

            $('.set_all_disabled').prop('disabled', false);


            $('#creat_dep').addClass('hide');
            $('#more').addClass('hide');


            $('#edit_dep').prop('disabled', false);
            $('#edit_dep').text("Save");
            $('#edit_dep').attr('id', 'save_dep');
            $('#save_dep').prop('disabled', false);
        });
    </script>
    <!--Edit Row in Table -->

    {{----------------------  delete department ------------------------------------}}

    <script>
        $(document).on('click', '#delete_dep', function () {
            var cur_po_id = $('#set_po_id').attr('current_po_id');

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/delete_fees/',
                dataType: 'json',
                type: 'get',
                data: {
                    cur_po_id: cur_po_id,
                },

                success: function (response) {

                    window.location.href = '/fees';
                }

            });

        });
    </script>


    <script>

        $(document).on('click','#editbtn',function(){
            var currentTD = $(this).parents('tr').find('td');
            if ($(this).html() == 'Edit') {
                currentTD = $(this).parents('tr').find('td');
                $.each(currentTD, function () {




                    $("input[type=text]").attr('disabled', false).css({
                        'border': '#eaeaea solid 1px',
                        'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                        'border-radius':'2px',
                        'background-color':'#fff',
                        'width':'150px',
                        'margin-left':'15px'
                    }).focus();

                    $("input[type=number]").attr('disabled',false).css({
                        'border': '#eaeaea solid 1px',
                        'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                        'border-radius':'2px',
                        'background-color':'#fff',
                        'margin-left':'15px'
                    });
                    $("select").removeAttr('disabled');
                });
            } else {
                $.each(currentTD, function () {
                    $("input[type=text]").attr('disabled', true).css({
                        'border': '#eaeaea solid 1px',

                        'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                        'border-radius':'2px',
                        'background-color':'#fff',
                        'width':'150px',
                        'margin-left':'15px'
                    }).focus();
                    $("input[type=number]").attr('disabled',true).css({
                        'box-shadow':'none',
                        'border': 'none',
                        'background-color':'fff',
                        'text-align':'center'
                    });
                    $("select").prop("disabled", true);
                });

            }

            $(this).html($(this).html() == 'Edit' ? 'Save' : 'Edit')

        });

    </script>




    {{-----------   on leave page -------------------}}
    <script type='text/javascript'>

        $(window).bind('beforeunload', function(){
            if($('#save_dep').length){
                return 'Are you sure you want to leave?';
            }

        });


    </script>


    <!--select salary scale -->

    <script>
        $(".myselect1").select2({
            width: 'resolve'

        });

        $(".myselect2").select2({
            width: 'resolve',


        });
        $(".myselect3").select2({
            width: 'resolve',


        });
        $(".myselect4").select2({
            width: 'resolve',


        });
        $(".myselect5").select2({
            width: 'resolve'

        });

        $(".myselect6").select2({
            width: 'resolve',


        });
        $(".myselect7").select2({
            width: 'resolve',


        });
        $(".myselect8").select2({
            width: 'resolve',


        });

    </script>
    <script>

        $(document).on('click', '#print', function () {

            //  window.location.href = 'get_po2/276'

            var id = $('#set_po_id').attr('current_po_id');
            window.open('/get_fees_rpt/' + id + '', '_blank');


        });

    </script>


@endsection