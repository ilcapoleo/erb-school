@extends('layouts.main_app')

@section('main_content')
    <link href="{{asset('css/fastselect.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/select2.min.css')}}" rel="stylesheet" />

    <style>
        input.col-md-4.text-center {

            border:0;
            background-color: #fff;
        }



        .collapse > .row>.col-md-4>.btn, .collapse > .row>.col-md-4>.dropdown>.btn {
            margin-top: 0;

        }



        .third-nav{
            background-color:#eeeeee;
        }





        .myselect2{
            width:171px;
        }

        span.select2.select2-container.select2-container--default{
            width: 100%!important;
        }
        tbody th, tbody td {
            text-align: center;
        }
    </style>
    <div class="main">
        <!-- Navbar-content -->
        <nav class="navbar navbar-default navbar2">

            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                    <!--breadcrumbs-->
                    <div class="row">

                        <div class="col-md-10 col-lg-10">

                        <ol class="breadcrumb breadcrumb_nav">
                            <li><a href="#">Vendors</a></li>
                            <li class="active"></li>
                        </ol>
                        </div>

                        <div class="col-md-2 col-lg-2">
                            <div class="col-md-6">
                                <a href="#">
                                    <span class="glyphicon glyphicon-arrow-left"></span>
                                </a>
                            </div>

                            <div class="col-md-6">
                                <a href="#">
                                    <span class="glyphicon glyphicon-arrow-right">
                                </span>
                                </a>
                            </div>





                        </div>



                        </div>


                    <!--End breadcrumbs-->
                    <div class="row">
                        <div class="col-md-4 col-lg-4 buttons">
                            <button id="edit_user" type="button" class="btn btn-danger " >Edit</button>
                            <button type="button" class="btn" id="creat" >Create</button>
                        </div>

                        <div class="col-md-4 col-lg-4">
                            <div class="dropdown">
                                <button class="btn btn-primary  dropdown-toggle" type="button" data-toggle="dropdown" id="more"  hidden>More
                                    <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#" id="delete">Delete</a></li>
                                    <li><a href="#">Export</a></li>
                                    <li><a href="#">Duplicate</a></li>
                                    <li><a href="#">Print</a></li>

                                </ul>
                            </div>
                        </div>
                        <!--search-->
                        <!--
                         <div class="col-md-4">
                             <form class="navbar-form navbar-right" >
                                 <div class="input-group form-group">
                                     <input type="text" value="" class="form-control" placeholder="Search ...">
                                 </div>
                             </form>
                         </div>
                         -->
                    </div> <!--collapse -->

                </div><!--collapse -->

            </div>
        </nav>
        <!-- End Navbar-content -->
        <!-- MAIN CONTENT -->
        <div class="main-content">

            <div class="container-fluid">

                <!-- INPUTS -->
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Vendors</h3>
                    </div>
                    <div class="panel-body">

                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="vendor_code" class="col-md-6 col-lg-6 control-label">Vendor Code</label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <input  class="form-control required"  type="text" id="vendor_code" >

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="vendor_name" class="col-md-6 col-lg-6 control-label">Vendor Name </label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <input  class="form-control required"  type="text" id="vendor_name" >

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">

                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="arabic_vendor_name" class="col-md-6 col-lg-6 control-label">Arabic Vendor Name</label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <input  class="form-control required"  type="text" id="arabic_vendor_name" >

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="usr" class="col-md-6 col-lg-6 control-label">Address </label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <textarea class="form-control" rows="3" id="comment"></textarea>


                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="usr" class="col-md-6 col-lg-6 control-label">Arabic Address  </label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <textarea class="form-control" rows="3" id="comment"></textarea>


                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="url" class="col-md-6 col-lg-6 control-label">Website </label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <input type="url" name="url" class="form-control " id="url"
                                                   pattern="https://.*"   />

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="url" class="col-md-6 col-lg-6 control-label">E-mail </label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <input type="email" class="form-control "  id="email">


                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="contact_school" class="col-md-6 col-lg-6 control-label">Contact School Person </label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <select id="contact_school" class="myselect1 "   style="width: 50%" name="cost_center">

                                                <option>Ahmed ismail</option>

                                                <option >Ali atef</option>

                                                <option >Mohammed Ashraf</option>

                                                <option >Omar ali</option>


                                            </select>
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="contact_vendor_person" class="col-md-6 col-lg-6 control-label">Contact Vendor Person</label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <input  class="form-control "  type="text" id="contact_vendor_person" >

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="phonenum" class="col-md-6 col-lg-6 control-label">Phone 1 </label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <input id="phonenum" type="tel" class="form-control " pattern="^\d{4}-\d{3}-\d{4}$" required >
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="phonenum2" class="col-md-6 col-lg-6 control-label">Phone 2 </label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <input id="phonenum2" type="tel" class="form-control " pattern="^\d{4}-\d{3}-\d{4}$" required >
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="tax_file_number" class="col-md-6 col-lg-6 control-label">Tax File Number</label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <input  class="form-control "  type="text" id="tax_file_number" >

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="tax_office_name" class="col-md-6 col-lg-6 control-label">Tax Office Name</label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <input  class="form-control "  type="text" id="tax_office_name" >

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="tax_office_code" class="col-md-6 col-lg-6 control-label">Tax Office Code</label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <input  class="form-control "  type="text" id="tax_office_code" >

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="reg_code" class="col-md-6 col-lg-6 control-label">Commercial Regesteration Code</label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <input  class="form-control "  type="text" id="reg_code" >

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="tax_reg_number" class="col-md-6 col-lg-6 control-label">Tax Regesteration Number</label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <input  class="form-control "  type="text" id="tax_reg_number" >

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="tax_reg_number" class="col-md-6 col-lg-6 control-label">Vat Regesteration Number</label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <input  class="form-control "  type="text" id="tax_reg_number" >

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="acc_name" class="col-md-6 col-lg-6 control-label">Account Name</label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <select id="acc_name" class="myselect2 required"   style="width: 50%" name="acc_name">

                                                <option>Acc-1</option>

                                                <option >Acc-2</option>

                                                <option >Acc-3</option>

                                                <option >Acc-4</option>


                                            </select>
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>


                    </div>
                    <br>
                </div>
            </div>

        </div>
        <!-- END INPUTS -->


    </div>

@endsection


@section('custom_footer')
    <script src="{{asset('js/item_name_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/tax_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/select2.min.js')}}"></script>




    <!-----------------active link in nav and active link in sidebar ---->
    <script>
        $('.side_sheets').addClass('hide');
        $('#accounting').removeClass('hide');

        $("#menu li").removeClass('active');
        $('#accounting_nav_bar').addClass('active');

        $('#pages ul li a').removeClass('active');
        $('#vendor_side_bar').addClass('active');

    </script>

    <!--edit fields -->


    <script>
        $(document).on('click','#edit_user',function(){

            $(this).text("Save");
            $(this).attr('id','save_user');
            $("input[type=text]").attr('disabled', false).css({
                'border': '#eaeaea solid 1px',

                'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                'border-radius':'2px',
                'background-color':'#fff',
                'width':'150px',
                'margin-left':'15px'
            }).focus();

            $("input[type=password]").attr('disabled',false).css({
                'border': '#eaeaea solid 1px',

                'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                'border-radius':'2px',
                'background-color':'#fff',
                'margin-left':'15px'
            });

            $("input[type=email]").attr('disabled',false).css({
                'border': '#eaeaea solid 1px',

                'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                'border-radius':'2px',
                'background-color':'#fff',
                'margin-left':'15px'
            });



            $("select").removeAttr('disabled');
            $("input[type=radio]").attr('disabled', false);
            $("input[type=password]").attr('disabled', false);
            $("input[type=email]").attr('disabled', false);

        });


    </script>


    <!--add row to table -->
    <script>
        $("#add_row").click(function () {

            $("#myTable2").each(function () {

                var tds = '<tr>';
                jQuery.each($('tr:last td', this), function () {
                    tds += '<td>' + $(this).html() + '</td>';
                });
                tds += '</tr>';
                if ($('tbody', this).length > 0) {
                    $('tbody', this).append(tds);
                } else {
                    $(this).append(tds);
                }
            });
        });


    </script>

    <!--End add row to table -->


    <!--delete row in table -->
    <script>

        $(document).on('click', 'button#delete_row', function () {

            $(this).closest('tr').remove();
            return false;
        });

    </script>
    <!--Edit Row in Table -->

    <script>

        $(document).on('click','#editbtn',function(){
            var currentTD = $(this).parents('tr').find('td');
            if ($(this).html() == 'Edit') {
                currentTD = $(this).parents('tr').find('td');
                $.each(currentTD, function () {




                    $("input[type=text]").attr('disabled', false).css({
                        'border': '#eaeaea solid 1px',
                        'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                        'border-radius':'2px',
                        'background-color':'#fff',
                        'width':'150px',
                        'margin-left':'15px'
                    }).focus();

                    $("input[type=number]").attr('disabled',false).css({
                        'border': '#eaeaea solid 1px',
                        'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                        'border-radius':'2px',
                        'background-color':'#fff',
                        'margin-left':'15px'
                    });
                    $("select").removeAttr('disabled');
                });
            } else {
                $.each(currentTD, function () {
                    $("input[type=text]").attr('disabled', true).css({
                        'border': '#eaeaea solid 1px',

                        'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                        'border-radius':'2px',
                        'background-color':'#fff',
                        'width':'150px',
                        'margin-left':'15px'
                    }).focus();
                    $("input[type=number]").attr('disabled',true).css({
                        'box-shadow':'none',
                        'border': 'none',
                        'background-color':'fff',
                        'text-align':'center'
                    });
                    $("select").prop("disabled", true);
                });

            }

            $(this).html($(this).html() == 'Edit' ? 'Save' : 'Edit')

        });

    </script>





    <!--select salary scale -->

    <script>
        $(".myselect1").select2({
            width: 'resolve'

        });

        $(".myselect2").select2({
            width: 'resolve',


        });
        $(".myselect3").select2({
            width: 'resolve',


        });
        $(".myselect4").select2({
            width: 'resolve',


        });

    </script>


@endsection