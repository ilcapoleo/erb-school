@extends('layouts.main_app')

@section('main_content')
    <link href="{{asset('css/fastselect.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/select2.min.css')}}" rel="stylesheet" />
   <style>


        .collapse > .row>.col-md-4>.btn, .collapse > .row>.col-md-4>.dropdown>.btn {
    margin-top: 0;
            
}

        .material-switch > label::before {
            background: rgb(0, 0, 0);
            box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
            border-radius: 8px;
            content: '';
            height: 16px;
            margin-top: 3px;
            position: absolute;
            opacity: 0.3;
            transition: all 0.4s ease-in-out;
            width: 40px;
            left: -29px;
        }


        .material-switch > label::after {
            background: rgb(255, 255, 255);
            border-radius: 16px;
            box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
            content: '';
            height: 24px;
            left: -45px;
            margin-top: -8px;
            position: absolute;
            top: 6px;
            transition: all 0.3s ease-in-out;
            width: 24px;
        }

        .material-switch > input[type="checkbox"]:checked + label::after {
            background: inherit;
            left: -5px;
        }
        .panel .table > thead > tr > td:first-child, .panel .table > thead > tr > th:first-child, .panel .table > tbody > tr > td:first-child, .panel .table > tbody > tr > th:first-child, .panel .table > tfoot > tr > td:first-child, .panel .table > tfoot > tr > th:first-child{
            padding-left: 0px;
        }

        ul{
            margin: 0;
            padding: 0;
            list-style: none;
        }
        span.select2.select2-container.select2-container--default{
            width: 100%!important;
        }
    </style>
    <div class="main">
        <!-- Navbar-content -->
        <nav class="navbar navbar-default navbar2">

            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                    <!--breadcrumbs-->
                    <div class="row">

                        <div class="col-md-10 col-lg-10">

                        <ol class="breadcrumb breadcrumb_nav">
                            <li><a href="#">Budget</a></li>
                            <li class="active">Budget  Master</li>
                        </ol>
                        </div>


                        {{--@if(isset($master_inventory))--}}
                            {{--<div class="col-md-2 col-lg-2">--}}
                                {{--@if(@\App\inventory_master::where('doc_id','<',$master_inventory->doc_id)->orderBy('doc_id','desc')->first()->doc_id)--}}
                                    {{--<div class="col-md-6">--}}
                                        {{--<a href="/inventory/{{@\App\inventory_master::where('doc_id','<',$master_inventory->doc_id)->orderBy('doc_id','desc')->first()->doc_id}}"><span--}}
                                                    {{--class="glyphicon glyphicon-arrow-left"></span>--}}
                                        {{--</a>--}}
                                    {{--</div>--}}
                                {{--@endif--}}
                                {{--@if(@\App\inventory_master::where('doc_id','>',$master_inventory->doc_id)->orderBy('doc_id','asc')->first()->doc_id)--}}
                                    {{--<div class="col-md-6">--}}
                                        {{--<a href="/inventory/{{@\App\inventory_master::where('doc_id','>',$master_inventory->doc_id)->orderBy('doc_id','asc')->first()->doc_id}}">--}}
                                    {{--<span class="glyphicon glyphicon-arrow-right">--}}
                                {{--</span>--}}
                                        {{--</a>--}}
                                    {{--</div>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--@endif--}}

                    </div>   <!--End breadcrumbs-->
                        <div class="row">
                            <div class="col-md-4 col-lg-4 buttons">
                                <button id="edit_user" type="button" class="btn btn-danger " >Edit</button>
                                <button type="button" class="btn" id="creat" >Create</button>
                            </div>

                            <div class="col-md-4">
                                <div class="dropdown">
                                    <button class="btn btn-primary  dropdown-toggle" type="button" data-toggle="dropdown" id="more"  hidden>More
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" id="delete">Delete</a></li>
                                        <li><a href="#">Export</a></li>
                                        <li><a href="#">Duplicate</a></li>
                                        <li><a href="#">Print</a></li>

                                    </ul>
                                </div>
                            </div>
                           <!--search-->
                           <!--
                            <div class="col-md-4">
                                <form class="navbar-form navbar-right" >
                                    <div class="input-group form-group">
                                        <input type="text" value="" class="form-control" placeholder="Search ...">
                                    </div>
                                </form>
                            </div>
                            -->
                        </div> <!--collapse -->
              
                </div><!--collapse -->

                <!--buttons row +second breadcrumb-->

                <div class="row third-nav">


                    <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                        <div class="col-md-3 col-lg-3 col-sm-3 col-xs-3">
                            <button id="send_mail" type="button" class="btn btn_third_nav" >Send by Mail</button>
                        </div>

                        <div class="col-md-3 col-lg-3 col-sm-3 col-xs-3 ">
                            <button  type="button"  id="sendto_approved_btn"  class="btn btn_third_nav" data-toggle="modal" data-target="#exampleModal" >To Approve</button>
                        </div>

                        <div class="col-md-3 col-lg-3 col-sm-3 col-xs-3 ">
                            <button type="button" id="approved_btn" class="btn btn_third_nav" >Approve</button>
                        </div>


                    </div>
                    <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">

                                <span class="breadcrumbs">

                            <a href="#" class="breadcrumb">Draft </a>
                               <a href="#" class="breadcrumb">To Approve</a>
                               <a href="#" class="breadcrumb">Approved</a>
                            </span>

                    </div>


                </div>

            </div>
        </nav>
        <!-- End Navbar-content -->

        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Budget Master</h3>
                    </div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <div class="col-md-6 col-lg-6">
                                        <div class="form-group">

                                            <label class="col-md-6 col-lg-6 control-label">Person In Charge</label>
                                            <div class="col-md-6 col-lg-6">

                                                <input disabled class="form-control"  type="text" id="pname" name="pname">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-6">
                                        <div class="form-group">

                                            <label class="col-md-6 col-lg-6 control-label">Request By Cost Center</label>
                                            <div class="col-md-6 col-lg-6">

                                                <input disabled class="form-control"  type="text" id="fname" name="fname">
                                            </div>
                                        </div>



                                    </div>
                                </div>
                            </div>

                            <br>
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <div class="col-md-6 col-lg-6">
                                        <div class="form-group">

                                            <label class="col-md-6 col-lg-6 control-label">Currency</label>
                                            <div class="col-md-6 col-lg-6">

                                                <select disabled class="myselect1" required   style="width: 50%" name="state">

                                                    <option>Dollar</option>

                                                    <option >Egyptian pound</option>


                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-6">
                                        <div class="form-group">

                                            <label class="col-md-6 col-lg-6 control-label">Academic Year</label>

                                            <div class="col-md-6 col-lg-6">
                                                <select disabled class="myselect2"  style="width: 50%" name="state">

                                                    <option>2017</option>

                                                    <option >2018</option>

                                                    <option >2019</option>

                                                    <option >2020</option>


                                                </select>

                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <br>
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <div class="col-md-6 col-lg-6">
                                        <div class="form-group">

                                            <label class="col-md-6 col-lg-6 control-label">Rate</label>
                                            <div class="col-md-6 col-lg-6">

                                                <input disabled id="number" required  class="row_data" min="1"   type="number" value="0">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-lg-6">

                                                            <div class="form-group">
                                                                <label class=" control-label col-md-6 col-lg-6 " for="datetime1">Date</label>

                                                                <div class="col-md-6 col-lg-6">
                                                                    <div class="form-group">
                                                                <div class="input-group">
                                                                    <input disabled type="text" id="datetime1" class="form-control " name="date">
                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-calendar">
                                                                        </i>
                                                                    </div>
                                                                </div>
                                                                </div>

                                                                </div>
                                                            </div>

                                    </div>

                                </div>
                            </div>


                        </div>




                        <div class="row">

                                <ul class="nav nav-tabs">
                                    <li class="active" ><a href="#details">Details</a></li>

                                </ul>

                                <div class="tab-content">




                                    <div id="details" class="tab-pane fade in active table-responsive ">



                                        <table  class="table table-bordred table-striped table-bordered table-striped table-hover table-condensed table-responsive text-center" id="myTable2" >
                                            <thead>

                                            <th>Item Type</th>
                                            <th>Item Name</th>
                                            <th>Total Quantity</th>
                                            <th>Total Price</th>
                                            <th>Account Name</th>
                                            <th>Supplier Name</th>

                                            <th>Edit</th>
                                            <th>Delete</th>

                                            </thead>
                                            <tbody>
                                            <tr>




                                                  <td>
                                                      <div class="form-group">
                                                          <select disabled class="myselect3"  style="width: 50%" name="state">

                                                              <option>Type 1</option>

                                                              <option >Type 2</option>

                                                              <option >Type 3</option>

                                                              <option >Type 4</option>

                                                          </select>

                                                      </div>


                                                  </td>
                                                <td>
                                                    <div class="form-group">
                                                        <select disabled class="myselect4"  style="width: 50%" name="state">

                                                            <option>pencil</option>

                                                            <option >paper</option>

                                                            <option >books</option>

                                                            <option >notebooks</option>

                                                        </select>

                                                    </div>


                                                </td>


                                                <td>
                                                    <input disabled id="number" required  class="row_data" min="1"   type="number" value="0">


                                                </td>

                                                <td>
                                                    <input disabled id="number" required  class="row_data" min="1"   type="number" value="0">


                                                </td>

                                                <td>
                                                    <div class="form-group">
                                                        <select disabled class="myselect5"  style="width: 50%" name="state">

                                                            <option>name 1</option>

                                                            <option >name 2</option>

                                                            <option >name 3</option>

                                                        </select>

                                                    </div>



                                                </td>

                                                <td>
                                                    <div class="form-group">
                                                        <select disabled class="myselect6"  style="width: 50%" name="state">

                                                            <option>Kamal Saad</option>

                                                            <option >Samir & Ali</option>

                                                            <option >Fnon</option>

                                                        </select>

                                                    </div>


                                                </td>


                                                <td><button class="btn btn-primary btn-xs" id="editbtn"   >Edit</button>
                                                </td>


                                                <td>
                                                    <button class="btn btn-danger btn-xs" id="delete_row" ><span class="lnr lnr-trash"></span></button>

                                                </td>


                                            </tr>
                                            </tbody>
                                        </table>

                                        <div class="row">
                                            <div class="col-md-4 col-lg-4">
                                                <button type="button" class="btn btn-primary" id="add_row" >Add Row</button>
                                            </div>
                                        </div>





                                    </div>
                            </div>
                        </div>
                        
                        
                        
                        
                        
                    </div>
                 </div>
            <!-- END MAIN CONTENT -->
            </div>
        <!-- END MAIN -->
    </div>
    </div>

@endsection


@section('custom_footer')

    <script src="{{asset('js/item_name_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/tax_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/select2.min.js')}}"></script>

    <script src="{{asset('assets/scripts/bootstrap-datetimepicker.min.js')}}"></script>

<!--date/time picker-->
 
              <script>

$('#datetime1').datetimepicker({format: 'yyyy-mm-dd',autoclose: true,minview:2});

$('#datetime2').datetimepicker({format: 'yyyy-mm-dd',autoclose: true,minview:2});
$('#datetime3').datetimepicker({format: 'yyyy-mm-dd',autoclose: true,minview:2});
                  
 
</script>


    {{--------------------  active side & nav bar ------------------------}}
    <script>
        $('.side_sheets').addClass('hide');
        $('#budget').removeClass('hide');

        $("#menu li").removeClass('active');
        $('#Budget_nav_bar').addClass('active');

        $('#pages ul li a').removeClass('active');
        $('#budget_total_side_bar').addClass('active');

    </script>


    <!--edit fields -->


    <script>
        $(document).on('click','#edit_user',function(){

            $(this).text("Save");
            $(this).attr('id','save_user');
            $("input[type=text]").attr('disabled', false).css({
                'border': '#eaeaea solid 1px',

                'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                'border-radius':'2px',
                'background-color':'#fff',
                'width':'150px',
                'margin-left':'15px'
            }).focus();

            $("input[type=password]").attr('disabled',false).css({
                'border': '#eaeaea solid 1px',

                'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                'border-radius':'2px',
                'background-color':'#fff',
                'margin-left':'15px'
            });

            $("input[type=email]").attr('disabled',false).css({
                'border': '#eaeaea solid 1px',

                'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                'border-radius':'2px',
                'background-color':'#fff',
                'margin-left':'15px'
            });



            $("select").removeAttr('disabled');
            $("input[type=radio]").attr('disabled', false);
            $("input[type=password]").attr('disabled', false);
            $("input[type=email]").attr('disabled', false);

        });


    </script>
 















    <!--add row to table -->
<script>
    $("#add_row").click(function () {

        $("#myTable2").each(function () {

            var tds = '<tr>';
            jQuery.each($('tr:last td', this), function () {
                tds += '<td>' + $(this).html() + '</td>';
            });
            tds += '</tr>';
            if ($('tbody', this).length > 0) {
                $('tbody', this).append(tds);
            } else {
                $(this).append(tds);
            }
        });
    });


</script>

    <!--End add row to table -->


<!--delete row in table -->
    <script>

        $(document).on('click', 'button#delete_row', function () {

            $(this).closest('tr').remove();
            return false;
        });

    </script>
    <!--Edit Row in Table -->

<script>
    
        $(document).on('click','#editbtn',function(){
              var currentTD = $(this).parents('tr').find('td');
              if ($(this).html() == 'Edit') {
                  currentTD = $(this).parents('tr').find('td');
                  $.each(currentTD, function () {
                      
                      
                      
                      
                $("input[type=text]").attr('disabled', false).css({
                'border': '#eaeaea solid 1px',
                'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                'border-radius':'2px',
                'background-color':'#fff',
                'width':'150px',
                'margin-left':'15px'
            }).focus();

            $("input[type=number]").attr('disabled',false).css({
                'border': '#eaeaea solid 1px',

                'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                'border-radius':'2px',
                'background-color':'#fff',
                'margin-left':'15px'
            });

          


            $("select").removeAttr('disabled');

        
                  });
                 
                  
              } else {
                 $.each(currentTD, function () {
               $("input[type=text]").attr('disabled', true).css({
                'border': '#eaeaea solid 1px',

                'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                'border-radius':'2px',
                'background-color':'#fff',
                'width':'150px',
                'margin-left':'15px'
            }).focus();

            $("input[type=number]").attr('disabled',true).css({
                'border': '#eaeaea solid 1px',

                'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                'border-radius':'2px',
                'background-color':'#fff',
                'margin-left':'15px'
            });

                   $("select").prop("disabled", true);
        
                  });
                  
                  
              }
    
              $(this).html($(this).html() == 'Edit' ? 'Save' : 'Edit')
    
          });

</script>
        <!--select salary scale -->

        <script>
        $(".myselect1").select2({
            width: 'resolve'

        });

        $(".myselect2").select2({
            width: 'resolve',
        });

        $(".myselect3").select2({
            width: 'resolve',

        });

        $(".myselect4").select2({
            width: 'resolve',
        });
        $(".myselect5").select2({
            width: 'resolve',
        });
        $(".myselect6").select2({
            width: 'resolve',
        });
       


           </script>

   



@endsection