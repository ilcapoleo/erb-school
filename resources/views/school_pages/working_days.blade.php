@extends('layouts.main_app')

@section('main_content')
    <link href="{{asset('css/fastselect.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/select2.min.css')}}" rel="stylesheet" />

    <style>
        input.col-md-4.text-center {

            border:0;
            background-color: #fff;
        }



        .collapse > .row>.col-md-4>.btn, .collapse > .row>.col-md-4>.dropdown>.btn {
            margin-top: 0;

        }



        .third-nav{
            background-color:#eeeeee;
        }



        .myselect2{
            width:171px;
        }

        span.select2.select2-container.select2-container--default{
            width: 100%!important;
        }
        tbody th, tbody td {
            text-align: center;
        }
    </style>
    <div class="main">
        <!-- Navbar-content -->
        <nav class="navbar navbar-default navbar2">

            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                    <!--breadcrumbs-->
                    <div class="row">

                        <div class="col-md-10 col-lg-10">

                        <ol class="breadcrumb breadcrumb_nav">
                            <li><a href="#">Working Days</a></li>
                            <li class="active">


                            </li>
                        </ol>
                        </div>

                        <div class="col-md-2 col-lg-2">
                            <div class="col-md-6">
                                <a href="#">
                                    <span class="glyphicon glyphicon-arrow-left"></span>
                                </a>
                            </div>

                            <div class="col-md-6">
                                <a href="#">
                                    <span class="glyphicon glyphicon-arrow-right">
                                </span>
                                </a>
                            </div>





                        </div>


                    </div>


                    <!--End breadcrumbs-->
                    @if(isset($department))
                        <div id="set_po_id" current_po_id="{{$department->salary_id}}" current_po_status=""
                             closed_or_not="" current_id="{{Auth::user()->id}}"></div>
                    @else
                        <div id="set_po_id" current_po_id="" current_po_status="draft_po" closed_or_not="0"
                             current_id="{{Auth::user()->id}}"></div>
                    @endif
                    <div class="row">
                        <div class="col-md-4 col-lg-4 buttons">
                            @if(isset($department))
                                <button id="edit_dep" type="button" class="btn btn-danger ">Edit</button>
                                <button type="button" class="btn" id="creat_dep">Create</button>
                            @else
                                <button id="save_dep" type="button" class="btn btn-danger ">Save</button>
                                {{--<button id="edit_user" type="button" class="btn btn-danger " >Edit</button>--}}
                                <button type="button" class="hide btn" id="creat_dep">Create</button>
                            @endif
                        </div>

                        <div class="col-md-4">
                            @if(isset($department))
                                <div class="dropdown">
                                    <button class=" btn btn-primary  dropdown-toggle" type="button"
                                            data-toggle="dropdown" id="more" hidden>More
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" id="delete_dep">Delete</a></li>
                                        {{--<li><a href="#">Export</a></li>--}}
                                        <li><a href="#" id="dublicate_dep">Duplicate</a></li>
                                        <li><a href="#" id="print">Print</a></li>

                                    </ul>
                                </div>
                            @else
                                <div class="dropdown">
                                    <button class="hide btn btn-primary  dropdown-toggle" type="button"
                                            data-toggle="dropdown" id="more" hidden>More
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" id="delete_dep">Delete</a></li>
                                        <li><a href="#">Export</a></li>
                                        <li><a href="#" id="dublicate_dep">Duplicate</a></li>
                                        <li><a href="#" id="print">Print</a></li>

                                    </ul>
                                </div>
                            @endif
                        </div>
                        <!--search-->
                        <!--
                         <div class="col-md-4">
                             <form class="navbar-form navbar-right" >
                                 <div class="input-group form-group">
                                     <input type="text" value="" class="form-control" placeholder="Search ...">
                                 </div>
                             </form>
                         </div>

                         -->
                        <div class="alert alert-success alert-autocloseable-success" style="display:none;position:absolute;">
                            workind day Details Saved Successfully!

                        </div>

                        <!--end Success messages -->


                        <!-- failuer messages -->
                        <div class="alert alert-danger errorMessage1" style="display:none;position:absolute;">
                            Your should fill all fields!

                        </div>
                        <!--end failuer messages -->
                    </div> <!--collapse -->

                </div><!--collapse -->

            </div>
        </nav>
        <!-- End Navbar-content -->
        <!-- MAIN CONTENT -->
        <div class="main-content">

            <div class="container-fluid">

                <!-- INPUTS -->
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Working Days</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">



                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="usr" class="col-md-6 col-lg-6 control-label">Academic Year</label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            <select class="myselect4"   style="width: 50%" name="state">

                                                <option>2018</option>

                                                <option >2017</option>

                                                <option >2016</option>



                                            </select>
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>




                        <br>

                        <div class="row">

                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#Working_Days">Working Days</a></li>


                            </ul>

                            <div class="tab-content">

                                <div id="Working_Days" class="tab-pane fade in active">
                                    <div class="row" >
                                        <table  class="table table-bordred table-striped table-bordered table-striped table-hover table-condensed table-responsive " id="myTable2" >
                                            <thead>

                                            <th>Salary Scale</th>

                                            <th>Days</th>

                                            <th>Edit</th>
                                            <th>Delete</th>

                                            </thead>
                                            <tbody>
                                            <tr>

                                                <td  >

                                                    <select id="salary_scale_id" name="salary_scale_id"
                                                            class="get_all_selectors set_all_disabled select_2_enable form-control err_input required cost_center_error"
                                                            @if(isset($department)) disabled @endif >
                                                        <option value=""></option>
                                                        @foreach($salary_name as $id=>$role)
                                                            @if(isset($department))
                                                                @if($department->salary_id == $id)
                                                                    <option selected value="{{$id}}">{{$role}}</option>
                                                                @else
                                                                    <option value="{{$id}}">{{$role}}</option>
                                                                @endif
                                                            @else
                                                                <option value="{{$id}}">{{$role}}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>

                                                </td>

                                                <td>
                                                    <div class="form-group">
                                                        <input id="number"   class="row_data" min="1" max="365"  type="number" value="1">
                                                    </div>

                                                </td>

                                                <td><button class="btn btn-primary btn-xs" id="editbtn"   >Edit</button>
                                                </td>


                                                <td>
                                                    <button class="btn btn-danger btn-xs" id="delete_row" ><span class="lnr lnr-trash"></span></button>

                                                </td>


                                            </tr>
                                            </tbody>
                                        </table>
                                        <div class="row">
                                            <div class="col-md-4 col-lg-4 ">
                                                <button type="button" class="btn btn-primary" id="add_row" >Add Item</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>





                            </div>
                        </div>




                    </div>
                    <br>
                </div>
            </div>

        </div>
        <!-- END INPUTS -->


    </div>

@endsection


@section('custom_footer')
    <script src="{{asset('js/item_name_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/tax_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/select2.min.js')}}"></script>

    <script>
        $('.side_sheets').addClass('hide');
        $('#HR').removeClass('hide');

        $("#menu li").removeClass('active');
        $('#hr_nav_bar').addClass('active');

        $('#pages ul li a').removeClass('active');
        $('#working_days_side_bar').addClass('active');

    </script>

    <!--edit fields -->


    <script>
        $(document).on('click','#edit_user',function(){

            $(this).text("Save");
            $(this).attr('id','save_user');
            $("input[type=text]").attr('disabled', false).css({
                'border': '#eaeaea solid 1px',

                'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                'border-radius':'2px',
                'background-color':'#fff',
                'width':'150px',
                'margin-left':'15px'
            }).focus();

            $("input[type=password]").attr('disabled',false).css({
                'border': '#eaeaea solid 1px',

                'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                'border-radius':'2px',
                'background-color':'#fff',
                'margin-left':'15px'
            });

            $("input[type=email]").attr('disabled',false).css({
                'border': '#eaeaea solid 1px',

                'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                'border-radius':'2px',
                'background-color':'#fff',
                'margin-left':'15px'
            });



            $("select").removeAttr('disabled');
            $("input[type=radio]").attr('disabled', false);
            $("input[type=password]").attr('disabled', false);
            $("input[type=email]").attr('disabled', false);

        });


    </script>


    <!--add row to table -->
    <script>
        $("#add_row").click(function () {

            $("#myTable2").each(function () {

                var tds = '<tr>';
                jQuery.each($('tr:last td', this), function () {
                    tds += '<td>' + $(this).html() + '</td>';
                });
                tds += '</tr>';
                if ($('tbody', this).length > 0) {
                    $('tbody', this).append(tds);
                } else {
                    $(this).append(tds);
                }
            });
        });


    </script>

    <!--End add row to table -->


    <!--delete row in table -->
    <script>

        $(document).on('click', 'button#delete_row', function () {

            $(this).closest('tr').remove();
            return false;
        });

    </script>
    <!--Edit Row in Table -->

    <script>

        $(document).on('click','#editbtn',function(){
            var currentTD = $(this).parents('tr').find('td');
            if ($(this).html() == 'Edit') {
                currentTD = $(this).parents('tr').find('td');
                $.each(currentTD, function () {




                    $("input[type=text]").attr('disabled', false).css({
                        'border': '#eaeaea solid 1px',
                        'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                        'border-radius':'2px',
                        'background-color':'#fff',
                        'width':'150px',
                        'margin-left':'15px'
                    }).focus();

                    $("input[type=number]").attr('disabled',false).css({
                        'border': '#eaeaea solid 1px',
                        'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                        'border-radius':'2px',
                        'background-color':'#fff',
                        'margin-left':'15px'
                    });
                    $("select").removeAttr('disabled');
                });
            } else {
                $.each(currentTD, function () {
                    $("input[type=text]").attr('disabled', true).css({
                        'border': '#eaeaea solid 1px',

                        'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                        'border-radius':'2px',
                        'background-color':'#fff',
                        'width':'150px',
                        'margin-left':'15px'
                    }).focus();
                    $("input[type=number]").attr('disabled',true).css({
                        'box-shadow':'none',
                        'border': 'none',
                        'background-color':'fff',
                        'text-align':'center'
                    });
                    $("select").prop("disabled", true);
                });

            }

            $(this).html($(this).html() == 'Edit' ? 'Save' : 'Edit')

        });

    </script>





    <!--select salary scale -->

    <script>
        $(".myselect1").select2({
            width: 'resolve'

        });

        $(".myselect2").select2({
            width: 'resolve',


        });
        $(".myselect3").select2({
            width: 'resolve',


        });
        $(".myselect4").select2({
            width: 'resolve',


        });

    </script>


@endsection