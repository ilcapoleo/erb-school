@extends('layouts.main_app')

@section('main_content')
    <link href="{{asset('css/fastselect.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/select2.min.css')}}" rel="stylesheet" />

    <style>
        input.col-md-4.text-center {

            border:0;
            background-color: #fff;
        }



        .collapse > .row>.col-md-4>.btn, .collapse > .row>.col-md-4>.dropdown>.btn {
            margin-top: 0;

        }



        .third-nav{
            background-color:#eeeeee;
        }





        .myselect2{
            width:171px;
        }

        span.select2.select2-container.select2-container--default{
            width: 100%!important;
        }
        tbody th, tbody td {
            text-align: center;
        }
    </style>
    <div class="main">
        <!-- Navbar-content -->
        <nav class="navbar navbar-default navbar2">

            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                    <!--breadcrumbs-->
                    <div class="row">

                        <div class="col-md-10 col-lg-10">

                        <ol class="breadcrumb breadcrumb_nav">
                            <li><a href="/cost_centers">Cost Center</a></li>
                            <li class="active">
                                @if(isset($cost))
                                    @if($cost->Cost_id != 0)
                                        {{$cost->Cost_id}}
                                    @else
                                        Draft
                                    @endif
                                @endif
                            </li>
                        </ol>
                        </div>

                        <div class="col-md-2 col-lg-2">
                            @if(isset($cost))
                                <div class="col-md-2 col-lg-2 col-sm-4 col-xs-4">
                                    @if(@\App\CostCenter::where('Cost_id','<',$cost->Cost_id)->orderBy('Cost_id','desc')->first()->Cost_id)
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <a href="/cost_centers/{{@\App\CostCenter::where('Cost_id','<',$cost->Cost_id)->orderBy('Cost_id','desc')->first()->Cost_id}}/edit"><span
                                                        class="glyphicon glyphicon-arrow-left"></span>
                                            </a>
                                        </div>
                                    @endif
                                    @if(@\App\CostCenter::where('Cost_id','>',$cost->Cost_id)->orderBy('Cost_id','asc')->first()->Cost_id)
                                        <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                                            <a href="/cost_centers/{{@\App\CostCenter::where('Cost_id','>',$cost->Cost_id)->orderBy('Cost_id','asc')->first()->Cost_id}}/edit">
                                    <span class="glyphicon glyphicon-arrow-right">
                                </span>
                                            </a>
                                        </div>
                                    @endif
                                </div>
                            @endif





                        </div>



                        </div>


                    <!--End breadcrumbs-->
                    <div class="row">
                        <div class="col-md-4 col-lg-4 buttons">
                            <button id="edit_work"  type="button" class="btn btn-danger" >Edit</button>
                            <button type="button" class="btn" id="create_work" >Create</button>
                        </div>

                        <div class="col-md-4 col-lg-4">
                            <div class="dropdown">
                                <button class="btn btn-primary  dropdown-toggle" type="button" data-toggle="dropdown" id="more"  hidden>More
                                    <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li>

                                        <a data-toggle="modal" data-target="#delete{{ $cost->Cost_id }}"
                                           >delete</a>


                                    </li>
                                    <li><a href="#">Export</a></li>
                                    <li><a href="#">Duplicate</a></li>
                                    <li><a href="#" id="print">Print</a></li>

                                </ul>
                                <div id="delete{{ $cost->Cost_id }}" class="modal fade" role="dialog">
                                    <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Delete vendor</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>{{"Are You Sure Deleting".' '.$cost->Cost_id. ' ' .$cost->Cost_name. ' ' .$cost->Cost_center_code}}</p>
                                            </div>
                                            <div class="modal-footer">
                                                {!! Form::open(['method'=>'DELETE','route'=>['cost_centers.destroy',$cost->Cost_id]]) !!}
                                                <button type="button" class="btn btn-default btn-flat"
                                                        data-dismiss="modal">close
                                                </button>
                                                <button type="submit"
                                                        class="btn btn-danger btn-flat">delete
                                                </button>
                                                {!! Form::close() !!}
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div> <!--collapse -->

                </div><!--collapse -->

            </div>
        </nav>
        <!-- End Navbar-content -->
        <!-- MAIN CONTENT -->
        <div class="main-content">


            <div class="container-fluid">

                <!-- INPUTS -->
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Cost Center</h3>
                    </div>
                    <div class="panel-body">



            {!! Form::open(['method'=>'put' ,'id'=>'theform', 'action'=> ['CostCentersController@update',$cost->Cost_id],'file'=>'true' ,'enctype'=>'multipart/form-data']) !!}


            <div class="row">



                <div class="col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="cost_center_n" class="col-md-6 col-lg-6 control-label">Cost Center Name </label>

                        <div class=" col-md-6 col-lg-6">

                            <div class="form-group">
                                <input disabled class="form-control set_all_disabled required"  type="text" id="cost_center_n" value="{{$cost->Cost_name}}">

                            </div>

                        </div>

                    </div>

                </div>

            </div>
            <br>
            <div class="row">



                <div class="col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="cost_center_c" class="col-md-6 col-lg-6 control-label">Cost Center Code </label>

                        <div class=" col-md-6 col-lg-6">

                            <div class="form-group">
                                <input disabled class="form-control  set_all_disabled required"  type="text" id="cost_center_c" value="{{$cost->Cost_center_code}}" >

                            </div>

                        </div>

                    </div>

                </div>

            </div>

            <div class = 'form-group hidden'>
                {!! Form::submit('create user',['class'=>'btn btn-primary']) !!}
            </div>
            {!! Form::close() !!}
        </div>
                </div>
            </div>
        </div>
        <!-- END INPUTS -->


    </div>

@endsection


@section('custom_footer')
    <script src="{{asset('js/item_name_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/tax_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/select2.min.js')}}"></script>

    <script>
        $(document).on('click', '#edit_work', function () {
            $('#edit_work').text("Save");
            $('#edit_work').attr('id', 'save_work');
            $('#save_work').attr('form', 'theform');
            $('.set_all_disabled').prop('disabled', false);
            $('#create_work').addClass('hide');
            $('#more').addClass('hide');
        });
    </script>
    <script>
        $(document).on('click', '#save_work', function () {
            $('#save_work').attr('type', 'submit');

        });
    </script>
    <script>
        $(document).on('click', '#create_work', function () {

            window.location.href = '/cost_centers/create'
        });
    </script>


    <!-----------------active link in nav and active link in sidebar ---->
    <script>
        $('.side_sheets').addClass('hide');
        $('#accounting').removeClass('hide');

        $("#menu li").removeClass('active');
        $('#accounting_nav_bar').addClass('active');

        $('#pages ul li a').removeClass('active');
        $('#costcenter_side_bar').addClass('active');

    </script>

    <!--edit fields -->


    <script>
        $(document).on('click','#edit_user',function(){

            $(this).text("Save");
            $(this).attr('id','save_user');
            $("input[type=text]").attr('disabled', false).css({
                'border': '#eaeaea solid 1px',

                'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                'border-radius':'2px',
                'background-color':'#fff',
                'width':'150px',
                'margin-left':'15px'
            }).focus();

            $("input[type=password]").attr('disabled',false).css({
                'border': '#eaeaea solid 1px',

                'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                'border-radius':'2px',
                'background-color':'#fff',
                'margin-left':'15px'
            });

            $("input[type=email]").attr('disabled',false).css({
                'border': '#eaeaea solid 1px',

                'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                'border-radius':'2px',
                'background-color':'#fff',
                'margin-left':'15px'
            });



            $("select").removeAttr('disabled');
            $("input[type=radio]").attr('disabled', false);
            $("input[type=password]").attr('disabled', false);
            $("input[type=email]").attr('disabled', false);

        });


    </script>


    <!--add row to table -->
    <script>
        $("#add_row").click(function () {

            $("#myTable2").each(function () {

                var tds = '<tr>';
                jQuery.each($('tr:last td', this), function () {
                    tds += '<td>' + $(this).html() + '</td>';
                });
                tds += '</tr>';
                if ($('tbody', this).length > 0) {
                    $('tbody', this).append(tds);
                } else {
                    $(this).append(tds);
                }
            });
        });


    </script>

    <!--End add row to table -->


    <!--delete row in table -->
    <script>

        $(document).on('click', 'button#delete_row', function () {

            $(this).closest('tr').remove();
            return false;
        });

    </script>
    <!--Edit Row in Table -->

    <script>

        $(document).on('click','#editbtn',function(){
            var currentTD = $(this).parents('tr').find('td');
            if ($(this).html() == 'Edit') {
                currentTD = $(this).parents('tr').find('td');
                $.each(currentTD, function () {




                    $("input[type=text]").attr('disabled', false).css({
                        'border': '#eaeaea solid 1px',
                        'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                        'border-radius':'2px',
                        'background-color':'#fff',
                        'width':'150px',
                        'margin-left':'15px'
                    }).focus();

                    $("input[type=number]").attr('disabled',false).css({
                        'border': '#eaeaea solid 1px',
                        'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                        'border-radius':'2px',
                        'background-color':'#fff',
                        'margin-left':'15px'
                    });
                    $("select").removeAttr('disabled');
                });
            } else {
                $.each(currentTD, function () {
                    $("input[type=text]").attr('disabled', true).css({
                        'border': '#eaeaea solid 1px',

                        'box-shadow':'0px 1px 2px 0 rgba(0, 0, 0, 0.1)',
                        'border-radius':'2px',
                        'background-color':'#fff',
                        'width':'150px',
                        'margin-left':'15px'
                    }).focus();
                    $("input[type=number]").attr('disabled',true).css({
                        'box-shadow':'none',
                        'border': 'none',
                        'background-color':'fff',
                        'text-align':'center'
                    });
                    $("select").prop("disabled", true);
                });

            }

            $(this).html($(this).html() == 'Edit' ? 'Save' : 'Edit')

        });

    </script>





    <!--select salary scale -->

    <script>
        $(".myselect1").select2({
            width: 'resolve'

        });

        $(".myselect2").select2({
            width: 'resolve',


        });
        $(".myselect3").select2({
            width: 'resolve',


        });
        $(".myselect4").select2({
            width: 'resolve',


        });

    </script>

    {{-----------------------print----------------}}
    <script>

        $(document).on('click', '#print', function () {

            //  window.location.href = 'get_po2/276'

            var id = $('#set_po_id').attr('current_po_id');
            window.open('/get_cost_center_rpt/' + id + '', '_blank');


        });

    </script>
@endsection