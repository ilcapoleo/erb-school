@extends('layouts.main_app')

@section('main_content')
    <link href="{{asset('css/fastselect.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/select2.min.css')}}" rel="stylesheet"/>

    <style>
        input.col-md-4.text-center {

            border: 0;
            background-color: #fff;
        }

        .collapse > .row > .col-md-4 > .btn, .collapse > .row > .col-md-4 > .dropdown > .btn {
            margin-top: 0;

        }
        .main-content {
            padding: 56px  0 28px 5px;        }


        .third-nav {
            background-color: #eeeeee;
        }

        .myselect2 {
            width: 171px;
        }

        span.select2.select2-container.select2-container--default {
            width: 100% !important;
        }

        tbody th, tbody td {
            text-align: center;
        }

        .red-border{
            border: 1px solid red  !important;
        }
        .error{
            border: 1px solid red  !important;
            border-left: 1px double red !important;
            border-top: 2px double red !important;
        }
        .alert{
            margin-bottom: -37px;
        }
    </style>

    <div class="main">
        <!-- Navbar-content -->
        <nav class="navbar navbar-default navbar2">

            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                    <!--breadcrumbs-->
                    <div class="row">

                        <div class="col-md-10 col-lg-10">

                            <ol class="breadcrumb breadcrumb_nav">
                                <li><a href="/products">Product</a></li>
                                <li class="active" id="second">
                                    @if(isset($product))
                                        @if($product->pro_id != 0)
                                            {{$product->pro_id}}
                                        @else
                                            Draft
                                        @endif
                                    @endif
                                </li>
                            </ol>
                        </div>


                        @if(isset($product))
                            <div class="col-md-2 col-lg-2 col-sm-4 col-xs-4">
                                @if(@\App\products::where('pro_id','<',$product->pro_id)->orderBy('pro_id','desc')->first()->pro_id)
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <a href="/products/{{@\App\products::where('pro_id','<',$product->pro_id)->orderBy('pro_id','desc')->first()->pro_id}}/edit"><span
                                                    class="glyphicon glyphicon-arrow-left"></span>
                                        </a>
                                    </div>
                                @endif
                                @if(@\App\products::where('pro_id','>',$product->pro_id)->orderBy('pro_id','asc')->first()->pro_id)
                                    <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                                        <a href="/products/{{@\App\products::where('pro_id','>',$product->pro_id)->orderBy('pro_id','asc')->first()->pro_id}}/edit">
                                    <span class="glyphicon glyphicon-arrow-right">
                                </span>
                                        </a>
                                    </div>
                                @endif
                            </div>
                        @endif




                    </div>


                    <!--End breadcrumbs-->
                    <div class="row">
                        <div class="col-lg-4  col-md-4 col-sm-6 col-xs-6 buttons">
                            @if(isset($product))
                                <button id="edit_po" type="button" class="btn btn-danger ">Edit</button>
                                <button type="button" class="btn" id="creat_dep">Create</button>
                            @else
                                <button id="save_po" type="button" class="btn btn-danger ">Save</button>
                                {{--<button id="edit_user" type="button" class="btn btn-danger " >Edit</button>--}}
                                <button type="button" class="hide btn" id="creat_dep">Create</button>
                            @endif
                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-4">
                            @if(isset($product))
                                <div class="dropdown">
                                    <button class=" btn btn-primary  dropdown-toggle" type="button"
                                            data-toggle="dropdown" id="more" hidden>More
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li class="  " data-toggle="modal" data-target="#exampleModal"><a href="#"
                                                                                                          id="delete_btn">Delete</a>
                                        </li>
                                        <li><a href="#">Export</a></li>
                                        {{--<li><a href="#" id="dublicate_dep">Duplicate</a></li>--}}
                                        <li><a href="#" id="print">Print</a></li>

                                    </ul>
                                </div>
                            @else
                                <div class="dropdown">
                                    <button class="hide btn btn-primary  dropdown-toggle" type="button"
                                            data-toggle="dropdown" id="more" hidden>More
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li class="  " data-toggle="modal" data-target="#exampleModal"><a href="#"
                                                                                                          id="delete_btn">Delete</a>
                                        </li>
                                        <li><a href="#">Export</a></li>
                                        {{--<li><a href="#" id="dublicate_dep">Duplicate</a></li>--}}
                                        <li><a href="#" id="print">Print</a></li>

                                    </ul>
                                </div>
                            @endif
                        </div>
                    </div>

                </div><!--collapse -->


                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                     aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Purchase Order</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                Are You Sure to Delete this PO ?!!
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-danger fa fa-trash" id="delete_po">Delete
                                </button>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </nav>
        <!-- End Navbar-content -->

        <!-- Success messages -->
        <div class="alert alert-success alert-autocloseable-success" style="display:none;position:absolute;" >
            Your Product Saved Successfully!

        </div>
        <!--end Success messages -->

        <!-- failuer messages -->
        <div class="alert alert-danger errorMessage1" style="display:none;position:absolute;" >
            Your should fill all fields!

        </div>
        <!--end failuer messages -->

        <!-- failuer messages -->
        <div class="alert alert-danger errorMessage2" style="display:none;position:absolute;" >
            PLease fill Table!

        </div>
        <div  class="alert alert-danger errorMessage3" style=" display: none;">
        </div>

        <div  class="alert alert-danger errorMessage4" style=" display: none;">
            Please fill table!
        </div>



        <!-- MAIN CONTENT -->
        <div class="main-content">

            <div class="container-fluid">

                <!-- INPUTS -->
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Product</h3>
                    </div>
                    <div class="panel-body">

                        @if(isset($product))
                            <div id="set_po_id" current_po_id="{{$product->pro_id}}" current_po_status=""
                                 closed_or_not=""
                                 current_id="{{Auth::user()->id}}"></div>
                        @else
                            <div id="set_po_id" current_po_id="" current_po_status="draft_po" closed_or_not="0"
                                 current_id="{{Auth::user()->id}}"></div>
                        @endif

                        <div class="row">


                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="product_name" class="col-md-6 col-lg-6 control-label">Product Name </label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            @if(isset($product))
                                                <input id="product_name" type="text" name="product_name"
                                                       class="get_valide_for set_all_disabled get_all_date_input form-control required err_input job_name_error has_error "
                                                       value="{{$product->pro_name}}" disabled>
                                            @else
                                                <input id="product_name" type="text" name="product_name"
                                                       class="get_valide_for set_all_disabled get_all_date_input form-control required err_input job_name_error has_error"
                                                       value="">
                                            @endif
                                        </div>




                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">


                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="usr" class="col-md-6 col-lg-6 control-label">Short Name </label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">
                                            @if(isset($product))
                                                <input id="short_product_name" type="text"
                                                       class="get_valide_for set_all_disabled get_all_date_input form-control required err_input job_name_error has_error "
                                                       value="{{$product->product_short_name}}" disabled>
                                            @else
                                                <input id="short_product_name" type="text"
                                                       class="get_valide_for set_all_disabled get_all_date_input form-control required err_input job_name_error has_error"
                                                       value="">
                                            @endif
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">


                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="usr" class="col-md-6 col-lg-6 control-label">Product Type </label>
                                    <div class=" col-md-6 col-lg-6">
                                        <div class="form-group">

                                            <select id="product_type" name="product_type"
                                                    class="get_valide_for validate_tab required get_all_selectors set_all_disabled select_2_enable form-control department_error err_input has_error"
                                                    @if(isset($product)) disabled @endif >
                                                <option value=""></option>
                                                @foreach($products_type as $id=>$role)
                                                    @if(isset($product))
                                                        @if($product->pro_type_id == $id)
                                                            <option selected value="{{$id}}">{{$role}}</option>
                                                        @else
                                                            <option value="{{$id}}">{{$role}}</option>
                                                        @endif
                                                    @else
                                                        <option value="{{$id}}">{{$role}}</option>
                                                    @endif
                                                @endforeach
                                            </select>



                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">


                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="usr" class="col-md-6 col-lg-6 control-label">Income Account </label>

                                    <div class=" col-md-6 col-lg-6">

                                        <div class="form-group">

                                            <select id="income_acc" name="product_type"
                                                    class="get_valide_for validate_tab required get_all_selectors set_all_disabled select_2_enable form-control department_error err_input has_error"
                                                    @if(isset($product)) disabled @endif >
                                                <option value=""></option>
                                                @foreach($accounts as $id=>$role)
                                                    @if(isset($product))
                                                        @if($product->account_id == $id)
                                                            <option selected value="{{$id}}">{{$role}}</option>
                                                        @else
                                                            <option value="{{$id}}">{{$role}}</option>
                                                        @endif
                                                    @else
                                                        <option value="{{$id}}">{{$role}}</option>
                                                    @endif
                                                @endforeach
                                            </select>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>


                        <br>

                        <div class="row">

                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#supplier">Suppliers</a></li>


                            </ul>

                            <div class="tab-content">

                                <div id="supplier" class="tab-pane fade in active">
                                    <br>

                                    <div class="row">
                                        <table class="table table-bordred table-striped table-bordered table-striped table-hover table-condensed table-responsive "
                                               id="op_table">
                                            <thead>
                                            <th>Supplier</th>
                                            <th>Cost Price</th>
                                            <th>Currency</th>
                                            <th>Edit</th>
                                            <th>Delete</th>
                                            </thead>
                                            <tbody>
                                            @if(isset($product_details))
                                                @foreach($product_details as $documents)
                                            <tr class="">
                                                <td td_type="search" data-select2-id="12" class="supplier_v" current_value="{{$documents->supplier_id}}">{{$documents->VendorName}}</td>
                                                <td td_type="input" class="cost_price_v" current_value="{{$documents->cost_price}}">{{$documents->cost_price}}</td>
                                                <td td_type="search" class="currency_v" current_value="{{$documents->currency_id}}">{{$documents->Currency_name}}</td>
                                                <td>
                                                    <button class="set_all_disabled btn btn-primary btn-xs edit_row" disabled>Edit</button>
                                                </td>
                                                <td>
                                                    <button class="set_all_disabled btn btn-danger btn-xs delete_row" disabled>
                                                        <span class="lnr lnr-trash">
                                                        </span>
                                                    </button>
                                                </td>
                                            </tr>
                                                @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                        <div class="row">
                                            <div class="col-md-4 col-lg-4 ">
                                                <button type="button" class="btn btn-primary" id="add_row"
                                                        @if(isset($product)) disabled @endif>Add Item
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>


                    </div>
                    <br>
                </div>
            </div>

        </div>
        <!-- END INPUTS -->


    </div>

@endsection


@section('custom_footer')
    <script src="{{asset('js/item_name_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/tax_fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/select2.min.js')}}"></script>

    <script> $('.select_2_enable').select2(); </script>
    <!-----------------active link in nav and active link in sidebar ---->
    <script>
        $('.side_sheets').addClass('hide');
        $('#Purshase').removeClass('hide');

        $("#menu li").removeClass('active');
        $('#Purchase_nav_bar').addClass('active');

        $('#pages ul li a').removeClass('active');
        $('#product_side_bar').addClass('active');

    </script>


    <script>

        $('.alert-autocloseable-success').hide();


        $(document).on('click', '#save_po', function () {
            var check_table = 1 ;
            var inputs_valid = $('.get_valide_for');
            inputs_valid.each(function(){
               if($(this).val() == '' || !$(this).val()) {
                   $(this)
                   $(this).css({
                       "border": "1px solid red",
                   });
                   $(this).siblings('.select2').css({
                       "border": "1px solid red",
                   });
                   check_table = 0;

                   notification('glyphicon glyphicon-warning-sign','Warning!','You Should Fill All Fields!','danger')


                                     $(".errorMessage2").hide();
               }
               else{
                   $(this).css({'border' : ""});
                   $(this).siblings('.select2').css({'border' : ""});
               }
            });

           /* var isValid = true;
            var i = $(".has_error,.select2-container--default .select2-selection--single");
            console.log(i);
            $(".has_error,.select2-container--default .select2-selection--single").each(function() {
                if (($(this).val()) == '') {
                    isValid = false;
                   var name= $(this).attr('id')
                    $(this).css({
                        "border": "1px solid red",

                    });
                    $('.errorMessage2').show();
                }
                else {
                    $(this).css({'border' : ""});

                }

            });
*/

            var rowCount = $('#op_table tbody tr:not(.added_now)').length;


            if (rowCount == 0 && check_table == 1) {


                $(".errorMessage1").hide();


                notification('glyphicon glyphicon-warning-sign','Warning!','Please Fill Table!','danger')




            }



            if (!$('#op_table tr').hasClass('edited_now') && rowCount > 0 && !$('#op_table tr').hasClass('added_now') && check_table == 1) {
               // $("#op_table tbody .added_now").remove();
                $('#save_po').attr('disabled', 'disabled');
                $('.set_all_disabled').prop('disabled', true);


                var product_name = $("#product_name").val();
                var product_short_name = $("#short_product_name").val();

                var product_type = $("#product_type").val();
                var income_account = $("#income_acc").val();

                var input = $('#op_table > tbody  > tr');
                var row = [];

                input.each(function () {

                    if (!$(this).hasClass('added_now')) {
                        row.push({
                            "supplier": $(this).find('.supplier_v').attr('current_value'),
                            "cost_price": $(this).find('.cost_price_v').attr('current_value'),
                            "currency": $(this).find('.currency_v').attr('current_value'),

                        });
                    }

                });


                var current_user_id = $('#set_po_id').attr('current_id');
                var current_po_id = $('#set_po_id').attr('current_po_id');

                console.log('product_name : ' + product_name + 'product_short_name : ' + product_short_name + 'product_type : '+product_type + 'income_account : '+income_account)
                console.log(row);
                console.log('current_user_id :' + current_user_id + 'current_po_id : '+current_po_id);

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/products/',
                    dataType: 'json',
                    type: 'post',
                    data: {

                        product_name: product_name,
                        product_short_name: product_short_name,
                        product_type: product_type,
                        income_account: income_account,

                        _token: '{{csrf_token()}}',

                        table_rows: row,
                        current_user_id: current_user_id,
                        current_po_id: current_po_id
                    },

                    success: function (response) {
                        $(".errorMessage1").hide();
                        $(".errorMessage2").hide();
                        console.log(response);
                        if (response['error']) {
                            $('.err_input').removeClass('red-border');
                            $('.err_input').parent().find('.select2').removeClass('red-border');
                            $('.errorMessage3').empty();

                            //$('.errorMessage2').show();
                            $.each(response['error'], function (key, val) {
                                /*alert(key + val);*/
                                var row = '<div>'+val+'</div><br>'



                                // $('.errorMessage3').append(row);
                                //
                                // $('.errorMessage3').css("display","block");



                                notification('glyphicon glyphicon-warning-sign','Warning!','The product name has already been taken.','danger')



                                console.log(key + '_error');
                                $("." + key + '_error').addClass('red-border');
                                $('.' + key + '_error').parent().find('.select2').addClass('red-border');

                                //console.log(val[0]);

                            })
                            $('#save_po').removeAttr('disabled');
                            $('.set_all_disabled').prop('disabled', false);

                        }

                        else {
                            $('.errorMessage3').empty();
                            $('.err_input').removeClass('red-border');
                            $('.err_input').parent().find('.select2').removeClass('red-border');
                            $('.errorMessage1').hide();

                            $('#save_po').text("Edit");
                            $('#save_po').attr('id', 'edit_po');
                            $('#add_row').prop('disabled', true);
                            $('#set_po_id').attr('current_po_id', response['current_po_id']);
                            $('.set_all_disabled').prop('disabled', true);
                            $('#creat_dep').removeClass('hide');

                            $('#more').removeClass('hide');


                            $('#edit_po').removeAttr('disabled');
                            //po-active
                            console.log(response);

                            notification('glyphicon glyphicon-ok-sign','Congratulations!','Product Saved Succefully.','success')


                        }

                    },
                    error: function (response) {
                        alert(' Cant Save This Product -  !');
                        $('#save_po').prop('disabled', false);
                    }

                });
            }
        });

    </script>




    {{---------------------  edit journal ----------------------}}
    <script>
        $(document).on('click', '#edit_po', function () {
            $('#edit_po').text("Save");
            $('#edit_po').attr('id', 'save_po');

            $('.set_all_disabled').prop('disabled', false);
            $('#add_row').prop('disabled', false);

            $('#creat_dep').addClass('hide');

            $('#more').addClass('hide');
        });
    </script>

    {{----------------  create new journal ------------------}}
    <script>
        $(document).on('click', '#creat_dep', function () {

            window.location.href = '/products/create';

        });
    </script>

    <!--edit fields -->

    {{------------  add new row  ------------------------}}
    <script>
        $(document).on('click', '#add_row', function () {

            $(this).attr('disabled', true);

            var row = '<tr class= "added_now" >' +

                '<td td_type="search">' +
                '<select class="supplier select2 form-control js-example-disabled-results">\n'
                + '<option value="">' + '' + '</option>'
                    @foreach($supplier as $suppliersing)
                +'<option curr_id="{{$suppliersing->CurrencyID}}" class="{{$suppliersing->VendorID}}" value="{{$suppliersing->VendorID}}">'+'{{$suppliersing->VendorName}}'
                    @endforeach
                + '</option>'
                + '</select></td>'

                + '<td td_type="input"><input class="cost_price row_data pull-left form-control required set_all_disabled" min="1.00" onkeypress="return (event.charCode == 8 || event.charCode == 1) ? null : event.charCode >= 48 && event.charCode <= 57 ||  event.charCode == 46" data-role="input" type="number"  placeholder="0"/></td>'

                +'<td td_type="search">' +
                '<select class="currency select2 form-control js-example-disabled-results">\n'
                + '<option value="">' + '' + '</option>'
                    @foreach($currency as $id=>$role)
                +'<option value="{{$id}}">'+'{{$role}}'
                    @endforeach
                + '</option>'
                + '</select></td>'


                + '<td>' + '<button class="set_all_disabled btn btn-primary btn-xs Save_row">' + 'Save' + '</button>'
                + '</td>'
                + '<td>'
                + '<button class="set_all_disabled btn btn-danger btn-xs delete_row"  >' + '<span class="lnr lnr-trash"></span>' + '</button>'
                + '</td>'

                + '</tr>';



            $("#op_table").prepend(row);
            var all_selected_v = $('.supplier_v');


            all_selected_v.each(function () {

                var selectt = $(this).parents('tr').find('.supplier_v').attr('current_value');

                console.log('selectt : ' + selectt);
                $('.'+selectt).remove();

            }) ;
            $(".js-example-disabled-results").select2();
            //get_master_acc_drop_down($('.master_account'));

            //$(".po_values").siblings('.select2').css('width', '71px');

        });

    </script>


    <script>
        $(document).on("click", ".Save_row", function () {

            var empty = false;
            var call_save = false;
            var input = $(this).parents("tr").find('input[type="number"]');
            var selectors = $(this).parents("tr").find('select');


            input.each(function () {
                if (!$(this).val() || $(this).val() == "") {
                    $(this).addClass("error");
                    empty = true;
                } else {
                    $(this).removeClass("error");
                }
            });

            selectors.each(function () {
                if (!$(this).val() || $(this).val() == "") {
                    $(this).addClass("error");
                    empty = true;
                } else {
                    $(this).removeClass("error");
                }
            });

            console.log('empty ' + empty);

            $(this).parents("tr").find(".error").first().focus();


            if (!empty) {

                    $(this).prop('disabled', true);
                    save_table_effect($(this));
            }


        });

        function save_table_effect(thiss) {


            console.log('passed')

            /*--------------  all select ------------*/
            var select_array = ['supplier_v', 'currency_v'];
            var r = 0;
            var all_dowp_down = thiss.parents("tr").find('select');
            all_dowp_down.each(function () {
                var value_select = $(this).val();
                var text_select = $(this).find('option:selected').text();
                $(this).parent("td").addClass(select_array[r]).attr('current_value', value_select).html(text_select);
                r++;

            });
            /*--------- all input  ------------------*/
            var input_array = ['cost_price_v'];
            var all_input = thiss.parents("tr").find('input[type="number"]');

            var i = 0;
            all_input.each(function () {

                var value_select = $(this).val();
                $(this).parent("td").addClass(input_array[i]).attr('current_value', value_select).html(value_select);

                i++;
            });
            /*--------------------------*/

            if (thiss.parents("tr").hasClass('added_now')) {
                thiss.parents("tr").removeClass('added_now');

            }
            if (thiss.parents("tr").hasClass('edited_now')) {
                thiss.parents("tr").removeClass('edited_now');

            }

            //|| thiss.siblings().find('.create_edit_v') ){
            //console.log(thiss.closest('tr').attr('id'))


            if ($('.added_now').length == 0) {
                $("#add_row").removeAttr("disabled");
            }

            thiss.text('Edit');
            thiss.removeClass('Save_row');
            thiss.addClass('edit_row');
            thiss.prop('disabled', false);
        }
    </script>


    {{----------------------  edit row --------------------}}
    <script>
        $(document).on("click", ".edit_row", function () {
            if (!$('#op_table tr').hasClass('added_now') && !$('#op_table tr').hasClass('edited_now') ) {
                var input_class_array = ['cost_price'];
                var s = 0;

                $('#add_row').prop('disabled', true);
                $(this).parents("tr").addClass('edited_now');

                $(this).parents("tr").find("td:not(:last-child)").each(function () {
                    if ($(this).attr('td_type') == 'input') {

                        $(this).html('<td style="display: block;" td_type="input"><input value = "'+$(this).text() +'" class="'+ input_class_array[s] +' row_data pull-left form-control " min="1.00" onkeypress="return (event.charCode == 8 || event.charCode == 1) ? null : event.charCode >= 48 && event.charCode <= 57 ||  event.charCode == 46" data-role="input" type="number"  placeholder="0" /></td>');
                        s++;
                    }

                    else if ($(this).attr('td_type') == 'search') { 
                        if ($(this).hasClass('supplier_v')) {
                            $(this).html('<select class="supplier select2 form-control js-example-disabled-results">\n'
                                + '<option value="">' + '' + '</option>'
                                    @foreach($supplier as $suppliersing)
                                +'<option curr_id="{{$suppliersing->CurrencyID}}" class="{{$suppliersing->VendorID}}" value="{{$suppliersing->VendorID}}">'+'{{$suppliersing->VendorName}}'
                                    @endforeach
                                + '</option>'
                                + '</select>');

                            var selected_v = $(this).attr('current_value');
                            $(this).find(".supplier").val(selected_v);
                            var all_selected_v = $('.supplier_v');


                            all_selected_v.each(function () {

                                var selectt = $(this).parents('tr').find('.supplier_v').attr('current_value');
                                if(selected_v != selectt){
                                    console.log('selectt : ' + selectt);
                                    $('.'+selectt).remove();
                                }

                            }) ;

                            $(".supplier").select2();
                            $(".supplier").siblings('.select2').css('width', '71px');
                        }

                        else if ($(this).hasClass('currency_v')) {
                            $(this).html('<select class="currency form-control js-example-disabled-results">\n'
                                + '<option value="">' + '' + '</option>'
                                    @foreach($currency as $id=>$role)
                                +'<option value="{{$id}}">'+'{{$role}}'
                                    @endforeach
                                + '</option>'
                                + '</select>');

                            var selected_v = $(this).attr('current_value');
                            $(this).find(".currency").val(selected_v);
                            $(".currency").select2();
                            $(".currency").siblings('.select2').css('width', '71px');
                        }

                    }


                });

                $("#add_row").attr("disabled", "disabled");
                $(this).text('Save');
                $(this).removeClass('edit_row');
                $(this).addClass('Save_row');

            }
        });

    </script>

    <!--delete row in table -->
    <script>

        $(document).on('click', '.delete_row', function () {

            $(this).hide(function () {
                $(this).closest('td').append('<button class="btn btn-danger set_all_disabled" type="button" id="yes" style="display: none;">yes</button>' +
                    '<button class="btn btn-default set_all_disabled" type="button" id="no" style="display: none;">no</button>');
                $(this).closest('td').find("#yes").show('slow').delay(1000);
                $(this).closest('td').find("#no").show('slow').delay(1000);

            });
        });

        $(document).on('click', '#yes', function () {

            $(this).closest('tr').remove();
            if ($('.added_now').length == 0) {
                $("#add_row").removeAttr("disabled");
            }
        });

        $(document).on('click', '#no', function () {

            console.log('noo ');

            $(this).closest('td').find(".delete_row").show('slow').delay(1000);
            $(this).closest('td').find("#yes").hide('slow').delay(1000).remove();
            $(this).closest('td').find("#no").hide('slow').delay(1000).remove();
            /*
            $(this).closest('td').find("#yes").hide('slow').delay(1000);
            $(this).closest('td').find("#no").hide('slow').delay(1000);*/


        });


    </script>




    {{-----------   on leave page -------------------}}
    <script type='text/javascript'>

        $(window).bind('beforeunload', function () {
            if ($('#save_po').length) {
                return 'Are you sure you want to leave?';
            }

        });


    </script>

    {{---------------------- delete product ------------------------------}}

    <script>
        $(document).on('click', '#delete_po', function () {
            var cur_po_id = $('#set_po_id').attr('current_po_id');

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/products/delete',
                dataType: 'json',
                type: 'Post',
                data: {
                    _token: '{{csrf_token()}}',
                    cur_po_id: cur_po_id,
                },

                success: function (response) {
                    console.log(response);
                    window.location.href = '/journals';
                }

            });

        });
    </script>

    {{-----------------------print----------------}}
    <script>

        $(document).on('click', '#print', function () {

            //  window.location.href = 'get_po2/276'

            var id = $('#set_po_id').attr('current_po_id');
            window.open('/get_product_rpt/' + id + '', '_blank');


        });

    </script>

    {{------------------------ change currency on supplier change ------------------------------}}
    <script>
        $(document).on('change','.supplier',function(){
           var currency = $(this).parents('tr').find('.currency');
            currency.select2('destroy');
            currency.val($('option:selected', this).attr('curr_id'));
            currency.select2();
            currency.prop('disabled',true);
        });
    </script>
@endsection