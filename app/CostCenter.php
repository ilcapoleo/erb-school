<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CostCenter extends Model
{
    protected $table = 'cost_centers';
    protected $primaryKey = 'Cost_id';

    public function salary()
    {
        return $this->belongsTo(SalaryScale::class, 'cost_id');
    }

}
