<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AcademicYear extends Model
{
    public $timestamps = false;
    protected $table = 'acadimic_year';
    protected $fillable = ['year_name','short_name','start','end'];
    protected $primaryKey = 'acadimic_id';

}
