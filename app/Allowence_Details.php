<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Allowence_Details extends Model
{
    //
    public $timestamps = false;
    protected $table = 'allowence_details';
    protected $primaryKey = 'allow_details_id';
}
