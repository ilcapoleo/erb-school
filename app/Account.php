<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    //
    public $timestamps = false;
    protected $table = 'accounts';
    protected $primaryKey = 'child_id';
}
