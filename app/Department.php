<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    //
    public $timestamps = false;
    protected $table = 'departments';
    protected $primaryKey = 'dep_id';

    public function Employee(){
        return $this->hasMany('App\Employee','depart_id');
    }

}
