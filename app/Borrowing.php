<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Borrowing extends Model
{
    //
    public $timestamps = false;
    protected $table = 'borrowing';
    protected $primaryKey = 'borrow_id';
}
