<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Salay_plan extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $timestamps = false;
    protected $table = 'salay_plan';
    protected $primaryKey = 'plan_id';


}