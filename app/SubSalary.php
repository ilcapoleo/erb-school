<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubSalary extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $timestamps = false;
    protected $table = 'sub_salary';
    protected $primaryKey = 'sub_id';
    protected $fillable = [
        'sub_name', 'sub_code'
    ];


}
