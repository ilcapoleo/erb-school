<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Details_inventory extends Model
{
    //
    public $timestamps = false;
    protected $table = 'inventory_details';
    protected $primaryKey = 'sub_doc_id';
}
