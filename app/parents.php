<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class parents extends Model
{
    //
    public $timestamps = false;
    protected $table = 'parents';
    protected $primaryKey = 'par_id';
    protected $fillable = ['par_id','1name','2name','3name','family_name','ID_NO','Pass_ID','EMail','Contact_NO',
        'Address',
        'Country_ID',
        'other_Notes',
        'Finance',
        'Main_account',
        'Relation_Staff_ID',
        'person_id',
        'MainId',
    ];
    protected $mappedOnly = true;
    protected $maps = [

        '1name' => 'first_name',
        '2name' => 'middle_name',
        '3name' => 'last_name',

    ];
         protected $hidden = ['1name','2name','3name'];

         protected $appends = ['first_name','middle_name','last_name'];
         public function appends()
         {
             return $this->appends;
         }



}
