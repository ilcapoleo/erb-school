<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class po extends Model
{

    public $timestamps = false;
    protected $table = 'po_details';
    protected $primaryKey = 'po_id';
}
