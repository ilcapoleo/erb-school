<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classes extends Model
{
    //
    public $timestamps = false;
    protected $table = 'class';
    protected $primaryKey = 'classid';
}
