<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Operation extends Model
{
    //
    public $timestamps = false;
    protected $table = 'inventory_documents';
    protected $primaryKey = 'inv_doc_id';
}
