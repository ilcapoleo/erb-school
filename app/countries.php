<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class countries extends Model
{
    //
    public $timestamps = false;
    protected $table = 'countries';
    protected $fillable = ['name'];
    protected $primaryKey = 'id';

}
