<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Allowence_deduction extends Model
{
    //
    public $timestamps = false;
    protected $table = 'allowence_deduction';
    protected $primaryKey = 'allow_id';
}
