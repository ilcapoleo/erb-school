<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Details_po extends Model
{
    //
    public $timestamps = false;
    protected $table = 'po_details';
    protected $primaryKey = 'sub_po_id';

    protected $fillable = ['po_id','item_type_id','item_name_id','quantity','unit_price','include_tax','sub_total','tax_string','closed_details'];

}
