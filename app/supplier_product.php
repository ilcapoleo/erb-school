<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class supplier_product extends Model
{
    //
    public $timestamps = false;
    protected $table = 'supplier_product';
    //protected $primaryKey = 'pro_id';
}
