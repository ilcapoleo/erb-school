<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class fees extends Model
{
    protected $table = 'fees_head';
    public $timestamps = false;
    protected $primaryKey = 'fees_id';
    protected $fillable = [
        'Fees_Name', 'Fees_type','account_id','journal_id'
    ];


}
