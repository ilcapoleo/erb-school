<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class leave_t extends Model
{
    //
    public $timestamps = false;
    protected $table = 'leave_type';
    protected $primaryKey = 'leave_id';
}
