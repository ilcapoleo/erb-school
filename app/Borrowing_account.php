<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Borrowing_account extends Model
{
    //
    public $timestamps = false;
    protected $table = 'borrowing_account';
    protected $primaryKey = 'acc_id';
}
