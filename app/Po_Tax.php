<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Po_Tax extends Model
{
    //
    public $timestamps = false;
    protected $table = 'po_tax';
    //protected $primaryKey = 'tax_id';
}
