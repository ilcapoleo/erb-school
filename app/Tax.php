<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Tax extends Model
{
    //
    public $timestamps = false;
    protected $table = 'tax';
    protected $primaryKey = 'tax_id';

    /*public function get_tax_id($tax_name)
    {
        return DB::table('tax')->where('tax_name','=',$tax_name)
            ->first();
        //Tax::where('tax_name', $tax_name)->first()->tax_id;

    }*/
}
