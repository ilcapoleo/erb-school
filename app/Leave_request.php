<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Leave_request extends Model
{
    //
    public $timestamps = false;
    protected $table = 'leave_request';
    protected $primaryKey = 'leave_r_id';
}
