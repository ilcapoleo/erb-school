<?php

namespace App\Http\Controllers;

use App\Department;
use App\Job;
use Illuminate\Http\Request;

class JobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $showed_search_list = array('ID','Job Name','Job Short Code','Department');
        $database_search_list = array(1,2,3,4);

        $jobs=Job::all();

      return view ('school_pages.jobs.index',compact('jobs','showed_search_list','database_search_list'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = Department::pluck('dep_name','dep_id');

        return view ('school_pages.jobs.create',compact('departments'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $jobs = new Job();
        $jobs->job_name  = $request->job_name;
        $jobs->job_short_code= $request->job_code;
        $jobs->department_id = $request->department;
        $jobs->save();
        return redirect('jobs/'.$jobs->job_id.'/edit');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $job = Job::findorfail($id);
        $departments = Department::pluck('dep_name','dep_id');
        return view ('school_pages.jobs.create',compact('job','departments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $jobs = Job::findorfail($id);
        $jobs->job_name  = $request->job_name;
        $jobs->job_short_code= $request->job_code;
        $jobs->department_id = $request->department;
        $jobs->save();
        return redirect('jobs/'.$id.'/edit');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $job = Job::findOrFail($id);
        $job->delete();

//        DB::table('allowence_details')
//            ->where('allow_id', $request->cur_po_id)
//            ->delete();

        return response($id);
    }
}
