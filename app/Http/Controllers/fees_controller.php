<?php

namespace App\Http\Controllers;

use App\fees;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class fees_controller extends Controller
{
    public function creat_fees()
    {
        {
            $fees_type = DB::table('fees_type')->orderBy('fees_type', 'asc')
                ->pluck('fees_type', 'fee_id');
            $accounts = DB::table('accounts')->orderBy('child_name', 'asc')
                ->pluck('child_name', 'child_id');
            $journals = DB::table('journals')->orderBy('Journal_name', 'asc')
                ->pluck('Journal_name', 'journal_id');


            return view('school_pages.fees', compact('fees_type', 'accounts', 'journals'));
        }

    }


    public function saveData(Request $req)
    {
        $rules = array(


            'fees_name' => 'required',
            'fees_type' => 'required',
            'account_name' => 'required',
            'journal_name' => 'required',
//            'sub_code' => 'required',

        );
        $validator = \Illuminate\Support\Facades\Validator::make($req->all(), $rules);

        if ($validator->passes()) {

            /*------------- edit -----------------*/
            if ($req->current_po_id != "" && $req->current_po_id) {

                $department = fees::findOrFail($req->current_po_id);
                $department->Fees_Name = $req->fees_name;
                $department->Fees_type = $req->fees_type;
                $department->account_id = $req->account_name;
                $department->journal_id = $req->journal_name;
                $department->save();


                $depart_id = $department->fees_id;
//                $sub= SubSalary::findOrFail($req->current_po_id);
//                $sub->sub_name = $req->sub_name;
//                $sub->sub_code = $req->sub_code;
//                $sub->save();



                return response([$req, 'edit_po', 'current_po_id' => $depart_id]);
            } /*----------------- create new  ---------------*/
            else {
                $user = new fees();

                $user->Fees_Name = $req->fees_name;
                $user->Fees_type = $req->fees_type;
                $user->account_id = $req->account_name;
                $user->journal_id = $req->journal_name;
                $user->save();
                $depart_id = $user->fees_id;

            }
            /*

                $user_data=$user->scal_save($data);*/
            /*$user=SalaryScale::create(['salary_scalar_name' => 'Flight 10'],['short_code_name' => 'Flight 10']);*/
            return response([$req, 'edit_po', 'current_po_id' => $depart_id]);
        }

        elseif ($validator->fails()) {

            return response()->json(['error' => $validator->messages()]);

        }


    }
    public function delete_fees(Request $req)
    {


        $department = fees::findOrFail($req->cur_po_id);
        $department->delete();

        return response($req);


    }

    public function get_this_fees($id)
    {


        $fees_type = DB::table('fees_type')->orderBy('fees_type', 'asc')
            ->pluck('fees_type', 'fee_id');
        $accounts = DB::table('accounts')->orderBy('child_name', 'asc')
            ->pluck('child_name', 'child_id');
        $journals = DB::table('journals')->orderBy('Journal_name', 'asc')
            ->pluck('Journal_name', 'journal_id');

        $department = fees::findOrFail($id);


        return view('school_pages.fees',compact('fees_type', 'accounts', 'journals','department'));

    }


}

