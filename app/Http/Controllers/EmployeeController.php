<?php

namespace App\Http\Controllers;

use App\AcademicYear;
use App\Allowence_deduction;
use App\Allowence_Details;
use App\Classes;
use App\countries;
use App\Country;
use App\Department;
use App\Employee;
use App\EmployeeAllowence;
use App\EmployeeFamily;
use App\EmployeeSalary;
use App\Job;
use App\Nationality;
use App\parents;
use App\relations_students_parents;
use App\Setting;
use App\students;
use App\SubSalary;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

use Image;
use phpDocumentor\Reflection\Types\Parent_;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $showed_search_list = array('ID', 'Employee Name', 'Nationality', 'Department', 'Job', 'Gender', 'Birth Date', 'Leave');
        $database_search_list = array(1, 2, 3, 4, 5, 6, 7, 8);
        $employees = Employee::orderby('employee_id', 'desc')->get();

        return view('school_pages.employees.index', ['showed_search_list' => $showed_search_list, 'database_search_list' => $database_search_list], compact('employees'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $nation = Nationality::pluck('name', 'id');
        $academic_year = AcademicYear::pluck('year_name', 'acadimic_id');
        $academic_year_table = AcademicYear::all();
        $depart = Department::pluck('dep_name', 'dep_id');
        $job = Job::pluck('job_name', 'job_id');
        $allows = Allowence_deduction::where('allow_type', 0)->pluck('allow_name', 'allow_id');
        $sub_salary = SubSalary::pluck('sub_name', 'sub_id');
        $class = Classes::pluck('ClassName', 'classid');
        $country = Country::pluck('Country_Name', 'Country_ID');
        /////////////////////Date OF Birth/////////////////////////////////
        $get_min_age = json_decode(Setting::where('type', 'min_age')->first()->value);
        $min_age = Carbon::now()->addYears(-$get_min_age->employee)->toDateString();
        ///////////////////////////////////////////////////////////////////

        return view('school_pages.employees.create', compact('country', 'min_age', 'nation', 'academic_year', 'depart', 'job', 'allows', 'sub_salary', 'class', 'academic_year_table'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->Marital_Status == "married" && $request->family =="yes" )
        {
            $rules = [
                'employee_name' => 'required',
                'family_name' => 'required',
                'children_number' => 'required|min:1',
                'country' => 'required',
                'parent_email' => ['required','unique:employee,email,'.$request->edit_employee_id.',employee_id'],
                'partner_first_name' => 'required',
                'partner_family_name' => 'required',
                'partner_middle_name' => 'required',
                'partner_last_name' => 'required',
                'middle_name' => 'required',
                'last_name' => 'required',
                'gender' => 'required',
                'nation' => 'required',
                'dateofbirth' => 'required',
                'user_type' => 'required',
                'Marital_Status' => 'required',
                'family' => 'required',
                'Percentage_Salary' => 'required',
                'Date_of_contract' => 'required',
                'academic_year2' => 'required',
                'job' => 'required',
                'leave' => 'required',
                'salary_table_row' => 'required',
            ];
            $validator = Validator::make($request->all(), $rules);
            $validator->SetAttributeNames([
                'employee_name' => 'Employee First Name',
                'middle_name' => 'Employee Middle Name',
                'last_name' => 'Employee Last Name',
                'gender' => 'gender',
                'nation' => 'nationality',
                'dateofbirth' => 'date of birth',
                'user_type' => 'user_type',
                'Marital_Status' => 'Marital Status',
                'Percentage_Salary' => 'Percentage Salary',
                'Date_of_contract' => 'Date of contract',
                'academic_year2' => 'academic year',
                'job' => 'job',
                'leave' => 'leave',
                'salary_table_row' => 'salary table row',
                'children_number' => 'children number',
                'family_name' => 'family name',
                'country' => 'country',
                'parent_email' => 'parent email',
                'partner_first_name' => 'partner first name',
                'partner_family_name' => 'partner family name',
                'partner_middle_name' => 'partner middle name',
                'partner_last_name' => 'partner last name',
            ]);
        } else {
            $rules = [
                'employee_name' => 'required',
                'family_name' => 'required',
                'country' => 'required',
                'parent_email' => ['required','unique:employee,email,'.$request->edit_employee_id.',employee_id'],
                'middle_name' => 'required',
                'last_name' => 'required',
                'gender' => 'required',
                'nation' => 'required',
                'dateofbirth' => 'required',
                'user_type' => 'required',
                'Marital_Status' => 'required',
                'family' => 'required',
                'Percentage_Salary' => 'required',
                'Date_of_contract' => 'required',
                'academic_year2' => 'required',
                'job' => 'required',
                'leave' => 'required',
                'salary_table_row' => 'required',
            ];
            $validator = Validator::make($request->all(), $rules);
            $validator->SetAttributeNames([
                'employee_name' => 'Employee First Name',
                'middle_name' => 'Employee Middle Name',
                'last_name' => 'Employee Last Name',
                'gender' => 'gender',
                'nation' => 'nationality',
                'dateofbirth' => 'date of birth',
                'user_type' => 'user_type',
                'Marital_Status' => 'Marital Status',
                'family' => 'Family',
                'Percentage_Salary' => 'Percentage Salary',
                'Date_of_contract' => 'Date of contract',
                'academic_year2' => 'academic year',
                'job' => 'job',
                'leave' => 'leave',
                'salary_table_row' => 'salary table row',
                'family_name' => 'family name',
                'country' => 'country',
                'parent_email' => 'parent email',

            ]);
        }

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()]);
        } else {
            try {
                DB::beginTransaction();

                if ($request->edit_employee_id != "" && $request->edit_employee_id) {

                    $emp = Employee::findorfail($request->edit_employee_id);
                    DB::table('employee_salaries')->where('employee_id', $request->edit_employee_id)->where('is_closed', 0)->delete();
                    if($request->Marital_Status == 'married' && $request->family == 'yes')
                    {
                        $is_parent = @parents::where('Relation_staff_ID',$request->edit_employee_id)->first()->par_id ?parents::where('Relation_staff_ID',$request->edit_employee_id)->first()->par_id :"" ;
                        if($is_parent != ""){
                            $parent = parents::findorfail($is_parent);
                            $parent->{'1name'} = $request->employee_name;
                            $parent->{'2name'}= $request->middle_name;
                            $parent->{'3name'}= $request->last_name;
                            $parent->family_name = $request->family_name;
                            $parent->Email = $request->parent_email;
                            $parent->Address = $request->address;
                            $parent->Country_ID = $request->country;
                            $parent->save();
                            $student = DB::table('student_parent_relations')->pluck('person_id')[0];

                            $other_parent = DB::table('student_parent_relations')->pluck('par_id')->where('person_id',$student)
                                ->where('par_id','<>',$parent->par_id);
                            $parent2 = parents::findorfail($other_parent);
                            $parent2->{'1name'} = $request->partner_first_name;
                            $parent2->{'2name'} = $request->partner_middle_name;
                            $parent2->{'3name'}= $request->partner_last_name;
                            $parent2->family_name = $request->partner_family_name;
                            $parent2->Address = $request->address;
                            $parent2->Country_ID = $request->country;
                            $parent2->save();
                        }
                    }

                } else {

                    $emp = new Employee();
                }
                $emp->children_number = $request->children_number;
                $emp->family_name = $request->family_name;
                $emp->country_id = $request->country;
                $emp->email = $request->parent_email;
                $emp->partner_first_name = $request->partner_first_name;
                $emp->partner_family_name = $request->partner_family_name;
                $emp->partner_middle_name = $request->partner_middle_name;
                $emp->partner_last_name = $request->partner_last_name;
                $emp->employee_name = $request->employee_name;
                $emp->middle_name = $request->middle_name;
                $emp->last_name = $request->last_name;
                $emp->gender = $request->gender;
                $emp->nationality_id = $request->nation;
                $emp->date_of_birth = $request->dateofbirth;
                $emp->mobile_work = $request->phone;
                $emp->office_location = $request->office_loc;
                $emp->home_address = $request->address;
                $emp->user_type = $request->user_type;
                $emp->marital_status = $request->Marital_Status;
                $emp->family = $request->family ;
                $emp->percentage_salary = $request->Percentage_Salary;
                $emp->date_of_contract = $request->Date_of_contract;
                $emp->contract_years = $request->contract_years;
                $emp->acadimic_year_id = $request->academic_year2;
                $emp->job_id = $request->job;
                $emp->depart_id = $request->depart;
                $emp->manager_id = $request->manager;
                $emp->is_manager = $request->is_manager;
                $emp->leave = $request->leave ? $request->leave : "no";
                $emp->date_of_leave = $request->leave == "yes" ? $request->Date_Leaving : null;
                $emp->save();
                $image = $request->file('profile_image');

                $filename = $emp->employee_id . '_' . '.png';
                if ($image) {
                    Image::make($request->file('profile_image'))->save('uploads/employees/' . $filename);

                }
                foreach ($request->salary_table_row as $row) {
                    $sal = new EmployeeSalary();
                    $sal->employee_id = $emp->employee_id;
                    $sal->sub_salary_id = $row[0];
                    $sal->academic_year_id = $row[1];
                    $sal->start_in = $row[2];
                    $sal->save();
                }
                DB::commit();
                session()->flash('success', 'created');
            } catch (Exception $ex) {
                DB::rollBack();
                session()->flash('error', 'failed');
                return back();
            }

            return response(['employee_id' => $emp->employee_id]);

        }
    }

    public function getManager(Request $request)
    {
        $id = $request->id;
        $emp_id = $request->emp_id;
        if ($emp_id != null || $emp_id != "") {
            $manager_name = Employee::where('depart_id', $id)->where('is_manager', '1')->where('employee_id', '<>', $emp_id)->get();
        } else {
            $manager_name = Employee::where('depart_id', $id)->where('is_manager', '1')->get();
        }

        if(count($manager_name) != 0){
        return view('school_pages.employees.get_manager', compact('manager_name'));
        }else{

            return response (0);
        }
    }

    public function getJob(Request $request)
    {
        $id = $request->id;
        $jobs = Job::where('department_id', $id)->get();
        return view('school_pages.employees.get_job', compact('jobs'));
    }

    public function getAllowVal(Request $request)
    {
        $id = $request->id;
        $currency = $request->currency;

        $allow_val = @Allowence_Details::where('allow_id', $id)->where('currency_id', $currency)->first()->value ? Allowence_Details::where('allow_id', $id)->where('currency_id', $currency)->first()->value : 0;

        return response($allow_val);


    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $employee = Employee::findorfail($id);
        $employee_family = EmployeeFamily::where('employee_id', $id)->get();
        $employee_allowances = EmployeeAllowence::where('employee_id', $id)->get();
        $employee_salary = EmployeeSalary::where('employee_id', $id)->get();
        $user = Employee::find($id)->employee_user_id ? User::where('id', Employee::find($id)->employee_user_id)->first()->name : "Not Assigned To User Yet";
        $academic_year_table = AcademicYear::all();
        $nation = Nationality::pluck('name', 'id');
        $academic_year = AcademicYear::pluck('year_name', 'acadimic_id');
        $depart = Department::pluck('dep_name', 'dep_id');
        $job = Job::pluck('job_name', 'job_id');
        $allows = Allowence_deduction::where('allow_type', 0)->pluck('allow_name', 'allow_id');
        $sub_salary = SubSalary::pluck('sub_name', 'sub_id');
        $class = Classes::pluck('ClassName', 'classid');
        $country = Country::pluck('Country_Name', 'Country_ID');

        /////////////////////Date OF Birth//////////////////////////////
        $get_min_age = json_decode(Setting::where('type', 'min_age')->first()->value);
        $min_age = Carbon::now()->addYears(-$get_min_age->employee)->toDateString();
        ////////////////////////////////////////////////////////////////

        return view('school_pages.employees.create', compact('country', 'min_age', 'academic_year_table', 'user', 'employee_salary', 'employee_allowances', 'employee_family', 'employee', 'nation', 'academic_year', 'depart', 'job', 'allows', 'sub_salary', 'class'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
