<?php

namespace App\Http\Controllers;

use App\cost_centers;
use Illuminate\Http\Request;
use App\Class_data;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;


class roles_permissions_controller extends Controller
{
    //
   /* public function ret_cost_page()
    {
        return view('admin_pages.cost_center_page');
    }*/

   /*---------------- permission --------------------*/
    public function get_permission_page()
    {
        return view('custom.create_permissions');
    }

    public function add_new_permission(Request $request)
    {
        $permission = Permission::create(['name' => $request->permission]);

        return response([$permission,'permission_id'=>$permission->id]);
    }

    public function edit_permission($id)
    {
        $permission = Permission::findOrFail($id);

    }

    /*------------------ roles -------------------------*/
    public function get_roles_page()
    {
        $permissions = DB::table('permissions')->orderBy('id','asc')->pluck('name','name');
        $modules = DB::table('modules')->orderBy('name','desc')->pluck('name','id');
        return view('custom.create_roles',compact('permissions','modules'));
    }

    public function add_new_role(Request $request)
    {
        if($request->permissions)
        {
            $role = Role::create(['name' => $request->role]);
            $role->givePermissionTo($request->permissions);

            $module_id = Role::findOrFail($role->id);
            $module_id->module_id = $request->module;
            $module_id->save();

        }

        return response ($role->id);

    }


}
