<?php

namespace App\Http\Controllers;



use App\Employee;
use App\po_rpt;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\PDF_HF;



class creat_rpt_po extends Controller
{


    //
    /* public function ret_cost_page()
     {
         return view('admin_pages.cost_center_page');
     }*/
    public function get_po2($id)

    {
        // define('FPDF_FONTPATH',public_path('font'));

        /*$accounts = Chart::all();
        $pdf = PDF::loadView('admin_pages.journal_entry_repory',['accounts'=>$accounts]);
        return $pdf->stream('accounts.pdf');*/

        //get invoices data

        $master_po = DB::table('po_master')
            ->join('vendors', 'po_master.supplier_id', '=', 'vendors.VendorID')
            ->leftJoin('employee', 'po_master.beneficiary_user', '=', 'employee.employee_id')
            ->join('departments', 'po_master.department_id', '=', 'departments.dep_id')
            ->join('currencies', 'po_master.cur_id', '=', 'currencies.Currency_id')
            ->join('document_status', 'po_master.po_status_id', '=', 'document_status.status_id')
            ->where('po_master.po_id', '=', $id)
            ->orderBy('po_master.po_id', 'desc')
            ->first();



        $details_po = DB::table('po_details')
            ->join('products', 'po_details.item_name_id', '=', 'products.pro_id')
            ->join('products_type', 'po_details.item_type_id', '=', 'products_type.pro_type_id')
            ->where('po_details.po_id', '=', $id)
            ->orderBy('po_details.sub_po_id', 'desc')
            ->get();

        //$requested_by = @Employee::find($master_po->creator_user_id)->employee_name;
        $requested_by = @Employee::find($master_po->creator_user_id);

        $approved_by = @Employee::find($master_po->approved_user_id);


        $pdf = new PDF_HF('P', 'mm', 'A4');

        $pdf->AddPage();

        $pdf->AliasNbPages('{pages}');

//set font to arial, bold, 14pt

        $pdf->SetFont('Arial', 'B', 12);
        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX(90);

        if($master_po->po_seq == 0 )
        {$pdf->Cell(40,10,"Po : Draft",0,0,'C');}
        else{
            $pdf->Cell(40,10,"Po No : $master_po->po_seq",0,0,'C');
        }

        $pdf->Ln(11);

        $pdf->setFillColor(230, 230, 230);


//Cell(width , height , text , border , end line , [align] )

        $y = $pdf->GetY();
        $pdf->SetY($y);
        $pdf->Cell(189, 50, "", 0, 0, 'C');

        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX(90);


        $pdf->Ln(10);
        $pdf->SetX(10);
        $pdf->Cell(15, 10, 'Supplier', 0, 0, 'L');
        $pdf->SetX(65);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(70);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, $master_po->VendorName, 0, 0, 'L');
        $pdf->SetX(70);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');

        $pdf->SetX(-100);
        $pdf->SetFont('', 'B');

        $pdf->Cell(15, 10, 'Order Date', 0, 0, 'L');
        $pdf->SetX(-65);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(-60);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, $master_po->order_date, 0, 0, 'L');
        $pdf->SetX(-60);
        $pdf->Cell(30, 12, '.................................', 0, 0, 'L');

        $pdf->Ln(10);
        $pdf->SetX(10);
        $pdf->SetFont('', 'B');

        $pdf->Cell(15, 10, 'Beneficiary User', 0, 0, 'L');
        $pdf->SetX(65);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(70);
        $pdf->SetFont('', '');
        $pdf->Multicell(40,5,$master_po->employee_name.' '.$master_po->middle_name.' '.$master_po->last_name.' '.$master_po->family_name,0,1,0);
//        $full_name = $master_po->employee_name.' '.$master_po->middle_name.' '.$master_po->last_name.' '.$master_po->family_name ;
//        $cell_width = 40;
        /*$tempcell_width = 30;*/

       /* while ($pdf->GetStringWidth($full_name) > $cell_width)
        {
            $pdf->SetFontSize($tempcell_width -=0.1 );
        }*/
//        $tempcell_width = $pdf->wrap_text($full_name,$cell_width);

//        $pdf->Cell($cell_width, 10,$full_name ,0, 0, 'L');

//        $pdf->SetFontSize($tempcell_width);
        $pdf->SetFont('Arial', 'B', 12);


        $pdf->SetX(70);
        $pdf->Cell(30, 2, '..............................', 0, 0, 'L');

        $pdf->SetX(-100);
        $pdf->SetFont('', 'B');

        $pdf->Cell(15, 10, 'Expected Date', 0, 0, 'L');
        $pdf->SetX(-65);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(-60);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, $master_po->expected_date, 0, 0, 'L');
        $pdf->SetX(-60);
        $pdf->Cell(30, 12, '.................................', 0, 0, 'L');


        $pdf->Ln(10);
        $pdf->SetX(10);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'Beneficiary management', 0, 0, 'L');
        $pdf->SetX(65);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(70);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, $master_po->dep_name, 0, 0, 'L');
        $pdf->SetX(70);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');

        $pdf->SetX(-100);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'Date Approved', 0, 0, 'L');
        $pdf->SetX(-65);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(-60);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, $master_po->approved_date, 0, 0, 'L');
        $pdf->SetX(-60);
        $pdf->Cell(30, 12, '.................................', 0, 0, 'L');


        $pdf->Ln(10);
        $pdf->SetX(10);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'currency', 0, 0, 'L');
        $pdf->SetX(65);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(70);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, $master_po->Currency_name, 0, 0, 'L');
        $pdf->SetX(70);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');


        $pdf->Ln(10);
        $pdf->setFillColor(230, 230, 230);
        $pdf->SetFont('', 'B');
        $pdf->Cell(30, 7, 'Item Type', 1, 0, 'C', 1);
        $pdf->Cell(30, 7, 'Item Name', 1, 0, 'C', 1);
        $pdf->Cell(20, 7, 'Quantity', 1, 0, 'C', 1);
        $pdf->Cell(20, 7, 'Unit Price', 1, 0, 'C', 1);
        $pdf->Cell(20, 7, 'Include', 1, 0, 'C', 1);
        $pdf->Cell(30, 7, 'Tax', 1, 0, 'C', 1);
        $pdf->Cell(20, 7, 'Total Tax', 1, 0, 'C', 1);
        $pdf->Cell(20, 7, 'Subtotal', 1, 1, 'C', 1);
        /*-----------------------  change header -----------------------------*/
        $headrs_name = array(
            array("Item Type", 30),
            array("Item Name", 30),
            array("Quantity	", 20),
            array("Unit Price", 20),
            array("Include", 20),
            array("Tax", 30),
            array("Total Tax", 20),
            array("Subtotal", 20)
        );
        $pdf->SetFillColor(50, 50, 50);


        $pdf->set_header_names($headrs_name);
        /*-------------------------------------------------------*/
        $pdf->SetFontSize(10);

        /*set the table header */
        $pdf->setHeaderTitle(true);

        /*------------------------------*/
        $pdf->SetWidths(array(30, 30, 20, 20, 20, 30, 20, 20));


        $un_taxes1 = 0;
        $un_taxes2 = 0;
        $txs1 = 0;
        $txs2 = 0;
        foreach ($details_po as $val) {
            $pdf->SetFont('Arial', '', 12);

            if ($val->include_tax == 0) {
                $yes = 'no';
            } else {
                $yes = 'yes';

            }

            $quantity = $val->quantity;
            $unit_price = $val->unit_price;
            $Include = $val->include_tax;
            $taxs = $val->tax_string;

            $re_sub_total = 0;
            $total_tax = 0;

            if ($taxs || $taxs != "") {
                $taxss = explode(',', $taxs);

                foreach ($taxss as $single_taxs) {
                    $tax_id = DB::table('tax')->where('tax_name', '=', $single_taxs)
                        ->first();//Tax::get_tax_id($single_taxs);
                    $id_tax = $tax_id->tax_id;
                    //$sub_val = DB::select('call calc_tax(?,?,?,?)',array($id_tax,$unit_price ,$quantity,$Include));
                    $sub_val = DB::select('call calc_tax(?,?,?,?)', [$id_tax, $unit_price, $quantity, $Include]);
                    $re_sub_total = $sub_val[0]->subtotal;
                    $total_tax = $total_tax + $sub_val[0]->taxval;

                }
                $un_taxes1 += $re_sub_total;
                $txs1 += $total_tax;


            } else if ($taxs == "") {

                $sub_val = DB::select('call calc_tax(?,?,?,?)', [0, $unit_price, $quantity, $Include]);
                $re_sub_total = $sub_val[0]->subtotal;
                $total_tax = +$sub_val[0]->taxval;
                $un_taxes2 += $re_sub_total;
                $txs2 += $total_tax;

            }


            $pdf->Row(array($val->pro_type_name, $val->pro_name, $val->quantity, $val->unit_price, $yes, $val->tax_string, $total_tax,$re_sub_total ));


        }

        $un_taxes = $un_taxes1 + $un_taxes2;
        $txs = $txs1 + $txs2;
        $pdf->SetX(40);

        /*remove the table header */
        $pdf->setHeaderTitle(false);
        /*------------------------------*/

        $y = $pdf->GetY();
        $po_tax_details = DB::select('call PO_TAX_DETAILS(?)',[$id]);

        $pdf->Ln(5);

        $pdf->SetX(10);
        $pdf->setFillColor(230, 230, 230);
        $pdf->SetFont('', 'B');
        $pdf->Cell(30, 7, 'Tax Name	', 1, 0, 'C', 1);
        $pdf->Cell(30, 7, 'Tax Value', 1, 0, 'C', 1);

        /*-----------------------  change header -----------------------------*/
        $headrs_name = array(
            array("Tax Name", 30),
            array("Tax Value", 30),

        );
        $pdf->SetFillColor(50, 50, 50);


        $pdf->set_header_names($headrs_name);
        /*-------------------------------------------------------*/
        $pdf->SetFontSize(10);

        /*set the table header */
        $pdf->setHeaderTitle(true);

        /*------------------------------*/

        $pdf->SetWidths(array(30, 30));

        $pdf->Ln(7);


        foreach ($po_tax_details as $val) {
            $pdf->SetFont('Arial', '', 12);

            $pdf->Row(array($val->tax_name, $val->TOATL));


        }

        /*remove the table header */
        $pdf->setHeaderTitle(false);
        /*------------------------------*/






        $pdf->SetY($y);

        $pdf->SetX(-80);
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->Cell(15, 10, 'Untaxes Amount', 0, 0, 'L');

        $pdf->SetX(-30);
        $pdf->setFillColor(230, 230, 230);

        $pdf->SetFont('', '');
        $pdf->Cell(20, 10, $un_taxes, 1, 0, 'C', 1);
        $pdf->Ln(10);
        $pdf->SetX(-80);
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->Cell(15, 10, 'Taxes', 0, 0, 'L');

        $pdf->SetX(-30);
        $pdf->setFillColor(230, 230, 230);

        $pdf->SetFont('Arial', '', 12);
        $pdf->Cell(20, 10, $txs, 1, 0, 'C', 1);
        $pdf->Ln(10);
        $pdf->SetX(-80);
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->Cell(15, 10, 'Total', 0, 0, 'L');

        $pdf->SetX(-30);
        $pdf->setFillColor(230, 230, 230);

        $pdf->SetFont('Arial', '', 12);
        $pdf->Cell(20, 10, $un_taxes + $txs, 1, 0, 'C', 1);








        $pdf->Ln(20);
        $pdf->SetX(10);
        $pdf->SetFont('Arial', 'UB', 12);

        $pdf->Cell(15, 10, 'Note:', 0, 0, 'L');
        $pdf->Ln(10);
        $pdf->SetX(10);
        $pdf->SetFont('', '');
        $pdf->Cell(80, 10, $master_po->po_notes, 0, 0, 'L');








        //APPROVED BY ---- REQUESTED BY //
        $pdf->Ln(5);
        $y = $pdf->GetY();
        $pdf->SetY($y);
        $x = $pdf->GetX();
        $pdf->SetX($x);

        $pdf->Ln(10);
        $pdf->SetX(20);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'Requested By', 0, 0, 'C');


        $pdf->SetX(-50);
        $pdf->SetFont('', 'B');

        $pdf->Cell(15, 10, 'Approved By', 0, 0, 'C');


        $pdf->Ln(10);
        $pdf->SetX(10);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, $requested_by->employee_name.' '.$requested_by->middle_name.' '.$requested_by->last_name.' '.$requested_by->family_name, 0, 0, 'L');
        $pdf->SetX(20);
        $pdf->Cell(15, 14, '........................', 0, 0, 'C');

        $pdf->SetX(-60);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10,  @$approved_by->employee_name.' '.@$approved_by->middle_name.' '.@$approved_by->last_name.' '.@$approved_by->family_name, 0, 0, 'L');
        $pdf->SetX(-50);
        $pdf->Cell(15, 14, '.........................', 0, 0, 'C');

        $pdf->Ln(20);
        $pdf->SetX(20);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'Accountant', 0, 0, 'C');


        $pdf->SetX(-50);
        $pdf->SetFont('', 'B');

        $pdf->Cell(15, 10, 'Financial Manager', 0, 0, 'C');

        $pdf->Ln(10);
        $pdf->SetX(20);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, "", 0, 0, 'L');
        $pdf->SetX(20);
        $pdf->Cell(15, 14, '........................', 0, 0, 'C');


        $pdf->SetX(-50);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, "", 0, 0, 'L');
        $pdf->SetX(-50);
        $pdf->Cell(15, 14, '........................', 0, 0, 'C');


        $pdf->Output();
        exit;
    }
}