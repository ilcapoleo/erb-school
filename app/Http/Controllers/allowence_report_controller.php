<?php

namespace App\Http\Controllers;

use App\AcademicYear;
use App\Account;
use App\Allowence_deduction;
use App\Allowence_Details;
use App\BeneficiaryType;
use App\Currency;
use App\SalaryScale;
use App\SubSalary;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\PDF_HF;


class allowence_report_controller extends Controller
{
    //-----------------------------table allowance-------------------------//
    public function get_allowence(Request $request)

    {

        $print_array = array();
        $array =  explode(',', $request->input('print_ids')[0]);

        foreach ($array as $single)
        {
            array_push($print_array, $single);
        }


        $query = DB::table('allowence_deduction')
            ->join('allowence_deduction_types','allowence_deduction.allow_type','=','allowence_deduction_types.allow_type_id')

            ->wherein('allowence_deduction.allow_id',$print_array)

            ->orderBy('allowence_deduction.allow_id','desc')
            ->get();




        $pdf = new PDF_HF('P','mm','A4');

        $pdf->AddPage();

        $pdf->AliasNbPages('{pages}');

//set font to arial, bold, 14pt

        $pdf->SetFont('Arial','B',12);

//Cell(width , height , text , border , end line , [align] )
        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX( 90);

        $pdf->Cell(40,10,"Allowence & deduction Table",0,0,'C');
        $pdf->Ln(11);

        $pdf->setFillColor(230,230,230);


        $pdf->Ln(15);
        $pdf->Cell(45	,7,'ID',1,0,'C',1);
        $pdf->Cell(45	,7,'Type',1,0,'C',1);
        $pdf->Cell(50	,7,'Name',1,0,'C',1);
        $pdf->Cell(50	,7,'Short Name',1,0,'C',1);
        /*-----------------------  change header -----------------------------*/
        $headrs_name = array(
            array("ID",45),
            array("Type",45),
            array("Name",50),
            array("Short Name",50),
        );
        $pdf->set_header_names($headrs_name);
        /*-------------------------------------------------------*/
        $pdf->SetFontSize(10);

        /*set the table header */
        $pdf->setHeaderTitle(true);

        /*------------------------------*/
        $pdf->SetWidths(array(45,45,50,50));
        $pdf->Ln(7);

        foreach ($query as $val)
        {

            $pdf->SetFont('Arial','B',12);

            $pdf->Row(array($val->allow_id,$val->allow_type_name,$val->allow_name,$val->allow_short_code));

        }


        $pdf->Ln(5);
        $pdf->SetX( 40);

        /*remove the table header */
        $pdf->setHeaderTitle(false);
        /*------------------------------*/


        $pdf->Output();
        exit;


    }







    //----------------------------report allowance-----------------------------//


    public function get_allowence_rpt(Request $request)

    {
        /*dd($request->acadmic_year_print);*/

            $allow = Allowence_deduction::where('allow_id',$request->id)->first();
            $type =$allow->allow_type== 0  ? "Alowence":"Deduction";
            $child = Account::where('Child_id',$allow->account_id)->first()->Child_name;
            $acadimic = @AcademicYear::where('acadimic_id',$request->acadmic_year_print)->first()->year_name ? AcademicYear::where('acadimic_id',$request->acadmic_year_print)->first()->year_name : "";


        $pdf = new PDF_HF('P','mm','A4');

        $pdf->AddPage();

        $pdf->AliasNbPages('{pages}');

//set font to arial, bold, 14pt

        $pdf->SetFont('Arial','B',12);

//Cell(width , height , text , border , end line , [align] )
        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX( 80);

        $pdf->Cell(40,10,"Allowence & deduction NO:",0,0,'C');
        $pdf->Ln(11);

        $pdf->Ln(10);
        $pdf->SetX( 10);
        $pdf->Cell(15,10,'Type',0,0,'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( 60);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,$type,0,0,'L');
        $pdf->SetX(60);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');


        $pdf->Ln(10);
        $pdf->SetX( 10);
        $pdf->SetFont('','B');
        $pdf->Cell(15,10,'Name',0,0,'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( 60);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,$allow->allow_name,0,0,'L');
        $pdf->SetX(60);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');

        $pdf->Ln(10);
        $pdf->SetX( 10);
        $pdf->SetFont('','B');
        $pdf->Cell(15,10,'Short Code',0,0,'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( 60);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,$allow->allow_short_code,0,0,'L');
        $pdf->SetX(60);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');

        $pdf->Ln(10);
        $pdf->SetX( 10);
        $pdf->SetFont('','B');
        $pdf->Cell(15,10,'Account Name',0,0,'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( 60);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,$child,0,0,'L');
        $pdf->SetX(60);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');


        $pdf->Output();
        exit;


    }


    //-----------------------------table scale of allowance-------------------------//
    public function get_allowence_det(Request $request)

    {

        $print_array = array();
        $array =  explode(',', $request->input('print_ids')[0]);

        foreach ($array as $single)
        {
            array_push($print_array, $single);
        }


        $query = DB::table('acadimic_year')
            ->pluck('year_name', 'acadimic_id');




        $pdf = new PDF_HF('P','mm','A4');

        $pdf->AddPage();

        $pdf->AliasNbPages('{pages}');

//set font to arial, bold, 14pt

        $pdf->SetFont('Arial','B',12);

//Cell(width , height , text , border , end line , [align] )
        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX( 90);

        $pdf->Cell(40,10,"Scale of Allowance Table",0,0,'C');
        $pdf->Ln(11);

        $pdf->setFillColor(230,230,230);


        $pdf->Ln(15);
        $pdf->Cell(190	,7,'ID',1,0,'C',1);

        /*-----------------------  change header -----------------------------*/
        $headrs_name = array(
            array("ID",190),

        );
        $pdf->set_header_names($headrs_name);
        /*-------------------------------------------------------*/
        $pdf->SetFontSize(10);

        /*set the table header */
        $pdf->setHeaderTitle(true);

        /*------------------------------*/
        $pdf->SetWidths(array(190));
        $pdf->Ln(7);

        foreach ($query as $val)
        {

            $pdf->SetFont('Arial','B',12);

            $pdf->Row(array($val->year_name));

        }


        $pdf->Ln(5);
        $pdf->SetX( 40);

        /*remove the table header */
        $pdf->setHeaderTitle(false);
        /*------------------------------*/


        $pdf->Output();
        exit;


    }


    public function get_allowence_det_rpt(Request $request)

    {
        /*dd($request->acadmic_year_print);*/

        $allow = Allowence_Details::where('allow_details_id',$request->id)->first();
        $acadimic = @AcademicYear::where('acadimic_id',$request->acadmic_year_print)->first()->year_name ? AcademicYear::where('acadimic_id',$request->acadmic_year_print)->first()->year_name : "";


        $pdf = new PDF_HF('P','mm','A4');

        $pdf->AddPage();

        $pdf->AliasNbPages('{pages}');

//set font to arial, bold, 14pt

        $pdf->SetFont('Arial','B',12);

//Cell(width , height , text , border , end line , [align] )
        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX( 80);

        $pdf->Cell(40,10,"Scale of Allowance:",0,0,'C');
        $pdf->Ln(11);


        $pdf->Ln(10);
        $pdf->SetX( 10);
        $pdf->SetFont('','BU');
        $pdf->Cell(15,10,'Details :',0,0,'L');



        $pdf->Ln(10);
        $pdf->SetX( 10);
        $pdf->SetFont('','B');
        $pdf->Cell(15,10,'Academic Year',0,0,'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(60);
        $pdf->SetFont('','');
        $pdf->Cell(15, 10, $acadimic, 0, 0, 'L');
        $pdf->SetX( 60);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,'',0,0,'L');
        $pdf->SetX(60);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');


        $pdf->setFillColor(230, 230, 230);
        $pdf->SetFont('','B');
        $pdf->Ln(15);
        $pdf->Cell(20	,7,'Type	',1,0,'C',1);
        $pdf->Cell(65	,7,'Allowence or Deduction Name	',1,0,'C',1);
        $pdf->Cell(35	,7,'Beneficiary Type	',1,0,'C',1);
        $pdf->Cell(30	,7,'Salary Scale	',1,0,'C',1);
        $pdf->Cell(20	,7,'Value',1,0,'C',1);
        $pdf->Cell(20	,7,'Currency',1,0,'C',1);
        /*-----------------------  change header -----------------------------*/
        $headrs_name = array(
            array("Type",20),
            array("Allowence or Deduction Name		",65),
            array("Beneficiary Type",35),
            array("Salary Scale	",30),
            array("Value",20),
            array("Currency",20),
        );
        $pdf->set_header_names($headrs_name);
        /*-------------------------------------------------------*/
        $pdf->SetFontSize(10);

        /*set the table header */
        $pdf->setHeaderTitle(true);

        /*------------------------------*/
        $pdf->SetWidths(array(20,65,35,30,20,20));
        $pdf->Ln(7);

        $details_allow =
            //Allowence_Details::where('acadimic_id',$request->acadmic_year_print)/*->where('allow_id',$request->id)*/->get();
        DB::table('allowence_details')
            ->join('allowence_deduction','allowence_details.allow_id','=','allowence_deduction.allow_id')
            ->where('allowence_details.acadimic_id','=',$request->acadmic_year_print)
            ->get();

        foreach ($details_allow as $val)
        {

            if($val->allow_type == 0)
            {
                $type = 'Allowence';
            }
            else{
                $type = 'Deduction';
            }
            //$type = @Allowence_Details::find($val->allow_type_id)->allow_type_name;

            $allow_name=@Allowence_deduction::find($val->allow_id)->allow_name;

            $LE = @Currency::find($val->currency_id)->Currency_name;
            $ben = @BeneficiaryType::find($val->benficinary_id)->Beneficiary_name;
            $sal = @SubSalary::find($val->salary_id)->sub_name;
            $pdf->SetFont('Arial','',12);

            $pdf->Row(array($type,$allow_name,$ben,$sal,$val->value,$LE));

        }

        $pdf->Ln(5);
        $pdf->SetX( 40);

        /*remove the table header */
        $pdf->setHeaderTitle(false);
        /*------------------------------*/


        $pdf->Output();
        exit;


    }








}