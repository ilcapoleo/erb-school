<?php

namespace App\Http\Controllers;

use App\Department;
use App\Employee;
use App\Job;
use App\Nationality;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\PDF_HF;


class nationality_report_controller extends Controller
{


    //-----------------------------table-------------------------//

    public function get_nationality(Request $request)

    {




        $array =  explode(',', $request->input('print_ids')[0]);


        $nationalities= DB::table('nationalities')
            ->orderBy('id','desc')
            ->whereIn('id',$array)
            ->get();





        $pdf = new PDF_HF('P','mm','A4');

        $pdf->AddPage();

        $pdf->AliasNbPages('{pages}');





//set font to arial, bold, 14pt

        $pdf->SetFont('Arial','B',14);

//Cell(width , height , text , border , end line , [align] )
        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX( 90);

        $pdf->Cell(40,10,"Nationality Table",0,0,'C');
        $pdf->Ln(11);

        $pdf->setFillColor(230,230,230);

        $pdf->Ln(15);
        $pdf->Cell(90	,7,'ID',1,0,'C',1);
        $pdf->Cell(100	,7,'Nationality Name',1,0,'C',1);


        /*-----------------------  change header -----------------------------*/
        $headrs_name = array(
            array("ID",90),
            array("Nationality Name",100),

        );
        $pdf->set_header_names($headrs_name);
        /*-------------------------------------------------------*/
        $pdf->SetFontSize(10);

        /*set the table header */
        $pdf->setHeaderTitle(true);

        /*------------------------------*/
        $pdf->SetWidths(array(90,100));
        $pdf->Ln(7);

        foreach ($nationalities as $val)
        {

            $pdf->SetFont('Arial','B',12);

            $pdf->Row(array($val->id,$val->name));

        }


        $pdf->Ln(5);
        $pdf->SetX( 40);

        /*remove the table header */
        $pdf->setHeaderTitle(false);
        /*---------------------------------------*/





        $pdf->Output();
        exit;


    }




    //----------------------------report-----------------------------//

    public function get_nationality_rpt(Request $request)

    {

        $nationality = Nationality::where('id',$request->id)->first();

        $pdf = new PDF_HF('P','mm','A4');

        $pdf->AddPage();

        $pdf->AliasNbPages('{pages}');

//set font to arial, bold, 14pt

        $pdf->SetFont('Arial','B',12);

//Cell(width , height , text , border , end line , [align] )
        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX( 90);

        $pdf->Cell(40,10,"Nationality Report :" .''.$nationality->id,0,0,'C');
        $pdf->Ln(11);



        $pdf->Ln(15);
        $pdf->SetX( 10);
        $pdf->SetFont('','B');

        $pdf->Cell(15,10,'Nationality Name',0,0,'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( 60);
        $pdf->SetFont('','');
        $pdf->Cell(50,8,$nationality->name,0,0,'L');
        $pdf->SetX(60);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');



        $pdf->Output();
        exit;


    }


}