<?php

namespace App\Http\Controllers;


use App\Class_form;
use App\Classes;
use App\CostCenter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\PDF_HF;


class class_report_controller extends Controller
{


    //-----------------------------table-------------------------//

    public function get_class(Request $request)

    {




        $print_array = array();
        $array =  explode(',', $request->input('print_ids')[0]);

        foreach ($array as $single)
        {
            array_push($print_array, $single);
        }
        $query = DB::table('class')
            ->join('cost_centers','class.Costid','=','cost_centers.Cost_id')
//                ->join('employee','departments.manager_id','=','employee.employee_user_id')
            ->wherein('class.classid',$print_array)

            ->orderBy('class.Costid','desc')
            ->get();






        $pdf = new PDF_HF('P','mm','A4');

        $pdf->AddPage();

        $pdf->AliasNbPages('{pages}');





//set font to arial, bold, 14pt

        $pdf->SetFont('Arial','B',14);

//Cell(width , height , text , border , end line , [align] )
        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX( 90);

        $pdf->Cell(40,10,"Class Table",0,0,'C');
        $pdf->Ln(11);

        $pdf->setFillColor(230,230,230);

        $pdf->Ln(15);
        $pdf->Cell(60	,7,'ID',1,0,'C',1);
        $pdf->Cell(60	,7,'CLass ID',1,0,'C',1);
        $pdf->Cell(70	,7,'Class Name',1,0,'C',1);

        /*-----------------------  change header -----------------------------*/
        $headrs_name = array(

            array("ID",60),
            array("CLass ID ",60),
            array("Class Name",70),

        );
        $pdf->set_header_names($headrs_name);
        /*-------------------------------------------------------*/
        $pdf->SetFontSize(10);

        /*set the table header */
        $pdf->setHeaderTitle(true);

        /*------------------------------*/
        $pdf->SetWidths(array(60,60,70));
        $pdf->Ln(7);

        foreach ($query as $val)
        {

            $pdf->SetFont('Arial','B',12);

            $pdf->Row(array($val->classid,$val->ClassName,$val->Costid));

        }


        $pdf->Ln(5);
        $pdf->SetX( 40);

        /*remove the table header */
        $pdf->setHeaderTitle(false);
        /*------------------------------*/

        $pdf->Output();
        exit;


    }




    //----------------------------report-----------------------------//

    public function get_class_rpt(Request $request)

   {

       $depart = Classes::where('classid',$request->id)->first();



       $manager = CostCenter::where('Cost_id',$depart->Costid)->first()->Cost_name;

//       $cost =Class_form::where('class_id',$request->id)->first()->form_name;
       $print_array = array();
       $array =  explode(',', $request->input('print_ids')[0]);

//       foreach ($array as $single)
//       {
//           array_push($print_array, $single);
//       }

        $query = DB::table('class_form')
            ->where('class_id','=',$depart->classid)
            ->get();

       $pdf = new PDF_HF('P','mm','A4');

       $pdf->AddPage();

       $pdf->AliasNbPages('{pages}');

//set font to arial, bold, 14pt

        $pdf->SetFont('Arial','B',12);

//Cell(width , height , text , border , end line , [align] )
        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX( 80);

        $pdf->Cell(40,10,"Class NO:".' '.$depart->classid,0,0,'C');
        $pdf->Ln(11);

        $pdf->Ln(10);
        $pdf->SetX( 10);
        $pdf->Cell(15,10,'Class name',0,0,'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetFont('','');
        $pdf->SetX(60);
        $pdf->Cell(15, 10, $depart->ClassName, 0, 0, 'L');
        $pdf->SetX( 60);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,"",0,0,'L');
        $pdf->SetX(60);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');


        $pdf->Ln(10);
        $pdf->SetX( 10);
        $pdf->SetFont('','B');
        $pdf->Cell(15,10,'Cost Center',0,0,'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(60);
        $pdf->SetFont('','');
        $pdf->Cell(15, 10, $manager, 0, 0, 'L');
        $pdf->SetX( 60);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,"",0,0,'L');
        $pdf->SetX(60);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');


        $pdf->Ln(30);
        $pdf->SetX( 10);
        $pdf->SetFont('','BU');
        $pdf->Cell(15,10,'Form :',0,0,'L');





        $pdf->setFillColor(230, 230, 230);
        $pdf->SetFont('','B');
        $pdf->Ln(15);
        $pdf->Cell(190	,7,'Form Name	',1,0,'C',1);





        /*-----------------------  change header -----------------------------*/
        $headrs_name = array(
            array("Form Name",190),

        );
        $pdf->set_header_names($headrs_name);
        /*-------------------------------------------------------*/
        $pdf->SetFontSize(10);

        /*set the table header */
        $pdf->setHeaderTitle(true);

        /*------------------------------*/
        $pdf->SetWidths(array(190));
        $pdf->Ln(7);

        foreach ($query as $val)
        {

            $pdf->SetFont('Arial','B',12);

            $pdf->Row(array($val->form_name));

        }
        $pdf->Ln(5);
        $pdf->SetX( 40);

        /*remove the table header */
        $pdf->setHeaderTitle(false);
        /*------------------------------*/

        $pdf->Output();
        exit;


    }

}