<?php

namespace App\Http\Controllers;


use App\Journal;
use App\Vendor;
use App\Journal_document;
use App\SalaryScale;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;




class creat_accounting_controller extends Controller
{

    public function creat_journals()
    {
        $master_acc = DB::table('accounts')
            ->where('Level_No','=',3)
            ->orderBy('child_id', 'desc')
            ->pluck('Child_name', 'Child_id');

        $currencies = DB::table('currencies')->orderBy('Currency_name', 'asc')
            ->pluck('Currency_name', 'Currency_id');

        return view('school_pages.journals',compact('master_acc','currencies'));
    }


    public function check_unique(Request $request)
    {
        if ($request->current_po_id != "" && $request->current_po_id) {
            $rules = array(
                'Document_Name' => ['required','unique:journal_documents,Document_Name,'.$request->current_po_id.',Journal_id'],
                'Short_Name' => ['required','unique:journal_documents,document_short_code,'.$request->current_po_id.',Journal_id'],

            );
        }
        else {
            $rules = array(
                'Document_Name' => ['required','unique:journal_documents,Document_Name'],
                'Short_Name' => ['required','unique:journal_documents,document_short_code'],

            );
        }


            $validator = \Illuminate\Support\Facades\Validator::make($request->all(), $rules);

        if ($validator->passes()) {

            return response(['passed' => 1]);
        }

        elseif ($validator->fails()) {

            return response()->json(['error' => $validator->messages()]);

        }
    }


    public function add_new_journal(Request $request)
    {

        $rules = array(

            'journal_name' => ['required','unique:journals,Journal_name,'.$request->current_po_id.',journal_id'],
            'short_name' => ['required','unique:journals,journal_short_code,'.$request->current_po_id.',journal_id'],

        );

        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), $rules);
        if ($validator->passes()) {

            if ($request->current_po_id != "" && $request->current_po_id) {



                $journal = Journal::findOrFail($request->current_po_id);
                $journal->Journal_name = $request->journal_name;
                $journal->journal_short_code = $request->short_name;
                $journal->save();

                $journal_id = $journal->journal_id;


                DB::table('journal_documents')->where('Journal_id', $journal_id)->delete();

                if ($request->table_rows != "") {

                    foreach ($request->table_rows as $row) {

                        $document = new Journal_document();
                        $document->Journal_id = $journal_id;
                        $document->Document_Name = $row['document_name'];
                        $document->master_acc_id = $row['master_acc'];
                        $document->debit_credit = $row['tans_type'];
                        $document->currency_id = $row['currency'];
                        $document->document_short_code = $row['document_short_name'];
                        $document->save();

                    }
                }


                return response([$request, 'current_po_id' => $journal_id]);
            }

            else{
                $journal = new Journal();
                $journal->Journal_name = $request->journal_name;
                $journal->journal_short_code = $request->short_name;
                $journal->save();

                $journal_id = $journal->journal_id;

                if ($request->table_rows != "") {

                    foreach ($request->table_rows as $row) {

                        $document = new Journal_document();
                        $document->Journal_id = $journal_id;
                        $document->Document_Name = $row['document_name'];
                        $document->master_acc_id = $row['master_acc'];
                        $document->debit_credit = $row['tans_type'];
                        $document->currency_id = $row['currency'];
                        $document->document_short_code = $row['document_short_name'];
                        $document->save();

                    }
                }


                return response([$request, 'current_po_id' => $journal_id]);
            }



        }

        elseif ($validator->fails()) {

            return response()->json(['error' => $validator->messages()]);

        }

    }


    /*---------------  delete journal ---------------*/
    public function delete_journal(Request $request)
    {
        $master_po = Journal::findOrFail($request->cur_po_id);

        $master_po->delete();

        DB::table('journal_documents')
            ->where('Journal_id', $request->cur_po_id)
            ->delete();

        return response($request);

    }


    /*------------  edit index journal -------------------*/
    public function edit_index_journals($id)
    {

        $master_acc = DB::table('accounts')
            ->where('level_number','=',3)
            ->orderBy('child_id', 'desc')
            ->pluck('child_name', 'child_id');

        $currencies = DB::table('currencies')->orderBy('Currency_name', 'asc')
            ->pluck('Currency_name', 'Currency_id');

        $journal = DB::table('journals')
            ->where('journal_id','=',$id)
            ->first();

        $journal_documents = DB::table('journal_documents')
            ->join('accounts','journal_documents.master_acc_id','=','accounts.child_id')
            ->join('currencies','journal_documents.currency_id','=','currencies.Currency_id')
            ->where('journal_documents.Journal_id','=',$id)
            ->orderBy('Document_id','asc')
            ->get();

        return view('school_pages.journals',compact('master_acc','currencies','journal','journal_documents'));
    }

    public function journals_table()
    {
        $showed_search_list = array('ID','Journal Name','Journal Short Code');
        $database_search_list = array('journal_id','journal_name','journal_short_code');

        return view('school_pages.show_journals',['showed_search_list' => $showed_search_list,'database_search_list'=>$database_search_list]);
    }
    public function search_journals_table(Request $request)
    {

        $search_colums=array();
        $book=array();

        $string = $request->sudent_data;

        foreach ($string as $value) {

            list($k, $v) = explode('|', $value);
            if($v == "")
            {
                array_push($search_colums,"all");
                array_push($book,$k);
            }
            else{
                array_push($search_colums,$v);
                array_push($book,$k);

            }

        }

        //$search_colums = array('s_name','s_class','s_age');

        $showed_colums = array('ID','Journal Name','Journal Short Code');
        $colums_type = array('input','input','input');
        $table_colums = array('journal_id','Journal_name','journal_short_code');

        //$book = $request->sudent_data;

        if(count($book) >0 && $book[0] != "all")
        {
            $name = DB::table('journals')
//                ->join('cost_centers','salary_scale.cost_id','=','cost_centers.Cost_id')
//                ->join('employee','departments.manager_id','=','employee.employee_user_id')





                ->Where(function ($query) use($book,$search_colums ,$table_colums) {
                    /*for ($i = 0; $i < count($book); $i++){
                        for ($h = 0; $h < count($search_colums); $h++){
                            $query->orwhere($search_colums[$h], 'like',  '%' . $book[$i] .'%');
                        }
                    }*/
                    for ($i = 0; $i < count($book); $i++){
                        if($search_colums[$i] == "all")
                        {
                            $table_colums;
                            for ($h = 0; $h < count($table_colums); $h++){
                                $query->orwhere($table_colums[$h], 'like',  '%' . $book[$i] .'%');
                            }

                        }
                        else
                        {
                            $query->orwhere($search_colums[$i], 'like',  '%' . $book[$i] .'%');
                        }

                    }
                })
                ->orderBy('journal_id','desc')
                ->get();

        }
        else
        {

            $name = DB::table('journals')
//                ->join('cost_centers','salary_scale.cost_id','=','cost_centers.Cost_id')
//                ->join('employee','departments.manager_id','=','employee.employee_user_id')

                ->orderBy('journal_id','desc')
                ->get();
        }
        return response(["name"=>$name ,"colums"=> $table_colums,"showed_colums"=>$showed_colums,"book"=>$book,"searched_colums"=>$search_colums,"colums_type"=>$colums_type]);
    }



    /*------------ vendor page -------------------*/
    public function creat_vendor()
    {
        $account = DB::table('accounts')->orderBy('child_id', 'asc')
            ->pluck('child_name', 'child_id');

        return view('school_pages.vendor',compact('account'));
    }

    /*----------------- vendors ---------------*/

    public function get_this_vendor($id)
    {
        $account = DB::table('accounts')->orderBy('child_id', 'asc')
            ->pluck('child_name', 'child_id');

        $vendors = Vendor::findOrFail($id);




        return view('school_pages.vendor',compact('account','vendors'));

    }


    public function show_vendors()
    {
        $showed_search_list = array('ID','Vendor Name','Account Name','Contact Vendor Person');
        $database_search_list = array('VendorID','VendorName','child_name','ContactVendorPerson');

        return view('school_pages.show_vendors',['showed_search_list' => $showed_search_list,'database_search_list'=>$database_search_list]);
    }

    public function search_vendors_table(Request $request)
    {

        $search_colums=array();
        $book=array();

        $string = $request->sudent_data;

        foreach ($string as $value) {

            list($k, $v) = explode('|', $value);
            if($v == "")
            {
                array_push($search_colums,"all");
                array_push($book,$k);
            }
            else{
                array_push($search_colums,$v);
                array_push($book,$k);

            }

        }

        //$search_colums = array('s_name','s_class','s_age');

        $showed_colums = array('ID','Vendor Name','Account Name','Contact Vendor Person');
        $colums_type = array('input','input','input','input','input','input');
        $table_colums = array('VendorID','VendorName','child_name','ContactVendorPerson');

        //$book = $request->sudent_data;

        if(count($book) >0 && $book[0] != "all")
        {
            $name = DB::table('vendors')




                ->Where(function ($query) use($book,$search_colums ,$table_colums) {
                    /*for ($i = 0; $i < count($book); $i++){
                        for ($h = 0; $h < count($search_colums); $h++){
                            $query->orwhere($search_colums[$h], 'like',  '%' . $book[$i] .'%');
                        }
                    }*/
                    for ($i = 0; $i < count($book); $i++){
                        if($search_colums[$i] == "all")
                        {
                            $table_colums;
                            for ($h = 0; $h < count($table_colums); $h++){
                                $query->orwhere($table_colums[$h], 'like',  '%' . $book[$i] .'%');
                            }

                        }
                        else
                        {
                            $query->orwhere($search_colums[$i], 'like',  '%' . $book[$i] .'%');
                        }

                    }
                })
                ->orderBy('vendors.VendorID','desc')
                ->get();

        }
        else
        {

            $name = DB::table('vendors')
                ->orderBy('vendors.VendorID','desc')
                ->get();
        }
        return response(["name"=>$name ,"colums"=> $table_colums,"showed_colums"=>$showed_colums,"book"=>$book,"searched_colums"=>$search_colums,"colums_type"=>$colums_type]);
    }







    /*------- check------------*/

    public function creat_check()
    {
        return view('school_pages.check');
    }


}






