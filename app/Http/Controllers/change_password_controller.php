<?php

namespace App\Http\Controllers;


use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;




class change_password_controller extends Controller
{
    //
    /* public function ret_cost_page()
     {
         return view('admin_pages.cost_center_page');
     }*/
    public function change_password()
    {
        return view('school_pages.change_password');
    }


    public function change_old_pass(Request $request)
    {

        $rules = array(

            'old_password' => 'required',
            'password' => 'required|confirmed:password_confirmation',
            'password_confirmation' => 'required'


        );


        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), $rules);

        if ($validator->passes())
        {
            $current_password = Auth::User()->password;
            if(Hash::check($request->old_password, $current_password))
            {
                $user_id = Auth::User()->id;
                $obj_user = User::find($user_id);
                $obj_user->password = Hash::make($request->password);;
                $obj_user->save();
                return response()->json(['changed']);
            }

            else
            {
                $error = array('current-password' => 'Please enter correct current password');
                return response()->json(array('error' => $error));
            }

        }

        elseif ($validator->fails())
        {
            return response()->json(['error'=>$validator->messages()]);
        }
    }

}
