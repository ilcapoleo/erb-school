<?php

namespace App\Http\Controllers;


use App\SalaryScale;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Fees_plane;




class fees_plane_controller extends Controller
{









    public function creat_scale_fees()
    {
        $acadimic_year = DB::table('acadimic_year')
            ->pluck('year_name', 'acadimic_id');

        $fees_name = DB::table('fees_head')
            ->Orderby('fees_id', 'desc')
            ->get();

        $classes = DB::table('class')
            ->pluck('ClassName', 'classid');


        $currency = DB::table('currencies')
            ->pluck('Currency_name', 'Currency_id');

        $nationality = DB::table('nationalty_type')
            ->pluck('nationalty_type', 'nationalty_type_id');


        return view('school_pages.scale_fees',compact('acadimic_year','fees_name','classes','currency','nationality'));

    }

    public function get_journal_document(Request $request)
    {

        $item_type = DB::table('journal_documents')
            ->where('Journal_id','=',$request->journal_id)
            ->where('currency_id','=',$request->currency_id)
            ->orderBy('Document_id', 'desc')
            ->get();

        return response()->json($item_type);
    }

    public function add_new_sub_fee(Request $request)
    {
        $rules = array(

            'academic_year' => 'required',
            'fee_name' => 'required',
            'table_rows' => 'required',

        );
        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), $rules);

        if ($validator->passes()) {
            if ($request->row_id != "" && $request->row_id)
            {
                $row = $request->table_rows[0];

                $fee = Fees_plane::findOrFail($request->row_id);
                $fee->Fees_id = $request->fee_name;
                $fee->Year_id = $request->academic_year;
                $fee->class_id = $row['Class'];
                $fee->Currency_id = $row['currency'];
                $fee->value = $row['fee_value'];
                $fee->NO_of_deposits = $row['deposit'];
                $fee->nationalty_type_id = $row['nationality'];
                $fee->journal_doc_id = $row['document_name'];

                $fee->save();

                $fee_id = $fee->enID;

                return response([$request, 'just_add_row_id' => $fee_id]);
            }

            else
            {
                $row = $request->table_rows[0];

                $fee = new Fees_plane();

                $fee->Fees_id = $request->fee_name;
                $fee->Year_id = $request->academic_year;
                $fee->class_id = $row['Class'];
                $fee->Currency_id = $row['currency'];
                $fee->value = $row['fee_value'];
                $fee->NO_of_deposits = $row['deposit'];
                $fee->nationalty_type_id = $row['nationality'];
                $fee->journal_doc_id = $row['document_name'];

                $fee->save();

                $fee_id = $fee->enID;

                return response([$request, 'just_add_row_id' => $fee_id]);
            }

        }

        elseif ($validator->fails()) {

            return response()->json(['error' => $validator->messages()]);

        }
    }


    public function delete_sub_fee(Request $request)
    {
        $rules = array(

            'row' => 'exists:fees_plan,enID',

        );
        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), $rules);
        if ($validator->passes()) {

            $fee = Fees_plane::findOrFail($request->row);
            $fee->delete();

            return response($request);
        }

        elseif ($validator->fails()) {

            return response()->json(['error' => $validator->messages()]);

        }
    }

    public function sub_fees_table(Request $request)
    {
        $fee_table = DB::table('fees_plan')
            ->join('class','fees_plan.class_id','=','class.classid')
            ->join('currencies','fees_plan.Currency_id','=','currencies.Currency_id')
            ->join('nationalty_type','fees_plan.nationalty_type_id','=','nationalty_type.nationalty_type_id')
            ->join('journal_documents','fees_plan.journal_doc_id','=','journal_documents.Document_id')
            ->where('Year_id','=',$request->acadimic_year)
            ->where('Fees_id','=',$request->fee_name)

            ->get(array(

                'fees_plan.enID',
                'fees_plan.class_id',
                'ClassName',
                'currencies.Currency_id',
                'currencies.Currency_name',
                'NO_of_deposits',
                'fees_plan.nationalty_type_id',
                'nationalty_type',
                'fees_plan.journal_doc_id',
                'Document_Name',
                'fees_plan.value',


            ));


        return response()->json(['fees_detailes' => $fee_table]);
    }
}






