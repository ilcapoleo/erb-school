<?php

namespace App\Http\Controllers;



use App\Master_po;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\PDF_HF;


class creat_pdf_controller extends Controller
{
    //
    /* public function ret_cost_page()
     {
         return view('admin_pages.cost_center_page');
     }*/
    public function get_pdf()

    {
       // define('FPDF_FONTPATH',public_path('font'));


        /*$accounts = Chart::all();
        $pdf = PDF::loadView('admin_pages.journal_entry_repory',['accounts'=>$accounts]);
        return $pdf->stream('accounts.pdf');*/

//get invoices data
        $query = Master_po::all();


        $pdf = new PDF_HF('P','mm','A4');



        $pdf->AddPage();
/*
        $pdf->Header = (function() {
            $path = public_path() . '/assets/img/';
            $filepath = $path . 'user.png';


            // Select Arial bold 15
            $this->SetFont('Arial','B',15);
            // Move to the right

            $this->Cell(15,15,"",0,0,'C');

            $this->image($filepath,10,9.7,15);
            // Framed title

            $this->Cell(50,15,'Student Report',0,0,'C');
            // Line break
            $this->Ln(20);
            if ($this->page != 1 && $this->set_header == true)
            {   $this->Ln(5);
                $this->SetFontSize(14);
                $this->Cell(50	,7,'test1 ',1,0,'C');
                $this->Cell(40	,7,'test1',1,0,'C');
                $this->Cell(20	,7,'test1',1,0,'C');
                $this->Cell(40	,7,'test1',1,0,'C');
                $this->Cell(40	,7,'test1 Year',1,1,'C');
            }
        });*/
        $pdf->AliasNbPages('{pages}');

//set font to arial, bold, 14pt

        $pdf->SetFont('Arial','B',14);



//Cell(width , height , text , border , end line , [align] )

        $pdf->Cell(189,50,"",1,0,'C');

        $pdf->Cell(0,3,"",0,0,'C');
        $pdf->Ln(4);

        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX( 90);

        $pdf->Cell(40,10,"Title",0,0,'C');
        $pdf->Ln(11);

        $pdf->SetX( 75);
        $pdf->Cell(15,10,'Form :',0,0,'C');
        $pdf->SetX( -80);
        $pdf->Cell(15,10,'To :',0,0,'C');

        $pdf->Ln(11);
        $pdf->SetX( 30);
        $pdf->Cell(40,10,'Main Code :',0,0,'C');
        $pdf->SetX( 125);
        $pdf->Cell(40,10,'other Code :',0,0,'C');

        $pdf->Ln(11);
        $pdf->SetX( 30);
        $pdf->Cell(40,10,'Main Code :',0,0,'C');
        $pdf->SetX( 125);
        $pdf->Cell(40,10,'other Code :',0,0,'C');

        $pdf->Ln(20);
        $pdf->SetX( 70);
        $pdf->Cell(40,10,'Previews :',0,0,'C');
        $pdf->Cell(30,10,'.................',0,1,'L');

        $pdf->Ln(15);
        $pdf->Cell(50	,7,'First name ',1,0,'C');
        $pdf->Cell(40	,7,'Birth date',1,0,'C');
        $pdf->Cell(20	,7,'Gender',1,0,'C');
        $pdf->Cell(40	,7,'Code',1,0,'C');
        $pdf->Cell(40	,7,'Acadimic Year',1,1,'C');
        /*-----------------------  change header -----------------------------*/
        $headrs_name = array(
            array("First name",50),
            array("Birth date",40),
            array("Gender",20),
            array("Code",40),
            array("Academic Year",40)
        );
        $pdf->set_header_names($headrs_name);
        /*-------------------------------------------------------*/
        $pdf->SetFontSize(10);

        /*set the table header */
        $pdf->setHeaderTitle(true);

        /*------------------------------*/
        $pdf->SetWidths(array(50,40,20,40,40));

        foreach ($query as $val)
        {
            $pdf->SetFont('Arial','B',12);

            $pdf->Row(array($val->supplier_id,$val->beneficiary_user,$val->department_id,$val->order_date,$val->expected_date));


            /*$pdf->Cell(50	,7,$val->First_name,1,0,'C');
            $pdf->Cell(40	,7,$val->Birth_of_date,1,0,'C');
            $pdf->Cell(20	,7,$val->Gender_type,1,0,'C');
            if($pdf->GetStringWidth($val->code)>40)
            {
                $pdf->SetFont('Arial','B',10);
                $pdf->Cell(40	,7,$val->code,1,0,'C');

            }
            else
                {
                    $pdf->SetFont('Arial','B',12);
                    $pdf->Cell(40	,7,$val->code,1,0,'C');
                }


            $pdf->Cell(40	,7,$val->Acadmic_year,1,1,'C');*/
        }


        $pdf->Ln(5);
        $pdf->SetX( 40);

        /*remove the table header */
        $pdf->setHeaderTitle(false);
        /*------------------------------*/
        $pdf->Cell(20	,7,'Total',1,0,'C');
        $pdf->Cell(50	,7,'',1,0,'C');
        $pdf->Cell(50	,7,'',1,1,'C');

        $pdf->SetX( 40);
        $pdf->Cell(20	,7,'Cash',1,0,'C');
        $pdf->Cell(100	,7,'',1,1,'C');

        $pdf->Ln(5);
        $pdf->SetX( 7);
        $cell_h = 20;
        $cell_w = 28;

        $pdf->Cell($cell_w	,$cell_h,'',1,0,'C');
        $pdf->Cell($cell_w	,$cell_h,'',1,0,'C');
        $pdf->Cell($cell_w	,$cell_h,'',1,0,'C');
        $pdf->Cell($cell_w	,$cell_h,'',1,0,'C');
        $pdf->Cell($cell_w	,$cell_h,'',1,0,'C');
        $pdf->Cell($cell_w	,$cell_h,'',1,0,'C');
        $pdf->Cell($cell_w	,$cell_h,'',1,1,'C');


        $pdf->Ln(-20);
        $pdf->SetX( 7);
        $pdf->Cell($cell_w	,5,'',0,0,'C');
        $pdf->Cell($cell_w	,5,'',0,0,'C');



        $pdf->SetFillColor(185,183,188);


        $pdf->Cell($cell_w	,5,'tr1',1,0,'C',true);
        $pdf->Cell($cell_w	,5,'tr1',1,0,'C',true);
        $pdf->Cell($cell_w	,5,'tr1',1,0,'C',true);
        $pdf->Cell($cell_w	,5,'tr1',1,0,'C',true);
        $pdf->Cell($cell_w	,5,'',0,1,'C');

        $pdf->Ln(0);
        $pdf->SetX( 7);
        $pdf->Cell($cell_w	,5,'',0,0,'C');
        $pdf->Cell($cell_w	,5,'',0,0,'C');
        $pdf->Cell($cell_w	,5,'0.00',0,0,'C');
        $pdf->Cell($cell_w	,5,'0.00',0,0,'C');
        $pdf->Cell($cell_w	,5,'0.00',0,0,'C');
        $pdf->Cell($cell_w	,5,'0.00',0,0,'C');
        $pdf->Cell($cell_w	,5,'',0,1,'C');

        $pdf->Ln(0);
        $pdf->SetX( 7);
        $pdf->Cell($cell_w	,5,'',0,0,'C');
        $pdf->Cell($cell_w	,5,'',0,0,'C');

        $pdf->SetFillColor(185,183,188);

        $pdf->Cell($cell_w	,5,'tr3',1,0,'C' ,true);
        $pdf->Cell($cell_w	,5,'tr3',1,0,'C',true);
        $pdf->Cell($cell_w	,5,'tr3',1,0,'C',true);
        $pdf->Cell($cell_w	,5,'tr3',1,0,'C',true);
        $pdf->Cell($cell_w	,5,'',0,1,'C');


        $pdf->Ln(0);
        $pdf->SetX( 7);
        $pdf->Cell($cell_w	,5,'',0,0,'C');
        $pdf->Cell($cell_w	,5,'',0,0,'C');
        $pdf->Cell($cell_w	,5,'0.00',0,0,'C');
        $pdf->Cell($cell_w	,5,'0.00',0,0,'C');
        $pdf->Cell($cell_w	,5,'0.00',0,0,'C');
        $pdf->Cell($cell_w	,5,'0.00',0,0,'C');
        $pdf->Cell($cell_w	,5,'',0,1,'C');


        $pdf->Ln(0);
        $pdf->SetX( 7);
        $cell_h = 14;
        $pdf->Cell($cell_w	,$cell_h,'',1,0,'C');
        $pdf->Cell($cell_w	,$cell_h,'',1,0,'C');
        $pdf->Cell($cell_w	,$cell_h,'',1,0,'C');
        $pdf->Cell($cell_w	,$cell_h,'',1,0,'C');
        $pdf->Cell($cell_w	,$cell_h,'',1,0,'C');
        $pdf->Cell($cell_w	,$cell_h,'',1,0,'C');
        $pdf->Cell($cell_w	,$cell_h,'',1,1,'C');


        $pdf->Ln(-14);
        $pdf->SetX( 7);
        $pdf->SetFillColor(185,183,188);

        $pdf->Cell($cell_w	,7,'tr1',1,0,'C',true);
        $pdf->Cell($cell_w	,7,'tr1',1,0,'C',true);
        $pdf->Cell($cell_w	,7,'tr1',1,0,'C',true);
        $pdf->Cell($cell_w	,7,'tr1',1,0,'C',true);
        $pdf->Cell($cell_w	,7,'tr1',1,0,'C',true);
        $pdf->Cell($cell_w	,7,'tr1',1,0,'C',true);
        $pdf->Cell($cell_w	,7,'tr1',1,1,'C',true);



        $pdf->Ln(0);
        $pdf->SetX( 7);
        $pdf->SetFillColor(185,183,188);

        $pdf->Cell($cell_w	,7,'0.00',1,0,'C',true);
        $pdf->Cell($cell_w	,7,'0.00',1,0,'C',true);
        $pdf->Cell($cell_w	,7,'0.00',1,0,'C',true);
        $pdf->Cell($cell_w	,7,'0.00',1,0,'C',true);
        $pdf->Cell($cell_w	,7,'0.00',1,0,'C',true);
        $pdf->Cell($cell_w	,7,'0.00',1,0,'C',true);
        $pdf->Cell($cell_w	,7,'0.00',1,1,'C',true);

        $pdf->Ln(9);
        $pdf->SetX( 30);
        $pdf->Cell($cell_w	,7,'sign 1',0,0,'C');


        $pdf->SetX( 150);
        $pdf->Cell($cell_w	,7,'sign 2',0,1,'C');

        $cell_w = 50;
        $pdf->Ln(2);
        $pdf->SetX( 19);
        $pdf->Cell($cell_w	,7,'...............................',0,0,'C');


        $pdf->SetX( 139);
        $pdf->Cell($cell_w	,7,'...............................',0,1,'C');

        $pdf->Output();
        exit;


    }



}