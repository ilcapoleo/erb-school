<?php

namespace App\Http\Controllers;

use App\cost_centers;
use App\Modules;
use Illuminate\Http\Request;
use App\Class_data;

class module_controller extends Controller
{

    public function get_module_page()
    {
        return view('custom.create_module');
    }

    public function add_new_module(Request $request)
    {
        $module = Modules::create(['name' => $request->module_name,'link' => $request->module_link]);

        return response($module);

    }



}
