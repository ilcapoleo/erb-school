<?php

namespace App\Http\Controllers;

use App\CostCenter;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CostCentersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $showed_search_list = array('ID','Cost Center Name','Cost Center Code');


        $cost_centers = CostCenter::orderby('Cost_id','desc')->get();


        $database_search_list = array(1,2,3);

        return view('school_pages.costcenters.index',['showed_search_list' => $showed_search_list,'database_search_list'=>$database_search_list],compact('cost_centers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('school_pages.costcenters.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {



        $rules = [
            'cost_center_n' => 'required',
            'cost_center_c' => 'required',


        ];
        $validator = Validator::make($request->all(), $rules);
        $validator->SetAttributeNames([
            'cost_center_n' => 'Vendor Code',
            'cost_center_c' => 'English Vendor Name',



        ]);
        if ($validator->fails()) {

            session()->flash('error', 'failed');

            return redirect()->back()->withInput()->withErrors($validator);

        } else {

                $cost = new CostCenter();

                $cost->Cost_name = $request->cost_center_n;
                $cost->Cost_center_code = $request->cost_center_c;
                $cost->save();
                session()->flash('success', 'created');
                return redirect('cost_centers/'.$cost->Cost_id.'/edit');

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cost = CostCenter::findorfail($id);

        return view('school_pages.costcenters.edit',compact('cost'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $rules = [
            'vendor_code' => 'required',
            'vendor_name' => 'required',



        ];
        $validator = Validator::make($request->all(), $rules);
        $validator->SetAttributeNames([
            'vendor_code' => 'Vendor Code',
            'vendor_name' => 'English Vendor Name',



        ]);
        if ($validator->fails()) {

            session()->flash('error', 'failed');

//            dd(back()->withInput()->withErrors($validator));
            return back()->withInput()->withErrors($validator);

        } else {
            try {
                DB::beginTransaction();

                $cost = new CostCenter();
                $cost->VendorCode =$request->vendor_code;
                $cost->Cost_center_code =$request->arabic_vendor_name;

                $cost->save();

                DB::commit();
                session()->flash('success', 'created');
            }catch (Exception $ex){
                DB::rollBack();
                session()->flash('error', 'failed');
                return back();
            }

            return redirect('cost_centers/'.$cost->Cost_id.'/edit');


        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cost = CostCenter::find($id);
        $cost->delete();
        session()->flash('success', trans('admin.deleted'));
        return redirect('/cost_centers');
    }




    //-----------------------------table-------------------------//

    public function get_cost_center(Request $request)

    {

        $print_array = array();
        $array =  explode(',', $request->input('print_ids')[0]);

        foreach ($array as $single)
        {
            array_push($print_array, $single);
        }


        $query = DB::table('cost_centers')
            ->get();


        $pdf = new PDF_HF('P','mm','A4');

        $pdf->AddPage();

        $pdf->AliasNbPages('{pages}');

//set font to arial, bold, 14pt

        $pdf->SetFont('Arial','B',12);

//Cell(width , height , text , border , end line , [align] )
        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX( 90);

        $pdf->Cell(40,10,"Cost Center Table",0,0,'C');
        $pdf->Ln(11);

        $pdf->setFillColor(230,230,230);


        $pdf->Ln(15);
        $pdf->Cell(60	,7,'ID',1,0,'C',1);
        $pdf->Cell(60	,7,'Cost Center Name',1,0,'C',1);
        $pdf->Cell(70	,7,'Cost Center Code',1,0,'C',1);
        /*-----------------------  change header -----------------------------*/
        $headrs_name = array(
            array("ID",60),
            array("Cost Center Name",60),
            array("Cost Center Code",70),


        );
        $pdf->set_header_names($headrs_name);
        /*-------------------------------------------------------*/
        $pdf->SetFontSize(10);

        /*set the table header */
        $pdf->setHeaderTitle(true);

        /*------------------------------*/
        $pdf->SetWidths(array(60,60,70));
        $pdf->Ln(7);

        foreach ($query as $val)
        {

            $pdf->SetFont('Arial','B',12);

            $pdf->Row(array($val->Cost_id,$val->Cost_name,$val->Cost_center_code));

        }


        $pdf->Ln(5);
        $pdf->SetX( 40);

        /*remove the table header */
        $pdf->setHeaderTitle(false);
        /*------------------------------*/

        $pdf->Output();
        exit;


    }




    //----------------------------report-----------------------------//

    public function get_cost_center_rpt($id)

    {
        $cost=CostCenter::find($id);


        $pdf = new PDF_HF('P','mm','A4');

        $pdf->AddPage();

        $pdf->AliasNbPages('{pages}');

//set font to arial, bold, 14pt

        $pdf->SetFont('Arial','B',12);

//Cell(width , height , text , border , end line , [align] )
        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX( 80);

        $pdf->Cell(40,10,"Cost Center NO:",0,0,'C');
        $pdf->Ln(11);

        $pdf->Ln(10);
        $pdf->SetX( 10);
        $pdf->Cell(15,10,'Cost Center Name',0,0,'L');
        $pdf->SetX(60);
        $pdf->SetFont('','');
        $pdf->Cell(15, 10, @$cost->Cost_name, 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( 60);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,"",0,0,'L');
        $pdf->SetX(60);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');


        $pdf->Ln(10);
        $pdf->SetX( 10);
        $pdf->SetFont('','B');
        $pdf->Cell(15,10,'Vendor Name',0,0,'L');
        $pdf->SetX(60);
        $pdf->SetFont('','');
        $pdf->Cell(15, 10, @$cost->Cost_center_code	, 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( 60);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,"",0,0,'L');
        $pdf->SetX(60);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');




        $pdf->Output();
        exit;


    }







}
