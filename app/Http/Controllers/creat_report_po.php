<?php

namespace App\Http\Controllers;



use App\Master_po;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\PDF_HF;


class creat_report_po extends Controller
{
    //
    /* public function ret_cost_page()
     {
         return view('admin_pages.cost_center_page');
     }*/
    public function get_po1(Request $request)

    {
        // define('FPDF_FONTPATH',public_path('font'));



        /*$accounts = Chart::all();
        $pdf = PDF::loadView('admin_pages.journal_entry_repory',['accounts'=>$accounts]);
        return $pdf->stream('accounts.pdf');*/

//get invoices data
         //$request->input('print_ids');
        $print_array = array();
        $array =  explode(',', $request->input('print_ids')[0]);

        foreach ($array as $single)
        {
            array_push($print_array, $single);
        }
        /*dd($print_array);*/

        /*dd($print_array);*/
        $query = DB::table('po_master')
            ->join('vendors','po_master.supplier_id','=','vendors.VendorID')
            ->join('employee','po_master.creator_user_id','=','employee.employee_id')
            ->join('document_status','po_master.po_status_id','=','document_status.status_id')
            ->whereIn('po_master.po_id',$print_array)
            ->orderBy('po_master.po_id','desc')
            ->get();





        $pdf = new PDF_HF('P','mm','A4');



        $pdf->AddPage();
        /*
                $pdf->Header = (function() {
                    $path = public_path() . '/assets/img/';
                    $filepath = $path . 'user.png';


                    // Select Arial bold 15
                    $this->SetFont('Arial','B',15);
                    // Move to the right

                    $this->Cell(15,15,"",0,0,'C');

                    $this->image($filepath,10,9.7,15);
                    // Framed title

                    $this->Cell(50,15,'Student Report',0,0,'C');
                    // Line break
                    $this->Ln(20);
                    if ($this->page != 1 && $this->set_header == true)
                    {   $this->Ln(5);
                        $this->SetFontSize(14);
                        $this->Cell(50	,7,'test1 ',1,0,'C');
                        $this->Cell(40	,7,'test1',1,0,'C');
                        $this->Cell(20	,7,'test1',1,0,'C');
                        $this->Cell(40	,7,'test1',1,0,'C');
                        $this->Cell(40	,7,'test1 Year',1,1,'C');
                    }
                });*/
        $pdf->AliasNbPages('{pages}');

//set font to arial, bold, 14pt

        $pdf->SetFont('Arial','B',14);



//Cell(width , height , text , border , end line , [align] )
        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX( 90);

        $pdf->Cell(40,10,"Purchase Order Table",0,0,'C');
        $pdf->Ln(11);

        $pdf->setFillColor(230,230,230);

        $pdf->Ln(15);
        $pdf->Cell(40	,7,'Po Number ',1,0,'C',1);
        $pdf->Cell(40	,7,'Order Date',1,0,'C',1);
        $pdf->Cell(30	,7,'Supplier',1,0,'C',1);
        $pdf->Cell(40	,7,'Creator Name',1,0,'C',1);
        $pdf->Cell(40	,7,'Status',1,1,'C',1);
        /*-----------------------  change header -----------------------------*/
        $headrs_name = array(
            array("First name",40),
            array("Order Date",40),
            array("Supplier",30),
            array("Creator Name",40),
            array("Status",40)
        );
        $pdf->set_header_names($headrs_name);
        /*-------------------------------------------------------*/
        $pdf->SetFontSize(10);

        /*set the table header */
        $pdf->setHeaderTitle(true);

        /*------------------------------*/
        $pdf->SetWidths(array(40,40,30,40,40));

        foreach ($query as $val)
        {
            $pdf->SetFont('Arial','B',12);

            $pdf->Row(array($val->po_seq,$val->order_date,$val->VendorName,$val->employee_name,$val->status_short_name));


            /*$pdf->Cell(50	,7,$val->First_name,1,0,'C');
            $pdf->Cell(40	,7,$val->Birth_of_date,1,0,'C');
            $pdf->Cell(20	,7,$val->Gender_type,1,0,'C');
            if($pdf->GetStringWidth($val->code)>40)
            {
                $pdf->SetFont('Arial','B',10);
                $pdf->Cell(40	,7,$val->code,1,0,'C');

            }
            else
                {
                    $pdf->SetFont('Arial','B',12);
                    $pdf->Cell(40	,7,$val->code,1,0,'C');
                }


            $pdf->Cell(40	,7,$val->Acadmic_year,1,1,'C');*/
        }


        $pdf->Ln(5);
        $pdf->SetX( 40);

        /*remove the table header */
        $pdf->setHeaderTitle(false);
        /*------------------------------*/








        $pdf->Output();
        exit;


    }



}