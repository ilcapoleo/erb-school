<?php

namespace App\Http\Controllers;



use App\po;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\PDF_HF;


class creat_po_invoice extends Controller

{
    public $ss;
    protected $col = 0; // Current column
    protected $y0;      // Ordinate of column start
    //
    /* public function ret_cost_page()
     {
         return view('admin_pages.cost_center_page');
     }*/
    public function get_po()

    {
       // define('FPDF_FONTPATH',public_path('font'));


        /*$accounts = Chart::all();
        $pdf = PDF::loadView('admin_pages.journal_entry_repory',['accounts'=>$accounts]);
        return $pdf->stream('accounts.pdf');*/

//get invoices data
        $query = po::all();

        $pdf = new PDF_HF('P','mm','A4');



        $pdf->AddPage();
/*
        $pdf->Header = (function() {
            $path = public_path() . '/assets/img/';
            $filepath = $path . 'user.png';


            // Select Arial bold 15
            $this->SetFont('Arial','B',15);
            // Move to the right

            $this->Cell(15,15,"",0,0,'C');

            $this->image($filepath,10,9.7,15);
            // Framed title

            $this->Cell(50,15,'Student Report',0,0,'C');
            // Line break
            $this->Ln(20);
            if ($this->page != 1 && $this->set_header == true)
            {   $this->Ln(5);
                $this->SetFontSize(14);
                $this->Cell(50	,7,'test1 ',1,0,'C');
                $this->Cell(40	,7,'test1',1,0,'C');
                $this->Cell(20	,7,'test1',1,0,'C');
                $this->Cell(40	,7,'test1',1,0,'C');
                $this->Cell(40	,7,'test1 Year',1,1,'C');
            }
        });*/



        $query2 = DB::table('cost_centers')
             ->select('cost_centers.*')
             ->get();


        $pdf->AliasNbPages('{pages}');

//set font to arial, bold, 14pt

        $pdf->SetFont('Arial','B',8);



//Cell(width , height , text , border , end line , [align] )


        $pdf->Cell(0,3,"",0,0,'C');
        $pdf->Ln(4);

        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX( 90);
        $pdf->SetFont('','UB');
        $pdf->SetFontSize(10);
        $pdf->Cell(40,10,"Expenditures Approval",0,0,'C');
        $pdf->Ln(11);
        $pdf->SetFont('','B');
        $pdf->SetX( 10);
        $pdf->SetFont('','U');
        $pdf->SetFontSize(8);
        $pdf->Cell(15,10,'Date included in the section below till the red line ',0,0,'L');

        $pdf->SetX( -90);
        $pdf->SetFont('','U');
        $pdf->Cell(15,10,'Claimant cost center',0,0,'C');
        $pdf->SetX( -60);
        $pdf->SetFont('','');

        $pdf->Cell(30,5,'      /      /    ',1,0,'L');

        $pdf->Ln(3);
        $pdf->SetX( 10);
        $pdf->SetFont('','');

        $pdf->Cell(15,10,'belongs to the claiment ',0,0,'L');



        $pdf->Ln(5);
        $pdf->SetX( 10);
        $pdf->SetFont('','U');

        $pdf->Cell(82,10,'These / This expenditure/s belong/s to the following cost centers',0,0,'C');


        /*-----------------------  change header -----------------------------*/
        $headrs_name = array(
            array("doc type",90),
            array("supplier id",50),
            array("supplier id",50),
        );
        $pdf->set_header_names($headrs_name);
        /*-------------------------------------------------------*/
        $pdf->SetFontSize(8);
        $pdf->SetFont('','');

        /*set the table header */
        $pdf->setHeaderTitle(true);

        /*------------------------------*/
        $pdf->SetWidths(array(10,20,30));

        $pdf->Ln(6);





        $xa= $pdf->GetX();
        $b = $pdf->GetY()+3 ;
        $c = $pdf->GetY()+3 ;
        $d = $pdf->GetY()+3 ;
        $e = $pdf->GetY()+3 ;

        $ss=0;



        foreach ($query2 as $val)
        {



          $ss = $ss+1;
          if ($ss <= 7){

              $pdf->SetY($b);
              $CONTENT=$val->Cost_center_code.'  -  '.$val->Cost_name;

              $pdf->MultiCell(50 ,10, $CONTENT);

              $pdf->SetXY( 50,$b +4);
              $pdf->Cell(2,2,'',1,0,'R');
              $b=($b+3);
              $pdf->Ln(5);

          }
            elseif($ss >= 7 & $ss <= 14) {


                $pdf->SetXY( 60,$c);
                $CONTENT=$val->Cost_center_code.'  -  '.$val->Cost_name;
                $pdf->MultiCell(100,10,$CONTENT);

                $pdf->SetXY( 100,$c +4);

                $pdf->Cell(2,2,'',1,0,'R');
                $c=($c+3);

                $pdf->Ln(5);

            }

          elseif($ss >= 14 & $ss <= 21){


              $pdf->SetXY(110, $d);
              $CONTENT=$val->Cost_center_code.'  -  '.$val->Cost_name;
              $pdf->MultiCell(150,10,$CONTENT);

              $pdf->SetXY( 150,$d +4);

              $pdf->Cell(2,2,'',1,0,'R');
              $d=($d+3);

              $pdf->Ln(5);

          }
          else{


              $pdf->SetXY(160, $e);
              $CONTENT=$val->Cost_center_code.'  -  '.$val->Cost_name;
              $pdf->MultiCell(200,10,$CONTENT);

              $pdf->SetXY( 190,$e +4);

              $pdf->Cell(2,2,'',1,0,'R');
              $e=($e+3);

              $pdf->Ln(5);

          }








        }





        /*remove the table header */
        $y = $pdf->GetY() +20;
        $pdf->SetY( $y);
        $pdf->Cell(190,10,'Description of work :',0,0,'L');

        $pdf->Ln(8);
        $pdf->SetX(10);
        $pdf->Cell(190,20,'',1,0,'L');

        $pdf->Ln(22);
        $pdf->SetX( 10);
        $pdf->Cell(15,10,'Carried out by/supplier: ',0,0,'L');
        $pdf->SetX( 90);
        $pdf->Cell(30,5,'',1,0,'L');



        $pdf->Ln(6);
        $pdf->SetX( 10);
        $pdf->Cell(15,10,'Amount including any TAX: ',0,0,'L');
        $pdf->SetX( 90);
        $pdf->SetFont('','');

        $pdf->Cell(30,5,'',1,0,'L');

        $pdf->SetX( -90);
        $pdf->SetFont('','');

        $pdf->Cell(15,10,'Date Required :',0,0,'L');
        $pdf->SetX( -60);
        $pdf->Cell(30,5,'      /      /    ',1,0,'L');

        $pdf->Ln(6);
        $pdf->SetX( 10);
        $pdf->SetFont('','U');

        $pdf->Cell(15,10,'Requested by: Name ',0,0,'L');
        $pdf->SetX( 90);
        $pdf->Cell(30,5,'',1,0,'L');

        $pdf->SetX( -90);
        $pdf->SetFont('','');

        $pdf->Cell(15,10,'Date:',0,0,'L');
        $pdf->SetX( -60);
        $pdf->Cell(30,5,'      /      /    ',1,0,'L');


        $pdf->Ln(6);
        $pdf->SetX( 40);
        $pdf->SetFont('','');
        $pdf->Cell(15,10,'Climant department',0,0,'L');
        $pdf->SetX( 90);
        $pdf->Cell(30,5,'',1,0,'L');

        $pdf->Ln(6);
        $pdf->SetX( 40);
        $pdf->Cell(15,10,'Signature',0,0,'L');
        $pdf->SetX( 90);
        $pdf->Cell(30,5,'',1,0,'L');

        $pdf->Ln(6);
        $pdf->SetX( 10);
        $pdf->SetFont('','U');
        $pdf->Cell(15,10,'Authorized by: Name ',0,0,'L');
        $pdf->SetX( 90);
        $pdf->Cell(30,5,'',1,0,'L');

        $pdf->Ln(6);
        $pdf->SetX( 40);
        $pdf->SetFont('','');

        $pdf->Cell(15,10,'Signature',0,0,'L');
        $pdf->SetX( 90);
        $pdf->Cell(30,5,'',1,0,'L');


        $pdf->Ln(6);
        $pdf->SetX( 10);
        $pdf->SetFont('','U');
        $pdf->Cell(15,10,'Goods/Services have been recieved as ordered by:',0,0,'L');
        $pdf->SetX( 90);
        $pdf->Cell(30,5,'',1,0,'L');

        $pdf->Ln(6);
        $pdf->SetX( 40);
        $pdf->SetFont('','');
        $pdf->Cell(15,10,'Name',0,0,'L');
        $pdf->SetX( 90);
        $pdf->Cell(30,5,'',1,0,'L');
        $pdf->SetX( -90);
        $pdf->SetFont('','');

        $pdf->Cell(15,10,'Date Recieved:',0,0,'L');
        $pdf->SetX( -60);
        $pdf->Cell(30,5,'      /      /    ',1,0,'L');



        $pdf->Ln(6);
        $pdf->SetX( 40);
        $pdf->Cell(15,10,'Signature',0,0,'L');
        $pdf->SetX( 90);
        $pdf->Cell(30,5,'',1,0,'L');
        $s = $pdf->GetY() + 10;
        $pdf -> Line(10, $s,200, $s);

        $pdf->Ln(8);
        $pdf->SetX( 90);
        $pdf->SetFont('','UB');
        $pdf->SetFontSize(10);
        $pdf->Cell(40,10,"For Use of Financial department",0,0,'C');
        $pdf->Ln(11);


        $pdf->Ln(3);
        $pdf->SetX( 50);
        $pdf->SetFont('','');
        $pdf->SetFontSize(8);
        $pdf->Cell(15,10,'Correct tax invoice ',0,0,'L');
        $pdf->Ln(3);
        $pdf->SetX( 50);
        $pdf->Cell(15,10,'recieved ',0,0,'L');
        $pdf->Ln(3);
        $pdf->SetX( 50);
        $pdf->Cell(15,10,'Amount is correct',0,0,'L');
        $pdf->SetX( 90);
        $pdf->Cell(30,5,'',0,0,'L');

        $pdf->SetX( -90);
        $pdf->Cell(15,10,'Invoice Date :',0,0,'L');
        $pdf->SetX( -60);
        $pdf->Cell(30,5,'      /      /    ',1,0,'L');


        $pdf->Ln(10);
        $pdf->SetX( 10);
        $pdf->SetFont('','U');
        $pdf->Cell(15,10,'Budget to be charged to A/C No: ',0,0,'L');
        $pdf->SetX( 90);
        $pdf->Cell(30,5,'',1,0,'L');

        $pdf->SetX( -90);
        $pdf->SetFont('','');
        $pdf->Cell(15,10,'Balance remaining:',0,0,'L');
        $pdf->SetX( -60);
        $pdf->Cell(30,5,' EGP      £   ',1,0,'L');


        $pdf->Ln(6);
        $pdf->SetX( 40);
        $pdf->SetFont('','');
        $pdf->Cell(15,10,'Cost Center No',0,0,'L');
        $pdf->SetX( 90);
        $pdf->Cell(30,5,'',1,0,'L');



        $pdf->Ln(6);
        $pdf->SetX( 10);
        $pdf->SetFont('','U');
        $pdf->Cell(15,10,'Accountant:',0,0,'L');
        $pdf->SetX( 90);
        $pdf->Cell(30,5,'',1,0,'L');
        $pdf->SetX( -90);
        $pdf->SetFont('','');
        $pdf->Cell(15,10,'Date :',0,0,'L');
        $pdf->SetX( -60);
        $pdf->Cell(30,5,'      /      /    ',1,0,'L');

        $pdf->Ln(6);
        $pdf->SetX( 40);
        $pdf->SetFont('','');
        $pdf->Cell(15,10,'Signature',0,0,'L');
        $pdf->SetX( 90);
        $pdf->Cell(30,5,'',1,0,'L');

        $pdf->Ln(6);
        $pdf->SetX( 10);
        $pdf->SetFont('','U');
        $pdf->Cell(15,10,'Confirmed by Head of finance:',0,0,'L');
        $pdf->SetX( 90);
        $pdf->Cell(30,5,'',1,0,'L');
        $pdf->SetX( -90);
        $pdf->SetFont('','');
        $pdf->Cell(15,10,'Date :',0,0,'L');
        $pdf->SetX( -60);
        $pdf->Cell(30,5,'      /      /    ',1,0,'L');

        $pdf->Ln(6);
        $pdf->SetX( 40);
        $pdf->SetFont('','');
        $pdf->Cell(15,10,'Signature',0,0,'L');
        $pdf->SetX( 90);
        $pdf->Cell(30,5,'',1,0,'L');


        $pdf->Ln(6);
        $pdf->SetX( 10);
        $pdf->SetFont('','U');
        $pdf->Cell(15,10,'Authorized by School Peincipal:',0,0,'L');
        $pdf->SetX( 90);
        $pdf->Cell(30,5,'',1,0,'L');
        $pdf->SetX( -90);
        $pdf->SetFont('','');
        $pdf->Cell(15,10,'Date :',0,0,'L');
        $pdf->SetX( -60);
        $pdf->Cell(30,5,'      /      /    ',1,0,'L');


        $pdf->Ln(6);
        $pdf->SetX( 40);
        $pdf->SetFont('','');
        $pdf->Cell(15,10,'Signature',0,0,'L');
        $pdf->SetX( 90);
        $pdf->Cell(30,5,'',1,0,'L');

        $pdf -> Line(10, 400,200, 400);




        $pdf->Output();
        exit;


    }



}