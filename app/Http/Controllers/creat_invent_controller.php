<?php

namespace App\Http\Controllers;



use App\inventory_master;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\PDF_HF;


class creat_invent_controller extends Controller
{
    //
    /* public function ret_cost_page()
     {
         return view('admin_pages.cost_center_page');
     }*/
    public function get_invent()

    {
       // define('FPDF_FONTPATH',public_path('font'));


        /*$accounts = Chart::all();
        $pdf = PDF::loadView('admin_pages.journal_entry_repory',['accounts'=>$accounts]);
        return $pdf->stream('accounts.pdf');*/

//get invoices data
       /* $query = inventory_master::all();*/
        $query2 = DB::table('inventory_master')
             ->join('vendors', 'inventory_master.supplier_id', '=','vendors.VendorID')
             ->join ('inventory_details','inventory_master.doc_id', '=', 'inventory_details.doc_id')
             ->join('products','inventory_details.pro_id','=' ,'products.pro_id')
            ->select('inventory_details.pro_id', 'products.pro_name' ,'inventory_details.qut')
            ->where('inventory_master.doc_id', '=', 40)
            ->get();

        $query3 = DB::table('inventory_master')
            ->join ('vendors','inventory_master.supplier_id', '=', 'vendors.VendorID')
            ->select( 'inventory_master.*','vendors.VendorName')
            ->where('inventory_master.doc_id', '=', 40)
            ->first();

        $query4 = DB::table('inventory_master')
            ->join ('inventory_details','inventory_master.doc_id', '=', 'inventory_details.doc_id')
            ->select(DB::raw('SUM(qut) as mileage'))
            ->where('inventory_master.doc_id', '=', 40)
            ->first();

        $pdf = new PDF_HF('P','mm','A4');

        $pdf->AddPage();
/*
        $pdf->Header = (function() {
            $path = public_path() . '/assets/img/';
            $filepath = $path . 'user.png';


            // Select Arial bold 15
            $this->SetFont('Arial','B',15);
            // Move to the right

            $this->Cell(15,15,"",0,0,'C');

            $this->image($filepath,10,9.7,15);
            // Framed title

            $this->Cell(50,15,'Student Report',0,0,'C');
            // Line break
            $this->Ln(20);
            if ($this->page != 1 && $this->set_header == true)
            {   $this->Ln(5);
                $this->SetFontSize(14);
                $this->Cell(50	,7,'test1 ',1,0,'C');
                $this->Cell(40	,7,'test1',1,0,'C');
                $this->Cell(20	,7,'test1',1,0,'C');
                $this->Cell(40	,7,'test1',1,0,'C');
                $this->Cell(40	,7,'test1 Year',1,1,'C');
            }
        });*/
        $pdf->AliasNbPages('{pages}');

//set font to arial, bold, 14pt

        $pdf->SetFont('Arial','B',14);



//Cell(width , height , text , border , end line , [align] )

        $pdf->Cell(189,50,"",0,0,'C');

        $pdf->Cell(0,3,"",0,0,'C');
        $pdf->Ln(4);

        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX( 90);

        $pdf->Cell(40,10,"Inventory Id",0,0,'C');

            $pdf->Ln(11);

        $pdf->Ln(11);




        $pdf->Cell(40,10,'Date :',0,0,'L');
        $pdf->Cell(40,10,$query3->doc_date,0,0,'L');

        $pdf->Ln(11);

        $pdf->Cell(40,10,'supplier:',0,0,'L');
        $pdf->Cell(40,10,$query3->VendorName,0,0,'C');

        $pdf->Ln(11);
        $pdf->Cell(40,10,'Description:',0,0,'L');
        $pdf->Ln(11);
        $pdf->Cell(189,50,$query3->notes,1,0,'C');


        $pdf->Ln(60);
        $pdf->Cell( 60	,7,'Pro_ID',1,0,'C');
        $pdf->Cell(60	,7,'Pro_name',1,0,'C');
        $pdf->Cell(70	,7,'qut',1,0,'C');

        /*-----------------------  change header -----------------------------*/
        $headrs_name = array(
            array("doc type",90),
            array("supplier id",50),
            array("supplier id",50),
        );
        $pdf->set_header_names($headrs_name);
        /*-------------------------------------------------------*/
        $pdf->SetFontSize(5);

        /*set the table header */
        $pdf->setHeaderTitle(true);

        /*------------------------------*/
        $pdf->SetWidths(array(60,60,70));
        $pdf->Ln(7);
        foreach ($query2 as $val)
        {

            $pdf->SetFont('Arial','B',12);
             $pdf->Row(array($val->pro_id,$val->pro_name,$val->qut));



        }


        $pdf->Ln(10);
        $pdf->SetX( 40);

        /*remove the table header */
        $pdf->setHeaderTitle(false);
        /*------------------------------*/
        $pdf->Cell(20	,7,'Total',1,0,'C');
        $pdf->Cell(50	,7,$query4->mileage,1,0,'C');
        $pdf->Cell(50	,7,'',1,1,'C');

        $pdf->SetX( 40);
        $pdf->Cell(20	,7,'Cash',1,0,'C');
        $pdf->Cell(100	,7,'',1,1,'C');

        $pdf->Ln(5);
        $pdf->SetX( 7);
        $cell_h = 20;
        $cell_w = 28;

        $pdf->Cell($cell_w	,$cell_h,'',1,0,'C');
        $pdf->Cell($cell_w	,$cell_h,'',1,0,'C');
        $pdf->Cell($cell_w	,$cell_h,'',1,0,'C');
        $pdf->Cell($cell_w	,$cell_h,'',1,0,'C');
        $pdf->Cell($cell_w	,$cell_h,'',1,0,'C');
        $pdf->Cell($cell_w	,$cell_h,'',1,0,'C');
        $pdf->Cell($cell_w	,$cell_h,'',1,1,'C');


        $pdf->Ln(-20);
        $pdf->SetX( 7);
        $pdf->Cell($cell_w	,5,'',0,0,'C');
        $pdf->Cell($cell_w	,5,'',0,0,'C');



        $pdf->SetFillColor(185,183,188);


        $pdf->Cell($cell_w	,5,'tr1',1,0,'C',true);
        $pdf->Cell($cell_w	,5,'tr1',1,0,'C',true);
        $pdf->Cell($cell_w	,5,'tr1',1,0,'C',true);
        $pdf->Cell($cell_w	,5,'tr1',1,0,'C',true);
        $pdf->Cell($cell_w	,5,'',0,1,'C');

        $pdf->Ln(0);
        $pdf->SetX( 7);
        $pdf->Cell($cell_w	,5,'',0,0,'C');
        $pdf->Cell($cell_w	,5,'',0,0,'C');
        $pdf->Cell($cell_w	,5,'0.00',0,0,'C');
        $pdf->Cell($cell_w	,5,'0.00',0,0,'C');
        $pdf->Cell($cell_w	,5,'0.00',0,0,'C');
        $pdf->Cell($cell_w	,5,'0.00',0,0,'C');
        $pdf->Cell($cell_w	,5,'',0,1,'C');

        $pdf->Ln(0);
        $pdf->SetX( 7);
        $pdf->Cell($cell_w	,5,'',0,0,'C');
        $pdf->Cell($cell_w	,5,'',0,0,'C');

        $pdf->SetFillColor(185,183,188);

        $pdf->Cell($cell_w	,5,'tr3',1,0,'C' ,true);
        $pdf->Cell($cell_w	,5,'tr3',1,0,'C',true);
        $pdf->Cell($cell_w	,5,'tr3',1,0,'C',true);
        $pdf->Cell($cell_w	,5,'tr3',1,0,'C',true);
        $pdf->Cell($cell_w	,5,'',0,1,'C');


        $pdf->Ln(0);
        $pdf->SetX( 7);
        $pdf->Cell($cell_w	,5,'',0,0,'C');
        $pdf->Cell($cell_w	,5,'',0,0,'C');
        $pdf->Cell($cell_w	,5,'0.00',0,0,'C');
        $pdf->Cell($cell_w	,5,'0.00',0,0,'C');
        $pdf->Cell($cell_w	,5,'0.00',0,0,'C');
        $pdf->Cell($cell_w	,5,'0.00',0,0,'C');
        $pdf->Cell($cell_w	,5,'',0,1,'C');


        $pdf->Ln(0);
        $pdf->SetX( 7);
        $cell_h = 14;
        $pdf->Cell($cell_w	,$cell_h,'',1,0,'C');
        $pdf->Cell($cell_w	,$cell_h,'',1,0,'C');
        $pdf->Cell($cell_w	,$cell_h,'',1,0,'C');
        $pdf->Cell($cell_w	,$cell_h,'',1,0,'C');
        $pdf->Cell($cell_w	,$cell_h,'',1,0,'C');
        $pdf->Cell($cell_w	,$cell_h,'',1,0,'C');
        $pdf->Cell($cell_w	,$cell_h,'',1,1,'C');


        $pdf->Ln(-14);
        $pdf->SetX( 7);
        $pdf->SetFillColor(185,183,188);

        $pdf->Cell($cell_w	,7,'tr1',1,0,'C',true);
        $pdf->Cell($cell_w	,7,'tr1',1,0,'C',true);
        $pdf->Cell($cell_w	,7,'tr1',1,0,'C',true);
        $pdf->Cell($cell_w	,7,'tr1',1,0,'C',true);
        $pdf->Cell($cell_w	,7,'tr1',1,0,'C',true);
        $pdf->Cell($cell_w	,7,'tr1',1,0,'C',true);
        $pdf->Cell($cell_w	,7,'tr1',1,1,'C',true);



        $pdf->Ln(0);
        $pdf->SetX( 7);
        $pdf->SetFillColor(185,183,188);

        $pdf->Cell($cell_w	,7,'0.00',1,0,'C',true);
        $pdf->Cell($cell_w	,7,'0.00',1,0,'C',true);
        $pdf->Cell($cell_w	,7,'0.00',1,0,'C',true);
        $pdf->Cell($cell_w	,7,'0.00',1,0,'C',true);
        $pdf->Cell($cell_w	,7,'0.00',1,0,'C',true);
        $pdf->Cell($cell_w	,7,'0.00',1,0,'C',true);
        $pdf->Cell($cell_w	,7,'0.00',1,1,'C',true);

        $pdf->Ln(9);
        $pdf->SetX( 30);
        $pdf->Cell($cell_w	,7,'sign 1',0,0,'C');


        $pdf->SetX( 150);
        $pdf->Cell($cell_w	,7,'sign 2',0,1,'C');

        $cell_w = 50;
        $pdf->Ln(2);
        $pdf->SetX( 19);
        $pdf->Cell($cell_w	,7,'...............................',0,0,'C');


        $pdf->SetX( 139);
        $pdf->Cell($cell_w	,7,'...............................',0,1,'C');

        $pdf->Output();
        exit;


    }



}