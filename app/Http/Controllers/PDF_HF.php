<?php
/**
 * Created by PhpStorm.
 * User: sky
 * Date: 22/05/2018
 * Time: 11:27 م
 */

namespace App\Http\Controllers;
use Codedge\Fpdf\Fpdf\Fpdf;
/*require('wordwrap.php');*/
use App\WordWraps;

class PDF_HF extends WordWraps
{



    public $set_header;
    //public $printed_by;


    public $header_colums = array(
        array("Volvo",50),
        array("BMW",40),
        array("Saab",20),
        array("Land Rover",40),
        array("Landss",40)
    );

    public function Header()
    {
        $length = count($this->header_colums);

        //$header_colums = array("Volvo", "BMW", "Toyota", "Toyota", "Toyota");
        $path = public_path() . '/assets/img/';
        $filepath = $path . 'logo-dark.png';


        // Select Arial bold 15
        $this->SetFont('Arial','B',15);
        // Move to the right

        $this->Cell(15,15,"",0,0,'C');

        $this->image($filepath,10,9.7,15,0,'png');
        // Framed title

        $this->Cell(80,15,'The British School Alexandria',0,0,'C');
        // Line break
        $this->Ln(20);

        $this->Line(10, 30,200,30);
        $this->Ln(10);

        if ($this->page != 1 && $this->set_header == true)
        {   $this->Ln(5);
            $this->SetFontSize(14);
            for ($row = 0; $row < $length; $row++) {
                for ($column = 0; $column < 1; $column++){
                    if($row +1 == $length)
                    {
                        $this->Cell($this->header_colums[$row][$column+1], 7, $this->header_colums[$row][$column], 1, 1, 'C');
                    }
                    else{
                        $this->Cell($this->header_colums[$row][$column+1], 7, $this->header_colums[$row][$column], 1, 0, 'C');
                    }


                    }
            }
            /*$this->Cell(50	,7,'First_name ',1,0,'C');
            $this->Cell(40	,7,'Birth_of_date',1,0,'C');
            $this->Cell(20	,7,'Gender',1,0,'C');
            $this->Cell(40	,7,'Code',1,0,'C');
            $this->Cell(40	,7,'Acadimic Year',1,1,'C');*/
        }

    }


    public function setHeaderTitle($title)
    {
        $this->set_header=$title;
    }

    public function set_header_names($header_names)
    {
        $this->header_colums=$header_names;
    }


   /* public function set_printerd_by($user_printed)
    {
        $this->printed_by =  $user_printed;
    }*/

   public function wrap_text($string , $cell_width)
   {
       $tempcell_width = $cell_width;
       while ($this->GetStringWidth($string) > $cell_width)
       {
           $this->SetFontSize($tempcell_width -=0.1 );
       }

       return $tempcell_width;
   }

    function Footer()
    {
        $employee = \DB::table('employee')
            ->where('employee_user_id','=',auth()->user()->id)
            ->first();





        // Go to 1.5 cm from bottom
        $date = date("Y-m-d");
        $timezone = date_default_timezone_get();
        $this->Line(10, 280,200,280);

        $this->SetY(-15);
        // Select Arial italic 8
        $this->SetFont('Arial','I',8);
        // Print centered page number
        $this->SetX( -40);
        $this->Cell(0,10,$date."  ". date("h:i:sa"),0,0,'C');

        $this->SetX( 10);
        $this->Cell(0,10,'Octopus For Integrated Solutions',0,0,'L');

        /*-------------------------------Printed By:------------------------------------------------*/
        $this->Line(10, 280,200,280);
        $this->SetY(-10);
        $this->SetX( 10);
        $this->Cell(0,10,'Printed By',0,0,'L');
        $this->SetX(24);
        $this->Cell(0, 10, ':', 0, 0, 'L');
        $this->SetX( 26);
        $this->SetFont('','');
        $this->Cell(0,10,$employee->employee_name .' '.$employee->middle_name.' '.$employee->last_name.' '.$employee->family_name,0,0,'L');
        /*--------------------------------------------------------------------------------------------*/

        $this->SetX( 0);
        $this->Cell(0,10,'Page '.$this->PageNo()." of {pages}",0,0,'C');
    }

    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb=0;
        for($i=0;$i<count($data);$i++)
            $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
        $h=5*$nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for($i=0;$i<count($data);$i++)
        {
            $w=$this->widths[$i];
            $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'C';
            //Save the current position
            $x=$this->GetX();
            $y=$this->GetY();
            //Draw the border
            $this->Rect($x,$y,$w,$h);
            //Print the text
            $this->MultiCell($w,5,$data[$i],0,$a);
            //Put the position to the right of the cell
            $this->SetXY($x+$w,$y);
        }
        //Go to the next line
        $this->Ln($h);
    }
    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}