<?php

namespace App\Http\Controllers;


use App\AcademicYear;

use App\Allowence_deduction;
use App\Classes;
use App\cost_centers;
use App\countries;
use App\Employee;
use App\EmployeeAllowence;
use App\EmployeeFamily;
use App\EmployeeSalary;
use App\Job;
use App\Nationality;
use App\nationalty_type;
use App\parents;
use App\relations_students_parents;
use App\students;

use App\SubSalary;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;

use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\PDF_HF;
use Image;



class employee_report_controller extends Controller
{



    //-----------------------------------TABLE-----------------------//
    public function get_employee(Request $request)

    {

        $array =  explode(',', $request->input('print_ids')[0]);

            $query = DB::table('employee')
            ->join('nationalities','nationalities.id','employee.nationality_id')
            ->join('departments','departments.dep_id','employee.depart_id')
            ->join('jobs','jobs.job_id','employee.job_id')
            ->whereIn('employee_id',$array)
            ->get();


        $pdf = new PDF_HF('P','mm','A4');

        $pdf->AddPage();

        $pdf->AliasNbPages('{pages}');

//set font to arial, bold, 14pt

        $pdf->SetFont('Arial','B',12);

//Cell(width , height , text , border , end line , [align] )
        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX( 90);

        $pdf->Cell(40,10,"Employees Table",0,0,'C');
        $pdf->Ln(11);

        $pdf->setFillColor(230,230,230);


        $pdf->Ln(15);
        $pdf->Cell(10	,7,'ID',1,0,'C',1);
        $pdf->Cell(35	,7,'Employee Name',1,0,'C',1);
        $pdf->Cell(25	,7,'Nationality',1,0,'C',1);

        $pdf->Cell(30	,7,'Department',1,0,'C',1);

        $pdf->Cell(20	,7,'Job',1,0,'C',1);

        $pdf->Cell(20	,7,'Gender',1,0,'C',1);

        $pdf->Cell(30	,7,'Birth Date',1,0,'C',1);

        $pdf->Cell(20	,7,'Leave',1,0,'C',1);


        /*-----------------------  change header -----------------------------*/
        $headrs_name = array(
            array("Employee ID",10),
            array("Employee Name",35),
            array("Nationality",25),
            array("Department",30),
            array("Job",20),
            array("Gender",20),
            array("Birth Date",30),
            array("Leave",20),


        );
        $pdf->set_header_names($headrs_name);
        /*-------------------------------------------------------*/
        $pdf->SetFontSize(10);

        /*set the table header */
        $pdf->setHeaderTitle(true);

        /*------------------------------*/
        $pdf->SetWidths(array(10,35,25,30,20,20,30,20));
        $pdf->Ln(7);

        foreach ($query as $val)
        {

            $pdf->SetFont('Arial','B',12);

            $pdf->Row(array($val->employee_id,$val->employee_name,$val->name,$val->dep_name,$val->job_name,$val->gender,$val->date_of_birth,$val->leave));

        }


        $pdf->Ln(5);
        $pdf->SetX( 40);

        /*remove the table header */
        $pdf->setHeaderTitle(false);
        /*------------------------------*/

        $pdf->Output();
        exit;


    }






    //-----------------------------report-------------------------//

    public function get_employee_rpt($id)

    {

        $employee = Employee::findorfail($id);

        $employee_family = EmployeeFamily::where('employee_id', $id)->get();
        $employee_allownaces = EmployeeAllowence::where('employee_id', $id)->get();
        $employee_salary = EmployeeSalary::where('employee_id', $id)->get();


        //------------------------------IMAGE-------------------//

        if(file_exists ('uploads/employees/'.$id.'_.png'))
        {
            //Image::make('uploads/employees/'.$id.'_.png')->save('public/uploads/employees/'.$id.'_.png');

            $path = public_path('uploads/employees/'.$id.'_.png');

        }else {

             $path = 'uploads/employees/default2.png';

        }



        $pdf = new PDF_HF('P', 'mm', 'A4');

        $pdf->AddPage();

        $pdf->AliasNbPages('{pages}');

//set font to arial, bold, 14pt

        $pdf->SetFont('Arial', 'B', 10);

//Cell(width , height , text , border , end line , [align] )

        $pdf->SetX(10);
//        $pdf->Image( $path,10,30,50,0,'jpg');

        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX(90);
        $pdf->SetFont('Arial', 'UB');
        $pdf->Cell(40, 10, "Personal Information", 0, 0, 'C');
        $pdf->Ln(5);

        $y = $pdf->GetY();
        $pdf->SetY($y);
        $pdf->Cell(189, 50, "", 0, 0, 'C');

        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX(90);
        $y1 = $pdf->GetY();
        $pdf->SetY($y1);


        $pdf->Ln(5);
        $pdf->SetX(10);
        $pdf->SetFont('Arial', 'B');

        $pdf->Image(@$path, 20, $pdf->SetY($y1), 30, 0, 'PNG');

        $pdf->Ln(5);
        $pdf->SetX(10);
        $pdf->SetFont('', 'B');

        $pdf->Cell(15, 10, 'Related User', 0, 0, 'L');
        $pdf->SetX(45);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, "", 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->Cell(30, 12, '.................................', 0, 0, 'L');



        $pdf->Ln(10);
        $pdf->SetX(10);
        $pdf->SetFont('Arial', 'B');

        $pdf->Cell(15, 10, 'First Name', 0, 0, 'L');
        $pdf->SetX(45);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, @$employee->employee_name, 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');

        $pdf->SetX(-105);
        $pdf->SetFont('', 'B');

        $pdf->Cell(15, 10, 'Middle Name', 0, 0, 'L');
        $pdf->SetX(-65);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(-60);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, @$employee->middle_name, 0, 0, 'L');
        $pdf->SetX(-60);
        $pdf->Cell(30, 12, '.................................', 0, 0, 'L');

        $pdf->Ln(10);
        $pdf->SetX(10);
        $pdf->SetFont('Arial', 'B');

        $pdf->Cell(15, 10, 'Last Name', 0, 0, 'L');
        $pdf->SetX(45);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, @$employee->last_name, 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');

        $pdf->SetX(-105);
        $pdf->SetFont('', 'B');

        $pdf->Cell(15, 10, 'Family Name', 0, 0, 'L');
        $pdf->SetX(-65);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(-60);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, @$employee->family_name, 0, 0, 'L');
        $pdf->SetX(-60);
        $pdf->Cell(30, 12, '.................................', 0, 0, 'L');






        $pdf->Ln(10);
        $pdf->SetX(10);
        $pdf->SetFont('', 'B');

        $pdf->Cell(15, 10, 'Gender', 0, 0, 'L');
        $pdf->SetX(45);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, @$employee->gender, 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');

        $pdf->SetX(-105);
        $pdf->SetFont('', 'B');

        $pdf->Cell(15, 10, 'Nationality', 0, 0, 'L');
        $pdf->SetX(-65);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(-60);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, @Nationality::find($employee->nationality_id)->name, 0, 0, 'L');
        $pdf->SetX(-60);
        $pdf->Cell(30, 12, '.................................', 0, 0, 'L');


        $pdf->Ln(10);
        $pdf->SetX(10);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'Date of Birth', 0, 0, 'L');
        $pdf->SetX(45);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, @$employee->date_of_birth, 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');

        $pdf->SetX(-105);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'Work Mobile', 0, 0, 'L');
        $pdf->SetX(-65);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(-60);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, @$employee->mobile_work, 0, 0, 'L');
        $pdf->SetX(-60);
        $pdf->Cell(30, 12, '.................................', 0, 0, 'L');


        $pdf->Ln(10);
        $pdf->SetX(10);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'Office Location', 0, 0, 'L');
        $pdf->SetX(45);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, @$employee->office_location, 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');


        $pdf->SetX(-105);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'Country', 0, 0, 'L');
        $pdf->SetX(-65);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(-60);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, @$employee->Country_Name, 0, 0, 'L');
        $pdf->SetX(-60);
        $pdf->Cell(30, 12, '.................................', 0, 0, 'L');


        $pdf->Ln(10);
        $pdf->SetX(10);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'Home Address', 0, 0, 'L');
        $pdf->SetX(45);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10,@$employee->home_address, 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');


        $pdf->Ln(10);

        $pdf->SetFont('Arial', 'B', 10);

//Cell(width , height , text , border , end line , [align] )

        $pdf->SetX(10);
//        $pdf->Image( $path,10,30,50,0,'jpg');

        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX(80);

//        $pdf->Cell( 40, 40, $pdf->Image($image1, $pdf->GetX(), $pdf->GetY(), 33.78), 0, 0, 'L', false );
        $pdf->SetFont('Arial', 'UB');
        $pdf->Cell(40, 10, "HR Settings", 0, 0, 'C');
        $pdf->Ln(5);

        $y = $pdf->GetY();
        $pdf->SetY($y);
        $pdf->Cell(189, 50, "", 0, 0, 'C');

        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX(90);


        $pdf->Ln(5);
        $pdf->SetX(10);
        $pdf->SetFont('Arial', 'B');

        $pdf->Cell(15, 10, 'User Type', 0, 0, 'L');
        $pdf->SetX(45);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, @$employee->user_type, 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');

        $pdf->SetX(-105);
        $pdf->SetFont('', 'B');

        $pdf->Cell(15, 10, 'Marital Status', 0, 0, 'L');
        $pdf->SetX(-65);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(-60);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, @$employee->marital_status, 0, 0, 'L');
        $pdf->SetX(-60);
        $pdf->Cell(30, 12, '.................................', 0, 0, 'L');


        $pdf->Ln(10);
        $pdf->SetX(10);
        $pdf->SetFont('Arial', 'B');

        $pdf->Cell(15, 10, 'With Partner', 0, 0, 'L');
        $pdf->SetX(45);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, @$employee->with_partner, 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');

        if ($employee_family->count() > 0) {
            ///////////////////

            $pdf->Ln(15);

            $x = $pdf->GetX();
            $pdf->SetX($x);

            $y = $pdf->GetY();
            $pdf->SetY($y);


//            $pdf->Line($x, $y, $x + 190, $y);


            $pdf->SetX($pdf->GetX() - 94);
            $pdf->SetX(80);
            $pdf->SetFont('', 'UB');
            $pdf->Cell(40, 10, "Family", 0, 0, 'C');
            $pdf->Ln(5);

            $y = $pdf->GetY();
            $pdf->SetY($y);
            $pdf->Cell(189, 50, "", 0, 0, 'C');

            $pdf->SetX($pdf->GetX() - 94);
            $pdf->SetX(90);

            ///////////////////

            $pdf->Ln(10);
            $pdf->setFillColor(230, 230, 230);
            $pdf->SetFont('', 'B');
            $pdf->Cell(100, 7, 'Name	', 1, 0, 'C', 1);
            $pdf->Cell(90, 7, 'Class', 1, 0, 'C', 1);

            /*-----------------------  change header -----------------------------*/
            $headrs_name = array(
                array("Name", 100),
                array("Class", 90),

            );
            $pdf->SetFillColor(100, 90);


            $pdf->set_header_names($headrs_name);
            /*-------------------------------------------------------*/
            $pdf->SetFontSize(10);

            /*set the table header */
            $pdf->setHeaderTitle(true);

            /*------------------------------*/
            $pdf->SetWidths(array(100, 90));
            $pdf->Ln(7);


            foreach ($employee_family as $val) {

                $pdf->SetFont('Arial', '', 12);


                $pdf->Row(array($val->name, Classes::find($val->class_id)->ClassName));


            }

            $pdf->SetX(40);

            /*remove the table header */
            $pdf->setHeaderTitle(false);
        }
        /*------------------------------*/
        $pdf->Ln(10);
        $pdf->SetX(10);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'Percentage Salary', 0, 0, 'L');
        $pdf->SetX(45);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, @$employee->percentage_salary, 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');


        $pdf->SetX(-105);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'Academic Year', 0, 0, 'L');
        $pdf->SetX(-65);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(-60);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, @AcademicYear::find($employee->acadimic_year_id)->year_name, 0, 0, 'L');
        $pdf->SetX(-60);
        $pdf->Cell(30, 12, '.................................', 0, 0, 'L');


        $pdf->Ln(10);
        $pdf->SetX(10);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'Department', 0, 0, 'L');
        $pdf->SetX(45);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, "", 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');


        $pdf->SetX(-105);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'Jop Title', 0, 0, 'L');
        $pdf->SetX(-65);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(-60);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, @Job::find($employee->job_id)->job_name, 0, 0, 'L');
        $pdf->SetX(-60);
        $pdf->Cell(30, 12, '.................................', 0, 0, 'L');

        $pdf->Ln(10);
        $pdf->SetX(10);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'Manager', 0, 0, 'L');
        $pdf->SetX(45);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, @Employee::find($employee->manager_id)->employee_name, 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');


        $pdf->SetX(-105);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'Date of contract', 0, 0, 'L');
        $pdf->SetX(-65);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(-60);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, @$employee->date_of_contract, 0, 0, 'L');
        $pdf->SetX(-60);
        $pdf->Cell(30, 12, '.................................', 0, 0, 'L');


        $pdf->Ln(10);
        $pdf->SetX(10);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'leave', 0, 0, 'L');
        $pdf->SetX(45);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, @$employee->leave, 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');


        $pdf->SetX(-105);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'Date of Leaving:', 0, 0, 'L');
        $pdf->SetX(-65);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(-60);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, @$employee->date_of_leave, 0, 0, 'L');
        $pdf->SetX(-60);
        $pdf->Cell(30, 12, '.................................', 0, 0, 'L');

        /************************************************************************************************/
        if ($employee_allownaces->count() > 0) {
            $pdf->Ln(15);

            $x = $pdf->GetX();
            $pdf->SetX($x);

            $y = $pdf->GetY();
            $pdf->SetY($y);


            $pdf->Line($x, $y, $x + 190, $y);


            $pdf->SetX($pdf->GetX() - 94);
            $pdf->SetX(80);
            $pdf->SetFont('', 'UB');
            $pdf->Cell(40, 10, "Allowance", 0, 0, 'C');
            $pdf->Ln(5);

            $y = $pdf->GetY();
            $pdf->SetY($y);
            $pdf->Cell(189, 50, "", 0, 0, 'C');

            $pdf->SetX($pdf->GetX() - 94);
            $pdf->SetX(90);


            $pdf->Ln(5);
            $pdf->setFillColor(230, 230, 230);
            $pdf->SetFont('', 'B');
            $pdf->Cell(45, 7, 'Allowance', 1, 0, 'C', 1);
            $pdf->Cell(45, 7, 'Academic Year', 1, 0, 'C', 1);
            $pdf->Cell(40, 7, 'From	', 1, 0, 'C', 1);
            $pdf->Cell(30, 7, 'To', 1, 0, 'C', 1);
            $pdf->Cell(30, 7, 'Value', 1, 0, 'C', 1);

            /*-----------------------  change header -----------------------------*/
            $headrs_name = array(
                array("Allowance", 45),
                array("Academic Year", 45),
                array("From	", 40),
                array("To", 30),
                array("Value", 30),


            );
            $pdf->SetFillColor(100, 90);


            $pdf->set_header_names($headrs_name);
            /*-------------------------------------------------------*/
            $pdf->SetFontSize(10);

            /*set the table header */
            $pdf->setHeaderTitle(true);

            /*------------------------------*/
            $pdf->SetWidths(array(45, 45, 40, 30, 30));
            $pdf->Ln(7);


            foreach ($employee_allownaces as $val) {
                $pdf->SetFont('Arial', '', 12);


                $pdf->Row(array(Allowence_deduction::find($val->allow_id)->allow_name, $val->value, $val->from_date, $val->to_date, AcademicYear::find($val->academic_year_id)->year_name));


            }

            $pdf->SetX(40);

            /*remove the table header */
            $pdf->setHeaderTitle(false);
        }
        /**************************************************************************************/
        if ($employee_salary->count() > 0) {

            $pdf->Ln(15);

            $x = $pdf->GetX();
            $pdf->SetX($x);

            $y = $pdf->GetY();
            $pdf->SetY($y);


            $pdf->Line($x, $y, $x + 190, $y);


            $pdf->SetX($pdf->GetX() - 94);
            $pdf->SetX(80);
            $pdf->SetFont('', 'UB');
            $pdf->Cell(40, 10, "Sallary Scale", 0, 0, 'C');
            $pdf->Ln(5);

            $y = $pdf->GetY();
            $pdf->SetY($y);
            $pdf->Cell(189, 50, "", 0, 0, 'C');

            $pdf->SetX($pdf->GetX() - 94);
            $pdf->SetX(90);


            $pdf->Ln(5);
            $pdf->setFillColor(230, 230, 230);
            $pdf->SetFont('', 'B');
            $pdf->Cell(60, 7, 'Salary Scale	', 1, 0, 'C', 1);
            $pdf->Cell(60, 7, 'Academic Year	', 1, 0, 'C', 1);
            $pdf->Cell(70, 7, 'Start From	', 1, 0, 'C', 1);


            /*-----------------------  change header -----------------------------*/
            $headrs_name = array(
                array("Salary Scale", 60),
                array("Academic Year", 60),
                array("Start From", 70),


            );
            $pdf->SetFillColor(100, 90);


            $pdf->set_header_names($headrs_name);
            /*-------------------------------------------------------*/
            $pdf->SetFontSize(10);

            /*set the table header */
            $pdf->setHeaderTitle(true);

            /*------------------------------*/
            $pdf->SetWidths(array(60, 60, 70));
            $pdf->Ln(7);


            foreach ($employee_salary as $val) {

                $pdf->SetFont('Arial', '', 12);

                $pdf->Row(array(SubSalary::find($val->sub_salary_id)->sub_name, AcademicYear::find($val->academic_year_id)->year_name, $val->start_in));

            }

            $pdf->SetX(40);

            /*remove the table header */
            $pdf->setHeaderTitle(false);

        }


        $pdf->Output();
        exit;


    }


}