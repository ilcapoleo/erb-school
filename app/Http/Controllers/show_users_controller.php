<?php

namespace App\Http\Controllers;


use App\Employee;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Facades\Response;
use Illuminate\Validation\Rule;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Validator;



class show_users_controller extends Controller
{
    //
   /* public function ret_cost_page()
    {
        return view('admin_pages.cost_center_page');
    }*/
     public function get_table_page()
     {
         $showed_search_list = array('User Name','User Email','Employee Name','Created Date');
         $database_search_list = array(1,2,3,4);

         $users = DB::table('userss')
             ->join('employee','userss.id','=','employee.employee_user_id')
             ->orderBy('userss.id','desc')
             ->get();


         return view('school_pages.show_users',['showed_search_list' => $showed_search_list,'database_search_list'=>$database_search_list],compact('users'));
     }


    public function search_users_table(Request $request)
    {

        $search_colums=array();
        $book=array();

        $string = $request->sudent_data;

        foreach ($string as $value) {

            list($k, $v) = explode('|', $value);
            if($v == "")
            {
                array_push($search_colums,"all");
                array_push($book,$k);
            }
            else{
                array_push($search_colums,$v);
                array_push($book,$k);

            }

        }

        //$search_colums = array('s_name','s_class','s_age');

        $showed_colums = array('User Name','User Email','Created Date');
        $colums_type = array('input','input','input');
        $table_colums = array('name','email','created_at');

        //$book = $request->sudent_data;

        if(count($book) >0 && $book[0] != "all")
        {
            $name = DB::table('userss')



                ->Where(function ($query) use($book,$search_colums ,$table_colums) {
                    /*for ($i = 0; $i < count($book); $i++){
                        for ($h = 0; $h < count($search_colums); $h++){
                            $query->orwhere($search_colums[$h], 'like',  '%' . $book[$i] .'%');
                        }
                    }*/
                    for ($i = 0; $i < count($book); $i++){
                        if($search_colums[$i] == "all")
                        {
                            $table_colums;
                            for ($h = 0; $h < count($table_colums); $h++){
                                $query->orwhere($table_colums[$h], 'like',  '%' . $book[$i] .'%');
                            }

                        }
                        else
                        {
                            $query->orwhere($search_colums[$i], 'like',  '%' . $book[$i] .'%');
                        }

                    }
                })
                ->orderBy('id','desc')
                ->get();

        }
        else
        {

            $name = DB::table('userss')

                ->orderBy('id','desc')
                ->get();
        }
        return response(["name"=>$name ,"colums"=> $table_colums,"showed_colums"=>$showed_colums,"book"=>$book,"searched_colums"=>$search_colums,"colums_type"=>$colums_type]);
    }


    public function delete_all_selected_users(Request $request)
    {
        foreach ($request->deleted_user_ids as $class_id_one)
        {
            $user = User::findOrFail($class_id_one);
            $user->delete();

            Employee::where('employee_user_id', $class_id_one)->update(array('employee_user_id' => NULL));


        }

        return response($request);
    }

}
