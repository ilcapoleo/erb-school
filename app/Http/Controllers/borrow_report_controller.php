<?php

namespace App\Http\Controllers;

use App\AcademicYear;
use App\Allowence_deduction;
use App\Borrowing;
use App\Currency;
use App\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\PDF_HF;


class borrow_report_controller extends Controller
{
    //-----------------------------table-------------------------//

    public function get_borrow(Request $request)

    {

        $print_array = array();
        $array = explode(',', $request->input('print_ids')[0]);

        foreach ($array as $single) {
            array_push($print_array, $single);
        }

        $query = DB::table('borrowing')
            ->join('currencies', 'borrowing.currency_id', '=', 'currencies.Currency_id')
            ->join('document_status', 'borrowing.borrow_status', '=', 'document_status.status_id')
            ->join('employee as employee_name_borrowing', 'borrowing.employe_id', '=', 'employee_name_borrowing.employee_id')
            ->join('employee as employee_name_creator', 'borrowing.requested_by_id', '=', 'employee_name_creator.employee_user_id')
            ->select('*', 'employee_name_borrowing.employee_name as employee_borrowing_name', 'employee_name_creator.employee_name as employee_creator_name')
            ->wherein('borrowing.borrow_id', $print_array)
            ->orderBy('borrowing.borrow_id', 'desc')
            ->get();

//        dd($query);


        $pdf = new PDF_HF('P', 'mm', 'A4');

        $pdf->AddPage();

        $pdf->AliasNbPages('{pages}');

//set font to arial, bold, 14pt

        $pdf->SetFont('Arial', 'B', 12);

//Cell(width , height , text , border , end line , [align] )
        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX(90);

        $pdf->Cell(40, 10, "Borrowings Table", 0, 0, 'C');
        $pdf->Ln(11);
        $pdf->setFillColor(230,230,230);

        $pdf->Ln(15);
        $pdf->Cell(20, 7, 'ID', 1, 0, 'C',1);
        $pdf->Cell(40, 7, 'Employee Name', 1, 0, 'C',1);
        $pdf->Cell(25, 7, 'Value', 1, 0, 'C',1);
        $pdf->Cell(30, 7, 'Currency', 1, 0, 'C',1);
        $pdf->Cell(30, 7, 'Status', 1, 0, 'C',1);
        $pdf->Cell(35, 7, 'Creator Name', 1, 0, 'C',1);

        /*-----------------------  change header -----------------------------*/
        $headrs_name = array(
            array("ID", 20),
            array("Employee Name", 40),
            array("Value", 25),
            array("Currency", 30),
            array("Status", 30),
            array("Creator Name", 35),

        );
        $pdf->set_header_names($headrs_name);
        /*-------------------------------------------------------*/
        $pdf->SetFontSize(10);

        /*set the table header */
        $pdf->setHeaderTitle(true);

        /*------------------------------*/
        $pdf->SetWidths(array(20, 40, 25, 30, 30, 35));
        $pdf->Ln(7);
        foreach ($query as $val) {

            $pdf->SetFont('Arial', 'B', 12);

            $pdf->Row(array($val->borrow_id, $val->employee_name, $val->value, $val->Currency_name, $val->status_name, $val->employee_creator_name));

        }


        $pdf->Ln(5);
        $pdf->SetX(40);

        /*remove the table header */
        $pdf->setHeaderTitle(false);
        /*------------------------------*/

        $pdf->Output();
        exit;


    }

    //----------------------------report-----------------------------//

    public function get_borrow_rpt($id)

    {
        $borrow = Borrowing::where('borrow_id',$id)->first();

        $name = Employee::where('employee_id',$borrow->employe_id)->first()->employee_name;
        $currency =  Currency::where('Currency_id',$borrow->currency_id)->first()->Currency_name;
        $acad_year = isset(AcademicYear::where('acadimic_id',$borrow->accadimic_year_id)->first()->year_name) ? AcademicYear::where('acadimic_id',$borrow->accadimic_year_id)->first()->year_name : "" ;
        $deduction_type=Allowence_deduction::where('allow_id',$borrow->deduction_id)->first()->allow_name;
        $query = DB::table('borrowing_account')
            ->join('months', 'borrowing_account.month_id', '=', 'months.month_id')
            ->where('borrowing_account.borrow_id', '=', $id)
            ->orderBy('borrowing_account.acc_id', 'desc')
            ->get();

        $req_by = Employee::where('employee_user_id',$borrow->requested_by_id)->first()->employee_name;
        $app_by = isset(Employee::where('employee_user_id',$borrow->approved_by)->first()->employee_name) ? Employee::where('employee_user_id',$borrow->approved_by)->first()->employee_name : "";
        $pdf = new PDF_HF('P', 'mm', 'A4');

        $pdf->AddPage();

        $pdf->AliasNbPages('{pages}');

//set font to arial, bold, 14pt

        $pdf->SetFont('Arial', 'B', 12);

//Cell(width , height , text , border , end line , [align] )
        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX(80);

        $pdf->Cell(40, 10, "Borrowings NO: $borrow->borrow_id", 0, 0, 'C');

        $pdf->SetX(60);
        $pdf->SetFont('', '');

        $pdf->Ln(15);
        $pdf->SetX(10);
        $pdf->SetFont('', 'UB');
        $pdf->Cell(15, 10, 'Borrowing', 0, 0, 'L');


        $pdf->Ln(10);
        $pdf->SetX(10);
        $pdf->SetFont('', 'B');

        $pdf->Cell(15, 10, 'Name', 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(60);
        $pdf->SetFont('', '');

        $pdf->Cell(50, 7, $name, 0, 0, 'L');
        $pdf->SetX(60);
        $pdf->Cell(50, 10, '..............................', 0, 0, 'L');

        $pdf->Ln(15);
        $pdf->SetX(10);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'Value', 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(60);

        $pdf->SetFont('', '');
        $pdf->Cell(50, 7,$borrow->value , 0, 0, 'L');
        $pdf->SetX(60);

        $pdf->Cell(50, 10, '..............................', 0, 0, 'L');

        $pdf->Ln(15);
        $pdf->SetX(10);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'Currency', 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(60);
        $pdf->SetFont('', '');
        $pdf->Cell(50, 7, $currency, 0, 0, 'L');
        $pdf->SetX(60);
        $pdf->Cell(50, 10, '..............................', 0, 0, 'L');

        $pdf->Ln(15);
        $pdf->SetX(10);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'Date', 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(60);
        $pdf->SetFont('', '');
        $pdf->Cell(50, 7, $borrow->date, 0, 0, 'L');
        $pdf->SetX(60);
        $pdf->Cell(50, 10, '..............................', 0, 0, 'L');
if(isset($borrow->rate)) {
    $pdf->Ln(15);
    $pdf->SetX(10);
    $pdf->SetFont('', 'B');
    $pdf->Cell(15, 10, 'Rate', 0, 0, 'L');
    $pdf->SetX(50);
    $pdf->Cell(15, 10, ':', 0, 0, 'L');
    $pdf->SetX(60);
    $pdf->SetFont('', '');
    $pdf->Cell(50, 7, $borrow->rate, 0, 0, 'L');
    $pdf->SetX(60);
    $pdf->Cell(50, 10, '..............................', 0, 0, 'L');
}


        //APPROVED BY ---- REQUESTED BY //
        $pdf->Ln(10);
        $y = $pdf->GetY() ;
        $pdf->SetY($y);
        $x = $pdf->GetX() ;
        $pdf->SetX($x);

        $pdf->Ln(10);
        $pdf->SetX( 20);
        $pdf->SetFont('','B');
        $pdf->Cell(15,10,'Requested By',0,0,'C');



        $pdf->SetX( -50);
        $pdf->SetFont('','B');

        $pdf->Cell(15,10,'Approved By',0,0,'C');




        $pdf->Ln(10);
        $pdf->SetX( 20);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,$req_by,0,0,'L');
        $pdf->SetX( 20);
        $pdf->Cell(15,14,'........................',0,0,'C');

        $pdf->SetX( -50);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,$app_by  ,0,0,'L');
        $pdf->SetX( -50);
        $pdf->Cell(15,14,'.........................',0,0,'C');





        $pdf->Ln(20);
        $pdf->SetX(10);
        $pdf->SetFont('', 'UB');
        $pdf->Cell(15, 10, 'Deduction Information', 0, 0, 'L');




        $pdf->Ln(15);
        $pdf->SetX(10);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'Deduction Type', 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(60);
        $pdf->Cell(15, 10, $deduction_type, 0, 0, 'L');
        $pdf->SetX(60);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');

        $pdf->SetX(-90);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'Date', 0, 0, 'L');
        $pdf->SetX(-70);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(-60);
        $pdf->Cell(15, 10, $borrow->deliver_date, 0, 0, 'L');
        $pdf->SetX(-60);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');

        $pdf->Ln(15);
        $pdf->SetX(10);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'Academic Year', 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, $acad_year, 0, 0, 'L');
        $pdf->SetX(60);
        $pdf->Cell(15, 10, "", 0, 0, 'L');
        $pdf->SetX(60);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');


        $pdf->Ln(10);
        $pdf->setFillColor(230, 230, 230);
        $pdf->Cell(60, 7, 'Months', 1, 0, 'C', 1);
        $pdf->Cell(60, 7, 'Value', 1, 0, 'C', 1);
        $pdf->Cell(70, 7, 'Paid', 1, 0, 'C', 1);

        /*-----------------------  change header -----------------------------*/
        $headrs_name = array(
            array("Months", 60),
            array("Value", 60),
            array("Paid", 70)
        );
        $pdf->SetFillColor(50, 50, 50);


        $pdf->set_header_names($headrs_name);
        /*-------------------------------------------------------*/
        $pdf->SetFontSize(10);

        /*set the table header */
        $pdf->setHeaderTitle(true);

        /*------------------------------*/
        $pdf->SetWidths(array(60, 60, 70));
        $pdf->Ln(7);
        $tot = 0 ;
        foreach ($query as $val) {

            if($val->paid==0){
                $yes='no';
            }
            else{
                $yes='yes';

            }
            $pdf->SetFont('Arial', 'B', 12);

            $pdf->Row(array($val->month_name, $val->value_ber_month, $yes));
            $tot += $val->value_ber_month;

        }
            $pdf->Ln(5);
            $pdf->SetX(40);

            /*remove the table header */
            $pdf->setHeaderTitle(false);
            /*------------------------------*/

            $pdf->SetX(30);
            $pdf->SetFont('', 'B');
            $pdf->Cell(15, 10, 'Total', 0, 0, 'C');
            $pdf->SetX(68);
            $pdf->Cell(15, 10, ':', 0, 0, 'L');
            $pdf->SetX(95);
            $pdf->Cell(15, 10, $tot, 0, 0, 'L');
            $pdf->SetX(80);
            $pdf->SetFont('', '');
            $pdf->Cell(30, 12, '..............................', 0, 0, 'L');


        $pdf->Ln(20);
        $pdf->SetX( 20);
        $pdf->SetFont('','B');
        $pdf->Cell(15,10,'Accountant',0,0,'C');



        $pdf->SetX( -50);
        $pdf->SetFont('','B');

        $pdf->Cell(15,10,'Financial Manager',0,0,'C');

        $pdf->Ln(10);
        $pdf->SetX( 20);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,"",0,0,'L');
        $pdf->SetX( 20);
        $pdf->Cell(15,14,'........................',0,0,'C');


        $pdf->SetX( -50);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,"",0,0,'L');
        $pdf->SetX( -50);
        $pdf->Cell(15,14,'.........................',0,0,'C');

            $pdf->Output();
            exit;





    }

}