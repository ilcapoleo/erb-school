<?php

namespace App\Http\Controllers;

use App\leave_t;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\PDF_HF;


class leave_report_controller extends Controller
{

    public function get_leave(Request $request)

    {
        // define('FPDF_FONTPATH',public_path('font'));



        /*$accounts = Chart::all();
        $pdf = PDF::loadView('admin_pages.journal_entry_repory',['accounts'=>$accounts]);
        return $pdf->stream('accounts.pdf');*/

//get invoices data
        //$request->input('print_ids');
        $print_array = array();
        $array =  explode(',', $request->input('print_ids')[0]);

        foreach ($array as $single)
        {
            array_push($print_array, $single);
        }


     $query = DB::table('leave_type')
            ->whereIn('leave_type.leave_id',$print_array)
            ->orderBy('leave_type.leave_id','desc')

            ->get();



        $pdf = new PDF_HF('P','mm','A4');

        $pdf->AddPage();

        $pdf->AliasNbPages('{pages}');

//set font to arial, bold, 14pt

        $pdf->SetFont('Arial','B',14);

//Cell(width , height , text , border , end line , [align] )
        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX( 90);

        $pdf->Cell(40,10,"Leave Type Table",0,0,'C');
        $pdf->Ln(11);

        $pdf->setFillColor(230,230,230);

        $pdf->Ln(15);
        $pdf->Cell(60	,7,'ID',1,0,'C',1);
        $pdf->Cell(70	,7,'Leave Name',1,0,'C',1);
        $pdf->Cell(60	,7,'Short Code',1,0,'C',1);

        /*-----------------------  change header -----------------------------*/
        $headrs_name = array(
            array("ID",60),
            array("Leave Name",70),
            array("Short Code",60),

        );
        $pdf->set_header_names($headrs_name);
        /*-------------------------------------------------------*/
        $pdf->SetFontSize(10);

        /*set the table header */
        $pdf->setHeaderTitle(true);

        /*------------------------------*/
        $pdf->SetWidths(array(60,70,60));
        $pdf->Ln(7);

        foreach ($query as $val)
        {

            $pdf->SetFont('Arial','B',12);

            $pdf->Row(array($val->leave_id,$val->leave_name,$val->short_code));

        }


        $pdf->Ln(5);
        $pdf->SetX( 40);

        /*remove the table header */
        $pdf->setHeaderTitle(false);
        /*------------------------------*/

        $pdf->Output();
        exit;


    }



}