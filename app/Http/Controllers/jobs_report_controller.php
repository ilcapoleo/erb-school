<?php

namespace App\Http\Controllers;

use App\Department;
use App\Employee;
use App\Job;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\PDF_HF;


class jobs_report_controller extends Controller
{


    //-----------------------------table-------------------------//

    public function get_jobs(Request $request)

    {




        $print_array = array();
        $array =  explode(',', $request->input('print_ids')[0]);




        foreach ($array as $single)
        {
            array_push($print_array, $single);
        }
        $query = DB::table('jobs')
            ->join('departments','jobs.department_id','=','departments.dep_id')

            ->orderBy('jobs.job_id','desc')
            ->whereIn('job_id',$print_array)
            ->get();






        $pdf = new PDF_HF('P','mm','A4');

        $pdf->AddPage();

        $pdf->AliasNbPages('{pages}');





//set font to arial, bold, 14pt

        $pdf->SetFont('Arial','B',14);

//Cell(width , height , text , border , end line , [align] )
        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX( 90);

        $pdf->Cell(40,10,"Jobs Table",0,0,'C');
        $pdf->Ln(11);

        $pdf->setFillColor(230,230,230);

        $pdf->Ln(15);
        $pdf->Cell(50	,7,'ID',1,0,'C',1);
        $pdf->Cell(50	,7,'Job Name',1,0,'C',1);
        $pdf->Cell(45	,7,'Job Code ',1,0,'C',1);
        $pdf->Cell(45	,7,'Department',1,0,'C',1);

        /*-----------------------  change header -----------------------------*/
        $headrs_name = array(
            array("ID",50),
            array("Job Name",50),
            array("Job Code ",45),
            array("Department",45),

        );
        $pdf->set_header_names($headrs_name);
        /*-------------------------------------------------------*/
        $pdf->SetFontSize(10);

        /*set the table header */
        $pdf->setHeaderTitle(true);

        /*------------------------------*/
        $pdf->SetWidths(array(50,50,45,45));
        $pdf->Ln(7);

        foreach ($query as $val)
        {

            $pdf->SetFont('Arial','B',12);

            $pdf->Row(array($val->job_id,$val->job_name,$val->job_short_code,$val->dep_name));

        }


        $pdf->Ln(5);
        $pdf->SetX( 40);

        /*remove the table header */
        $pdf->setHeaderTitle(false);
        /*---------------------------------------*/





        $pdf->Output();
        exit;


    }




    //----------------------------report-----------------------------//

    public function get_jobs_rpt(Request $request)

    {

        $job = Job::where('job_id',$request->id)->first();
        $dep = Department::where('dep_id',$job->department_id)->first()->dep_name;




        $pdf = new PDF_HF('P','mm','A4');

        $pdf->AddPage();

        $pdf->AliasNbPages('{pages}');

//set font to arial, bold, 14pt

        $pdf->SetFont('Arial','B',12);

//Cell(width , height , text , border , end line , [align] )
        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX( 90);

        $pdf->Cell(40,10,"Jobs Report :" .''.$job->job_name,0,0,'C');
        $pdf->Ln(11);



        $pdf->Ln(15);
        $pdf->SetX( 10);
        $pdf->SetFont('','B');

        $pdf->Cell(15,10,'Job Name',0,0,'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( 60);
        $pdf->SetFont('','');
        $pdf->Cell(50,8,$job->job_name,0,0,'L');
        $pdf->SetX(60);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');

        $pdf->Ln(15);
        $pdf->SetX( 10);
        $pdf->SetFont('','B');

        $pdf->Cell(15,10,'Job Code',0,0,'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( 60);
        $pdf->SetFont('','');
        $pdf->Cell(50,8,$job->job_short_code,0,0,'L');
        $pdf->SetX(60);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');

        $pdf->Ln(15);
        $pdf->SetX( 10);
        $pdf->SetFont('','B');

        $pdf->Cell(15,10,'Department',0,0,'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( 60);
        $pdf->SetFont('','');
        $pdf->Cell(50,8,$dep,0,0,'L');
        $pdf->SetX(60);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');





        $pdf->Output();
        exit;


    }


}