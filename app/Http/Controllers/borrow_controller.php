<?php

namespace App\Http\Controllers;


use App\Borrowing;
use App\Borrowing_account;
use App\User;
use App\Leave_type;
use App\Leave_request;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;


class borrow_controller extends Controller

{
    //
    /* public function ret_cost_page()
     {
         return view('admin_pages.cost_center_page');
     }*/



    public function borrow_page()
    {



        $emploee = DB::table('employee')
            ->join('departments','employee.depart_id','departments.dep_id')
            ->where('employee.depart_id','<>',null)
            ->get();
        $currencies = DB::table('currencies')->orderBy('Currency_name', 'desc')
            ->pluck('Currency_name', 'Currency_id');

        $deduction = DB::table('allowence_deduction')->orderBy('allow_id', 'asc')
            ->where('allow_type','=',1)
            ->pluck('allow_name', 'allow_id');

        $acadimic_year = DB::table('acadimic_year')
            ->pluck('year_name', 'acadimic_id');
        /*$months = DB::table('months')->orderBy('month_id', 'asc')
            ->pluck('month_name', 'month_id');*/

        return view('school_pages.borrow',compact('emploee','currencies','deduction','acadimic_year'));
    }


    public function get_months_drop_down(Request $request)
    {
        $selected_v_ids = array();
        $cur_selected = $request->current_selected_value;

        if($request->all_selected_months != "") {
            foreach ($request->all_selected_months as $single_select) {
                array_push($selected_v_ids, $single_select);

            }



        if (count($selected_v_ids) > 0) {
            $months = DB::table('months')
                ->orderBy('month_id', 'asc')
                ->where(function ($query) use ($selected_v_ids) {
                    for ($h = 0; $h < count($selected_v_ids); $h++) {
                        $query->where('month_id', '<>', $selected_v_ids[$h]);
                    }
                })
                ->get();


            if($request->current_selected_value && $request->current_selected_value != "" )
            {
                $selected_months = DB::table('months')
                    ->orderBy('month_id', 'asc')
                    ->where(function ($query) use ($selected_v_ids,$cur_selected) {
                        for ($h = 0; $h < count($selected_v_ids); $h++) {
                            if($selected_v_ids[$h] == $cur_selected)
                            {
                                $query->where('month_id', '=', $selected_v_ids[$h]);
                            }

                        }
                    })
                    ->get();
            }
            else
            {
                $selected_months="";
            }


        }
        }else {
            $months = DB::table('months')
                ->orderBy('month_id', 'asc')
                ->get();


            if($request->current_selected_value && $request->current_selected_value != "" && count($selected_v_ids)>0)
            {
                $selected_months = DB::table('months')
                    ->orderBy('month_id', 'asc')
                    ->where(function ($query) use ($selected_v_ids,$cur_selected) {
                        for ($h = 0; $h < count($selected_v_ids); $h++) {
                            if($selected_v_ids[$h] == $cur_selected)
                            {
                                $query->where('month_id', '=', $selected_v_ids[$h]);
                            }

                        }
                    })
                    ->get();
            }
            else
            {
                $selected_months="";
            }
        }

        $data['data'] = $months;
        $data['extra_v'] = $selected_months;


        return response()->json($data);


    }

    public function save_borrow(Request $request)
    {


        $rules = array(

            'Name' => 'required',
            'Value' => 'required|Numeric',
            'Currency' => 'required',
            'Date' => 'required|Date',
            //'Deduction' => 'required_if:paid,==,0',
            /*'acadimic_year'=>'required_if:deduction_type,',
            'deliver_date'=>'required_if:deduction_type,'*/


        );
        $customMessages = [
            'required_if' => 'This field is required.'
        ];
        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), $rules,$customMessages);


        if ($validator->passes()) {

            /*-----------------------------------------  edit inventory -------------------------------------------------------------------*/
            if ($request->current_po_id != "" && $request->current_po_id) {

                $master_po = Borrowing::findOrFail($request->current_po_id);
                //$master_po->create_by_user_id = $request->cur_user_id;
                $master_po->employe_id = $request->Name;
                $master_po->value = $request->Value;
                $master_po->currency_id = $request->Currency;
                $master_po->rate = $request->rate_value;
                $master_po->date = $request->Date;

                //$master_po->requested_by_id	 = $request->cur_user_id;



                $curr_date = Carbon::now('Africa/Cairo')->addHour();

                $curr_date_only = Carbon::now('Africa/Cairo')->toDateString();

                $curr_time = Carbon::now('Africa/Cairo')->addHour()->toTimeString();


                //$master_po->requested_date	= $curr_date_only;
                $status = DB::table('document_status')->where('status_name', '=', $request->status)->first();
                $master_po->borrow_status = $status->status_id;

                if ($request->status == 'approved_po') {

                    //$curr_date = date("Y-m-d H:i:s");
                    if( !$master_po->approved_date || $master_po->approved_date == null)
                    {
                        $master_po->approved_date = $curr_date;
                    }

                    if( !$master_po->approved_by || $master_po->approved_by == null)
                    {
                        $master_po->approved_by = $request->cur_user_id;
                    }

                }


                $master_po->save();

                $master_po_id = $master_po->borrow_id;



                if($request->deduction_type && $request->acadimic_year && $request->deliver_date && $request->status == 'approved_po' )
                {

                    DB::table('borrowing_account')
                        ->where('borrow_id', $master_po_id)
                        ->where('paid','=',0)
                        ->delete();

                    $master_po = Borrowing::findOrFail($master_po_id);
                    $master_po->deliver_date = $request->deliver_date;
                    $master_po->delever_by = $request->cur_user_id;
                    $master_po->deduction_id = $request->deduction_type;
                    $master_po->accadimic_year_id = $request->acadimic_year;
                    $master_po->save();

                    if ($request->table_rows != "")
                    {
                        foreach ($request->table_rows as $row)
                        {
                            $po_details = new Borrowing_account();
                            $po_details->month_id = $row['month_id'];
                            $po_details->borrow_id = $master_po_id;
                            $po_details->paid = 0;
                            $po_details->value_ber_month = $row['borrow_value'];
                            $po_details->save();

                        }
                    }

                }





                return response([$request, 'edit_po', 'current_po_id' => $master_po_id, 'status' => $request->status]);
            } else {


                /*-----------------------------------------  add new  -------------------------------------------------------------------*/
                $master_po = new Borrowing();

                $master_po->employe_id = $request->Name;
                $master_po->value = $request->Value;
                $master_po->currency_id = $request->Currency;
                $master_po->rate = $request->rate_value;
                $master_po->date = $request->Date;

                $master_po->requested_by_id	 = $request->cur_user_id;



                $curr_date = Carbon::now('Africa/Cairo')->addHour();

                $curr_date_only = Carbon::now('Africa/Cairo')->toDateString();

                $curr_time = Carbon::now('Africa/Cairo')->addHour()->toTimeString();


                $master_po->requested_date	= $curr_date_only;

                /*$master_po->doc_date = date('Y-m-d', strtotime($request->document_date)) .' '. $curr_time;*/
                $status = DB::table('document_status')->where('status_name', '=', $request->status)->first();
                $master_po->borrow_status = $status->status_id;

                if ($request->status == 'approved_po') {

                    $master_po->approved_by = $request->cur_user_id;
                    $master_po->approved_date = $curr_date;

                }

                $master_po->save();

                $master_po_id = $master_po->borrow_id;
                //$return_approved_date = $master_po->approved_date;

                /*if ($request->table_rows != "")
                {
                    foreach ($request->table_rows as $row)
                    {
                        $po_details = new Details_inventory();
                        $po_details->doc_id = $master_po_id;
                        $po_details->pro_id = $row['item_product'];
                        $po_details->qut = $row['quantity'];
                        $po_details->po_id = $row['po_id'];
                        $po_details->save();

                    }
                }*/

                /*$user->roles()->attach($data['role']);*/
                return response([$request, 'current_po_id' => $master_po_id, 'status' => $request->status]);

            }
        } elseif ($validator->fails()) {
            /* $messages = $validator->messages();

             foreach ($rules as $key => $value)
             {
                 $verrors[$key] = $messages->first($key);
             }*/
            return response()->json(['error' => $validator->messages()]);
            /* return Response::json(array(
                 'success' => false,
                 'errors' => $validator->getMessageBag()->toArray()

             ), 400);*/ // 400 being the HTTP code for an invalid request.
            //   return response($verrors);
        }

    }


    public function delete_borrow(Request $request)
    {

        $master_po = Borrowing::findOrFail($request->cur_po_id);

        $master_po->delete();

        return response($request);


    }

    public function edit_borrow($id)
    {
        $emploee = DB::table('employee')
            ->join('departments','employee.depart_id','departments.dep_id')
            ->where('employee.depart_id','<>',null)
            ->get();
        $currencies = DB::table('currencies')->orderBy('Currency_name', 'desc')
            ->pluck('Currency_name', 'Currency_id');

        $deduction = DB::table('allowence_deduction')->orderBy('allow_id', 'asc')
            ->where('allow_type','=',1)
            ->pluck('allow_name', 'allow_id');


        $acadimic_year = DB::table('acadimic_year')
            ->pluck('year_name', 'acadimic_id');

        $master_borrow = DB::table('borrowing')
            ->join('document_status','borrowing.borrow_status','=','document_status.status_id')
            ->where('borrowing.borrow_id','=',$id)
            ->orderBy('borrowing.borrow_id','desc')
            ->first();

        $borrow_details = DB::table('borrowing_account')
            ->join('months','borrowing_account.month_id','=','months.month_id')
            ->where('borrowing_account.borrow_id','=',$id)
            ->orderBy('borrowing_account.acc_id','desc')
            ->get();


        return view('school_pages.borrow',compact('emploee','currencies','deduction','master_borrow','borrow_details','acadimic_year'));

    }


    public function get_table_show_borrow()
    {
        $showed_search_list = array('ID','Employee Name','Value','Currency','Created BY','Status');
        $database_search_list = array(1,2,3,4,5,6);

        $borrows = DB::table('borrowing')
            ->join('currencies','borrowing.currency_id','=','currencies.Currency_id')
            ->join('document_status','borrowing.borrow_status','=','document_status.status_id')

            ->join('employee as employee_name_borrowing', 'borrowing.employe_id','=','employee_name_borrowing.employee_id')
            ->join('employee as employee_name_creator', 'borrowing.requested_by_id','=','employee_name_creator.employee_user_id')
            ->select('*','employee_name_borrowing.employee_name as employee_borrowing_name', 'employee_name_creator.employee_name as employee_creator_name')

            ->orderBy('borrowing.borrow_id','desc')
            ->get();



        return view('school_pages.show_borrow',['showed_search_list' => $showed_search_list,'database_search_list'=>$database_search_list],compact('borrows'));
    }


    public function search_borrow_table(Request $request)
    {

        $search_colums=array();
        $book=array();

        $string = $request->sudent_data;

        foreach ($string as $value) {

            list($k, $v) = explode('|', $value);
            if($v == "")
            {
                array_push($search_colums,"all");
                array_push($book,$k);
            }
            else{
                array_push($search_colums,$v);
                array_push($book,$k);

            }

        }

        //$search_colums = array('s_name','s_class','s_age');

        $showed_colums = array('ID','Employee Name','Value','Currency','Created BY','Status');
        $colums_type = array('input','input','input','input','input','input');
        $table_colums = array('borrow_id','employee_borrowing_name','value','Currency_name','employee_creator_name','status_short_name');
        $table_colums_search_exception = array('borrow_id','employee_name_borrowing.employee_name','value','Currency_name','employee_name_creator.employee_name','status_short_name');
        //$book = $request->sudent_data;

        if(count($book) >0 && $book[0] != "all")
        {
            $name = DB::table('borrowing')
                ->join('currencies','borrowing.currency_id','=','currencies.Currency_id')
                ->join('document_status','borrowing.borrow_status','=','document_status.status_id')

                ->join('employee as employee_name_borrowing', 'borrowing.employe_id','=','employee_name_borrowing.employee_id')
                ->join('employee as employee_name_creator', 'borrowing.requested_by_id','=','employee_name_creator.employee_user_id')

                ->select('*','employee_name_borrowing.employee_name as employee_borrowing_name', 'employee_name_creator.employee_name as employee_creator_name')



                ->Where(function ($query) use($book,$search_colums ,$table_colums,$table_colums_search_exception) {
                    /*for ($i = 0; $i < count($book); $i++){
                        for ($h = 0; $h < count($search_colums); $h++){
                            $query->orwhere($search_colums[$h], 'like',  '%' . $book[$i] .'%');
                        }
                    }*/
                    for ($i = 0; $i < count($book); $i++){
                        if($search_colums[$i] == "all")
                        {
                            $table_colums_search_exception;
                            for ($h = 0; $h < count($table_colums_search_exception); $h++){
                                $query->orwhere($table_colums_search_exception[$h], 'like',  '%' . $book[$i] .'%');
                            }

                        }
                        else
                        {
                            $query->orwhere($search_colums[$i], 'like',  '%' . $book[$i] .'%');
                        }

                    }
                })

                ->orderBy('borrowing.borrow_id','desc')
                ->get();

        }
        else
        {

            $name = DB::table('borrowing')
                ->join('currencies','borrowing.currency_id','=','currencies.Currency_id')
                ->join('document_status','borrowing.borrow_status','=','document_status.status_id')

                ->join('employee as employee_name_borrowing', 'borrowing.employe_id','=','employee_name_borrowing.employee_id')
                ->join('employee as employee_name_creator', 'borrowing.requested_by_id','=','employee_name_creator.employee_user_id')
                ->select('*','employee_name_borrowing.employee_name as employee_borrowing_name', 'employee_name_creator.employee_name as employee_creator_name')

                ->orderBy('borrowing.borrow_id','desc')
                ->get();
        }
        return response(["name"=>$name ,"colums"=> $table_colums,"showed_colums"=>$showed_colums,"book"=>$book,"searched_colums"=>$search_colums,"colums_type"=>$colums_type]);
    }


    public function delete_all_selected_borrow(Request $request)
    {
        $deleted = array();
        foreach ($request->deleted_user_ids as $class_id_one)
        {
            $user = Borrowing::findOrFail($class_id_one);
            if($user->borrow_status == 1)
            {
                $user->delete();
                array_push($deleted,1);

            }
            else
            {
                array_push($deleted,0);
            }
            /* elseif($user->po_status_id == 2)
             {
                 $user->po_canceled = 1;
                 $user->po_status_id = 5;
                 $user->save();
                 array_push($deleted,0);
             }*/

        }

        return response(['deleted_ids'=>$request->deleted_user_ids,'deleted'=>$deleted]);
    }
}

