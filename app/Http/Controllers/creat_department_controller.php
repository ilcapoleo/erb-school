<?php

namespace App\Http\Controllers;


use App\User;
use App\Department;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;




class creat_department_controller extends Controller
{
    //
    /* public function ret_cost_page()
     {
         return view('admin_pages.cost_center_page');
     }*/
    public function creat_department()
    {

        $cost_center = DB::table('cost_centers')->orderBy('Cost_name', 'asc')
            ->pluck('Cost_name', 'Cost_id');

        $managers = DB::table('employee')->orderBy('employee_id', 'desc')
            ->pluck('employee_name', 'employee_id');


        return view('school_pages.department',compact('cost_center','managers'));
    }


    public function add_new_department(Request $request)
    {
        $rules = array(

            'department_name' => 'required',
            'department_code' => 'required',
            'cost_center' => 'required',
            /*'manager' => 'required',*/

        );
        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), $rules);

        if ($validator->passes()) {

            /*------------- edit -----------------*/
            if ($request->current_po_id != "" && $request->current_po_id) {

                $department = Department::findOrFail($request->current_po_id);
                $department->dep_name = $request->department_name;
                $department->dep_short_code = $request->department_code;
                $department->cost_center_id = $request->cost_center;
                /*$department->manager_id = $request->manager;*/
                $department->save();

                $depart_id = $department->dep_id;

                return response([$request, 'edit_po', 'current_po_id' => $depart_id]);
            } /*----------------- create new  ---------------*/
            else {
                $department = new Department();
                $department->dep_name = $request->department_name;
                $department->dep_short_code = $request->department_code;
                $department->cost_center_id = $request->cost_center;
                /*$department->manager_id = $request->manager;*/
                $department->save();

                $depart_id = $department->dep_id;

                return response([$request, 'edit_po', 'current_po_id' => $depart_id]);
            }
        }


        elseif ($validator->fails()) {

            return response()->json(['error' => $validator->messages()]);

        }


    }



    public function delete_this_department(Request $request)
    {


        $department = Department::findOrFail($request->cur_po_id);
        $department->delete();

        return response($request);


    }

    public function delete_all_selected_dep(Request $request)
    {
        foreach ($request->deleted_user_ids as $class_id_one)
        {
            $user = Department::findOrFail($class_id_one);

            $user->delete();

        }

        return response(['deleted_ids'=>$request->deleted_user_ids]);
    }

    public function get_this_department($id)
    {
        $cost_center = DB::table('cost_centers')->orderBy('Cost_name', 'asc')
            ->pluck('Cost_name', 'Cost_id');

        /*$managers = DB::table('employee')->orderBy('employee_id', 'desc')
            ->pluck('employee_name', 'employee_id');*/

        $department = Department::findOrFail($id);


        return view('school_pages.department',compact('cost_center','department'));

    }

    public function get_table_departments()
    {
        $showed_search_list = array('ID','Department Name','Department Code','Cost Name');
        $database_search_list = array(1,2,3,4);

        $departments = DB::table('departments')
            ->join('cost_centers','departments.cost_center_id','=','cost_centers.Cost_id')

            ->orderBy('departments.dep_id','desc')
            ->get();

        return view('school_pages.show_departments',['showed_search_list' => $showed_search_list,'database_search_list'=>$database_search_list],compact('departments'));
    }

    public function search_departments_table(Request $request)
    {

        $search_colums=array();
        $book=array();

        $string = $request->sudent_data;

        foreach ($string as $value) {

            list($k, $v) = explode('|', $value);
            if($v == "")
            {
                array_push($search_colums,"all");
                array_push($book,$k);
            }
            else{
                array_push($search_colums,$v);
                array_push($book,$k);

            }

        }

        //$search_colums = array('s_name','s_class','s_age');

        $showed_colums = array('ID','Department Name','Department Code','Cost Name','Manager');
        $colums_type = array('input','input','input','input','input','input');
        $table_colums = array('dep_id','dep_name','dep_short_code','Cost_name','employee_name');

        //$book = $request->sudent_data;

        if(count($book) >0 && $book[0] != "all")
        {
            $name = DB::table('departments')
                ->join('cost_centers','departments.cost_center_id','=','cost_centers.Cost_id')
                ->join('employee','departments.manager_id','=','employee.employee_user_id')





                ->Where(function ($query) use($book,$search_colums ,$table_colums) {
                    /*for ($i = 0; $i < count($book); $i++){
                        for ($h = 0; $h < count($search_colums); $h++){
                            $query->orwhere($search_colums[$h], 'like',  '%' . $book[$i] .'%');
                        }
                    }*/
                    for ($i = 0; $i < count($book); $i++){
                        if($search_colums[$i] == "all")
                        {
                            $table_colums;
                            for ($h = 0; $h < count($table_colums); $h++){
                                $query->orwhere($table_colums[$h], 'like',  '%' . $book[$i] .'%');
                            }

                        }
                        else
                        {
                            $query->orwhere($search_colums[$i], 'like',  '%' . $book[$i] .'%');
                        }

                    }
                })
                ->orderBy('departments.dep_id','desc')
                ->get();

        }
        else
        {

            $name = DB::table('departments')
                ->join('cost_centers','departments.cost_center_id','=','cost_centers.Cost_id')
                ->join('employee','departments.manager_id','=','employee.employee_user_id')

                ->orderBy('departments.dep_id','desc')
                ->get();
        }
        return response(["name"=>$name ,"colums"=> $table_colums,"showed_colums"=>$showed_colums,"book"=>$book,"searched_colums"=>$search_colums,"colums_type"=>$colums_type]);
    }

}
