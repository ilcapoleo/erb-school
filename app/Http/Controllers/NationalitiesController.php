<?php

namespace App\Http\Controllers;

use App\Nationality;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use function Sodium\compare;

class NationalitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $showed_search_list = array('Nationality Name');
        $database_search_list = array(1);

        $countries = DB::table('nationalities')
            ->orderBy('id','desc')
            ->get();

        return view('school_pages.nationalities.index',['showed_search_list' => $showed_search_list,'database_search_list'=>$database_search_list],compact('countries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('school_pages.nationalities.nationalities');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $nationality = Nationality::find($id);
        return view('school_pages.nationalities.nationalities',compact('nationality'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    /*-----------------  delete countries from table  ------------------*/
    public function delete_all_selected_nationalities(Request $request)
    {
        $deleted = array();
        foreach ($request->deleted_user_ids as $class_id_one)
        {
            $user = \App\Nationality::findOrFail($class_id_one);

            $user->delete();
            array_push($deleted,1);


        }

        return response(['deleted_ids'=>$request->deleted_user_ids,'deleted'=>$deleted]);
    }



    public function add_new_nationality(Request $request)
    {

        $rules = array(

            'nationality_name' => ['required','unique:nationalities,name,'.$request->current_po_id.',id'],



        );
        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), $rules);

        if ($validator->passes()) {

            /*------------- edit -----------------*/
            if ($request->current_po_id != "" && $request->current_po_id) {

                $department = Nationality::findOrFail($request->current_po_id);
                $department->name = $request->nationality_name;

                $department->save();

                $depart_id = $department->id;

                return response([$request, 'edit_po', 'current_po_id' => $depart_id]);
            } /*----------------- create new  ---------------*/
            else {
                $department = new Nationality();

                $department->name = $request->nationality_name;

                $department->save();

                $depart_id = $department->id;


                return response([$request, 'edit_po', 'current_po_id' => $depart_id]);
            }
        }


        elseif ($validator->fails()) {

            return response()->json(['error' => $validator->messages()]);

        }
    }

    public function delete_this_nationality(Request $request)
    {


        $job = Nationality::findOrFail($request->cur_po_id);
        $job->delete();

        return response($request);


    }
}
