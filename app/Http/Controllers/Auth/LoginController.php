<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;


    /*--- add more than one field check --*/


    /**
     * Where to redirect users after login.
     *
     * @var string
     *
     */
    protected $redirectTo = '/change_password';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /*custumize login */

    public function credentials(Request $request)
    {
        //return $request->only($this->username(), 'password','id');
        return $request->only($this->username(), 'password');
    }

    public function username()
    {
        return 'name';
    }


}
