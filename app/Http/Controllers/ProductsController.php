<?php

namespace App\Http\Controllers;

use App\Account;
use App\Borrowing;
use App\Currency;
use App\products;
use App\ProductType;
use App\supplier_product;
use App\Vendor;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $showed_search_list = array('ID','Product Name','Product Short Name','Product Type','Income Account');

        /*$database_search_list = array('SID','First_name','nationalty_type','Gender_type','code','code');*/
        $products = DB::table('products')
            ->leftjoin('products_type','products.pro_type_id','=','products_type.pro_type_id')
            ->leftjoin('accounts','products.account_id','=','accounts.child_id')
            ->orderBy('products.pro_id','desc')
            ->get();

        //dd($students);

        $database_search_list = array(1,2,3,4,5);

        return view('school_pages.products.index',['showed_search_list' => $showed_search_list,'database_search_list'=>$database_search_list],compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $products_type= ProductType::pluck('pro_type_name','pro_type_id');
        $accounts = Account::where('Level_NO','3')->pluck('Child_name','Child_id');
        $supplier = Vendor::all();
        $currency =  Currency::pluck('Currency_name','Currency_id');
        return view('school_pages.products.product',compact('products_type','accounts','supplier','currency'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(

            'product_name' => ['required','unique:products,pro_name,'.$request->current_po_id.',pro_id'],
            'product_short_name' => ['required','unique:products,product_short_name,'.$request->current_po_id.',pro_id'],
            'product_type' => 'required',
            'income_account' => 'required',
            'table_rows' => 'required',

        );

        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), $rules);
        if ($validator->passes()) {

            if ($request->current_po_id != "" && $request->current_po_id) {



                $product = products::findOrFail($request->current_po_id);
                $product->pro_name = $request->product_name;
                $product->product_short_name = $request->product_short_name;
                $product->pro_type_id = $request->product_type;
                $product->account_id = $request->income_account;

                $product->cost_price = 0.0;

                $product->save();

                $product_id = $product->pro_id;


                DB::table('supplier_product')->where('product_id', $product_id)->delete();

                if ($request->table_rows != "") {

                    foreach ($request->table_rows as $row) {

                        $document = new supplier_product();
                        $document->product_id = $product_id;

                        $document->supplier_id = $row['supplier'];
                        $document->cost_price = $row['cost_price'];
                        $document->currency_id = $row['currency'];

                        $document->save();

                    }
                }


                return response([$request, 'current_po_id' => $product_id]);
            }

            else{
                $product = new products();
                $product->pro_name = $request->product_name;
                $product->product_short_name = $request->product_short_name;
                $product->pro_type_id = $request->product_type;
                $product->account_id = $request->income_account;

                $product->cost_price = 0.0;

                $product->save();

                $product_id = $product->pro_id;


                if ($request->table_rows != "") {

                    foreach ($request->table_rows as $row) {

                        $document = new supplier_product();
                        $document->product_id = $product_id;

                        $document->supplier_id = $row['supplier'];
                        $document->cost_price = $row['cost_price'];
                        $document->currency_id = $row['currency'];

                        $document->save();

                    }
                }


                return response([$request, 'current_po_id' => $product_id]);
            }



        }

        elseif ($validator->fails()) {

            return response()->json(['error' => $validator->messages()]);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $products_type= ProductType::pluck('pro_type_name','pro_type_id');
        $accounts = Account::where('Level_NO','3')->pluck('Child_name','Child_id');
        $supplier = Vendor::all();
        $currency =  Currency::pluck('Currency_name','Currency_id');

        $product = DB::table('products')
            ->where('pro_id','=',$id)
            ->first();

        $product_details = DB::table('supplier_product')
            ->join('vendors','supplier_product.supplier_id','=','vendors.VendorID')
            ->join('currencies','supplier_product.currency_id','=','currencies.Currency_id')

            ->where('supplier_product.product_id','=',$id)

            ->get();


        return view('school_pages.products.product',compact('products_type','accounts','supplier','currency','product','product_details'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {

        $master_po = products::findOrFail($request->cur_po_id);

        $master_po->delete();

        DB::table('supplier_product')->where('product_id', $request->cur_po_id)->delete();

        return response($request);
    }
}
