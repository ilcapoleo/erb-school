<?php

namespace App\Http\Controllers;


use App\SalaryScale;

use App\SubSalary;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use phpDocumentor\Reflection\Types\Compound;


class creat_salary_scale_controller extends Controller
{
    //
    /* public function ret_cost_page()
     {
         return view('admin_pages.cost_center_page');
     }*/
    public function saveData(Request $req)
    {

        $rules = array(

            'salary_name' => 'required',
            'code_name' => 'required',
            'cost_center' => 'required',
            'table_rows' => 'required',

        );
        $validator = \Illuminate\Support\Facades\Validator::make($req->all(), $rules);

        if ($validator->passes()) {

            /*------------- edit -----------------*/
            if ($req->current_po_id != "" && $req->current_po_id) {

                $department = SalaryScale::findOrFail($req->current_po_id);
                $department->salary_name = $req->salary_name;
                $department->code_name = $req->code_name;
                $department->cost_id = $req->cost_center;
                $department->save();


                $depart_id = $department->salary_id;
//                $sub= SubSalary::findOrFail($req->current_po_id);
//                $sub->sub_name = $req->sub_name;
//                $sub->sub_code = $req->sub_code;
//                $sub->save();
   DB::table('sub_salary')->where('salary_id',$depart_id)->delete();
                if ($req->table_rows != "") {
                    foreach ($req->table_rows as $row) {
                        $sub = new SubSalary();
                        $sub->salary_id = $depart_id;
                        $sub->sub_name = $row['sub_name'];
                        $sub->sub_code = $row['sub_code'];

                        $sub->save();




                    }
                }


                return response([$req, 'edit_po', 'current_po_id' => $depart_id]);
            } /*----------------- create new  ---------------*/
            else {
                $user = new SalaryScale();

                $user->salary_name = $req->salary_name;
                $user->code_name = $req->code_name;
                $user->cost_id = $req->cost_center;
                $user->save();
                $depart_id = $user->salary_id;
                if ($req->table_rows != "") {
                    foreach ($req->table_rows as $row) {
                        $sub = new SubSalary();
                        $sub->salary_id = $depart_id;
                        $sub->sub_name = $row['sub_name'];
                        $sub->sub_code = $row['sub_code'];

                        $sub->save();

                        $po_details_id = $sub->sub_id;



                    }
                }
            }
            /*

                $user_data=$user->scal_save($data);*/
            /*$user=SalaryScale::create(['salary_scalar_name' => 'Flight 10'],['short_code_name' => 'Flight 10']);*/
            return response([$req, 'edit_po', 'current_po_id' => $depart_id,'form' => $po_details_id]);
        }

        elseif ($validator->fails()) {

            return response()->json(['error' => $validator->messages()]);

        }


    }

    //
    /* public function ret_cost_page()
     {
         return view('admin_pages.cost_center_page');
     }*/
    public function delete_salary_scale(Request $req)
    {


        $department = SalaryScale::findOrFail($req->cur_po_id);
        $department->delete();

        return response($req);


    }

    public function get_this_salary($id)
    {


        $cost_center = DB::table('cost_centers')->orderBy('Cost_name', 'asc')
            ->pluck('Cost_name', 'Cost_id');

        $department = SalaryScale::findOrFail($id);
        $form =SubSalary::where('salary_id',$id)->get();


        return view('school_pages.salary_scale',compact('cost_center', 'department','form'));

    }


    public function salaryTable()
    {
        $showed_search_list = array('ID','salary Name','short Code','Cost Center');
        $database_search_list = array(1,2,3,4);

        $salarys = DB::table('salary_scale')
            ->join('cost_centers','salary_scale.cost_id','=','cost_centers.Cost_id')
//                ->join('employee','departments.manager_id','=','employee.employee_user_id')

            ->orderBy('salary_scale.salary_id','desc')
            ->get();

        return view('school_pages.show_salary',['showed_search_list' => $showed_search_list,'database_search_list'=>$database_search_list],compact('salarys'));
    }

    public function creat_salary_scale()
    {
        $cost_center = DB::table('cost_centers')->orderBy('Cost_name', 'asc')
            ->pluck('Cost_name', 'Cost_id');

        return view('school_pages.salary_scale',compact('cost_center'));
    }

    public function search_salary_table(Request $request)
    {

        $search_colums=array();
        $book=array();

        $string = $request->sudent_data;

        foreach ($string as $value) {

            list($k, $v) = explode('|', $value);
            if($v == "")
            {
                array_push($search_colums,"all");
                array_push($book,$k);
            }
            else{
                array_push($search_colums,$v);
                array_push($book,$k);

            }

        }

        //$search_colums = array('s_name','s_class','s_age');

        $showed_colums = array('ID','Salary Name','short Code','Cost center');
        $colums_type = array('input','input','input','input','input','input');
        $table_colums = array('salary_id','salary_name','code_name','Cost_name');

        //$book = $request->sudent_data;

        if(count($book) >0 && $book[0] != "all")
        {
            $name = DB::table('salary_scale')
                ->join('cost_centers','salary_scale.cost_id','=','cost_centers.Cost_id')
//                ->join('employee','departments.manager_id','=','employee.employee_user_id')





                ->Where(function ($query) use($book,$search_colums ,$table_colums) {
                    /*for ($i = 0; $i < count($book); $i++){
                        for ($h = 0; $h < count($search_colums); $h++){
                            $query->orwhere($search_colums[$h], 'like',  '%' . $book[$i] .'%');
                        }
                    }*/
                    for ($i = 0; $i < count($book); $i++){
                        if($search_colums[$i] == "all")
                        {
                            $table_colums;
                            for ($h = 0; $h < count($table_colums); $h++){
                                $query->orwhere($table_colums[$h], 'like',  '%' . $book[$i] .'%');
                            }

                        }
                        else
                        {
                            $query->orwhere($search_colums[$i], 'like',  '%' . $book[$i] .'%');
                        }

                    }
                })
                ->orderBy('salary_scale.salary_id','desc')
                ->get();

        }
        else
        {

            $name = DB::table('salary_scale')
                ->join('cost_centers','salary_scale.cost_id','=','cost_centers.Cost_id')
//                ->join('employee','departments.manager_id','=','employee.employee_user_id')

                ->orderBy('salary_scale.salary_id','desc')
                ->get();
        }
        return response(["name"=>$name ,"colums"=> $table_colums,"showed_colums"=>$showed_colums,"book"=>$book,"searched_colums"=>$search_colums,"colums_type"=>$colums_type]);
    }

    public function creat_working_days()
    {

        $salary_name = DB::table('salary_scale')->orderBy('salary_name', 'asc')
            ->pluck('salary_name', 'salary_id');
        return view('school_pages.working_days',compact('salary_name'));
    }

}




