<?php

namespace App\Http\Controllers;

use App\AcademicYear;
use App\Country;
use App\Department;
use App\Job;
use App\Nationality;
use Illuminate\Http\Request;

class AjaxController extends Controller
{
    public function DropDown (Request $request)
    {
        switch ($request->key) {

            case 'nation';

                $nationality = Nationality::all();

                return response()->json($nationality);
                break;
            case 'country';
                $country = Country::pluck('Country_Name', 'Country_ID');
                return response()->json($country);
                break;
            case 'academic_year2';
                $academic_year = AcademicYear::all();
                return response()->json($academic_year);
                break;
            case 'depart';
                $department = Department::all();
                return response()->json($department);
                break;
            case 'job';
                $job = Job::all();
                return response()->json($job);
                break;
            case 'salary_scale';
                $sub_salary = SubSalary::pluck('sub_name','sub_id');
                return response()->json($sub_salary);
                break;
            case 'class';
                $class = Classes::pluck('ClassName','classid');
                return response()->json($class);
                break;

        }

    }

    public function nation()
    {
        $nationality = Nationality::all();

        return response()->json($nationality);

    }
    public function country()
    {
        $country = Country::all();
        return response()->json($country);

    }
    public function depart()
    {
        $department = Department::all();
        return response()->json($department);

    }
    public function job()
    {
        $job = Job::all();
        return response()->json($job);

    }
    public function academic()
    {
        $academic_year = AcademicYear::all();
        return response()->json($academic_year);

    }

}
