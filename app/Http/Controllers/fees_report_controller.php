<?php

namespace App\Http\Controllers;

use App\Account;
use App\fees;
use App\Fees_type;
use App\Journal;
use Hamcrest\FeatureMatcher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class fees_report_controller extends Controller
{


    //-----------------------------table-------------------------//

    public function get_fees(Request $request)

    {




        $print_array = array();
        $array =  explode(',', $request->input('print_ids')[0]);

        foreach ($array as $single)
        {
            array_push($print_array, $single);
        }
        $query = DB::table('fees_head')
            ->join('fees_type','fees_head.Fees_type','=','fees_type.fee_id')
            ->join('accounts','fees_head.account_id','=','accounts.child_id')
            ->join('journals','fees_head.journal_id','=','journals.journal_id')
            ->wherein('fees_head.fees_id',$print_array)

            ->orderBy('fees_head.fees_id','desc')
            ->get();






        $pdf = new PDF_HF('P','mm','A4');

        $pdf->AddPage();

        $pdf->AliasNbPages('{pages}');





//set font to arial, bold, 14pt

        $pdf->SetFont('Arial','B',14);

//Cell(width , height , text , border , end line , [align] )
        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX( 90);

        $pdf->Cell(40,10,"Fees Head Table",0,0,'C');
        $pdf->Ln(11);

        $pdf->setFillColor(230,230,230);

        $pdf->Ln(15);
        $pdf->Cell(35	,7,'ID',1,0,'C',1);
        $pdf->Cell(40	,7,'Fees Name',1,0,'C',1);
        $pdf->Cell(40	,7,'Fees Type',1,0,'C',1);
        $pdf->Cell(35	,7,'Account ID',1,0,'C',1);
        $pdf->Cell(40	,7,'Journal ID',1,0,'C',1);

        /*-----------------------  change header -----------------------------*/
        $headrs_name = array(

            array("ID",35),
            array("Fees Name",40),
            array("Fees Type",40),
            array("Account ID",35),
            array("Journal ID",40),
        );
        $pdf->set_header_names($headrs_name);
        /*-------------------------------------------------------*/
        $pdf->SetFontSize(35,40,40,35,40);

        /*set the table header */
        $pdf->setHeaderTitle(true);

        /*------------------------------*/
        $pdf->SetWidths(array(35,40,40,35,40));
        $pdf->Ln(7);

        foreach ($query as $val)
        {

            $pdf->SetFont('Arial','B',12);

            $pdf->Row(array($val->fees_id,$val->Fees_Name,$val->fees_type,$val->child_name,$val->Journal_name));

        }


        $pdf->Ln(5);
        $pdf->SetX( 40);

        /*remove the table header */
        $pdf->setHeaderTitle(false);
        /*------------------------------*/

        $pdf->Output();
        exit;


    }


    //----------------------------report-----------------------------//


    public function get_fees_rpt(Request $request)

    {
        $depart = fees::where('fees_id',$request->id)->first();



        $manager = Account::where('child_id',$depart->account_id)->first()->child_name;

        $Fees = Fees_type::where('fee_id',$depart->Fees_type)->first()->fees_type;

        $cost =Journal::where('journal_id',$depart->journal_id)->first()->Journal_name;

        $pdf = new PDF_HF('P','mm','A4');

        $pdf->AddPage();

        $pdf->AliasNbPages('{pages}');

//set font to arial, bold, 14pt

        $pdf->SetFont('Arial','B',12);

//Cell(width , height , text , border , end line , [align] )
        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX( 90);

        $pdf->Cell(40,10,"Fees Report for:".' '.$depart->Fees_Name.' '."Fees",0,0,'C');
        $pdf->Ln(11);

        $pdf->Ln(15);
        $pdf->SetX( 10);
        $pdf->Cell(15,10,'Fees Name',0,0,'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( 60);
        $pdf->SetFont('','');
        $pdf->Cell(50,8,$depart->Fees_Name,0,0,'L');
        $pdf->SetX(60);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');

        $pdf->Ln(15);
        $pdf->SetX( 10);
        $pdf->SetFont('','B');
        $pdf->Cell(15,10,'Fees type',0,0,'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( 60);
        $pdf->SetFont('','');
        $pdf->Cell(50,8,$Fees,0,0,'L');
        $pdf->SetX(60);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');

        $pdf->Ln(15);
        $pdf->SetX( 10);
        $pdf->SetFont('','B');
        $pdf->Cell(15,10,'Journal',0,0,'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( 60);
        $pdf->SetFont('','');
        $pdf->Cell(50,8,$manager,0,0,'L');
        $pdf->SetX(60);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');

        $pdf->Ln(15);
        $pdf->SetX( 10);
        $pdf->SetFont('','B');
        $pdf->Cell(15,10,'Acount',0,0,'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( 60);
        $pdf->SetFont('','');
        $pdf->Cell(50,8,$cost,0,0,'L');
        $pdf->SetX(60);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');




        $pdf->Output();
        exit;


    }

}
