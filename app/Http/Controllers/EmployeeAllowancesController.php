<?php

namespace App\Http\Controllers;

use App\AcademicYear;
use App\Allowence_deduction;
use App\Employee;
use App\EmployeeAllowence;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class EmployeeAllowancesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $showed_search_list = array('Employee Name','Academic Year');
        $database_search_list = array(1,2);

        $salary_plans = DB::table('employee_allowences')
            ->join('employee','employee_allowences.employee_id','employee.employee_id')
            ->join('acadimic_year','employee_allowences.academic_year_id','acadimic_year.acadimic_id')
            ->select('year_name','employee_name','middle_name','last_name','family_name','employee.employee_id','acadimic_id')
            ->groupBy('year_name','employee_name','middle_name','last_name','family_name','employee.employee_id','acadimic_id')
            ->get();

        //
        return view('school_pages.employees_allowance.index',['showed_search_list' => $showed_search_list,'database_search_list'=>$database_search_list],compact('salary_plans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $employees = Employee::all();
        $academic_year_table = AcademicYear::all();
        $allows = Allowence_deduction::where('allow_type', 0)->pluck('allow_name', 'allow_id');
        $employee_allowances = EmployeeAllowence::select('academic_year_id', 'allow_id', 'from_date', 'to_date', 'value', 'employee_id')->get();
        return view('school_pages.employees_allowance.create', compact('employee_allowances', 'employees', 'academic_year_table', 'allows'));


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        return response($request);
        $rules = [
            'academic_year_id' => 'required',
            'allow_id' => 'required',
            'employee_id' => 'required',
            'from_date' => 'required',
            'to_date' => 'required',
            'value' => 'required',

        ];

        $validator = Validator::make($request->all(), $rules);
        $validator->SetAttributeNames([
            'academic_year_id' => 'Select Academic Year',
            'allow_id' => 'Allowance ',
            'employee_id' => 'SelectEmployee ',
            'from_date' => 'Allowance From Date ',
            'to_date' => 'Allowance To Date',
            'value' => 'Allowance Value',

        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()]);

        } else {
            try {
                DB::beginTransaction();
                if ($request->id != "" && $request->id) {

                    $allow = EmployeeAllowence::findorfail($request->id);
                } else {

                    $allow = new EmployeeAllowence();
                }

                $allow->academic_year_id = $request->academic_year_id;
                $allow->allow_id = $request->allow_id;
                $allow->employee_id = $request->employee_id;
                $allow->from_date = $request->from_date;
                $allow->to_date = $request->to_date;
                $allow->value = $request->value;
                $allow->save();
                return response(['employee_allowance_id' => $allow->id]);
                DB::commit();
                session()->flash('success', 'created');
            } catch (Exception $ex) {
                DB::rollBack();
                session()->flash('error', 'failed');
                return response(['error' => 'failed']);
            }
            return response(['employee_allowance_id' => $allow->id]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

        /*$selected_employee_allowence = EmployeeAllowence::find($id)->get();
        dd($selected_employee_allowence);

        $employees = Employee::all();
        $academic_year_table = AcademicYear::all();
        $allows = Allowence_deduction::where('allow_type', 0)->pluck('allow_name', 'allow_id');
        $employee_allowances = EmployeeAllowence::select('academic_year_id', 'allow_id', 'from_date', 'to_date', 'value', 'employee_id')->get();
        return view('school_pages.employees_allowance.create', compact('employee_allowances', 'employees', 'academic_year_table', 'allows'));*/
    }

    public function show_employee_allowence($emp_id,$year_id)
    {
        //

        $selected_employee_allowence = DB::table('employee_allowences')
            ->join('allowence_deduction','employee_allowences.allow_id','=','allowence_deduction.allow_id')
            ->where('employee_id','=',$emp_id)
            ->where('academic_year_id','=',$year_id)
            ->get();
        $showen_emp_id = $emp_id;
        $showen_year_id = $year_id;

        //dd($selected_employee_allowence);

        $employees = Employee::all();
        /*dd($employees);*/
        $academic_year_table = AcademicYear::all();
        $allows = Allowence_deduction::where('allow_type', 0)->pluck('allow_name', 'allow_id');
        $employee_allowances = EmployeeAllowence::select('academic_year_id', 'allow_id', 'from_date', 'to_date', 'value', 'employee_id')->get();
        return view('school_pages.employees_allowance.create', compact('employee_allowances', 'employees', 'academic_year_table', 'allows','selected_employee_allowence','showen_emp_id','showen_year_id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $rules = [
            'id' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);
        $validator->SetAttributeNames([
            'id' => 'ID Please',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()]);

        } else {
            $emp_delete = EmployeeAllowence::findOrFail($request->id);
            if ($emp_delete->is_closed == "0") {
                $emp_delete->delete();
                return response(['deleted' => $request]);
            } else {
                return response(['Not Valid' => $request]);
            }
        }
    }

    public function getAllowTable(Request $request)
    {
//        return response($request);

        $rules = [
            'Acadimic_year' => 'required',
            'employee' => 'required',

        ];

        $validator = Validator::make($request->all(), $rules);
        $validator->SetAttributeNames([
            'Acadimic_year' => 'Academic Year',
            'employee' => 'Employee ',

        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()]);

        } else {

            $employee_allowances = EmployeeAllowence::where('employee_id', $request->employee)
                ->where('academic_year_id', $request->Acadimic_year)->join('allowence_deduction','employee_allowences.allow_id','=','allowence_deduction.allow_id')->get();

            return response(['employee_allowances'=>$employee_allowances]);
        }
    }


}
