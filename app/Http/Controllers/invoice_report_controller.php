<?php

namespace App\Http\Controllers;

use App\Employee;
use App\inventory_master;
use App\invoice_master;
use App\Invoice_tax;
use App\po;
use App\po_report;
use App\products;
use App\Supplier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\PDF_HF;


class invoice_report_controller extends Controller
{

    public function get_invoice(Request $request)

    {



        $print_array = array();
        $array =  explode(',', $request->input('print_ids')[0]);

        foreach ($array as $single)
        {
            array_push($print_array, $single);
        }

        $query = DB::table('invoice_master')
            ->join('currencies','invoice_master.inv_currency_id','=','currencies.Currency_id')
            ->join('inventory_master','invoice_master.inventory_m_id','=','inventory_master.doc_id')
            ->join('inventory_details','inventory_master.doc_id','=','inventory_details.doc_id')
            ->join('vendors','inventory_master.supplier_id','=','vendors.VendorID')
            ->join('employee','invoice_master.invoice_creator_id','=','employee.employee_id')
            ->join('document_status','invoice_master.invoice_doc_status','=','document_status.status_id')
            ->whereIn('invoice_master.invoice_m_id',$print_array)
            ->select('Currency_name','invoice_m_id','invoice_seq','invoice_date','VendorName','employee_name','status_short_name', DB::raw('SUM(qut*unit_price) As total'))
            ->groupBy('Currency_name','invoice_m_id','invoice_seq','invoice_date','VendorName','employee_name','status_short_name')

            ->orderBy('invoice_master.invoice_m_id','desc')
            ->get();



        $pdf = new PDF_HF('P','mm','A4');

        $pdf->AddPage();

        $pdf->AliasNbPages('{pages}');

//set font to arial, bold, 14pt

        $pdf->SetFont('Arial','B',14);

//Cell(width , height , text , border , end line , [align] )
        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX( 90);

        $pdf->Cell(40,10,"Invoice Table",0,0,'C');
        $pdf->Ln(11);

        $pdf->setFillColor(230,230,230);

        $pdf->Ln(15);
        $pdf->Cell(20	,7,'ID',1,0,'C',1);
        $pdf->Cell(30	,7,'Invoice Date',1,0,'C',1);
        $pdf->Cell(40	,7,'Supplier Name',1,0,'C',1);
        $pdf->Cell(35	,7,'Currency',1,0,'C',1);
        $pdf->Cell(30	,7,'Status',1,0,'C',1);
        $pdf->Cell(35	,7,'Creator Name',1,0,'C',1);

        /*-----------------------  change header -----------------------------*/
        $headrs_name = array(
            array("ID",20),
            array("Invoice Date",30),
            array("Supplier Name",40),
            array("Currency",35),
            array("Status",30),
            array("Creator Name",35),

        );
        $pdf->set_header_names($headrs_name);
        /*-------------------------------------------------------*/
        $pdf->SetFontSize(10);

        /*set the table header */
        $pdf->setHeaderTitle(true);

        /*------------------------------*/
        $pdf->SetWidths(array(20,30,40,35,30,35));
        $pdf->Ln(7);

        foreach ($query as $val)
        {

            $pdf->SetFont('Arial','B',12);

            $pdf->Row(array($val->invoice_seq,$val->invoice_date,$val->VendorName,$val->Currency_name,$val->status_short_name,$val->employee_name));

        }


        $pdf->Ln(5);
        $pdf->SetX( 40);

        /*remove the table header */
        $pdf->setHeaderTitle(false);
        /*------------------------------*/

        $pdf->Output();
        exit;


    }

    public function array_push_assoc($array, $key, $value){

        if(array_key_exists($key,$array))
        {
            $array[$key] += $value;

        }
        else{
            $array[$key] = $value;
        }

        return $array;
    }


    public function get_invoice_rpt($id)

    {

        $invo = invoice_master::where('invoice_m_id',$id)->first();
        $invit = inventory_master::where('doc_id',$invo->inventory_m_id)->first();
        $supplier = Supplier::where('VendorID',$invit->supplier_id)->first()->VendorName;
//        $doc_type = $invit->doc_type == 1 ? "Purchase" : "return purchase" ;
        $curr = $invo->inv_currency_id == 1 ? "$":"LE";

        $query = DB::table('invoice_master')
            ->join('inventory_details','invoice_master.inventory_m_id','=','inventory_details.doc_id')
            ->join('inventory_master','invoice_master.inventory_m_id','=','inventory_master.doc_id')
            ->join("po_details",function($join){
                $join->on("po_details.po_id","=","inventory_details.po_id")
                    ->on("po_details.item_name_id","=","inventory_details.pro_id");
            })
            ->where('invoice_master.invoice_m_id','=',$id)
            ->get();




        $pdf = new PDF_HF('P','mm','A4');

        $pdf->AddPage();

        $pdf->AliasNbPages('{pages}');

//set font to arial, bold, 14pt

        $pdf->SetFont('Arial','B',12);

//Cell(width , height , text , border , end line , [align] )
        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX( 90);

        if($invo->invoice_seq == 0 )
            $pdf->Cell(40,10,"Invoice : Draft ",0,0,'C');
        else{
            $pdf->Cell(40,10,"Invoice NO : $invo->invoice_seq",0,0,'C');
        }


        $pdf->SetX( 110);
        /*$pdf->Cell(40,10,$invo->invoice_m_id.''."*",0,0,'C');*/
        $pdf->Ln(11);

        $pdf->SetX(10);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'Supplier', 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, $supplier, 0, 0, 'L');
        $pdf->SetX(40);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');

        $pdf->SetX(-90);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'Date', 0, 0, 'L');
        $pdf->SetX(-65);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(-63);
        $pdf->Cell(15, 10, $invo->invoice_date, 0, 0, 'L');
        $pdf->SetX(-60);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');



        $pdf->Ln(15);
        $pdf->SetX(10);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'Inventory NO ', 0, 0, 'L');
        $pdf->SetX(40);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10,$invit->doc_seq.''."*" , 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');

        $pdf->SetX(-90);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'Invoice Ref', 0, 0, 'L');
        $pdf->SetX(-65);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(-63);
        $pdf->Cell(15, 10, $invo->invoice_ref, 0, 0, 'L');
        $pdf->SetX(-60);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');

        $pdf->Ln(15);
        $pdf->SetX(10);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'Currency', 0, 0, 'L');
        $pdf->SetX(40);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, $curr, 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 12, '.................................', 0, 0, 'L');



        $pdf->Ln(15);
        $pdf->setFillColor(230, 230, 230);

        $pdf->Cell(10	,7,'PO',1,0,'C',1);
        $pdf->Cell(40	,7,'Items',1,0,'C',1);
        $pdf->Cell(15	,7,'Qut',1,0,'C',1);
        $pdf->Cell(15	,7,'Price',1,0,'C',1);
        $pdf->Cell(20	,7,'Include',1,0,'C',1);
        $pdf->Cell(40	,7,'Tax',1,0,'C',1);
        $pdf->Cell(25	,7,'Total Tax',1,0,'C',1);
        $pdf->Cell(30	,7,'Sub Total',1,0,'C',1);

        /*-----------------------  change header -----------------------------*/
        $headrs_name = array(
            array("PO",10),
            array("Items",40),
            array("Qut",15),
            array("Price",15),
            array("Include",20),
            array("Tax",40),
            array("Total Tax",25),
            array("Sub Total",30),


        );
        $pdf->set_header_names($headrs_name);
        /*-------------------------------------------------------*/
        $pdf->SetFontSize(10);

        /*set the table header */
        $pdf->setHeaderTitle(true);
        /*------------------------------*/
        $pdf->SetWidths(array(10,40,15,15,20,40,25,30));
        $pdf->Ln(7);

        $value_be_tax1= 0;
        $value_be_tax2= 0;
        $tot_tax1=0;
        $tot_tax2=0;

        $po_details_tax_array = array();

        foreach ($query as $val) {
            $get_taxs = DB::table('po_tax')
                ->join('po_details', 'po_tax.sub_po', '=', 'po_details.sub_po_id')
                ->where('po_tax.po', '=', $val->po_id)
                ->where('po_tax.sub_po', '=', $val->sub_po_id)
                ->orderBy('po_tax.sub_po', 'desc')
                ->get();

            $po = po_report::where('po_id', $val->po_id)->first()->po_seq;
            $item = products::where('pro_id', $val->pro_id)->first()->pro_name;
            $inc_tax = $val->include_tax == 0 ? "no" : "yes";
            $re_sub_total = 0;
            $total_tax = 0;

            if (!$val->tax_string || $val->tax_string == "" || $val->tax_string == Null || $val->tax_string == null) {
                $sub_val = DB::select('call calc_tax(?,?,?,?)', [0, $val->unit_price, $val->qut, $val->include_tax]);
                $re_sub_total = $sub_val[0]->subtotal;
                $total_tax = $total_tax + $sub_val[0]->taxval;
                $value_be_tax1 += $re_sub_total;
                $tot_tax1 += $total_tax;
            } else {
                foreach ($get_taxs as $get_single) {
                    if ($val->sub_po_id == $get_single->sub_po) {
                        $sub_val = DB::select('call calc_tax(?,?,?,?)', [$get_single->tax_id, $get_single->unit_price, $val->qut, $val->include_tax]);
                        $re_sub_total = $sub_val[0]->subtotal;
                        $total_tax = $total_tax + $sub_val[0]->taxval;

                    }

                }

                $value_be_tax2 += $re_sub_total;
                $tot_tax2 += $total_tax;
            }


            $value_be_tax = $value_be_tax1 + $value_be_tax2;
            $tot_tax = $tot_tax1 + $tot_tax2;


            $pdf->SetFont('Arial', 'B', 12);

            $po_tax_details =  DB::select('call PO_TAX_DETAILS(?)',[$val->po_id]);

            /*$po_tax_details +=$po_tax_details;*/
            //array_push($po_details_tax_array, $po_tax_details);
            foreach ($po_tax_details as $single_po_tax_details)
            {
                $po_details_tax_array = $this->array_push_assoc($po_details_tax_array, $single_po_tax_details->tax_name, $single_po_tax_details->TOATL);
            }

            $pdf->Row(array($po, $item, $val->qut, $val->unit_price, $inc_tax, $val->tax_string,$total_tax , $re_sub_total));
        }



        $pdf->SetX( 40);

        /*remove the table header */
        $pdf->setHeaderTitle(false);
        /*------------------------------*/
        $y = $pdf->GetY();
        $po_tax_details = DB::select('call PO_TAX_DETAILS(?)',[$id]);

        $pdf->Ln(5);

        $pdf->SetX(10);
        $pdf->setFillColor(230, 230, 230);
        $pdf->SetFont('', 'B');
        $pdf->Cell(30, 7, 'Tax Name	', 1, 0, 'C', 1);
        $pdf->Cell(30, 7, 'Tax Value', 1, 0, 'C', 1);

        /*-----------------------  change header -----------------------------*/
        $headrs_name = array(
            array("Tax Name", 30),
            array("Tax Value", 30),

        );
        $pdf->SetFillColor(50, 50, 50);


        $pdf->set_header_names($headrs_name);
        /*-------------------------------------------------------*/
        $pdf->SetFontSize(10);

        /*set the table header */
        $pdf->setHeaderTitle(true);

        /*------------------------------*/

        $pdf->SetWidths(array(30, 30));

        $pdf->Ln(7);

        if(isset($po_details_tax_array))
        {
            foreach ($po_details_tax_array as $key=>$val) {
                $pdf->SetFont('Arial', '', 12);

                $pdf->Row(array($key, $val));
            }
        }


        /*remove the table header */
        $pdf->setHeaderTitle(false);
        /*------------------------------*/
        $pdf->SetY($y);

        $pdf->SetX(-80);
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(15, 10, 'Value Before Tax', 0, 0, 'L');

        $pdf->SetX(-35);
        $pdf->setFillColor(230, 230, 230);

        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(30, 10, $value_be_tax, 1, 0, 'C',1);
        $pdf->Ln(10);

        $pdf->SetX(-80);
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(15, 10, 'Taxes', 0, 0, 'L');

        $pdf->SetX(-35);
        $pdf->setFillColor(230, 230, 230);

        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(30, 10, $tot_tax, 1, 0, 'C',1);
        $pdf->Ln(10);
        $pdf->SetX(-80);
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(15, 10, 'Total', 0, 0, 'C');

        $pdf->SetX(-35);
        $pdf->setFillColor(230, 230, 230);

        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(30, 10, $tot_tax + $value_be_tax, 1, 0, 'C',1);


        $pdf->Ln(10);
        $pdf->SetX(10);
        $pdf->SetFont('Arial','BU',12);

        $pdf->Cell(15, 10, 'Note:', 0, 0, 'L');
        $pdf->Ln(10);
        $pdf->SetX(10);
        $pdf->SetFont('', '');
        $pdf->Cell(80, 10, $invo->invoice_notes, 0, 0, 'L');




        $requested_by = @Employee::find($query->first()->invoice_creator_id);

        $approved_by = @Employee::find($query->first()->invoice_app_user_id);



        //APPROVED BY ---- REQUESTED BY //
        $pdf->Ln(5);
        $y = $pdf->GetY();
        $pdf->SetY($y);
        $x = $pdf->GetX();
        $pdf->SetX($x);

        $pdf->Ln(10);
        $pdf->SetX(20);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'Requested By', 0, 0, 'C');


        $pdf->SetX(-50);
        $pdf->SetFont('', 'B');

        $pdf->Cell(15, 10, 'Approved By', 0, 0, 'C');


        $pdf->Ln(10);
        $pdf->SetX(10);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, $requested_by->employee_name.' '.$requested_by->middle_name.' '.$requested_by->last_name.' '.$requested_by->family_name, 0, 0, 'L');
        $pdf->SetX(20);
        $pdf->Cell(15, 14, '........................', 0, 0, 'C');

        $pdf->SetX(-60);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10,  @$approved_by->employee_name.' '.@$approved_by->middle_name.' '.@$approved_by->last_name.' '.@$approved_by->family_name, 0, 0, 'L');
        $pdf->SetX(-50);
        $pdf->Cell(15, 14, '.........................', 0, 0, 'C');

        $pdf->Ln(20);
        $pdf->SetX(20);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'Accountant', 0, 0, 'C');


        $pdf->SetX(-50);
        $pdf->SetFont('', 'B');

        $pdf->Cell(15, 10, 'Financial Manager', 0, 0, 'C');

        $pdf->Ln(10);
        $pdf->SetX(20);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, "", 0, 0, 'L');
        $pdf->SetX(20);
        $pdf->Cell(15, 14, '........................', 0, 0, 'C');


        $pdf->SetX(-50);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, "", 0, 0, 'L');
        $pdf->SetX(-50);
        $pdf->Cell(15, 14, '........................', 0, 0, 'C');








        $pdf->Output();
        exit;


    }

}