<?php

namespace App\Http\Controllers;


use App\Currency;
use App\Account;
use App\Employee;
use App\Vendor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\PDF_HF;


class vendor_report_controller extends Controller
{


    //-----------------------------table-------------------------//

    public function get_vendor(Request $request)

    {



        $array = explode(',', $request->input('print_ids')[0]);


        $vendor = DB::table('vendors')
            ->join('accounts', 'vendors.account_id', '=', 'accounts.child_id')
            ->whereIn('vendors.VendorID',$array)
            ->get();


        $pdf = new PDF_HF('P', 'mm', 'A4');

        $pdf->AddPage();

        $pdf->AliasNbPages('{pages}');

//set font to arial, bold, 14pt

        $pdf->SetFont('Arial', 'B', 12);

//Cell(width , height , text , border , end line , [align] )
        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX(90);

        $pdf->Cell(40, 10, "Vendors Table", 0, 0, 'C');
        $pdf->Ln(11);

        $pdf->setFillColor(230, 230, 230);


        $pdf->Ln(15);
        $pdf->Cell(45, 7, 'ID', 1, 0, 'C', 1);
        $pdf->Cell(45, 7, 'Vendor Code', 1, 0, 'C', 1);
        $pdf->Cell(50, 7, 'Vendor Name', 1, 0, 'C', 1);
        $pdf->Cell(50, 7, 'Account Name', 1, 0, 'C', 1);
        /*-----------------------  change header -----------------------------*/
        $headrs_name = array(
            array("ID", 45),
            array("Vendor Code", 45),
            array("Vendor Name", 50),
            array("Account Name", 50),

        );
        $pdf->set_header_names($headrs_name);
        /*-------------------------------------------------------*/
        $pdf->SetFontSize(10);

        /*set the table header */
        $pdf->setHeaderTitle(true);

        /*------------------------------*/
        $pdf->SetWidths(array(45, 45, 50, 50));
        $pdf->Ln(7);

        foreach ($vendor as $val) {

            $pdf->SetFont('Arial', 'B', 12);

            $pdf->Row(array($val->VendorID, $val->VendorCode, $val->VendorName, $val->Child_name));

        }


        $pdf->Ln(5);
        $pdf->SetX(40);

        /*remove the table header */
        $pdf->setHeaderTitle(false);
        /*------------------------------*/

        $pdf->Output();
        exit;


    }


    //----------------------------report-----------------------------//

    public function get_vendor_rpt($id)
    {

        $vendor = Vendor::find($id);



    $pdf = new PDF_HF('P', 'mm', 'A4');
        $pdf->AddPage();

        $pdf->AliasNbPages('{pages}');

//set font to arial, bold, 14pt

        $pdf->SetFont('Arial', 'B', 12);

//Cell(width , height , text , border , end line , [align] )
        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX(80);

        $pdf->Cell(40, 10, "Vendor NO:".' '.@$id , 0, 0, 'C');
        $pdf->Ln(11);



        $pdf->Ln(10);
        $pdf->SetX(10);
        $pdf->Cell(15, 10, 'Vendor Code', 0, 0, 'L');
        $pdf->SetX(85);
        $pdf->SetFont('', '');
        $pdf->Cell(15, 10, @$vendor->VendorCode, 0, 0, 'L');
        $pdf->SetX(80);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(60);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, "", 0, 0, 'L');
        $pdf->SetX(85);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');
//
//
        $pdf->Ln(10);
        $pdf->SetX(10);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'Vendor Name', 0, 0, 'L');
        $pdf->SetX(85);
        $pdf->SetFont('', '');
        $pdf->Cell(15, 10, @$vendor->VendorName, 0, 0, 'L');
        $pdf->SetX(80);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(60);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, "", 0, 0, 'L');
        $pdf->SetX(85);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');
//
//
        $pdf->Ln(10);
        $pdf->SetX(10);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'Arabic Vendor Name', 0, 0, 'L');
        $pdf->SetX(85);
        $pdf->SetFont('', '');
        $pdf->Cell(15, 10, @$vendor->VendorNameAR, 0, 0, 'L');
        $pdf->SetX(80);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(60);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, "", 0, 0, 'L');
        $pdf->SetX(85);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');
//
        $pdf->Ln(10);
        $pdf->SetX(10);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'Address', 0, 0, 'L');
        $pdf->SetX(85);
        $pdf->SetFont('', '');
        $pdf->Cell(15, 10, @$vendor->VendorAddress, 0, 0, 'L');
        $pdf->SetX(80);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(60);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, "", 0, 0, 'L');
        $pdf->SetX(85);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');
//
//
        $pdf->Ln(10);
        $pdf->SetX(10);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'Arabic Address', 0, 0, 'L');
        $pdf->SetX(85);
        $pdf->SetFont('', '');
        $pdf->Cell(15, 10, @$vendor->VendorAddressAR, 0, 0, 'L');
        $pdf->SetX(80);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(60);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, "", 0, 0, 'L');
        $pdf->SetX(85);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');
//
//
        $pdf->Ln(10);
        $pdf->SetX(10);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'Website', 0, 0, 'L');
        $pdf->SetX(85);
        $pdf->SetFont('', '');
        $pdf->Cell(15, 10, @$vendor->website, 0, 0, 'L');
        $pdf->SetX(80);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(60);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, "", 0, 0, 'L');
        $pdf->SetX(85);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');
//
//
        $pdf->Ln(10);
        $pdf->SetX(10);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'E-mail', 0, 0, 'L');
        $pdf->SetX(85);
        $pdf->SetFont('', '');
        $pdf->Cell(15, 10, @$vendor->Email, 0, 0, 'L');
        $pdf->SetX(80);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(60);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, "", 0, 0, 'L');
        $pdf->SetX(85);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');
//
//
        $pdf->Ln(10);
        $pdf->SetX(10);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'Contact School Person', 0, 0, 'L');
        $pdf->SetX(85);
        $pdf->SetFont('', '');
        $pdf->Cell(15, 10, @$vendor->ContactSchoolPerson, 0, 0, 'L');
        $pdf->SetX(80);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(60);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, "", 0, 0, 'L');
        $pdf->SetX(85);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');
//
//
        $pdf->Ln(10);
        $pdf->SetX(10);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'Contact Vendor Person', 0, 0, 'L');
        $pdf->SetX(85);
        $pdf->SetFont('', '');
        $pdf->Cell(15, 10, @$vendor->ContactVendorPerson, 0, 0, 'L');
        $pdf->SetX(80);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(60);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, "", 0, 0, 'L');
        $pdf->SetX(85);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');
//
//
        $pdf->Ln(10);
        $pdf->SetX(10);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'Phone 1', 0, 0, 'L');
        $pdf->SetX(85);
        $pdf->SetFont('', '');
        $pdf->Cell(15, 10, @$vendor->Phone1, 0, 0, 'L');
        $pdf->SetX(80);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(60);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, "", 0, 0, 'L');
        $pdf->SetX(85);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');
//
//
        $pdf->Ln(10);
        $pdf->SetX(10);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'Phone 2', 0, 0, 'L');
        $pdf->SetX(85);
        $pdf->SetFont('', '');
        $pdf->Cell(15, 10, @$vendor->Phone2, 0, 0, 'L');
        $pdf->SetX(80);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(60);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, "", 0, 0, 'L');
        $pdf->SetX(85);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');
//
//
        $pdf->Ln(10);
        $pdf->SetX(10);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'Tax File Number', 0, 0, 'L');
        $pdf->SetX(85);
        $pdf->SetFont('', '');
        $pdf->Cell(15, 10, @$vendor->TaxFileNumber, 0, 0, 'L');
        $pdf->SetX(80);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(60);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, "", 0, 0, 'L');
        $pdf->SetX(85);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');
//
//
        $pdf->Ln(10);
        $pdf->SetX(10);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'Tax Office Name', 0, 0, 'L');
        $pdf->SetX(85);
        $pdf->SetFont('', '');
        $pdf->Cell(15, 10, @$vendor->TaxOfficeName, 0, 0, 'L');
        $pdf->SetX(80);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(60);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, "", 0, 0, 'L');
        $pdf->SetX(85);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');
//
        $pdf->Ln(10);
        $pdf->SetX(10);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'Tax Office Code', 0, 0, 'L');
        $pdf->SetX(85);
        $pdf->SetFont('', '');
        $pdf->Cell(15, 10, @$vendor->TaxOfficeCode, 0, 0, 'L');
        $pdf->SetX(80);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(60);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, "", 0, 0, 'L');
        $pdf->SetX(85);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');
//
//
        $pdf->Ln(10);
        $pdf->SetX(10);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'Commercial Regesteration Code', 0, 0, 'L');
        $pdf->SetX(85);
        $pdf->SetFont('', '');
        $pdf->Cell(15, 10, @$vendor->CommercialRegNo, 0, 0, 'L');
        $pdf->SetX(80);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(60);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, "", 0, 0, 'L');
        $pdf->SetX(85);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');
//
        $pdf->Ln(10);
        $pdf->SetX(10);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'Tax Regesteration Number', 0, 0, 'L');
        $pdf->SetX(85);
        $pdf->SetFont('', '');
        $pdf->Cell(15, 10, @$vendor->TaxRegNumber, 0, 0, 'L');
        $pdf->SetX(80);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(60);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, "", 0, 0, 'L');
        $pdf->SetX(85);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');
//
//
        $pdf->Ln(10);
        $pdf->SetX(10);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'Vat Regesteration Number', 0, 0, 'L');
        $pdf->SetX(85);
        $pdf->SetFont('', '');
        $pdf->Cell(15, 10, @$vendor->SalesTax, 0, 0, 'L');
        $pdf->SetX(80);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(60);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, "", 0, 0, 'L');
        $pdf->SetX(85);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');
//
//
        $pdf->Ln(10);
        $pdf->SetX(10);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'Currency', 0, 0, 'L');
        $pdf->SetX(85);
        $pdf->SetFont('', '');
        $pdf->Cell(15, 10, @Currency::find($vendor->CurrencyID)->Currency_name, 0, 0, 'L');
        $pdf->SetX(80);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(60);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, "", 0, 0, 'L');
        $pdf->SetX(85);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');
//
        $pdf->Ln(10);
        $pdf->SetX(10);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'Account Name', 0, 0, 'L');
        $pdf->SetX(85);
        $pdf->SetFont('', '');
        $pdf->Cell(15, 10, @Account::find($vendor->account_id)->Child_name, 0, 0, 'L');
        $pdf->SetX(80);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(60);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, "", 0, 0, 'L');
        $pdf->SetX(85);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');

        $pdf->Output();
        exit;

    }
    }
