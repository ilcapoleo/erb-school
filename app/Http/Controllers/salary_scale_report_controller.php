<?php

namespace App\Http\Controllers;



use App\SalaryScale;
use App\CostCenter;


use App\SubSalary;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\PDF_HF;


class salary_scale_report_controller extends Controller
{
    //-----------------------------table-------------------------//
    public function get_salary_scale(Request $request)

    {

        $print_array = array();
        $array =  explode(',', $request->input('print_ids')[0]);

        foreach ($array as $single)
        {
            array_push($print_array, $single);
        }


        $query = DB::table('salary_scale')
            ->join('cost_centers','salary_scale.cost_id','=','cost_centers.Cost_id')
//                ->join('employee','departments.manager_id','=','employee.employee_user_id')

            ->orderBy('salary_scale.salary_id','desc')
            ->wherein('salary_scale.salary_id',$print_array)

            ->get();




        $pdf = new PDF_HF('P','mm','A4');

        $pdf->AddPage();

        $pdf->AliasNbPages('{pages}');

//set font to arial, bold, 14pt

        $pdf->SetFont('Arial','B',12);

//Cell(width , height , text , border , end line , [align] )
        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX( 90);

        $pdf->Cell(40,10,"Salary Scale Table",0,0,'C');
        $pdf->Ln(11);

        $pdf->setFillColor(230,230,230);


        $pdf->Ln(15);
        $pdf->Cell(45	,7,'ID',1,0,'C',1);
        $pdf->Cell(45	,7,'Salary Name',1,0,'C',1);
        $pdf->Cell(50	,7,'Short Code',1,0,'C',1);
        $pdf->Cell(50	,7,'Cost Center',1,0,'C',1);
        /*-----------------------  change header -----------------------------*/
        $headrs_name = array(
            array("ID",45),
            array("Salary Name",45),
            array("Short Code",50),
            array("Cost Center",50),
        );
        $pdf->set_header_names($headrs_name);
        /*-------------------------------------------------------*/
        $pdf->SetFontSize(10);

        /*set the table header */
        $pdf->setHeaderTitle(true);

        /*------------------------------*/
        $pdf->SetWidths(array(45,45,50,50));
        $pdf->Ln(7);

        foreach ($query as $val)
        {

            $pdf->SetFont('Arial','B',12);

            $pdf->Row(array($val->salary_id,$val->salary_name,$val->code_name,$val->Cost_name));

        }


        $pdf->Ln(5);
        $pdf->SetX( 40);

        /*remove the table header */
        $pdf->setHeaderTitle(false);
        /*------------------------------*/



        $pdf->Output();
        exit;


    }







    //----------------------------reportt-----------------------------//


    public function get_salary_scale_rpt(Request $request)

   {
       $depart = SalaryScale::where('salary_id',$request->id)->first();
       $cost =CostCenter::where('Cost_id',$depart->cost_id)->first()->Cost_name;
       $query = DB::table('sub_salary')
           ->where('salary_id','=',$depart->salary_id)
           ->get();
//       $sub_name = SubSalary::where('salary_id',$depart->salary_id)->first()->sub_name;

//       $print_array = array();
//       $array =  explode(',', $request->input('print_ids')[0]);
//
//       foreach ($array as $single)
//       {
//           array_push($print_array, $single);
//       }
//       $query = DB::table('sub_salary')
//           ->join('salary_scale','sub_salary.salary_id','=','salary_scale.salary_id')
////            ->join('accounts','fees_head.account_id','=','accounts.child_id')
//
//           ->wherein('sub_salary.salary_id',$print_array)
//
////           ->orderBy('sub_salary.sub_id','desc')
//           ->get();

        $pdf = new PDF_HF('P','mm','A4');

       $pdf->AddPage();

      $pdf->AliasNbPages('{pages}');

////set font to arial, bold, 14pt

       $pdf->SetFont('Arial','B',12);

////Cell(width , height , text , border , end line , [align] )
       $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX( 80);

        $pdf->Cell(40,10,"Salary Scale NO:".$depart->salary_id,0,'C');
       $pdf->Ln(11);

       $pdf->Ln(10);
       $pdf->SetX( 10);
        $pdf->Cell(15,10,'Salary Scale Name',0,'L');
        $pdf->SetX(60);
        $pdf->SetFont('','');
       $pdf->Cell(15, 10, $depart->salary_name, 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
       $pdf->SetX( 60);
        $pdf->SetFont('','');
       $pdf->Cell(30,10,"",0,0,'L');
        $pdf->SetX(60);
       $pdf->Cell(30, 12, '..............................', 0, 0, 'L');


       $pdf->Ln(10);
        $pdf->SetX( 10);
       $pdf->SetFont('','B');
        $pdf->Cell(15,10,'Short Code',0,0,'L');
        $pdf->SetX(60);
        $pdf->SetFont('','');
       $pdf->Cell(15, 10, $depart->code_name, 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
       $pdf->SetX( 60);
       $pdf->SetFont('','');
       $pdf->Cell(30,10,"",0,0,'L');
       $pdf->SetX(60);
       $pdf->Cell(30, 12, '..............................', 0, 0, 'L');

       $pdf->Ln(10);
       $pdf->SetX( 10);
       $pdf->SetFont('','B');
       $pdf->Cell(15,10,'Cost Center',0,0,'L');
       $pdf->SetX(60);
        $pdf->SetFont('','');
       $pdf->Cell(15, 10, $cost, 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
       $pdf->SetX( 60);
       $pdf->SetFont('','');
      $pdf->Cell(30,10,"",0,0,'L');
       $pdf->SetX(60);
      $pdf->Cell(30, 12, '..............................', 0, 0, 'L');


       $pdf->Ln(30);
       $pdf->SetX( 10);
       $pdf->SetFont('','BU');
       $pdf->Cell(15,10,'Sub Salary Scale :',0,0,'L');





       $pdf->setFillColor(230, 230, 230);
       $pdf->SetFont('','B');
       $pdf->Ln(15);
        $pdf->Cell(100	,7,'Sub Salary Name',1,0,'C',1);
        $pdf->Cell(90	,7,'Short code	',1,0,'C',1);

       /*-----------------------  change header -----------------------------*/
        $headrs_name = array(
            array("Sub Salary Name",100),
            array("Short code",90),

        );
        $pdf->set_header_names($headrs_name);
        /*-------------------------------------------------------*/
        $pdf->SetFontSize(10);
        //        /*set the table header */
       $pdf->setHeaderTitle(true);

        /*------------------------------*/
       $pdf->SetWidths(array(100,90));
       $pdf->Ln(7);



         foreach ($query as $val)
        {

//            $pdf->SetFont('Arial','B',12);first()->sub_code;

            $pdf->SetFont('Arial','',12);

            $pdf->Row(array($val->sub_name,$val->sub_code));

        }

       $pdf->Ln(5);
       $pdf->SetX( 40);

       /*remove the table header */
       $pdf->setHeaderTitle(false);
        /*------------------------------*/



        $pdf->Output();
       exit;


   }






}