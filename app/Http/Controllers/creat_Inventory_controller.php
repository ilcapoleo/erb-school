<?php

namespace App\Http\Controllers;


use App\Details_inventory;
use App\Details_po;
use App\Employee;
use App\Master_inventory;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;




class creat_inventory_controller extends Controller
{
    //
    /* public function ret_cost_page()
     {
         return view('admin_pages.cost_center_page');
     }*/
    public function creat_inventory()
    {
        $supplier = DB::table('vendors')->orderBy('VendorName', 'asc')
            ->pluck('VendorName', 'VendorID');

        $document_types = DB::table('inventory_documents')->orderBy('doc_type_name', 'asc')
            ->pluck('doc_type_name', 'inv_doc_id');

        $curr_date = Carbon::now('Africa/Cairo')->addHour()->toDateString();

        return view('school_pages.inventory',compact('supplier','curr_date','document_types'));
    }

    public function get_po_values_drop_down(Request $request)
    {
        $not_found = 0;

        $item_type = DB::table('po_master')
            ->join('po_details', 'po_master.po_id', '=', 'po_details.po_id')
            ->where('po_master.approved_user_id', '<>', NULL)
            ->where('po_master.supplier_id', '=', $request->supplier)
            ->where('po_master.approved_date', '<>', NULL)
            ->where('po_master.po_canceled', '=', 0)
            ->where('po_details.closed_details', '=', 0)
            ->select('po_master.po_id', 'po_master.po_seq')
            ->groupBy('po_master.po_id', 'po_master.po_seq')
            ->orderBy('po_master.po_id', 'desc')
            ->get();

        if ($request->curr_selectet_value && $request->curr_selectet_value != '' && $request->curr_selectet_value != null)
        {
            if($item_type->count() >0)
            {
                foreach ($item_type as $one_item) {
                    if ($one_item->po_id == $request->curr_selectet_value) {
                        $not_found = 0;
                    } else {
                        $not_found = 1;
                    }
                }
            }
            else{
                $not_found = 1;
            }



        if ($not_found == 1) {
            $extra_po_colsed = DB::table('po_master')
                ->where('po_id', '=', $request->curr_selectet_value)
                ->select('po_id', 'po_seq')
                ->first();
        } else {
            $extra_po_colsed = "";
        }
    }
    else
        {
            $extra_po_colsed = "";
        }

        return response()->json(['item_type'=>$item_type,'extra_po_colsed'=>$extra_po_colsed]);
    }

    public function get_item_product_drop_down(Request $request)
    {
        $po_id = $request->po_id;
        $current_edit_pro_id = $request->product_id;
        $selected_v_ids = array();

        if($request->item_selected_array != "")
        {
            foreach ($request->item_selected_array as $single_select)
            {
                array_push($selected_v_ids,$single_select);

            }

            if(count($selected_v_ids)>0)
            {
                $not_found = 0;

                $item_type = DB::table('products')
                    ->join('po_details','products.pro_id','=','po_details.item_name_id')
                    ->where('po_details.po_id','=',$po_id)

                    ->where('closed_details','=',0)

                    ->where(function ($query) use($selected_v_ids) {
                        for ($h = 0; $h < count($selected_v_ids); $h++){
                            $query->where('products.pro_id','<>',$selected_v_ids[$h]);
                        }
                    })
                    ->orderBy('products.pro_id', 'desc')
                    ->groupBy('products.pro_id','products.pro_name')
                    ->select('products.pro_id','products.pro_name')
                    ->get();

                if ($request->product_id && $request->product_id != '' && $request->product_id != null)
                {
                    foreach ($item_type as $one_item) {
                        if ($one_item->pro_id == $request->product_id) {
                            $not_found = 0;
                        } else {
                            $not_found = 1;
                        }
                    }

                    if ($not_found == 1) {
                        $add_item_type = DB::table('products')
                            ->join('po_details','products.pro_id','=','po_details.item_name_id')
                            ->where('po_details.po_id','=',$po_id)
                            ->where('products.pro_id','=', $request->product_id)
                            /*->select('products.pro_id','products.pro_name')*/
                            ->get();
                    } else {
                        $add_item_type = "";
                    }
                }
                else
                {
                    $add_item_type = "";
                }

            }
        }



        else
            {
                $not_found = 0;

                $item_type = DB::table('products')
                    ->join('po_details','products.pro_id','=','po_details.item_name_id')
                    ->where('po_details.po_id','=',$po_id)

                    ->where('closed_details','=',0)
                    ->orderBy('products.pro_id', 'desc')
                    ->groupBy('products.pro_id','products.pro_name')
                    ->select('products.pro_id','products.pro_name')
                    ->get();



               /* if($request->checked == 1 && $request->seq != 0)
                {
                    $add_item_type = DB::table('products')
                        ->join('po_details','products.pro_id','=','po_details.item_name_id')
                        //->where('po_details.po_id','=',$po_id)
                        ->where(function($query) use ($current_edit_pro_id)
                        {
                            if ( $current_edit_pro_id != "" && $current_edit_pro_id != "undefined" &&  $current_edit_pro_id) $query->where('pro_id','=',$current_edit_pro_id , 'AND', 'closed_details','=',0);
                            else $query->where('closed_details','=',0);
                        })
                        //->where('products.pro_id','=',$request->product_id)
                        ->where('po_details.po_id','=',$po_id)

                        ->orderBy('products.pro_id', 'desc')
                        ->groupBy('products.pro_id','products.pro_name')
                        ->select('products.pro_id','products.pro_name')
                        ->get();
               }
                else
                {
                    $add_item_type="";
                }*/

                if ($request->product_id && $request->product_id != '' && $request->product_id != null)
                {
                    if($item_type->count() >0)
                    {
                        foreach ($item_type as $one_item) {
                            if ($one_item->pro_id == $request->product_id) {
                                $not_found = 0;
                            } else {
                                $not_found = 1;
                            }
                        }
                    }
                    else{
                        $not_found = 1;
                    }

                    if ($not_found == 1) {
                        $add_item_type = DB::table('products')
                            ->join('po_details','products.pro_id','=','po_details.item_name_id')
                            ->where('po_details.po_id','=',$po_id)
                            ->where('products.pro_id','=', $request->product_id)
                            /*->select('products.pro_id','products.pro_name')*/
                            ->get();
                    } else {
                        $add_item_type = "";
                    }
                }
                else
                {
                    $add_item_type = "";
                }


            }
        //$data['data'] = "";
        $data['data'] = $item_type;
        $data['extra_v'] = $add_item_type;
        $data['$item_type'] = $item_type;

        //$item_type->unique('pro_id');
        //return response()->json([$item_type,'$selected_v_ids'=>$selected_v_ids]);
        return response()->json($data);
        //return response($selected_v_ids);
    }

    public function get_quantity_limit(Request $request)
    {
        $po_id = $request->po_id;
        $item_pro_id = $request->item_pro_id;
        $inventory_id = $request->inventory_id;
/*        $quantitys = DB::table('po_details')
            ->where('po_id','=',$po_id)
            ->where('item_name_id','=',$item_pro_id)
            ->get();*/

        $last_quantity = 0;
        if($inventory_id == "" || !$inventory_id)
        {
            $inventory_id = 0;
        }
        $last_quantity = DB::select('call clac_remain_qut(?,?,?)',[$po_id,$item_pro_id,$inventory_id]);
        
        /*foreach ($quantitys as $single_quantity)
        {
            $last_quantity += $single_quantity->quantity;
        }*/

        return response(['quantity'=>$last_quantity[0]->bal]);
    }

    public function add_new_inventory(Request $request)
    {
        $rules = array(

            'cur_user_id' => 'required',
            'supplier' => 'required',
            //'beneficiary_user' => 'required',
            'document_type' => 'required',
            'document_date' => 'required|date',
            //'approved_date' => 'required|date|after_or_equal:order_date',

        );
        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), $rules);
        if ($validator->passes()) {

/*-----------------------------------------  edit inventory -------------------------------------------------------------------*/

            if ($request->current_po_id != "" && $request->current_po_id) {

                $can_save_qut = 1;
                $error_array = array();
                if ($request->table_rows != "") {
                foreach ($request->table_rows as $row) {

                    $remain_quantity = DB::select('call clac_remain_qut(?,?,?)', [$row['po_id'], $row['item_product'], $request->current_po_id]);
                    /*return response(['remain'=>$remain_quantity]);*/
                    if ($remain_quantity[0]->bal < $row['quantity']) {
                        $can_save_qut = 0;
                        array_push($error_array, 0);
                        /*return response([$request, 'errors'=>'no quantity']);*/
                    }
                    else{
                        array_push($error_array, 1);
                    }
                }
                }
                if ($can_save_qut == 1) {


                $master_po = Master_inventory::findOrFail($request->current_po_id);
                //$master_po->create_by_user_id = $request->cur_user_id;
                $master_po->doc_type = $request->document_type;
                $master_po->supplier_id = $request->supplier;


                $curr_date = Carbon::now('Africa/Cairo')->addHour();

                $curr_time = Carbon::now('Africa/Cairo')->addHour()->toTimeString();

                $master_po->doc_date = date('Y-m-d', strtotime($request->document_date)) . ' ' . $curr_time;


                if ($request->status == 'draft_po') {
                    $status = DB::table('document_status')->where('status_name', '=', $request->status)->first();
                    $master_po->doc_seq = 0;
                    $master_po->doc_status_id = $status->status_id;

                } elseif ($request->status == 'approved_po') {
                    if ($master_po->doc_seq == 0) {
                        $new_seq = DB::table('inventory_master')->max('doc_seq') + 1;
                        $master_po->doc_seq = $new_seq;
                    }

                    $status = DB::table('document_status')->where('status_name', '=', $request->status)->first();
                    $master_po->doc_status_id = $status->status_id;

                    //$curr_date = date("Y-m-d H:i:s");
                    if (!$master_po->app_date || $master_po->app_date == null) {
                        $master_po->app_date = $curr_date;
                    }

                    if (!$master_po->app_user_id || $master_po->app_user_id == null) {
                        $master_po->app_user_id = Employee::where('employee_user_id','=',$request->cur_user_id)->first()->employee_id;//$request->cur_user_id;
                    }

                } else {
                    if ($master_po->doc_seq == 0) {
                        $new_seq = DB::table('inventory_master')->max('doc_seq') + 1;
                        $master_po->doc_seq = $new_seq;
                    }
                    $status = DB::table('document_status')->where('status_name', '=', $request->status)->first();
                    $master_po->doc_status_id = $status->status_id;
                }


                $master_po->notes = $request->document_note;

                $master_po->save();

                $master_po_id = $master_po->doc_id;

                    DB::table('inventory_details')
                        ->join('po_details','inventory_details.pro_id','=','po_details.item_name_id')
                        ->where('inventory_details.doc_id', $master_po_id)
                        ->update(['closed_details' => 0]);


                DB::table('inventory_details')->where('doc_id', $master_po_id)->delete();


                if ($request->table_rows != "") {
                    foreach ($request->table_rows as $row) {
                        $po_details = new Details_inventory();
                        $po_details->doc_id = $master_po_id;
                        $po_details->pro_id = $row['item_product'];
                        $po_details->qut = $row['quantity'];
                        $po_details->po_id = $row['po_id'];
                        $po_details->save();



                        $po_ditails = Details_po::where('po_id', '=', $row['po_id'])
                            ->where('item_name_id', '=', $row['item_product'])
                            ->update(['closed_details' => $row['closed']]);


                    }
                }


                return response([$request, 'edit_po', 'current_po_id' => $master_po_id, 'status' => $request->status,'po_seq'=>$master_po->doc_seq]);
            }
                else{
                    return response([$request, 'errors'=>'no quantity','table_error_array'=>$error_array]);
                }


        }

            /*-------------------------  add new inventory  -----------------------------------*/
        else {
                $can_save_qut = 1;
                $error_array = array();
                if ($request->table_rows != "") {
                    foreach ($request->table_rows as $row) {

                        $remain_quantity = DB::select('call clac_remain_qut(?,?,?)', [$row['po_id'], $row['item_product'], 0]);

                        if ($remain_quantity[0]->bal < $row['quantity']) {
                            $can_save_qut = 0;
                            array_push($error_array, 0);

                        } else {
                            array_push($error_array, 1);
                        }
                    }
                }

                if ($can_save_qut == 1) {


                /*-----------------------------------------  add new  -------------------------------------------------------------------*/
                $master_po = new Master_inventory();
                $master_po->create_by_user_id = Employee::where('employee_user_id','=',$request->cur_user_id)->first()->employee_id;//$request->cur_user_id;
                $master_po->doc_type = $request->document_type;
                $master_po->supplier_id = $request->supplier;

                $curr_date = Carbon::now('Africa/Cairo')->addHour();

                $curr_time = Carbon::now('Africa/Cairo')->addHour()->toTimeString();

                $master_po->doc_date = date('Y-m-d', strtotime($request->document_date)) . ' ' . $curr_time;

                if ($request->status == 'draft_po') {
                    $status = DB::table('document_status')->where('status_name', '=', $request->status)->first();
                    $master_po->doc_seq = 0;
                    $master_po->doc_status_id = $status->status_id;

                } elseif ($request->status == 'approved_po') {
                    $new_seq = DB::table('inventory_master')->max('doc_seq') + 1;
                    $master_po->doc_seq = $new_seq;
                    $master_po->app_user_id = Employee::where('employee_user_id','=',$request->cur_user_id)->first()->employee_id;//$request->cur_user_id;
                    $status = DB::table('document_status')->where('status_name', '=', $request->status)->first();
                    $master_po->doc_status_id = $status->status_id;

                    //$curr_date = date("Y-m-d H:i:s");

                    $master_po->app_date = $curr_date;
                } else {
                    $new_seq = DB::table('inventory_master')->max('doc_seq') + 1;
                    $master_po->doc_seq = $new_seq;
                    $status = DB::table('document_status')->where('status_name', '=', $request->status)->first();
                    $master_po->doc_status_id = $status->status_id;
                }


                $master_po->notes = $request->document_note;

                $master_po->save();

                $master_po_id = $master_po->doc_id;
                //$return_approved_date = $master_po->approved_date;

                if ($request->table_rows != "") {
                    foreach ($request->table_rows as $row) {
                        $po_details = new Details_inventory();
                        $po_details->doc_id = $master_po_id;
                        $po_details->pro_id = $row['item_product'];
                        $po_details->qut = $row['quantity'];
                        $po_details->po_id = $row['po_id'];
                        $po_details->save();


                        $po_ditails = Details_po::where('po_id', '=', $row['po_id'])
                            ->where('item_name_id', '=', $row['item_product'])
                            ->update(['closed_details' => $row['closed']]);
                    }
                }

                return response([$request, 'current_po_id' => $master_po_id, 'status' => $request->status,'po_seq'=>$master_po->doc_seq]);
            }

            else{
                return response([$request, 'errors'=>'no quantity','table_error_array'=>$error_array]);
            }
            }
        }

        /*------------------------- validation failed   -----------------------------------*/
        elseif ($validator->fails()) {

            return response()->json(['error' => $validator->messages()]);

        }


    }

    /*-------------------------  delete inventory --------------------------------*/
    public function delete_this_inventory(Request $request)
    {
        $master_po = Master_inventory::findOrFail($request->cur_po_id);

        $master_po->delete();

        return response($request);

    }

    /*------------------------------- return edit page ------------------------------*/
    public  function eidet_this_inventory($id)
    {

        $master_inventory = DB::table('inventory_master')
            ->join('vendors','inventory_master.supplier_id','=','vendors.VendorID')
            ->join('document_status','inventory_master.doc_status_id','=','document_status.status_id')
            ->where('inventory_master.doc_id','=',$id)
            ->orderBy('inventory_master.doc_id','desc')
            ->first();

        $details_inventory = DB::table('inventory_details')
            ->join('products','inventory_details.pro_id','=','products.pro_id')
            ->join('po_master','inventory_details.po_id','=','po_master.po_id')
            ->where('inventory_details.doc_id','=',$id)
            ->orderBy('inventory_details.sub_doc_id','desc')
            ->get();

        $all_max_quantity = array();
        $all_closed_state = array();
        foreach ($details_inventory as $details_inventory_single)
        {
            $last_quantity = 0;
            /*$quantitys = DB::table('po_details')
                ->where('po_id','=',$details_inventory_single->po_id)
                ->where('item_name_id','=',$details_inventory_single->pro_id)
                ->get();*/
            $last_quantity = DB::select('call clac_remain_qut(?,?,?)',[$details_inventory_single->po_id,$details_inventory_single->pro_id,$details_inventory_single->doc_id]);
            $closed = DB::table('po_details')
                ->where('po_id','=',$details_inventory_single->po_id)
                ->where('item_name_id','=',$details_inventory_single->pro_id)
                ->first();

            array_push($all_closed_state, $closed->closed_details);
            /*$last_quantity = 0;
            foreach ($quantitys as $single_quantity)
            {
                $last_quantity += $single_quantity->quantity;
            }*/

            array_push($all_max_quantity, $last_quantity[0]->bal);
        }



        $supplier = DB::table('vendors')->orderBy('VendorName', 'asc')
            ->pluck('VendorName', 'VendorID');

        $document_types = DB::table('inventory_documents')->orderBy('doc_type_name', 'asc')
            ->pluck('doc_type_name', 'inv_doc_id');

        $curr_date = Carbon::now('Africa/Cairo')->addHour()->toDateString();


        return view('school_pages.inventory',compact('supplier','curr_date','master_inventory','details_inventory','all_max_quantity','all_closed_state','document_types'));

    }


    /*-----------------------------  inventory table -----------------------------------*/

    /*----------------  set search list for input ---------------------*/
    public function get_table_show_inventory()
    {
        $showed_search_list = array('Inventory Number','Doc Type','Supplier','Creator Name','Date','Status');
        $database_search_list = array(1,2,3,4,5,6);

        $inventoreys = DB::table('inventory_master')
            ->leftJoin('vendors','inventory_master.supplier_id','=','vendors.VendorID')
            ->join('employee','inventory_master.create_by_user_id','=','employee.employee_id')
            //->join('employee','userss.id','=','employee.employee_user_id')
            ->leftJoin('document_status','inventory_master.doc_status_id','=','document_status.status_id')
            ->leftJoin('inventory_documents','inventory_master.doc_type','=','inventory_documents.inv_doc_id')
            /*->where('inventory_master.doc_seq','<>','0')*/
            /*->select('doc_id','doc_seq','doc_type_name','VendorName','employee_name','status_short_name','create_by_user_id')*/
            ->orderBy('inventory_master.doc_id','desc')
            ->get();



        return view('school_pages.show_inventory',['showed_search_list' => $showed_search_list,'database_search_list'=>$database_search_list],compact('inventoreys'));
    }

    /*--------------------  search the table ---------------------------*/

    public function search_inventory_table(Request $request)
    {

        $search_colums=array();
        $book=array();

        $string = $request->sudent_data;

        foreach ($string as $value) {

            list($k, $v) = explode('|', $value);
            if($v == "")
            {
                array_push($search_colums,"all");
                array_push($book,$k);
            }
            else{
                array_push($search_colums,$v);
                array_push($book,$k);

            }

        }

        //$search_colums = array('s_name','s_class','s_age');

        $showed_colums = array('Inventory Number','Date','Doc Type','Supplier','Creator Name','Status');
        $colums_type = array('input','input','input','input','input','input');
        $table_colums = array('doc_seq','doc_date','doc_type_name','VendorName','name','status_short_name');

        //$book = $request->sudent_data;

        if(count($book) >0 && $book[0] != "all")
        {
            $name = DB::table('inventory_master')
                ->leftJoin('vendors','inventory_master.supplier_id','=','vendors.VendorID')
                ->join('userss','inventory_master.create_by_user_id','=','userss.id')
                ->leftJoin('document_status','inventory_master.doc_status_id','=','document_status.status_id')
                ->leftJoin('inventory_documents','inventory_master.doc_type','=','inventory_documents.inv_doc_id')
                /*->where('inventory_master.doc_seq','<>','0')*/



                ->Where(function ($query) use($book,$search_colums ,$table_colums) {
                    /*for ($i = 0; $i < count($book); $i++){
                        for ($h = 0; $h < count($search_colums); $h++){
                            $query->orwhere($search_colums[$h], 'like',  '%' . $book[$i] .'%');
                        }
                    }*/
                    for ($i = 0; $i < count($book); $i++){
                        if($search_colums[$i] == "all")
                        {
                            $table_colums;
                            for ($h = 0; $h < count($table_colums); $h++){
                                $query->orwhere($table_colums[$h], 'like',  '%' . $book[$i] .'%');
                            }

                        }
                        else
                        {
                            $query->orwhere($search_colums[$i], 'like',  '%' . $book[$i] .'%');
                        }

                    }
                })
                ->orderBy('inventory_master.doc_id','desc')
                ->get();

        }
        else
        {

            $name = DB::table('inventory_master')
                ->leftJoin('vendors','inventory_master.supplier_id','=','vendors.VendorID')
                ->join('userss','inventory_master.create_by_user_id','=','userss.id')
                ->leftJoin('document_status','inventory_master.doc_status_id','=','document_status.status_id')
                ->leftJoin('inventory_documents','inventory_master.doc_type','=','inventory_documents.inv_doc_id')
                /*->where('inventory_master.doc_seq','<>','0')*/
                ->orderBy('inventory_master.doc_id','desc')
                ->get();
        }
        return response(["name"=>$name ,"colums"=> $table_colums,"showed_colums"=>$showed_colums,"book"=>$book,"searched_colums"=>$search_colums,"colums_type"=>$colums_type]);
    }


    /*-------------------------  delete inventory ----------------------------------*/

    public function delete_all_selected_inventory(Request $request)
    {
        $deleted = array();
        foreach ($request->deleted_user_ids as $class_id_one)
        {
            $master_po = Master_inventory::findOrFail($class_id_one);
            if($master_po->doc_status_id == 1)
            {
                $master_po->delete();
                array_push($deleted,1);

            }
            else
            {
                array_push($deleted,0);
            }
        }

        return response(['deleted_ids'=>$request->deleted_user_ids,'deleted'=>$deleted]);
    }


}
