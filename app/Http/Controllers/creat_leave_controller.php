<?php

namespace App\Http\Controllers;


use App\User;
use App\Leave_type;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;




class creat_leave_controller extends Controller
{
    //
    /* public function ret_cost_page()
     {
         return view('admin_pages.cost_center_page');
     }*/
    public function creat_leave()
    {
        $deduction = DB::table('allowence_deduction')->orderBy('allow_id', 'asc')
            ->where('allow_type','=',1)
            ->pluck('allow_name', 'allow_id');

        return view('school_pages.leave',compact('deduction'));
    }


    public function add_new_leave_type(Request $request)
    {
        $rules = array(

            'leave_name' => 'required',
            'short_name' => 'required',
            'max' => 'required',
            'Deduction' => 'required_if:paid,==,0',


        );
        $customMessages = [
            'required_if' => 'The deduction field is required.'
        ];
        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), $rules,$customMessages);

        if ($validator->passes()) {

            /*------------- edit -----------------*/
            if ($request->current_po_id != "" && $request->current_po_id) {

                $department = Leave_type::findOrFail($request->current_po_id);

                $department->leave_name = $request->leave_name;
                $department->short_code = $request->short_name;
                /*-------------------  days leave unit = 1  ---------------------------*/
                $department->leave_unit = $request->leave_unit;
                $department->max_unit = $request->max;
                $department->paid = $request->paid;
                $department->deduction_id = $request->Deduction;

                $department->save();

                $depart_id = $department->leave_id;

                return response([$request, 'edit_po', 'current_po_id' => $depart_id]);
            } /*----------------- create new  ---------------*/
            else {
                $department = new Leave_type();
                $department->leave_name = $request->leave_name;
                $department->short_code = $request->short_name;
                /*-------------------  days leave unit = 1  ---------------------------*/
                $department->leave_unit = $request->leave_unit;
                $department->max_unit = $request->max;
                $department->paid = $request->paid;
                $department->deduction_id = $request->Deduction;

                $department->save();

                $depart_id = $department->leave_id;

                return response([$request, 'edit_po', 'current_po_id' => $depart_id]);
            }
        }


        elseif ($validator->fails()) {

            return response()->json(['error' => $validator->messages()]);

        }
    }


    public function delete_this_leave_type(Request $request)
    {


        $leave_type = Leave_type::findOrFail($request->cur_po_id);
        $leave_type->delete();

        return response($request);


    }

    public function get_this_leave_type($id)
    {

        $leave_type = Leave_type::findOrFail($id);

        $deduction = DB::table('allowence_deduction')->orderBy('allow_id', 'asc')
            ->where('allow_type','=',1)
            ->pluck('allow_name', 'allow_id');

        return view('school_pages.leave',compact('deduction','leave_type'));

    }



    public function creat_leave_request()
    {
        return view('school_pages.leave_request');
    }


    public function creat_leave_calender()
    {
        return view('school_pages.leave_calender');
    }

    public function show_leave_type()
    {
        $showed_search_list = array('Leave Name','Short Code');
        $database_search_list = array(1,2);
        $leave_types = DB::table('leave_type')
            ->orderBy('leave_type.leave_id','desc')
            ->get();

        return view('school_pages.show_leave_type',['showed_search_list' => $showed_search_list,'database_search_list'=>$database_search_list],compact('leave_types'));
    }

    public function search_leave_type_table(Request $request)
    {

        $search_colums=array();
        $book=array();

        $string = $request->sudent_data;

        foreach ($string as $value) {

            list($k, $v) = explode('|', $value);
            if($v == "")
            {
                array_push($search_colums,"all");
                array_push($book,$k);
            }
            else{
                array_push($search_colums,$v);
                array_push($book,$k);

            }

        }

        //$search_colums = array('s_name','s_class','s_age');

        $showed_colums = array('Leave Name','Short Code');
        $colums_type = array('input','input','input','input','input','input');
        $table_colums = array('leave_name','short_code');

        //$book = $request->sudent_data;

        if(count($book) >0 && $book[0] != "all")
        {
            $name = DB::table('leave_type')




                ->Where(function ($query) use($book,$search_colums ,$table_colums) {
                    /*for ($i = 0; $i < count($book); $i++){
                        for ($h = 0; $h < count($search_colums); $h++){
                            $query->orwhere($search_colums[$h], 'like',  '%' . $book[$i] .'%');
                        }
                    }*/
                    for ($i = 0; $i < count($book); $i++){
                        if($search_colums[$i] == "all")
                        {
                            $table_colums;
                            for ($h = 0; $h < count($table_colums); $h++){
                                $query->orwhere($table_colums[$h], 'like',  '%' . $book[$i] .'%');
                            }

                        }
                        else
                        {
                            $query->orwhere($search_colums[$i], 'like',  '%' . $book[$i] .'%');
                        }

                    }
                })
                ->orderBy('leave_type.leave_id','desc')
                ->get();

        }
        else
        {

            $name = DB::table('leave_type')
                ->orderBy('leave_type.leave_id','desc')
                ->get();
        }
        return response(["name"=>$name ,"colums"=> $table_colums,"showed_colums"=>$showed_colums,"book"=>$book,"searched_colums"=>$search_colums,"colums_type"=>$colums_type]);
    }

}

