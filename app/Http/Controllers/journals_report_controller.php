<?php

namespace App\Http\Controllers;


use App\Account;
use App\Class_form;
use App\Classes;
use App\CostCenter;
use App\Currency;
use App\Journal;
use App\Journal_document;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\PDF_HF;


class journals_report_controller extends Controller
{


    //-----------------------------table-------------------------//

    public function get_journals(Request $request)

    {




        $print_array = array();
        $array =  explode(',', $request->input('print_ids')[0]);

        foreach ($array as $single)
        {
            array_push($print_array, $single);
        }

        $query = DB::table('journals')
//                ->join('cost_centers','salary_scale.cost_id','=','cost_centers.Cost_id')
//                ->join('employee','departments.manager_id','=','employee.employee_user_id')
            ->wherein('journals.journal_id',$print_array)

            ->orderBy('journal_id','desc')
            ->get();






        $pdf = new PDF_HF('P','mm','A4');

        $pdf->AddPage();

        $pdf->AliasNbPages('{pages}');





//set font to arial, bold, 14pt

        $pdf->SetFont('Arial','B',14);

//Cell(width , height , text , border , end line , [align] )
        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX( 90);

        $pdf->Cell(40,10,"Journals Table",0,0,'C');
        $pdf->Ln(11);

        $pdf->setFillColor(230,230,230);

        $pdf->Ln(15);
        $pdf->Cell(60	,7,'ID',1,0,'C',1);
        $pdf->Cell(60	,7,'Journal Name',1,0,'C',1);
        $pdf->Cell(70	,7,'Journal Short Code',1,0,'C',1);

        /*-----------------------  change header -----------------------------*/
        $headrs_name = array(

            array("ID",60),
            array("Journal Name",60),
            array("journal_short Code",70),

        );
        $pdf->set_header_names($headrs_name);
        /*-------------------------------------------------------*/
        $pdf->SetFontSize(10);

        /*set the table header */
        $pdf->setHeaderTitle(true);

        /*------------------------------*/
        $pdf->SetWidths(array(60,60,70));
        $pdf->Ln(7);

        foreach ($query as $val)
        {

            $pdf->SetFont('Arial','B',12);

            $pdf->Row(array($val->journal_id,$val->Journal_name,$val->journal_short_code));

        }


        $pdf->Ln(5);
        $pdf->SetX( 40);

        /*remove the table header */
        $pdf->setHeaderTitle(false);
        /*------------------------------*/

        $pdf->Output();
        exit;


    }




    //----------------------------report-----------------------------//

    public function get_journals_rpt($id)

   {
       $query = DB::table('journal_documents')
           ->join('accounts','journal_documents.master_acc_id','=','accounts.child_id')
           ->join('currencies','journal_documents.currency_id','=','currencies.Currency_id')
           ->where('journal_documents.Journal_id','=',$id)
           ->orderBy('Document_id','asc')
           ->get();





      $depart = Journal::where('journal_id',$id)->first();
       $master =Journal_document::where('Journal_id',$id)->first()->master_acc_id;
       $doc =Journal_document::where('master_acc_id',$master)->first()->Document_Name;
     $doco =Journal_document::where('master_acc_id',$master)->first()->document_short_code	;
      $docou =Journal_document::where('master_acc_id',$master)->first()->debit_credit;
      $docoum =Journal_document::where('master_acc_id',$master)->first()->currency_id;
       if($docou == 1){
           $deb = "dr";
       }
       else{
           $deb = "cr";
       }
       $cuurency =Currency::where('Currency_id',$docou)->first()->child_name;
        if($cuurency == 1)
        {
     $curr = "$";
       }
        else
       {
           $curr ="LE";
        }

//
//
//
//       $manager = Journal_document::where('Journal_id',$depart->account_id)->first()->child_name;
//
//       $Fees = Fees_type::where('fee_id',$depart->Fees_type)->first()->fees_type;
//
//       $cost =Journal::where('journal_id',$depart->journal_id)->first()->Journal_name;



       $pdf = new PDF_HF('P','mm','A4');

       $pdf->AddPage();

       $pdf->AliasNbPages('{pages}');

//set font to arial, bold, 14pt

        $pdf->SetFont('Arial','B',12);

//Cell(width , height , text , border , end line , [align] )
        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX( 80);

       $pdf->Cell(40,10,"Journals NO:".' '.$depart->journal_id,0,0,'C');
        $pdf->Ln(11);

        $pdf->Ln(10);
        $pdf->SetX( 10);
        $pdf->Cell(15,10,'Journal Name',0,0,'L');
        $pdf->SetX(60);
        $pdf->SetFont('','');
        $pdf->Cell(15, 10, $depart->Journal_name, 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( 60);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,"",0,0,'L');
        $pdf->SetX(60);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');


        $pdf->Ln(10);
        $pdf->SetX( 10);
        $pdf->SetFont('','B');
        $pdf->Cell(15,10,'Short Name',0,0,'L');
        $pdf->SetX(60);
        $pdf->SetFont('','');
       $pdf->Cell(15, 10, $depart->journal_short_code	, 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( 60);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,"",0,0,'L');
        $pdf->SetX(60);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');


        $pdf->Ln(30);
        $pdf->SetX( 10);
        $pdf->SetFont('','BU');
        $pdf->Cell(15,10,'Document :',0,0,'L');


        $pdf->setFillColor(230, 230, 230);
        $pdf->SetFont('','B');
        $pdf->Ln(15);
        $pdf->Cell(40	,7,'Document Name',1,0,'C',1);
        $pdf->Cell(35	,7,'Short Name',1,0,'C',1);
        $pdf->Cell(40	,7,'Master Account',1,0,'C',1);
        $pdf->Cell(40	,7,'Transaction Type	',1,0,'C',1);
        $pdf->Cell(35	,7,'Currency',1,0,'C',1);
        /*-----------------------  change header -----------------------------*/
        $headrs_name = array(
            array("Document Name",40),
            array("Short Name",35),
            array("Master Account",40),
            array("Transaction Type",40),
            array("Currency",35)

        );
        $pdf->set_header_names($headrs_name);
        /*-------------------------------------------------------*/
        $pdf->SetFontSize(10);

        /*set the table header */
        $pdf->setHeaderTitle(true);

        /*------------------------------*/
        $pdf->SetWidths(array(40,35,40,40,35));
        $pdf->Ln(7);
   //     $print_array = array();
   //     $array =  explode(',', $request->input('print_ids')[0]);

//        foreach ($array as $single)
//        {
//            array_push($print_array, $single);
//        }
//        $query = DB::table('journal_documents')
//            ->join('currencies','journal_documents.Currency_id','=','currencies.Currency_id')
//            ->wherein('journal_documents.Journal_id','currencies.Currency_id')
////            ->wherein('class.classid',$print_array)
////            ->wherein('class.classid',$print_array)
//
//            ->orderBy('class.Costid','desc')
//            ->get();
//        foreach ($query as $val)
//        {
//            $pdf->Row(array($val->Document_Name));
//
////
//        }


        foreach ($query as $val)
        {


            $pdf->SetFont('Arial','B',12);
                if($val->debit_credit == 2)
                {
                    $cur = "LE";
                }
                elseif($val->debit_credit == 1){
                    $cur = "$";
                }
                if($val->currency_id == 2){
                    $tran = "Dr";
                }
                elseif ($val->currency_id == 1){
                    $tran = "Cr";
                }
            $pdf->Row(array($val->Document_Name,$val->document_short_code,$val->child_name,$tran,$cur));

        }
        $pdf->Ln(5);
        $pdf->SetX( 40);

        /*remove the table header */
        $pdf->setHeaderTitle(false);
        /*------------------------------*/

        $pdf->Output();
        exit;


    }




}