<?php

namespace App\Http\Controllers;


use App\User;
use App\Job;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;




class creat_job_controller extends Controller
{
    //
    /* public function ret_cost_page()
     {
         return view('admin_pages.cost_center_page');
     }*/
    public function creat_job()
    {
        $departments = DB::table('departments')->orderBy('dep_id', 'asc')
            ->pluck('dep_name', 'dep_id');

        return view('school_pages.job',compact('departments'));
    }

    public function add_new_job(Request $request)
    {

        $rules = array(

            'job_name' => 'required',
            'job_code' => 'required',
            'department' => 'required',


        );
        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), $rules);

        if ($validator->passes()) {

            /*------------- edit -----------------*/
            if ($request->current_po_id != "" && $request->current_po_id) {

                $department = Job::findOrFail($request->current_po_id);
                $department->job_name = $request->job_name;
                $department->job_short_code = $request->job_code;
                $department->department_id = $request->department;

                $department->save();

                $depart_id = $department->job_id;

                return response([$request, 'edit_po', 'current_po_id' => $depart_id]);
            } /*----------------- create new  ---------------*/
            else {
                $department = new Job();
                $department->job_name = $request->job_name;
                $department->job_short_code = $request->job_code;
                $department->department_id = $request->department;

                $department->save();

                $depart_id = $department->job_id;

                return response([$request, 'edit_po', 'current_po_id' => $depart_id]);
            }
        }


        elseif ($validator->fails()) {

            return response()->json(['error' => $validator->messages()]);

        }
    }



    public function delete_this_job(Request $request)
    {


        $job = Job::findOrFail($request->cur_po_id);
        $job->delete();

        return response($request);


    }

    public function get_this_job($id)
    {
        $departments = DB::table('departments')->orderBy('dep_id', 'asc')
            ->pluck('dep_name', 'dep_id');

        $job = Job::findOrFail($id);


        return view('school_pages.job',compact('departments','job'));

    }

    public function get_table_jobs()
    {
        $showed_search_list = array('ID','Job Name','Job Code','Department');
        $database_search_list = array(1,2,3,4);

        $jobs = DB::table('jobs')
            ->join('departments','jobs.department_id','=','departments.dep_id')

            ->orderBy('jobs.job_id','desc')
            ->get();


        return view('school_pages.show_jobs',['showed_search_list' => $showed_search_list,'database_search_list'=>$database_search_list],compact('jobs'));
    }

    public function search_jobs_table(Request $request)
    {

        $search_colums=array();
        $book=array();

        $string = $request->sudent_data;

        foreach ($string as $value) {

            list($k, $v) = explode('|', $value);
            if($v == "")
            {
                array_push($search_colums,"all");
                array_push($book,$k);
            }
            else{
                array_push($search_colums,$v);
                array_push($book,$k);

            }

        }

        //$search_colums = array('s_name','s_class','s_age');

        $showed_colums = array('ID','Job Name','Job Code','Department');
        $colums_type = array('input','input','input','input','input','input');
        $table_colums = array('job_id','job_name','job_short_code','dep_name');

        //$book = $request->sudent_data;

        if(count($book) >0 && $book[0] != "all")
        {
            $name = DB::table('jobs')
                ->join('departments','jobs.department_id','=','departments.dep_id')




                ->Where(function ($query) use($book,$search_colums ,$table_colums) {
                    /*for ($i = 0; $i < count($book); $i++){
                        for ($h = 0; $h < count($search_colums); $h++){
                            $query->orwhere($search_colums[$h], 'like',  '%' . $book[$i] .'%');
                        }
                    }*/
                    for ($i = 0; $i < count($book); $i++){
                        if($search_colums[$i] == "all")
                        {
                            $table_colums;
                            for ($h = 0; $h < count($table_colums); $h++){
                                $query->orwhere($table_colums[$h], 'like',  '%' . $book[$i] .'%');
                            }

                        }
                        else
                        {
                            $query->orwhere($search_colums[$i], 'like',  '%' . $book[$i] .'%');
                        }

                    }
                })
                ->orderBy('jobs.job_id','desc')
                ->get();

        }
        else
        {

            $name = DB::table('jobs')
                ->join('departments','jobs.department_id','=','departments.dep_id')

                ->orderBy('jobs.job_id','desc')
                ->get();
        }
        return response(["name"=>$name ,"colums"=> $table_colums,"showed_colums"=>$showed_colums,"book"=>$book,"searched_colums"=>$search_colums,"colums_type"=>$colums_type]);
    }


}
