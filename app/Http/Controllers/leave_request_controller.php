<?php

namespace App\Http\Controllers;


use App\User;
use App\Leave_type;
use App\Leave_request;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;



class leave_request_controller extends Controller
{
    //
    /* public function ret_cost_page()
     {
         return view('admin_pages.cost_center_page');
     }*/



    public function leave_request_page()
    {



        $emploee = DB::table('employee')
            ->join('departments','employee.depart_id','departments.dep_id')
            ->where('employee.depart_id','<>',null)
            ->get();


        $leave_types = DB::table('leave_type')->orderBy('leave_id', 'asc')
            ->get();

        $curr_date = Carbon::now('Africa/Cairo')->addHour()->toDateString();

        return view('school_pages.leave_request',compact('emploee','leave_types','curr_date'));
    }


    public function get_department(Request $request)
    {
        $department = DB::table('employee')
            ->join('departments','employee.depart_id','departments.dep_id')
            ->where('employee.depart_id','<>',null)
            ->where('employee.employee_id','=',$request->employee_id)
            ->pluck('dep_name');

        return response(['department'=>$department]);
    }

    public function check_date(Request $request)
    {

        $holiday = false;

        $start_date = Carbon::parse($request->start_date);
        $end_date = Carbon::parse($request->end_date);

        if($start_date->isFriday() || $end_date->isFriday() || $end_date->isSaturday() || $start_date->isSaturday())
        {
            $holiday = true;
        }


        return response (['holiday'=>$holiday]);
    }



    public function save_leave_req(Request $request)
    {


        $rules = array(

            'start_date' => 'required|date',
            'end_date' => 'required|date',
            'requested_date' => 'required|date',
            'employee' => 'required|int',
            'leave_type' => 'required|int',
            'duration' => 'required|int',


        );



        /*$eventsCount =0;*/
        /*if($request->duration_unit == 1)
        {*/
        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), $rules);
        if($validator->passes())
        {
            $begin = $request->start_date;
            $end = $request->end_date;

            if ($request->current_po_id != "" && $request->current_po_id) {
            $eventsCount =
                Leave_request::
                where('request_status_id', '<>', 6)
                    ->where('request_creator', '=', $request->cur_user_id)

                    ->where(function ($query) use ($begin, $end) {
                        $query->where(function ($q) use ($begin, $end) {
                            $q->where('leave_start', '>=', $begin)
                                ->where('leave_start', '<', $end);
                        })->orWhere(function ($q) use ($begin, $end) {
                            $q->where('leave_start', '<=', $begin)
                                ->where('leave_end', '>', $end);
                        })->orWhere(function ($q) use ($begin, $end) {
                            $q->where('leave_end', '>', $begin)
                                ->where('leave_end', '<=', $end);
                        })->orWhere(function ($q) use ($begin, $end) {
                            $q->where('leave_start', '>=', $begin)
                                ->where('leave_end', '<=', $end);
                        });
                    })->count();
            //$request->current_po_id
            $all_have_same_duration = Leave_request::
            where('request_status_id', '<>', 6)
                ->where('request_creator', '=', $request->cur_user_id)

                ->where(function ($query) use ($begin, $end) {
                    $query->where(function ($q) use ($begin, $end) {
                        $q->where('leave_start', '>=', $begin)
                            ->where('leave_start', '<', $end);
                    })->orWhere(function ($q) use ($begin, $end) {
                        $q->where('leave_start', '<=', $begin)
                            ->where('leave_end', '>', $end);
                    })->orWhere(function ($q) use ($begin, $end) {
                        $q->where('leave_end', '>', $begin)
                            ->where('leave_end', '<=', $end);
                    })->orWhere(function ($q) use ($begin, $end) {
                        $q->where('leave_start', '>=', $begin)
                            ->where('leave_end', '<=', $end);
                    });
                })->get();
            foreach ($all_have_same_duration as $single)
            {
                if($single->leave_r_id == $request->current_po_id)
                {
                    $eventsCount--;
                }
            }


        } else {
            $eventsCount =
                Leave_request::
                where('request_status_id', '<>', 6)
                    ->where('request_creator', '=', $request->cur_user_id)
                    ->where(function ($query) use ($begin, $end) {
                        $query->where(function ($q) use ($begin, $end) {
                            $q->where('leave_start', '>=', $begin)
                                ->where('leave_start', '<', $end);
                        })->orWhere(function ($q) use ($begin, $end) {
                            $q->where('leave_start', '<=', $begin)
                                ->where('leave_end', '>', $end);
                        })->orWhere(function ($q) use ($begin, $end) {
                            $q->where('leave_end', '>', $begin)
                                ->where('leave_end', '<=', $end);
                        })->orWhere(function ($q) use ($begin, $end) {
                            $q->where('leave_start', '>=', $begin)
                                ->where('leave_end', '<=', $end);
                        });
                    })->count();
        }

        //}
        $holiday = 0;

        $start_date = Carbon::parse($request->start_date);
        $end_date = Carbon::parse($request->end_date);

        if ($start_date->isFriday() || $end_date->isFriday() || $end_date->isSaturday() || $start_date->isSaturday()) {
            $holiday = 1;
        }




        if ($eventsCount == 0 && $holiday == 0) {

            /*------------- edit -----------------*/
            if ($request->current_po_id != "" && $request->current_po_id) {

                $department = Leave_request::findOrFail($request->current_po_id);

                $curr_date = Carbon::now('Africa/Cairo')->addHour();

                $curr_time = Carbon::now('Africa/Cairo')->addHour()->toTimeString();


                $department->employee_id = $request->employee;
                //$department->request_date = date('Y-m-d', strtotime($request->requested_date)) .' '. $curr_time;
                $department->leave_type_id = $request->leave_type;
                $department->leave_start = $request->start_date;
                $department->leave_end = $request->end_date;
                $department->leave_duration = $request->duration;

                $status = DB::table('document_status')->where('status_name', '=', $request->status)->first();
                $department->request_status_id = $status->status_id;
                //$department->request_creator = $request->cur_user_id;

                /*-------------------  days leave unit = 1  ---------------------------*/


                if ($request->status == 'approved_po') {

                    if (!$department->approved_date || $department->approved_date == null) {
                        $department->approved_date = $curr_date;
                    }

                    if (!$department->approved_by || $department->approved_by == null) {
                        $department->approved_by = $request->cur_user_id;
                    }

                }
                $department->description = $request->leave_description;


                $department->save();

                $depart_id = $department->leave_r_id;
                $approv_date = $department->approved_date;

                return response([$request, 'edit_po', 'current_po_id' => $depart_id, 'status' => $request->status, 'approv_date' => $approv_date]);
            } /*----------------- create new  ---------------*/
            else {
                $department = new Leave_request();

                $curr_date = Carbon::now('Africa/Cairo')->addHour();

                $curr_time = Carbon::now('Africa/Cairo')->addHour()->toTimeString();

                $status = DB::table('document_status')->where('status_name', '=', $request->status)->first();

                $department->employee_id = $request->employee;
                $department->request_date = $curr_date;

                $department->leave_type_id = $request->leave_type;
                $department->leave_start = $request->start_date;
                $department->leave_end = $request->end_date;
                $department->leave_duration = $request->duration;


                $department->request_status_id = $status->status_id;
                $department->request_creator = $request->cur_user_id;


                if ($request->status == 'approved_po') {


                    $department->approved_date = $curr_date;
                    $department->approved_by = $request->cur_user_id;


                }
                $department->description = $request->leave_description;


                $department->save();

                $depart_id = $department->leave_r_id;
                $approv_date = $department->approved_date;


                return response([$request, 'edit_po', 'current_po_id' => $depart_id, 'status' => $request->status, 'approv_date' => $approv_date]);
            }
        }


        elseif($holiday == 1)
        {
            return response()->json(['holiday' => 'days is holiday']);
        }
        elseif($eventsCount > 0)
        {
            return response()->json(['inturapt' => 'days inturupt with other holidays']);
        }
    }



        elseif ($validator->fails()) {

            return response()->json(['error' => $validator->messages()]);

        }
    }


    public function edit_leave_request_page($id)
    {

        $emploee = DB::table('employee')
            ->join('departments','employee.depart_id','departments.dep_id')
            ->where('employee.depart_id','<>',null)
            ->get();


        $leave_types = DB::table('leave_type')->orderBy('leave_id', 'asc')
            ->get();

        $curr_date = Carbon::now('Africa/Cairo')->addHour()->toDateString();

        $leave_request = DB::table('leave_request')
        ->join('document_status','leave_request.request_status_id','=','document_status.status_id')
        ->join('employee','leave_request.employee_id','=','employee.employee_id')
        ->join('departments','employee.depart_id','=','departments.dep_id')
        ->where('leave_request.leave_r_id','=',$id)
        ->first();

        return view('school_pages.leave_request',compact('emploee','leave_types','curr_date','leave_request'));

    }


    public function delete_leave_request_page(Request $request)
    {


        $leave_type = Leave_request::findOrFail($request->cur_po_id);
        $leave_type->delete();

        return response($request);


    }


    public function get_table_show_leave_request()
    {
        $showed_search_list = array('ID','Requested Date','Employee','Leave Type','From','To','Status');
        $database_search_list = array(1,2,3,4,5,6,7);


        $leave_requests = DB::table('leave_request')
            ->join('employee' ,'leave_request.employee_id','=','employee.employee_id')


            ->join('leave_type','leave_request.leave_type_id','=','leave_type.leave_id')
            ->join('document_status','leave_request.request_status_id','=','document_status.status_id')

            ->orderBy('leave_request.leave_r_id','desc')
            ->get();

        return view('school_pages.show_leave_request',['showed_search_list' => $showed_search_list,'database_search_list'=>$database_search_list],compact('leave_requests'));
    }


    public function search_leave_request_table(Request $request)
    {

        $search_colums=array();
        $book=array();

        $string = $request->sudent_data;

        foreach ($string as $value) {

            list($k, $v) = explode('|', $value);
            if($v == "")
            {
                array_push($search_colums,"all");
                array_push($book,$k);
            }
            else{
                array_push($search_colums,$v);
                array_push($book,$k);

            }

        }

        //$search_colums = array('s_name','s_class','s_age');

        $showed_colums = array('ID','Requested Date','Employee','Leave Type','From','To','Status');
        $colums_type = array('input','input','input','input','input','input');
        $table_colums = array('leave_r_id','request_date','employee_name','leave_name','leave_start','leave_end','status_short_name');

        //$book = $request->sudent_data;

        if(count($book) >0 && $book[0] != "all")
        {
            $name = DB::table('leave_request')
                ->join('employee','leave_request.employee_id','=','employee.employee_id')
                ->join('leave_type','leave_request.leave_type_id','=','leave_type.leave_id')
                ->join('document_status','leave_request.request_status_id','=','document_status.status_id')





                ->Where(function ($query) use($book,$search_colums ,$table_colums) {
                    /*for ($i = 0; $i < count($book); $i++){
                        for ($h = 0; $h < count($search_colums); $h++){
                            $query->orwhere($search_colums[$h], 'like',  '%' . $book[$i] .'%');
                        }
                    }*/
                    for ($i = 0; $i < count($book); $i++){
                        if($search_colums[$i] == "all")
                        {
                            $table_colums;
                            for ($h = 0; $h < count($table_colums); $h++){
                                $query->orwhere($table_colums[$h], 'like',  '%' . $book[$i] .'%');
                            }

                        }
                        else
                        {
                            $query->orwhere($search_colums[$i], 'like',  '%' . $book[$i] .'%');
                        }

                    }
                })
                ->orderBy('leave_request.leave_r_id','desc')
                ->get();

        }
        else
        {

            $name = DB::table('leave_request')
                ->join('employee' ,'leave_request.employee_id','=','employee.employee_id')


                ->join('leave_type','leave_request.leave_type_id','=','leave_type.leave_id')
                ->join('document_status','leave_request.request_status_id','=','document_status.status_id')

                ->orderBy('leave_request.leave_r_id','desc')
                ->get();

        }
        return response(["name"=>$name ,"colums"=> $table_colums,"showed_colums"=>$showed_colums,"book"=>$book,"searched_colums"=>$search_colums,"colums_type"=>$colums_type]);
    }


    public function delete_all_selected_leave_req(Request $request)
    {
        $deleted = array();
        foreach ($request->deleted_user_ids as $class_id_one)
        {
            $user = Leave_request::findOrFail($class_id_one);
            if($user->request_status_id == 1)
            {
                $user->delete();
                array_push($deleted,1);

            }
            else
            {
                array_push($deleted,0);
            }
            /* elseif($user->po_status_id == 2)
             {
                 $user->po_canceled = 1;
                 $user->po_status_id = 5;
                 $user->save();
                 array_push($deleted,0);
             }*/

        }

        return response(['deleted_ids'=>$request->deleted_user_ids,'deleted'=>$deleted]);
    }




}

