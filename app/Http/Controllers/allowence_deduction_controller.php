<?php

namespace App\Http\Controllers;


use App\Allowence_deduction;
use App\Allowence_Details;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;


class allowence_deduction_controller extends Controller
{
    //
    /* public function ret_cost_page()
     {
         return view('admin_pages.cost_center_page');
     }*/
    public function get_page()
    {
        $accounts = DB::table('accounts')->where('Level_NO','=',3)
            ->pluck('Child_name', 'Child_id');

        $acadimic_year = DB::table('acadimic_year')
            ->pluck('year_name', 'acadimic_id');

        return view('school_pages.allowence_deduct',compact('accounts','acadimic_year'));
    }


    public function get_b_type(Request $request)
    {
        $item_type = DB::table('Beneficiary_type')->orderBy('Beneficiary_id', 'asc')->get();
        return response()->json($item_type);

    }

    public function get_currency(Request $request)
    {
        $item_type = DB::table('currencies')->orderBy('Currency_id', 'asc')->get();
        return response()->json($item_type);

    }



    public function get_salary(Request $request)
    {
        $selected_v_ids = array();
        $cur_selected = $request->current_selected_value;

        if($request->all_selected_months != "") {
            foreach ($request->all_selected_months as $single_select) {
                array_push($selected_v_ids, $single_select);

            }



            if (count($selected_v_ids) > 0) {
                $months = DB::table('salary_scale')
                    ->orderBy('salary_id', 'asc')
                    ->where(function ($query) use ($selected_v_ids) {
                        for ($h = 0; $h < count($selected_v_ids); $h++) {
                            $query->where('salary_id', '<>', $selected_v_ids[$h]);
                        }
                    })
                    ->get();


                if($request->current_selected_value && $request->current_selected_value != "")
                {
                    $selected_months = DB::table('salary_scale')
                        ->orderBy('salary_id', 'asc')
                        ->where(function ($query) use ($selected_v_ids,$cur_selected) {
                            for ($h = 0; $h < count($selected_v_ids); $h++) {
                                if($selected_v_ids[$h] == $cur_selected)
                                {
                                    $query->where('salary_id', '=', $selected_v_ids[$h]);
                                }

                            }
                        })
                        ->get();
                }
                else
                {
                    $selected_months="";
                }


            }
        }else {
            $months = DB::table('salary_scale')
                ->orderBy('salary_id', 'asc')
                ->get();


            if($request->current_selected_value && $request->current_selected_value != "")
            {
                $selected_months = DB::table('salary_scale')
                    ->orderBy('salary_id', 'asc')
                    ->where(function ($query) use ($selected_v_ids,$cur_selected) {
                        for ($h = 0; $h < count($selected_v_ids); $h++) {
                            if($selected_v_ids[$h] == $cur_selected)
                            {
                                $query->where('salary_id', '=', $selected_v_ids[$h]);
                            }

                        }
                    })
                    /*->where('salary_id', '=', $cur_selected)*/
                    ->get();
            }
            else
            {
                $selected_months="";
            }
        }

        $data['data'] = $months;
        $data['extra_v'] = $selected_months;


        return response()->json($data);


    }


    public function save_new(Request $request)
    {

        $rules = array(

            'Type' => 'required',
            /*Rule::unique('allowence_deduction')->ignore($request->current_po_id,'allow_id')*/
            'Name' => ['required','unique:allowence_deduction,allow_name,'.$request->current_po_id.',allow_id'],
            'Short_Name' => ['required','unique:allowence_deduction,allow_short_code,'.$request->current_po_id.',allow_id'],
            //'Name' => 'required|unique:allowence_deduction,allow_name'.$request->current_po_id,
            //'Short_Name' => 'required|unique:allowence_deduction,allow_short_code'.$request->current_po_id,
            'Account_Name' => 'required',


        );
        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), $rules);

        if ($validator->passes()) {

            /*------------- edit -----------------*/
            if ($request->current_po_id != "" && $request->current_po_id) {

                $department = Allowence_deduction::findOrFail($request->current_po_id);
                $department->allow_type = $request->Type;
                $department->allow_name = $request->Name;
                $department->allow_short_code = $request->Short_Name;
                $department->account_id = $request->Account_Name;

                $department->save();

                $depart_id = $department->allow_id;

                return response([$request, 'edit_po', 'current_po_id' => $depart_id]);
            } /*----------------- create new  ---------------*/
            else {
                $department = new Allowence_deduction();
                $department->allow_type = $request->Type;
                $department->allow_name = $request->Name;
                $department->allow_short_code = $request->Short_Name;
                $department->account_id = $request->Account_Name;

                $department->save();

                $depart_id = $department->allow_id;

                return response([$request, 'edit_po', 'current_po_id' => $depart_id]);
            }
        }


        elseif ($validator->fails()) {

            return response()->json(['error' => $validator->messages()]);

        }



    }


    public function save_table_row(Request $request)
    {
        $rules = array(

            'Acadimic_year' => 'required',

        );
        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), $rules);

        if ($validator->passes()) {

            if ($request->row_id != "" && $request->row_id) {


                $row = $request->table_rows[0];


                $po_details = Allowence_Details::findOrFail($request->row_id);

                $po_details->allow_id = $row['allow_or_deduction'];
                $po_details->acadimic_id = $request->Acadimic_year;

                $po_details->salary_id = $row['salary_scale'];
                $po_details->currency_id = $row['currency'];
                $po_details->value = $row['allow_value'];
                $po_details->benficinary_id = $row['beneficinary_type'];

                $po_details->save();


                $salrary_plane_id = $po_details->allow_details_id;

                return response([$request, 'just_add_row_id' => $salrary_plane_id]);
            }
            else{
                $row = $request->table_rows[0];


                $po_details = new Allowence_Details();


                $po_details->allow_id = $row['allow_or_deduction'];
                $po_details->acadimic_id = $request->Acadimic_year;

                $po_details->salary_id = $row['salary_scale'];
                $po_details->currency_id = $row['currency'];
                $po_details->value = $row['allow_value'];
                $po_details->benficinary_id = $row['beneficinary_type'];

                $po_details->save();


                $salrary_plane_id = $po_details->allow_details_id;

                return response([$request, 'just_add_row_id' => $salrary_plane_id]);
            }

        }
        elseif ($validator->fails()) {

            return response()->json(['error' => $validator->messages()]);

        }
    }

    public function get_table_on_drop_down(Request $request)
    {
        $allow_details = DB::table('allowence_details')
            ->join('Beneficiary_type','allowence_details.benficinary_id','=','Beneficiary_type.Beneficiary_id')
            ->join('sub_salary','allowence_details.salary_id','=','sub_salary.sub_id')
            ->join('currencies','allowence_details.currency_id','=','currencies.Currency_id')

            ->join('allowence_deduction','allowence_details.allow_id','=','allowence_deduction.allow_id')
            ->join('allowence_deduction_types','allowence_deduction.allow_type','=','allowence_deduction_types.allow_type_id')

            //->where('allowence_details.allow_id', $request->allow_id)
            ->where('allowence_details.acadimic_id', $request->Acadimic_year)
            ->get();

        return response(['allow_detailes'=>$allow_details]);

    }


    public function edit_allownce($id)
    {
        $accounts = DB::table('accounts')->where('Level_NO','=',3)
            ->pluck('Child_name', 'Child_id');

        $acadimic_year = DB::table('acadimic_year')
            ->pluck('year_name', 'acadimic_id');

        $allow_master = Allowence_deduction::findOrFail($id);

        return view('school_pages.allowence_deduct',compact('accounts','acadimic_year','allow_master'));


    }


    public function delete_allow(Request $request)
    {

        $master_po = Allowence_deduction::findOrFail($request->cur_po_id);

        $master_po->delete();

        DB::table('allowence_details')
            ->where('allow_id', $request->cur_po_id)
            ->delete();

        return response($request);



    }

    public function allowence_deduction_table()
    {
        $showed_search_list = array('ID','Type','Name','Short Name');
        $database_search_list = array(1,2,3,4);

        $allows = DB::table('allowence_deduction')
            ->join('allowence_deduction_types','allowence_deduction.allow_type','=','allowence_deduction_types.allow_type_id')


            ->orderBy('allowence_deduction.allow_id','desc')
            ->get();

        return view('school_pages.show_allowence',['showed_search_list' => $showed_search_list,'database_search_list'=>$database_search_list],compact('allows'));
    }

    public function search_allow_table(Request $request)
    {

        $search_colums=array();
        $book=array();

        $string = $request->sudent_data;

        foreach ($string as $value) {

            list($k, $v) = explode('|', $value);
            if($v == "")
            {
                array_push($search_colums,"all");
                array_push($book,$k);
            }
            else{
                array_push($search_colums,$v);
                array_push($book,$k);

            }

        }

        //$search_colums = array('s_name','s_class','s_age');

        $showed_colums = array('ID','Type','Name','Short Name');
        $colums_type = array('input','input','input','input','input','input');
        $table_colums = array('allow_id','allow_type_name','allow_name','allow_short_code');

        //$book = $request->sudent_data;

        if(count($book) >0 && $book[0] != "all")
        {
            $name = DB::table('allowence_deduction')
                ->join('allowence_deduction_types','allowence_deduction.allow_type','=','allowence_deduction_types.allow_type_id')




                ->Where(function ($query) use($book,$search_colums ,$table_colums) {
                    /*for ($i = 0; $i < count($book); $i++){
                        for ($h = 0; $h < count($search_colums); $h++){
                            $query->orwhere($search_colums[$h], 'like',  '%' . $book[$i] .'%');
                        }
                    }*/
                    for ($i = 0; $i < count($book); $i++){
                        if($search_colums[$i] == "all")
                        {
                            $table_colums;
                            for ($h = 0; $h < count($table_colums); $h++){
                                $query->orwhere($table_colums[$h], 'like',  '%' . $book[$i] .'%');
                            }

                        }
                        else
                        {
                            $query->orwhere($search_colums[$i], 'like',  '%' . $book[$i] .'%');
                        }

                    }
                })
                ->orderBy('allowence_deduction.allow_id','desc')
                ->get();

        }
        else
        {

            $name = DB::table('allowence_deduction')
                ->join('allowence_deduction_types','allowence_deduction.allow_type','=','allowence_deduction_types.allow_type_id')


                ->orderBy('allowence_deduction.allow_id','desc')
                ->get();
        }
        return response(["name"=>$name ,"colums"=> $table_colums,"showed_colums"=>$showed_colums,"book"=>$book,"searched_colums"=>$search_colums,"colums_type"=>$colums_type]);
    }



    public function get_allowence_details()
    {
        $acadimic_year = DB::table('acadimic_year')
            ->pluck('year_name', 'acadimic_id');

        $allow_type = DB::table('allowence_deduction_types')
            ->pluck('allow_type_name', 'allow_type_id');

        $currency = DB::table('currencies')
            ->pluck('Currency_name', 'Currency_id');


        $sub_salary_plan = DB::table('sub_salary')
            ->pluck('sub_name', 'sub_id');

        $Beneficiary_type = DB::table('Beneficiary_type')
            ->pluck('Beneficiary_name', 'Beneficiary_id');

        $allow_deduction = DB::table('allowence_deduction')
            ->get();

        return view('school_pages.allowence_details',compact('acadimic_year','allow_deduction','allow_type','currency','Beneficiary_type','sub_salary_plan'));

    }

    public function return_allowence_details($id)
    {
        $acadimic_year = DB::table('acadimic_year')
            ->pluck('year_name', 'acadimic_id');

        $allow_type = DB::table('allowence_deduction_types')
            ->pluck('allow_type_name', 'allow_type_id');

        $currency = DB::table('currencies')
            ->pluck('Currency_name', 'Currency_id');


        $sub_salary_plan = DB::table('sub_salary')
            ->pluck('sub_name', 'sub_id');

        $Beneficiary_type = DB::table('Beneficiary_type')
            ->pluck('Beneficiary_name', 'Beneficiary_id');

        $allow_deduction = DB::table('allowence_deduction')
            ->get();

        $allow_details = DB::table('allowence_details')
            ->join('Beneficiary_type','allowence_details.benficinary_id','=','Beneficiary_type.Beneficiary_id')
            ->join('sub_salary','allowence_details.salary_id','=','sub_salary.sub_id')
            ->join('currencies','allowence_details.currency_id','=','currencies.Currency_id')

            ->join('allowence_deduction','allowence_details.allow_id','=','allowence_deduction.allow_id')
            ->join('allowence_deduction_types','allowence_deduction.allow_type','=','allowence_deduction_types.allow_type_id')

            //->where('allowence_details.allow_id', $request->allow_id)
            ->where('allowence_details.acadimic_id', $id)
            ->get();


        $year_id = $id ;


        return view('school_pages.allowence_details',compact('acadimic_year','allow_deduction','allow_type','currency','Beneficiary_type','sub_salary_plan','allow_details','year_id'));
    }

    public function delete_allow_deduction_details(Request $request)
    {
        $rules = array(

            'row' => 'exists:allowence_details,allow_details_id',

        );
        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), $rules);
        if ($validator->passes()) {

            $fee = Allowence_Details::findOrFail($request->row);
            $fee->delete();

            return response($request);
        }

        elseif ($validator->fails()) {

            return response()->json(['error' => $validator->messages()]);

        }
    }


    /*-------------------  student table  --------------------------*/
    public function scale_of_allowence_table()
    {
        $showed_search_list = array('Academic Year');
        $database_search_list = array(1);

        $salary_plans = DB::table('acadimic_year')
            //->join('salay_plan','acadimic_year.acadimic_id','salay_plan.acadimic_id')
            ->select('acadimic_year.acadimic_id','acadimic_year.year_name')
            ->groupBy('acadimic_year.acadimic_id','acadimic_year.year_name')
            ->get();

        /*dd($salary_plans);*/
        return view('school_pages.show_scale_allowence',['showed_search_list' => $showed_search_list,'database_search_list'=>$database_search_list],compact('salary_plans'));
    }

}
