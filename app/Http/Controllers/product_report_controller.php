<?php

namespace App\Http\Controllers;

use App\Account;
use App\Borrowing;
use App\Currency;
use App\products;
use App\ProductType;
use App\supplier_product;
use App\Vendor;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\PDF_HF;


class product_report_controller extends Controller
{
    //-----------------------------table-------------------------//
    public function get_product(Request $request)

    {

        $print_array = array();
        $array =  explode(',', $request->input('print_ids')[0]);

        foreach ($array as $single)
        {
            array_push($print_array, $single);
        }


        $query = DB::table('products')
            ->leftjoin('products_type', 'products_type.pro_type_id', '=', 'products.pro_type_id')
            ->leftjoin('accounts', 'accounts.Child_id', '=', 'products.account_id')
            ->whereIn('pro_id',$print_array)
            ->get();


        $pdf = new PDF_HF('P','mm','A4');

        $pdf->AddPage();

        $pdf->AliasNbPages('{pages}');

//set font to arial, bold, 14pt

        $pdf->SetFont('Arial','B',12);

//Cell(width , height , text , border , end line , [align] )
        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX( 90);

        $pdf->Cell(40,10,"Products Table",0,0,'C');
        $pdf->Ln(11);

        $pdf->setFillColor(230,230,230);


        $pdf->Ln(15);
        $pdf->Cell(20	,7,'ID',1,0,'C',1);
        $pdf->Cell(40	,7,'Product Name',1,0,'C',1);
        $pdf->Cell(50	,7,'Product Short Name',1,0,'C',1);
        $pdf->Cell(40	,7,'Product Type',1,0,'C',1);
        $pdf->Cell(40	,7,'Incom Account',1,0,'C',1);
        /*-----------------------  change header -----------------------------*/
        $headrs_name = array(
            array("ID",20),
            array("Product Name",40),
            array("Product Short Name",50),
            array("Product Type",40),
            array("Incom Account",40),
        );
        $pdf->set_header_names($headrs_name);
        /*-------------------------------------------------------*/
        $pdf->SetFontSize(10);

        /*set the table header */
        $pdf->setHeaderTitle(true);

        /*------------------------------*/
        $pdf->SetWidths(array(20,40,50,40,40));
        $pdf->Ln(7);

        foreach ($query as $val)
        {

            $pdf->SetFont('Arial','B',12);

            $pdf->Row(array($val->pro_id,$val->pro_name,$val->product_short_name,$val->pro_type_name,$val->Child_name));

        }


        $pdf->Ln(5);
        $pdf->SetX( 40);

        /*remove the table header */
        $pdf->setHeaderTitle(false);
        /*------------------------------*/

        $pdf->Output();
        exit;


    }


    //----------------------------reportt-----------------------------//


    public function get_product_rpt($id)

    {
        $product = DB::table('products')
            ->join('products_type', 'products_type.pro_type_id', '=', 'products.pro_type_id')
            ->join('accounts', 'accounts.Child_id', '=', 'products.account_id')
            ->where('pro_id','=',$id)
            ->first();

        $product_details = DB::table('supplier_product')
            ->join('vendors','supplier_product.supplier_id','=','vendors.VendorID')
            ->join('currencies','supplier_product.currency_id','=','currencies.Currency_id')

            ->where('supplier_product.product_id','=',$id)

            ->get();


        $pdf = new PDF_HF('P','mm','A4');

        $pdf->AddPage();

        $pdf->AliasNbPages('{pages}');

//set font to arial, bold, 14pt

        $pdf->SetFont('Arial','B',12);

//Cell(width , height , text , border , end line , [align] )
        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX( 80);




        $pdf->Cell(40,10,"Products NO : $product->pro_id",0,0,'C');


        $pdf->Ln(10);
        $pdf->SetX( 10);
        $pdf->SetFont('','BU');
        $pdf->Cell(15,10,'Product Details:',0,0,'L');




        $pdf->Ln(15);
        $pdf->SetX( 10);
        $pdf->SetFont('','B');

        $pdf->Cell(15,10,'Product Name',0,0,'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( 60);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,$product->pro_name,0,0,'L');
        $pdf->SetX(60);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');


        $pdf->Ln(10);
        $pdf->SetX( 10);
        $pdf->SetFont('','B');
        $pdf->Cell(15,10,'Short Name',0,0,'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( 60);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,$product->product_short_name,0,0,'L');
        $pdf->SetX(60);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');

        $pdf->Ln(10);
        $pdf->SetX( 10);
        $pdf->SetFont('','B');
        $pdf->Cell(15,10,'Product Type',0,0,'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( 60);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,$product->pro_type_name,0,0,'L');
        $pdf->SetX(60);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');

        $pdf->Ln(10);
        $pdf->SetX( 10);
        $pdf->SetFont('','B');
        $pdf->Cell(15,10,'Income Account',0,0,'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( 60);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,$product->Child_name,0,0,'L');
        $pdf->SetX(60);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');

        $pdf->Ln(30);
        $pdf->SetX( 10);
        $pdf->SetFont('','BU');
        $pdf->Cell(15,10,'Suppliers:',0,0,'L');






        $pdf->setFillColor(230, 230, 230);
        $pdf->SetFont('','B');
        $pdf->Ln(15);
        $pdf->Cell(70	,7,'Supplier	',1,0,'C',1);
        $pdf->Cell(60	,7,'Cost Price		',1,0,'C',1);
        $pdf->Cell(60	,7,'Currency	',1,0,'C',1);

        /*-----------------------  change header -----------------------------*/
        $headrs_name = array(
            array("Supplier	",70),
            array("Cost Price		",60),
            array("Currency	",60),

        );
        $pdf->set_header_names($headrs_name);
        /*-------------------------------------------------------*/
        $pdf->SetFontSize(10);

        /*set the table header */
        $pdf->setHeaderTitle(true);

        /*------------------------------*/
        $pdf->SetWidths(array(70,60,60));
        $pdf->Ln(7);

        foreach ($product_details as $val)
        {


            $pdf->Row(array($val->VendorName,$val->cost_price,$val->Currency_name));

        }

        $pdf->Ln(5);
        $pdf->SetX( 40);

        /*remove the table header */
        $pdf->setHeaderTitle(false);
        /*------------------------------*/

        $pdf->Output();
        exit;


    }






}