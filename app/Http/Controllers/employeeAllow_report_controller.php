<?php

namespace App\Http\Controllers;


use App\AcademicYear;

use App\Allowence_deduction;
use App\Classes;
use App\cost_centers;
use App\countries;
use App\Employee;
use App\EmployeeAllowence;
use App\EmployeeFamily;
use App\EmployeeSalary;
use App\Job;
use App\Nationality;
use App\nationalty_type;
use App\parents;
use App\relations_students_parents;
use App\students;

use App\SubSalary;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;

use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\PDF_HF;
use Image;



class employeeAllow_report_controller extends Controller
{



    //-----------------------------------TABLE-----------------------//
    public function get_employeeAllow(Request $request)

    {

        $array1 =  explode(',', $request->input('print_ids1')[0]);
        $array2 =  explode(',', $request->input('print_ids2')[0]);



        $pdf = new PDF_HF('P','mm','A4');

        $pdf->AddPage();

        $pdf->AliasNbPages('{pages}');

//set font to arial, bold, 14pt

        $pdf->SetFont('Arial','B',12);

//Cell(width , height , text , border , end line , [align] )
        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX( 90);

        $pdf->Cell(40,10,"Employees Table",0,0,'C');
        $pdf->Ln(11);

        $pdf->setFillColor(230,230,230);


        $pdf->Ln(15);
        $pdf->Cell(100	,7,'Employee Name',1,0,'C',1);
        $pdf->Cell(90	,7,'Academic Year',1,0,'C',1);



        /*-----------------------  change header -----------------------------*/
        $headrs_name = array(

            array("Employee Name",100),
            array("Academic Year",90),



        );
        $pdf->set_header_names($headrs_name);
        /*-------------------------------------------------------*/
        $pdf->SetFontSize(10);

        /*set the table header */
        $pdf->setHeaderTitle(true);

        /*------------------------------*/
        $pdf->SetWidths(array(100,90));
        $pdf->Ln(7);

        for($i=0;$i<count($array1);$i++)
        {

            $pdf->SetFont('Arial','B',12);
            $pdf->Row(array(Employee::find($array1[$i])->employee_name,AcademicYear::find($array2[$i])->year_name));

        }


        $pdf->Ln(5);
        $pdf->SetX( 40);

        /*remove the table header */
        $pdf->setHeaderTitle(false);
        /*------------------------------*/

        $pdf->Output();
        exit;


    }






    //-----------------------------report-------------------------//

    public function get_employeeAllow_rpt($empid, $year_id)

    {

        $emp = Employee::find($empid);
        $academic_year = AcademicYear::find($year_id);
        $query = EmployeeAllowence::where('employee_id',$empid)
            ->where('academic_year_id',$year_id)->get();

        $pdf = new PDF_HF('P','mm','A4');

        $pdf->AddPage();

        $pdf->AliasNbPages('{pages}');

////set font to arial, bold, 14pt

        $pdf->SetFont('Arial','B',12);

////Cell(width , height , text , border , end line , [align] )
        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX( 80);

        $pdf->Cell(40,10,"Employee Allowance For:".$empid,0,'C');
        $pdf->Ln(11);

        $pdf->Ln(10);
        $pdf->SetX( 10);
        $pdf->Cell(15,10,'Employee Name',0,'L');
        $pdf->SetX(60);
        $pdf->SetFont('','');
        $pdf->Cell(15, 10, $emp->employee_name.' '.$emp->middle_name.' '.$emp->last_name.' '.$emp->family_name, 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( 60);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,"",0,0,'L');
        $pdf->SetX(60);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');


        $pdf->Ln(10);
        $pdf->SetX( 10);
        $pdf->SetFont('','B');
        $pdf->Cell(15,10,'Academic Year',0,0,'L');
        $pdf->SetX(60);
        $pdf->SetFont('','');
        $pdf->Cell(15, 10,$academic_year->year_name , 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( 60);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,"",0,0,'L');
        $pdf->SetX(60);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');


        $pdf->Ln(30);
        $pdf->SetX( 10);
        $pdf->SetFont('','BU');
        $pdf->Cell(15,10,'Allowance',0,0,'L');





        $pdf->setFillColor(230, 230, 230);
        $pdf->SetFont('','B');
        $pdf->Ln(15);
        $pdf->Cell(45	,7,'Allowance',1,0,'C',1);
        $pdf->Cell(50	,7,'From',1,0,'C',1);
        $pdf->Cell(50	,7,'To',1,0,'C',1);
        $pdf->Cell(45	,7,'Value',1,0,'C',1);

        /*-----------------------  change header -----------------------------*/
        $headrs_name = array(
            array("Allowance",45),
            array("From",50),
            array("To",50),
            array("Value",45),
        );
        $pdf->set_header_names($headrs_name);
        /*-------------------------------------------------------*/
        $pdf->SetFontSize(10);
        //        /*set the table header */
        $pdf->setHeaderTitle(true);

        /*------------------------------*/
        $pdf->SetWidths(array(45,50,50,45));
        $pdf->Ln(7);



        foreach ($query as $val)
        {

            $pdf->SetFont('Arial','',12);

            $pdf->Row(array(Allowence_deduction::find($val->allow_id)->allow_name,$val->from_date,$val->to_date,$val->value));

        }

        $pdf->Ln(5);
        $pdf->SetX( 40);

        /*remove the table header */
        $pdf->setHeaderTitle(false);
        /*------------------------------*/



        $pdf->Output();
            exit;

    }


}