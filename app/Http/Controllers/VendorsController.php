<?php

namespace App\Http\Controllers;

use App\Currency;
use App\Employee;
use App\Vendor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class VendorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $showed_search_list = array('ID','Vendor Code','Vendor Name','Account Name');

        $vendors = DB::table('vendors')
            ->leftJoin('accounts','vendors.account_id','=','accounts.child_id')
            ->orderBy('vendors.VendorID','desc')
            ->get();

        $database_search_list = array(1,2,3,4);

        return view('school_pages.vendors.index',['showed_search_list' => $showed_search_list,'database_search_list'=>$database_search_list],compact('vendors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        /*------------ vendor page -------------------*/

        $accounts = DB::table('accounts')->where('Level_NO',3)->orderBy('Child_id', 'asc')
            ->pluck('Child_name', 'Child_id')->all();

        $employees = Employee::pluck('employee_name','employee_id')->all();
        $currency = Currency::pluck('Currency_name','Currency_id')->all();

        return view('school_pages.vendors.create',compact('employees','accounts','currency'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


//        dd($request->all());

        $rules = [
            'vendor_code' => 'required|unique:vendors,VendorCode',
            'vendor_name' => 'required|unique:vendors,VendorName',
            'arabic_vendor_name' => 'required',
            'acc_name' => 'required',
            'currency' => 'required',


        ];
        $validator = Validator::make($request->all(), $rules);
        $validator->SetAttributeNames([
            'vendor_code' => 'Vendor Code',
            'vendor_name' => 'English Vendor Name',
            'arabic_vendor_name' => 'Arabic Vendor Name',
            'acc_name' => 'Account Name',
            'currency' => 'Currency Type',

        ]);
        if ($validator->fails()) {

//            session()->flash('error', 'failed');
//            dd(back()->withInput()->withErrors($validator));
            return back()->withInput()->withErrors($validator);

        } else {
            try {
                DB::beginTransaction();

                $vendor = new Vendor();
                $vendor->VendorCode =$request->vendor_code;
                $vendor->CurrencyID =$request->currency;
                $vendor->VendorNameAR =$request->arabic_vendor_name;
                $vendor->VendorName =$request->vendor_name;
                $vendor->VendorAddressAR =$request->address_ar;
                $vendor->VendorAddress =$request->address;
                $vendor->website =$request->url;
                $vendor->Email =$request->email;
                $vendor->ContactSchoolPerson =$request->contact_school_person;
                $vendor->ContactVendorPerson =$request->contact_vendor_person;
                $vendor->Phone1 =$request->phonenum;
                $vendor->Phone2 =$request->phonenum2;
                $vendor->TaxFileNumber =$request->tax_file_number;
                $vendor->TaxOfficeName =$request->tax_office_name;
                $vendor->TaxOfficeCode =$request->tax_office_code;
                $vendor->CommercialRegNo =$request->reg_code;
                $vendor->TaxRegNumber =$request->tax_reg_number;
                $vendor->account_id =$request->acc_name;
                $vendor->SalesTax =$request->tax_reg_number;
                $vendor->save();
                $succsess = 1;

                DB::commit();
                session()->flash('success', 'created');
            }catch (Exception $ex){
                DB::rollBack();
                session()->flash('error', 'failed');
                return back();
            }

            return redirect('vendors/'.$vendor->VendorID.'/edit')->with(['succsess'=>$succsess]);


        }


//                $vendor->BankBeneficiaryName =$request->vendor_code;
//                $vendor->BanlAddress =$request->vendor_code;
//                $vendor->Person_type_ID =$request->vendor_code;
//                $vendor->TaxExempted =$request->vendor_code;
//                $vendor->TaxInAdvance =$request->vendor_code;
//                $vendor->BankName =$request->vendor_code;
//                $vendor->BankSortCode =$request->vendor_code;
//                $vendor->SalesTaxRegNo =$request->vendor_code;
//                $vendor->BankBranchName =$request->vendor_code;
//                $vendor->SupplierTransactionType =$request->vendor_code;
//                $vendor->TransactionType =$request->vendor_code;
//                $vendor->SMSPhone =$request->vendor_code;
//                $vendor->ServiceTypeID =$request->vendor_code;
//                $vendor->BankID =$request->vendor_code;
//                $vendor->BankBranchID =$request->vendor_code;
//                $vendor->BankAccountNo =$request->vendor_code;
//                $vendor->BankSwiftCode =$request->vendor_code;
//                $vendor->BankIBANCode =$request->vendor_code;








    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vendor = Vendor::findorfail($id);
        $succsess = Session::get('succsess');
        $accounts = DB::table('accounts')->where('Level_NO',3)->orderBy('Child_id', 'asc')
            ->pluck('Child_name', 'Child_id')->all();

        $employees = Employee::pluck('employee_name','employee_id')->all();
        $currency = Currency::pluck('Currency_name','Currency_id')->all();
        if ($succsess == 1)
        {
            session()->flash('success', 'Updated');
            return view('school_pages.vendors.edit',compact('succsess','vendor','accounts','employees','currency'));

        }
        else{
            return view('school_pages.vendors.edit',compact('vendor','accounts','employees','currency'));

        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'vendor_code' => ['required','unique:vendors,VendorCode,'.$id.',VendorID'],
            'vendor_name' => ['required','unique:vendors,VendorName,'.$id.',VendorID'],
            'arabic_vendor_name' => 'required',
            'acc_name' => 'required',
            'currancy' => 'required',


        ];
        $validator = Validator::make($request->all(), $rules);
        $validator->SetAttributeNames([
            'vendor_code' => 'Vendor Code',
            'vendor_name' => 'English Vendor Name',
            'arabic_vendor_name' => 'Arabic Vendor Name',
            'acc_name' => 'Account Name',
            'currancy' => 'Currency Type',



        ]);
        if ($validator->fails()) {

            return back()->withInput()->withErrors($validator->messages());

        } else {
            try {
                DB::beginTransaction();

                $vendor =  Vendor::findorfail($id);
                $vendor->VendorCode =$request->vendor_code;
                $vendor->VendorNameAR =$request->arabic_vendor_name;
                $vendor->CurrencyID =$request->currancy;
                $vendor->VendorName =$request->vendor_name;
                $vendor->VendorAddressAR =$request->address_ar;
                $vendor->VendorAddress =$request->address;
                $vendor->website =$request->url;
                $vendor->Email =$request->email;
                $vendor->ContactSchoolPerson =$request->contact_school_person;
                $vendor->ContactVendorPerson =$request->contact_vendor_person;
                $vendor->Phone1 =$request->phonenum;
                $vendor->Phone2 =$request->phonenum2;
                $vendor->TaxFileNumber =$request->tax_file_number;
                $vendor->TaxOfficeName =$request->tax_office_name;
                $vendor->TaxOfficeCode =$request->tax_office_code;
                $vendor->CommercialRegNo =$request->reg_code;
                $vendor->TaxRegNumber =$request->tax_reg_number;
                $vendor->account_id =$request->acc_name;
                $vendor->SalesTax =$request->tax_reg_number;
                $vendor->save();
                $succsess = 1;

                DB::commit();
                session()->flash('success', 'Updated');
            }catch (Exception $ex){
                DB::rollBack();
                session()->flash('error', 'failed');
                return back();
            }

            return redirect('vendors/'.$vendor->VendorID.'/edit')->with(['succsess'=>$succsess]);


        }


//                $vendor->BankBeneficiaryName =$request->vendor_code;
//                $vendor->BanlAddress =$request->vendor_code;
//                $vendor->Person_type_ID =$request->vendor_code;
//                $vendor->TaxExempted =$request->vendor_code;
//                $vendor->TaxInAdvance =$request->vendor_code;
//                $vendor->BankName =$request->vendor_code;
//                $vendor->BankSortCode =$request->vendor_code;
//                $vendor->SalesTaxRegNo =$request->vendor_code;
//                $vendor->BankBranchName =$request->vendor_code;
//                $vendor->SupplierTransactionType =$request->vendor_code;
//                $vendor->TransactionType =$request->vendor_code;
//                $vendor->SMSPhone =$request->vendor_code;
//                $vendor->ServiceTypeID =$request->vendor_code;
//                $vendor->BankID =$request->vendor_code;
//                $vendor->BankBranchID =$request->vendor_code;
//                $vendor->BankAccountNo =$request->vendor_code;
//                $vendor->BankSwiftCode =$request->vendor_code;
//                $vendor->BankIBANCode =$request->vendor_code;



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $vendor = Vendor::find($id);
        $vendor->delete();
        session()->flash('success', 'Deleted');
        return redirect('/vendors');
    }





    public function get_vendor(Request $request)

    {

        $print_array = array();
        $array =  explode(',', $request->input('print_ids')[0]);

        foreach ($array as $single)
        {
            array_push($print_array, $single);
        }


        $query = DB::table('vendors')
            ->join('accounts','vendors.account_id','=','accounts.child_id')
            ->get();


        $pdf = new PDF_HF('P','mm','A4');

        $pdf->AddPage();

        $pdf->AliasNbPages('{pages}');

//set font to arial, bold, 14pt

        $pdf->SetFont('Arial','B',12);

//Cell(width , height , text , border , end line , [align] )
        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX( 90);

            $pdf->Cell(40,10,"Vendors Table",0,0,'C');
        $pdf->Ln(11);

        $pdf->setFillColor(230,230,230);


        $pdf->Ln(15);
        $pdf->Cell(45	,7,'ID',1,0,'C',1);
        $pdf->Cell(45	,7,'Vendor Code',1,0,'C',1);
        $pdf->Cell(50	,7,'Vendor Name',1,0,'C',1);
        $pdf->Cell(50	,7,'Account Name',1,0,'C',1);
        /*-----------------------  change header -----------------------------*/
        $headrs_name = array(
            array("ID",45),
            array("Vendor Code",45),
            array("Vendor Name",50),
            array("Account Name",50),

        );
        $pdf->set_header_names($headrs_name);
        /*-------------------------------------------------------*/
        $pdf->SetFontSize(10);

        /*set the table header */
        $pdf->setHeaderTitle(true);

        /*------------------------------*/
        $pdf->SetWidths(array(20,40,50,40,40));
        $pdf->Ln(7);

        foreach ($query as $val)
        {

            $pdf->SetFont('Arial','B',12);

            $pdf->Row(array($val->VendorID,$val->VendorCode,$val->VendorName,$val->Child_name));

        }


        $pdf->Ln(5);
        $pdf->SetX( 40);

        /*remove the table header */
        $pdf->setHeaderTitle(false);
        /*------------------------------*/

        $pdf->Output();
        exit;


    }




    public function get_vendor_rpt($id)

    {
        dd($id);
        $product = DB::table('products')
            ->join('products_type', 'products_type.pro_type_id', '=', 'products.pro_type_id')
            ->join('accounts', 'accounts.child_id', '=', 'products.account_id')
            ->where('pro_id','=',$id)
            ->first();

        $product_details = DB::table('supplier_product')
            ->join('vendors','supplier_product.supplier_id','=','vendors.VendorID')
            ->join('currencies','supplier_product.currency_id','=','currencies.Currency_id')

            ->where('supplier_product.product_id','=',$id)

            ->get();


        $pdf = new PDF_HF('P','mm','A4');

        $pdf->AddPage();

        $pdf->AliasNbPages('{pages}');

//set font to arial, bold, 14pt

        $pdf->SetFont('Arial','B',12);

//Cell(width , height , text , border , end line , [align] )
        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX( 80);

        $pdf->Cell(40,10,"Products NO:",0,0,'C');
        $pdf->Ln(11);

        $pdf->Ln(10);
        $pdf->SetX( 10);
        $pdf->Cell(15,10,'Product Name',0,0,'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( 60);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,$product->pro_name,0,0,'L');
        $pdf->SetX(60);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');


        $pdf->Ln(10);
        $pdf->SetX( 10);
        $pdf->SetFont('','B');
        $pdf->Cell(15,10,'Short Name',0,0,'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( 60);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,$product->product_short_name,0,0,'L');
        $pdf->SetX(60);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');

        $pdf->Ln(10);
        $pdf->SetX( 10);
        $pdf->SetFont('','B');
        $pdf->Cell(15,10,'Product Type',0,0,'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( 60);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,$product->pro_type_name,0,0,'L');
        $pdf->SetX(60);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');

        $pdf->Ln(10);
        $pdf->SetX( 10);
        $pdf->SetFont('','B');
        $pdf->Cell(15,10,'Income Account',0,0,'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( 60);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,$product->Child_name,0,0,'L');
        $pdf->SetX(60);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');

        $pdf->Ln(30);
        $pdf->SetX( 10);
        $pdf->SetFont('','BU');
        $pdf->Cell(15,10,'Suppliers:',0,0,'L');






        $pdf->setFillColor(230, 230, 230);
        $pdf->SetFont('','B');
        $pdf->Ln(15);
        $pdf->Cell(70	,7,'Supplier	',1,0,'C',1);
        $pdf->Cell(60	,7,'Cost Price		',1,0,'C',1);
        $pdf->Cell(60	,7,'Currency	',1,0,'C',1);

        /*-----------------------  change header -----------------------------*/
        $headrs_name = array(
            array("Supplier	",70),
            array("Cost Price		",60),
            array("Currency	",60),

        );
        $pdf->set_header_names($headrs_name);
        /*-------------------------------------------------------*/
        $pdf->SetFontSize(10);

        /*set the table header */
        $pdf->setHeaderTitle(true);

        /*------------------------------*/
        $pdf->SetWidths(array(70,60,60));
        $pdf->Ln(7);

        foreach ($product_details as $val)
        {


            $pdf->Row(array($val->VendorName,$val->cost_price,$val->Currency_name));

        }

        $pdf->Ln(5);
        $pdf->SetX( 40);

        /*remove the table header */
        $pdf->setHeaderTitle(false);
        /*------------------------------*/

        $pdf->Output();
        exit;


    }










}
