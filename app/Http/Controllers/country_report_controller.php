<?php

namespace App\Http\Controllers;


use App\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\PDF_HF;


class country_report_controller extends Controller
{


    //-----------------------------table-------------------------//

    public function get_country(Request $request)

    {




        $array =  explode(',', $request->input('print_ids')[0]);




        $countries = DB::table('countries')
            ->orderBy('Country_ID','desc')
            ->whereIn('Country_ID',$array)
            ->get();





        $pdf = new PDF_HF('P','mm','A4');

        $pdf->AddPage();

        $pdf->AliasNbPages('{pages}');





//set font to arial, bold, 14pt

        $pdf->SetFont('Arial','B',14);

//Cell(width , height , text , border , end line , [align] )
        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX( 90);

        $pdf->Cell(40,10,"Country Table",0,0,'C');
        $pdf->Ln(11);

        $pdf->setFillColor(230,230,230);

        $pdf->Ln(15);
        $pdf->Cell(90	,7,'ID',1,0,'C',1);
        $pdf->Cell(100	,7,'Country Name',1,0,'C',1);


        /*-----------------------  change header -----------------------------*/
        $headrs_name = array(
            array("ID",90),
            array("Country Name",100),

        );
        $pdf->set_header_names($headrs_name);
        /*-------------------------------------------------------*/
        $pdf->SetFontSize(10);

        /*set the table header */
        $pdf->setHeaderTitle(true);

        /*------------------------------*/
        $pdf->SetWidths(array(90,100));
        $pdf->Ln(7);

        foreach ($countries as $val)
        {

            $pdf->SetFont('Arial','B',12);

            $pdf->Row(array($val->Country_ID,$val->Country_Name));

        }


        $pdf->Ln(5);
        $pdf->SetX( 40);

        /*remove the table header */
        $pdf->setHeaderTitle(false);
        /*---------------------------------------*/





        $pdf->Output();
        exit;


    }




    //----------------------------report-----------------------------//

    public function get_country_rpt(Request $request)

    {

        $country = Country::where('Country_ID',$request->id)->first();



        $pdf = new PDF_HF('P','mm','A4');

        $pdf->AddPage();

        $pdf->AliasNbPages('{pages}');

//set font to arial, bold, 14pt

        $pdf->SetFont('Arial','B',12);

//Cell(width , height , text , border , end line , [align] )
        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX( 90);

        $pdf->Cell(40,10,"Country Report :" .''.$country->Country_ID,0,0,'C');
        $pdf->Ln(11);



        $pdf->Ln(15);
        $pdf->SetX( 10);
        $pdf->SetFont('','B');

        $pdf->Cell(15,10,'Country Name',0,0,'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( 60);
        $pdf->SetFont('','');
        $pdf->Cell(50,8,$country->Country_Name,0,0,'L');
        $pdf->SetX(60);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');



        $pdf->Output();
        exit;


    }


}