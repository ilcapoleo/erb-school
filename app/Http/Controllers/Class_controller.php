<?php

namespace App\Http\Controllers;


use App\Class_form;
use App\Classes;
use App\SalaryScale;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;




class Class_controller extends Controller
{



    public function creat_classes()
    {

        $cost_centers = DB::table('cost_centers')->orderBy('Cost_id', 'asc')
            ->pluck('Cost_name', 'Cost_id');

        return view('school_pages.classes',compact('cost_centers'));
    }

    public function check_unique_class(Request $request)
    {
        if ($request->current_po_id != "" && $request->current_po_id) {
            $rules = array(
                'class_name' => ['required','unique:class_form,form_name,'.$request->current_po_id.',class_id']
            );
        }
        else {
            $rules = array(
                'class_name' => ['required','unique:class_form,form_name']

            );
        }


        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), $rules);

        if ($validator->passes()) {

            return response(['passed' => 1]);
        }

        elseif ($validator->fails()) {

            return response()->json(['error' => $validator->messages()]);

        }
    }


    public function cost_center(Request $request)
    {

        $rules = array(

            'class_name' => ['required','unique:class,ClassName,'.$request->current_po_id.',classid'],
            'cost_center' => ['required'],

        );

        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), $rules);

        if ($validator->passes()) {

            if ($request->current_po_id != "" && $request->current_po_id) {

                $class = Classes::findOrFail($request->current_po_id);
                $class->ClassName = $request->class_name;
                $class->Costid = $request->cost_center;
                $class->save();

                $class_id = $class->classid;

                DB::table('class_form')->where('class_id', $class_id)->delete();

                if ($request->table_rows != "") {

                    foreach ($request->table_rows as $row) {

                        $document = new Class_form();
                        $document->class_id = $class_id;

                        $document->form_name = $row['form_name'];

                        $document->save();

                    }
                }


                return response([$request, 'current_po_id' => $class_id]);
            }

            else{
                $class = new Classes();
                $class->ClassName = $request->class_name;
                $class->Costid = $request->cost_center;
                $class->save();

                $class_id = $class->classid;

                if ($request->table_rows != "") {

                    foreach ($request->table_rows as $row) {

                        $document = new Class_form();
                        $document->class_id = $class_id;

                        $document->form_name = $row['form_name'];

                        $document->save();

                    }
                }


                return response([$request, 'current_po_id' => $class_id]);
            }
        }

        elseif ($validator->fails()) {

            return response()->json(['error' => $validator->messages()]);

        }

    }


    /*---------------  delete class ---------------*/
    public function delete_class(Request $request)
    {
        $master_po = Classes::findOrFail($request->cur_po_id);

        $master_po->delete();

        DB::table('class_form')
            ->where('class_id', $request->cur_po_id)
            ->delete();

        return response($request);

    }


    /*--------------  edit class details  -----------------*/
    public function edit_classes($id)
    {
        $cost_centers = DB::table('cost_centers')->orderBy('Cost_id', 'asc')
            ->pluck('Cost_name', 'Cost_id');
        $class = DB::table('class')
            ->where('classid','=',$id)
            ->first();
        $form = DB::table('class_form')
            ->where('class_id','=',$id)
            ->get();


        return view('school_pages.classes',compact('cost_centers','class','form'));
    }

//    public function fees_table()
//    {
//        $showed_search_list = array('ID','fees Name','fees Code',' account name','journal name','document name');
//        $database_search_list = array('fees_id','salary_name','code_name','cost_id');
//
//        return view('school_pages.show_salary',['showed_search_list' => $showed_search_list,'database_search_list'=>$database_search_list]);
//    }
    public function classes_table()
    {
        $showed_search_list = array('ID','classes Name','cost center ');
        $database_search_list = array('classid','ClassName','Costid');

        return view('school_pages.show_classes',['showed_search_list' => $showed_search_list,'database_search_list'=>$database_search_list]);
    }
    public function search_classes_table(Request $request)
    {

        $search_colums=array();
        $book=array();

        $string = $request->sudent_data;

        foreach ($string as $value) {

            list($k, $v) = explode('|', $value);
            if($v == "")
            {
                array_push($search_colums,"all");
                array_push($book,$k);
            }
            else{
                array_push($search_colums,$v);
                array_push($book,$k);

            }

        }

        //$search_colums = array('s_name','s_class','s_age');

        $showed_colums = array('ID','classid','ClassName','Costid');
        $colums_type = array('input','input','input','input');
        $table_colums = array('classid','ClassName','Costid');

        //$book = $request->sudent_data;

        if(count($book) >0 && $book[0] != "all")
        {
            $name = DB::table('class')
                ->join('cost_centers','class.Costid','=','cost_centers.Cost_id')
//                ->join('employee','departments.manager_id','=','employee.employee_user_id')





                ->Where(function ($query) use($book,$search_colums ,$table_colums) {
                    /*for ($i = 0; $i < count($book); $i++){
                        for ($h = 0; $h < count($search_colums); $h++){
                            $query->orwhere($search_colums[$h], 'like',  '%' . $book[$i] .'%');
                        }
                    }*/
                    for ($i = 0; $i < count($book); $i++){
                        if($search_colums[$i] == "all")
                        {
                            $table_colums;
                            for ($h = 0; $h < count($table_colums); $h++){
                                $query->orwhere($table_colums[$h], 'like',  '%' . $book[$i] .'%');
                            }

                        }
                        else
                        {
                            $query->orwhere($search_colums[$i], 'like',  '%' . $book[$i] .'%');
                        }

                    }
                })
                ->orderBy('classid','desc')
                ->get();

        }
        else
        {

            $name = DB::table('class')
                ->join('cost_centers','class.Costid','=','cost_centers.Cost_id')
//                ->join('employee','departments.manager_id','=','employee.employee_user_id')

                ->orderBy('class.Costid','desc')
                ->get();
        }
        return response(["name"=>$name ,"colums"=> $table_colums,"showed_colums"=>$showed_colums,"book"=>$book,"searched_colums"=>$search_colums,"colums_type"=>$colums_type]);
    }
}






