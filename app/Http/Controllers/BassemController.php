<?php

namespace App\Http\Controllers;

use App\bassem;
use Illuminate\Http\Request;

class BassemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\bassem  $bassem
     * @return \Illuminate\Http\Response
     */
    public function show(bassem $bassem)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\bassem  $bassem
     * @return \Illuminate\Http\Response
     */
    public function edit(bassem $bassem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\bassem  $bassem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, bassem $bassem)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\bassem  $bassem
     * @return \Illuminate\Http\Response
     */
    public function destroy(bassem $bassem)
    {
        //
    }
}
