<?php

namespace App\Http\Controllers;

use App\Department;
use App\Employee;
use App\Leave_request;
use App\Leave_type;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\PDF_HF;


class rpt_leaveRequest_report_controller extends Controller
{

    public function get_leaveRequest_rpt(Request $request)

    {

        $leave_req = Leave_request::where('leave_r_id',$request->id)->first();
        $emp = Employee::where('employee_id',$leave_req->employee_id)->first()->employee_name;
        $emp_department = Employee::where('employee_id',$leave_req->employee_id)->first()->depart_id;
        $department = Department::find($emp_department)->dep_name;
        $leave_type = Leave_type::where('leave_id',$leave_req->leave_type_id)->first()->leave_name;
        $leave_unit = Leave_type::where('leave_id',$leave_req->leave_type_id)->first()->leave_unit ==1 ? "day" : "Hours";
        $request_creator = Employee::where('employee_user_id',$leave_req->request_creator)->first()->employee_name ?  Employee::where('employee_user_id',$leave_req->request_creator)->first()->employee_name : "";
        $approved_by = Employee::where('employee_user_id',$leave_req->approved_by)->first()->employee_name ?  Employee::where('employee_user_id',$leave_req->approved_by)->first()->employee_name : "";
//        $leave_req->request_status_id;
//        $leave_req->approved_by;
//        $leave_req->request_creator;


        $pdf = new PDF_HF('P','mm','A4');

        $pdf->AddPage();

        $pdf->AliasNbPages('{pages}');

//set font to arial, bold, 14pt

        $pdf->SetFont('Arial','B',12);

//Cell(width , height , text , border , end line , [align] )
        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX( 80);

        $pdf->Cell(40,10,"Leave Request NO:" .''.$request->id,0,0,'C');
        $pdf->Ln(11);

        $pdf->Ln(10);
        $pdf->SetX( 10);
        $pdf->SetFont('','B');
        $pdf->Cell(15,10,'Employee',0,0,'L');
        $pdf->SetX( 50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetFont('','');

        $pdf->SetX( 60);
        $pdf->Cell(40,8,$emp,0,0,'L');
        $pdf->SetX( 60);
        $pdf->Cell(15,12,'.................................. ',0,0,'L');





        $pdf->SetX( -100);
        $pdf->SetFont('','B');


        $pdf->Cell(15,10,'Requested Date',0,0,'L');
        $pdf->SetX(-70);

        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetFont('','');

        $pdf->SetX( -60);
        $pdf->Cell(40,8,$leave_req->request_date,0,0,'L');
        $pdf->SetX( -60);
        $pdf->Cell(15,12,'.................................. ',0,0,'L');





        $pdf->Ln(10);
        $pdf->SetX( 10);
        $pdf->SetFont('','B');
        $pdf->Cell(15,10,'Department',0,0,'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetFont('','');
        $pdf->SetX( 60);
        $pdf->Cell(40,8,$department,0,0,'L');
        $pdf->SetX( 60);
        $pdf->Cell(15,12,'.................................. ',0,0,'L');




        $pdf->SetX( -100);
        $pdf->SetFont('','B');

        $pdf->Cell(15,10,'Approved Date',0,0,'L');
        $pdf->SetX(-70);

        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetFont('','');

        $pdf->SetX( -60);
        $pdf->Cell(40,8,$leave_req->approved_date,0,0,'L');
        $pdf->SetX( -60);
        $pdf->Cell(15,12,'.................................. ',0,0,'L');

        $pdf->Ln(10);
        $pdf->SetX( 10);
        $pdf->SetFont('','B');

        $pdf->Cell(15,10,'From',0,0,'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetFont('','');
        $pdf->SetX( 60);
        $pdf->Cell(40,8,$leave_req->leave_start,0,0,'L');
        $pdf->SetX( 60);
        $pdf->Cell(15,12,'.................................. ',0,0,'L');


        $pdf->SetX( -100);
        $pdf->SetFont('','B');

        $pdf->Cell(15,10,'To',0,0,'L');
        $pdf->SetX(-70);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetFont('','');

        $pdf->SetX( -60);
        $pdf->Cell(40,8,$leave_req->leave_end,0,0,'L');
        $pdf->SetX( -60);
        $pdf->Cell(15,12,'.................................. ',0,0,'L');

        $pdf->Ln(10);
        $pdf->SetX( 10);
        $pdf->SetFont('','B');

        $pdf->Cell(15,10,'Duration',0,0,'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetFont('','');
        $pdf->SetX( 60);
        $pdf->Cell(40,8,$leave_req->leave_duration,0,0,'L');
        $pdf->SetX( 60);
        $pdf->Cell(15,12,'.................................. ',0,0,'L');
        $pdf->SetX( 70);
        $pdf->Cell(50,8,$leave_unit,0,0,'L');




        $pdf->SetX( -100);
        $pdf->SetFont('','B');


        $pdf->Cell(15,10,'Leave Type',0,0,'L');
        $pdf->SetX(-70);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetFont('','');

        $pdf->SetX( -60);
        $pdf->Cell(40,8,$leave_type,0,0,'L');
        $pdf->SetX( -60);
        $pdf->Cell(15,12,'.................................. ',0,0,'L');


        $pdf->Ln(10);
        $pdf->SetX( 10);
        $pdf->SetFont('','B');

        $pdf->Cell(15,10,'Description',0,0,'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetFont('','');
        $pdf->SetX(60);
        $pdf->Cell(40,8,$leave_req->description ,0,0,'L');
        $pdf->SetX(60);
        $pdf->Cell(40,20,' ',1,0,'L');


        $pdf->Ln(10);
        $y = $pdf->GetY() ;
        $pdf->SetY($y);
        $x = $pdf->GetX() ;
        $pdf->SetX($x);

        $pdf->Ln(10);
        $pdf->SetY( $y+10);
        $pdf->SetX( $x+10);
        $pdf->SetFont('','B');
        $pdf->Cell(15,10,'Requested By',0,0,'C');


        $pdf->SetY( $y+10);
        $pdf->SetX($x-60);
        $pdf->SetFont('','B');

        $pdf->Cell(15,10,'Approved By',0,0,'C');




        $pdf->Ln(10);
        $pdf->SetX( 20);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,$request_creator,0,0,'L');
        $pdf->SetX( 20);
        $pdf->Cell(15,14,'........................',0,0,'C');


        $pdf->SetX( -50);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,$approved_by,0,0,'L');
        $pdf->SetX( -50);
        $pdf->Cell(15,14,'.........................',0,0,'C');

        $pdf->Output();
        exit;


    }



}