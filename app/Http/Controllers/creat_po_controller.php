<?php

namespace App\Http\Controllers;


use App\Account;
use App\Currency;
use App\Details_po;
use App\Employee;
use App\Master_po;
use App\Notfication;
use App\Notification;
use App\Notifications\BritishSchool;
use App\products;
use App\ProductType;
use App\Supplier;
use App\Po_Tax;
use App\Tax;
use App\User;
use App\Vendor;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;




class creat_po_controller extends Controller
{
    //
    /* public function ret_cost_page()
     {
         return view('admin_pages.cost_center_page');
     }*/
    public function creat_po()
    {
        $supplier = DB::table('vendors')->orderBy('VendorName', 'asc')
            ->pluck('VendorName', 'VendorID');
        $users_info =DB::table('employee')
            ->where('depart_id','<>',null)
            ->orderBy('employee_id', 'desc')
            ->pluck('employee_name', 'employee_id');
            /*DB::table('userss')
            ->join('employee','userss.id','=','employee.employee_user_id')
            ->where('employee.depart_id','<>',null)
            ->orderBy('name', 'asc')
            ->pluck('name', 'id');*/
        $department_info = DB::table('departments')->orderBy('dep_name', 'asc')
            ->pluck('dep_name', 'dep_id');
        $currencies = DB::table('currencies')->orderBy('Currency_name', 'asc')
            ->pluck('Currency_name', 'Currency_id');

        $curr_date = Carbon::now('Africa/Cairo')->addHour()->toDateString();

        return view('school_pages.po', compact('supplier', 'users_info', 'department_info', 'currencies','curr_date'));
    }

    public function get_item_type_drop_down(Request $request)
    {
        $item_type = DB::table('products_type')->orderBy('pro_type_id', 'desc')->get();
        return response()->json($item_type);

    }

    public function get_item_name(Request $request)
    {


        $drop_down = DB::table('products')
            ->where('pro_type_id', '=', $request->item_type)
            ->pluck('pro_name', 'pro_id');
        $user = Auth::User()->id;

        $file = $user.".json";

        /* if you want to add new value to json file
                $json = json_decode(file_get_contents($file), true);

                $json[] = array("text" => 'new_test', "value" => 'new_test');*/


        $json = array();

        foreach ($drop_down as $id => $role) {
            $json[] = array("text" => $role, "value" => $id);
        }


        /*file_put_contents($file, json_encode($json));*/
        Storage::disk('uploads')->put('jsons/'.$file,json_encode($json));
        /*Storage::setVisibility('public/jsons/'.$file, 'public');*/

        return response($drop_down);
    }

    public function get_tax(Request $request)
    {
        $drop_down = DB::table('tax')
            ->pluck('tax_name', 'tax_name');

        $file = "data_tax.json";
        /* if you want to add new value to json file
                $json = json_decode(file_get_contents($file), true);

                $json[] = array("text" => 'new_test', "value" => 'new_test');*/


        $json = array();

        foreach ($drop_down as $id => $role) {
            $json[] = array("text" => $role, "value" => $id);
        }


        file_put_contents($file, json_encode($json));

        return response($drop_down);
    }

    public function add_new_po(Request $request)
    {
        $rules = array(

            'cur_user_id' => 'required',
            'supplier' => 'required|exists:vendors,VendorID',
            //'beneficiary_user' => 'required',
            'benefit_management' => 'required',
            'currency' => 'required',
            'order_date' => 'required|date',
            'expected_date' => 'required|after_or_equal:order_date',
            //'approved_date' => 'required|date|after_or_equal:order_date',

        );
        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), $rules);
        if ($validator->passes()) {

            if ($request->current_po_id != "" && $request->current_po_id) {
                $master_po = Master_po::findOrFail($request->current_po_id);
                //$master_po->creator_user_id = $request->cur_user_id;
                $master_po->supplier_id = $request->supplier;
                if ($request->beneficiary_user) {
                    $master_po->beneficiary_user = $request->beneficiary_user;
                } else {
                    $master_po->beneficiary_user = Null;
                }

                $master_po->department_id = $request->benefit_management;
                $master_po->cur_id = $request->currency;

                $curr_date = Carbon::now('Africa/Cairo')->addHour();

                $curr_time = Carbon::now('Africa/Cairo')->addHour()->toTimeString();

                $master_po->order_date = date('Y-m-d', strtotime($request->order_date)) .' '. $curr_time;
                $master_po->expected_date = date('Y-m-d', strtotime($request->expected_date)).' '. $curr_time;




                if ($request->status == 'draft_po') {
                    $status = DB::table('document_status')->where('status_name', '=', $request->status)->first();
                    $master_po->po_seq = 0;
                    $master_po->po_status_id = $status->status_id;

                } elseif ($request->status == 'approved_po') {
                    if($master_po->po_seq == 0)
                    {
                        $new_seq = DB::table('po_master')->max('po_seq') + 1;
                        $master_po->po_seq = $new_seq;
                    }

                    $status = DB::table('document_status')->where('status_name', '=', $request->status)->first();
                    $master_po->po_status_id = $status->status_id;

                    //$curr_date = date("Y-m-d H:i:s");
                    if( !$master_po->approved_date || $master_po->approved_date == null)
                    {
                        $master_po->approved_date = $curr_date;
                    }

                    if( !$master_po->approved_user_id || $master_po->approved_user_id == null)
                    {
                        $master_po->approved_user_id = Employee::where('employee_user_id','=',$request->cur_user_id)->first()->employee_id;///$request->cur_user_id;
                    }

                } else {
                    if($master_po->po_seq == 0)
                    {
                        $new_seq = DB::table('po_master')->max('po_seq') + 1;
                        $master_po->po_seq = $new_seq;
                    }
                    $status = DB::table('document_status')->where('status_name', '=', $request->status)->first();
                    $master_po->po_status_id = $status->status_id;
                }


                $master_po->po_canceled = $request->canceled_po;
                $master_po->po_notes = $request->po_note;

                $master_po->save();

                $master_po_id = $request->current_po_id;
                $return_approved_date = $master_po->approved_date;

                DB::table('po_details')->where('po_id', $master_po_id)->delete();
                //DB::table('po_tax')->where('po', $master_po_id)->delete();
                if ($request->table_rows != "") {

                    foreach ($request->table_rows as $row) {
                        $po_details = new Details_po();
                        $po_details->po_id = $master_po_id;
                        $po_details->item_type_id = $row['item_type'];
                        $po_details->item_name_id = $row['item_name'];
                        $po_details->quantity = $row['quantity'];
                        $po_details->unit_price = $row['unit_price'];
                        $po_details->include_tax = $row['Include'];
                        $po_details->sub_total = $row['subtotal'];

                        $po_details->closed_details = 0;

                        $po_details->tax_string = $row['tax'];
                        $po_details->save();

                        $po_details_id = $po_details->sub_po_id;
                        if ($row['tax']) {

                            $taxs = explode(',', $row['tax']);

                            foreach ($taxs as $single_taxs) {
                                //$tax_id = DB::table('tax')->where('tax_name','')
                                $tax_id = DB::table('tax')->where('tax_name', '=', $single_taxs)
                                    ->first();//Tax::get_tax_id($single_taxs);
                                if ($tax_id) {
                                    $po_tax = new Po_Tax();
                                    $po_tax->po = $master_po_id;
                                    $po_tax->sub_po = $po_details_id;
                                    $po_tax->tax_id = $tax_id->tax_id;
                                    $po_tax->save();
                                }

                            }


                        }


                    }
                }

                $po_tax_details = DB::select('call PO_TAX_DETAILS(?)',[$master_po_id]);
                return response([$request, 'edit_po', 'current_po_id' => $master_po_id, 'status' => $request->status,'return_approved_date'=>$return_approved_date,'po_seq'=>$master_po->po_seq,'po_tax_details'=>$po_tax_details]);
            } else {



                $master_po = new Master_po();
                $master_po->creator_user_id = Employee::where('employee_user_id','=',$request->cur_user_id)->first()->employee_id;

                $master_po->supplier_id = $request->supplier;
                if ($request->beneficiary_user) {
                    $master_po->beneficiary_user = $request->beneficiary_user;
                } else {
                    $master_po->beneficiary_user = Null;
                }
                $master_po->department_id = $request->benefit_management;
                $master_po->cur_id = $request->currency;

                $curr_date = Carbon::now('Africa/Cairo')->addHour();

                $curr_time = Carbon::now('Africa/Cairo')->addHour()->toTimeString();

                $master_po->order_date = date('Y-m-d', strtotime($request->order_date)) .' '. $curr_time;
                $master_po->expected_date = date('Y-m-d', strtotime($request->expected_date)).' '. $curr_time;



                if ($request->status == 'draft_po') {
                    $status = DB::table('document_status')->where('status_name', '=', $request->status)->first();
                    $master_po->po_seq = 0;
                    $master_po->po_status_id = $status->status_id;

                } elseif ($request->status == 'approved_po') {
                    $new_seq = DB::table('po_master')->max('po_seq') + 1;
                    $master_po->po_seq = $new_seq;
                    $master_po->approved_user_id = Employee::where('employee_user_id','=',$request->cur_user_id)->first()->employee_id;//$request->cur_user_id;
                    $status = DB::table('document_status')->where('status_name', '=', $request->status)->first();
                    $master_po->po_status_id = $status->status_id;

                    //$curr_date = date("Y-m-d H:i:s");

                    $master_po->approved_date = $curr_date;
                } else {
                    $new_seq = DB::table('po_master')->max('po_seq') + 1;
                    $master_po->po_seq = $new_seq;
                    $status = DB::table('document_status')->where('status_name', '=', $request->status)->first();
                    $master_po->po_status_id = $status->status_id;
                }


                $master_po->po_canceled = $request->canceled_po;
                $master_po->po_notes = $request->po_note;

                $master_po->save();

                $master_po_id = $master_po->po_id;
                $return_approved_date = $master_po->approved_date;

                if ($request->table_rows != "") {
                    foreach ($request->table_rows as $row) {
                        $po_details = new Details_po();
                        $po_details->po_id = $master_po_id;
                        $po_details->item_type_id = $row['item_type'];
                        $po_details->item_name_id = $row['item_name'];
                        $po_details->quantity = $row['quantity'];
                        $po_details->unit_price = $row['unit_price'];
                        $po_details->include_tax = $row['Include'];
                        $po_details->sub_total = $row['subtotal'];
                        $po_details->closed_details = 0;
                        $po_details->tax_string = $row['tax'];
                        $po_details->save();

                        $po_details_id = $po_details->sub_po_id;
                        if ($row['tax']) {
                            $taxs = explode(',', $row['tax']);

                            foreach ($taxs as $single_taxs) {
                                //$tax_id = DB::table('tax')->where('tax_name','')
                                $tax_id = DB::table('tax')->where('tax_name', '=', $single_taxs)
                                    ->first();//Tax::get_tax_id($single_taxs);
                                if ($tax_id) {
                                    $po_tax = new Po_Tax();
                                    $po_tax->po = $master_po_id;
                                    $po_tax->sub_po = $po_details_id;
                                    $po_tax->tax_id = $tax_id->tax_id;
                                    $po_tax->save();
                                }

                            }


                        }


                    }
                }
                $po_tax_details = DB::select('call PO_TAX_DETAILS(?)',[$master_po_id]);
                /*$user->roles()->attach($data['role']);*/
//                $notification = new Notification();
//                $notification->id = $master_po_id;
//                $notification->notifiable_type = 'po';
//                $notification->notifiable_id = $master_po_id;
//                $notification->type = 'BritishSchool';
//                $notification->data = 'new_po';
//                $notification->save();
//
//                \auth()->user()->notify(new BritishSchool($notification));


                return response([$request, 'current_po_id' => $master_po_id, 'status' => $request->status,'return_approved_date'=>$return_approved_date,'po_seq'=>$master_po->po_seq,'po_tax_details'=>$po_tax_details]);

            }
        } elseif ($validator->fails()) {
            /* $messages = $validator->messages();

             foreach ($rules as $key => $value)
             {
                 $verrors[$key] = $messages->first($key);
             }*/
            return response()->json(['error' => $validator->messages()]);
            /* return Response::json(array(
                 'success' => false,
                 'errors' => $validator->getMessageBag()->toArray()

             ), 400);*/ // 400 being the HTTP code for an invalid request.
            //   return response($verrors);
        }


    }


    public function set_po_to_close(Request $request)
    {
        $master_po = Master_po::findOrFail($request->cur_po_id);
        $master_po->po_canceled = 1;
        $master_po->save();

        return response($request);

    }

    public function delete_this_po(Request $request)
    {
        $master_po = Master_po::findOrFail($request->cur_po_id);

        $master_po->delete();

        return response($request);

    }

    public function get_subtotal(Request $request)
    {
        foreach ($request->table_rows as $row)
        {
            $quantity = $row['quantity'];
            $unit_price = $row['unit_price'];
            $Include = $row['Include'];
            $taxs = $row['tax'];

        }
        $re_sub_total = 0;
        $total_tax = 0;

        if ($taxs || $taxs != "")
        {
            $taxss = explode(',', $taxs);

            foreach ($taxss as $single_taxs)
            {
                $tax_id = DB::table('tax')->where('tax_name', '=', $single_taxs)
                    ->first();//Tax::get_tax_id($single_taxs);
                $id_tax = $tax_id->tax_id;
                //$sub_val = DB::select('call calc_tax(?,?,?,?)',array($id_tax,$unit_price ,$quantity,$Include));
                $sub_val = DB::select('call calc_tax(?,?,?,?)',[$id_tax,$unit_price ,$quantity,$Include]);
                $re_sub_total = $sub_val[0]->subtotal;
                $total_tax = $total_tax + $sub_val[0]->taxval;
            }
        }
        else if($taxs == ""){
            $sub_val = DB::select('call calc_tax(?,?,?,?)',[0,$unit_price ,$quantity,$Include]);
            $re_sub_total = $sub_val[0]->subtotal;
            $total_tax = $total_tax + $sub_val[0]->taxval;
        }

        //$sub_val = DB::select('call calc_tax(?,?,?,?)',[1,10.40,10,1]);

        return response(['$quantity'=>$quantity,'$unit_price'=>$unit_price,'$Include'=>$Include,'$tax'=>$taxs,'$sub_val'=>$re_sub_total,'$total_tax'=>$total_tax]);
    }




    public function eidet_this_po($id)
    {

        $master_po = DB::table('po_master')
            ->join('vendors','po_master.supplier_id','=','vendors.VendorID')
            ->leftJoin('employee','po_master.beneficiary_user','=','employee.employee_id')
            ->join('departments','po_master.department_id','=','departments.dep_id')
            ->join('currencies','po_master.cur_id','=','currencies.Currency_id')
            ->join('document_status','po_master.po_status_id','=','document_status.status_id')

            ->where('po_master.po_id','=',$id)

            ->orderBy('po_master.po_id','desc')
            ->first();

        $details_po = DB::table('po_details')
            ->join('products','po_details.item_name_id','=','products.pro_id')
            ->join('products_type','po_details.item_type_id','=','products_type.pro_type_id')

            ->where('po_details.po_id','=',$id)
            ->orderBy('po_details.sub_po_id','desc')
            ->get();

        $tax_po = DB::table('po_tax')
            ->join('tax','po_tax.tax_id','=','tax.tax_id')
            ->where('po_tax.po','=',$id)
            ->orderBy('po_tax.sub_po','desc')
            ->get();

        $supplier = DB::table('vendors')->orderBy('VendorName', 'asc')
            ->pluck('VendorName', 'VendorID');
        /*$users_info = DB::table('userss')->orderBy('name', 'asc')
            ->pluck('name', 'id');*/
        $users_info =DB::table('employee')
            ->where('depart_id','<>',null)
            ->orderBy('employee_id', 'desc')
            ->pluck('employee_name', 'employee_id');

        $department_info = DB::table('departments')->orderBy('dep_name', 'asc')
            ->pluck('dep_name', 'dep_id');
        $currencies = DB::table('currencies')->orderBy('Currency_name', 'asc')
            ->pluck('Currency_name', 'Currency_id');

        $get_taxs = DB::table('po_tax')
            ->join('po_details','po_tax.sub_po','=','po_details.sub_po_id')
            ->where('po_tax.po','=',$id)
            ->orderBy('po_tax.sub_po','desc')
            ->get();
        $get_details = DB::table('po_details')
                    ->where('po_id','=',$id)
                    ->orderBy('sub_po_id','desc')
                    ->get();

        $sub_total_array = array();
        $total_tax_array = array();
        $tax_sum =0;
        $sub_total_sum =0;

        $curr_date = Carbon::now('Africa/Cairo')->addHour()->toDateString();

        foreach ($get_details as $get_details_single)
        {
            $re_sub_total =0;
            $total_tax = 0;

            if(!$get_details_single->tax_string  || $get_details_single->tax_string  == "" || $get_details_single->tax_string == NULL|| $get_details_single->tax_string == null )
            {

                $sub_val = DB::select('call calc_tax(?,?,?,?)',[0,$get_details_single->unit_price,$get_details_single->quantity,$get_details_single->include_tax]);
                $re_sub_total = $sub_val[0]->subtotal;
                $total_tax = $total_tax + $sub_val[0]->taxval;

            }
            else
                {
                    foreach ($get_taxs as $get_single)
                    {
                        if($get_details_single->sub_po_id == $get_single->sub_po)
                        {

                            $sub_val = DB::select('call calc_tax(?,?,?,?)',[$get_single->tax_id,$get_single->unit_price,$get_single->quantity,$get_single->include_tax]);
                            $re_sub_total = $sub_val[0]->subtotal;
                            $total_tax = $total_tax + $sub_val[0]->taxval;

                        }

                    }
                }

            $tax_sum += $total_tax;
            $sub_total_sum += $re_sub_total;
            array_push($sub_total_array, $re_sub_total);
            array_push($total_tax_array, $total_tax);
        }

        $po_tax_details = DB::select('call PO_TAX_DETAILS(?)',[$master_po->po_id]);

        /*dd($po_tax_details);*/
        return view('school_pages.po', compact('supplier', 'users_info', 'department_info', 'currencies','master_po','details_po','tax_po','sub_total_array','total_tax_array','tax_sum','sub_total_sum','sub_total_sum','po_tax_details'));

    }


    public function get_table_show_po()
    {
        $showed_search_list = array('Po Number','Supplier','Creator Name','Order Date','Status');
        $database_search_list = array(1,2,3,4,5);



        $pos = DB::table('po_master')
            ->join('vendors','po_master.supplier_id','=','vendors.VendorID')
            ->join('employee','po_master.creator_user_id','=','employee.employee_id')
            ->join('document_status','po_master.po_status_id','=','document_status.status_id')
            /*->join('employee','userss.id','=','employee.employee_user_id')*/

        /*    /*->where('creator_user_id','=',$request->user_id)
            ->where(function($query) use ($user_id_table)
            {
                if ( $user_id_table != "non") $query->where('creator_user_id','=',$user_id_table);
            })*/
            ->orderBy('po_master.po_id','desc')
            ->get();



        return view('school_pages.show_op',['showed_search_list' => $showed_search_list,'database_search_list'=>$database_search_list],compact('pos'));
    }

    /*call porsedure*/
/*DB::select('call proc(?,?,?)',array(NULL,$request->class_name ,$request->cost_id ));*/


    public function search_po_table(Request $request)
    {

        $search_colums=array();
        $book=array();
        $user_id_table = $request->user_id;

        $string = $request->sudent_data;

        foreach ($string as $value) {

            list($k, $v) = explode('|', $value);
            if($v == "")
            {
                array_push($search_colums,"all");
                array_push($book,$k);
            }
            else{
                array_push($search_colums,$v);
                array_push($book,$k);

            }

        }

        //$search_colums = array('s_name','s_class','s_age');

        $showed_colums = array('Po Number','Order Date','Supplier','Creator Name','Status');
        $colums_type = array('input','input','input','input','input','input');
        $table_colums = array('po_seq','order_date','VendorName','name','status_short_name');

        //$book = $request->sudent_data;

        if(count($book) >0 && $book[0] != "all")
        {
            $name = DB::table('po_master')
                ->join('vendors','po_master.supplier_id','=','vendors.VendorID')
                ->join('userss','po_master.creator_user_id','=','userss.id')
                ->join('document_status','po_master.po_status_id','=','document_status.status_id')




                ->Where(function ($query) use($book,$search_colums ,$table_colums) {
                    /*for ($i = 0; $i < count($book); $i++){
                        for ($h = 0; $h < count($search_colums); $h++){
                            $query->orwhere($search_colums[$h], 'like',  '%' . $book[$i] .'%');
                        }
                    }*/
                    for ($i = 0; $i < count($book); $i++){
                        if($search_colums[$i] == "all")
                        {
                            $table_colums;
                            for ($h = 0; $h < count($table_colums); $h++){
                                $query->orwhere($table_colums[$h], 'like',  '%' . $book[$i] .'%');
                            }

                        }
                        else
                        {
                            $query->orwhere($search_colums[$i], 'like',  '%' . $book[$i] .'%');
                        }

                    }
                })
                ->where(function($query) use ($user_id_table)
                {
                    if ( $user_id_table != "non") $query->where('creator_user_id','=',$user_id_table);
                })

                ->orderBy('po_master.order_date','desc')
                ->get();

        }
        else
        {

            $name = DB::table('po_master')
                ->join('vendors','po_master.supplier_id','=','vendors.VendorID')
                ->join('userss','po_master.creator_user_id','=','userss.id')
                ->join('document_status','po_master.po_status_id','=','document_status.status_id')

                /*->where('creator_user_id','=',$request->user_id)*/
                ->where(function($query) use ($user_id_table)
                {
                    if ( $user_id_table != "non") $query->where('creator_user_id','=',$user_id_table);
                })
                ->orderBy('po_master.order_date','desc')
                ->get();
        }
        return response(["name"=>$name ,"colums"=> $table_colums,"showed_colums"=>$showed_colums,"book"=>$book,"searched_colums"=>$search_colums,"colums_type"=>$colums_type]);
    }



    public function delete_all_selected_po(Request $request)
    {
        $deleted = array();
        foreach ($request->deleted_user_ids as $class_id_one)
        {
            $user = Master_po::findOrFail($class_id_one);
            if($user->po_status_id == 1)
            {
                $user->delete();
                array_push($deleted,1);

            }
            else
            {
                array_push($deleted,0);
            }
           /* elseif($user->po_status_id == 2)
            {
                $user->po_canceled = 1;
                $user->po_status_id = 5;
                $user->save();
                array_push($deleted,0);
            }*/

        }

        return response(['deleted_ids'=>$request->deleted_user_ids,'deleted'=>$deleted]);
    }


    public function get_currency_of_supplier(Request $request)
    {
        $current_supplier = $request->supplier;
        $supplier = Supplier::findOrFail($current_supplier);

        $currency = $supplier->CurrencyID;

        return response($currency);
    }

    public function get_department_user(Request $request)
    {
        $department = DB::table('employee')
            ->where('employee_id','=',$request->user)
            ->pluck('depart_id');

        return response($department);

    }

    public function get_users_for_department(Request $request)
    {

        if($request->department_id != '')
        {
            /*$users = DB::table('userss')
                ->join('employee','userss.id','=','employee.employee_user_id')
                ->join('departments','employee.depart_id','=','departments.dep_id')
                ->where('departments.dep_id','=',$request->department_id)
                ->pluck('userss.name', 'userss.id');*/
            $users =  DB::table('employee')
                ->join('departments','employee.depart_id','=','departments.dep_id')
                ->where('departments.dep_id','=',$request->department_id)
                ->orderBy('employee_id', 'desc')
                ->pluck('employee_name', 'employee_id');
        }
       else{
          /* $users = DB::table('userss')
               ->join('employee','userss.id','=','employee.employee_user_id')
               ->join('departments','employee.depart_id','=','departments.dep_id')
               ->pluck('userss.name', 'userss.id');*/
           $users =  DB::table('employee')
               ->join('departments','employee.depart_id','=','departments.dep_id')
               ->orderBy('employee_id', 'desc')
               ->pluck('employee_name', 'employee_id');
       }

       return response($users);

    }


    public function refresh_supplier(Request $request)
    {

        $supplier = DB::table('vendors')
            ->orderBy('VendorID', 'desc')
            ->pluck('VendorName', 'VendorID');


        return response($supplier);
    }

    public function get_price_foritem(Request $request)
    {
        $price = DB::table('supplier_product')
            ->where('product_id','=',$request->item_id)
            ->where('supplier_id','=',$request->supplier)
            ->pluck('cost_price');

        return response($price);
    }

    public function refresh_drop_users(Request $request)
    {

        $department = $request->department;


        $users_info =DB::table('employee')
            ->where(function($query) use ($department)
            {
                if ( $department != "") $query->where('depart_id','=',$department);
            })
            ->where('depart_id','<>',null)
            ->orderBy('employee_id', 'desc')
            ->pluck('employee_name', 'employee_id');

        return response($users_info);
    }

    public function refresh_drop_departments(Request $request)
    {
        /*$user = $request->user;*/


        $department_info = DB::table('departments')->orderBy('dep_name', 'asc')

            ->pluck('dep_name', 'dep_id');

        return response($department_info);

    }


    public function set_new_dropdown(Request $request)
    {

        $rules = array(
            'new_product' => 'required|unique:products,pro_name',
        );
        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), $rules);
        if ($validator->passes()) {
            $product = new products();
            $product->pro_name = $request->new_product;
            $product->pro_type_id = $request->item_type;
            $product->cost_price = 0;
            $product->account_id = null;
            $product->active = null;

            $product->save();

            $product_id = $product->pro_id;

            $this->get_item_name($request);

            return response([$request,'product_id'=>$product_id]);

        }
        elseif ($validator->fails()) {

            $this->get_item_name($request);
            return response()->json(['error' => $validator->messages()]);

        }



    }





    public function get_table_show_product()
    {
        $showed_search_list = array('Po Number','Supplier','Creator Name','Status');
        $database_search_list = array('po_seq','VendorName','name','status_short_name');

        return view('school_pages.show_product',['showed_search_list' => $showed_search_list,'database_search_list'=>$database_search_list]);
    }

}