<?php

namespace App\Http\Controllers;




use App\AcademicYear;
use App\SalaryScale;

use App\Salay_plan;
use App\SubSalary;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\PDF_HF;


class salary_plan_report_controller extends Controller
{
    //-----------------------------table-------------------------//
    public function get_salary_plan(Request $request)

    {

        $print_array = array();
        $array =  explode(',', $request->input('print_ids')[0]);

        foreach ($array as $single)
        {
            array_push($print_array, $single);
        }


        $salary_plans = DB::table('acadimic_year')
            //->join('salay_plan','acadimic_year.acadimic_id','salay_plan.acadimic_id')
            ->select('acadimic_year.acadimic_id','acadimic_year.year_name')
            ->wherein('acadimic_year.acadimic_id', $print_array)
            ->groupBy('acadimic_year.acadimic_id','acadimic_year.year_name')
            ->get();




        $pdf = new PDF_HF('P','mm','A4');

        $pdf->AddPage();

        $pdf->AliasNbPages('{pages}');

//set font to arial, bold, 14pt

        $pdf->SetFont('Arial','B',12);

//Cell(width , height , text , border , end line , [align] )
        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX( 90);

        $pdf->Cell(40,10,"Salaries Plan Table",0,0,'C');
        $pdf->Ln(11);

        $pdf->setFillColor(230,230,230);


        $pdf->Ln(15);
        $pdf->Cell(190	,7,'Academic Year',1,0,'C',1);

        /*-----------------------  change header -----------------------------*/
        $headrs_name = array(
            array("Academic Year",190),

        );
        $pdf->set_header_names($headrs_name);
        /*-------------------------------------------------------*/
        $pdf->SetFontSize(10);

        /*set the table header */
        $pdf->setHeaderTitle(true);

        /*------------------------------*/
        $pdf->SetWidths(array(190));
        $pdf->Ln(7);

        foreach ($salary_plans as $val)
        {

            $pdf->SetFont('Arial','B',12);

            $pdf->Row(array($val->year_name));

        }


        $pdf->Ln(5);
        $pdf->SetX( 40);

        /*remove the table header */
        $pdf->setHeaderTitle(false);
        /*------------------------------*/





        $pdf->Output();
        exit;


    }


    //----------------------------reportt-----------------------------//


    public function get_salary_plan_rpt($id)

   {
       $plane_table = DB::table('salay_plan')
           ->where('salay_plan.acadimic_id','=',$id)

           ->join('acadimic_year','salay_plan.acadimic_id','=','acadimic_year.acadimic_id')
           ->join('salary_scale','salay_plan.salary_scale_id','=','salary_scale.salary_id')
           ->join('sub_salary','salay_plan.sub_salary_id','=','sub_salary.sub_id')
           ->join('currencies','salay_plan.currency_id','=','currencies.Currency_id')

           ->get();







          //dd($plane_table);
        $pdf = new PDF_HF('P','mm','A4');

       $pdf->AddPage();

      $pdf->AliasNbPages('{pages}');

////set font to arial, bold, 14pt

       $pdf->SetFont('Arial','B',12);

////Cell(width , height , text , border , end line , [align] )
       $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX( 60);

        $pdf->Cell(40,10,"Salaries Plan For Academic Year:".@AcademicYear::find($id)->year_name,0,'C');
       $pdf->Ln(11);

       $pdf->Ln(10);
       $pdf->SetX( 10);
        $pdf->Cell(15,10,'Academic Year',0,'L');
        $pdf->SetX(60);
        $pdf->SetFont('','');
       $pdf->Cell(15, 10, @AcademicYear::find($id)->year_name, 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
       $pdf->SetX( 60);
        $pdf->SetFont('','');
       $pdf->Cell(30,10,"",0,0,'L');
        $pdf->SetX(60);
       $pdf->Cell(30, 12, '..............................', 0, 0, 'L');






       $pdf->Ln(30);
       $pdf->SetX( 10);
       $pdf->SetFont('','BU');
       $pdf->Cell(15,10,'Sub Salary Scale :',0,0,'L');





       $pdf->setFillColor(230, 230, 230);
       $pdf->SetFont('','B');
       $pdf->Ln(15);
        $pdf->Cell(30	,7,'Salary Scale	',1,0,'C',1);
        $pdf->Cell(30	,7,'Forigen Salary',1,0,'C',1);
        $pdf->Cell(50	,7,'Sub Salary Scale Name',1,0,'C',1);
        $pdf->Cell(30	,7,'Annual Salary',1,0,'C',1);
        $pdf->Cell(30	,7,'Monthly Salary',1,0,'C',1);
        $pdf->Cell(20	,7,'Currency',1,0,'C',1);

        /*-----------------------  change header -----------------------------*/
        $headrs_name = array(
            array("Salary Scale",30),
            array("Forigen Salary",30),
            array("Sub Salary Scale Name",50),
            array("Annual Salary",30),
            array("Monthly Salary",30),
            array("Currency",20),

        );
        $pdf->set_header_names($headrs_name);
        /*-------------------------------------------------------*/
        $pdf->SetFontSize(10);
        //        /*set the table header */
       $pdf->setHeaderTitle(true);

        /*------------------------------*/
       $pdf->SetWidths(array(30,30,50,30,30,20));
       $pdf->Ln(7);



         foreach ($plane_table as $val)
        {


            if($val->forigen_type == 1){
                $yes = "Yes";
            }
            elseif ($val->forigen_type == 0){
                $yes = "No";
            }





//            $pdf->SetFont('Arial','B',12);first()->sub_code;

            $pdf->SetFont('Arial','',12);

            $pdf->Row(array($val->salary_name,$yes,$val->sub_name,$val->annual_salary,$val->monthly_salary,$val->Currency_name));

        }

       $pdf->Ln(5);
       $pdf->SetX( 40);

       /*remove the table header */
       $pdf->setHeaderTitle(false);
        /*------------------------------*/


        $pdf->Output();
       exit;


   }






}