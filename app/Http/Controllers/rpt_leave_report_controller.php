<?php

namespace App\Http\Controllers;

use App\Allowence_deduction;
use App\leave_t;
use App\Leave_type;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\PDF_HF;


class rpt_leave_report_controller extends Controller
{

    public function get_leave_rpt(Request $request)

    {

        $leave = Leave_type::where('leave_id', $request->id)->first();

        $leave_name = $leave->leave_name;
        $short_code = $leave->short_code;
        $leave_unit = $leave->leave_unit == 1 ? "days" : "hours";
        $max_days = $leave->max_unit;
        $paid_type = $leave->paid == 1 ? "paid" : "Unpaid";
        $deduction = isset(Allowence_deduction::where('allow_id', $leave->deduction_id)->first()->allow_name) ? Allowence_deduction::where('allow_id', $leave->deduction_id)->first()->allow_name : "false";

        $pdf = new PDF_HF('P', 'mm', 'A4');

        $pdf->AddPage();

        $pdf->AliasNbPages('{pages}');

//set font to arial, bold, 14pt

        $pdf->SetFont('Arial', 'B', 12);

//Cell(width , height , text , border , end line , [align] )
        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX(90);

        $pdf->Cell(40, 10, "Leave Type NO:" . ' ' . $request->id, 0, 0, 'C');
        $pdf->Ln(11);


        $pdf->Ln(10);
        $pdf->SetX(10);
        $pdf->Cell(15, 10, 'Leave Name ', 0, 0, 'L');

        $pdf->SetX(90);
        $pdf->SetFont('', 'B');

        $pdf->Cell(50, 8, $leave_name, 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');

        $pdf->SetX(90);

        $pdf->Cell(15, 11, '............................. ', 0, 0, 'L');


        $pdf->Ln(10);
        $pdf->SetX(10);
        $pdf->Cell(15, 10, 'Short Code ', 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(90);
        $pdf->SetFont('', 'B');

        $pdf->Cell(50, 8, $short_code, 0, 0, 'L');

        $pdf->SetX(90);

        $pdf->Cell(15, 11, '............................. ', 0, 0, 'L');

        $pdf->Ln(10);
        $pdf->SetX(10);
        $pdf->Cell(15, 10, 'Leave Unit ', 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(90);
        $pdf->SetFont('', 'B');

        $pdf->Cell(50, 8, $leave_unit, 0, 0, 'L');
        $pdf->SetX(90);

        $pdf->Cell(15, 11, '............................. ', 0, 0, 'L');

        $pdf->Ln(10);
        $pdf->SetX(10);
        $pdf->Cell(15, 10, 'Max Days', 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(90);
        $pdf->SetFont('', 'B');

        $pdf->Cell(50, 8, $max_days, 0, 0, 'L');
        $pdf->SetX(90);

        $pdf->Cell(15, 11, '............................. ', 0, 0, 'L');


        $pdf->Ln(10);
        $pdf->SetX(10);
        $pdf->Cell(15, 10, 'Paid Type', 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(90);
        $pdf->SetFont('', 'B');

        $pdf->Cell(50, 8, $paid_type, 0, 0, 'L');
        $pdf->SetX(90);

        $pdf->Cell(15, 11, '............................. ', 0, 0, 'L');

        if ($deduction == false) {
            $pdf->Ln(10);
            $pdf->SetX(10);
            $pdf->Cell(15, 10, 'Deduction', 0, 0, 'L');
            $pdf->SetX(50);
            $pdf->Cell(15, 10, ':', 0, 0, 'L');
            $pdf->SetX(90);
            $pdf->SetFont('', 'B');

            $pdf->Cell(50, 8, $deduction, 0, 0, 'L');
            $pdf->SetX(90);

            $pdf->Cell(15, 11, '............................. ', 0, 0, 'L');
        }




        $pdf->Output();
        exit;


    }



//    public function getReaveRpt(Request $request)
//
//    {
//
//
//
//
////        $master_po = DB::table('po_master')
////            ->join('vendors','po_master.supplier_id','=','vendors.VendorID')
////            ->leftJoin('userss','po_master.beneficiary_user','=','userss.id')
////            ->join('departments','po_master.department_id','=','departments.dep_id')
////            ->join('currencies','po_master.cur_id','=','currencies.Currency_id')
////            ->join('document_status','po_master.po_status_id','=','document_status.status_id')
////            ->where('po_master.po_id','=',$id)
////            ->orderBy('po_master.po_id','desc')
////            ->first();
////
////        $details_po = DB::table('po_details')
////            ->join('products','po_details.item_name_id','=','products.pro_id')
////            ->join('products_type','po_details.item_type_id','=','products_type.pro_type_id')
////            ->where('po_details.po_id','=',$id)
////            ->orderBy('po_details.sub_po_id','desc')
////            ->get();
//
//
//        $pdf = new PDF_HF('P','mm','A4');
//
//        $pdf->AddPage();
//
//        $pdf->AliasNbPages('{pages}');
//
////set font to arial, bold, 14pt
//
//        $pdf->SetFont('Arial','B',14);
//
////Cell(width , height , text , border , end line , [align] )
//        $pdf->SetX($pdf->GetX() - 94);
//        $pdf->SetX( 90);
//
//        $pdf->Cell(40,10,"Leave Type Report",0,0,'C');
//        $pdf->Ln(11);
//
//
//
//        $pdf->Ln(10);
//        $pdf->SetX( 10);
//        $pdf->Cell(15,10,'Leave Name ',0,0,'L');
//        $pdf->SetX( 90);
//        $pdf->SetFont('','B');
//
//        $pdf->Cell(50,8,'',1,0,'L');
//
//
//        $pdf->Ln(10);
//        $pdf->SetX( 10);
//        $pdf->Cell(15,10,'Short Code ',0,0,'L');
//        $pdf->SetX( 90);
//        $pdf->SetFont('','B');
//
//        $pdf->Cell(50,8,'',1,0,'L');
//
//
//        $pdf->Ln(10);
//        $pdf->SetX( 10);
//        $pdf->Cell(15,10,'Leave Unit ',0,0,'L');
//        $pdf->SetX( 90);
//        $pdf->SetFont('','B');
//
//        $pdf->Cell(50,8,'',1,0,'L');
//
//
//        $pdf->Ln(10);
//        $pdf->SetX( 10);
//        $pdf->Cell(15,10,'Max Days',0,0,'L');
//        $pdf->SetX( 90);
//        $pdf->SetFont('','B');
//
//        $pdf->Cell(50,8,'',1,0,'L');
//
//        $pdf->Ln(10);
//        $pdf->SetX( 10);
//        $pdf->Cell(15,10,'Paid Type',0,0,'L');
//        $pdf->SetX( 90);
//        $pdf->SetFont('','B');
//
//        $pdf->Cell(50,8,'',1,0,'L');
//
//        $pdf->Ln(10);
//        $pdf->SetX( 10);
//        $pdf->Cell(15,10,'Deduction',0,0,'L');
//        $pdf->SetX( 90);
//        $pdf->SetFont('','B');
//
//        $pdf->Cell(50,8,'',1,0,'L');
//
//
//        $pdf->Output();
//        exit;
//
//
//    }



}