<?php

namespace App\Http\Controllers;


use App\Details_inventory;
use App\Employee;
use App\inventory_master;
use App\Details_po;
use App\invoice_master_id;
use App\Invoice_tax;
use App\invoice_master;
use App\Supplier;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;




class creat_Invoice_controller extends Controller
{
    //
    /* public function ret_cost_page()
     {
         return view('admin_pages.cost_center_page');
     }*/


    public function creat_inventory_acc()
    {
        $supplier = DB::table('vendors')->orderBy('VendorName', 'asc')
            ->pluck('VendorName', 'VendorID');

        $curr_date = Carbon::now('Africa/Cairo')->addHour()->toDateString();

        $currencies = DB::table('currencies')->orderBy('Currency_name', 'asc')
            ->pluck('Currency_name', 'Currency_id');

        return view('school_pages.invoice',compact('supplier','curr_date', 'currencies'));

    }


    public function creat_pay_invoice()
    {
        return view('school_pages.pay_invoice');
    }

    public function get_this_invoice($id)
    {
        $supplier = DB::table('vendors')->orderBy('VendorName', 'asc')
            ->pluck('VendorName', 'VendorID');

        $curr_date = Carbon::now('Africa/Cairo')->addHour()->toDateString();

        $currencies = DB::table('currencies')->orderBy('Currency_name', 'asc')
            ->pluck('Currency_name', 'Currency_id');

        return view('school_pages.invoice',compact('supplier','curr_date', 'currencies'));
    }


    public function get_inventory_for_supplier(Request $request)
    {
        $inventory = DB::table('inventory_master')
            ->where('doc_status_id','=',3)
            ->orwhere('doc_status_id','=',2)
            ->where('supplier_id','=',$request->supplier)
            ->orderBy('doc_seq', 'desc')
            ->get();

        $current_supplier = $request->supplier;
        $supplier = Supplier::findOrFail($current_supplier);

        $currency = $supplier->CurrencyID;

        return response()->json(['inventory'=>$inventory,'currency'=>$currency]);

    }

    public function get_all_inventory_data(Request $request)
    {
        $inventory_data = DB::table('inventory_details')
            ->join('products','inventory_details.pro_id','=','products.pro_id')
            ->join('po_master','inventory_details.po_id','=','po_master.po_id')
            /*->join('po_details','inventory_details.po_id','=','po_details.po_id')
            ->join('po_details','inventory_details.pro_id','=','po_details.item_name_id')*/

            ->join("po_details",function($join){
                $join->on("po_details.po_id","=","inventory_details.po_id")
                    ->on("po_details.item_name_id","=","inventory_details.pro_id");
            })

            ->where('inventory_details.doc_id','=',$request->inventory_id)


            ->orderBy('inventory_details.sub_doc_id', 'desc')
            ->get();


        $include_string = array();
        $max_quantity_array = array();
        $sub_total_array = array();
        $total_tax_array = array();
        $tax_sum =0;
        $sub_total_sum =0;


        if($inventory_data)
        {
        foreach ($inventory_data as $inventory_data_single)
        {
            if($inventory_data_single->include_tax == 1)
            {
                array_push($include_string, 'Yes');
            }
            else
                {
                    array_push($include_string, 'no');
                }

        }



            foreach ($inventory_data as $inventory_data_single) {

                $last_quantity = DB::select('call clac_remain_qut(?,?,?)',[$inventory_data_single->po_id,$inventory_data_single->pro_id,$inventory_data_single->doc_id]);
                array_push($max_quantity_array, $last_quantity[0]->bal);

                $get_taxs = DB::table('po_tax')
                    ->join('po_details', 'po_tax.sub_po', '=', 'po_details.sub_po_id')
                    ->where('po_tax.po', '=', $inventory_data_single->po_id)
                    ->where('po_tax.sub_po', '=', $inventory_data_single->sub_po_id)
                    ->orderBy('po_tax.sub_po', 'desc')
                    ->get();

                /*$get_taxs = DB::table('inventory_tax')
                    ->join('inventory_details', 'inventory_tax.sub_inventory', '=', 'inventory_details.sub_doc_id')
                    ->where('inventory_tax.inventory_id', '=', $inventory_data_single->po_id)
                    ->where('inventory_tax.sub_inventory', '=', $inventory_data_single->sub_po_id)
                    ->orderBy('inventory_tax.sub_inventory', 'desc')
                    ->get();*/


                $re_sub_total =0;
                $total_tax = 0;

                if(!$inventory_data_single->tax_string  || $inventory_data_single->tax_string  == "" || $inventory_data_single->tax_string == Null|| $inventory_data_single->tax_string == null )
                {
                    $sub_val = DB::select('call calc_tax(?,?,?,?)',[0,$inventory_data_single->unit_price,$inventory_data_single->qut,$inventory_data_single->include_tax]);
                    $re_sub_total = $sub_val[0]->subtotal;
                    $total_tax = $total_tax + $sub_val[0]->taxval;
                }
                else{
                    foreach ($get_taxs as $get_single)
                    {

                        if($inventory_data_single->sub_po_id == $get_single->sub_po)
                        {
                            $sub_val = DB::select('call calc_tax(?,?,?,?)',[$get_single->tax_id,$get_single->unit_price,$inventory_data_single->qut,$get_single->include_tax]);
                            $re_sub_total = $sub_val[0]->subtotal;
                            $total_tax = $total_tax + $sub_val[0]->taxval;

                        }


                    }
                }

                /*foreach ($get_taxs as $get_single)
                {
                    if($inventory_data_sing>le-sub_po_id == $get_single->sub_po)
                    {
                        $sub_val = DB::select('call calc_tax(?,?,?,?)',[$get_single->tax_id,$get_single->unit_price,$inventory_data_single->qut,$get_single->include_tax]);
                        $re_sub_total = $sub_val[0]->subtotal;
                        $total_tax = $total_tax + $sub_val[0]->taxval;

                    }


                }*/
                $tax_sum += $total_tax;
                $sub_total_sum += $re_sub_total;
                array_push($sub_total_array, $re_sub_total);
                array_push($total_tax_array, $total_tax);

            }
        }




        return response (['inventory_data'=>$inventory_data,'yesy_no'=>$include_string,'sub_total_array'=>$sub_total_array,'total_tax_array'=>$total_tax_array,'max_quantity_array'=>$max_quantity_array]);

    }



    public function add_new_invoice(Request $request)
    {
        $rules = array(

            'cur_user_id' => 'required',
            'supplier' => 'required',
            'inventory' => 'required',
            'currency' => 'required',
            'document_date' => 'required|date',


        );
        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), $rules);
        if ($validator->passes()) {

            /*-----------------------------------------  edit inventory -------------------------------------------------------------------*/
            if ($request->current_po_id != "" && $request->current_po_id) {

                $master_po = invoice_master::findOrFail($request->current_po_id);


                $master_po->inventory_m_id = $request->inventory;

                //$master_po->invoice_creator_id	= $request->cur_user_id;
                $master_po->inv_currency_id	= $request->currency;
                $master_po->invoice_ref	= $request->Invoice_Reference;

                $curr_date = Carbon::now('Africa/Cairo')->addHour();

                $curr_time = Carbon::now('Africa/Cairo')->addHour()->toTimeString();

                $master_po->invoice_date = date('Y-m-d', strtotime($request->document_date)) .' '. $curr_time;


                if ($request->status == 'draft_po') {
                    $status = DB::table('document_status')->where('status_name', '=', $request->status)->first();
                    $master_po->invoice_seq	= 0;
                    $master_po->invoice_doc_status = $status->status_id;

                } elseif ($request->status == 'approved_po') {
                    if($master_po->invoice_seq == 0)
                    {
                        $new_seq = DB::table('invoice_master')->max('invoice_seq') + 1;
                        $master_po->invoice_seq = $new_seq;
                    }


                    $status = DB::table('document_status')->where('status_name', '=', $request->status)->first();
                    $master_po->invoice_doc_status = $status->status_id;
                    if( !$master_po->invoice_app_date || $master_po->invoice_app_date == null)
                    {
                        $master_po->invoice_app_date = $curr_date;
                    }

                    if( !$master_po->invoice_app_user_id || $master_po->invoice_app_user_id == null)
                    {
                        $master_po->invoice_app_user_id = Employee::where('employee_user_id','=',$request->cur_user_id)->first()->employee_id;//$request->cur_user_id;
                    }



                } else {
                    if($master_po->invoice_seq == 0)
                    {
                        $new_seq = DB::table('invoice_master')->max('invoice_seq') + 1;
                        $master_po->invoice_seq = $new_seq;
                    }

                    $status = DB::table('document_status')->where('status_name', '=', $request->status)->first();
                    $master_po->invoice_doc_status = $status->status_id;
                }


                $master_po->invoice_notes = $request->document_note;

                $master_po->save();

                //$master_po_id = $master_po->inventory_m_id;
                $master_po_id = $request->inventory;
                $master_invoice_id = $master_po->invoice_m_id;
                DB::table('inventory_details')->where('doc_id', $master_po_id)->delete();


                if ($request->table_rows != "")
                {
                    foreach ($request->table_rows as $row)
                    {
                        $po_details = new Details_inventory();
                        $po_details->doc_id = $master_po_id;
                        $po_details->pro_id = $row['item_product'];
                        $po_details->qut = $row['quantity'];
                        $po_details->po_id = $row['po_id'];
                        $po_details->unit_price = $row['unit_price'];
                        $po_details->include_tax = $row['include_check'];
                        $po_details->tax_string = $row['tax_value'];
                        $po_details->save();

                        $po_ditails = Details_po::where('po_id','=',$row['po_id'])
                            ->where('item_name_id','=',$row['item_product'])
                            ->update(['closed_details' => $row['closed']]);

                        $po_details_id = $po_details->sub_doc_id;
                        if ($row['tax_value']) {
                            $taxs = explode(',', $row['tax_value']);

                            foreach ($taxs as $single_taxs) {
                                //$tax_id = DB::table('tax')->where('tax_name','')
                                $tax_id = DB::table('tax')->where('tax_name', '=', $single_taxs)
                                    ->first();//Tax::get_tax_id($single_taxs);
                                if ($tax_id) {
                                    $po_tax = new Invoice_tax();
                                    $po_tax->inventory_id = $master_po_id;
                                    $po_tax->sub_inventory = $po_details_id;
                                    $po_tax->inv_tax_id = $tax_id->tax_id;
                                    $po_tax->save();
                                }

                            }


                        }


                    }
                }



                    $inventory_master = inventory_master::findOrFail($master_po_id);
                    $inventory_master->doc_status_id = 4;
                    $inventory_master->save();



                return response([$request, 'edit_po', 'current_po_id' => $master_po_id, 'status' => $request->status,'current_invoice_id' => $master_invoice_id]);
            } else {


                /*-----------------------------------------  add new  -------------------------------------------------------------------*/

                $master_po = new invoice_master_id();
                $master_po->inventory_m_id = $request->inventory;

                $master_po->invoice_creator_id	= Employee::where('employee_user_id','=',$request->cur_user_id)->first()->employee_id;//$request->cur_user_id;
                $master_po->inv_currency_id	= $request->currency;
                $master_po->invoice_ref	= $request->Invoice_Reference;

                $curr_date = Carbon::now('Africa/Cairo')->addHour();

                $curr_time = Carbon::now('Africa/Cairo')->addHour()->toTimeString();

                $master_po->invoice_date = date('Y-m-d', strtotime($request->document_date)) .' '. $curr_time;

                if ($request->status == 'draft_po') {
                    $status = DB::table('document_status')->where('status_name', '=', $request->status)->first();
                    $master_po->invoice_seq	= 0;
                    $master_po->invoice_doc_status = $status->status_id;

                } elseif ($request->status == 'approved_po') {
                    $new_seq = DB::table('invoice_master')->max('invoice_seq') + 1;
                    $master_po->invoice_seq = $new_seq;
                    $master_po->invoice_app_user_id = Employee::where('employee_user_id','=',$request->cur_user_id)->first()->employee_id;//$request->cur_user_id;
                    $status = DB::table('document_status')->where('status_name', '=', $request->status)->first();
                    $master_po->invoice_doc_status = $status->status_id;

                    //$curr_date = date("Y-m-d H:i:s");

                    $master_po->invoice_app_date = $curr_date;
                } else {
                    $new_seq = DB::table('invoice_master')->max('invoice_seq') + 1;
                    $master_po->invoice_seq = $new_seq;
                    $status = DB::table('document_status')->where('status_name', '=', $request->status)->first();
                    $master_po->invoice_doc_status = $status->status_id;
                }


                $master_po->invoice_notes = $request->document_note;

                $master_po->save();


                $master_po_id = $request->inventory;

                //return response ($master_po_id);
                //$return_approved_date = $master_po->approved_date;
                /*return response ($master_po);*/
                $master_invoice_id = $master_po->invoice_m_id;
                DB::table('inventory_details')->where('doc_id', $master_po_id)->delete();
                if ($request->table_rows != "")
                {
                    foreach ($request->table_rows as $row)
                    {
                        $po_details = new Details_inventory();
                        $po_details->doc_id = $master_po_id;
                        $po_details->pro_id = $row['item_product'];
                        $po_details->qut = $row['quantity'];
                        $po_details->po_id = $row['po_id'];
                        $po_details->unit_price = $row['unit_price'];
                        $po_details->include_tax = $row['include_check'];
                        $po_details->tax_string = $row['tax_value'];
                        $po_details->save();

                        $po_ditails = Details_po::where('po_id','=',$row['po_id'])
                            ->where('item_name_id','=',$row['item_product'])
                            ->update(['closed_details' => $row['closed']]);

                        $po_details_id = $po_details->sub_doc_id;
                        if ($row['tax_value']) {
                            $taxs = explode(',', $row['tax_value']);

                            foreach ($taxs as $single_taxs) {
                                //$tax_id = DB::table('tax')->where('tax_name','')
                                $tax_id = DB::table('tax')->where('tax_name', '=', $single_taxs)
                                    ->first();//Tax::get_tax_id($single_taxs);
                                if ($tax_id) {
                                    $po_tax = new Invoice_tax();
                                    $po_tax->inventory_id = $master_po_id;
                                    $po_tax->sub_inventory = $po_details_id;
                                    $po_tax->inv_tax_id = $tax_id->tax_id;
                                    $po_tax->save();
                                }

                            }


                        }


                    }
                }



                $inventory_master = inventory_master::findOrFail($master_po_id);
                $inventory_master->doc_status_id = 4;
                $inventory_master->save();



                /*$user->roles()->attach($data['role']);*/
                return response([$request, 'current_po_id' => $master_po_id, 'status' => $request->status,'current_invoice_id' => $master_invoice_id]);

            }
        } elseif ($validator->fails()) {

            return response()->json(['error' => $validator->messages()]);

        }


    }


    public function delete_this_invoice(Request $request)
    {

        if(invoice_master::where('inventory_m_id', '=',$request->cur_po_id)->exists())
        {
            $master_invoice = invoice_master::where('inventory_m_id', '=',$request->cur_po_id);
            $master_invoice->delete();
        }


        if(Invoice_tax::where('inventory_id','=',$request->cur_po_id)->exists())
        {
            $invoice_tax = Invoice_tax::where('inventory_id','=',$request->cur_po_id);
            $invoice_tax->delete();

        }

        $inventory_master = inventory_master::findOrFail($request->cur_po_id);
        $inventory_master->doc_status_id = 2;
        $inventory_master->save();

        return response($request);
    }

    public function array_push_assoc($array, $key, $value){

        if(array_key_exists($key,$array))
        {
            $array[$key] += $value;

        }
        else{
            $array[$key] = $value;
        }

        return $array;
    }

    public function edited_invoice($id)
    {


        $master_invoice_details = DB::table('invoice_master')
            ->join('inventory_master', 'invoice_master.inventory_m_id', '=', 'inventory_master.doc_id')
            ->join('document_status', 'invoice_master.invoice_doc_status', '=', 'document_status.status_id')
            ->where('invoice_master.invoice_m_id', '=', $id)
            ->orderBy('invoice_master.invoice_m_id', 'desc')
            ->first();


        $tax_invoice = DB::table('inventory_tax')
            ->join('tax','inventory_tax.inv_tax_id','=','tax.tax_id')
            ->where('inventory_tax.inventory_id','=',$id)
            ->orderBy('inventory_tax.sub_inventory','desc')
            ->get();

        $supplier = DB::table('vendors')->orderBy('VendorName', 'desc')
            ->pluck('VendorName', 'VendorID');

        $curr_date = Carbon::now('Africa/Cairo')->addHour()->toDateString();

        $currencies = DB::table('currencies')->orderBy('Currency_name', 'desc')
            ->pluck('Currency_name', 'Currency_id');

        $inventory_m_id = $master_invoice_details->inventory_m_id;
        $inventory_details_data = DB::table('inventory_details')
             ->join('products','inventory_details.pro_id','=','products.pro_id')
             ->join('po_master','inventory_details.po_id','=','po_master.po_id')


             ->where('inventory_details.doc_id','=',$inventory_m_id)


             ->orderBy('inventory_details.sub_doc_id', 'desc')
             ->get();


        $get_taxs = DB::table('inventory_tax')
            ->leftJoin('inventory_details', 'inventory_tax.sub_inventory', '=', 'inventory_details.sub_doc_id')
            ->where('inventory_tax.inventory_id', '=', $inventory_m_id)
            ->orderBy('inventory_tax.sub_inventory', 'desc')
            ->get();


        /*$get_details = DB::table('inventory_details')
            ->where('doc_id','=',$id)
            ->orderBy('sub_doc_id','desc')
            ->get();*/

          $include_string = array();
          $max_quantity_array = array();
          $sub_total_array = array();
          $total_tax_array = array();

          $po_details_tax_array = array();
          $tax_sum =0;
          $sub_total_sum =0;


         if($inventory_details_data)
         {
             foreach ($inventory_details_data as $inventory_data_single)
             {

                 $last_quantity = DB::select('call clac_remain_qut(?,?,?)',[$inventory_data_single->po_id,$inventory_data_single->pro_id,$inventory_data_single->doc_id]);
                 array_push($max_quantity_array, $last_quantity[0]->bal);


                 if($inventory_data_single->include_tax == 1)
                 {
                     array_push($include_string, 'Yes');
                 }
                 else
                 {
                     array_push($include_string, 'no');
                 }







                 $re_sub_total =0;
                 $total_tax = 0;
             if(!$inventory_data_single->tax_string  || $inventory_data_single->tax_string  == "" || $inventory_data_single->tax_string == NULL|| $inventory_data_single->tax_string == null )
             {
                 $sub_val = DB::select('call calc_tax(?,?,?,?)',[0,$inventory_data_single->unit_price,$inventory_data_single->qut,$inventory_data_single->include_tax]);
                 $re_sub_total = $sub_val[0]->subtotal;
                 $total_tax = $total_tax + $sub_val[0]->taxval;
             }
             else{
                 foreach ($get_taxs as $get_single)
                 {

                     if($inventory_data_single->sub_doc_id == $get_single->sub_inventory || !$get_single->inv_tax_id)
                     {
                         $sub_val = DB::select('call calc_tax(?,?,?,?)',[$get_single->inv_tax_id,$get_single->unit_price,$get_single->qut,$get_single->include_tax]);
                         $re_sub_total = $sub_val[0]->subtotal;
                         $total_tax = $total_tax + $sub_val[0]->taxval;

                     }


                 }
             }


                 $tax_sum += $total_tax;
                 $sub_total_sum += $re_sub_total;
                 array_push($sub_total_array, $re_sub_total);
                 array_push($total_tax_array, $total_tax);



                 $po_tax_details =  DB::select('call PO_TAX_DETAILS(?)',[$inventory_data_single->po_id]);

                 /*$po_tax_details +=$po_tax_details;*/
                 //array_push($po_details_tax_array, $po_tax_details);
                 foreach ($po_tax_details as $single_po_tax_details)
                 {
                     $po_details_tax_array = $this->array_push_assoc($po_details_tax_array, $single_po_tax_details->tax_name, $single_po_tax_details->TOATL);
                 }

             }



          }




        //, 'total_tax_array', 'sub_total_array'
        return view('school_pages.invoice',compact('supplier','curr_date', 'currencies', 'master_invoice_details', 'tax_invoice', 'inventory_details_data','include_string','max_quantity_array','sub_total_array','total_tax_array','po_details_tax_array'));
        //return view('school_pages.invoice',compact('supplier','curr_date', 'currencies', 'master_invoice_details'));

    }




    public function create_invoice($id)
    {
        $supplier = DB::table('vendors')->orderBy('VendorName', 'asc')
            ->pluck('VendorName', 'VendorID');

        $curr_date = Carbon::now('Africa/Cairo')->addHour()->toDateString();

        $currencies = DB::table('currencies')->orderBy('Currency_name', 'asc')
            ->pluck('Currency_name', 'Currency_id');

        $master_inventory_details = DB::table('inventory_master')
            ->join('vendors', 'inventory_master.supplier_id', '=', 'vendors.VendorID')
            ->where('inventory_master.doc_id', '=', $id)
            ->first();


        return view('school_pages.invoice',compact('supplier','curr_date', 'currencies', 'master_inventory_details'));




    }


    /*---------------------  invoice table  ---------------------------*/
    public function get_table_show_invoice()
    {
        $showed_search_list = array('Invoice Number','Supplier','Creator Name','Status','Currency');
        $database_search_list = array(1,2,3,4,5);

        $invoices = DB::table('invoice_master')
            ->join('currencies','invoice_master.inv_currency_id','=','currencies.Currency_id')
            ->join('inventory_master','invoice_master.inventory_m_id','=','inventory_master.doc_id')
            ->join('inventory_details','inventory_master.doc_id','=','inventory_details.doc_id')
            ->join('vendors','inventory_master.supplier_id','=','vendors.VendorID')
            ->join('employee','invoice_master.invoice_creator_id','=','employee.employee_id')
            //->join('employee','userss.id','=','employee.employee_user_id')
            ->join('document_status','invoice_master.invoice_doc_status','=','document_status.status_id')
            ->select('Currency_name','invoice_m_id','invoice_seq','invoice_date','VendorName','status_short_name','employee_name', DB::raw('SUM(inventory_details.qut*inventory_details.unit_price) As total'),'middle_name','last_name','family_name')
            ->groupBy('Currency_name','invoice_m_id','invoice_seq','invoice_date','VendorName','status_short_name','employee_name','middle_name','last_name','family_name')

            ->orderBy('invoice_master.invoice_m_id','desc')
            ->get();


        return view('school_pages.show_invoice',['showed_search_list' => $showed_search_list,'database_search_list'=>$database_search_list],compact('invoices'));
    }


    public function search_invoice_table(Request $request)
    {

        $search_colums=array();
        $book=array();

        $string = $request->sudent_data;

        foreach ($string as $value) {

            list($k, $v) = explode('|', $value);
            if($v == "")
            {
                array_push($search_colums,"all");
                array_push($book,$k);
            }
            else{
                array_push($search_colums,$v);
                array_push($book,$k);

            }

        }

        //$search_colums = array('s_name','s_class','s_age');

        $showed_colums = array('Invoice Number','Date','Supplier','Creator Name','Status','Currency');
        $colums_type = array('input','input','input','input','input','input');
        $table_colums = array('invoice_seq','invoice_date','VendorName','name','status_short_name','Currency_name');

        //$book = $request->sudent_data;

        if(count($book) >0 && $book[0] != "all")
        {
            $name = DB::table('invoice_master')
                ->join('currencies','invoice_master.inv_currency_id','=','currencies.Currency_id')
                ->join('inventory_master','invoice_master.inventory_m_id','=','inventory_master.doc_id')
                ->join('inventory_details','inventory_master.doc_id','=','inventory_details.doc_id')
                ->join('vendors','inventory_master.supplier_id','=','vendors.VendorID')
                ->join('employee','invoice_master.invoice_creator_id','=','employee.employee_id')
                ->join('document_status','invoice_master.invoice_doc_status','=','document_status.status_id')




                ->Where(function ($query) use($book,$search_colums ,$table_colums) {
                    /*for ($i = 0; $i < count($book); $i++){
                        for ($h = 0; $h < count($search_colums); $h++){
                            $query->orwhere($search_colums[$h], 'like',  '%' . $book[$i] .'%');
                        }
                    }*/
                    for ($i = 0; $i < count($book); $i++){
                        if($search_colums[$i] == "all")
                        {
                            $table_colums;
                            for ($h = 0; $h < count($table_colums); $h++){
                                $query->orwhere($table_colums[$h], 'like',  '%' . $book[$i] .'%');
                            }

                        }
                        else
                        {
                            $query->orwhere($search_colums[$i], 'like',  '%' . $book[$i] .'%');
                        }

                    }
                })
                ->select('Currency_name','invoice_m_id','invoice_seq','invoice_date','VendorName','name','status_short_name', DB::raw('SUM(qut*unit_price) as total'))
                ->groupBy('Currency_name','invoice_m_id','invoice_seq','invoice_date','VendorName','name','status_short_name')
                ->orderBy('invoice_master.invoice_m_id','desc')
                ->get();

        }
        else
        {

            $name = DB::table('invoice_master')
                ->join('currencies','invoice_master.inv_currency_id','=','currencies.Currency_id')
                ->join('inventory_master','invoice_master.inventory_m_id','=','inventory_master.doc_id')
                ->join('inventory_details','inventory_master.doc_id','=','inventory_details.doc_id')
                ->join('vendors','inventory_master.supplier_id','=','vendors.VendorID')
                ->join('userss','invoice_master.invoice_creator_id','=','userss.id')
                ->join('document_status','invoice_master.invoice_doc_status','=','document_status.status_id')
                ->select('Currency_name','invoice_m_id','invoice_seq','invoice_date','VendorName','name','status_short_name', DB::raw('SUM(qut*unit_price) As total'))
                ->groupBy('Currency_name','invoice_m_id','invoice_seq','invoice_date','VendorName','name','status_short_name')

                ->orderBy('invoice_master.invoice_m_id','desc')
                ->get();
        }
        return response(["name"=>$name ,"colums"=> $table_colums,"showed_colums"=>$showed_colums,"book"=>$book,"searched_colums"=>$search_colums,"colums_type"=>$colums_type]);
    }

    public function delete_all_selected_invoice(Request $request)
    {
        $deleted = array();
        foreach ($request->deleted_user_ids as $class_id_one)
        {
            $user = invoice_master_id::findOrFail($class_id_one);
            if($user->invoice_doc_status == 1)
            {
                $user->delete();
                array_push($deleted,1);

            }
            else
            {
                array_push($deleted,0);
            }
            /* elseif($user->po_status_id == 2)
             {
                 $user->po_canceled = 1;
                 $user->po_status_id = 5;
                 $user->save();
                 array_push($deleted,0);
             }*/

        }

        return response(['deleted_ids'=>$request->deleted_user_ids,'deleted'=>$deleted]);
    }

}
