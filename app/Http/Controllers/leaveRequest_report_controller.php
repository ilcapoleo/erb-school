<?php

namespace App\Http\Controllers;

use App\Leave_request;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\PDF_HF;


class leaveRequest_report_controller extends Controller
{

    public function get_leaveRequest(Request $request)

    {


        $print_array = array();
        $array =  explode(',', $request->input('print_ids')[0]);

        foreach ($array as $single)
        {
            array_push($print_array, $single);
        }




             $query = DB::table('leave_request')

                 ->join('employee as employeep','leave_request.employee_id','=','employeep.employee_id')
                 ->join('employee as employeec' ,'leave_request.request_creator','=','employeec.employee_user_id')
                 ->join('leave_type','leave_request.leave_type_id','=','leave_type.leave_id')
                 ->join('document_status','leave_request.request_status_id','=','document_status.status_id')
                 ->select('*','employeep.employee_name as employeepp', 'employeec.employee_name as employeecc')
                 ->wherein('leave_request.leave_r_id',$print_array)
                 ->orderBy('leave_request.leave_r_id','desc')
                 ->get();











        $pdf = new PDF_HF('P','mm','A4');

        $pdf->AddPage();

        $pdf->AliasNbPages('{pages}');

//set font to arial, bold, 14pt

        $pdf->SetFont('Arial','B',14);

//Cell(width , height , text , border , end line , [align] )
        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX( 90);

        $pdf->Cell(40,10,"Leave Request Table",0,0,'C');
        $pdf->Ln(11);

        $pdf->setFillColor(230,230,230);

        $pdf->Ln(15);
        $pdf->Cell(10	,7,'ID',1,0,'C',1);
        $pdf->Cell(40	,7,'Requested Date',1,0,'C',1);
        $pdf->Cell(25	,7,'Employee',1,0,'C',1);
        $pdf->Cell(30	,7,'Leave Type',1,0,'C',1);
        $pdf->Cell(15	,7,'From',1,0,'C',1);
        $pdf->Cell(15	,7,'To',1,0,'C',1);
        $pdf->Cell(20	,7,'Status',1,0,'C',1);
        $pdf->Cell(35	,7,'Creator Name',1,0,'C',1);

        /*-----------------------  change header -----------------------------*/
        $headrs_name = array(
            array("ID",10),
            array("Requested Date",40),
            array("Employee",25),
            array("Leave Type",30),
            array("From",15),
            array("To",15),
            array("Status",20),
            array("Creator Name",35)


        );
        $pdf->set_header_names($headrs_name);
        /*-------------------------------------------------------*/
        $pdf->SetFontSize(10);

        /*set the table header */
        $pdf->setHeaderTitle(true);

        /*------------------------------*/
        $pdf->SetWidths(array(10,40,25,30,15,15,20,35));
        $pdf->Ln(7);

        foreach ($query as $val)
        {

            $pdf->SetFont('Arial','B',12);

            $pdf->Row(array($val->leave_r_id,$val->request_date,$val->employee_name,$val->leave_name,$val->leave_start,$val->leave_end,$val->status_name,$val->employeecc));

        }


        $pdf->Ln(5);
        $pdf->SetX( 40);

        /*remove the table header */
        $pdf->setHeaderTitle(false);
        /*------------------------------*/

        $pdf->Output();
        exit;


    }



}