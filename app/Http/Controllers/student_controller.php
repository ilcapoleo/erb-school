<?php

namespace App\Http\Controllers;

use App\AcademicYear;
use App\acadimic_year;
use App\Classes;
use App\cost_centers;
use App\countries;
use App\Country;
use App\Employee;
use App\nationalty_type;
use App\parents;
use App\relations_students_parents;
use App\Setting;
use App\students;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Class_data;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

use Image;



class student_controller extends Controller
{
    //
    public function ret_student_page()
    {
        /////////////////////Date OF Birth//////////////////////////////
        $get_min_age =json_decode(Setting::where('type','min_age')->first()->value);
        $min_age = Carbon::now()->addYears(-$get_min_age->student)->toDateString();
        ////////////////////////////////////////////////////////////////


        $curr_date = Carbon::now('Africa/Cairo')->addHour()->toDateString();

        $nationality = DB::table('nationalty_type')->orderBy('nationalty_type_id', 'desc')
            ->pluck('nationalty_type', 'nationalty_type_id');

        $acadimic_year = DB::table('acadimic_year')->orderBy('acadimic_id', 'desc')
            ->pluck('year_name', 'acadimic_id');

        $class = DB::table('class')->orderBy('classid', 'desc')
            ->pluck('ClassName', 'classid');

        $country = DB::table('countries')->orderBy('Country_ID', 'desc')
            ->pluck('Country_Name', 'Country_ID');

        $staff_drop = DB::table('employee')
            ->where('marital_status','=','family')->orwhere('family','yes')
            ->get();
        return view('school_pages.student',compact('nationality','acadimic_year','class','country','curr_date','min_age','staff_drop'));
    }

    public function all_students()
    {
        return view('admin_pages.all_students_page');
    }

    public function fill_nationality_option()
    {
        $nationality = DB::table('nationalty_type')->get();
        return response()->json($nationality);

    }


    public function fill_acadimic_option()
    {
        $acadimic = AcademicYear::orderBy('acadimic_id', 'DESC')->get();
        return response()->json($acadimic);

    }

    public function fill_registration_option()
    {
        $class = Classes::orderBy('classid', 'DESC')->get();
        return response()->json($class);

    }

    public function fill_countries_option()
    {
        $country = Country::orderBy('Country_ID', 'DESC')->get();
        return response()->json($country);

    }

    public function fill_mother_countries_option()
    {
        $country = Country::orderBy('Country_ID', 'DESC')->get();
        return response()->json($country);

    }



    public function add_new_student(Request $request)
    {
        /*$this->validate($request,[
        'first_name'=>'required'
        ]);*/

        $student = new students();



        $student->person_id = NULL;
        $student->First_name = $request->first_name;
        $student->Birth_of_date = $request->birth_day;
        $student->Gander_type = $request->gender;
        $student->registration_date = $request->registration_date;
        $student->registration_class_id = $request->registration_class;
        $student->Ismas_code = $request->code;

        $student->person_type = $request->gender;

        $student->Nationalty_type_id = $request->nationality;
        $student->is_leave = $request->leave_v;

        $student->Leave_date = $request->leave_date;
        $student->reg_acadmic_year = $request->acadmic_year;



        /*$get_student_id = $student->person_id;*/


        return response($request);
    }

    public function add_new_parent(Request $request)
    {

        $parent = new parents();



        $parent->par_id = NULL;
        $parent->{'1name'} = $request->first_parent_name;
        $parent->{'2name'} = $request->second_parent_name;
        $parent->{'3name'} = $request->third_parent_name;
        $parent->family_name = $request->family_parent_name;
        $parent->ID_NO = $request->parent_id_number;
        $parent->Pass_ID = $request->parent_passport_number;
        $parent->EMail = $request->parent_email;
        $parent->Contact_NO = $request->parent_phone;
        $parent->Address = $request->parent_address;
        $parent->Country_ID = $request->parent_country;
        $parent->other_Notes = $request->parent_note;
        $parent->Finance = $request->parent_finance;
        $parent->Main_account = $request->main_type;
        /*$parent->Relation_Staff_ID = $request->;*/

        $parent->save();

        $student = DB::table('students')->orderBy('person_id', 'desc')->first();

        $student_id = $student->person_id;
        $parent_id = $parent->par_id;
        $main_id = $parent->Main_account;

        $relation = new relations_students_parents();

        $relation->person_id = $student_id;
        $relation->MainId = $main_id;
        $relation->par_id = $parent_id;
        /*$get_student_id = $student->person_id;*/
        $relation->save();

        return response($request);
    }


    public function submit_all_form(Request $request)

    {




        $rules = array(

            'code' => ['required', 'unique:students,Ismas_code,' . $request->edit_student_id . ',person_id'],
            'first_name' => 'required',
            'birth_day' => 'required',
            'gender' => 'required',
            'nationality' => 'required',
            'acadmic_year' => 'required',
            'registration_date' => 'required',
            'registration_class' => 'required',
            'leave_date' => 'required_if:leave_v,==,1',

            'first_parent_name' => 'required',
            'second_parent_name' => 'required',
            'third_parent_name' => 'required',
            'family_parent_name' => 'required',
            'parent_country' => 'required',



        );

        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), $rules);

        $niceNames = array(
            'leave_date' => 'Leave Date',
            'acadmic_year' => 'Academic year',
            'first_name' => 'Student First Name',
            'first_name' => 'Student First Name',
            'first_parent_name' => 'Father First Name',
            'second_parent_name' => 'Father Second Name',
            'third_parent_name' => 'Father Third Name',
            'family_parent_name' => 'Father Family Name',
            'parent_country' => 'Father Country',
        );
        $validator->setAttributeNames($niceNames);

        if ($validator->passes()) {

            if ($request->edit_student_id != "" && $request->edit_student_id) {

                DB::table('student_parent_relations')->where('person_id','=',$request->edit_student_id)->delete();
                /*--------------  edit  student data ---------------*/

                $last_father_id = '';
                $last_mother_id = '';

                $student = students::findOrFail($request->edit_student_id);




                //$student->person_id = NULL;
                $student->First_name = $request->first_name;
                $student->Birth_of_date = $request->birth_day;
                $student->Gander_type = $request->gender;
                $student->registration_date = $request->registration_date;
                $student->registration_class_id = $request->registration_class;
                $student->Ismas_code = $request->code;

                $student->person_type = $request->gender;

                $student->Nationalty_type_id = $request->nationality;

                $student->is_leave = $request->leave_v;
                $student->Leave_date = $request->leave_date;

                $student->reg_acadmic_year = $request->acadmic_year;



                $student->save();

                $student_id = $student->person_id;

                /*return response($student_id);*/
                /*--------------  image  ------------------------------*/
                $image = $request->file('id-input-file-3');

                $filename = $student_id . '_' . '.png';

                //$image->move('public', $filename);
                //Storage::disk('uploads')->put('images',$image, 'public');
                if ($image) {
                    Image::make($request->file('id-input-file-3'))->save('uploads/students/' . $filename);
                    //Storage::disk('uploads')->putFileAs('students', $image,$filename);
                }


                //$request->file('id-input-file-3')->storeAs('public', $filename);
                if ($request->edit_father_id != "" && $request->edit_father_id) {


                    $father_edit = parents::findOrFail($request->edit_father_id);

                    $father_edit->{'1name'} = $request->first_parent_name;
                    $father_edit->{'2name'} = $request->second_parent_name;
                    $father_edit->{'3name'} = $request->third_parent_name;
                    $father_edit->family_name = $request->family_parent_name;
                    $father_edit->ID_NO = $request->parent_id_number;
                    $father_edit->Pass_ID = $request->parent_passport_number;
                    $father_edit->EMail = $request->parent_email;
                    $father_edit->Contact_NO = $request->parent_phone;
                    $father_edit->Address = $request->parent_address;
                    $father_edit->Country_ID = $request->parent_country;
                    $father_edit->other_Notes = $request->parent_note;
                    $father_edit->Finance = $request->parent_finance;
                    $father_edit->Main_account = $request->main_type;
                    /*$parent->Relation_Staff_ID = $request->;*/

                    if($request->staff_employee_gender == 'male')
                    {
                        $father_edit->Relation_Staff_ID = $request->staff_employee_id;
                    }

                    $father_edit->save();

                    $father_id = $father_edit->par_id;

                    $last_father_id = $father_id;


                    $father_new_relation = new relations_students_parents();

                    $father_new_relation->person_id = $student_id;
                    $father_new_relation->MainId = 1;
                    $father_new_relation->par_id = $request->edit_father_id;
                    $father_new_relation->save();
                } else {

                    /*-------------- father data ---------------*/
                    $parent = new parents();


                    $parent->par_id = NULL;
                    $parent->{'1name'} = $request->first_parent_name;
                    $parent->{'2name'} = $request->second_parent_name;
                    $parent->{'3name'} = $request->third_parent_name;
                    $parent->family_name = $request->family_parent_name;
                    $parent->ID_NO = $request->parent_id_number;
                    $parent->Pass_ID = $request->parent_passport_number;
                    $parent->EMail = $request->parent_email;
                    $parent->Contact_NO = $request->parent_phone;
                    $parent->Address = $request->parent_address;
                    $parent->Country_ID = $request->parent_country;
                    $parent->other_Notes = $request->parent_note;
                    $parent->Finance = $request->parent_finance;
                    //$parent->Main_account = $request->main_type;
                    $parent->Main_account = 1;
                    /*$parent->Relation_Staff_ID = $request->;*/

                    if($request->staff_employee_gender == 'male')
                    {
                        $parent->Relation_Staff_ID = $request->staff_employee_id;
                    }

                    $parent->save();

                    $father_id = $parent->par_id;
                    $last_father_id = $father_id;
                    $father_type = $parent->Main_account;


                    /*------------ father relation ---------------*/
                    $father_relation = new relations_students_parents();

                    $father_relation->person_id = $student_id;
                    $father_relation->MainId = $father_type;
                    $father_relation->par_id = $father_id;
                    $father_relation->save();

                }


                if ($request->edit_mother_id != "" && $request->edit_mother_id) {

                    $mother_edit = parents::findOrFail($request->edit_mother_id);


                    $mother_edit->{'1name'} = $request->mother_first_parent_name;
                    $mother_edit->{'2name'} = $request->mother_second_parent_name;
                    $mother_edit->{'3name'} = $request->mother_third_parent_name;
                    $mother_edit->family_name = $request->mother_family_parent_name;
                    $mother_edit->ID_NO = $request->mother_id_number;
                    $mother_edit->Pass_ID = $request->mother_passport_number;
                    $mother_edit->EMail = $request->mother_email;
                    $mother_edit->Contact_NO = $request->mother_phone;
                    $mother_edit->Address = $request->mother_address;
                    $mother_edit->Country_ID = $request->mother_country;
                    $mother_edit->other_Notes = $request->mother_note;
                    $mother_edit->Finance = $request->mother_finance;
                    $mother_edit->Main_account = $request->mother_main_type;
                    /*$parent->Relation_Staff_ID = $request->;*/

                    if($request->staff_employee_gender == 'female')
                    {
                        $mother_edit->Relation_Staff_ID = $request->staff_employee_id;
                    }

                    $mother_edit->save();
                    /*$parent->Relation_Staff_ID = $request->;*/
                    $mother_id = $mother_edit->par_id;
                    $last_mother_id = $mother_id;

                    /*------------ mother relation ---------------*/

                    $mother_new_relation = new relations_students_parents();

                    $mother_new_relation->person_id = $student_id;
                    $mother_new_relation->MainId = 0;
                    $mother_new_relation->par_id = $request->edit_mother_id;
                    $mother_new_relation->save();
                } else {

                    $mother = new parents();


                    $mother->par_id = NULL;
                    $mother->{'1name'} = $request->mother_first_parent_name;
                    $mother->{'2name'} = $request->mother_second_parent_name;
                    $mother->{'3name'} = $request->mother_third_parent_name;
                    $mother->family_name = $request->mother_family_parent_name;
                    $mother->ID_NO = $request->mother_id_number;
                    $mother->Pass_ID = $request->mother_passport_number;
                    $mother->EMail = $request->mother_email;
                    $mother->Contact_NO = $request->mother_phone;
                    $mother->Address = $request->mother_address;
                    $mother->Country_ID = $request->mother_country;
                    $mother->other_Notes = $request->mother_note;
                    $mother->Finance = $request->mother_finance;
                    //$mother->Main_account = $request->mother_main_type;
                    $mother->Main_account = 0;
                    /*$parent->Relation_Staff_ID = $request->;*/

                    if($request->staff_employee_gender == 'female')
                    {
                        $mother->Relation_Staff_ID = $request->staff_employee_id;
                    }

                    $mother->save();

                    $mother_id = $mother->par_id;
                    $last_mother_id = $mother_id;
                    $mother_type = $mother->Main_account;


                    $mother_relation = new relations_students_parents();

                    $mother_relation->person_id = $student_id;
                    $mother_relation->MainId = $mother_type;
                    $mother_relation->par_id = $mother_id;
                    $mother_relation->save();

                }


                /*---------------------------------------*/
                return response([$request,'student_id'=>$student_id,'mother_id'=>$last_mother_id,'father_id'=>$last_father_id,'staff_id'=>$request->staff_employee_id,'staff_gender'=>$request->staff_employee_gender]);
            } /*-------------- student data ---------------*/


            else {
            /*----------------------------   add new student    ---------------------------------------------*/


                $last_father_id = '';
                $last_mother_id = '';

            $student = new students();


            //$student->person_id = NULL;
            $student->First_name = $request->first_name;
            $student->Birth_of_date = $request->birth_day;
            $student->Gander_type = $request->gender;
            $student->registration_date = $request->registration_date;
            $student->registration_class_id = $request->registration_class;
            $student->Ismas_code = $request->code;

            $student->person_type = $request->gender;

            $student->Nationalty_type_id = $request->nationality;
            $student->is_leave = $request->leave_v;
            $student->Leave_date = $request->leave_date;
            $student->reg_acadmic_year = $request->acadmic_year;

            /* if($request->img)
             {


                 $img = $request->img;
                 $new_name = $student->First_name .'-'. $student->code . '.jpg';
                 $img->move(public_path("images"),$new_name);


             }*/


            $student->save();

            $student_id = $student->person_id;

            /*return response($student_id);*/
            /*--------------  image  ------------------------------*/
            $image = $request->file('id-input-file-3');

            $filename = $student_id . '_' . '.png';

            //$image->move('public', $filename);
            //Storage::disk('uploads')->put('images',$image, 'public');
                if($image)
                {
                    Image::make($request->file('id-input-file-3'))->save('uploads/students/'.$filename);
                    //Storage::disk('uploads')->putFileAs('students', $image,$filename);
                }


                //$request->file('id-input-file-3')->storeAs('public', $filename);
                if ($request->edit_father_id != "" && $request->edit_father_id) {


                    $father_edit = parents::findOrFail($request->edit_father_id);

                    $father_edit->{'1name'} = $request->first_parent_name;
                    $father_edit->{'2name'} = $request->second_parent_name;
                    $father_edit->{'3name'} = $request->third_parent_name;
                    $father_edit->family_name = $request->family_parent_name;
                    $father_edit->ID_NO = $request->parent_id_number;
                    $father_edit->Pass_ID = $request->parent_passport_number;
                    $father_edit->EMail = $request->parent_email;
                    $father_edit->Contact_NO = $request->parent_phone;
                    $father_edit->Address = $request->parent_address;
                    $father_edit->Country_ID = $request->parent_country;
                    $father_edit->other_Notes = $request->parent_note;
                    $father_edit->Finance = $request->parent_finance;
                    $father_edit->Main_account = $request->main_type;
                    /*$parent->Relation_Staff_ID = $request->;*/

                    if($request->staff_employee_gender == 'male')
                    {
                        $father_edit->Relation_Staff_ID = $request->staff_employee_id;
                    }

                    $father_edit->save();

                    $father_id = $father_edit->par_id;

                    $last_father_id =  $father_id;


                    $father_new_relation = new relations_students_parents();

                    $father_new_relation->person_id = $student_id;
                    $father_new_relation->MainId = 1;
                    $father_new_relation->par_id = $request->edit_father_id;
                    $father_new_relation->save();
                }
                else{

                    /*-------------- father data ---------------*/
                    $parent = new parents();


                    $parent->par_id = NULL;
                    $parent->{'1name'} = $request->first_parent_name;
                    $parent->{'2name'} = $request->second_parent_name;
                    $parent->{'3name'} = $request->third_parent_name;
                    $parent->family_name = $request->family_parent_name;
                    $parent->ID_NO = $request->parent_id_number;
                    $parent->Pass_ID = $request->parent_passport_number;
                    $parent->EMail = $request->parent_email;
                    $parent->Contact_NO = $request->parent_phone;
                    $parent->Address = $request->parent_address;
                    $parent->Country_ID = $request->parent_country;
                    $parent->other_Notes = $request->parent_note;
                    $parent->Finance = $request->parent_finance;
                    $parent->Main_account = $request->main_type;
                    /*$parent->Relation_Staff_ID = $request->;*/

                    if($request->staff_employee_gender == 'male')
                    {
                        $parent->Relation_Staff_ID = $request->staff_employee_id;
                    }

                    $parent->save();

                    $father_id = $parent->par_id;
                    $last_father_id = $father_id;
                    $father_type = $parent->Main_account;


                    /*------------ father relation ---------------*/
                    $father_relation = new relations_students_parents();

                    $father_relation->person_id = $student_id;
                    $father_relation->MainId = $father_type;
                    $father_relation->par_id = $father_id;
                    $father_relation->save();

                }


                if ($request->edit_mother_id != "" && $request->edit_mother_id) {

                    $mother_edit = parents::findOrFail($request->edit_mother_id);


                    $mother_edit->{'1name'} = $request->mother_first_parent_name;
                    $mother_edit->{'2name'} = $request->mother_second_parent_name;
                    $mother_edit->{'3name'} = $request->mother_third_parent_name;
                    $mother_edit->family_name = $request->mother_family_parent_name;
                    $mother_edit->ID_NO = $request->mother_id_number;
                    $mother_edit->Pass_ID = $request->mother_passport_number;
                    $mother_edit->EMail = $request->mother_email;
                    $mother_edit->Contact_NO = $request->mother_phone;
                    $mother_edit->Address = $request->mother_address;
                    $mother_edit->Country_ID = $request->mother_country;
                    $mother_edit->other_Notes = $request->mother_note;
                    $mother_edit->Finance = $request->mother_finance;
                    $mother_edit->Main_account = $request->mother_main_type;
                    /*$parent->Relation_Staff_ID = $request->;*/

                    if($request->staff_employee_gender == 'female')
                    {
                        $mother_edit->Relation_Staff_ID = $request->staff_employee_id;
                    }

                    $mother_edit->save();
                    /*$parent->Relation_Staff_ID = $request->;*/
                    $mother_id = $mother_edit->par_id;
                    $last_mother_id = $mother_id;

                    /*------------ mother relation ---------------*/
                    $mother_new_relation = new relations_students_parents();

                    $mother_new_relation->person_id = $student_id;
                    $mother_new_relation->MainId = 0;
                    $mother_new_relation->par_id = $request->edit_mother_id;
                    $mother_new_relation->save();
                }

                else {

                    $mother = new parents();


                    $mother->par_id = NULL;
                    $mother->{'1name'} = $request->mother_first_parent_name;
                    $mother->{'2name'} = $request->mother_second_parent_name;
                    $mother->{'3name'} = $request->mother_third_parent_name;
                    $mother->family_name = $request->mother_family_parent_name;
                    $mother->ID_NO = $request->mother_id_number;
                    $mother->Pass_ID = $request->mother_passport_number;
                    $mother->EMail = $request->mother_email;
                    $mother->Contact_NO = $request->mother_phone;
                    $mother->Address = $request->mother_address;
                    $mother->Country_ID = $request->mother_country;
                    $mother->other_Notes = $request->mother_note;
                    $mother->Finance = $request->mother_finance;
                    $mother->Main_account = $request->mother_main_type;
                    /*$parent->Relation_Staff_ID = $request->;*/
                    if($request->staff_employee_gender == 'female')
                    {
                        $mother->Relation_Staff_ID = $request->staff_employee_id;
                    }

                    $mother->save();

                    $mother_id = $mother->par_id;
                    $last_mother_id = $mother_id;
                    $mother_type = $mother->Main_account;


                    $mother_relation = new relations_students_parents();

                    $mother_relation->person_id = $student_id;
                    $mother_relation->MainId = $mother_type;
                    $mother_relation->par_id = $mother_id;
                    $mother_relation->save();

                }


            /*---------------------------------------*/
                return response([$request,'student_id'=>$student_id,'mother_id'=>$last_mother_id,'father_id'=>$last_father_id,'staff_id'=>$request->staff_employee_id,'staff_gender'=>$request->staff_employee_gender]);
        }
        }

        elseif ($validator->fails()) {

            return response()->json(['error' => $validator->messages()]);

        }
    }


    public function get_all_students_data(Request $request)
    {
        $student = DB::table('students')
            ->join('student_parent_relations', 'students.person_id', '=', 'student_parent_relations.person_id')
            ->join('parents', 'parents.par_id', '=', 'student_parent_relations.par_id')
            ->where('Relation_Staff_ID','=',NULL)
            ->where('student_parent_relations.MainId', '=', 1)
            ->get();
        return response($student);
    }

    /*---------------------  get married staff   ------------------------------------*/
    public function all_stuff_employee(Request $request)
    {
        $staff = DB::table('employee')
            ->where('marital_status','=','family')
            ->get();

        return response($staff);
    }
    /*-------------------------------------------------------*/
    public function return_father_data(Request $request)
    {
        $student_selcted = $request->student_selcted_id;

        $student = DB::table('parents')
            ->join('student_parent_relations', 'parents.par_id', '=', 'student_parent_relations.par_id')
            ->where('student_parent_relations.person_id','=', $student_selcted)
            ->where('student_parent_relations.MainId', '=', 1)
            ->get();

        return response()->json(['father_data'=>$student]);
    }


    /*============================ return_staff_with_partner_data ===========================================*/

    public function return_staff_with_partner_data(Request $request)
    {
        $employee = DB::table('employee')
            ->where('employee_id','=', $request->employee_selcted_id)
            ->get();



        $employee_parent_id = @parents::
            where('Relation_Staff_ID','=',$request->employee_selcted_id)
            ->first()->par_id;


        $first_student_employee = @DB::table('student_parent_relations')
            ->where('par_id','=',$employee_parent_id)
            ->first()->person_id;

        $partner_parent_id = @DB::table('student_parent_relations')
            ->where('person_id','=',$first_student_employee)
            ->where('par_id','<>',$employee_parent_id)
            ->first()->par_id;

        if($employee_parent_id == NULL || !$employee_parent_id)
        {
            $employee_parent_id = '';
        }
        if($partner_parent_id == NULL || !$partner_parent_id)
        {
            $partner_parent_id = '';
        }



        return response()->json(['employee_data'=>$employee,'parent_for_employee'=>$employee_parent_id,'partner_parent_id'=>$partner_parent_id]);
    }

    /*=======================================================================*/


    public function return_mother_data(Request $request)
    {
        $student_selcted = $request->student_selcted_id;

        $student = DB::table('parents')
            ->join('student_parent_relations', 'parents.par_id', '=', 'student_parent_relations.par_id')
            ->where('student_parent_relations.person_id','=', $student_selcted)
            ->where('student_parent_relations.MainId', '=', 0)
            ->get();

        return response()->json(['mother_data'=>$student]);
    }

    public function fill_siblings_table(Request $request)
    {
        $father_data = DB::table('student_parent_relations')
            ->where('person_id','=',$request->student_selcted_id)
            ->where('MainId', '=', 1)
            ->pluck('par_id');

        $siblings_data = DB::table('parents')
            ->leftJoin('student_parent_relations', 'parents.par_id', '=', 'student_parent_relations.par_id')
            ->leftJoin('students', 'student_parent_relations.person_id', '=', 'students.person_id')
            ->leftJoin('class', 'students.registration_class_id', '=', 'class.classid')

            ->where('student_parent_relations.par_id','=', $father_data)
            /*->where('relation.Sid','<>', $request->student_selcted_id)*/
            ->where('student_parent_relations.MainId', '=', 1)
            ->get();

        return response($siblings_data);
    }


    /*----------------------  sibling staff  ----------------------------------*/
    public function fill_siblings_staff_table(Request $request)
    {

        $curr_student = $request->curr_student;
        $parent_id = @DB::table('parents')
            ->where('Relation_Staff_ID','=',$request->staff_selcted_id)
            ->first()->par_id;

        $employee_child_number = @DB::table('employee')
            ->where('employee_id','=',$request->staff_selcted_id)
            ->first()->children_number;



        $siblings_data = @DB::table('parents')
            ->Join('student_parent_relations', 'parents.par_id', '=', 'student_parent_relations.par_id')
            ->Join('students', 'student_parent_relations.person_id', '=', 'students.person_id')
            ->Join('class', 'students.registration_class_id', '=', 'class.classid')

            ->where('student_parent_relations.par_id','=', $parent_id)
            ->Where(function ($query) use($curr_student) {

                if($curr_student != '' && $curr_student && $curr_student != NULL)
                {
                    $query->where('student_parent_relations.person_id','<>', $curr_student);
                }

            })

            //->where('relation.MainId', '=', 1)
            ->get();

            $count = $siblings_data

                //->where('students.person_id','<>',$request->curr_student)
                ->count();
            $remain_childrens = $employee_child_number - $count;


        return response(['siblings_data'=>$siblings_data,'remain_childrens'=>$remain_childrens,'count'=>$count,'employee_child_number'=>$employee_child_number]);

    }



    public function get_allstudents_table()
    {
        $students = DB::select('call students()',array());


        return response()->json(['students_data'=>$students]);
    }



    public function delete_student_row(Request $request)
    {
        $rlation = relations_students_parents::findOrFail($request->student_id);
        $rlation->delete();

        $student = students::findOrFail($request->student_id);
        $student->delete();
        return response($request);
    }

    public function delete_relation_only_row(Request $request)
    {
        $rlation = relations_students_parents::findOrFail($request->student_id);
        $rlation->delete();

        return response($request);
    }

    public function re_student_data($student_id)
    {
        /////////////////////Date OF Birth//////////////////////////////
        $get_min_age =json_decode(Setting::where('type','min_age')->first()->value);
        $min_age = Carbon::now()->addYears(-$get_min_age->student)->toDateString();
        ////////////////////////////////////////////////////////////////

        $curr_date = Carbon::now('Africa/Cairo')->addHour()->toDateString();

        $nationality = DB::table('nationalty_type')->orderBy('nationalty_type_id', 'desc')
            ->pluck('nationalty_type', 'nationalty_type_id');

        $acadimic_year = DB::table('acadimic_year')->orderBy('acadimic_id', 'desc')
            ->pluck('year_name', 'acadimic_id');

        $class = DB::table('class')->orderBy('classid', 'desc')
            ->pluck('ClassName', 'classid');

        $country = DB::table('countries')->orderBy('Country_ID', 'desc')
            ->pluck('Country_Name', 'Country_ID');


        $query = DB::table('students')
            ->where('person_id','=',$student_id)
            ->first();

        $staff_parent = DB::table('student_parent_relations')
            ->join('parents','student_parent_relations.par_id','=','parents.par_id')
            ->where('parents.Relation_Staff_ID','<>',NULL)
            ->where('student_parent_relations.person_id','=',$student_id)
            ->first();

        $staff_drop = Employee::where('marital_status','=','family')->orwhere('family','yes')
            ->get();

        //$count_staff = $staff_parent->count();
        /*$parent_staff = $staff_parent->pluck('par_id');*/



        //dd($staff_parent);
            if($staff_parent )
            {
                //dd($staff_parent);
                $staff_employee_id =  $staff_parent->Relation_Staff_ID;
                //$staff_parent_id = $staff_parent->par_id;
                //dd($staff_employee_id);

                $query_father = DB::table('parents')
                    ->join('student_parent_relations', 'parents.par_id', '=', 'student_parent_relations.par_id')
                    ->where('student_parent_relations.person_id','=',$student_id)
                    ->where('student_parent_relations.MainId', '=', 1)
                    ->first();
                $query_mother = DB::table('parents')
                    ->join('student_parent_relations', 'parents.par_id', '=', 'student_parent_relations.par_id')
                    ->where('student_parent_relations.person_id','=',$student_id)
                    ->where('student_parent_relations.MainId', '=', 0)
                    ->first();



                $staff_data_all = DB::table('employee')
                    ->where('employee_id','=',$staff_employee_id)
                    ->first();


                //dd($staff_data_all);

                $has_staff_siblings = DB::table('parents')
                    ->join('student_parent_relations', 'parents.par_id', '=', 'student_parent_relations.par_id')
                    ->join('students', 'student_parent_relations.person_id', '=', 'students.person_id')
                    //->join('class', 'students.registration_class_id', '=', 'class.classid')

                    ->where('student_parent_relations.par_id','=', $staff_parent->par_id)
                    //->where('student_parent_relations.person_id','<>', $student_id)
                    //->where('relation.MainId', '=', 1)
                    ->where('parents.Relation_Staff_ID','<>',NULL)
                    ->count();

                return view('school_pages.student',compact('nationality','acadimic_year','class','country','query','curr_date','min_age','has_staff_siblings','staff_data_all','query_father','query_mother','staff_drop'));
            }

            else
                {

                    $query_father = DB::table('parents')
                        ->join('student_parent_relations', 'parents.par_id', '=', 'student_parent_relations.par_id')
                        ->where('student_parent_relations.person_id','=',$student_id)
                        ->where('student_parent_relations.MainId', '=', 1)
                        ->first();



                    $query_mother = DB::table('parents')
                        ->join('student_parent_relations', 'parents.par_id', '=', 'student_parent_relations.par_id')
                        ->where('student_parent_relations.person_id','=',$student_id)
                        ->where('student_parent_relations.MainId', '=', 0)
                        ->first();

                    $has_siblings = DB::table('parents')
                        ->join('student_parent_relations', 'parents.par_id', '=', 'student_parent_relations.par_id')
                        ->join('students', 'student_parent_relations.person_id', '=', 'students.person_id')
                        //->join('class', 'students.registration_class_id', '=', 'class.classid')

                        ->where('student_parent_relations.par_id','=', $query_father->par_id)
                        ->where('student_parent_relations.person_id','<>', $student_id)
                        ->where('student_parent_relations.MainId', '=', 1)
                        ->where('parents.Relation_Staff_ID','=',NULL)
                        ->count();
//                    $par = parents::first();
//                    dd($par->appends('first_name'));
                    return view('school_pages.student',compact('nationality','acadimic_year','class','country','query','query_father','query_mother','has_siblings','curr_date','min_age','staff_drop'));
                }



        /*$query=  array("query"=>$student);
        $query_father = array("query_father"=>$father);
        $query_mother = array("query_mother"=>$mother);*/




    }

    public function edit_all_form(Request $request)

    {

       /* $image = $request->file('id-input-file-3');

        if($image){
            $filename = $request->first_name.'_'.$request->code.'.'.'jpg';
            $image->move('uploads',$filename);

        }*/
        /*-------------- student data ---------------*/


        $student = students::findOrFail($request->edit_student_id);


        $student->First_name = $request->first_name;
        $student->Birth_of_date = $request->birth_day;
        $student->Gander_type = $request->gender;
        $student->registration_date = $request->registration_date;
        $student->registration_class_id = $request->registration_class;
        $student->Ismas_code = $request->code;

        $student->person_type = $request->gender;

        $student->Nationalty_type_id = $request->nationality;
        $student->is_leave = $request->leave_v;
        $student->Leave_date = $request->leave_date;
        $student->reg_acadmic_year = $request->acadmic_year;


        $student->save();

        $student_id = $student->person_id;

            /*-------------- father data ---------------*/
        if($request->remove_all_delete == 1)
        {
            $rlation = relations_students_parents::findOrFail($student_id);
            $rlation->delete();

            /*-------------- father data ---------------*/
            $parent = new parents();



            $parent->par_id = NULL;
            $parent->{'1name'} = $request->first_parent_name;
            $parent->{'2name'} = $request->second_parent_name;
            $parent->{'3name'} = $request->third_parent_name;
            $parent->family_name = $request->family_parent_name;
            $parent->ID_NO = $request->parent_id_number;
            $parent->Pass_ID = $request->parent_passport_number;
            $parent->EMail = $request->parent_email;
            $parent->Contact_NO = $request->parent_phone;
            $parent->Address = $request->parent_address;
            $parent->Country_ID = $request->parent_country;
            $parent->other_Notes = $request->parent_note;
            $parent->Finance = $request->parent_finance;
            $parent->Main_account = $request->main_type;
            /*$parent->Relation_Staff_ID = $request->;*/

            $parent->save();

            $father_id = $parent->par_id;
            $father_type = $parent->Main_account;


            /*-------------- mother data ---------------*/
            $mother = new parents();



            $mother->par_id = NULL;
            $mother->{'1name'} = $request->mother_first_parent_name;
            $mother->{'2name'} = $request->mother_second_parent_name;
            $mother->{'3name'} = $request->mother_third_parent_name;
            $mother->family_name = $request->mother_family_parent_name;
            $mother->ID_NO = $request->mother_id_number;
            $mother->Pass_ID = $request->mother_passport_number;
            $mother->EMail = $request->mother_email;
            $mother->Contact_NO = $request->mother_phone;
            $mother->Address = $request->mother_address;
            $mother->Country_ID = $request->mother_country;
            $mother->other_Notes = $request->mother_note;
            $mother->Finance = $request->mother_finance;
            $mother->Main_account = $request->mother_main_type;
            /*$parent->Relation_Staff_ID = $request->;*/

            $mother->save();

            $mother_id = $mother->par_id;
            $mother_type = $mother->Main_account;

            /*------------ father relation ---------------*/
            $father_relation = new relations_students_parents();

            $father_relation->person_id = $student_id;
            $father_relation->MainId = $father_type;
            $father_relation->par_id = $father_id;
            $father_relation->save();

            /*------------ mother relation ---------------*/
            $mother_relation = new relations_students_parents();

            $mother_relation->person_id = $student_id;
            $mother_relation->MainId = $mother_type;
            $mother_relation->par_id = $mother_id;
            $mother_relation->save();
        }
        else
            {
                $sibling_student_id_value = $request->sibling_student_id;
                if($request->has_siblings == 1 && $sibling_student_id_value != null && $sibling_student_id_value != "nothing")
                {
                    $rlation = relations_students_parents::findOrFail($student_id);
                    $rlation->delete();




                    $sibling_student_father = DB::table('student_parent_relations')
                        ->where('person_id','=', $sibling_student_id_value)
                        ->where('MainId','=',1)
                        ->first();

                    /*------------ father relation ---------------*/
                    $father_new_relation = new relations_students_parents();

                    $father_new_relation->person_id = $student_id;
                    $father_new_relation->MainId = $sibling_student_father->MainId;
                    $father_new_relation->par_id = $sibling_student_father->par_id;
                    $father_new_relation->save();

                    if($request->enable_father_edit == 1)
                    {
                        $father_edit = parents::findOrFail($sibling_student_father->par_id);

                        $father_edit->{'1name'} = $request->first_parent_name;
                        $father_edit->{'2name'} = $request->second_parent_name;
                        $father_edit->{'3name'} = $request->third_parent_name;
                        $father_edit->family_name = $request->family_parent_name;
                        $father_edit->ID_NO = $request->parent_id_number;
                        $father_edit->Pass_ID = $request->parent_passport_number;
                        $father_edit->EMail = $request->parent_email;
                        $father_edit->Contact_NO = $request->parent_phone;
                        $father_edit->Address = $request->parent_address;
                        $father_edit->Country_ID = $request->parent_country;
                        $father_edit->other_Notes = $request->parent_note;
                        $father_edit->Finance = $request->parent_finance;
                        $father_edit->Main_account = $request->main_type;
                        /*$parent->Relation_Staff_ID = $request->;*/

                        $father_edit->save();

                    }



                    if($request->has_same_mother == 1)
                    {
                        $sibling_student_mother = DB::table('student_parent_relations')
                            ->where('person_id','=', $sibling_student_id_value)
                            ->where('MainId','=',0)
                            ->first();
                        /*------------ mother relation ---------------*/
                        $mother_new_relation = new relations_students_parents();

                        $mother_new_relation->person_id = $student_id;
                        $mother_new_relation->MainId = $sibling_student_mother->MainId;
                        $mother_new_relation->par_id = $sibling_student_mother->par_id;
                        $mother_new_relation->save();

                        if($request->enable_mother_edit == 1)
                        {
                            $mother_edit = parents::findOrFail($sibling_student_mother->par_id);


                            $mother_edit->{'1name'} = $request->mother_first_parent_name;
                            $mother_edit->{'2name'} = $request->mother_second_parent_name;
                            $mother_edit->{'3name'} = $request->mother_third_parent_name;
                            $mother_edit->family_name = $request->mother_family_parent_name;
                            $mother_edit->ID_NO = $request->mother_id_number;
                            $mother_edit->Pass_ID = $request->mother_passport_number;
                            $mother_edit->EMail = $request->mother_email;
                            $mother_edit->Contact_NO = $request->mother_phone;
                            $mother_edit->Address = $request->mother_address;
                            $mother_edit->Country_ID = $request->mother_country;
                            $mother_edit->other_Notes = $request->mother_note;
                            $mother_edit->Finance = $request->mother_finance;
                            $mother_edit->Main_account = $request->mother_main_type;
                            /*$parent->Relation_Staff_ID = $request->;*/

                            $mother_edit->save();
                            /*$parent->Relation_Staff_ID = $request->;*/

                        }


                    }
                    else
                    {
                        $mother = new parents();



                        $mother->par_id = NULL;
                        $mother->{'1name'} = $request->mother_first_parent_name;
                        $mother->{'2name'} = $request->mother_second_parent_name;
                        $mother->{'3name'} = $request->mother_third_parent_name;
                        $mother->family_name = $request->mother_family_parent_name;
                        $mother->ID_NO = $request->mother_id_number;
                        $mother->Pass_ID = $request->mother_passport_number;
                        $mother->EMail = $request->mother_email;
                        $mother->Contact_NO = $request->mother_phone;
                        $mother->Address = $request->mother_address;
                        $mother->Country_ID = $request->mother_country;
                        $mother->other_Notes = $request->mother_note;
                        $mother->Finance = $request->mother_finance;
                        $mother->Main_account = $request->mother_main_type;
                        /*$parent->Relation_Staff_ID = $request->;*/

                        $mother->save();

                        $mother_id = $mother->par_id;
                        $mother_type = $mother->Main_account;


                        $mother_relation = new relations_students_parents();

                        $mother_relation->person_id = $student_id;
                        $mother_relation->MainId = $mother_type;
                        $mother_relation->par_id = $mother_id;
                        $mother_relation->save();


                    }

                }



                else
                    {
                        $parent = parents::findOrFail($request->edit_father_id);


                        $parent->{'1name'} = $request->first_parent_name;
                        $parent->{'2name'} = $request->second_parent_name;
                        $parent->{'3name'} = $request->third_parent_name;
                        $parent->family_name = $request->family_parent_name;
                        $parent->ID_NO = $request->parent_id_number;
                        $parent->Pass_ID = $request->parent_passport_number;
                        $parent->EMail = $request->parent_email;
                        $parent->Contact_NO = $request->parent_phone;
                        $parent->Address = $request->parent_address;
                        $parent->Country_ID = $request->parent_country;
                        $parent->other_Notes = $request->parent_note;
                        $parent->Finance = $request->parent_finance;
                        $parent->Main_account = $request->main_type;
                        /*$parent->Relation_Staff_ID = $request->;*/

                        $parent->save();






                        $mother = parents::findOrFail($request->edit_mother_id);


                        $mother->{'1name'} = $request->mother_first_parent_name;
                        $mother->{'2name'} = $request->mother_second_parent_name;
                        $mother->{'3name'} = $request->mother_third_parent_name;
                        $mother->family_name = $request->mother_family_parent_name;
                        $mother->ID_NO = $request->mother_id_number;
                        $mother->Pass_ID = $request->mother_passport_number;
                        $mother->EMail = $request->mother_email;
                        $mother->Contact_NO = $request->mother_phone;
                        $mother->Address = $request->mother_address;
                        $mother->Country_ID = $request->mother_country;
                        $mother->other_Notes = $request->mother_note;
                        $mother->Finance = $request->mother_finance;
                        $mother->Main_account = $request->mother_main_type;


                        $mother->save();

                        $mother_id = $mother->par_id;
                    }




            }
        /*---------------------------------------*/
        return response($request);
    }


    public function get_all_students_datafor_edit(Request $request)
    {

        $parent_of_current_student = DB::table('student_parent_relations')
            ->where('person_id','=',$request->current_student)
            ->where('MainId', '=', 1)
            ->pluck('par_id');

        $student = DB::table('students')
            ->join('student_parent_relations', 'students.person_id', '=', 'student_parent_relations.person_id')
            ->join('parents', 'parents.par_id', '=', 'student_parent_relations.par_id')
            ->where('student_parent_relations.MainId', '=', 1)
            ->where('student_parent_relations.person_id','<>',$request->current_student)
            ->where('student_parent_relations.par_id','<>',$parent_of_current_student)
            ->get();
        return response($student);
    }

    public function fill_siblings_table_edit(Request $request)
    {
        $father_data = DB::table('student_parent_relations')
            ->where('person_id','=',$request->student_selcted_id)
            ->where('MainId', '=', 1)
            //->pluck('ParId');
            ->first();

        $siblings_data = DB::table('parents')
            ->leftjoin('student_parent_relations', 'parents.par_id', '=', 'student_parent_relations.par_id')
            ->leftjoin('students', 'student_parent_relations.person_id', '=', 'students.person_id')
            ->leftjoin('class', 'students.registration_class_id', '=', 'class.classid')

            ->where('student_parent_relations.par_id','=', $father_data->par_id)
            ->where('student_parent_relations.person_id','<>', $request->student_selcted_id)
            ->where('student_parent_relations.MainId', '=', 1)
            ->get();

        return response($siblings_data);
    }

  /*  public function set_image()
    {
        var_dump(Input::all());
        $image = Input::file('id-input-file-3');
        $old_image = Input::get('old_image');
        if ($image)
        {
            $filename = Input::get('first_name').'_'.Input::get('code').'.'.'jpg';

            if(File::exists(public_path('uploads/'.$filename))) {
                File::delete(public_path('uploads/'.$filename));
            }

            if(File::exists(public_path('uploads/'.$old_image))) {
                File::delete(public_path('uploads/'.$old_image));
            }

            $move = $image->move('uploads',$filename);

            if($move)
            {

                return Response::json(['succsess'=>true]);

            }
            return Response::json(['succsess'=>false]);

        }
    }*/



    /*-------------------  student table  --------------------------*/
    public function get_table_show_student()
    {
        $showed_search_list = array('ID','Student Name','Nationality','Gender','Code','Acadmic year');

        /*$database_search_list = array('SID','First_name','nationalty_type','Gender_type','code','code');*/
        $students = DB::select('call students()',array());

        //dd($students);

        $database_search_list = array(1,2,3,4,5,6);
        return view('school_pages.show_students',['showed_search_list' => $showed_search_list,'database_search_list'=>$database_search_list],compact('students'));
    }

//    public function search_student_table(Request $request)
//    {
//
//        $search_colums=array();
//        $book=array();
//        $user_id_table = $request->user_id;
//
//        $string = $request->sudent_data;
//
//        foreach ($string as $value) {
//
//            list($k, $v) = explode('|', $value);
//            if($v == "")
//            {
//                array_push($search_colums,"all");
//                array_push($book,$k);
//            }
//            else{
//                array_push($search_colums,$v);
//                array_push($book,$k);
//
//            }
//
//        }
//
//        //$search_colums = array('s_name','s_class','s_age');
//
//        $showed_colums = array('ID','Student Name','Nationality','Gender','Code','Acadmic year');
//        $colums_type = array('input','input','input','input','input','input');
//
//
//        $table_colums = array('SID','First_name','nationalty_type','Gender_type','code','Acadmic_year');
//
//        //$book = $request->sudent_data;
//
//        if(count($book) >0 && $book[0] != "all")
//        {
//            $name = DB::table('students')
//                ->join('nationalty_type','students.Nationality_type_id','=','nationalty_type.nationalty_type_id')
//
//
//                ->Where(function ($query) use($book,$search_colums ,$table_colums) {
//                    /*for ($i = 0; $i < count($book); $i++){
//                        for ($h = 0; $h < count($search_colums); $h++){
//                            $query->orwhere($search_colums[$h], 'like',  '%' . $book[$i] .'%');
//                        }
//                    }*/
//                    for ($i = 0; $i < count($book); $i++){
//                        if($search_colums[$i] == "all")
//                        {
//                            $table_colums;
//                            for ($h = 0; $h < count($table_colums); $h++){
//                                $query->orwhere($table_colums[$h], 'like',  '%' . $book[$i] .'%');
//                            }
//
//                        }
//                        else
//                        {
//                            $query->orwhere($search_colums[$i], 'like',  '%' . $book[$i] .'%');
//                        }
//
//                    }
//                })
//
//                ->orderBy('students.person_id','desc')
//                ->get();
//
//        }
//        else
//        {
//
//
//            $name = DB::table('students')
//                ->join('nationalty_type','students.Nationality_type_id','=','nationalty_type.nationalty_type_id')
//                ->orderBy('students.person_id','desc')
//                ->get();
//
//        }
//        return response(["name"=>$name ,"colums"=> $table_colums,"showed_colums"=>$showed_colums,"book"=>$book,"searched_colums"=>$search_colums,"colums_type"=>$colums_type]);
//    }



    public function delete_all_selected_student(Request $request)
    {

        foreach ($request->deleted_user_ids as $class_id_one)
        {
            DB::table('student_parent_relations')
                ->where('person_id','=',$class_id_one)
                ->delete();

            $user = students::findOrFail($class_id_one);


                $user->delete();

        }

        return response(['deleted_ids'=>$request->deleted_user_ids]);
    }

   /* public function refresh_student_table()
    {
        $students = DB::select('call students()',array());

        //return response(["table_data"=>$students]);
        return response()->json(['table_data'=>$students]);

    }*/

   public function update_nationality(Request $request)
   {

   }
}
