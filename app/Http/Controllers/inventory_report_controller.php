<?php

namespace App\Http\Controllers;



use App\Master_inventory;
use App\Master_po;
use App\Supplier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\PDF_HF;


class inventory_report_controller extends Controller
{
    //
    /* public function ret_cost_page()
     {
         return view('admin_pages.cost_center_page');
     }*/
    public function print_inventory_table(Request $request)

    {




       // define('FPDF_FONTPATH',public_path('font'));


        /*$accounts = Chart::all();
        $pdf = PDF::loadView('admin_pages.journal_entry_repory',['accounts'=>$accounts]);
        return $pdf->stream('accounts.pdf');*/

//get invoices data
        $print_array = array();
        $array =  explode(',', $request->input('print_ids')[0]);

        foreach ($array as $single)
        {
            array_push($print_array, $single);
        }

        $query = DB::table('inventory_master')
            ->leftJoin('vendors','inventory_master.supplier_id','=','vendors.VendorID')
            ->join('employee','inventory_master.create_by_user_id','=','employee.employee_id')
            ->leftJoin('document_status','inventory_master.doc_status_id','=','document_status.status_id')
            ->leftJoin('inventory_documents','inventory_master.doc_type','=','inventory_documents.inv_doc_id')
            ->whereIn('inventory_master.doc_id',$print_array)
            ->orderBy('inventory_master.doc_id','desc')
            ->get();




        $pdf = new PDF_HF('P','mm','A4');



        $pdf->AddPage();
/*
        $pdf->Header = (function() {
            $path = public_path() . '/assets/img/';
            $filepath = $path . 'user.png';


            // Select Arial bold 15
            $this->SetFont('Arial','B',15);
            // Move to the right

            $this->Cell(15,15,"",0,0,'C');

            $this->image($filepath,10,9.7,15);
            // Framed title

            $this->Cell(50,15,'Student Report',0,0,'C');
            // Line break
            $this->Ln(20);
            if ($this->page != 1 && $this->set_header == true)
            {   $this->Ln(5);
                $this->SetFontSize(14);
                $this->Cell(50	,7,'test1 ',1,0,'C');
                $this->Cell(40	,7,'test1',1,0,'C');
                $this->Cell(20	,7,'test1',1,0,'C');
                $this->Cell(40	,7,'test1',1,0,'C');
                $this->Cell(40	,7,'test1 Year',1,1,'C');
            }
        });*/
        $pdf->AliasNbPages('{pages}');

//set font to arial, bold, 14pt

        $pdf->SetFont('Arial','B',12);



//Cell(width , height , text , border , end line , [align] )
        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX( 90);

        $pdf->Cell(40,10,"Inventory  Table",0,0,'C');
        $pdf->Ln(11);



        $pdf->setFillColor(230,230,230);


        $pdf->Ln(15);
        $pdf->Cell(30	,7,'ID ',1,0,'C',1);
        $pdf->Cell(35	,7,'Date',1,0,'C',1);
        $pdf->Cell(50	,7,'Document type name',1,0,'C',1);
        $pdf->Cell(30	,7,'Supplier',1,0,'C',1);
        $pdf->Cell(20	,7,'Creator',1,0,'C',1);
        $pdf->Cell(25	,7,'status',1,1,'C',1);
        /*-----------------------  change header -----------------------------*/
        $headrs_name = array(
            array("ID",30),
            array("Date",35),
            array("Document type name",50),
            array("Supplier",30),
            array("Creator",20),
            array("status",25)
        );
        $pdf->set_header_names($headrs_name);
        /*-------------------------------------------------------*/
        $pdf->SetFontSize(12);

        /*set the table header */
        $pdf->setHeaderTitle(true);

        /*------------------------------*/
        $pdf->SetWidths(array(30,35,50,30,20,25));

        foreach ($query as $val)
        {
            $pdf->SetFont('Arial','B',12);

            $pdf->Row(array($val->doc_seq,$val->doc_date,$val->doc_type_name,$val->VendorName,$val->employee_name,$val ->status_short_name));


            /*$pdf->Cell(50	,7,$val->First_name,1,0,'C');
            $pdf->Cell(40	,7,$val->Birth_of_date,1,0,'C');
            $pdf->Cell(20	,7,$val->Gender_type,1,0,'C');
            if($pdf->GetStringWidth($val->code)>40)
            {
                $pdf->SetFont('Arial','B',10);
                $pdf->Cell(40	,7,$val->code,1,0,'C');

            }
            else
                {
                    $pdf->SetFont('Arial','B',12);
                    $pdf->Cell(40	,7,$val->code,1,0,'C');
                }


            $pdf->Cell(40	,7,$val->Acadmic_year,1,1,'C');*/
        }


        $pdf->Ln(5);
        $pdf->SetX( 40);

        /*remove the table header */
        $pdf->setHeaderTitle(false);
        /*------------------------------*/


        $pdf->Output();
        exit;


    }



    public function get_inventory_rpt($id)

    {

        $doc = Master_inventory::where('doc_id',$id)->first();
        $supplier = Supplier::where('VendorID',$doc->supplier_id)->first()->VendorName;
        $doc_type = $doc->doc_type == 1 ? "Purchase" : "return purchase" ;

        $query = DB::table('inventory_details')
            ->join('products','inventory_details.pro_id','=','products.pro_id')
            ->join('po_master','inventory_details.po_id','=','po_master.po_id')
            ->where('inventory_details.doc_id','=',$id)
            ->orderBy('inventory_details.sub_doc_id','desc')
            ->get();



        $pdf = new PDF_HF('P','mm','A4');



        $pdf->AddPage();

        $pdf->AliasNbPages('{pages}');

//set font to arial, bold, 14pt

        $pdf->SetFont('Arial','B',12);



//Cell(width , height , text , border , end line , [align] )
        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX( 90);

        if($doc->doc_seq == 0 )
            $pdf->Cell(40,10,"Inventory : Draft",0,0,'C');
        else{
            $pdf->Cell(40,10,"Inventory : $doc->doc_seq",0,0,'C');
        }


        $pdf->Ln(11);
        $pdf->SetX(10);
        $pdf->SetFont('', 'UB');
        $pdf->Cell(15, 10, 'Purchase Order', 0, 0, 'L');




        $pdf->Ln(15);
        $pdf->SetX(10);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'Supplier', 0, 0, 'L');
        $pdf->SetX(45);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, $supplier, 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');

        $pdf->SetX(-105);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'Document Date', 0, 0, 'L');
        $pdf->SetX(-70);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(-65);
        $pdf->Cell(15, 10, $doc->doc_date, 0, 0, 'L');
        $pdf->SetX(-65);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');

        $pdf->Ln(15);
        $pdf->SetX(10);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'Document Type', 0, 0, 'L');
        $pdf->SetX(45);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, $doc_type, 0, 0, 'L');
        $pdf->SetX(50);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');


        $pdf->setFillColor(230, 230, 230);


        $pdf->Ln(15);
        $pdf->Cell(45	,7,'Close Po	',1,0,'C',1);
        $pdf->Cell(45	,7,'PO',1,0,'C',1);
        $pdf->Cell(45	,7,'Items',1,0,'C',1);
        $pdf->Cell(45	,7,'Quantity	',1,0,'C',1);

        /*-----------------------  change header -----------------------------*/
        $headrs_name = array(
            array("Close Po	",45),
            array("PO",45),
            array("Items",45),
            array("Quantity",45)
        );
        $pdf->set_header_names($headrs_name);
        /*-------------------------------------------------------*/
        $pdf->SetFontSize(10);

        /*set the table header */
        $pdf->setHeaderTitle(true);

        /*------------------------------*/
        $pdf->SetWidths(array(45,45,45,45));
        $pdf->Ln(7);
        $total = 0;
        foreach ($query as $val)
        {
            if($val->po_canceled==0){
                $yes='no';
            }
            else{
                $yes='yes';

            }

            $pdf->SetFont('Arial','B',12);

            $pdf->Row(array($yes,$val->po_seq,$val->pro_name,$val->qut));


            /*$pdf->Cell(50	,7,$val->First_name,1,0,'C');
            $pdf->Cell(40	,7,$val->Birth_of_date,1,0,'C');
            $pdf->Cell(20	,7,$val->Gender_type,1,0,'C');
            if($pdf->GetStringWidth($val->code)>40)
            {
                $pdf->SetFont('Arial','B',10);
                $pdf->Cell(40	,7,$val->code,1,0,'C');

            }
            else
                {
                    $pdf->SetFont('Arial','B',12);
                    $pdf->Cell(40	,7,$val->code,1,0,'C');
                }


            $pdf->Cell(40	,7,$val->Acadmic_year,1,1,'C');*/
            $total += $val->qut;
        }


        $pdf->Ln(5);
        $pdf->SetX( 40);

        /*remove the table header */
        $pdf->setHeaderTitle(false);
        /*------------------------------*/


        $pdf->SetX(115);
        $pdf->SetFont('', 'B');
        $pdf->Cell(15, 10, 'Total', 0, 0, 'L');
        $pdf->SetX(160);
        $pdf->Cell(15, 7, $total, 0, 0, 'L');
        $pdf->SetX(-70);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX(150);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 10, '..............................', 0, 0, 'L');

        $pdf->Ln(10);
        $pdf->SetX(10);
        $pdf->Cell(15, 10, 'Note :', 0, 0, 'L');
        $pdf->Ln(10);
        $pdf->SetX(10);
        $pdf->SetFont('', '');
        $pdf->Cell(80, 10, $doc->notes, 1, 0, 'L');
        $pdf->Output();
        exit;


    }


}