<?php

namespace App\Http\Controllers;

use App\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class CountriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $showed_search_list = array('country Name');
        $database_search_list = array(1);

        $countries = DB::table('countries')
            ->orderBy('Country_ID','desc')
            ->get();

        return view('school_pages.countries.index',['showed_search_list' => $showed_search_list,'database_search_list'=>$database_search_list],compact('countries'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('school_pages.countries.countries');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'country_name' => 'required|unique:countries',
        ];
        $validator = Validator::make($request->all(), $rules);
        $validator->SetAttributeNames([
            'country_name' => 'Country Name',
        ]);

        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator);
        } else {
            $succsess = 1;
            $countries = new Country();
            $countries->Country_Name = $request->country_name;
            $countries->save();
            return redirect('countries/'.$countries->Country_ID.'/edit')->with(['succsess'=>$succsess]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $country = Country::findorfail($id);
        return view('school_pages.countries.countries',compact('country'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $country = Country::findorfail($id);
        $succsess = Session::get('succsess');

        if ($succsess == 1)
        {
            return view('school_pages.countries.countries',compact('country','succsess'));
        }
        else{
            return view('school_pages.countries.countries',compact('country'));
        }


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [

            'country_name' => ['required','unique:countries,Country_Name,'.$id.',Country_ID'],
        ];
        $validator = Validator::make($request->all(), $rules);
        $validator->SetAttributeNames([
            'country_name' => 'Country Name',
        ]);

        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator);
        } else {
            $succsess = 1;
            $country = Country::findorfail($id);
            $country->Country_Name = $request->country_name;
            $country->save();


            return redirect('countries/'.$id.'/edit')->with(['succsess'=>$succsess]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $country = Country::findorfail($id);
        $country ->delete();
        return redirect('/countries');

    }
    /*-----------------  delete countries from table  ------------------*/
    public function delete_all_selected_countries(Request $request)
    {
        $deleted = array();
        foreach ($request->deleted_user_ids as $class_id_one)
        {
            $user = \App\Country::findOrFail($class_id_one);

                $user->delete();
                array_push($deleted,1);


        }

        return response(['deleted_ids'=>$request->deleted_user_ids,'deleted'=>$deleted]);
    }







}
