<?php

namespace App\Http\Controllers;


use App\AcademicYear;

use App\Classes;
use App\cost_centers;
use App\countries;
use App\nationalty_type;
use App\parents;
use App\relations_students_parents;
use App\students;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;

use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\PDF_HF;



class student_report_controller extends Controller
{


    //-----------------------------table-------------------------//

    public function get_student(Request $request)

    {




        $print_array = array();
        $array =  explode(',', $request->input('print_ids')[0]);

        foreach ($array as $single)
        {
            array_push($print_array, $single);
        }


        $query = DB::table('students')
            ->join('nationalty_type','students.Nationalty_type_id','=','nationalty_type.nationalty_type_id')
            ->join( 'acadimic_year','students.reg_acadmic_year','=','acadimic_year.acadimic_id')
            ->join('student_parent_relations', 'students.person_id', '=', 'student_parent_relations.person_id')

            ->join('parents', 'student_parent_relations.par_id', '=', 'parents.par_id')

            ->join('class','students.registration_class_id','=','class.classid')
            ->where('student_parent_relations.MainId','=',1)
            ->whereIn('students.person_id',$print_array)

            //->select('students.person_id','students.First_name','students.Gander_type','students.Ismas_code','parents.{'1name'}','parents.snam','parents.thname','nationalty_type.nationalty_type','acadimic_year.year_name')
            ->orderBy('students.person_id','desc')
            //->groupBy('students.person_id','students.First_name','students.Birth_of_date','students.registration_date','students.registration_class_id','students.person_type','students.Nationalty_type_id','students.is_leave','students.Gander_type','students.Ismas_code','parents.{'1name'}','parents.snam','parents.thname','nationalty_type.nationalty_type','acadimic_year.year_name')
            ->select('students.person_id','Gander_type','First_name','nationalty_type','Ismas_code','year_name','1name as fname','2name as sname','3name as thname')

            ->get();









        $pdf = new PDF_HF('P','mm','A4');

        $pdf->AddPage();

        $pdf->AliasNbPages('{pages}');





//set font to arial, bold, 14pt

        $pdf->SetFont('Arial','B',14);

//Cell(width , height , text , border , end line , [align] )
        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX( 90);

        $pdf->Cell(40,10,"Students Table",0,0,'C');
        $pdf->Ln(11);

        $pdf->setFillColor(230,230,230);

        $pdf->Ln(15);
        $pdf->Cell(10	,7,'ID',1,0,'C',1);
        $pdf->Cell(60	,7,'Student Name',1,0,'C',1);
        $pdf->Cell(30	,7,'Nationality ',1,0,'C',1);
        $pdf->Cell(20	,7,'Gender',1,0,'C',1);
        $pdf->Cell(30	,7,'Code',1,0,'C',1);
        $pdf->Cell(40	,7,'Academic Year',1,0,'C',1);

        /*-----------------------  change header -----------------------------*/
        $headrs_name = array(
            array("ID",10),
            array("Student Name",60),
            array("Nationality ",30),
            array("Gender",20),
            array("Code",30),
            array("Academic Year",40),


        );
        $pdf->set_header_names($headrs_name);
        /*-------------------------------------------------------*/
        $pdf->SetFontSize(10);

        /*set the table header */
        $pdf->setHeaderTitle(true);

        /*------------------------------*/
        $pdf->SetWidths(array(10,60,30,20,30,40));
        $pdf->Ln(7);




           foreach ($query as $val) {

               if($val->Gander_type == 1){
                   $gen = "Male";
               }
               elseif ($val->Gander_type == 0){
                   $gen = "Female";
               }

               $pdf->SetFont('Arial', 'B', 12);


           $pdf->Row(array($val->person_id,$val->First_name . ' ' . $val->fname . ' ' . $val->sname . ' ' . $val->thname,$val->nationalty_type,$gen,$val->Ismas_code,$val->year_name));

            }


        $pdf->Ln(5);
        $pdf->SetX( 40);

        /*remove the table header */
        $pdf->setHeaderTitle(false);
        /*------------------------------*/

        $pdf->Output();
        exit;


    }


    //-----------------------------report-------------------------//

    public function get_student_rpt($id)

    {


        $student = DB::table('students')
            ->join('nationalty_type','students.Nationalty_type_id','=','nationalty_type.nationalty_type_id')
            ->join( 'acadimic_year','students.reg_acadmic_year','=','acadimic_year.acadimic_id')
            ->join('class','students.registration_class_id','=','class.classid')
            ->where('students.person_id','=',$id)
            ->orderBy('students.person_id','desc')
            ->first();

        //------------------------------IMAGE-------------------//
        $student_id = $id;

        $filename =    $student_id.'_'.'.png';

        if(file_exists(public_path().'/uploads/students/'.$filename))
        {
            $path = public_path().'/uploads/students/'.$filename;
        }
        else{
            $path = public_path().'/uploads/students/defult_image.png';
        }




        //--------------father----------------//

        $query_father = DB::table('parents')
            ->leftjoin('student_parent_relations', 'parents.par_id', '=', 'student_parent_relations.par_id')
            ->leftjoin('countries','parents.Country_ID','=','countries.Country_ID')
            ->where('student_parent_relations.person_id','=',$student_id)
            ->where('student_parent_relations.MainId', '=', 1)
            ->select('Finance','family_name','EMail','Contact_NO','ID_NO','Pass_ID','Address','other_Notes','1name as fname','2name as sname','3name as thname','Country_Name')
            ->first();

        //-----------------Mother--------------------//

        $query_mother = DB::table('parents')
            ->leftjoin('student_parent_relations', 'parents.par_id', '=', 'student_parent_relations.par_id')
            ->leftjoin('countries','parents.Country_ID','=','countries.Country_ID')
            ->where('student_parent_relations.person_id','=',$student_id)
            ->where('student_parent_relations.MainId', '=', 0)
            ->select('Finance','family_name','EMail','Contact_NO','ID_NO','Pass_ID','Address','other_Notes','1name as fname','2name as sname','3name as thname','Country_Name')
            ->first();

     //--------------gender----------------//
       if($student->Gander_type == 1){
            $gen = "Male";
       }
      elseif ($student->Gander_type == 0){
            $gen = "Female";
        }
      //------------------leave-----------------//
        if($student->is_leave == 1){
            $yes = "Yes";
        }
        elseif ($student->is_leave == 0){
            $yes = "No";
        }




        //---------finance-------------//
        if($query_father->Finance == 1 ){
           $yes1= "Yes";
        }
        elseif($query_father->Finance == 0)
        {
            $yes1= "No";
        }

        if($query_mother->Finance == 1 ){
           $m_yes1= "Yes";
        }
        elseif($query_mother->Finance == 0)
        {
            $m_yes1= "No";
        }
       //-----------------siblings--------------//
        /*$father_data = DB::table('relation')
            ->where('Sid','=',$id)
            ->where('MainId', '=', 1)
            ->pluck('par_id')
            ->first();*/

       $fater_id = DB::table('student_parent_relations')
           ->where('person_id','=',$id)
           ->where('MainId','=',1)
           ->first();

        $siblings_data = DB::table('parents')
            ->join('student_parent_relations', 'parents.par_id', '=', 'student_parent_relations.par_id')
            ->join('students', 'student_parent_relations.person_id', '=', 'students.person_id')
            ->join('class','students.registration_class_id','=','class.classid')
            ->where('student_parent_relations.par_id','=',$fater_id->par_id)
            ->where('students.person_id','<>',$id)
            ->select('First_name','Ismas_code','ClassName','1name as fname','2name as sname','3name as thname')
            ->get();



        $pdf = new PDF_HF('P','mm','A4');

        $pdf->AddPage();

        $pdf->AliasNbPages('{pages}');

//set font to arial, bold, 14pt

        $pdf->SetFont('Arial','B',12);

//Cell(width , height , text , border , end line , [align] )


        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX( 90);
//        $pdf->Cell( 40, 40, $pdf->Image($image1, $pdf->GetX(), $pdf->GetY(), 33.78), 0, 0, 'L', false );
        $pdf->SetFont('Arial','UB');
        $pdf->Cell(40,10,"Student Information",0,0,'C');
        $pdf->Ln(5);

        $y = $pdf->GetY() ;
        $pdf->SetY( $y);
        $pdf->Cell(189,50,"",0,0,'C');

        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX( 90);
        $y1 = $pdf->GetY() ;
        $pdf->SetY( $y1);
        $pdf->Image( $path,10,$pdf->SetY( $y1),30);



        $pdf->SetX( 10);
        $pdf->SetFont('Arial','B');


        $pdf->Ln(5);
        $pdf->SetX( 10);
        $pdf->SetFont('Arial','B');

        $pdf->Cell(15,10,'First Name',0,0,'L');
        $pdf->SetX(45);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( 50);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,$student->First_name,0,0,'L');
        $pdf->SetX(50);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');

        $pdf->SetX( -105);
        $pdf->SetFont('','B');

        $pdf->Cell(15,10,'Birth Date',0,0,'L');
        $pdf->SetX(-65);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( -60);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,$student->Birth_of_date,0,0,'L');
        $pdf->SetX(-60);
        $pdf->Cell(30, 12, '.................................', 0, 0, 'L');

        $pdf->Ln(7);
        $pdf->SetX( 10);
        $pdf->SetFont('','B');

        $pdf->Cell(15,10,'Gender',0,0,'L');
        $pdf->SetX(45);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( 50);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,$gen,0,0,'L');
        $pdf->SetX(50);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');

        $pdf->SetX( -105);
        $pdf->SetFont('','B');

        $pdf->Cell(15,10,'Nationality',0,0,'L');
        $pdf->SetX(-65);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( -60);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,$student->nationalty_type,0,0,'L');
        $pdf->SetX(-60);
        $pdf->Cell(30, 12, '.................................', 0, 0, 'L');


        $pdf->Ln(7);
        $pdf->SetX( 10);
        $pdf->SetFont('','B');
        $pdf->Cell(15,10,'Code',0,0,'L');
        $pdf->SetX(45);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( 50);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,$student->Ismas_code,0,0,'L');
        $pdf->SetX(50);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');

        $pdf->SetX( -105);
        $pdf->SetFont('','B');
        $pdf->Cell(15,10,'Acadmic Year',0,0,'L');
        $pdf->SetX(-65);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( -60);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,$student->year_name,0,0,'L');
        $pdf->SetX(-60);
        $pdf->Cell(30, 12, '.................................', 0, 0, 'L');


        $pdf->Ln(7);
        $pdf->SetX( 10);
        $pdf->SetFont('','B');
        $pdf->Cell(15,10,'Registration Date',0,0,'L');
        $pdf->SetX(45);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( 50);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,$student->registration_date,0,0,'L');
        $pdf->SetX(50);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');


        $pdf->SetX( -105);
        $pdf->SetFont('','B');
        $pdf->Cell(15,10,'Registration Class',0,0,'L');
        $pdf->SetX(-65);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( -60);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,$student->ClassName,0,0,'L');
        $pdf->SetX(-60);
        $pdf->Cell(30, 12, '.................................', 0, 0, 'L');

        $pdf->Ln(7);
        $pdf->SetX( 10);
        $pdf->SetFont('','B');
        $pdf->Cell(15,10,'Leave',0,0,'L');
        $pdf->SetX(45);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( 50);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,$yes,0,0,'L');
        $pdf->SetX(50);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');



            $pdf->SetX(-105);
            $pdf->SetFont('', 'B');
            $pdf->Cell(15, 10, 'Has Siblings', 0, 0, 'L');
            $pdf->SetX(-65);
            $pdf->Cell(15, 10, ':', 0, 0, 'L');
            $pdf->SetX(-60);
            $pdf->SetFont('', '');
            $pdf->Cell(30, 10, $student->ClassName, 0, 0, 'L');
            $pdf->SetX(-60);
            $pdf->Cell(30, 12, '.................................', 0, 0, 'L');


        if($student->is_leave == 1){

        $pdf->SetX( -105);
        $pdf->SetFont('','B');
        $pdf->Cell(15,10,'Leave Date',0,0,'L');
        $pdf->SetX(-65);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( -60);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,$student->leave_date,0,0,'L');
        $pdf->SetX(-60);
        $pdf->Cell(30, 12, '.................................', 0, 0, 'L');
        }
        elseif ($student->is_leave == 0){

        }





            $pdf->Ln(8);
            $pdf->setFillColor(230, 230, 230);
            $pdf->SetFont('', 'B');
            $pdf->Cell(60, 7, 'Student Name', 1, 0, 'C', 1);
            $pdf->Cell(60, 7, 'Code', 1, 0, 'C', 1);
            $pdf->Cell(70, 7, 'Class', 1, 0, 'C', 1);

            /*-----------------------  change header -----------------------------*/
            $headrs_name = array(
                array("Student Name", 60),
                array("Code", 60),
                array("Class", 70),

            );
            $pdf->SetFillColor(50, 50, 50);


            $pdf->set_header_names($headrs_name);
            /*-------------------------------------------------------*/
            $pdf->SetFontSize(10);

            /*set the table header */
            $pdf->setHeaderTitle(true);

            /*------------------------------*/
            $pdf->SetWidths(array(60, 60, 70));
            $pdf->Ln(7);


            foreach ($siblings_data as $val) {
                $pdf->SetFont('Arial', '', 12);


                $pdf->Row(array($val->First_name . ' ' . $val->fname . ' ' . $val->sname . ' ' . $val->thname, $val->Ismas_code, $val->ClassName));


            }

            $pdf->SetX(40);

            /*remove the table header */
            $pdf->setHeaderTitle(false);
            /*------------------------------*/



       /************************************************************************************************/
        $pdf->Ln(5);

        $x = $pdf->GetX() ;
        $pdf->SetX( $x);

        $y = $pdf->GetY() ;
        $pdf->SetY( $y);


        $pdf->Line( $x, $y,$x+190,$y);


        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX( 90);
        $pdf->SetFont('','UB');
        $pdf->Cell(40,10,"Father Information",0,0,'C');
        $pdf->Ln(5);

        $y = $pdf->GetY() ;
        $pdf->SetY( $y);
        $pdf->Cell(189,50,"",0,0,'C');

        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX( 90);


        $pdf->Ln(5);
        $pdf->SetX( 10);
        $pdf->SetFont('','B');

        $pdf->Cell(15,10,'First Name',0,0,'L');
        $pdf->SetX(45);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( 50);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,"$query_father->fname",0,0,'L');
        $pdf->SetX(50);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');

        $pdf->SetX( -105);
        $pdf->SetFont('','B');

        $pdf->Cell(15,10,'Second Name',0,0,'L');
        $pdf->SetX(-65);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( -60);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,"$query_father->sname",0,0,'L');
        $pdf->SetX(-60);
        $pdf->Cell(30, 12, '.................................', 0, 0, 'L');

        $pdf->Ln(7);
        $pdf->SetX( 10);
        $pdf->SetFont('','B');

        $pdf->Cell(15,10,'Third Name',0,0,'L');
        $pdf->SetX(45);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( 50);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,"$query_father->thname",0,0,'L');
        $pdf->SetX(50);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');

        $pdf->SetX( -105);
        $pdf->SetFont('','B');

        $pdf->Cell(15,10,'Family Name',0,0,'L');
        $pdf->SetX(-65);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( -60);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,"$query_father->family_name",0,0,'L');
        $pdf->SetX(-60);
        $pdf->Cell(30, 12, '.................................', 0, 0, 'L');

        $pdf->Ln(7);
        $pdf->SetX( 10);
        $pdf->SetFont('','B');

        $pdf->Cell(15,10,'Email',0,0,'L');
        $pdf->SetX(45);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( 50);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,"$query_father->EMail",0,0,'L');
        $pdf->SetX(50);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');

        $pdf->SetX( -105);
        $pdf->SetFont('','B');

        $pdf->Cell(15,10,'Phone',0,0,'L');
        $pdf->SetX(-65);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( -60);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,"$query_father->Contact_NO",0,0,'L');
        $pdf->SetX(-60);
        $pdf->Cell(30, 12, '.................................', 0, 0, 'L');

        $pdf->Ln(7);
        $pdf->SetX( 10);
        $pdf->SetFont('','B');

        $pdf->Cell(15,10,'ID Number',0,0,'L');
        $pdf->SetX(45);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( 50);
        $pdf->SetFont('','');
        $pdf->Cell(50,8,"$query_father->ID_NO",0,0,'L');
        $pdf->SetX(50);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');

        $pdf->SetX( -105);
        $pdf->SetFont('','B');

        $pdf->Cell(15,10,'Passport ID',0,0,'L');
        $pdf->SetX(-65);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( -60);
        $pdf->SetFont('','');
        $pdf->Cell(50,10,"$query_father->Pass_ID",0,0,'L');
        $pdf->SetX(-60);
        $pdf->Cell(30, 12, '.................................', 0, 0, 'L');

        $pdf->Ln(7);
        $pdf->SetX( 10);
        $pdf->SetFont('','B');

        $pdf->Cell(15,10,'Address',0,0,'L');
        $pdf->SetX(45);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( 50);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,"$query_father->Address",0,0,'L');
        $pdf->SetX(50);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');

        $pdf->SetX( -105);
        $pdf->SetFont('','B');

        $pdf->Cell(15,10,'Country',0,0,'L');
        $pdf->SetX(-65);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( -60);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,"$query_father->Country_Name",0,0,'L');
        $pdf->SetX(-60);
        $pdf->Cell(30, 12, '.................................', 0, 0, 'L');

        $pdf->Ln(7);
        $pdf->SetX( 10);
        $pdf->SetFont('','B');

        $pdf->Cell(15,10,'Finance',0,0,'L');
        $pdf->SetX(45);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( 50);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,"$yes1",0,0,'L');
        $pdf->SetX(50);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');

        $pdf->SetX( -105);
        $pdf->SetFont('','B');

        $pdf->Cell(15,10,'Other Notes',0,0,'L');
        $pdf->SetX(-65);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( -60);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,"$query_father->other_Notes",0,0,'L');
        $pdf->SetX(-60);
        $pdf->Cell(30, 12, '.................................', 0, 0, 'L');


     /**************************************************************************************/


        $pdf->Ln(15);

        $x = $pdf->GetX() ;
        $pdf->SetX( $x);

        $y = $pdf->GetY() ;
        $pdf->SetY( $y);


        $pdf->Line( $x, $y,$x+190,$y);



        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX( 90);
        $pdf->SetFont('','UB');
        $pdf->Cell(40,10,"Mother Information",0,0,'C');
        $pdf->Ln(5);

        $y = $pdf->GetY() ;
        $pdf->SetY( $y);
        $pdf->Cell(189,50,"",0,0,'C');

        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX( 90);


        $pdf->Ln(5);
        $pdf->SetX( 10);
        $pdf->SetFont('','B');

        $pdf->Cell(15,10,'First Name',0,0,'L');
        $pdf->SetX(45);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( 50);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,"$query_mother->fname",0,0,'L');
        $pdf->SetX(50);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');

        $pdf->SetX( -105);
        $pdf->SetFont('','B');

        $pdf->Cell(15,10,'Second Name',0,0,'L');
        $pdf->SetX(-65);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( -60);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,"$query_mother->sname",0,0,'L');
        $pdf->SetX(-60);
        $pdf->Cell(30, 12, '.................................', 0, 0, 'L');

        $pdf->Ln(7);
        $pdf->SetX( 10);
        $pdf->SetFont('','B');

        $pdf->Cell(15,10,'Third Name',0,0,'L');
        $pdf->SetX(45);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( 50);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,"$query_mother->thname",0,0,'L');
        $pdf->SetX(50);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');

        $pdf->SetX( -105);
        $pdf->SetFont('','B');

        $pdf->Cell(15,10,'Family Name',0,0,'L');
        $pdf->SetX(-65);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( -60);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,"$query_mother->family_name",0,0,'L');
        $pdf->SetX(-60);
        $pdf->Cell(30, 12, '.................................', 0, 0, 'L');

        $pdf->Ln(7);
        $pdf->SetX( 10);
        $pdf->SetFont('','B');

        $pdf->Cell(15,10,'Email',0,0,'L');
        $pdf->SetX(45);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( 50);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,"$query_mother->EMail",0,0,'L');
        $pdf->SetX(50);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');

        $pdf->SetX( -105);
        $pdf->SetFont('','B');

        $pdf->Cell(15,10,'Phone',0,0,'L');
        $pdf->SetX(-65);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( -60);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,"$query_mother->Contact_NO",0,0,'L');
        $pdf->SetX(-60);
        $pdf->Cell(30, 12, '.................................', 0, 0, 'L');

        $pdf->Ln(7);
        $pdf->SetX( 10);
        $pdf->SetFont('','B');

        $pdf->Cell(15,10,'ID Number',0,0,'L');
        $pdf->SetX(45);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( 50);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,"$query_mother->ID_NO",0,0,'L');
        $pdf->SetX(50);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');

        $pdf->SetX( -105);
        $pdf->SetFont('','B');

        $pdf->Cell(15,10,'Passport ID',0,0,'L');
        $pdf->SetX(-65);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( -60);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,"$query_mother->Pass_ID",0,0,'L');
        $pdf->SetX(-60);
        $pdf->Cell(30, 12, '.................................', 0, 0, 'L');

        $pdf->Ln(7);
        $pdf->SetX( 10);
        $pdf->SetFont('','B');

        $pdf->Cell(15,10,'Address',0,0,'L');
        $pdf->SetX(45);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( 50);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,"$query_mother->Address",0,0,'L');
        $pdf->SetX(50);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');

        $pdf->SetX( -105);
        $pdf->SetFont('','B');

        $pdf->Cell(15,10,'Country',0,0,'L');
        $pdf->SetX(-65);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( -60);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,"$query_mother->Country_Name",0,0,'L');
        $pdf->SetX(-60);
        $pdf->Cell(30, 12, '.................................', 0, 0, 'L');

        $pdf->Ln(7);
        $pdf->SetX( 10);
        $pdf->SetFont('','B');

        $pdf->Cell(15,10,'Finance',0,0,'L');
        $pdf->SetX(45);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( 50);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,"$m_yes1",0,0,'L');
        $pdf->SetX(50);
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');

        $pdf->SetX( -105);
        $pdf->SetFont('','B');

        $pdf->Cell(15,10,'Other Notes',0,0,'L');
        $pdf->SetX(-65);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( -60);
        $pdf->SetFont('','');
        $pdf->Cell(30,10,"$query_mother->other_Notes",0,0,'L');
        $pdf->SetX(-60);
        $pdf->Cell(30, 12, '.................................', 0, 0, 'L');









        $pdf->Output();
        exit;


    }


}