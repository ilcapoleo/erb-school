<?php

namespace App\Http\Controllers;


use App\Employee;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Facades\Response;
use Illuminate\Validation\Rule;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Validator;



class create_user_controller extends Controller
{
    //
   /* public function ret_cost_page()
    {
        return view('admin_pages.cost_center_page');
    }*/

    public function add_account($id)
    {
        $user = User::all()->where('id','=',$id)->first();
        $roles = Role::orderBy('role_seq','asc')->get();
        $moduls = DB::table('modules')
            ->join('roles','modules.id','=','roles.module_id')
            ->where('modules.active','=',1)
            ->orderBy('modules.name','desc')->select('modules.id','modules.name')
            ->get()->unique('name');
        $selected_roles = DB::table('userss')
            ->join('model_has_roles','userss.id','=','model_has_roles.model_id')
            ->join('roles','model_has_roles.role_id','=','roles.id')
            ->where('userss.id','=',$id)
            ->orderBy('roles.id','asc')
            ->get();
        $emploee = DB::table('employee')->where('employee_user_id','=',null)
            ->orwhere('employee_user_id','=',$id)
            ->get();

        $selected_employee = DB::table('employee')->where('employee_user_id','=',$id)
            ->first();

        return view('school_pages.admin_adduser',compact('user','moduls','roles','selected_roles','emploee','selected_employee'));
    }



    public function get_add_user_page()
    {
        $roles = Role::orderBy('role_seq','asc')->get();
        $moduls = DB::table('modules')
            ->join('roles','modules.id','=','roles.module_id')
            ->where('modules.active','=',1)
            ->orderBy('modules.name','desc')->select('modules.id','modules.name')
            ->get()->unique('name');
        $emploee = DB::table('employee')->where('employee_user_id','=',null)->get();

        return view('school_pages.admin_adduser',compact('moduls','roles','emploee'));
    }
   /*---------------- permission --------------------*/
    public function create_new_user()
    {
        $roles = Role::orderBy('name')->get();
        $moduls = DB::table('modules')
            ->join('roles','modules.id','=','roles.module_id')
            ->orderBy('modules.name','desc')->select('modules.id','modules.name')
            ->orderBy('roles.id','asc')
            ->get()->unique('name');
        return view('custom.create_user',compact('roles','moduls'));
    }

    public function regist_new_user(Request $request)
    {
        $rules = array(
            'name' => 'required|string|max:255|unique:userss',
            /*'email' => 'required|string|email|max:255|unique:userss',*/
            'password' => 'required|string|min:6|confirmed:password_confirmation',
            'role'=>'required'
        );

        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), $rules);

        if ($validator->passes())
        {
            $user = new User;
                $user->name = $request->name;
                $user->email = /*$request->email*/NULL;
                $user->password = Hash::make($request->password);
                $user->save();

            $user->assignRole($request->roles_data);

            /*$user->roles()->attach($data['role']);*/
            return response($user);

        }

        elseif ($validator->fails())
        {
           /* $messages = $validator->messages();

            foreach ($rules as $key => $value)
            {
                $verrors[$key] = $messages->first($key);
            }*/
            return response()->json(['error'=>$validator->messages()]);
           /* return Response::json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()

            ), 400);*/ // 400 being the HTTP code for an invalid request.
         //   return response($verrors);
        }

    }



    public function create_new_accunt(Request $request)
    {
        if ($request->current_po_id != "" && $request->current_po_id) {
        $rules = array(
            'name' => [
                'required','string','max:255',
                Rule::unique('userss')->ignore($request->current_po_id,'id')
            ],//'required|string|max:255',Rule::unique('userss')->ignore($request->user_id,'id'),
            /*'email' => [
                'required','string','email','max:255',
                Rule::unique('userss')->ignore($request->current_po_id,'id'),
            ],*///'required|string|email|max:255|unique:userss',Rule::unique('userss')->ignore($request->user_id,'id'),
            'password' => 'confirmed:password_confirmation',
            'employee' => 'required',

        );
        }
        else{
            $rules = array(
                'name' => 'required|string|max:255|unique:userss',

                /*'email' => 'required|string|email|max:255|unique:userss',*/
                'password' => 'required|confirmed:password_confirmation',
                'password_confirmation' => 'required',
                'employee' => 'required',

            );
        }

        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), $rules);

        if ($validator->passes())
        {
            if ($request->current_po_id != "" && $request->current_po_id) {
                $user = User::findOrFail($request->current_po_id);

                if ($user) {
                    DB::table('model_has_roles')
                        ->where('model_type', 'App\User')
                        ->where('model_id', '=', $request->user_id)
                        ->delete();

                    $user->name = $request->name;
                    $user->email = /*$request->email*/NULL;
                    if ($request->password && $request->password != "") {
                        $user->password = Hash::make($request->password);
                    }

                    $user->save();

                    $user_id = $user->id;
                    if ($request->roles_data && $request->roles_data != "") {
                        $user->syncRoles($request->roles_data);
                    }
                    /*
                                    DB::table('employee')
                                        ->where("employee.employee_user_id", '=',  $request->user_id)
                                        ->update(['employee.employee_id'=> $request->employee]);*/

                    $employee = Employee::findOrFail($request->employee);
                    $employee->employee_user_id = $request->current_po_id;
                    $employee->save();

                }

                /*$user->roles()->attach($data['role']);*/

                return response([$user,'current_po_id' => $user_id]);
            }

            else{
                $user = new User;
                $user->name = $request->name;
                $user->email = /*$request->email*/NULL;
                $user->password = Hash::make($request->password);
                $user->save();

                if ($request->roles_data && $request->roles_data != "") {
                    $user->syncRoles($request->roles_data);
                }

                $user_id = $user->id;

                $employee = Employee::findOrFail($request->employee);
                $employee->employee_user_id = $user_id;
                $employee->save();



                return response([$user,'current_po_id' => $user_id]);
            }


        }

        elseif ($validator->fails())
        {
            /* $messages = $validator->messages();

             foreach ($rules as $key => $value)
             {
                 $verrors[$key] = $messages->first($key);
             }*/
            return response()->json(['error'=>$validator->messages()]);
            /* return Response::json(array(
                 'success' => false,
                 'errors' => $validator->getMessageBag()->toArray()

             ), 400);*/ // 400 being the HTTP code for an invalid request.
            //   return response($verrors);
        }

    }


    public function add_new_user2()
    {
        return view('school_pages.employee2');
    }
public function add_new_user()
    {
        return view('school_pages.employee');
    }

}
