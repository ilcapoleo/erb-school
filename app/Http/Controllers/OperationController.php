<?php

namespace App\Http\Controllers;

use App\Operation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class OperationController extends Controller
{
    //
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $showed_search_list = array('ID','Opeartion Name','IN/OUT');
        $database_search_list = array(1,2,3);

        $operation=Operation::all();


        return view('school_pages.operation.index',['showed_search_list' => $showed_search_list,'database_search_list'=>$database_search_list],compact('operation'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('school_pages.operation.create');


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        $rules = [
//            'doc_type_name' => 'required|unique:operation',
//        ];
//        $validator = Validator::make($request->all(), $rules);
//        $validator->SetAttributeNames([
//            'doc_type_name' => 'Operation Name',
//        ]);
//
//        if ($validator->fails()) {
//            return back()->withInput()->withErrors($validator);
//        } else {
//            $succsess = 1;
//            $countries = new Country();
//            $countries->Country_Name = $request->country_name;
//            $countries->save();
//            return redirect('countries/'.$countries->Country_ID.'/edit')->with(['succsess'=>$succsess]);
//        }
//

//        dd($request->all());

        $operation = new Operation();
        $operation->doc_type_name  = $request->doc_type_name;
        $operation->in_out  = $request->result;

        $operation->save();
//        return redirect('operation/'.$operation->inv_doc_id.'/edit');
//        return view('school_pages.operation.create');
        return redirect('operation/'.$operation->inv_doc_id.'/edit');



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $operation = Operation::findorfail($id);
        return view('school_pages.operation.create',compact('operation'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $operation= Operation::findorfail($id);
        $succsess = Session::get('succsess');

        if ($succsess == 1)
        {
            return view('school_pages.operation.create',compact('operation','succsess'));
        }
        else{
            return view('school_pages.operation.create',compact('operation'));
        }


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $operation = Operation::findorfail($id);
        $operation->doc_type_name  = $request->doc_type_name;

        $operation->in_out  = $request->result;

        $operation->save();
        return redirect('operation/'.$id.'/edit');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $operation = Operation::findOrFail($id);
        $operation->delete();

//        DB::table('allowence_details')
//            ->where('allow_id', $request->cur_po_id)
//            ->delete();

        return redirect('/operation');
    }

    /*-----------------  delete countries from table  ------------------*/
    public function delete_all_selected_operation(Request $request)
    {
        $deleted = array();
        foreach ($request->deleted_user_ids as $class_id_one)
        {
            $user = \App\Operation::findOrFail($class_id_one);

            $user->delete();
            array_push($deleted,1);


        }

        return response(['deleted_ids'=>$request->deleted_user_ids,'deleted'=>$deleted]);
    }






}
