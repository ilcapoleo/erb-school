<?php

namespace App\Http\Controllers;

use App\CostCenter;
use App\Department;
use App\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\PDF_HF;


class departments_report_controller extends Controller
{
    //-----------------------------table-------------------------//

    public function get_departments(Request $request)

    {



        $print_array = array();
        $array =  explode(',', $request->input('print_ids')[0]);

        foreach ($array as $single)
        {
            array_push($print_array, $single);
        }

        $query = DB::table('departments')
            ->join('cost_centers','departments.cost_center_id','=','cost_centers.Cost_id')
            ->join('employee','departments.manager_id','=','employee.employee_id')
            ->wherein('departments.dep_id',$print_array)
            ->orderBy('departments.dep_id','desc')
            ->get();





        $pdf = new PDF_HF('P','mm','A4');

        $pdf->AddPage();

        $pdf->AliasNbPages('{pages}');

//set font to arial, bold, 14pt

        $pdf->SetFont('Arial','B',14);

//Cell(width , height , text , border , end line , [align] )
        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX( 90);

        $pdf->Cell(40,10,"Departments Table",0,0,'C');
        $pdf->Ln(11);

        $pdf->setFillColor(230,230,230);

        $pdf->Ln(15);
        $pdf->Cell(20	,7,'ID',1,0,'C',1);
        $pdf->Cell(45	,7,'Department Name',1,0,'C',1);
        $pdf->Cell(45	,7,'Department Code',1,0,'C',1);
        $pdf->Cell(40	,7,'Cost Name',1,0,'C',1);
        $pdf->Cell(40	,7,'Manager',1,0,'C',1);
        /*-----------------------  change header -----------------------------*/
        $headrs_name = array(
            array("ID",20),
            array("Department Name",45),
            array("Department Code",45),
            array("Cost Name",40),
            array("Manager",40)
        );
        $pdf->set_header_names($headrs_name);
        /*-------------------------------------------------------*/
        $pdf->SetFontSize(10);

        /*set the table header */
        $pdf->setHeaderTitle(true);

        /*------------------------------*/
        $pdf->SetWidths(array(20,45,45,40,40));
        $pdf->Ln(7);

        foreach ($query as $val)
        {

            $pdf->SetFont('Arial','B',12);

            $pdf->Row(array($val->dep_id,$val->dep_name,$val->dep_short_code,$val->Cost_name,$val->employee_name));

        }


        $pdf->Ln(5);
        $pdf->SetX( 40);

        /*remove the table header */
        $pdf->setHeaderTitle(false);
        /*------------------------------*/

        $pdf->Output();
        exit;


    }

    //----------------------------report-----------------------------//



    public function get_departments_rpt(Request $request)

    {
        $depart = Department::where('dep_id',$request->id)->first();


     



        $manager = Employee::where('employee_id',$depart->manager_id)->first()->employee_name;

        $cost =CostCenter::where('Cost_id',$depart->cost_center_id)->first()->Cost_name;

        $pdf = new PDF_HF('P','mm','A4');

        $pdf->AddPage();

        $pdf->AliasNbPages('{pages}');

//set font to arial, bold, 14pt

        $pdf->SetFont('Arial','B',12);

//Cell(width , height , text , border , end line , [align] )
        $pdf->SetX($pdf->GetX() - 94);
        $pdf->SetX( 90);

        $pdf->Cell(40,10,"Departments Report for:".' '.$depart->dep_name.' '."Department",0,0,'C');
        $pdf->Ln(11);

        $pdf->Ln(15);
        $pdf->SetX( 10);
        $pdf->Cell(15,10,'Department Name',0,0,'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( 60);
        $pdf->SetFont('','');
        $pdf->Cell(50,8,$depart->dep_name,0,0,'L');
        $pdf->SetX(60);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');

        $pdf->Ln(15);
        $pdf->SetX( 10);
        $pdf->SetFont('','B');
        $pdf->Cell(15,10,'Department Code',0,0,'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( 60);
        $pdf->SetFont('','');
        $pdf->Cell(50,8,$depart->dep_short_code,0,0,'L');
        $pdf->SetX(60);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');

        $pdf->Ln(15);
        $pdf->SetX( 10);
        $pdf->SetFont('','B');
        $pdf->Cell(15,10,'Cost Center',0,0,'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( 60);
        $pdf->SetFont('','');
        $pdf->Cell(50,8,$cost,0,0,'L');
        $pdf->SetX(60);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');

        $pdf->Ln(15);
        $pdf->SetX( 10);
        $pdf->SetFont('','B');
        $pdf->Cell(15,10,'Manager',0,0,'L');
        $pdf->SetX(50);
        $pdf->Cell(15, 10, ':', 0, 0, 'L');
        $pdf->SetX( 60);
        $pdf->SetFont('','');
        $pdf->Cell(50,8,$manager,0,0,'L');
        $pdf->SetX(60);
        $pdf->SetFont('', '');
        $pdf->Cell(30, 12, '..............................', 0, 0, 'L');




        $pdf->Output();
        exit;


    }










}