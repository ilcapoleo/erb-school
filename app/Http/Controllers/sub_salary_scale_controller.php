<?php

namespace App\Http\Controllers;


use App\SalaryScale;

use App\Salay_plan;
use App\SubSalary;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use phpDocumentor\Reflection\Types\Compound;


class sub_salary_scale_controller extends Controller
{

    public function creat_subsalary_scale()
        {
            $acadimic_year = DB::table('acadimic_year')
                ->pluck('year_name', 'acadimic_id');


            $salary_sacle = DB::table('salary_scale')
                ->pluck('salary_name', 'salary_id');

            $sub_salary_scale = DB::table('sub_salary')
                ->get();

            $currencies = DB::table('currencies')
            ->pluck('Currency_name', 'Currency_id');

            return view('school_pages.subsalary_scale',compact('acadimic_year','salary_sacle','sub_salary_scale','currencies'));
        }


        public function add_new_sub_plane(Request $request)
        {
            $rules = array(

                'academic_year' => 'required',
                'table_rows' => 'required',
            );
            $validator = \Illuminate\Support\Facades\Validator::make($request->all(), $rules);


            if ($validator->passes()) {
            if ($request->row_id != "" && $request->row_id) {

                $row = $request->table_rows[0];

                $salrary_plane = Salay_plan::findOrFail($request->row_id);

                $salrary_plane->acadimic_id = $request->academic_year;
                $salrary_plane->salary_scale_id = $row['salary_scale'];
                $salrary_plane->forigen_type = $row['forigen'];
                $salrary_plane->sub_salary_id = $row['sub_salary_scale'];
                $salrary_plane->annual_salary = $row['annual_salary'];
                $salrary_plane->monthly_salary = $row['monthly_salary'];
                $salrary_plane->currency_id = $row['currency_value'];

                $salrary_plane->save();

                $salrary_plane_id = $salrary_plane->plan_id;

                return response([$request, 'just_add_row_id' => $salrary_plane_id]);
            }

            else{
                $row = $request->table_rows[0];

                $salrary_plane = new Salay_plan();

                $salrary_plane->acadimic_id = $request->academic_year;
                $salrary_plane->salary_scale_id = $row['salary_scale'];
                $salrary_plane->forigen_type = $row['forigen'];
                $salrary_plane->sub_salary_id = $row['sub_salary_scale'];
                $salrary_plane->annual_salary = $row['annual_salary'];
                $salrary_plane->monthly_salary = $row['monthly_salary'];
                $salrary_plane->currency_id = $row['currency_value'];

                $salrary_plane->save();

                $salrary_plane_id = $salrary_plane->plan_id;

                return response([$request, 'just_add_row_id' => $salrary_plane_id]);

            }
        }

            elseif ($validator->fails()) {

                return response()->json(['error' => $validator->messages()]);

            }
        }

    public function delete_sub_plane(Request $request)
    {
        $rules = array(

            'row' => 'exists:salay_plan,plan_id',

        );
        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), $rules);
        if ($validator->passes()) {

            $fee = Salay_plan::findOrFail($request->row);
            $fee->delete();

            return response($request);
        }

        elseif ($validator->fails()) {

            return response()->json(['error' => $validator->messages()]);

        }
    }


    public function sub_plane_table(Request $request)
    {


        $plane_table = DB::table('salay_plan')

            ->join('acadimic_year','salay_plan.acadimic_id','=','acadimic_year.acadimic_id')
            ->join('salary_scale','salay_plan.salary_scale_id','=','salary_scale.salary_id')
            ->join('sub_salary','salay_plan.sub_salary_id','=','sub_salary.sub_id')
            ->join('currencies','salay_plan.currency_id','=','currencies.Currency_id')
            ->where('salay_plan.acadimic_id','=',$request->acadimic_year)

            ->get(array(

                'salay_plan.plan_id',
                'salay_plan.salary_scale_id',
                'salay_plan.forigen_type',
                'salay_plan.sub_salary_id',
                'salay_plan.annual_salary',
                'salay_plan.monthly_salary',
                'salay_plan.currency_id',

                'currencies.Currency_name',
                'sub_salary.sub_name',
                'salary_scale.salary_name',

            ));


        return response()->json(['plane_detailes' => $plane_table]);
    }


    /*-------------------  student table  --------------------------*/
    public function salaries_plan_table()
    {
        $showed_search_list = array('Academic Year');
        $database_search_list = array(1);

        $salary_plans = DB::table('acadimic_year')
            //->join('salay_plan','acadimic_year.acadimic_id','salay_plan.acadimic_id')
            ->select('acadimic_year.acadimic_id','acadimic_year.year_name')
            ->groupBy('acadimic_year.acadimic_id','acadimic_year.year_name')
            ->get();

/*dd($salary_plans);*/
        return view('school_pages.show_salary_plan',['showed_search_list' => $showed_search_list,'database_search_list'=>$database_search_list],compact('salary_plans'));
    }

    public function show_salary_plan($year_id)
    {
       $year_data = DB::table('salay_plan')
       ->join('salary_scale','salay_plan.salary_scale_id','salary_scale.salary_id')
       ->join('sub_salary','salay_plan.sub_salary_id','sub_salary.sub_id')
       ->join('currencies','salay_plan.currency_id','currencies.Currency_id')
       ->where('salay_plan.acadimic_id','=',$year_id)
       ->get();
       $showen_year = $year_id;
        $acadimic_year = DB::table('acadimic_year')
            ->pluck('year_name', 'acadimic_id');


        $salary_sacle = DB::table('salary_scale')
            ->pluck('salary_name', 'salary_id');

        $sub_salary_scale = DB::table('sub_salary')
            ->get();

        $currencies = DB::table('currencies')
            ->pluck('Currency_name', 'Currency_id');

        return view('school_pages.subsalary_scale',compact('acadimic_year','salary_sacle','sub_salary_scale','currencies','year_data','showen_year'));

    }


}




