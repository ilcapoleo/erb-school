<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalaryScale extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $timestamps = false;
    protected $primaryKey = 'salary_id';
    protected $fillable = [
        'salary_scalar_name', 'short_code_name'
    ];
    protected $table = 'salary_scale';

    public function cost()
    {
        return $this->hasMany(CostCenter::class, 'cost_id');

    }

    /*public  function scal_save($data){
        $user=self::create(['salary_scalar_name' => 'Flight 10'],['short_code_name' => 'Flight 10']);
        return $user;    }
}*/
}