<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class invoice_master_id extends Model
{
    //
    public $timestamps = false;
    protected $table = 'invoice_master';
    protected $primaryKey = 'invoice_m_id';
}
