<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductType extends Model
{
    public $timestamps = false;
    protected $table = 'products_type';
    protected $primaryKey = 'pro_type_id';
}
