<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class po_rpt extends Model
{
    //
    public $timestamps = false;
    protected $table = 'po_details';
    protected $primaryKey = 'po_id';
}
