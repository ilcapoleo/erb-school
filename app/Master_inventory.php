<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Master_inventory extends Model
{
    //
    public $timestamps = false;
    protected $table = 'inventory_master';
    protected $primaryKey = 'doc_id';
}
