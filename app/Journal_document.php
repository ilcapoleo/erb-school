<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Journal_document extends Model
{
    //
    public $timestamps = false;
    protected $table = 'journal_documents';
    protected $primaryKey = 'Document_id';
}
