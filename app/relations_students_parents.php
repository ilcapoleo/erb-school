<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class relations_students_parents extends Model
{
    //
    public $timestamps = false;
    protected $table = 'student_parent_relations';
    protected $primaryKey = 'person_id';

}
