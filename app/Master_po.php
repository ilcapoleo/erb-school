<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Master_po extends Model
{
    //
    public $timestamps = false;
    protected $table = 'po_master';
    protected $primaryKey = 'po_id';
}
