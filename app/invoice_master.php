<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class invoice_master extends Model
{
    //
    public $timestamps = false;
    protected $table = 'invoice_master';
    protected $primaryKey = 'inventory_m_id';
}
