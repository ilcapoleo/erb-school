<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BeneficiaryType extends Model
{
    protected $table = 'Beneficiary_type';
    protected $primaryKey = 'Beneficiary_id';
}
