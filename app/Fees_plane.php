<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fees_plane extends Model
{
    //
    public $timestamps = false;
    protected $table = 'fees_plan';
    protected $primaryKey = 'enID';
}
