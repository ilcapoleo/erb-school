<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Class_form extends Model
{
    //
    public $timestamps = false;
    protected $table = 'class_form';
    protected $primaryKey = 'form_id';
}
