<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Journal extends Model
{
    //
    public $timestamps = false;
    protected $table = 'journals';
    protected $primaryKey = 'journal_id';
}
