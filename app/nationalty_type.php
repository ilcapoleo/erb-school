<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class nationalty_type extends Model
{
    //
    public $timestamps = false;
    protected $table = 'nationalty_type';
    protected $fillable = ['nationalty_type'];
    protected $primaryKey = 'nationalty_type_id';
}