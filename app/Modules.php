<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Role;

class Modules extends Model
{
    public $timestamps = false;
    protected $table = 'modules';
    protected $primaryKey = 'id';
    protected $fillable = ['name','link'];

    public function role()
    {
       return $this->hasMany(Role::class,'module_id','id');
    }
}
