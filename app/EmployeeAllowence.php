<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeAllowence extends Model
{
    //
    protected $table = 'employee_allowences';
    protected $primaryKey = 'id';
}
