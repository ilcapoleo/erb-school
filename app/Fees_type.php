<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fees_type extends Model
{
    protected $table = 'fees_type';
    public $timestamps = false;
    protected $primaryKey = 'fee_id';
    protected $fillable = ['fees_type'];
}
