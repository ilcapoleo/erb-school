<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class inventory_master extends Model
{
    //
    public $timestamps = false;
    protected $table = 'inventory_master';
    protected $primaryKey = 'doc_id';
}
