<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice_tax extends Model
{
    //
    public $timestamps = false;
    protected $table = 'inventory_tax';
    protected $primaryKey = 'inventory_id';
}
