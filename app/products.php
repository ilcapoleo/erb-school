<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class products extends Model
{
    //
    public $timestamps = false;
    protected $table = 'products';
    protected $primaryKey = 'pro_id';
}
