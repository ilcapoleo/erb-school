<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {

    Route::get('/', function () {
        return view('home');
    });
    /*----------------- user roues ------------------------*/
    Route::get('employee', 'create_user_controller@add_new_user');

    Route::get('employee2', 'create_user_controller@add_new_user2');

    Route::get('add_account/{id}', 'create_user_controller@add_account');

    Route::get('add_account', 'create_user_controller@get_add_user_page');


    /*---------------- users table---------------------*/
    Route::get('show_users', 'show_users_controller@get_table_page');
    Route::get('show_users', 'show_users_controller@get_table_page');

    Route::get('search_users_table', 'show_users_controller@search_users_table');

    Route::get('delete_all_selected_users', 'show_users_controller@delete_all_selected_users');


    /*-----------------------------  super admin pages ---------------------------------------*/
    Route::group(['middleware' => ['role:Super_admin']], function () {
        /*----------------------------  register user ---------------------------------*/
        Route::get('create_new_account', 'create_user_controller@create_new_accunt');

        Route::get('create_new_user', 'create_user_controller@create_new_user');

        Route::get('register_new_users', 'create_user_controller@regist_new_user');

        /*--------- permissions routes------------*/
        Route::get('create_permission', 'roles_permissions_controller@get_permission_page');

        Route::get('add_new_permission', 'roles_permissions_controller@add_new_permission');

        //Route::get('edit_permission/{id}','roles_permissions_controller@edit_permission');

        /*-----------------  roles routes ----------------------*/
        Route::get('create_role', 'roles_permissions_controller@get_roles_page');

        Route::get('add_new_role', 'roles_permissions_controller@add_new_role');
        /*-----------------------  modules routes -----------------------*/
        Route::get('create_module', 'module_controller@get_module_page');

        Route::get('add_new_module', 'module_controller@add_new_module');
    });

    /*-------po pag-----*/
    //Route::get('po','creat_po_controller@creat_po');
    //Route::group(['middleware' => 'timeout'], function () {
    Route::group(['middleware' => ['role:Accountant-PO|Manager-PO|Use-Own-PO|User-All-PO|Super_admin']], function () {
        Route::get('po', 'creat_po_controller@creat_po');
        Route::get('create_po/{id}', 'creat_po_controller@eidet_this_po');
        //});
        Route::get('get_item_type_drop_down', 'creat_po_controller@get_item_type_drop_down');

        Route::get('get_item_name', 'creat_po_controller@get_item_name');

        Route::get('/get_tax', 'creat_po_controller@get_tax')->name('get_tax');

        Route::get('add_new_po', 'creat_po_controller@add_new_po');

        Route::get('set_po_to_close', 'creat_po_controller@set_po_to_close');

        Route::get('get_subtotal', 'creat_po_controller@get_subtotal');

        Route::get('delete_this_po', 'creat_po_controller@delete_this_po');

        Route::get('show_po', 'creat_po_controller@get_table_show_po');

        Route::get('search_po_table', 'creat_po_controller@search_po_table');

        Route::get('delete_all_selected_po', 'creat_po_controller@delete_all_selected_po');

        Route::get('get_currency_of_supplier', 'creat_po_controller@get_currency_of_supplier');

        Route::get('get_department_user', 'creat_po_controller@get_department_user');

        Route::get('get_users_for_department', 'creat_po_controller@get_users_for_department');

        Route::get('refresh_supplier', 'creat_po_controller@refresh_supplier');

        Route::get('get_price_foritem', 'creat_po_controller@get_price_foritem');

        Route::get('refresh_drop_users', 'creat_po_controller@refresh_drop_users');

        Route::get('refresh_drop_departments', 'creat_po_controller@refresh_drop_departments');

        Route::get('set_new_dropdown', 'creat_po_controller@set_new_dropdown');

        Route::get('print_all_selected_po', 'creat_report_po@get_po1');


//        Route::get('product', 'creat_po_controller@creat_product');

        Route::get('show_product', 'creat_po_controller@get_table_show_product');

        /*-------Inventory-accountant  invoice page-----*/

        Route::get('invoice', 'creat_Invoice_controller@creat_inventory_acc');

        Route::get('get_inventory_for_supplier', 'creat_Invoice_controller@get_inventory_for_supplier');

        Route::get('get_all_inventory_data', 'creat_Invoice_controller@get_all_inventory_data');

        Route::get('add_new_invoice', 'creat_Invoice_controller@add_new_invoice');

        Route::get('delete_this_invoice', 'creat_Invoice_controller@delete_this_invoice');

        Route::get('inventory_acc/{id}', 'creat_Invoice_controller@edited_invoice');

        Route::get('create_invoice/{id}', 'creat_Invoice_controller@create_invoice');

        /*-------------------  invoice table ----------------------*/
        Route::get('show_invoice', 'creat_Invoice_controller@get_table_show_invoice');

        Route::get('search_invoice_table', 'creat_Invoice_controller@search_invoice_table');

        Route::get('delete_all_selected_invoice', 'creat_Invoice_controller@delete_all_selected_invoice');

        /*------Pay Purchasing Invoice----*/
        Route::get('pay_invoice', 'creat_Invoice_controller@creat_pay_invoice');

    });


    Route::group(['middleware' => ['role:Manager-Inventory|Accountant-Inventory|User-Own-Inventory|User-All-Inventory|Super_admin']], function () {
        /*-------Inventory page-----*/
        Route::get('inventory', 'creat_inventory_controller@creat_inventory');

        Route::get('show_inventory', 'creat_inventory_controller@get_table_show_inventory');

        Route::get('search_inventory_table', 'creat_inventory_controller@search_inventory_table');

        Route::get('delete_all_selected_inventory', 'creat_inventory_controller@delete_all_selected_inventory');

        Route::get('inventory/{id}', 'creat_inventory_controller@eidet_this_inventory');

        Route::get('get_po_values_drop_down', 'creat_inventory_controller@get_po_values_drop_down');

        Route::get('get_item_product_drop_down', 'creat_inventory_controller@get_item_product_drop_down');

        Route::get('get_quantity_limit', 'creat_inventory_controller@get_quantity_limit');

        Route::get('add_new_inventory', 'creat_inventory_controller@add_new_inventory');

        Route::get('delete_this_inventory', 'creat_inventory_controller@delete_this_inventory');

        /*----------------  print report ----------------------*/
        Route::post('print_inventory_table', 'inventory_report_controller@print_inventory_table');


    });

    Route::group(['middleware' => ['role:Manager-HR|Accountant-HR|Super_admin']], function () {
        /*-------Department page-----*/
        Route::get('department', 'creat_department_controller@creat_department');

        Route::get('department/{id}', 'creat_department_controller@get_this_department');

        Route::get('add_new_department', 'creat_department_controller@add_new_department');

        Route::get('delete_this_department', 'creat_department_controller@delete_this_department');
        /*----------  table -------------*/
        Route::get('department_table', 'creat_department_controller@get_table_departments');

        Route::get('search_departments_table', 'creat_department_controller@search_departments_table');

        Route::get('delete_all_selected_dep', 'creat_department_controller@delete_all_selected_dep');

        /*-------job page-----*/
        Route::get('job', 'creat_job_controller@creat_job');

        Route::get('job/{id}', 'creat_job_controller@get_this_job');

        Route::get('add_new_job', 'creat_job_controller@add_new_job');

        Route::get('delete_this_job', 'creat_job_controller@delete_this_job');

        /*----------------  table -------------------------------*/
        Route::get('jobs_table', 'creat_job_controller@get_table_jobs');

        Route::get('search_jobs_table', 'creat_job_controller@search_jobs_table');

        /*------Leaves Type----*/
        Route::get('leave', 'creat_leave_controller@creat_leave');

        Route::get('leave/{id}', 'creat_leave_controller@get_this_leave_type');

        Route::get('add_new_leave_type', 'creat_leave_controller@add_new_leave_type');

        Route::get('delete_this_leave_type', 'creat_leave_controller@delete_this_leave_type');

        /*--------------------  leave type table --------------------*/

        Route::get('show_leave_type', 'creat_leave_controller@show_leave_type');

        Route::get('search_leave_type_table', 'creat_leave_controller@search_leave_type_table');

        /*--------------------  Nationality --------------------*/


        /*------------------employees_Allowance-----------*/

        Route::resource('employees_allowance', 'EmployeeAllowancesController');

        /*------Allowence & Deduction----*/
        Route::get('allowence_deduction', 'allowence_deduction_controller@get_page');

        Route::get('allowence_details', 'allowence_deduction_controller@get_allowence_details');

        Route::get('allowence_details/{id}', 'allowence_deduction_controller@return_allowence_details');


        Route::get('allowence_deduction/get_b_type', 'allowence_deduction_controller@get_b_type');

        Route::get('allowence_deduction/get_currency', 'allowence_deduction_controller@get_currency');

        Route::get('allowence_deduction/get_salary', 'allowence_deduction_controller@get_salary');

        Route::get('allowence_deduction/save_new', 'allowence_deduction_controller@save_new');

        Route::get('allowence_deduction/save_table_row', 'allowence_deduction_controller@save_table_row');

        Route::get('allowence_deduction/get_table_on_drop_down', 'allowence_deduction_controller@get_table_on_drop_down');

        Route::get('allowence_deduction/edit/{id}', 'allowence_deduction_controller@edit_allownce');

        Route::get('allowence_deduction/delete', 'allowence_deduction_controller@delete_allow');


        Route::get('delete_allow_deduction_details', 'allowence_deduction_controller@delete_allow_deduction_details');

        Route::get('scale_of_allowence_table', 'allowence_deduction_controller@scale_of_allowence_table');


        /*--------------  table -----------------*/
        Route::get('allowence_deduction/table', 'allowence_deduction_controller@allowence_deduction_table');

        Route::get('allowence_deduction/search_allow_table', 'allowence_deduction_controller@search_allow_table');

        /*-------salary Scale-----*/
        Route::get('salary_scale', 'creat_salary_scale_controller@creat_salary_scale');

        Route::get('delete_salary_scale', 'creat_salary_scale_controller@delete_salary_scale');
        Route::get('delete_salary_scale', 'creat_salary_scale_controller@delete_salary_scale');

        Route::get('search_salary_table', 'creat_salary_scale_controller@search_salary_table');


        Route::get('working_days', 'creat_salary_scale_controller@creat_working_days');


        Route::get('salary_save', 'creat_salary_scale_controller@saveData');

        Route::get('salary_table', 'creat_salary_scale_controller@salaryTable');

        Route::get('salary_scal/{id}', 'creat_salary_scale_controller@get_this_salary');


    });

    /*--------------------------------Accouning ------------------------*/

    /*------journals----------*/

    Route::get('journals', 'creat_accounting_controller@creat_journals');

    Route::get('journals/table', 'creat_accounting_controller@journals_table');

    Route::get('search_journals_table', 'creat_accounting_controller@search_journals_table');

    Route::get('journals/{id}', 'creat_accounting_controller@edit_index_journals');

    Route::get('check_unique', 'creat_accounting_controller@check_unique');

    Route::get('add_new_journal', 'creat_accounting_controller@add_new_journal');

    Route::get('delete_journal', 'creat_accounting_controller@delete_journal');


    Route::resource('journal-entry', 'journalEntryController');

    /*--------------------  products -------------------------------*/
    Route::resource('products', 'ProductsController');
    Route::POST('products/delete', 'ProductsController@destroy');


    /*-----Vendors---------*/
    Route::get('vendor', 'creat_accounting_controller@creat_vendor');
    Route::Resource('vendors', 'VendorsController');

    /*--------------------  Vendors table --------------------*/
    Route::get('vendor/{id}', 'creat_accounting_controller@get_this_vendor');

    Route::get('show_vendors', 'creat_accounting_controller@show_vendors');

    Route::get('search_vendors_table', 'creat_accounting_controller@search_vendors_table');

    /*-----Cost Center---------*/
    Route::get('cost_center', 'creat_costcenter_controller@creat_cost_center');

    /*--------------------  Cost Center table --------------------*/

    Route::get('costcenter/{id}', 'creat_costcenter_controller@get_this_costcenter');


    Route::get('show_costcenter', 'creat_costcenter_controller@show_costcenter');


    Route::get('search_costcenter_table', 'creat_costcenter_controller@search_costcenter_table');


    /*-----Check Paid---------*/

    Route::get('check', 'creat_accounting_controller@creat_check');
    /*--------------------  Check Paid table --------------------*/

    Route::get('check/{id}', 'creat_check_controller@get_this_check');


    Route::get('show_check', 'creat_check_controller@show_check');


    Route::get('search_check_table', 'creat_check_controller@search_check_table');


    /*--------------------------------Admission ------------------------*/


    /*-------------------------------------------- student  --------------------------------------------------*/
    Route::get('student', 'student_controller@ret_student_page');

    Route::get('all_students', 'student_controller@all_students');

    Route::get('fill_nationality_option', 'student_controller@fill_nationality_option');

    Route::get('fill_acadimic_option', 'student_controller@fill_acadimic_option');

    Route::get('fill_registration_option', 'student_controller@fill_registration_option');

    Route::get('fill_countries_option', 'student_controller@fill_countries_option');

    Route::get('fill_mother_countries_option', 'student_controller@fill_mother_countries_option');

    /*Route::get('add_new_student','student_controller@add_new_student');

    Route::get('add_new_parent','student_controller@add_new_parent');*/

    Route::post('submit_all_form', 'student_controller@submit_all_form');


    Route::get('edit_all_form', 'student_controller@edit_all_form');

    Route::get('get_all_students_data', 'student_controller@get_all_students_data');

    /*-------------------------  staff student page   ---------------------------------*/
    Route::get('all_stuff_employee', 'student_controller@all_stuff_employee');

    Route::get('return_staff_with_partner_data', 'student_controller@return_staff_with_partner_data');

    Route::get('fill_siblings_staff_table', 'student_controller@fill_siblings_staff_table');
    /*=========================================================================*/

    Route::get('return_father_data', 'student_controller@return_father_data');


    Route::get('return_mother_data', 'student_controller@return_mother_data');

    Route::get('fill_siblings_table', 'student_controller@fill_siblings_table');

    Route::get('get_allstudents_table', 'student_controller@get_allstudents_table');


    Route::get('delete_student_row', 'student_controller@delete_student_row');


    Route::get('delete_relation_only_row', 'student_controller@delete_relation_only_row');

    Route::get('get_all_students_datafor_edit', 'student_controller@get_all_students_datafor_edit');

    Route::get('fill_siblings_table_edit', 'student_controller@fill_siblings_table_edit');

    Route::get('student/{student_id}', 'student_controller@re_student_data');

    Route::post('image_sub', 'student_controller@set_image')->name('image_send');


    Route::get('students_table', 'student_controller@get_table_show_student');

    Route::get('search_student_table', 'student_controller@search_student_table');

    Route::get('delete_all_selected_student', 'student_controller@delete_all_selected_student');

    Route::get('refresh_student_table', 'student_controller@refresh_student_table');


    /*----------------------------------fees-------------------------------*/
    /*Route::get('student','creat_admission_controller@creat_student');*/

    Route::get('fees', 'fees_controller@creat_fees');

    Route::get('fees_save', 'fees_controller@saveData');

    Route::get('delete_fees', 'fees_controller@delete_fees');

    Route::get('delete_fees/{id}', 'fees_controller@get_this_fees');

    Route::get('fees_table', 'creat_admission_controller@fees_table');

    Route::get('search_fees_table', 'creat_admission_controller@search_fees_table');

    Route::get('fees/{id}', 'fees_controller@get_this_fees');


    /*-------------------  scale_fees ------------------------------*/
    Route::get('scale_fees', 'fees_plane_controller@creat_scale_fees');

    Route::get('get_journal_document', 'fees_plane_controller@get_journal_document');

    Route::get('add_new_sub_fee', 'fees_plane_controller@add_new_sub_fee');

    Route::get('delete_sub_fee', 'fees_plane_controller@delete_sub_fee');

    Route::get('sub_fees_table', 'fees_plane_controller@sub_fees_table');


    /*--------------------------  sub salary scale ---------------------------------*/
    Route::get('subsalary_scale', 'sub_salary_scale_controller@creat_subsalary_scale');

    Route::get('add_new_sub_plane', 'sub_salary_scale_controller@add_new_sub_plane');

    Route::get('delete_sub_plane', 'sub_salary_scale_controller@delete_sub_plane');

    Route::get('sub_plane_table', 'sub_salary_scale_controller@sub_plane_table');

    Route::get('salaries_plan_table', 'sub_salary_scale_controller@salaries_plan_table');

    Route::get('subsalary_scale/{year_id}', 'sub_salary_scale_controller@show_salary_plan');
//-----------------------------------classes----------------

    Route::get('classes', 'Class_controller@creat_classes');

    Route::get('classes/{id}', 'Class_controller@edit_classes');

    Route::get('classes_table', 'Class_controller@classes_table');

    Route::get('search_classes_table', 'Class_controller@search_classes_table');

    Route::get('check_unique_class', 'Class_controller@check_unique_class');

    Route::get('add_new_class', 'Class_controller@cost_center');

    Route::get('delete_class', 'Class_controller@delete_class');

    Route::get('get_classes_rpt/{id}', 'class_report_controller@get_class_rpt');


    Route::group(['middleware' => ['role:Employee-HR|Manager-HR|Accountant-HR|Super_admin']], function () {
        /*----------------------------  leave request -----------------------------------------*/
        Route::get('leave_request', 'leave_request_controller@leave_request_page');

        Route::get('leave_request/check_date', 'leave_request_controller@check_date');

        Route::get('leave_request/edit/{id}', 'leave_request_controller@edit_leave_request_page');

        Route::get('leave_request/delete', 'leave_request_controller@delete_leave_request_page');

        Route::get('leave_request/get_department', 'leave_request_controller@get_department');

        Route::get('leave_request/save', 'leave_request_controller@save_leave_req');

        /*------------------------- leave request table -------------------------------------*/
        Route::get('show_leave_request', 'leave_request_controller@get_table_show_leave_request');

        Route::get('search_leave_request_table', 'leave_request_controller@search_leave_request_table');

        Route::get('delete_all_selected_leave_req', 'leave_request_controller@delete_all_selected_leave_req');

        /*------Borrowing----*/
        Route::get('borrow', 'borrow_controller@borrow_page');

        Route::get('borrow/get_months_drop_down', 'borrow_controller@get_months_drop_down');

        Route::get('borrow/save', 'borrow_controller@save_borrow');

        Route::get('borrow/delete', 'borrow_controller@delete_borrow');

        Route::get('borrow/edit/{id}', 'borrow_controller@edit_borrow');

        /*------------  table -------------------*/
        Route::get('borrow_table', 'borrow_controller@get_table_show_borrow');

        Route::get('search_borrow_table', 'borrow_controller@search_borrow_table');

        Route::get('delete_all_selected_borrow', 'borrow_controller@delete_all_selected_borrow');

    });

    /*-------Change password page-----*/
    Route::get('change_password', 'change_password_controller@change_password');

    Route::get('change_old_pass', 'change_password_controller@change_old_pass');


    /*------Leaves Request----*/
    /*------Calender Leaves Request----*/
    Route::get('leave_calender', 'creat_leave_controller@creat_leave_calender');

    Route::group(['middleware' => ['role:Manager-Budget|Accountant-Budget|User-Own-Budget|User-All-Budget|Super_admin']], function () {
        /*-------------Budget-------------*/
        Route::get('budget_details', 'creat_budget_controller@creat_budget');

        Route::get('budget_total', 'creat_budget_controller@creat_budget_total');

    });
    /*---------------------- pdf  -------------------------------------*/
    Route::get('get_pdf', 'creat_pdf_controller@get_pdf');

    Route::get('get_products', 'creat_product_controller@get_products');
    Route::get('get_invent', 'creat_invent_controller@get_invent');

    Route::get('get_po', 'creat_po_invoice@get_po');


    /*---------------------- Reports -------------------------------------*/
    Route::post('get_po1', 'creat_report_po@get_po1');   /*--table po ---*/

    Route::get('get_po2/{id}', 'creat_rpt_po@get_po2');  /*---report po ---*/


    /*---------------------------------------------Purchase---------------------------------------*/
    /*----------------------Product-------------------*/
    Route::post('get_product', 'product_report_controller@get_product'); /*--table Product ---*/

    Route::get('get_product_rpt/{id}', 'product_report_controller@get_product_rpt'); /*--Report Product ---*/


    /*------------------------------------------HR reports-------------------------------------*/

    /*----------------------employee-------------------*/
    Route::post('get_employee', 'employee_report_controller@get_employee'); /*--table employee ---*/

    Route::get('get_employee_rpt/{id}', 'employee_report_controller@get_employee_rpt'); /*--Report employee ---*/


    /*----------------------employee allowance-------------------*/
    Route::post('get_employeeAllow', 'employeeAllow_report_controller@get_employeeAllow'); /*--table employee ---*/

    Route::get('get_employeeAllow_rpt/{employee}/{academic_year}', 'employeeAllow_report_controller@get_employeeAllow_rpt'); /*--Report employee ---*/


    /*------------------Leave------------------*/
    Route::post('get_leave', 'leave_report_controller@get_leave'); /*--table leave ---*/

    Route::get('get_leave_rpt/{id}', 'rpt_leave_report_controller@get_leave_rpt'); /*--report leave ---*/

    /*---------------------Leave Request------------------*/
    Route::post('get_leaveRequest', 'leaveRequest_report_controller@get_leaveRequest'); /*--table leave Request ---*/

    Route::get('get_leaveRequest_rpt/{id}', 'rpt_leaveRequest_report_controller@get_leaveRequest_rpt'); /*--Report leave Request ---*/

    /*------------------Allowence-------------------*/
    Route::post('get_allowence', 'allowence_report_controller@get_allowence'); /*--table allowence ---*/

    Route::get('get_allowence_rpt/{id}', 'allowence_report_controller@get_allowence_rpt'); /*--report allowence---*/
    /*------------------scale of Allowence -------------------*/
    Route::post('get_allowence_det', 'allowence_report_controller@get_allowence_det'); /*--table allowence ---*/

    Route::get('get_allowence_det_rpt/{id}', 'allowence_report_controller@get_allowence_det_rpt'); /*--report allowence---*/

    /*----------------------Countries-------------------*/
    Route::post('get_country', 'country_report_controller@get_country'); /*--table country ---*/
    Route::get('get_country_rpt/{id}', 'country_report_controller@get_country_rpt'); /*--Report country ---*/


    /*----------------------Nationality-------------------*/
    Route::post('get_nationality', 'nationality_report_controller@get_nationality'); /*--table country ---*/
    Route::get('get_nationality_rpt/{id}', 'nationality_report_controller@get_nationality_rpt'); /*--Report country ---*/


    /*---------------Salary Sclae-------------------*/


    Route::post('get_salary_scale', 'salary_scale_report_controller@get_salary_scale'); /*--table salary scale ---*/

    Route::get('get_salary_scale_rpt/{id}', 'salary_scale_report_controller@get_salary_scale_rpt'); /*--report salary scale ---*/

    /*---------------Salaries Plan-------------------*/
    Route::post('get_salary_plan', 'salary_plan_report_controller@get_salary_plan'); /*--table salary plan---*/

    Route::get('get_salary_plan_rpt/{id}', 'salary_plan_report_controller@get_salary_plan_rpt'); /*--report salary plan---*/

    /*---------------Departments-----------------------*/
    Route::post('get_departments', 'departments_report_controller@get_departments'); /*--table departments ---*/

    Route::get('get_departments_rpt/{id}', 'departments_report_controller@get_departments_rpt'); /*--report departments ---*/


    /*---------------------------Jobs-------------------------------*/
    Route::post('get_jobs', 'jobs_report_controller@get_jobs'); /*--table jobs ---*/

    Route::get('get_jobs_rpt/{id}', 'jobs_report_controller@get_jobs_rpt'); /*--report jobs ---*/

    /*-------------------------------------------------------------------------------------------*/
    Route::post('get_borrow', 'borrow_report_controller@get_borrow'); /*--table  Borrow---*/

    Route::get('get_borrow_rpt/{id}', 'borrow_report_controller@get_borrow_rpt'); /*--report  Borrow---*/

    /*----------------------------------------------------------------------------------------*/
    Route::post('get_invoice', 'invoice_report_controller@get_invoice'); /*--table  Invoice---*/

    Route::get('get_invoice_rpt/{id}', 'invoice_report_controller@get_invoice_rpt'); /*--table  Invoice---*/

    /*-----------------------------------------------------------------------------------------*/

    Route::post('get_inventory', 'inventory_report_controller@print_inventory_table'); /*---table inventory --*/

    Route::get('get_inventory_rpt/{id}', 'inventory_report_controller@get_inventory_rpt'); /*---report inventory --*/


    /*----------------------------------------ADMISSION REPORTS--------------------------*/

    Route::post('get_class', 'class_report_controller@get_class'); /*--table jobs ---*/

    Route::get('get_class_rpt/{id}', 'class_report_controller@get_class_rpt'); /*--report jobs ---*/

    /*---------------------------------------------------*/
    Route::post('get_fees', 'fees_report_controller@get_fees'); /*--table fees ---*/

    Route::get('get_fees_rpt/{id}', 'fees_report_controller@get_fees_rpt'); /*--report fees ---*/

    /*----------------------------------------------------*/
    Route::post('get_student', 'student_report_controller@get_student'); /*--table fees ---*/

    Route::get('get_student_rpt/{id}', 'student_report_controller@get_student_rpt'); /*--report fees ---*/

    /*----------------------------------------Accounting REPORTS--------------------------*/
    /*-------journals-------*/

    Route::post('get_journals', 'journals_report_controller@get_journals'); /*--table journals ---*/

    Route::get('get_journals_rpt/{id}', 'journals_report_controller@get_journals_rpt'); /*--report journals ---*/

    /*-------vendors-------*/
    Route::post('get_vendor', 'vendor_report_controller@get_vendor'); /*--table vendor ---*/

    Route::get('get_vendor_rpt/{id}', 'vendor_report_controller@get_vendor_rpt'); /*--report vendor --*/

    /*-------Cost Center-------*/
    Route::post('get_cost_center', 'CostCentersController@get_cost_center'); /*--table cost center ---*/

    Route::get('get_cost_center_rpt/{id}', 'CostCentersController@get_cost_center_rpt'); /*--report cost center --*/

    /*-------User Table-------*/
    Route::post('get_user', 'creat_user_controller@get_user'); /*--table cost center ---*/


    /*------------------------ roles --------------------------------*/
    /*Route::group(['middleware' => ['role:admin-api|user-api']], function () {
        Route::get('module_1',function ()
        {
            return view('test_pages.module_1');
        });
    });*/
    Route::get('crystal_rpt', 'crystal_rpt_controller@crystal_rpt'); /*---report inventory --*/
    Route::resource('employees_allowance', 'EmployeeAllowancesController');
    Route::get('employees_allowance/{emp_id}/{year_id}', 'EmployeeAllowancesController@show_employee_allowence');
    Route::resource('employees', 'EmployeeController');
    Route::resource('journals-entry', 'JournalEntryController');
    Route::resource('jobs', 'JobController');

    Route::resource('operation', 'OperationController');
    Route::get('delete_all_selected_operation', 'OperationController@delete_all_selected_operation');

    Route::resource('countries', 'CountriesController');
    Route::get('delete_all_selected_countries', 'CountriesController@delete_all_selected_countries');
    Route::resource('nationalities', 'NationalitiesController');
    Route::get('delete_all_selected_nationalities', 'NationalitiesController@delete_all_selected_nationalities');
    Route::get('add_new_nationality', 'NationalitiesController@add_new_nationality');
    Route::get('delete_this_nationality', 'NationalitiesController@delete_this_nationality');
    Route::resource('cost_centers', 'CostCentersController');
    Route::get('get_manager', 'EmployeeController@getManager');
    Route::get('get_job', 'EmployeeController@getJob');
    Route::get('get_allow_value', 'EmployeeController@getAllowVal');
    Route::get('employee_allowences_get_table', 'EmployeeAllowancesController@getAllowTable');


    /*------------------------ Ajax DropDown  --------------------------------*/

    Route::get('drop-down', 'AjaxController@DropDown');
    Route::get('drop-down-nation', 'AjaxController@nation');
    Route::get('drop-down-country', 'AjaxController@country');
    Route::get('drop-down-depart', 'AjaxController@depart');
    Route::get('drop-down-job', 'AjaxController@job');
    Route::get('drop-down-academic', 'AjaxController@academic');

    Route::get('testpass', function () {
        event(new App\Events\StatusLiked('Someone'));
        return "Event has been sent!";
    });

    Route::get('ttt', function () {

        return view('test');
    });

    Route::get('/new_oto', function () {
        return view('welcome');
    });
    Route::get('/broadcast', function () {
        event(new \App\Events\TestEvent('Broadcasting in Laravel using Pusher!'));

    });
});