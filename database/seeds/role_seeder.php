<?php

use Illuminate\Database\Seeder;

class role_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $auther = \App\Role::create([
            'name' => 'Auther',
            'slug' => 'auther',
            'permissions' => json_encode([
                'create-post' => true
            ]),
        ]);

        $editor = \App\Role::create([
            'name' => 'Editor',
            'slug' => 'editor',
            'permissions' => json_encode([
                'update-post' => true,
                'publish-post' => true
            ]),
        ]);
    }
}
